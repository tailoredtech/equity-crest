<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PyroCMS Mandrill Helpers
 *
 * @author      PyroCMS Dev Team
 * @copyright   Copyright (c) 2012, PyroCMS LLC
 */

// ------------------------------------------------------------------------

/**
 *
 * To send emails via mandrill
 *
 * @param string $file The name of the file to load.
 * @param string $ext The file's extension.
 */
function send_mandrill_mail($templatename = null, $emailto = null, $emailname = null, $merge = null, $from_email= null, $cc = null, $bcc = null, $global_merge_vars=null, $attachments = null, $images = null)
{

	require_once FCPATH . 'system/libraries/Mandrill.php';
	$mandrill = new Mandrill('jZS3iYfa9Cb7n1_qunjnKw');
	try {
	$to =  array(
            array(
                'email' => $emailto,
                'name' => $emailname
            )
        );
    if(!empty($cc))
    {
    	$multiple_cc = explode(',', $cc);
    	if (empty ($multiple_cc) || $multiple_cc == $cc)
    		{
	    		//There is only one cc
	    		 $to[]=array(
			            'email' => $cc,
			            'type' => 'cc');
				if (!empty($merge))
					{
						$vars = $merge[0]['vars'];
						$merge[]=array('rcpt'=>$cc,'vars'=>$vars);
					}
    		}
    	else{
	    	$vars = $merge[0]['vars'];
			foreach($multiple_cc as $single_cc)
			{
				$to[] = array(
					'email' => $single_cc,
					'type' => 'cc'
				);
				if (!empty($vars)){
				$merge[]=array(
				'rcpt'=> $single_cc,
				'vars'=> $vars
				);
				}
			}	
    	}
    }
    
    if(!empty($bcc))
    {
    	$multiple_bcc = explode(',', $bcc);
		foreach($multiple_bcc as $single_bcc)
		{
			$to[] = array(
				'email' => $single_bcc,
				'name' => $bcc,
				'type' => 'bcc'
			);
		}
    }
    		    
    $message = array(
        'to' => $to,
        'from_email' => $from_email,
        'important' => false,
        'view_content_link' => true,
        'merge' => true,
        'global_merge_vars' => $global_merge_vars,
        'merge_vars' => $merge,
        'attachments' => $attachments,
        'images' => $images);
    
    $async = true;
    $result = $mandrill->messages->sendTemplate($templatename,'',$message, $async);
	return $result;
	}
	catch(Mandrill_Error $e) {
	    // Mandrill errors are thrown as exceptions
	   // echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
	    $the_string = 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage() . json_encode($merge);
	    if( $fh = @fopen( FCPATH . 'mandrill-log.log', "a+" ) )
		{
			$the_string=date ("Y-m-d H:i:s")." ".$the_string." ".PHP_EOL;
			fputs( $fh, $the_string, strlen($the_string) );
			fclose( $fh );
		}
	    // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists'
	   // throw $e;
	}
}

