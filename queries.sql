
ALTER TABLE `user_master` 
ADD COLUMN `from_entrepreneur_pop` TINYINT(3) NULL DEFAULT 0 AFTER `activated_on`;


ALTER TABLE `user_master` 
CHANGE COLUMN `from_entrepreneur_pop` `from_entrepreneur_popup` TINYINT(3) NULL DEFAULT '0' ;


ALTER TABLE `user_master` 
CHANGE COLUMN `from_entrepreneur_popup` `entrepreneur` TINYINT(3) NULL DEFAULT '0' ;

