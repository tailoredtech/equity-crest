<?php

function rand_string($length) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $str = '';
    $size = strlen($chars);
    for ($i = 0; $i < $length; $i++) {
        $str .= $chars[rand(0, $size - 1)];
    }
    return $str;
}

function DayDifference($start, $end) {
    $start = strtotime($start);
    $end = strtotime($end);
    $diff = $end - $start;
    return round($diff / 86400);
}

function format_money($amount){

    if($amount >= 10000000){
       $crores = round(($amount/10000000),2);
         if($crores == 1){
           return  $crores.' Crore';
         }else{
            return  $crores.' Crores'; 
         }
         
    }else if($amount >= 100000){
        $lakhs = round(($amount/100000),2);
        
        if($lakhs == 1){
           return  $lakhs.' Lakh';
         }else{
            return  $lakhs.' Lakhs'; 
         }
    }
    else if($amount >= 1000)
    {
       $tenthousand = round(($amount/1000),2); 
         if($tenthousand == 1){
           return  $tenthousand.' Thousand';
         }else{
            return  $tenthousand.' Thousand'; 
         }
    }
   
}

function rupeeFormat($str)
{   $insertstr=',';
    $len = strlen($str);
	$pos = array();
    if($len > 4){
        switch ($len) {
            case 14:
                $pos = array(1,3,5,7,9,11);
                break;
            case 13:
                $pos = array(2,4,6,8,10);
                break;
            case 12:
                $pos = array(1,3,5,7,9);
                break;
            case 11:
                $pos = array(2,4,6,8);
                break;
            case 10:
                $pos = array(3,5,7);
                break;
            case 9:
                $pos = array(2,4,6);
                break;
            case 8:
                $pos = array(1,3,5);
                break;
            case 7:
                $pos = array(2,4);
                break;
            case 6:
                $pos = array(1,3);
                break;
            case 5:
                $pos = array(2);
                break;
        }
        
        $offset=-1;
        foreach($pos as $p)
        {
            $offset++;
            $str = substr($str, 0, $p+$offset) . $insertstr . substr($str, $p+$offset);
            
        }
    }
  
    return $str;
}

function get_sectors($id,$table)
{
    $CI_OBJ = & get_instance();
     $CI_OBJ->db->select('*');
     $CI_OBJ->db->where('id',$id);
     $QueryObj = $CI_OBJ->db->get($table);
      $res = $QueryObj->result_array();

      return $res[0]['name'];  
}



if (!function_exists('pr')) {

    function pr($expression) {
        echo "<pre>";
        print_r($expression);
        echo "</pre>";
    }

}


if (!function_exists('server')) {

    function server() {
        echo "<pre>";
        print_r($_SERVER);
        echo "</pre>";
         die();
    }

}
?>
