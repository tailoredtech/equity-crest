<div class="container">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <h1 class="page-title">Reset Password</h1>
    </div>

    <div class="row content-box">
        <div class="col-sm-12">            
              <form method="post" id="reset-pass-form" action="<?php echo base_url(); ?>user/reset_password_process">
                <div class=" col-sm-4">
                    
                    <div class="form-group row">
                        <label for="password" class="col-md-12 col-sm-12 col-xs-12 form-label">Password</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="password" class="form-control form-input" id="userpassword" name="userpassword" placeholder="">
                        </div>
                    </div>               
               
                    <div class="form-group row">
                        <label for="password" class="col-md-12 col-sm-12 col-xs-12 form-label">Confirm Password</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="password" class="form-control form-input" id="confirm-password" name="confirm_password" placeholder="">
                        </div>
                    </div>               

               
                    <div class="form-group row">
                        <div class="col-md-12 col-sm-12 col-xs-12">                        
                            <input type="hidden" name="email" value="<?php echo $email; ?>" />
                            <input type="hidden" name="id" value="<?php echo $id; ?>" />
                            <input type="submit" value="Reset" name="submit" class="btn btn-eq-common">
                        </div>    
                    </div>
                   
                    
                </div>
              </form>            
        </div>
    </div>
</div>  

<script type="text/javascript">
        $(document).ready(function(){
            $('#reset-pass-form').validate({
                rules: {  
                            email: { required : true,
                                    email: true
                                },
                            userpassword: { required : true,
                                    minlength:6
                            },
                            confirm_password:{ required : true,
                                               minlength:6,
                                               equalTo: "#userpassword"
                            }
                        }
            });
          
          
            
        });
    </script>


        