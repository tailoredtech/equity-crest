<div class="container investee">
  <div class="row">
    <div class="col-sm-12 col-xs-12">
      <h2 class="section-title"></h2>
    </div>
    <?php  $this->load->view("front-end/investee_sidebar"); ?>
    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 sidebar">
        <?php 
            if($activePanel =='')
                $this->load->view("investee/investee_company_info");
            else if($activePanel =='team')
                $this->load->view("investee/investee_team");
            else if($activePanel =='business')
                $this->load->view("investee/investee_business");
            else if($activePanel =='revenue')
                $this->load->view("investee/investee_revenue");
            else if($activePanel =='market')
                $this->load->view("investee/investee_market");
            else if($activePanel =='raise')
                $this->load->view("investee/investee_raise");
            else if($activePanel =='financial')
                $this->load->view("investee/investee_financial");
            else if($activePanel =='product_info')
                $this->load->view("investee/investee_product_info");
            else if($activePanel =='financial_info')
                $this->load->view("investee/investee_financial_info");
            else if($activePanel =='achievements')
                $this->load->view("investee/investee_achievements");
            else if($activePanel =='updates')
                $this->load->view("investee/investee_updates");
        ?>
    </div>
</div>

<!-- POPUP company logo Change CONTAINER -->
<div class="modal fade" id="profileimgModal" tabindex="-1" role="dialog" aria-labelledby="profileimgModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="profileimgModalLabel">Change Company Logo</h4>
        </div>
        <div class="modal-body">
            <form id="upload-profile-image" action="<?php echo base_url(); ?>user/ajax_upload_image" method="post" enctype="multipart/form-data">
                <div class="form-group">
                        <label class="col-sm-2 form-label">Image</label>
                        <div class="col-sm-10">
                            <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput" style="margin-bottom:0;">
                                <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                                <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="userfile"></span>
                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                            </div>
                            <label id="logo_upload_error" for="question" class="error" style="height: 23px;padding: 0 0 0 1px;white-space: normal;">&nbsp;</label>
                        </div>
                </div>
                <div class="text-right form-group">
                    <input class="btn btn-eq-common" type="submit" value="Submit" />
                </div>
            </form>
        </div>
    </div>
  </div>
</div>


<!-- POPUP company banner Change CONTAINER -->
<div class="modal fade" id="bannerimgModal" tabindex="-1" role="dialog" aria-labelledby="bannerimgModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="bannerimgModalLabel">Change Company Banner</h4>
        </div>
        <div class="modal-body">
            <form id="upload-banner-image" action="<?php echo base_url(); ?>user/ajax_upload_image" method="post" enctype="multipart/form-data">
                <div class="form-group">
                        <label class="col-sm-2 form-label">Image</label>
                        <div class="col-sm-10">
                            <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput" style="margin-bottom:0;">
                                <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                                <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="userfile"></span>
                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                            </div>
                            <label id="logo_upload_error" for="question" class="error" style="height: 23px;padding: 0 0 0 1px;white-space: normal;">&nbsp;</label>
                        </div>
                </div>
                <div class="text-right form-group">
                    <input class="btn btn-eq-common" type="submit" value="Submit" />
                </div>
            </form>
        </div>
    </div>
  </div>
</div>

<!-- POPUP Financial Forecast Change CONTAINER -->
<div class="modal fade" id="financial_forecastimgModal" tabindex="-1" role="dialog" aria-labelledby="financial_forecastimgModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="financial_forecastimgModalLabel">Change Financial Forecast</h4>
        </div>
        <div class="modal-body">
            <form id="upload-financial-forecast-image" action="<?php echo base_url(); ?>user/ajax_upload_image" method="post" enctype="multipart/form-data">
                <div class="form-group">
                        <label class="col-sm-2 form-label">Image</label>
                        <div class="col-sm-10">
                            <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput" style="margin-bottom:0;">
                                <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                                <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="userfile"></span>
                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                            </div>
                            <label id="logo_upload_error" for="question" class="error" style="height: 23px;padding: 0 0 0 1px;white-space: normal;">&nbsp;</label>
                        </div>
                </div>
                <div class="text-right form-group">
                    <input class="btn btn-eq-common" type="submit" value="Submit" />
                </div>
            </form>
        </div>
    </div>
  </div>
</div>

<!-- POPUP Financial Statement Change CONTAINER -->
<div class="modal fade" id="financial_statementimgModal" tabindex="-1" role="dialog" aria-labelledby="financial_statementimgModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="financial_statementimgModalLabel">Change Financial Statement</h4>
        </div>
        <div class="modal-body">
            <form id="upload-financial-statement-image" action="<?php echo base_url(); ?>user/ajax_upload_image" method="post" enctype="multipart/form-data">
                <div class="form-group">
                        <label class="col-sm-2 form-label">Image</label>
                        <div class="col-sm-10">
                            <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput" style="margin-bottom:0;">
                                <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                                <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="userfile"></span>
                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                            </div>
                            <label id="logo_upload_error" for="question" class="error" style="height: 23px;padding: 0 0 0 1px;white-space: normal;">&nbsp;</label>
                        </div>
                </div>
                <div class="text-right form-group">
                    <input class="btn btn-eq-common" type="submit" value="Submit" />
                </div>
            </form>
        </div>
    </div>
  </div>
</div>

<!-- POPUP Other Change CONTAINER -->
<div class="modal fade" id="otherimgModal" tabindex="-1" role="dialog" aria-labelledby="otherimgModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="otherimgModalLabel">Change Financial Statement</h4>
        </div>
        <div class="modal-body">
            <form id="upload-other-image" action="<?php echo base_url(); ?>user/ajax_upload_image" method="post" enctype="multipart/form-data">
                <div class="form-group">
                        <label class="col-sm-2 form-label">Image</label>
                        <div class="col-sm-10">
                            <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput" style="margin-bottom:0;">
                                <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                                <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="userfile"></span>
                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                            </div>
                            <label id="logo_upload_error" for="question" class="error" style="height: 23px;padding: 0 0 0 1px;white-space: normal;">&nbsp;</label>
                        </div>
                </div>
                <div class="text-right form-group">
                    <input class="btn btn-eq-common" type="submit" value="Submit" />
                </div>
            </form>
        </div>
    </div>
  </div>
</div>

<script>
    $(document).ready(function(){

    $('.glyphicon-cog').on('click',function(){
        $(this).next('.privacy-options').toggle();
    });

 $('#next_submit').on('click',function() {  
     $('#investee-info-form').submit();
});
        $('textarea').htmlarea({
            toolbar: [
                ["bold", "italic", "underline"],
                ["p"],
                ["link", "unlink"],
                ["orderedList", "unorderedList"],
                ["indent", "outdent"],
                ["justifyleft", "justifycenter", "justifyright"]
            ]
        });
        $('#validity-period').datetimepicker({
            timepicker:false,
            format:'d-m-Y'
        });

        $('#investee-info-form').on('change', '.country', function(){
            $.ajax({
                url: "<?php echo base_url(); ?>user/states",
                type: 'POST',
                data : {countryId : $(this).val()}
            })
            .done(function( data ) {
                $('#investee-info-form .state').html(data);
            });
        });
        
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");
        
        jQuery.validator.addMethod("rupeeonly", function(value, element) {
            return this.optional(element) || /^[-,0-9]+$/i.test(value);
        }, "Only digits and comma");
        
        $('#investee-info-form').validate({
            ignore: ":hidden:not(textarea)", 
            rules: {
                WysiHtmlField: "required", 
                company_name: { required : true,
                    minlength:4
                },

                city:{ required : true,
                    lettersonly:true
                },

                investment_required: { required : true,
                                       rupeeonly: true
                },
                commitment_per_investor: { required : true,
                                       rupeeonly: true
                },
                commitment_per_investor: { required : true,
                                           rupeeonly: true
                },
                addressable_market: {
                    rupeeonly: true
                },
                
                valuation: { required : true,
                    number: true
                },
                equity_offered: { 
                    number: true
                },

                'experience[]': {
                    required: true
                },

                company_url:'required',
                year:'required',
                country:'required',
                state:'required',

                product_service_desc:'required',
                products_services_business:'required',
                business_problem_being_solved:'required',
                uses_of_funds:'required',
                costs_margins:'required'
            },
            messages: {
                city: {
                    required: "This field is required."
                }
            } ,
            errorPlacement: function (error, element) {
                console.log(element); 
                if($(element).parent().hasClass("select-style")){
                    $(element).parents('.col-sm-7').append(error);
                }else if($(element).attr("type") == 'file'){
                    $(element).parents('.col-sm-7').append(error);
                }else{
                    error.insertAfter(element); 
                } 
            }
        });

        $('textarea').keydown(function(event){
            if (event.keyCode == 13) {
                event.preventDefault();
                this.value = this.value + "\n";
            }
        });
        
        $('.add-purpose-btn').on('click',function(){
            $('#fund-uses').append($('#fund-uses-fields').html());
        });
        
        $('.section-head').on('click',function(){
            $(this).parents('.row').next('.container').toggle();
            //$(this).parents('.row').next('.container').next('.row').toggle();
        });
        
        $('.privacy-options select').on('change',function(){
             $.ajax({
                url: "<?php echo base_url(); ?>investee/save_privacy",
                type: 'POST',
                data : {optionVal : $(this).val(),optionKey :$(this).attr('name') }
            })
            .done(function( data ) {
                
            });
        });

        $("#upload-profile-image").on('submit',(function(e){
            e.preventDefault();
            $.ajax({
                    url: "<?php echo base_url(); ?>user/ajax_image_upload",
                    type: "POST",
                    data:  new FormData(this),
                    dataType :'json',
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(data){
                        
                           if (data.error == ''){
                               $(".modal").modal("hide");  
                               $('#company-logo img').attr('src','<?php echo base_url(); ?>'+data.img);
                               $('#user-img img').attr('src','<?php echo base_url(); ?>'+data.img);
                                                       
                           } else {
                                $('#logo_upload_error').html(data.error);
                           }                        
                    },
                    error: function(){

                    } 
            });         
        }));

        $("#upload-banner-image").on('submit',(function(e){
            e.preventDefault();
            $.ajax({
                    url: "<?php echo base_url(); ?>user/ajax_image_upload/banner_image",
                    type: "POST",
                    data:  new FormData(this),
                    dataType :'json',
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(data){
                        
                           if (data.error == ''){
                               $(".modal").modal("hide");  
                               $('#banner-img img').attr('src','<?php echo base_url(); ?>'+data.img);
                                                       
                           } else {
                                $('#logo_upload_error').html(data.error);
                           }                        
                    },
                    error: function(){

                    } 
            });         
        }));

        $("#upload-financial-forecast-image").on('submit',(function(e){
            e.preventDefault();
            $.ajax({
                    url: "<?php echo base_url(); ?>user/ajax_image_upload/financial_forecast",
                    type: "POST",
                    data:  new FormData(this),
                    dataType :'json',
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(data){
                        
                           if (data.error == ''){
                               $(".modal").modal("hide");  
                               var file = data.img;
                               var extension = file.substr( (file.lastIndexOf('.') +1) );
                               console.log(extension)
                               switch(extension)
                                {
                                    case 'pdf':
                                        file = 'assets/images/pdf_icon.gif';
                                        break;
                                    case 'doc':
                                    case 'docx':
                                        file = 'assets/images/doc_icon.gif';
                                        break;
                                    case 'ppt':
                                    case 'pptx':
                                        file = 'assets/images/ppt_icon.gif';
                                        break;
                                    case 'xls':
                                    case 'xlsx':
                                        file = 'assets/images/xsl_icon.gif';
                                        break;
                                    default:
                                }
                               $('#financial_forecast-img img').attr('src','<?php echo base_url(); ?>'+file);
                                                       
                           } else {
                                $('#logo_upload_error').html(data.error);
                           }                        
                    },
                    error: function(){

                    } 
            });         
        }));

        $("#upload-financial-statement-image").on('submit',(function(e){
            e.preventDefault();
            $.ajax({
                    url: "<?php echo base_url(); ?>user/ajax_image_upload/financial_statement",
                    type: "POST",
                    data:  new FormData(this),
                    dataType :'json',
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(data){
                        
                           if (data.error == ''){
                               $(".modal").modal("hide");  
                               var file = data.img;
                               var extension = file.substr( (file.lastIndexOf('.') +1) );
                               console.log(extension)
                               switch(extension)
                                {
                                    case 'pdf':
                                        file = 'assets/images/pdf_icon.gif';
                                        break;
                                    case 'doc':
                                    case 'docx':
                                        file = 'assets/images/doc_icon.gif';
                                        break;
                                    case 'ppt':
                                    case 'pptx':
                                        file = 'assets/images/ppt_icon.gif';
                                        break;
                                    case 'xls':
                                    case 'xlsx':
                                        file = 'assets/images/xsl_icon.gif';
                                        break;
                                    default:
                                }
                               $('#financial_statement-img img').attr('src','<?php echo base_url(); ?>'+file);
                                                       
                           } else {
                                $('#logo_upload_error').html(data.error);
                           }                        
                    },
                    error: function(){

                    } 
            });         
        }));
        
        $("#upload-other-image").on('submit',(function(e){
            e.preventDefault();
            $.ajax({
                    url: "<?php echo base_url(); ?>user/ajax_image_upload/other",
                    type: "POST",
                    data:  new FormData(this),
                    dataType :'json',
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(data){
                        
                           if (data.error == ''){
                               $(".modal").modal("hide");  
                               var file = data.img;
                               var extension = file.substr( (file.lastIndexOf('.') +1) );
                               console.log(extension)
                               switch(extension)
                                {
                                    case 'pdf':
                                        file = 'assets/images/pdf_icon.gif';
                                        break;
                                    case 'doc':
                                    case 'docx':
                                        file = 'assets/images/doc_icon.gif';
                                        break;
                                    case 'ppt':
                                    case 'pptx':
                                        file = 'assets/images/ppt_icon.gif';
                                        break;
                                    case 'xls':
                                    case 'xlsx':
                                        file = 'assets/images/xsl_icon.gif';
                                        break;
                                    default:
                                }
                               $('#other-img img').attr('src','<?php echo base_url(); ?>'+file);
                                                       
                           } else {
                                $('#logo_upload_error').html(data.error);
                           }                        
                    },
                    error: function(){

                    } 
            });         
        }));

        $('#investeeSaveClose').on('click',function() {  
            // var submit = $("#investee-info-form input[type=submit]");
            // var btn_name = submit.attr('name');
            // $('element[name="'+btn_name+'"]').trigger('click');
            var action = $("#investee-info-form").attr('action');
            $("#investee-info-form").attr('action', action+'/save');
            $("#investee-info-form input[type=submit]").trigger('click');
        });
    });
</script>