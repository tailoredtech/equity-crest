<div class="container">
    <div class="row" style="margin-top: 30px;">
        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 180px; text-align:center">
        	<h1> Thank you !</h1>
        	<br>
        Thank you for submitting the details on your business. We will revert with an evaluation of your business within 2 working days on your email ID. This assessment will give you an idea of where you stand vis-à-vis other start-ups that have registered over the past 30 days. Separately, you will also receive an evaluation on the key parameters like Idea, Team, Traction and Market .
         <br>
        <br>
        Feel free to reach out to us on <a href="mailto:info@equitycrest.com" > info@equitycrest.com </a> for further questions.
        </div>

    </div>

</div>