
<div class="container">
  <div class="row">
    <div class="col-xs-8 col-sm-8 block-title">
      <h2 class="page-title">Channel Partner Profile Update</h2>
    </div>
  </div>

        <?php
            if($this->session->flashdata('success') != '')
            { ?>
                <div class="alert alert-success">
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
            <?php }
        ?>  
<div class="row content-box">
    <form id="investor-info-form1" action="<?php echo base_url(); ?>user/edit_channel_partner_info_process" enctype="multipart/form-data" method="post" >
        <div class="investor-register-form">
            <div class="row remove_margin">
                <div class="col-md-12 section-head">
                  <h3> <img src="<?php echo base_url(); ?>assets/images/top-menu-arrow.png" /><font color="#1281e7">Personal Information</font></h3>
                </div>
            </div>
            <div class="personal-info col-sm-12">
        
                    <div class="row form-group">
                        <label for="name" class="col-sm-3 form-label">Name</label>
                        <div class="col-sm-7">
                            <input type="text" name="name" class="form-control form-input" id="name" value="<?php echo $user['name']; ?>" placeholder="">
                        </div>
                    </div>
        
                    <div class="form-group row">
                        <label for="company-name" class="col-sm-3 form-label">Company Name</label>
                        <div class="col-sm-7">
                            <input type="text" name="company_name" class="form-control form-input" id="company-name" value="<?php echo $user['company_name']; ?>" placeholder="">
                        </div>
                    </div>
                
                    <div class="row form-group">
                        <label for="experience" class="col-sm-3 form-label">Experience Summary</label>
                        <div class="col-sm-7">
                            <textarea class="form-control form-input form-area" name="experience" id="experience" placeholder=""><?php echo $user['experience']; ?></textarea>
                        </div>
                    </div>

                    <div class="row form-group">
                        <label for="role" class="col-sm-3 form-label">Role</label>
                        <div class="col-sm-7">
                            <input type="text" name="role" value="<?php echo $user['role']; ?>" class="form-control form-input" id="role" placeholder="Ex. CEO,Founder">
                        </div>
                    </div>
            
                    <div class="row form-group">
                        <label for="fb-url" class="col-sm-3 form-label">Facebook</label>
                        <div class="col-sm-7">
                            <input type="text" name="fb_url" value="<?php echo $user['fb_url']; ?>" class="form-control form-input" id="fb-url" placeholder="Facebook URL">
                        </div>
                    </div>
              
                    <div class="row form-group">
                        <label for="linkedin-url" class="col-sm-3 form-label">Linkedin</label>
                        <div class="col-sm-7">
                            <input type="text" name="linkedin_url" value="<?php echo $user['linkedin_url']; ?>" class="form-control form-input" id="linkedin-url" placeholder="Linkedin URL">
                        </div>
                    </div>
                
                    <div class="row form-group">
                        <label for="twitter-handle" class="col-sm-3 form-label">Twitter</label>
                        <div class="col-sm-7">
                            <input type="text" name="twitter_handle" value="<?php echo $user['twitter_handle']; ?>" class="form-control form-input" id="twitter-handle" placeholder="Twitter handle">
                        </div>
                    </div>            
   
                    <div class="row form-group">
                        <label for="country" class="col-sm-3 form-label">Country</label>
                        <div class="col-sm-7">
                            <div class="select-style">
                                <select class="form-control form-input country" name="country"  placeholder="">
                                    <option value="">Select Country</option>
                                    <?php foreach ($countries as $country) { ?>
                                        <option value="<?php echo $country['id']; ?>" <?php echo ($country['id'] == $user['country'] ) ? 'selected' : '' ?> ><?php echo $country['name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <label for="state" class="col-sm-3 form-label">State</label>
                        <div class="col-sm-7">
                            <div class="select-style">
                                <select class="form-control form-input state" name="state"  placeholder="">
                                     <?php foreach ($states as $state) { ?>
                                            <option value="<?php echo $state['id']; ?>" <?php echo ($state['id'] == $user['state'] ) ? 'selected' : '' ?>><?php echo $state['name']; ?></option>
                                      <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>          

                    <div class="row form-group">
                        <label for="city" class="col-sm-3 form-label">City</label>
                        <div class="col-sm-7">
                            <input type="text" name="city" value="<?php echo $user['city']; ?>" class="form-control form-input" id="city" placeholder="">
                        </div>
                    </div>         

                    <div class="row form-group">
                        <label for="mentoring-sectors" class="col-sm-3 form-label">Sector of Expertise</label>
                        <div class="col-sm-7">
                            <div class="select-style">
                                <select class="form-control form-input" name="sector_expertise[]"  placeholder="" multiple>
                                    <?php
                                    $sector_autocomplete_string = '';
                                    $sector_expertise = explode(",", $user['sector_expertise']);
                                    foreach ($sectors as $sector) {
                                        $sector_autocomplete_string .= '"' . $sector['name'] . '",';
                                        ?>
                                        <option value="<?php echo $sector['name']; ?>" <?php echo (in_array($sector['name'], $sector_expertise)) ? 'selected' : ''; ?> ><?php echo $sector['name']; ?></option>
                                    <?php
                                    }
                                    $sector_autocomplete_string = trim($sector_autocomplete_string, ",");
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <label for="expertise" class="col-sm-3 form-label">Area of Expertise</label>
                        <div class="col-sm-7 key-points-list">
                            <input type="text" class="form-control form-input form-area" row="5" id="expertise" name="expertise" value="<?php echo $user['area_of_expertise'] ?>" placeholder="" />
                        </div>
                    </div>
     
                    <div class="row">
                        <div class="col-sm-10">
                            <input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
                            <input type="submit" name="submit" class="btn btn-eq-common pull-right" value="Save" />
                        </div>
                    </div>                
          </div>
             
    </form> 
</div>

</div> <!-- /.container -->

<?= js('jquery-ui.min.js') ?>
<?= js('tag-it.js') ?>
<script>
    $('document').ready(function(){
        
        // $('#investor-info-form1').on('change', '.country', function(){
        //     $.ajax({
        //         url: "<?php echo base_url(); ?>user/states",
        //         type: 'POST',
        //         data : {countryId : $(this).val()}
        //     })
        //     .done(function( data ) {
        //         $('#investor-info-form .state').html(data);
        //     });
        // });


     $('#investor-info-form1').on('change', '.country', function(){
            $.ajax({
                url: "<?php echo base_url(); ?>user/states",
                type: 'POST',
                data : {countryId : $(this).val()}
            })
            .done(function( data ) {
                $('#investor-info-form1 .state').html(data);
            });
        });
      
        $('#expertise').tagit({
            allowSpaces : true
        });
  
    });


     $.validator.addMethod('urlCheck', function (value, element) {
                return this.optional(element) ||  /^(http(s)?:\/\/)?([\w]+\.)?linkedin\.com\//.test(value);
            }, "Please enter a valid LinkedIn Url");


    $.validator.addMethod('urlCheck1', function (value, element) {
                return this.optional(element) || /http:\/\/twitter\.com\/(#!\/)?[a-zA-Z0-9_]+/.test(value);
            }, "Please enter a valid Twitter Url");
         

        $('#investor-info-form1').validate({
            rules: {
                name: { required : true,
                    minlength:4
                },
                experience: { required: false},    
               
                country:{ required: false},
                state:{ required: false},
                city:{ required: false},
                
                company_name:{ required: true},
                
                linkedin_url: { required: false,
                                urlCheck: true
                            },
                twitter_handle: { required: false,
                                urlCheck1: true
                            },
               
              

            } ,
            messages: {
                name: {
                    required: "Please provide name",
                    minlength: "Name must be at least 4 characters long"
                },
                experience: {
                    required: "Please enter your experience summary"
                },
               
                country: {required: 'Please select country' },
                state: {required: 'Please select state'},
                city: {required: 'Please enter city'},
               
                company_name: {required: 'Please enter the current organization'},
               
              
            },
            errorPlacement: function (error, element) {
                if($(element).parent().hasClass("select-style")){
                    $(element).parents('.select-wrap').append(error);
                }else if($(element).attr("type") == 'file'){
                    $(element).parents('.select-wrap').append(error);
                }else{
                    error.insertAfter(element); 
                } 
            }
        });
</script>