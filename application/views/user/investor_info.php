<div class="container">
    <div class="row">
        <div class="col-xs-12 block-title">
          <h1 class="page-title">Investor Profile Update</h1>
        </div>
    </div>
</div>
<div class="container">
    <form id="investor-info-form" action="<?php echo base_url(); ?>user/investor_info_process" enctype="multipart/form-data" method="post" >
        <div>
        <!--Slider Accordion Starts here-->
            <!-- Start Personal Information panel -->
            <div class="accordion2">
                <div class="accordion-heading  accordion-opened" >
                    <a class="accordion-toggle <?php echo ($activePanel == 'personal-info' || $activePanel == '') ? '' : 'collapsed' ?>" data-toggle="collapse" href="#personal-info"><h4>Personal Information</h4></a>
                </div>
                <div id="personal-info" class="accordion-body collapse <?php echo ($activePanel == 'personal-info' || $activePanel == '') ? 'in' : '' ?>">
                    <div class="accordion-inner2">
                        <div class="form-group row">
                            <label for="sign-in-as" class="col-sm-5 form-label">Type of Investor</label>
                            <div class="col-sm-7">
                                <select name="type" id="type" class="form-control form-input">
                                <option value="individual" <?php echo ($this->session->userdata('type') == 'Angel') ? 'selected' : ''; ?>>Individual</option>
                                <option value="Angel" <?php echo ($this->session->userdata('type') == 'Angel') ? 'selected' : ''; ?>>Angel</option>
                                <option value="Venture Fund" <?php echo ($this->session->userdata('type') == 'Venture Fund') ? 'selected' : ''; ?>>Venture Fund</option>
                                <option value="Angel Fund" <?php echo ($this->session->userdata('type') == 'Angel Fund') ? 'selected' : ''; ?>>Angel Fund</option>
                                <option value="Accelarator" <?php echo ($this->session->userdata('type') == 'Accelarator') ? 'selected' : ''; ?>>Accelarator/ Incubators</option>
                                </select>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-sm-5 form-label">Name</label>
                            <div class="col-sm-7">
                                <input type="text" name="name" class="form-control form-input" id="name" value="<?php echo $this->session->userdata('name'); ?>" placeholder="" />
                            </div>
                        </div>
                        <div class="form-group row company-name-input" style="display:none;" >
                            <label for="company_name" class="col-sm-5 form-label">Company Name</label>
                            <div class="col-sm-7">
                                <input type="text" name="company_name" class="form-control form-input" id="company_name" value="" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="experience" class="col-sm-5 form-label">Experience Summary</label>
                            <div class="col-sm-7">
                                <textarea class="form-control form-input form-area" style="height:200px;" name="experience"  id="experience" placeholder=""></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="image" class="col-sm-5 form-label">Your Photo</label>
                                <div class="col-sm-7">
                                    <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput">
                                        <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                                        <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file"  id="image" name="image"></span>
                                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                    </div>
                                </div>  
                        </div>
                        <div class="form-group row">
                            <label for="role" class="col-sm-5 form-label">Role</label>
                            <div class="col-sm-7">
                                <input type="text" name="role" value="" class="form-control form-input" id="role" placeholder="Ex. CEO,Founder">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fb_url" class="col-sm-5 form-label">Facebook</label>
                            <div class="col-sm-7">
                                <input type="text" name="fb_url" value="" class="form-control form-input" id="fb_url" placeholder="Facebook URL">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="linkedin_url" class="col-sm-5 form-label">Linkedin</label>
                            <div class="col-sm-7">
                                <input type="text" name="linkedin_url" value="" class="form-control form-input" id="linkedin_url" placeholder="Linkedin URL">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="twitter_handle" class="col-sm-5 form-label">Twitter</label>
                            <div class="col-sm-7">
                                <input type="text" name="twitter_handle" value="" class="form-control form-input" id="twitter_handle" placeholder="Twitter handle">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="country" class="col-sm-5 form-label">Country</label>
                            <div class="col-sm-7 select-wrap">
                                <div class="select-style">
                                <select class="form-control form-input country" name="country" id="country"  placeholder="">
                                    <option value="">Select Country</option>
                                    <?php foreach ($countries as $country) { ?>
                                        <option value="<?php echo $country['id']; ?>"><?php echo $country['name']; ?></option>
                                    <?php } ?>
                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="state" class="col-sm-5 form-label">State</label>
                            <div class="col-sm-7 select-wrap">
                                <div class="select-style">
                                    <select class="form-control form-input state" name="state" id="state"  placeholder="Select State">
                                        <?php foreach ($states as $state) { ?>
                                            <option value="<?php echo $state['id']; ?>"><?php echo $state['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="city" class="col-sm-5 form-label">City</label>
                            <div class="col-sm-7">
                                <input type="text" name="city" value="" class="form-control form-input" id="city" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <input  type="submit" id="personal_info_submit" name="personal_info_submit"  class="btn btn-eq-common pull-right" value="Save" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Personal Information panel -->    

            <!-- Start Investment Philosophy panel -->
            <div class="accordion2">
                <div class="accordion-heading  accordion-opened" >
                    <a class="accordion-toggle <?php echo ($activePanel == 'investment') ? '' : 'collapsed' ?>" data-toggle="collapse" href="#investment"><h4>Investment Philosophy</h4></a>
                </div>
                <div id="investment" class="accordion-body collapse <?php echo ($activePanel == 'investment') ? 'in' : '' ?>">
                    <div class="accordion-inner2">
                        <div class="form-group row">
                            <label for="team-summary" class="col-sm-5 form-label">Sector Expertise</label>
                            <div class="col-sm-7">
                                <div class="select-style">
                                    <select class="form-control form-input" id="sector_expertise[]" name="sector_expertise[]"  placeholder="" multiple>
                                        <?php
                                        $sector_autocomplete_string = '';
                                        foreach ($sectors as $sector) {
                                            $sector_autocomplete_string .= '"' . $sector['name'] . '",';
                                            ?>
                                            <option value="<?php echo $sector['name']; ?>"><?php echo $sector['name']; ?></option>
                                        <?php
                                        }
                                        $sector_autocomplete_string = trim($sector_autocomplete_string, ",");
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="key_points" class="col-sm-5 form-label">Key Points in Company I look</label>

                            <div class="col-sm-7">
                                <input type="text" class="form-control form-input"  id="key_points" name="key_points" value="" placeholder="Ex. Team, Market, Traction, Production, Business Model, Others " />
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <input  type="submit" id="investment_submit" name="investment_submit"  class="btn btn-eq-common pull-right" value="Save" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Investment Philosophy panel -->

            <!-- Start Portfolio Early Stage panel -->
            <div class="accordion2">
                <div class="accordion-heading  accordion-opened" >
                    <a class="accordion-toggle <?php echo ($activePanel == 'startup-portfolio') ? '' : 'collapsed' ?>" data-toggle="collapse" href="#startup-portfolio"><h4>Portfolio of Early Stage Companies</h4></a>
                </div>
                <div id="startup-portfolio" class="accordion-body collapse <?php echo ($activePanel == 'startup-portfolio') ? 'in' : '' ?>">
                    <div class="accordion-inner2">
                        <div class="form-group row">
                            <label for="startup_portfolio" class="col-sm-5 form-label">Startup Portfolio</label>
                            <div class="col-sm-7">
                                <div class="select-style">
                                    <select class="form-control form-input" id="startup_portfolio" name="startup_portfolio"  placeholder="">
                                        <option value="" selected disabled>Select Startup Potfolio...</option>
                                        <!-- option value="0">Add New...</option -->
                                    </select>
                                    <span id="startup-portfolio-add" class="btn col-sm-2 input-placeholder pull-right">Add</span>
                                    <span id="startup-portfolio-remove" class="btn btn-eq-common input-placeholder pull-right">Remove</span>
                                </div>
                            </div>
                        </div>
                        <div id="portfolio_container">
                            <?php $count=0; ?>
                        </div>
                        <div class="row portfolio-startup-save pull-right" style="display: none;">
                            <div class="col-sm-12">
                                <input type="button" id="startup_portfolio_add_more" name="startup_portfolio_add_more"  class="btn btn-eq-common" value="Add More" />
                                <input type = "submit" id="startup_portfolio_submit" name="startup_portfolio_submit"  class="btn btn-eq-common" value="Save" style="margin-left:10px;" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Portfolio Early Stage panel -->

            <!-- Start Interest in Mentoring panel -->
            <div class="accordion2">
                <div class="accordion-heading  accordion-opened" >
                    <a class="accordion-toggle <?php echo ($activePanel == 'interest') ? '' : 'collapsed' ?>" data-toggle="collapse" href="#interest"><h4>Interest in Mentoring</h4></a>
                </div>
                <div id="interest" class="accordion-body collapse <?php echo ($activePanel == 'interest') ? 'in' : '' ?>">
                    <div class="accordion-inner2">
                        <div class="form-group row">
                            <label for="mentoring_sectors" class="col-sm-5 form-label">Sector</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control form-input form-area"  row="5" id="mentoring_sectors"  name="mentoring_sectors" value="" placeholder=""/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="expertise" class="col-sm-5 form-label">Area of Expertise</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control form-input form-area" row="5" id="expertise" name="expertise" value="" placeholder="" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="duration" class="col-sm-5 form-label">Duration</label>
                            <div class="col-sm-7" >
                                <input type="text" name="duration"  class="form-control form-input" id="duration" value="" placeholder="">
                                <span id="duration_span" class="col-sm-2 input-placeholder pull-right"  >hrs/week</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <input type="submit" class="btn btn-eq-common pull-right" id="interest_submit" name="interest_submit" value="Save" />
                            </div>
                        </div>
                        <input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
                        <input type="hidden" id="portfolio_count" name="portfolio_count" value="<?php echo $count; ?>" />
                    </div>
                </div>
            </div> 
            <!-- End Interest in Mentoring panel -->
        </div>
    </form>
</div>


<div style="display: none;">
    <div id="portfolio-fields">
        <div class="portfolio-box" id="id-0" portfolio-id="0" >
            <div class="form-group row">
                <label class="col-sm-5 form-label">Name of Company</label>
                <div class="col-sm-7">
                    <input type="text" id="portfolio_name" name="portfolio_name" class="form-control form-input" value="" placeholder="">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-5 form-label">Website URL</label>
                <div class="col-sm-7">
                    <input type="text" id="portfolio_url" name="portfolio_url" value="" class="form-control form-input" placeholder="">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-5 form-label">Location</label>
                <div class="col-sm-7">
                    <input type="text" id="portfolio_location" name="portfolio_location" value="" class="form-control form-input" placeholder="">
                </div>
            </div>
            <div class="form-group row">
                <label  class="col-sm-5 form-label">Sector</label>
                <div class="col-sm-7">
                    <div class="select-style">
                        <select class="form-control form-input" id="portfolio_sector" name="portfolio_sector"  placeholder="">
                            <option value="">Sector</option>
                            <?php foreach ($sectors as $sector) { ?>
                                <option value="<?php echo $sector['id']; ?>" ?><?php echo $sector['name']; ?></option>
                           <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-5 form-label">Role</label>
                <div class="col-sm-7">
                    <input type="text" id="portfolio_role" name="portfolio_role" value="" class="form-control form-input" placeholder="">
                    
                </div>
            </div>
            <div class="form-group row">
                <label  class="col-sm-5 form-label">Your Logo</label>
                <div class="col-sm-7">
                    <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput">
                        <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                        <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file"  id="portfolio_image" name="portfolio_image"></span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label  class="col-sm-5 form-label">Remarks</label>
                <div class="col-sm-7">
                    <textarea class="form-control form-input form-area"  id="portfolio_remarks" name="portfolio_remarks" placeholder=""></textarea>
                    <input type="hidden" id="id_" name="id_" value="" />
                    <input type="hidden" id="action_" name="action_" value="new" />
                </div>
            </div>
        </div>
    </div>

    <div id="ch-img" class="container pop-content">
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <h1 class="form-header page-title">Upload Image</h1>
            </div>
        </div>
        <div class="row">
            <div class="top-margin-10">
            <form id="upload-image" action="<?php echo base_url(); ?>user/ajax_upload_image" method="post" enctype="multipart/form-data">
                <div class="col-xs-12 col-sm-12">
                    <div class="form-group row">
                        <label class="col-sm-2 form-label">Image</label>
                        <div class="col-sm-10">
                            <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput" style="margin-bottom:0;">
                                <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                                <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" id="userfile" name="userfile"></span>
                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                            </div>
                            <label id="upload_error" for="question" class="error">&nbsp;</label>
                        </div>
                    </div>
                    <input type="hidden" class="portfolio-id" name="portfolio_id" value="" />
                    <input class="btn btn-eq-common pull-right" type="submit" value="Submit" />
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

<?= js('jquery-ui.min.js') ?>
<?= js('tag-it.js') ?>
<script>
$.noConflict();
</script>

<script>
    $('document').ready(function(){
        var portfolio_count = 0;
        
        $('#investor-info-form').on('change', '.country', function(){
            $.ajax({
                url: "<?php echo base_url(); ?>user/states",
                type: 'POST',
                data : {countryId : $(this).val()}
            })
            .done(function( data ) {
                $('#investor-info-form .state').html(data);
            });
        });
        
       $('textarea').htmlarea({
            toolbar: [
                        ["bold", "italic", "underline"],
                        ["p"],
                        ["link", "unlink"],
                        ["orderedList","unorderedList"],
                        ["indent","outdent"],
                        ["justifyleft", "justifycenter", "justifyright"] 
                    ]
        });
        
        $('#investor-info-form').on('change', '#type', function(){
            var type = $(this).val();
            if(type == 'angel'){
                $('.company-name-input').html($('#company-name-field').html());
                $('.company-name-input').show();
            }else{
                $('.company-name-input').html('');
                $('.company-name-input').hide();
            }
        });
      
        $('#investor-info-form').validate({
            rules: {
                name: { required : true,
                    minlength:4
                },
                experience: { required: false},    
                website:{ required: false},
                country:{ required: false},
                state:{ required: false},
                city:{ required: false},
                year:{ required: false},
                company_name:{ required: false},
                about_company:{ required: false},
                'optional_name[]':{ required: false}
            } ,
            messages: {
                name: {
                    required: "Please provide name",
                    minlength: "Name must be at least 4 characters long"
                },
                experience: {
                    required: "Please enter your experience summary"
                },
                website: {
                    required: "Please provide your website"
                },
                country: {required: 'Please select country' },
                state: {required: 'Please select state'},
                city: {required: 'Please enter city'},
                year:{required: 'Please enter year of establishment'},
                company_name: {required: 'Please enter name of the compnay'},
                about_company: {required: 'Please enter about your company'}
            },
            errorPlacement: function (error, element) {
                if($(element).parent().hasClass("select-style")){
                    $(element).parents('.select-wrap').append(error);
                }else if($(element).attr("type") == 'file'){
                    $(element).parents('.select-wrap').append(error);
                }else{
                    error.insertAfter(element); 
                } 
            }
        });
        
        
        $('#investor-info-form').on('change', '#startup_portfolio', function(){
            
            var portfolioId = $(this).val();
            $('.portfolio-box').hide();
            $('#portfolio_' + portfolioId ).show();
            $('.portfolio-startup-save').show();

            if(portfolioId == '0' || portfolioId == ''){
                $('#startup-portfolio-remove').hide();
                //clear the new form
            } else {
                $('#startup-portfolio-remove').show();
            }
            var portfolio_count = $('#portfolio_count').val();

            $('#id-' + portfolio_count).hide(); 
        });

        $('#startup-portfolio-add, #startup_portfolio_add_more').on('click',function(){
            portfolio_count = $('#portfolio_count').val();
            ++portfolio_count;
            //console.log(portfolio_count);
            $('#portfolio_count').val(portfolio_count);
            $('#portfolio-fields').find('input[name*="portfolio_name"]').attr('name','portfolio_name_'+portfolio_count);
            $('#portfolio-fields').find('input[name*="portfolio_url"]').attr('name','portfolio_url_'+portfolio_count);
            $('#portfolio-fields').find('input[name*="portfolio_location"]').attr('name','portfolio_location_'+portfolio_count);
            $('#portfolio-fields').find('select[name*="portfolio_sector"]').attr('name','portfolio_sector_'+portfolio_count);
            $('#portfolio-fields').find('input[name*="portfolio_role"]').attr('name','portfolio_role_'+portfolio_count);
            $('#portfolio-fields').find('input[name*="portfolio_image"]').attr('name','portfolio_image_'+portfolio_count);
            $('#portfolio-fields').find('textarea[name*="portfolio_remarks"]').attr('name','portfolio_remarks_'+portfolio_count);
            

            $('#portfolio-fields').find('input[id*="portfolio_name"]').attr('id','portfolio_name_'+portfolio_count);
            $('#portfolio-fields').find('input[id*="portfolio_url"]').attr('id','portfolio_url_'+portfolio_count);
            $('#portfolio-fields').find('input[id*="portfolio_location"]').attr('id','portfolio_location_'+portfolio_count);
            $('#portfolio-fields').find('select[id*="portfolio_sector"]').attr('id','portfolio_sector_'+portfolio_count);
            $('#portfolio-fields').find('input[id*="portfolio_role"]').attr('id','portfolio_role_'+portfolio_count);
            $('#portfolio-fields').find('input[id*="portfolio_image"]').attr('id','portfolio_image_'+portfolio_count);
            $('#portfolio-fields').find('textarea[id*="portfolio_remarks"]').attr('id','portfolio_remarks_'+portfolio_count);

            $('#portfolio-fields').find('input[name*="id_"]').attr('name','id_'+portfolio_count);
            $('#portfolio-fields').find('input[name*="action_"]').attr('name','action_'+portfolio_count);
            $('#portfolio-fields').find('input[id*="id_"]').attr('id','id_'+portfolio_count);
            $('#portfolio-fields').find('input[id*="action_"]').attr('id','action_'+portfolio_count);
            

            $('#portfolio-fields .portfolio-box').attr('portfolio-id',(-1 * portfolio_count));
            $('#portfolio-fields .portfolio-box').attr('id',"portfolio-" + portfolio_count);

            $('#portfolio_container').append($('#portfolio-fields').html());

            $('.portfolio-box').hide();
            $('#portfolio-' + portfolio_count ).show();
            $('.portfolio-startup-save').show();            
            $('#startup-portfolio-add').hide();

            $('#startup_portfolio option[value=""]').prop('selected', true);
            $('#startup-portfolio-remove').hide();

        });
       
        $('.portfolio-box').on('click','.change-img',function(){
            var portfolio_id = $(this).parents('.portfolio-box').attr('portfolio-id');
            //portfolio_id = portfolio_id.slice(-1);
            $('#ch-img .portfolio-id').val(portfolio_id);
            $.colorbox({inline: true, href: '#ch-img', innerWidth: '80%', maxWidth: '475px'});//, innerHeight: '175px'
            //$.colorbox({inline: true, href: '#ch-img', innerWidth: '400px', innerHeight: '175px'});
        });
        
       $("#upload-image").on('submit',(function(e){
            e.preventDefault();
            $.ajax({
                    url: "<?php echo base_url(); ?>investor/ajax_portfolio_image_upload",
                    type: "POST",
                    data:  new FormData(this),
                    dataType :'json',
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(data){
                           $('#portfolio_image_'+data.id).attr('src','<?php echo base_url(); ?>'+data.img);
                           $.colorbox.close()
                    },
                    error: function(){

                    } 
            });         
        }));

        $('#key_points').tagit({
            availableTags: ['Team','Market','Traction','Production','Business Model','Others'],
            minLength: 2
        });

        $('#mentoring_sectors').tagit({
            availableTags: [<?php echo $sector_autocomplete_string; ?>],
            minLength: 2
        });
		
        $('#expertise').tagit({
            allowSpaces : true
        });
        
			
        $('#investment-next').on('click',function(){
            if($('#investor-info-form').valid()){
                $(this).hide();
                $('.investor-register-form .container').hide();
                $('.investor-register-form .save-btn').parents('.row').hide();
                $('.container.investment').show();
            }
            
        });
        
        $('#portfolio-next').on('click',function(){
            $(this).hide();
            $('.investor-register-form .container').hide();
            $('.investor-register-form .save-btn').parents('.row').hide();
            $('.container.startup-portfolio').show();
            
        });
        
        $('#interest-next').on('click',function(){
            $(this).hide();
            $('.investor-register-form .container').hide();
            $('.investor-register-form .save-btn').parents('.row').hide();
            $('.container.interest').show();
        });
        
        $('#startup-portfolio-remove').on('click',function(){
            var portfolioId = $('#startup_portfolio').val();
            var id = "#portfolio_" + portfolioId;
            $.ajax({
                    url: "<?php echo base_url(); ?>investor/delete_portfolio",
                    type: "POST",
                    data:  {company_id:portfolioId},
                    dataType :'html',
                    success: function(data){
                       $('#startup_portfolio option[value=""]').prop('selected', true);
                       $('#startup-portfolio-remove').hide();
                       $('#startup-portfolio-add').show();
                       $('.portfolio-startup-save').hide();
                       $("#startup_portfolio option[value='" + portfolioId + "']").remove();
                       $('#portfolio_' + portfolioId ).hide();
                    },
                    error: function(){

                    } 
            });
        });
        
        $('#add-portfolios').on('click','.cancle-portfolio-btn',function(){
              $(this).parents('.new-portfolio-box').remove();
              portfolio_count--;
              $('#portfolio_count').val(portfolio_count);
        });
        
       
         $('.section-head').on('click',function(){
            $(this).parents('.row').next('.container').toggle();
            //$(this).parents('.row').next('.container').next('.row').toggle();
        });
    });
</script>