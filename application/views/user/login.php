<?php parse_str($_SERVER['QUERY_STRING'],$_GET);
    $user = '';
    if(isset($_GET['user']) && $_GET['user'] != ''){
       $user = $_GET['user'];
    }
?>

<div class="login-container">
    <div class="top-padding-30 login-box">
        <?php if($this->session->flashdata('success-msg')){ ?>
        <div class="row remove_margin failure-msg">
            <div class="col-sm-12">
                <p class="alert alert-success login-error-alert text-center" role="alert">
                    <?php echo $this->session->flashdata('success-msg'); ?>
                </p>
            </div>
        </div>
        <?php } ?>
        <?php if($this->session->flashdata('error-msg')){ ?>
        <div class="row  remove_margin failure-msg">
            <div class="col-sm-12">
                <p class="alert alert-danger login-error-alert text-center" role="alert">
                    <?php echo $this->session->flashdata('error-msg'); ?>
                </p>
            </div>
        </div>
        <?php } ?>

        <form method="post" role="form" name="login-form" id="login-form">

            <div class="row form-group remove_margin">                
                    <label for="email" class="col-xs-12">Email address</label>
                    <div class="col-xs-12">
                        <input type="text" class="form-control form-input input-focus" id="email" name="email" placeholder="">
                    </div>                
            </div>

            <div class="row form-group remove_margin">                
                    <label for="password" class="col-xs-12">Password</label>
                    <div class="col-xs-12">
                        <input type="password" class="form-control form-input input-focus" id="password" name="password" placeholder="">
                    </div>               
            </div>

            <div class="row remove_margin">
                <div class="col-xs-12">
                    <div class="checkbox login-checkbox">
                        <label><input type="checkbox" name="remember"> Remember me</label>                        
                    </div>  
                </div>
                <div class="join-now col-xs-12">
                    <input type="button" value="Login Now" name="submit" id="login_submit" class="btn btn-eq-common col-sm-12">        
                </div>
            </div>

            <div class="row remove_margin login_option">
                <div class="col-xs-12">
                    <a href="javascript:void(0);" class="forgot-password" data-toggle="modal" data-target="#forgotPasswordModal">Forgot your password?</a>                
                    <a href="javascript:void(0);" id="join" data-toggle="modal" data-target="#registerModal" >Don't have an account?</a>
                </div>
            </div>

            <div class="row remove_margin"> 		
                    <div class="col-xs-12">
                        <p>Or login with</p>
                        <ul class="login-social-icon">
                            <li><a href="<?php echo base_url(); ?>user/linkedin_login" id="linkedin">Linked-in</a></li>
                            <li><a href="<?php echo base_url(); ?>user/googleplus_login" id="google">Google</a></li>
                            <li><a href="<?php echo base_url(); ?>user/fb_login" id="facebook">Facebook</a></li>
                        </ul>
                    </div>
                               
            </div>    

        </form>

    </div> <!-- /.login-box -->    
   
</div>       

<script>
$(document).delegate('#login_submit', 'click', function() {
    $.ajax({
        url: '<?php echo base_url(); ?>user/login_process',
        type: 'post',
        data: $('#login-form').serialize(),
        dataType: 'json',
        beforeSend: function() {

            $('#login_submit').button('loading');
        },  
        complete: function() {
             // console.log('xfdgsdfgsfdgfgfgghxfgh');
            $('#login_submit').button('reset');
        },              
        success: function(json) {
            $('.failure-msg').remove();
            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {
                $('#login-form').prepend('<div class="row remove_margin failure-msg"><div class="col-sm-12"><p class="alert alert-danger login-error-alert text-center" role="alert">'+json['error']['error-msg']+'</p></div></div>');
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    }); 
});
</script>