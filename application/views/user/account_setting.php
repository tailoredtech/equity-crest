<div class="container">
    <div class="row">
        <div class="col-sm-3 block-title" >
        <h2>Change Password</h2>
        </div>
    </div>
</div>
<div class="container">
    <div class="row login-box account-setting bottom-padding-30">
        <div class="col-sm-12">
            <div class="top-padding-30">
                <?php if($this->session->flashdata('success-msg')){ ?>
                <div class="row failure-msg">
                    <div class="col-sm-10 col-md-offset-1">
                        <div class="alert alert-success" role="alert">
                            <?php echo $this->session->flashdata('success-msg'); ?>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if($this->session->flashdata('error-msg')){ ?>
                <div class="row failure-msg">
                    <div class="col-sm-8 col-md-offset-4">
                        <div class="alert alert-danger" role="alert">
                            <?php echo $this->session->flashdata('error-msg'); ?>
                        </div>
                    </div>
                </div>
                <?php } ?>
              <form id="account-setting-form" action="<?php echo base_url(); ?>user/account_setting_process"  method="post">
                <div class=" col-sm-8 col-sm-offset-1 ">
                    <div class="row">
                        <div class="form-group">
                            <label for="email" class="col-sm-4">Email ID</label>
                            <div class="col-sm-8">
                                <input type="text" name="name" class="form-control " id="name" value="<?php echo $this->session->userdata('email'); ?>" placeholder="" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="password" class="col-sm-4">Change Password</label>
                            <div class="col-sm-8">
                                <input type="password" class="form-control form-input" id="password" name="password" placeholder="">
                                <label class="error">Password should be of minimum 6 characters</label>    
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="change_password" class="col-sm-4">Confirm Password</label>
                            <div class="col-sm-8">
                                <input type="password" name="confirm_password" class="form-control form-input" id="confirm-password" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <input type="submit" value="Submit" name="submit" class="eq-btn col-sm-12 pull-right">
                        </div>
                    </div>
                </div>
              </form>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
        $('document').ready(function(){
                        $('#account-setting-form').validate({
                rules: {
                            password: { required : true,
                                    minlength:6
                            },
                            confirm_password:{ required : true,
                                               minlength:6,
                                               equalTo: "#password"
                            },
                        },
		messages: {
                		password: {
					required: "Please provide a password",
					minlength: "Your password must be at least 6 characters long"
				},
				confirm_password: {
					required: "Please provide a password",
					minlength: "Your password must be at least 6 characters long",
					equalTo: "Please enter the same password as above"
				},
			},
                
            });

        });
</script>