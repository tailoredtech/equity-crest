<?php parse_str($_SERVER['QUERY_STRING'],$_GET);
    $user = '';
    if(isset($_GET['user']) && $_GET['user'] != ''){
       $user = $_GET['user'];
    }
?>

<!-- POPUP REGISTER -->
<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">        
        <h4 class="modal-title" id="registerModalLabel">Register an account</h4>
      </div>
      <div class="modal-body">

        <div class="register-box"> 
                <div class="row remove_margin failure-msg" style="display:none">

                </div>
               
                <form method="post" id="register-form" >

                    <div class="register-box-innner register-box-1">

                        <div class="row">
                            <div  class="form-group">
                                <label for="sign-in-as" class="col-sm-12 join-label">Join Equity Crest as an</label>
                                <div id="radio-inline" class="col-sm-12">
                                    <span class="radio-inline radio">
                                        <input id="radio1" type="radio" name="role"  value="investor">
                                        <label for="radio1">Investor</label>
                                    </span>
                                    <span class="radio-inline radio">
                                            <input id="radio2" type="radio" name="role"  value="investee">
                                            <label for="radio2">Start-Up</label>
                                    </span>
                                    <span class="radio-inline radio">
                                            <input id="radio3" type="radio" name="role"  value="channel_partner">
                                            <label for="radio3">Partner</label>
                                    </span>
                                    <span class="radio-inline radio">
                                            <input id="radio4" type="radio" name="role"  value="media">
                                            <label for="radio4">Media</label>
                                    </span>
                                    <div class="clear-fix"></div>
                                </div>
                            </div>
                        </div>

                    </div> <!-- /.register-box-1 -->


                    <div class="register-box-innner register-box-2">

                        <div class="row">
                            
                            <div id="name-fields">
                               <div class="col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="name" class="register-field-label">Full Name *</label>                                        
                                        <input type="text" name="name" class="form-control form-input" id="name" placeholder="" >
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="inputEmail3" class="register-field-label">Mobile No *</label>                                    
                                    <input type="text" name="mobileno" class="form-control form-input customphone " id="mobile" placeholder="" >
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="inputEmail3" class="register-field-label">Email Id *</label>
                                    <input type="text" name="email" class="form-control form-input" id="email" placeholder="" >                                    
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="inputEmail3" class="register-field-label">Password *</label>
                                    <input type="password" name="user_password" class="form-control form-input" id="user_password" placeholder="">
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="inputEmail3" class="register-field-label">Confirm Password *</label>
                                    <input type="password" name="confirm_password" class="form-control form-input" id="confirm-password" placeholder="">
                                </div>
                            </div>
                        </div>

                        <div class="row register-agree">
                            <div  id="checkbox" class="col-sm-12">
                                <div class="checkbox" style="margin-top:0">

                                    <label class="terms_and_condition"><input type="checkbox" name="agree" >
                                        I agree to EC
                                        <a target="_blank" href="<?php echo base_url(); ?>home/terms_of_use" class="form-link">
                                                Terms of Use
                                        </a>,
                                        <a target="_blank" href="<?php echo base_url(); ?>home/privacy_policy" class="form-link">
                                                Privacy policy
                                        </a>&
                                        <a target="_blank" href="<?php echo base_url(); ?>home/nda" class="form-link">
                                                NDA
                                        </a>
                                    </label>
                                </div>                                    
                            </div>
                        </div>

                        <div class="row remove_margin register-agree">
                            <div class="join-now">
                                <input type="submit" id="register_submit" value="Register and Login Now" name="submit" class="btn btn-eq-common col-sm-12" >
                            </div> 
                        </div>

                        <div class="row text-center">
                            <div class="col-xs-12">
                                <div class="terms_and_condition">
                                     <p> 
                                    <a href="<?php echo base_url()?>user/login">Already Registered? Click here to Login</a>
                               </p>
                                  <p>  
                                    <a href="#" data-dismiss="modal" aria-label="Close"> Cancel and go back</a> 
                                </p>
                                 
                                </div>
                            </div>
                        </div> 

                    </div> <!-- /.register-box-2 -->            
                    
                </form>

        </div>  <!-- /.register-box -->
      </div> <!-- /.modal-body -->
    </div>
  </div>
</div>

  <div style="display: none;">

       <div id="investor-name-fields">
            <div class="col-sm-6 col-xs-12">
                <div class="form-group">
                    <label for="investor_type" class="register-field-label">Investor Type *</label>
                   
                        <select name="investor_type" class="form-control form-input" id="investor_type">
                        <option value="">Select Type</option>
                            <option value="Angel">Angel</option>
                            <option value="Family Office">Family Office</option>
                            <option value="Venture Fund">Venture Fund</option>
                            <option value="Angel Fund">Angel Fund</option>
                            <option value="Accelarator">Accelarator/ Incubators</option>
                        </select>
                    
                </div>
            </div>
    
 
             <div id = "inv_company-name-fields" style="display: none;" class="col-sm-6 col-xs-12">
                <div class="form-group">
                    <label for="name" class="register-field-label">Company Name *</label>
                   
                        <input type="text" name="company_name" class="form-control form-input" id="company_name" placeholder="" required>
                   
                </div>
            </div>

           <div class="col-sm-6 col-xs-12">
                <div class="form-group">
                    <label for="name" class="register-field-label">Full Name *</label>
                   
                        <input type="text" name="name" class="form-control form-input" id="name" placeholder="" required>
                   
                </div>
            </div>
            <div class="col-sm-6 col-xs-12">
                <div class="form-group">
                    <label for="linkedin_url" class="register-field-label">LinkedIn URL</label>
                   
                        <input type="text" name="linkedin_url" class="form-control form-input" id="linkedin_url" >
                    
                </div>
            </div>
       </div>


       <div id="investee-name-fields">
           <div class="col-sm-6 col-xs-12">
                <div class="form-group">
                    <label for="company-name" class="register-field-label">Company Name *</label>
                      <input type="text" name="company_name" class="form-control form-input" id="company-name" placeholder="" required>
                    </div>
           
        </div>    
           <div class="col-sm-6 col-xs-12">
                <div class="form-group">
                    <label for="name" class="register-field-label">Founder Name *</label>
                   
                        <input type="text" name="name" class="form-control form-input" id="name" placeholder="" required>
             
        </div>
    </div>
       </div>
       <div id="channel-name-fields">
           <div class="col-sm-6 col-xs-12">
                <div class="form-group">
                    <label for="company-name" class="register-field-label">Company Name *</label>
                    
                        <input type="text" name="company_name" class="form-control form-input" id="company-name" placeholder="" required>
                    
                </div>
            </div>
           <div class="col-sm-6 col-xs-12">
                <div class="form-group">
                    <label for="name" class="register-field-label">Representative Name *</label>
                   
                        <input type="text" name="name" class="form-control form-input" id="name" placeholder="" required>
                   
                </div>
            </div>
       </div>
       <div id="media-name-fields">
           <div class="col-sm-6 col-xs-12">
                <div class="form-group">
                    <label for="company-name" class="register-field-label">Company Name *</label>
                    
                        <input type="text" name="company_name" class="form-control form-input" id="company-name" placeholder="" required>
                   
                </div>
            </div>
           <div class="col-sm-6 col-xs-12">
                <div class="form-group">
                    <label for="name" class="register-field-label">Representative Name *</label>
                   
                        <input type="text" name="name" class="form-control form-input" id="name" placeholder="" required>
                    
                </div>
            </div>
       </div>
   </div>	
        
    <?=js('icheck.min.js')?>

    <script type="text/javascript">
        $(document).ready(function(){
//            $('.radio-inline input,.icheckbox').iCheck({
//                checkboxClass: 'icheckbox_minimal',
//                radioClass: 'iradio_minimal' // optional
//            });

          <?php if($user){
              if($user == 'investor'){ ?>
                  $('#name-fields').html($('#investor-name-fields').html());
                  $('input[value="investor"]').attr('checked','checked');
          <?php    }     
               if($user == 'investee'){ ?>
                  $('#name-fields').html($('#investee-name-fields').html());
                  $('input[value="investee"]').attr('checked','checked');
          <?php }  
          
                if($user == 'channel_partner'){ ?>
                    $('#name-fields').html($('#channel-name-fields').html());
                    $('input[value="channel_partner"]').attr('checked','checked');
           <?php }
           } ?>        

            $('.radio-inline  input[type="radio"]').click(function() {
//               var type = $(this).siblings('input[type="radio"]').val();
                 var type = $(this).val();
                switch (type) {
                                    case 'investor':
                                        $('#name-fields').html($('#investor-name-fields').html());
                                        break;
                                    case 'investee':
                                        $('#name-fields').html($('#investee-name-fields').html());
                                        break;
                                    case 'channel_partner':
                                        $('#name-fields').html($('#channel-name-fields').html());
                                        break;
                                    case 'media':
                                        $('#name-fields').html($('#media-name-fields').html());
                                        break;
                                }
            });


             $('.radio-inline  input[type="radio"]' ).click(function() {
//               var type = $(this).siblings('input[type="radio"]').val();
                 var type = $(this).val();
                 if(type == 'investor')
                 {
                    $('#investor_type').change(function(){
                      
                        var intype = $(this).val();
                        if(intype != 'Angel')
                        {
                          
                             $('#inv_company-name-fields').show();
                          
                        }
                        else
                        {
                            $('#inv_company-name-fields').hide();
                        }
                        });
                       
                 }
               
            });

			$.validator.addMethod('customphone', function (value, element) {
				return this.optional(element) ||  /^((\+)?[1-9]{1,2})?([-\s\.])?((\(\d{1,4}\))|\d{1,4})(([-\s\.])?[0-9]{1,12}){1,2}$/.test(value);
			}, "Please enter a valid mobile number");

            $.validator.addMethod('urlCheck', function (value, element) {
                return this.optional(element) ||  /^(http(s)?:\/\/)?([\w]+\.)?linkedin\.com\//.test(value);
            }, "Please enter a valid LinkedIn Url");





//validate="required:'input[name=investor_type][value != 'Family Office']'";

            $('#register-form').validate({
                rules: {

                            role: {required : true },
                            name: { required : true,
                                    minlength:4
                                },
                            investor_type: {required : true },
                            company_name:{ required :
                            {
                                 depends: function(element) {
                                   return ($('#investor_type').val() != "Family Office" && $('#investor_type').val() != "Angel Fund");
                                      }
                                     }
                                   },
                             
                            linkedin_url: { required : false,
                                urlCheck: true
                                },
                            mobileno: {required : true,
									   customphone: true
							},    
                            email: { required : true,
                                    email: true
                                },
                            user_password: { required : true,
                                    minlength:6
                            },
                            confirm_password:{ required : true,
                                               minlength:6,
                                               equalTo: "#user_password"
                            },
                            agree: { required : true }
                        },
                    
			messages: {
                role: { required: 'Please select your role' },
				name: {
					required: "Please provide your full name",
					minlength: "Your full name must be at least 4 characters long"
				},
                 investor_type: { required: 'Please select Investor Type' },
                 company_url: { required: 'Please enter the company url' },
               //  linkedin_url: { required: "Please provide your linkedin Url"},
                 mobileno: {
					required: "Please enter your mobile number",
                    number: "Please enter a valid mobile number",
					customphone:  "Please enter a valid mobile number",
                    minlength: "Please enter atleast 10  digit mobile number"
				},
				user_password: {
					required: "Please provide a password",
					minlength: "Your password must be at least 6 characters long"
				},
				confirm_password: {
					required: "Please provide a password",
					minlength: "Your password must be at least 6 characters long",
					equalTo: "Please enter the same password as above"
				},
				email: { 
                    required: "Please provide your email address",
                    email: "Please enter a valid email address"},
				agree: { required : "Please agree to EQ Terms of Use, Privacy policy & NDA"}
			},
             errorPlacement: function (error, element) {
                            if($(element).parent().hasClass("select-style")){
                                $(element).parents('.col-sm-8').append(error);
                            }else if($(element).attr("type") == 'file'){
                                $(element).parents('.col-sm-8').append(error);
                            }else if($(element).parent().hasClass("radio-inline")){
                                $('#radio-inline').append(error);
                            }else if ($(element).parents("label").hasClass("terms_and_condition")){
                                $('#checkbox').append(error);    
                            }else{
                                error.insertAfter(element); 
                            }
                           
                        }
            });

            $('#register-form').submit(function(){
                    if($("#register-form").valid()==false){

                    }else{
                      //  return true;
                        $.ajax({
                            url: '<?php echo base_url(); ?>user/register_process',
                            type: 'post',
                            data: $('#register-form').serialize(),
                            dataType: 'json',
                            beforeSend: function() {
                                $('#register_submit').button('loading');
                            },  
                            complete: function() {
                                $('#register_submit').button('reset');
                            },              
                            success: function(json) {
                                $('.failure-msg').hide();
                                $('.failure-msg').html('');
                                if(json['success']) {
                                    location = json['redirect'];
                                    //$('.failure-msg').html('<div class="col-xs-12"><p class="alert alert-success login-error-alert text-center" role="alert">'+json['success-msg']+'.</p></div>');
                                    $('#register-form')[0].reset();
                                } 
                                else{
                                    $('.failure-msg').html('<div class="col-xs-12"><p class="alert alert-danger login-error-alert text-center" role="alert">'+json['error-msg']+'.</p></div>');
                                }
                                 $('.failure-msg').show();
                            },
                            error: function(xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                    });

                } 
                    return false;
            });
});
    </script>
