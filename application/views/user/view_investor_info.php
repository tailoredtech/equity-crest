<div class="container">
    <div class="row">
        <div class="col-lg-10">
            <h1 class="page-title">
            <?php  

            if($this->session->userdata('role') == 'admin'){
              if($user['user_id'] == '')
              {
                    echo $user['name'].'&#39s';
              }
                }
                else
                {
                  echo "My";
                }
                ?> Profile</h1> 
            <?php //echo "<pre>"; print_r($user); echo "activepanel:-".$activePanel; ?>
        </div>
    </div>

    <div class="row common-profile-edit">
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 sidebar">           
           <div class="sidebar-filter-box">
                <div class="sidebar-filter-padding20 my-profile-box">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6 remove_right_padding my-profile-left">
                            <div class="profile-image">
                            <?php if ($user['image']!= '') { ?>
                            <img class="img-circle" src="<?php echo base_url(); ?>uploads/users/<?=$user_id?>/<?=$user['image']?>">                            
                        <?php } else { ?>
                            <img class="img-circle" src="<?php echo base_url(); ?>assets/images/gfxAvatarLarge.jpg">
                        <?php } ?>
                          <!--   <a  href="javascript:void(0)" data-toggle="modal" data-target="#profileimgModal" id="change-user-img">Edit</a> -->
                            </div>
                        </div>

                        <div class="col-sm-6 col-xs-6 my-profile-right">
                            <h3><?php echo $user['name']; ?></h3>
                            
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                             <ul class="my-profile-list">
                                <li class="<?php echo ($activePanel == 'personal-info' || $activePanel == '') ? 'active' : '' ?>" target-tab="#personal-info"><a href="#">Personal Information</a></li>
                                <li class="<?php echo ($activePanel == 'investment') ? 'active' : '' ?>" target-tab="#investment"><a href="#">Investment Interest</a></li> 
                                 <!-- <li class="<?php echo ($activePanel == 'startup-portfolio') ? 'active' : '' ?>" target-tab="#startup-portfolio"><a href="#">Portfolio</a></li>
                                <li class="<?php echo ($activePanel == 'interest') ? 'active' : '' ?>" target-tab="#interest"><a href="#">Interest in Mentoring</a></li>     -->                      
                           <!--  </ul> -->
                        </div>
                    </div>

                    <?php if($this->session->userdata('role')=='admin'){ ?>
                    <div class="row" style="border-top: 1px solid #e6e6e6; margin-top: 15px; padding-top: 5px;">                        
                        <form id="investor-update-form" action="<?php echo base_url(); ?>user/investor_info" method="post" >

                                <div class="col-xs-12">
                                    <h4>See profile of:</h4>
                                </div>
                           
                                <div class="col-xs-12 form-group">
                                    <div class="select-style">
                                        <select class="form-control form-input country" name="id" id="id" placeholder="">
                                            <option value="">Select Investor</option>
                                            <?php foreach ($investors_list as $investor) { ?>
                                                <option value="<?php echo $investor['id']; ?>" <?php echo ($investor['id'] == $user_id ? 'selected' : ''); ?> ><?php echo $investor['name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                           
                            
                                <div class="col-sm-12 ">
                                    <input type="submit" name="investor-update-form-submit"  class="btn btn-eq-common pull-right" value="Go" />
                                </div>
                            
                        </form>
                    </div>
                    <?php } ?>
                </div>
           </div>
        </div> <!-- /.sidebar -->

        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 col-main">
            <div class="profile-content-box">
                <form id="investor-info-form" action="<?php echo base_url(); ?>user/edit_investor_info_process" enctype="multipart/form-data" method="post" >
                 <input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
                    <!--Slider Accordion Starts here-->
                    <!-- Start Personal Information panel -->
                    <div class="accordion3">
                       <div class="accordion-heading  accordion-opened" >
                            <a class="accordion-toggle <?php echo ($activePanel == 'personal-info' || $activePanel == '') ? '' : 'collapsed' ?>" data-toggle="collapse" href="#personal-info"><h3>Personal Information</h3></a>
                        </div>
                        <div id="personal-info" class="accordion-body collapse <?php echo ($activePanel == 'personal-info' || $activePanel == '') ? 'in' : '' ?>">
                            <div class="accordion-inner">
                                <div class="profile-content-sub-section">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                            <h5 class="profile-content-sub-title">Personal</h5>
                                        </div>
                                    </div>
                                    <div  class="form-group row">
                                        <div class="col-xs-12"> <label for="sign-in-as" class="form-label" >Investor type : </label>
                                        <?php if(isset($user['investor_type']) && ($user['investor_type']!='' && $user['investor_type']!="0")) echo $user['investor_type']; else echo "Not Available"; ?></div>
                                     
                                    </div>  

                                     <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                   <!--  <div class="form-group row"> -->
                                                     <!--   <div class="col-xs-12">  <label for="name" class="form-label">Name : </label> <?php echo $user['name']; ?> </div>  -->
                                                        <!-- <div class="col-xs-12">
                                                            <?php echo $user['name']; ?>
                                                        </div> -->
                                                  <!--   </div> -->
                                          
                                                 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> 
                                                   <!-- <div class="form-group row">
                                                        <label for="mobilemumber" class="col-xs-12 form-label">Mobile Number : <?php echo $user['mob_no']; ?> </label>
                                                      <!--   <div class="col-xs-12">
                                                            <input type="text" name="mobileno" class="form-control form-input" id="mobileno" value="" placeholder="" />
                                                        </div> 
                                                    </div>
                                                <!-- </div> 
                                                
                                               <!--  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <div class="form-group row">
                                                        <label for="email" class="col-xs-12 form-label">Email : <?php echo $user['email']; ?></label>
                                                       <!--  <div class="col-xs-12">
                                                            <input type="text" name="email" class="form-control form-input" id="email" value="" placeholder="" readonly />
                                                        </div> -->
                                                    </div>
                                                 <!-- </div> -->
                                            

                                             <div class="form-group row">
                                              <div class="col-xs-12">   <label for="linkedin-url" class="form-label">Linkedin : &nbsp; </label>
                                                <?php if(isset($user['linkedin_url']) && $user['linkedin_url']!='' ) echo "<a href =".$user['linkedin_url']." target='_blank'>".$user['name']."</a>"; else echo "Not Available"; ?></div>
                                               <!--  <div class="col-xs-12">
                                                    <input type="text" name="linkedin_url" value="<?php echo $user['linkedin_url']; ?>" class="form-control form-input" id="linkedin-url" placeholder="Please enter your URL">
                                                </div> -->
                                             </div>

                                              <div class="form-group row">
                                               <div class="col-xs-12">  <label for="twitter-handle" class="form-label">Twitter : &nbsp; </label><?php if(isset($user['twitter_handle']) && $user['twitter_handle'] !='' ) echo "<a href='".$user['twitter_handle']."' target ='_blank'>".$user['name']."</a>"; else echo "Not Available"; ?></div>
                                               <!--  <div class="col-xs-12">
                                                    <input type="text" name="twitter_handle" value="<?php echo $user['twitter_handle']; ?>" class="form-control form-input" id="twitter-handle" placeholder="Please enter your URL">
                                                </div> -->
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                    </div>                                       

                                  <!--   <div class="row">
                                        <div class="col-lg-6 col-sm-6 col-xs-12">
                                         
                                        </div>
                                        <div class="col-lg-6 col-sm-6 col-xs-12">
                                            <label for="experience" class="col-xs-12 form-label">Experience Summary</label>
                                            <div class="col-xs-12">
                                                <textarea class="form-control form-input form-area" style="height:80px;" name="experience"  id="experience" placeholder=""><?php echo $user['experience']; ?></textarea>
                                            </div>
                                        </div>
                                    </div> -->

                                </div>    <!-- /.profile-content-sub-section -->
                                <div class="profile-content-sub-section">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                         <div class="row">
                                   
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <h5 class="profile-content-sub-title">Company</h5>
                                        </div>
                                    </div>
                                    <div  class="form-group row">
                                       <div class="col-xs-12"> 
                                        <label for="company_name" class="form-label">Current Organization : </label>
                                         <?php if(isset($user['company_name']) && ($user['company_name']!="" || $user['company_name'] != "NULL" )) { echo $user['company_name']; } else echo "Not Available"; ?> 
                                     </div>
                                 </div>
                                            
                                    <div  class="form-group row">    
                                       <div class="col-xs-12">   <label for="designation" class="form-label">Designation : </label><?php if(isset($user['role']) && ($user['role']!='' || $user['role']!='0')) echo $user['role']; else echo "Not Available"; ?> </div>
                                        <!-- <div class="col-xs-12">
                                            <input type="text" name="role" class="form-control form-input" id="role" value="<?php if($user['role']!='0') echo $user['role']; ?>" placeholder="">
                                        </div> -->
                                    </div>

                                  
                                      <div class="form-group row">
                                       
                                              <div class="col-xs-12">   <label for="city" class="form-label">City : </label> <?php if(isset($user['city']) && ($user['city'] != "" || $user['city']!="0")) { echo $user['city']; } else echo "Not Available"; ?> </div>
                                             
                                    </div>

                                    <div class="form-group row">

                                     <div class="col-xs-12"> <label for="state" class=" form-label">State : &nbsp; </label>  <?php  if(isset($user['state']) && $user['state'] != "") { foreach ($states as $state) { echo ($state['id'] == $user['state']) ? $state['name'] : ''; } } else echo "Not Available"; ?></div>
                                       
                                    </div> 
                                    <div class="form-group row">
                                     <div class="col-xs-12">  <label for="country" class=" form-label">Country : &nbsp; </label> <?php if(isset($user['country']) && $user['country'] != "") {foreach ($countries as $country) { echo ($country['id'] == $user['country'] ) ? $country['name'] : ''; } } else echo "Not Available"; ?></div>
                                    
                                    </div>    
                                      </div>
                                </div>                                 
                             

                                
                                 
                            </div> <!-- /.profile-content-sub-section -->

                       <!--          <div class="profile-content-sub-section">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <h5 class="profile-content-sub-title">Change password</h5>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <div class="form-group row">
                                                        <label for="newpassword" class="col-xs-12 form-label">New password</label>
                                                        <div class="col-xs-12">
                                                            <input type="password" name="user_password" class="form-control form-input" id="user_password" value="" placeholder="" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <div class="form-group row">
                                                        <label for="confirmpassword" class="col-xs-12 form-label">Confirm password</label>
                                                        <div class="col-xs-12">
                                                            <input type="password" name="confirm_password" class="form-control form-input" id="confirm_password" value="" placeholder="" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row remove_margin">
                                            <div class="col-sm-12">
                                                <input type="submit" name="account-setting-form-submit"  class="btn btn-eq-common pull-right" value="Save" />
                                            </div>
                                        </div> 
                                    </div>
                                </div>  --><!-- /.profile-content-sub-section --> 
                            </div>
                        </div>
                    </div>
                    <!-- End Personal Information panel -->    

                    <!-- Start Investment Philosophy panel -->
                    <div class="accordion3">
                        <div class="accordion-heading  accordion-opened" >
                            <a class="accordion-toggle <?php echo ($activePanel == 'investment') ? '' : 'collapsed' ?>" data-toggle="collapse" href="#investment"><h3>Investment Interest</h3></a>
                        </div>
                        <div id="investment" class="accordion-body collapse <?php echo ($activePanel == 'investment') ? 'in' : '' ?>">
                            <div class="accordion-inner">
                                <div class="profile-content-sub-section">
                                    <!-- <div class="row"> -->
                                       <!--  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> -->
                                           <div class="form-group row">
                                             <div class="col-xs-12"> 
                                               <label for="team-summary" class=" form-label">Sector Of Interest : </label>
                                                
                                                 <?php 
                                                   $sector_autocomplete_string = '';
                                                        if(isset($user['sector_expertise']) && $user['sector_expertise']!=''){
                                                            $str_sec = rtrim($user['sector_expertise'],',');
                                                            echo str_replace(",", ", ", $str_sec);
       
                                                       }   
                                                        else
                                                             {
                                                                echo "Not Available";
                                                             } ?></label>
                                                        </div>
                                                    </div>    
                                                   

                                            <div class="form-group row">
                                                <label for="startup_portfolio" class="col-xs-12 form-label">Startup Portfolio</label>
                                                <div class="col-xs-12">
                                                    <div class="select-style">

                                                       <?php
                                                       
                                                      foreach ($portfolios as $portfolio) { 
                                                     //   echo "<br>"; print_r($portfolio);
                                                       $port[] = $portfolio['name'];
                                                       $port_url[] = $portfolio['url'];
                                                      } 
                                                        
                                                    if(isset($port) && $port!='') {?>
                                                    <ul>
                                                    <?php  for($i = 0;$i<count($port);$i++)
                                                      {
                                                        //$port_string = implode(",",$port);
                                                        echo "<li>".$port[$i]." "; 
                                                        if(isset($port_url[$i]) && $port_url[$i] != '')
                                                        { ?>
                                                            <a href='http://<?php echo $port_url[$i] ?>' target='_blank'>(View Company Website)</a>
                                                            </li>
                                                       <?php }
                                                      
                                                      }?>
                                                  </ul>
                                                   <?php }
                                                    else
                                                    {
                                                       echo $port_string = "Not Available";
                                                    }
                                                         // echo $port_string;?>
                                                       
                                                 <!--    </div> -->
                                              <!--   </div> -->
                                           <!--  </div>  -->
                                          <!-- </div> -->
                                         </div>  
                                        </div>
                                    </div>

                                      <div class="form-group row">
                                              
                                            </div>  
                                        </div>
                                 
                                    <?php if($this->session->userdata('role')=='admin'){ ?>
                                  <div class="profile-content-sub-section">
                                     <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                  
                                                     <div class="col-xs-12"> 
                                                       <label for="name" class="form-label">Cheque Size : &nbsp; </label>
                                                        <?php echo $user['cheque_size']; ?>
                                                    </div>
                                                 
                                                      <div class="col-xs-12"> 
                                                       <label for="preference-as" class="form-label" >Investor Preference : &nbsp; </label>
                                                          <?php echo $user['preference_type']; ?>
                                                      </div> 

                                                <div class="col-xs-12"> 
                                                <label for="preference" class="form-label">How you got to know about equity crest? </label>
                                                    <?php echo $user['got_to_know']; ?>
                                                </div>
                                            </div>
                                                    <div id="others-info" style="display: <?php echo ($user['got_to_know'] == 'Others' ? "block" : 'none') ?>;">
                                                        <div id="others-field">
                                                           <div class="form-group row">
                                                                 <div class="col-xs-12">
                                                                    <input type="text" id="others-value" name="others-value" class="form-control form-input" value="<?php echo $user['others_value']; ?>" placeholder="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                      <!--   <div class="row remove_margin">
                                            <div class="col-sm-12">
                                                <input type="submit" name="account-setting-form-submit"  class="btn btn-eq-common pull-right" value="Save" />
                                            </div>
                                        </div>  -->
                                            </div>                              
                                              </div>
                                        </div>
                                                        <!-- <span id="startup-portfolio-add" class="btn col-sm-2 input-placeholder pull-right">Add</span>
                                                        <span id="startup-portfolio-remove" style="display:none;right: 19px;" class="btn col-sm-2 input-placeholder pull-right">Remove</span> -->
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                     </div>
                                </div>
                                <?php } ?>

                                             
                              
                            </div>
                                    
                                </div> <!-- /.profile-content-sub-section --> 
                            </div>
                        </div>
                    </div>
                
                </form>
            </div>
        </div>
    </div> <!-- / .col-main -->
    <?php // if($this->session->userdata('role')=='admin'){ ?>
   <!--  <div class="row pull-right sidebar-filter-padding20">
        <div class="col-sm-12 ">
            <input type="button" id="send_mail" name="send_mail"  class="btn btn-eq-common" value="Send Mail" />
        </div>
    </div> -->
    <?php //} ?>
</div>


<!-- <div style="display: none;">
    <div id="portfolio-fields">
        <div class="portfolio-box" id="id-0" portfolio-id="0" >
            <div class="form-group row">
                <label class="col-xs-12 form-label">Name of Company</label>
                <div class="col-xs-12">
                    <input type="text" id="portfolio_name" name="portfolio_name" class="form-control form-input" value="" placeholder="">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-xs-12 form-label">Website URL</label>
                <div class="col-xs-12">
                    <input type="text" id="portfolio_url" name="portfolio_url" value="" class="form-control form-input" placeholder="">
                </div>
            </div>
        </div>
    </div>
</div> -->



<?= js('jquery-ui.min.js') ?>
<?= js('tag-it.js') ?>
<script type="text/javascript">


    $('document').ready(function(){
    //    var portfolio_count = '<?php //echo $count; ?>';

       //  var  portfolio_count = $('#portfolio_count').val();
        $('.my-profile-list').on('click','li',function(e){
          
            var target=$(this).attr('target-tab');
               e.preventDefault();
            $('.accordion3').find('.accordion-body').removeClass('in');
            $('.accordion3').find('.accordion-heading a').addClass('collapsed');
            $(target).parent().find('.accordion-heading a').removeClass('collapsed');
            $('.accordion3 '+target).addClass('in');

            $("html, body").animate({
                 scrollTop:$(target).offset().top-150,
                 },"slow");
            //$(window).scrollTop($(target).offset().top-150);  

            $(this).addClass('active').siblings().removeClass('active');
           // alert(target);
        });

       
        
        $('#investor-info-form textarea').htmlarea({
            
            toolbar: [
                        ["bold", "italic", "underline"],
                        ["p"],
                        ["link", "unlink"],
                        ["orderedList","unorderedList"],
                        ["indent","outdent"],
                        ["justifyleft", "justifycenter", "justifyright"] 
                    ]
        });
        
        $('#investor-info-form').on('change', '#type', function(){

            var type = $(this).val();
            if(type == 'angel'){
                $('.company-name-input').html($('#company-name-field').html());
                $('.company-name-input').show();
            }else{
                $('.company-name-input').html('');
                $('.company-name-input').hide();
            }
        });
     
      $('#account-setting-form-submit').on('click', function(){
         $("#account-setting-form").valid();

      });
    


       
        
        $('.section-head').on('click',function(){
            $(this).parents('.row').next('.container').toggle();
            //$(this).parents('.row').next('.container').next('.row').toggle();
            
            if($(this).hasClass('companies')){
                $('.more-portfolio-btn').toggle();
                $('.portfolio-save').toggle();
                if ( $('#add-portfolios .new-portfolio-box' ).length ) {
 
                    $('#add-portfolios .new-portfolio-box').toggle();

                }
            }
        });
        
        $('.company').on('click',function(){
            var company_id = $(this).attr('id');
            company_id = company_id.split('-');
            company_id = company_id[1];
            $('.portfolio-box').hide();
            $('#portfolio-'+company_id).show();
        });
        
      
        
        $('#add-portfolios').on('click','.cancle-portfolio-btn',function(){
              $(this).parents('.new-portfolio-box').remove();
              portfolio_count--;
              $('#portfolio_count').val(portfolio_count);
        });


});
</script>
