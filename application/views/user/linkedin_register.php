<div class="container">
    <div class="row">
        <div class="col-sm-3 block-title" >
        <h2>Join Now</h2>
        </div>
    </div>
</div>
<div class="container">
    <div class="row register-box">
        <div class="col-sm-12">
            <div class="top-padding-30">               
                <div class="row success-msg" style="display:none;">
                    <div class="col-sm-12">
                        <p class="alert alert-success text-center" role="alert">
                            Thank you for registering on Equity Crest as an <span class="u"></span>. An activation e-mail has been sent to you on your e-mail ID <span class="e"></span>. Request you to please activate your Equity Crest account by clicking on the link sent across.
                        </p>
                    </div>
                </div>
                <div class="row failure-msg" style="display:none;">
                    <div class="col-sm-12">
                        <p class="alert alert-danger text-center" role="alert">
                            Oops! Something went wrong. Please Try Again.
                        </p>
                    </div>
                </div>
                <form method="post" id="register-form" action="<?php echo base_url(); ?>user/register_process">
                    <div class="col-sm-8 col-sm-offset-1">
                            <div class="row">
                                <div  class="form-group">
                                    <label for="sign-in-as" class="col-sm-4">Sign In as</label>
                                    <div id="radio-inline" class="col-sm-8">
                                        <label class="radio-inline ">
                                            <input type="radio" name="role"  value="investor"> Investor
                                        </label>
                                        <label class="radio-inline ">
                                                <input type="radio" name="role"  value="investee"> Start-Up
                                        </label>
                                        <label class="radio-inline ">
                                                <input type="radio" name="role"  value="channel_partner"> Partner
                                        </label>
                                        <label class="radio-inline ">
                                                <input type="radio" name="role"  value="media"> Media
                                        </label>
                                        <div class="clear-fix"></div>
                                    </div>
                                </div>

                            </div>
                            <div id="name-fields">
                           <div class="row"  >
                                    <div class="form-group">
                                        <label for="name" class="col-sm-4">Full Name *</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="name" class="form-control form-input" id="name" placeholder="" >
                                        </div>
                                    </div>
                            </div>
                            </div>
                            <div class="row">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 form-label ">Mobile No *</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="mobileno" class="form-control form-input customphone " id="mobile" placeholder="" >
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 form-label">Email Id *</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="email" class="form-control form-input" id="email" placeholder="" >
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                <div  id="checkbox" class="col-sm-6 col-sm-offset-4">
                                    <div class="checkbox">
                                        <label class="terms_and_condition"><input type="checkbox" name="agree" >
                                            I agree to EC
                                            <a href="<?php echo base_url(); ?>home/terms_of_use" class="form-link">
                                                    Terms of Use
                                            </a>,
                                            <a href="<?php echo base_url(); ?>home/privacy_policy" class="form-link">
                                                    Privacy policy
                                            </a>&
                                            <a href="<?php echo base_url(); ?>home/nda" class="form-link">
                                                    NDA
                                            </a>
                                        </label>
                                    </div>
                                    <div class="clear-fix"></div>  
                                </div>
                                <div class="join-now col-sm-1 pull-right">
                                        <input type="hidden" value="<?php echo $linkedin_id ?>" name="linkedin_id" />
                                        <input type="hidden" value="<?php echo $linkedin_picture; ?>" name="linkedin_picture" />
                                        <input type="submit" value="Join Now" name="submit" class="eq-btn col-sm-12 pull-right lato-regular" style="margin-right: 0px">
                                </div>                                                                
                            </div>
                            <div class="row">
                                <div class="col-md-offset-2 social-or"></div>
                                <div class="row social-sign-up">
                                        <div class="form-group">
                                            <p class="social-head col-sm-7" style="text-align: right;">Quick Sign in using</p>
                                            <div class="col-sm-5">
                                                <div class="  social-ico">
                                                        <a href="<?php echo base_url(); ?>user/linkedin_login" class="social-reg" id="linkedin">Linked-in</a>
                                                        <a href="<?php echo base_url(); ?>user/googleplus_login" class="social-reg" id="google">Google</a>
                                                        <a href="<?php echo base_url(); ?>user/fb_login" class="social-reg" id="facebook">Facebook</a>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>    
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

   <fdiv style="display: none;">
       <div id="investor-name-fields">
           <div class="row">
                <div class="form-group">
                    <label for="name" class="col-sm-4 form-label">Full Name *</label>
                    <div class="col-sm-8">
                        <input type="text" name="name" class="form-control form-input" id="name" placeholder="" required>
                    </div>
                </div>
            </div>
       </div>
       <div id="investee-name-fields">
           <div class="row">
                <div class="form-group">
                    <label for="company-name" class="col-sm-4 form-label">Company Name *</label>
                    <div class="col-sm-8">
                        <input type="text" name="company_name" class="form-control form-input" id="company-name" placeholder="" required>
                    </div>
                </div>
            </div>
           <div class="row">
                <div class="form-group">
                    <label for="name" class="col-sm-4 form-label">Founder Name *</label>
                    <div class="col-sm-8">
                        <input type="text" name="name" class="form-control form-input" id="name" placeholder="" required>
                    </div>
                </div>
            </div>
       </div>
       <div id="channel-name-fields">
           <div class="row">
                <div class="form-group">
                    <label for="company-name" class="col-sm-4 form-label">Company Name *</label>
                    <div class="col-sm-8">
                        <input type="text" name="company_name" class="form-control form-input" id="company-name" placeholder="" required>
                    </div>
                </div>
            </div>
           <div class="row">
                <div class="form-group">
                    <label for="name" class="col-sm-4 form-label">Representative Name *</label>
                    <div class="col-sm-8">
                        <input type="text" name="name" class="form-control form-input" id="name" placeholder="" required>
                    </div>
                </div>
            </div>
       </div>
       <div id="media-name-fields">
           <div class="row">
                <div class="form-group">
                    <label for="company-name" class="col-sm-4 form-label">Company Name *</label>
                    <div class="col-sm-8">
                        <input type="text" name="company_name" class="form-control form-input" id="company-name" placeholder="" required>
                    </div>
                </div>
            </div>
           <div class="row">
                <div class="form-group">
                    <label for="name" class="col-sm-4 form-label">Representative Name *</label>
                    <div class="col-sm-8">
                        <input type="text" name="name" class="form-control form-input" id="name" placeholder="" required>
                    </div>
                </div>
            </div>
       </div>
   </div>   

        
    <?=js('icheck.min.js')?>

    <script type="text/javascript">
        $(document).ready(function(){


                

            $('.radio-inline  input[type="radio"]').click(function() {
                 var type = $(this).val();
                switch (type) {
                                    case 'investor':
                                        $('#name-fields').html($('#investor-name-fields').html());
                                        break;
                                    case 'investee':
                                        $('#name-fields').html($('#investee-name-fields').html());
                                        break;
                                    case 'channel_partner':
                                        $('#name-fields').html($('#channel-name-fields').html());
                                        break;
                                    case 'media':
                                        $('#name-fields').html($('#media-name-fields').html());
                                        break;
                                }
            });

            $.validator.addMethod('customphone', function (value, element) {
                return this.optional(element) ||  /^((\+)?[1-9]{1,2})?([-\s\.])?((\(\d{1,4}\))|\d{1,4})(([-\s\.])?[0-9]{1,12}){1,2}$/.test(value);
            }, "Please enter a valid mobile number");


            $('#register-form').validate({
                rules: {
                            role:{ required : true },
                            name: { required : true,
                                    minlength:4
                                },
                            mobileno: { required : true,
                                        customphone: true
                                },    
                            email: { required : true,
                                    email: true
                                },
                            password: { required : true,
                                    minlength:6
                            },
                            confirm_password:{ required : true,
                                               minlength:6,
                                               equalTo: "#password"
                            },
                            agree:'required'
                        },
			messages: {
                                role:'Please select your role',
				name: {
					required: "Please provide your full name",
					minlength: "Your full name must be at least 4 characters long"
				},
                                mobileno: {
					required: "Please enter your mobile number",
                                        number: "Please enter valid mobile number",
					minlength: "Please enter 10  digit mobile number"
				},
				password: {
					required: "Please provide a password",
					minlength: "Your password must be at least 6 characters long"
				},
				confirm_password: {
					required: "Please provide a password",
					minlength: "Your password must be at least 6 characters long",
					equalTo: "Please enter the same password as above"
				},
				email: "Please enter a valid email address",
				agree: "Please accept our policy"
			},
                        errorPlacement: function (error, element) {
                            if($(element).parent().hasClass("select-style")){
                                $(element).parents('.col-sm-8').append(error);
                            }else if($(element).attr("type") == 'file'){
                                $(element).parents('.col-sm-8').append(error);
                            }else{
                                error.insertAfter(element); 
                            }
                            if($(element).parent().hasClass("radio-inline")){
                                $('#radio-inline').append(error);
                            }
                            if ($(element).parent().hasClass("terms_and_condition")){
                                $('#checkbox').append(error);    
                            }
                        }
            });

            $('#register-form').submit(function(){
                    if($("#register-form").valid()==false){

                    }else{
                        return true;
//                        var email = $('#email').val();
////                        var type = $('input[name="role"]').val();
//                        var type = $('input:radio[name="role"]:checked').val();
//                        $.ajax({
//                        type:'post',
//                        dataType:'json',
//                        url:$(this).attr('action'),
//                        data: $(this).serializeArray(),
//                        success: function(data) {
//                                if(data.success == true){
//                                    
//                                    switch (type) {
//                                        case 'investor':
//                                            type = "Investor";
//                                            break;
//                                        case 'investee':
//                                            type = "Start-Up";
//                                            break;
//                                        case 'channel_partner':
//                                            type = "Channel partner";
//                                            break;
//                                        case 'media':
//                                            type = "Media";
//                                            break;
//                                    }
//                                    
//                                    $('.success-msg .u').html(type);
//                                    $('.success-msg .e').html(email);
//                                    $('.success-msg').show();
//                                }else{
//                                    $('.failure-msg').show();
//                                }
//                                
//                                $("#register-form").trigger('reset');
//                            },
//                            error: function(xhr, textStatus, thrownError) {
//                                console.log('error');
//                            }
//                        });

                    } 
                    return false;
            });
        });
    </script>
