<div class="container">
    <div class="row">
        <h4 class="form-header col-lg-12 col-md-12 col-sm-12 col-xs-12 ">Investor Profile Update</h4>
    </div>
    <div class="container investor-register-form">
        <div class="container  col-lg-6 col-md-6 col-sm-6 col-xs-6 col-lg-offset-3 col-md-offset-3 col-xs-offset-3 ">
           <form id="investor-info-form" action="<?php echo base_url(); ?>user/investor_info_process" enctype="multipart/form-data" method="post" >
               <div class="row">
                    <div class="">
                        <label for="sign-in-as" class="form-label col-lg-3 col-md-3 col-sm-3 col-xs-3 black lato-bold ">Type of Investor</label>
                        <div class="col-sm-9">
                            <div class="select-style">
                                <select name="type" id="type" class="form-control form-input">
                                    <option value="individual">Individual</option>
                                    <option value="other">Other</option>
                                </select>
                            </div>
                        </div>
                    </div>
               </div>
               <div id="individual-form">
                   <div class="row">
                        <div class="form-group">
                            <label for="name" class="col-sm-3 form-label">Name</label>
                            <div class="col-sm-9">
                                <input type="text" name="name" class="form-control form-input" id="name" value="<?php echo $this->session->userdata('name'); ?>" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="experience" class="col-sm-3 form-label">Experience Summary</label>
                            <div class="col-sm-9">
                                <textarea class="form-control form-input form-area" name="experience" id="experience" placeholder=""></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="role" class="col-sm-3 form-label">Role</label>
                            <div class="col-sm-9">
                                <input type="text" name="role" class="form-control form-input" id="role" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="website" class="col-sm-3 form-label">Website URL</label>
                            <div class="col-sm-9">
                                <input type="text" name="website" class="form-control form-input" id="website" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="country" class="col-sm-3 form-label">Country</label>
                            <div class="col-sm-9">
                                <div class="select-style">
                                    <select class="form-control form-input country" name="country"  placeholder="">
                                        <option value="">Select Country</option>
                                        <?php foreach($countries as $country){ ?>
                                                <option value="<?php echo $country['id']; ?>" ><?php echo $country['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="state" class="col-sm-3 form-label">State</label>
                            <div class="col-sm-9">
                                <div class="select-style">
                                    <select class="form-control form-input state" name="state"  placeholder="">
                                        <option value="">Select State</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="city" class="col-sm-3 form-label">City</label>
                            <div class="col-sm-9">
                                <input type="text" name="city" class="form-control form-input" id="city" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 form-label">Your Photo</label>
                            <div class="col-sm-9">
                                <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput">
                                    <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                                    <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" id="image" name="image"></span>
                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                        </div>
                    </div>
               </div>
               <div id="other-form">
                   
               </div>
               <div id="optional-form" style="display: none;">
                   <div class="row">
                       <div class="form-group">
                        <div class="col-sm-9 col-md-offset-5">
                            Type of investor (Optional)
                        </div>
                       </div>
                   </div>
                   <div id="investor-team">
                       
                   </div>
                   <div class="col-sm-3 more-btn pull-right btn btn-default">
                           Add More
                   </div>
               </div>
                <div class="row">
                    <div class="join-now col-sm-4 col-sm-offset-3 pull-left">
    <!--                    <button class="join-btn col-sm-12 pull-right ">Save & Continue</button>-->
                        <input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
                        <input type="submit" class="join-btn col-sm-12 pull-right" name="submit" value="Save & Continue" />
                    </div>
                </div>
          </form>    
        </div>
    </div>
</div>
<div style="display: none;">
    <div id="individual-form-fields">
        <div class="row">
            <div class="form-group">
                <label for="name" class="col-sm-3 form-label">Name</label>
                <div class="col-sm-9">
                    <input type="text" name="name" class="form-control form-input" id="name" placeholder="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <label for="experience" class="col-sm-3 form-label">Experience Summary</label>
                <div class="col-sm-9">
                    <textarea class="form-control form-input form-area" name="experience" id="experience" placeholder=""></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <label for="role" class="col-sm-3 form-label">Role</label>
                <div class="col-sm-9">
                    <input type="text" name="role" class="form-control form-input" id="role" placeholder="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <label for="website" class="col-sm-3 form-label">Website URL</label>
                <div class="col-sm-9">
                    <input type="text" name="website" class="form-control form-input" id="website" placeholder="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <label for="country" class="col-sm-3 form-label">Country</label>
                <div class="col-sm-9">
                    <div class="select-style">
                        <select class="form-control form-input country" name="country"  placeholder="">
                            <option value="">Select Country</option>
                            <?php foreach($countries as $country){ ?>
                                    <option value="<?php echo $country['id']; ?>"><?php echo $country['name']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <label for="state" class="col-sm-3 form-label">State</label>
                <div class="col-sm-9">
                    <div class="select-style">
                        <select class="form-control form-input state" name="state"  placeholder="">
                            <option value="">Select State</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <label for="city" class="col-sm-3 form-label">City</label>
                <div class="col-sm-9">
                    <input type="text" name="city" class="form-control form-input" id="city" placeholder="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 form-label">Your Photo</label>
                <div class="col-sm-9">
                    <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput">
                        <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                        <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" id="image" name="image"></span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="other-form-fields">
        <div class="row">
                <div class="form-group">
                    <label for="company_name" class="col-sm-3 form-label">Name of the company</label>
                    <div class="col-sm-9">
                        <input type="text" name="company_name" class="form-control form-input" id="company_name" placeholder="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label for="about_company" class="col-sm-3 form-label">About your company</label>
                    <div class="col-sm-9">
                        <textarea class="form-control form-input form-area" name="about_company" id="about_company" placeholder=""></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label for="year" class="col-sm-3 form-label">Year of Establishment</label>
                    <div class="col-sm-9">
                        <input type="text" name="year" class="form-control form-input" id="year" placeholder="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label for="website" class="col-sm-3 form-label">Website URL</label>
                    <div class="col-sm-9">
                        <input type="text" name="website" class="form-control form-input" id="website" placeholder="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label for="country" class="col-sm-3 form-label">Country</label>
                    <div class="col-sm-9">
                        <div class="select-style">
                            <select class="form-control form-input country" name="country"  placeholder="">
                                <option value="">Select Country</option>
                                <?php foreach($countries as $country){ ?>
                                        <option value="<?php echo $country['id']; ?>"><?php echo $country['name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label for="state" class="col-sm-3 form-label">State</label>
                    <div class="col-sm-9">
                        <div class="select-style">
                            <select class="form-control form-input state" name="state"  placeholder="">
                                <option value="">Select State</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label for="city" class="col-sm-3 form-label">City</label>
                    <div class="col-sm-9">
                        <input type="text" name="city" class="form-control form-input" id="city" placeholder="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 form-label">Company Logo/Photo</label>
                    <div class="col-sm-9">
                        <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput">
                            <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                            <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" id="image" name="image"></span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <div id="investor-team-fields">
        <div class="row">
            <div class="form-group">
                <label for="name" class="col-sm-3 form-label">Name</label>
                <div class="col-sm-9">
                    <input type="text" name="optional_name[]" class="form-control form-input"  placeholder="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <label for="experience" class="col-sm-3 form-label">Experience Summary</label>
                <div class="col-sm-9">
                    <textarea class="form-control form-input form-area" name="optional_experience[]"  placeholder=""></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <label for="role" class="col-sm-3 form-label">Role</label>
                <div class="col-sm-9">
                    <input type="text" name="optional_role[]" class="form-control form-input"  placeholder="">
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('document').ready(function(){
        $('#investor-info-form').on('change', '.country', function(){
                $.ajax({
                    url: "<?php echo base_url(); ?>user/states",
                    type: 'POST',
                    data : {countryId : $(this).val()}
                })
                .done(function( data ) {
                    $('#investor-info-form .state').html(data);
                });
        });
        
        $('#type').change(function(){
            if($(this).val() == 'individual'){
                $('#individual-form').html($('#individual-form-fields').html());
                $('#other-form').html('');
                $('#optional-form').hide();
                $('#optional-form #investor-team').html('');
            }else{
                $('#other-form').html($('#other-form-fields').html());
                $('#individual-form').html('');
                $('#optional-form').show();
                $('#optional-form #investor-team').html('');
                $('#optional-form #investor-team').append($('#investor-team-fields').html());
            }
        });
        
        $('#optional-form .more-btn').click(function(){
            $('#optional-form #investor-team').append($('#investor-team-fields').html());
        });
        
        $('#investor-info-form').validate({
                rules: {
                            role:'required',
                            name: { required : true,
                                    minlength:4
                                },
                            experience: 'required',    
                            website:'required',
                            country:'required',
                            state:'required',
                            city:'required',
                            year:'required',
                            company_name:'required',
                            about_company:'required',
                            image:'required',
                            'optional_name[]':'required'
                        } ,
			messages: {
                                role:'Please select your role',
				name: {
					required: "Please provide name",
					minlength: "Name must be at least 4 characters long"
				},
                                experience: {
					required: "Please enter your experience summary"
				},
				website: {
					required: "Please provide your website"
				},
				country: 'Please select country',
                                state: 'Please select state',
                                city: 'Please enter city',
                                year:'Please enter year of establishment',
				company_name: "Please enter name of the compnay",
				about_company: "Please enter about your company",
                                image:'Please upload image'
			},
                 errorPlacement: function (error, element) {
                    if($(element).parent().hasClass("select-style")){
                        $(element).parents('.col-sm-9').append(error);
                    }else if($(element).attr("type") == 'file'){
                        $(element).parents('.col-sm-9').append(error);
                    }else{
                       error.insertAfter(element); 
                    } 
                }
            });
        
    });
</script>