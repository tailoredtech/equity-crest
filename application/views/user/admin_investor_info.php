<div class="container">
    <div class="row">
        <div class="col-lg-10">
            <h1 class="page-title">My Profile</h1>
            <?php //print_r($user) echo "activepanel:-".$activePanel; ?>
        </div>
    </div>

    <div class="row common-profile-edit">
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 sidebar">           
           <div class="sidebar-filter-box">
                <div class="sidebar-filter-padding20 my-profile-box">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6 remove_right_padding my-profile-left">
                            <div class="profile-image">
                            <?php if ($user['image']!= '') { ?>
                            <img class="img-circle" src="<?php echo base_url(); ?>uploads/users/<?=$user_id?>/<?=$user['image']?>">                            
                        <?php } else { ?>
                            <img class="img-circle" src="<?php echo base_url(); ?>assets/images/gfxAvatarLarge.jpg">
                        <?php } ?>
                            <a  href="javascript:void(0)" data-toggle="modal" data-target="#profileimgModal" id="change-user-img">Edit</a>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-6 my-profile-right">
                            <h3><?php echo $user['name']; ?></h3>
                            <h6><?php echo $user['role']; ?></h6>
                        </div>
                    </div>
                    <div class="row" style="border-top: 1px solid #e6e6e6; margin-top: 15px; padding-top: 5px;">                        
                        <form id="investor-update-form" action="<?php echo base_url(); ?>user/investor_info" method="post" >

                                <div class="col-xs-12">
                                    <h4>See profile of:</h4>
                                </div>
                           
                                <div class="col-xs-12 form-group">
                                    <div class="select-style">
                                        <select class="form-control form-input country" name="id" id="id" placeholder="">
                                            <option value="">Select Investor</option>
                                            <?php foreach ($investors_list as $investor) { ?>
                                                <option value="<?php echo $investor['id']; ?>" ><?php echo $investor['name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                           
                            
                                <div class="col-sm-12 ">
                                    <input type="submit" name="investor-update-form-submit"  class="btn btn-eq-common pull-right" value="Go" />
                                </div>
                            
                        </form>
                    </div>
                </div>
           </div>
        </div>
    </div>
</div>
