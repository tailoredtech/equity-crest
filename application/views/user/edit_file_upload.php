<div class="container">
    <div class="row">
        <div class="col-xs-12 block-title">
            <h1 class="page-title">Upload</h1>
        </div>
    </div>
</div>
<div class="container">
    <form id="investee-file-form" action="<?php echo base_url(); ?>user/edit_file_upload_process" enctype="multipart/form-data" method="post" >
        <div>
        <!--Slider Accordion Starts here-->
            <!-- Start Product Info panel -->
            <div class="accordion2">
                <div class="accordion-heading  accordion-opened" >                    
                    <div class="row">
                        <div class="col-xs-8"><a class="accordion-toggle" data-toggle="collapse" href="#Product_Info"><h4>Product Info</h4></a></div>
                        <div class="col-xs-4 pull-right">
                            <span class="glyphicon glyphicon-cog privacy-icon"></span>
                            <div class="privacy-options">
                                <select name="business" class="form-control form-input">
                                <option value="" disabled selected>Visible to</option>
                                <option value="0" <?php echo ($privacy['business'] == 0) ? 'selected' : ''; ?>>All</option>
                                <option value="1" <?php echo ($privacy['business'] == 1) ? 'selected' : ''; ?>>All investors & partners</option>
                                <option  value="2" <?php echo ($privacy['business'] == 2) ? 'selected' : ''; ?>>Pledged Investors</option>
                                <option  value="3" <?php echo ($privacy['business'] == 3) ? 'selected' : ''; ?>>Followers</option>
                                <option  value="4" <?php echo ($privacy['business'] == 4) ? 'selected' : ''; ?>>On Request</option>
                                </select>
                            </div>                                
                        </div>
                    </div>
                </div>
                <div id="Product_Info" class="accordion-body collapse in">
                    <div class="accordion-inner2">
                        <div class="form-group row">
                            <label for="company_presentation" class="col-sm-5 form-label">Company Presentation</label>
                            <div class="col-sm-7">
                                <textarea cols="50" rows="15" class="form-control form-input form-area" id="company_presentation" name="company_presentation" placeholder="Paste Slideshare Embed Code. Example: <iframe src='//www.slideshare.net/slideshow/embed_code/xxxxxx'></iframe>">
                                <?php echo isset($user['presentation']) ? $user['presentation'] : ''; ?>
                                </textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="proposal-video" class="col-sm-5 form-label">Proposal Video</label>
                            <div class="col-sm-7">
                                <input type="text" name="proposal_video" value="<?php echo isset($user['video_link']) ? $user['video_link'] : ''; ?>" class="form-control form-input" id="proposal-video" placeholder="Youtube link">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Product Info panel -->

            <!-- Start Financial Info panel -->
            <div class="accordion2">
                <div class="accordion-heading  accordion-opened" >                    
                    <div class="row">
                        <div class="col-xs-8"><a class="accordion-toggle" data-toggle="collapse" href="#Financial_Info"><h4>Financial Info</h4></a></div>
                        <div class="col-xs-4 pull-right">
                            <span class="glyphicon glyphicon-cog privacy-icon"></span>
                            <div class="privacy-options">
                                 <select name="financial" class="form-control form-input">
                            <option value="" disabled selected>Visible to</option>
                            <option value="0" <?php echo ($privacy['financial'] == 0) ? 'selected' : ''; ?>>All</option>
                            <option value="1" <?php echo ($privacy['financial'] == 1) ? 'selected' : ''; ?>>All investors & partners</option>
                            <option  value="2" <?php echo ($privacy['financial'] == 2) ? 'selected' : ''; ?>>Pledged Investors</option>
                            <option  value="3" <?php echo ($privacy['financial'] == 3) ? 'selected' : ''; ?>>Followers</option>
                            <option  value="4" <?php echo ($privacy['financial'] == 4) ? 'selected' : ''; ?>>On Request</option>
                        </select>
                            </div>                                
                        </div>
                    </div>
                </div>
                <div id="Financial_Info" class="accordion-body collapse in">
                    <div class="accordion-inner2">
                        <div class="form-group row">
                            <label for="financial-forecast" class="col-sm-5 form-label">Financial Forecast</label>
                            <?php if(isset($user['financial_forecast']) && $user['financial_forecast']!=''){ 
                                        
                                ?>
                            <div class="col-sm-7">
                                <span class="form-input" style="line-height:1.7em;display:block;position:relative;">
                                    <a href="<?php echo base_url(); ?>uploads/users/<?php echo $user['user_id'] ?>/files/<?php echo $user['financial_forecast'] ?>"><?php echo $user['financial_forecast'] ?></a>
                                    <span class="col-sm-2 input-placeholder pull-right glyphicon glyphicon-remove remove-file" style="position:absolute;cursor:pointer;top: 0px;line-height: 1.7;height: 28px;right: 0px;" data-file="financial_forecast"></span>
                                </span>
                            </div>
                            <?php }else{ ?>
                            <div class="col-sm-7">
                                <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput">
                                    <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                                    <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" id="financial-forecast" name="financial_forecast"></span>
                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="form-group row">
                            <label for="financial-statement" class="col-sm-5 form-label">Financial Statements</label>
                            <?php if(isset($user['financial_statement']) && $user['financial_statement']!=''){ ?>
                            <div class="col-sm-7">
                                <span class="form-input" style="line-height:1.7em;display:block;position:relative;">
                                    <a href="<?php echo base_url(); ?>uploads/users/<?php echo $user['user_id'] ?>/files/<?php echo $user['financial_statement'] ?>"><?php echo $user['financial_statement'] ?></a>
                                    <span class="col-sm-2 input-placeholder pull-right glyphicon glyphicon-remove remove-file" style="position:absolute;cursor:pointer;top: 0px;line-height: 1.7;height: 28px;right: 0px;" data-file="financial_statement"></span>
                                </span>
                            </div>
                            <?php }else{ ?>
                            <div class="col-sm-7">
                                <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput">
                                    <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                                    <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" id="financial-statement" name="financial_statement"></span>
                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="form-group row">
                            <label for="other-statement" class="col-sm-5 form-label">Others (if any )</label>
                            <?php if(isset($user['other']) && $user['other']!=''){ ?>
                            <div class="col-sm-7">
                                <span class="form-input" style="line-height:1.7em;display:block;position:relative;">
                                    <a href="<?php echo base_url(); ?>uploads/users/<?php echo $user['user_id'] ?>/files/<?php echo $user['other'] ?>"><?php echo $user['other'] ?></a>
                                    <span class="col-sm-2 input-placeholder pull-right glyphicon glyphicon-remove remove-file" style="position:absolute;cursor:pointer;top: 0px;line-height: 1.7;height: 28px;right: 0px;" data-file="other"></span>
                                </span>
                            </div>
                            <?php }else{ ?>
                            <div class="col-sm-7">
                                <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput">
                                    <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                                    <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" id="other-statement" name="other"></span>
                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        
                    </div>
                </div>
            </div>
            <!-- End Financial Info panel -->

            <!-- Start Achievements panel -->
            <div class="accordion2">
                <div class="accordion-heading  accordion-opened" >                    
                    <div class="row">
                        <div class="col-xs-8"><a class="accordion-toggle " data-toggle="collapse" href="#Achievements"><h4>Achievements</h4></a></div>
                        <div class="col-xs-4 pull-right">
                            <span class="glyphicon glyphicon-cog privacy-icon"></span>
                            <div class="privacy-options">
                                <select name="achievements" class="form-control form-input">
                                <option value="" disabled selected>Visible to</option>
                                <option value="0" <?php echo ($privacy['achievements'] == 0) ? 'selected' : ''; ?>>All</option>
                                <option value="1" <?php echo ($privacy['achievements'] == 1) ? 'selected' : ''; ?>>All investors & partners</option>
                                <option  value="2" <?php echo ($privacy['achievements'] == 2) ? 'selected' : ''; ?>>Pledged Investors</option>
                                <option  value="3" <?php echo ($privacy['achievements'] == 3) ? 'selected' : ''; ?>>Followers</option>
                                <option  value="4" <?php echo ($privacy['achievements'] == 4) ? 'selected' : ''; ?>>On Request</option>
                                </select>
                            </div>                                
                        </div>
                    </div>
                </div>
                <div id="Achievements" class="accordion-body collapse in">
                    <div class="accordion-inner2">
                        <div class="form-group row">
                            <label for="financial-forecast" class="col-sm-5 form-label">Awards / Recognition</label>
                            <div class="col-sm-7">
                                <textarea class="form-control form-input form-area" cols="50" rows="15" id="awards" name="awards"  placeholder=""><?php echo isset($user['awards']) ? $user['awards'] : "" ?></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="financial-forecast" class="col-sm-5 form-label">Testimonials</label>
                            <div class="col-sm-7">
                                <textarea class="form-control form-input form-area" cols="50" rows="15" id="testimonials" name="testimonials"  placeholder=""><?php echo isset($user['testimonials']) ? $user['testimonials'] : "" ?></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="financial-forecast" class="col-sm-5 form-label">Media Coverage</label>
                            <div class="col-sm-7">
                                <textarea class="form-control form-input form-area" cols="50" rows="15" id="media" name="media" placeholder=""><?php echo isset($user['media']) ? $user['media'] : "" ?></textarea>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
            <!-- End Achievements panel -->              
        </div>
        <div class="row text-center">
            <input type="submit" class="btn btn-eq-common" name="submit" value="Submit" />
            <a href="<?php echo base_url(); ?>user/investee_info"><input type="button" class="btn btn-eq-grey" value="Back" /></a>
        </div> 
    </form>
</div>        

<div style="display: none;">
    <div id="file-input-field">
        <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput">
            <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
            <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" ></span>
            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
        </div>
    </div>
</div>


<script>
    $(document).ready(function(){
        
        $('.privacy-options select').on('change',function(){
             $.ajax({
                url: "<?php echo base_url(); ?>investee/save_privacy",
                type: 'POST',
                data : {optionVal : $(this).val(),optionKey :$(this).attr('name') }
            })
            .done(function( data ) {
                
            });
        });
        
        $('textarea').htmlarea({
            toolbar: [
                ["bold", "italic", "underline"],
                ["p"],
                ["link", "unlink"],
                ["orderedList", "unorderedList"],
                ["indent", "outdent"],
                ["justifyleft", "justifycenter", "justifyright"]
            ]
        });
        
         $("#company_presentation").htmlarea("dispose");
        
        $('#investee-file-form').on('click','.remove-file',function(){

            var file = $(this).attr('data-file');
            var current = $(this).parent('.form-input').parent('.col-sm-7');
            $('#file-input-field').find('input').attr('name',file);
            $('#file-input-field').find('input').attr('id',file);
            $(current).html($('#file-input-field').html());
        });

       
    });
</script>