<div class="main">
    <div class="container wrap">
        <div class="row">
            <h4 class="form-header  col-sm-12 ">Company Information</h4>
        </div>
        <form id="investee-info-form" action="<?php echo base_url(); ?>user/investee_info_process" enctype="multipart/form-data" method="post" >
            <div class="container investor-register-form">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="lato-bold pull-left section-head">Company Info <span class="glyphicon glyphicon-chevron-down dropicon"></span></div>
                    </div>
                </div>
                <div class="container company-info  col-sm-7 col-sm-offset-2">
                    <div class="row">
                        <div class="form-group">
                            <label for="founder-name" class="col-sm-4 form-label black lato-bold ">Founder Name</label>
                            <div class="col-sm-8">
                                <input type="text" name="name" value="<?php echo $user['name'] ?>" class="form-control form-input" id="founder-name" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="sign-in-as" class="col-sm-4 form-label black lato-bold ">Name of the Company</label>
                            <div class="col-sm-8">
                                <input type="text" name="company_name" value="<?php echo $user['company_name'] ?>" class="form-control form-input" id="company_name" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="company_url" class="col-sm-4 form-label">Company URL</label>
                            <div class="col-sm-8">
                                <input type="text" name="company_url" class="form-control form-input" id="company_url" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="product_url" class="col-sm-4 form-label">Product Website URL</label>
                            <div class="col-sm-8">
                                <input type="text" name="product_url" class="form-control form-input" id="product_url" placeholder="www">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="country" class="col-sm-4 form-label">Country</label>
                            <div class="col-sm-8">
                                <div class="select-style">
                                    <select class="form-control form-input country" name="country"  placeholder="">
                                        <option value="">Select Country</option>
                                        <?php foreach ($countries as $country) { ?>
                                            <option value="<?php echo $country['id']; ?>"><?php echo $country['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="state" class="col-sm-4 form-label">State</label>
                            <div class="col-sm-8">
                                <div class="select-style">
                                    <select class="form-control form-input state" name="state"  placeholder="">
                                        <option value="">Select State</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="city" class="col-sm-4 form-label">City</label>
                            <div class="col-sm-8">
                                <input type="text" name="city" class="form-control form-input" id="city" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="year" class="col-sm-4 form-label">Founding Year</label>
                            <div class="col-sm-8">
                                <input type="text" name="year" class="form-control form-input" id="year" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="stage" class="col-sm-4 form-label">Stage</label>
                            <div class="col-sm-8">
                                <div class="select-style">
                                    <select class="form-input" id="stage" name="stage" placeholder="Select Startup Stage">
                                        <?php foreach ($stages as $stage) { ?>
                                            <option value='<?php echo $stage['id']; ?>'><?php echo $stage['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="sector" class="col-sm-4 form-label">Sector</label>
                            <div class="col-sm-8">
                                <div class="select-style">
                                    <select class="form-control form-input" id="sector" name="sector" placeholder="Select Sector">
                                        <?php foreach ($sectors as $sector) { ?>
                                            <option value='<?php echo $sector['id']; ?>'><?php echo $sector['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="business_type" class="col-sm-4 form-label">Type of Business</label>
                            <div class="col-sm-8">
                                <div class="select-style">
                                    <select class="form-control form-input" id="business_type" name="business_type" placeholder="Select Business Type">
                                        <?php foreach ($types as $type) { ?>
                                            <option value='<?php echo $type['id']; ?>'><?php echo $type['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="fb-url" class="col-sm-4 form-label">Facebook</label>
                            <div class="col-sm-8">
                                <input type="text" name="fb_url" class="form-control form-input" id="fb-url" placeholder="Facebook URL">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="linkedin-url" class="col-sm-4 form-label">Linkedin</label>
                            <div class="col-sm-8">
                                <input type="text" name="linkedin_url" class="form-control form-input" id="linkedin-url" placeholder="Linkedin URL">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="twitter-handle" class="col-sm-4 form-label">Twitter</label>
                            <div class="col-sm-8">
                                <input type="text" name="twitter_handle" class="form-control form-input" id="twitter-handle" placeholder="Twitter handle">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 form-label">Company Logo</label>
                            <div class="col-sm-8">
                                <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput">
                                    <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                                    <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="image"></span>
                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <input type="button" id="company-next" class="join-btn pull-right" value="Save" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12"><hr></div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="lato-bold pull-left section-head">Team Summary<span class="glyphicon glyphicon-chevron-down dropicon"></span></div>
                    </div>
                </div>
                
                <div class="container team-info section wrap" style="display: none;">
                    <div class="row">
                        <div class="col-sm-7 col-sm-offset-2">
                            <div class="row">
                                <div class="form-group">
                                    <label for="team-summary" class="col-sm-4 form-label">Team Summary</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control form-input form-area" id="team-summary" name="team_summary" placeholder=""></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-1 pull-right">
                            <span class="glyphicon glyphicon-cog"></span>
                            <div class="privacy-options" style="display: none;">
                                <label >Visible to</label>
                                <select name="team">
                                    <option value="0">Public</option>
                                    <option value="1">All Investors & Partners</option>
                                    <option  value="2">Pledged Investors</option>
                                    <option  value="3">Followers</option>
                                    <option  value="4">On Request</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="display: none;">
                    <div class="col-sm-6">
                        <input type="button" id="team-next" class="join-btn pull-right" value="Save" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12"><hr></div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="lato-bold pull-left section-head">Business<span class="glyphicon glyphicon-chevron-down dropicon"></span></div>
                    </div>
                </div>

                <div class="container business-info section wrap" style="display: none;">
                    <div class="row">
                        <div class="col-sm-7 col-sm-offset-2">
                            <div class="row">
                                <div class="form-group">
                                    <label for="products-services" class="col-sm-4 form-label">Business Description</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control form-input form-area" id="products-services" name="products_services" placeholder=""></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group">
                                    <label for="how-different" class="col-sm-4 form-label">Unique Selling Proposition</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control form-input form-area" id="how-different" name="how_different" placeholder=""></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-1 pull-right">
                            <span class="glyphicon glyphicon-cog"></span>
                            <div class="privacy-options" style="display: none;">
                                <label >Visible to</label>
                                <select name="business">
                                    <option value="0">Public</option>
                                    <option value="1">All Investors & Partners</option>
                                    <option  value="2">Pledged Investors</option>
                                    <option  value="3">Followers</option>
                                    <option  value="4">On Request</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="row" style="display: none;">
                    <div class="col-sm-6">
                        <input type="button" id="business-next" class="join-btn pull-right" value="Save" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12"><hr></div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="lato-bold pull-left section-head">Revenue Model<span class="glyphicon glyphicon-chevron-down dropicon"></span></div>
                    </div>
                </div>
                <div class="container revenue-info section wrap" style="display: none;">
                    <div class="row">
                        <div class="col-sm-7 col-sm-offset-2">
                            <div class="row">
                                <div class="form-group">
                                    <label for="how-we-make-money" class="col-sm-4 form-label">Revenue Model</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control form-input form-area" id="how-we-make-money" name="how_we_make_money" placeholder=""></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group">
                                    <label for="customer-traction" class="col-sm-4 form-label">Revenue Traction Summary</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control form-input form-area" id="customer-traction" name="customer_traction" placeholder=""></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-1 pull-right">
                            <span class="glyphicon glyphicon-cog"></span>
                            <div class="privacy-options" style="display: none;">
                                <label >Visible to</label>
                                <select name="monetization">
                                    <option value="0">Public</option>
                                    <option value="1">All Investors & Partners</option>
                                    <option  value="2">Pledged Investors</option>
                                    <option  value="3">Followers</option>
                                    <option  value="4">On Request</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="display: none;">
                    <div class="col-sm-6">
                        <input type="button" id="revenue-next" class="join-btn pull-right" value="Save" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12"><hr></div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="lato-bold pull-left section-head">Market<span class="glyphicon glyphicon-chevron-down dropicon"></span></div>
                    </div>
                </div>
                <div class="container market-info section wrap" style="display: none;">
                    <div class="row">
                        <div class="col-sm-7 col-sm-offset-2">
                            <div class="row">
                                <div class="form-group">
                                    <label for="addressable-market" class="col-sm-4 form-label">Total Addressable Market</label>
                                    <div class="col-sm-8">
                                        <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                        <input type="text" class="form-control form-input rupee-input" id="addressable-market" name="addressable_market" placeholder="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label for="competition" class="col-sm-4 form-label">Competition</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control form-input form-area" id="competition" name="competition" placeholder=""></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-1 pull-right">
                            <span class="glyphicon glyphicon-cog"></span>
                            <div class="privacy-options" style="display: none;">
                                <label >Visible to</label>
                                <select name="market">
                                    <option value="0">Public</option>
                                    <option value="1">All Investors & Partners</option>
                                    <option  value="2">Pledged Investors</option>
                                    <option  value="3">Followers</option>
                                    <option  value="4">On Request</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="display: none;">
                    <div class="col-sm-6">
                        <input type="button" id="market-next" class="join-btn pull-right" value="Save" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12"><hr></div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="lato-bold pull-left section-head">Raise<span class="glyphicon glyphicon-chevron-down dropicon"></span></div>
                    </div>
                </div>
                <div class="container raise-info section wrap" style="display: none;">
                    <div class="row">
                        <div class="col-sm-7 col-sm-offset-2">

                            
                            
                            <div class="row">
                                <div class="form-group">
                                    <label for="investment-required" class="col-sm-4 form-label">Investment Required</label>
                                    <div class="col-sm-8">
                                        <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                        <input  class="form-control form-input rupee-input" id="investment-required" name="investment_required" placeholder="Ex. 5,00,00,000">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label for="commitment-per-investor" class="col-sm-4 form-label">Minimum Commitment Per User</label>
                                    <div class="col-sm-8">
                                        <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                        <input  class="form-control form-input rupee-input" id="commitment-per-investor" name="commitment_per_investor" placeholder="Ex. 5,00,000">
                                        
                                    </div>
                                </div>
                            </div>  
                            <div class="row">
                                <div class="form-group">
                                    <label for="equity-offered" class="col-sm-4 form-label">Equity Offered</label>
                                    <div class="col-sm-3">

                                        <input  class="form-control form-input" id="equity-offered" name="equity_offered" maxlength="2" placeholder="">
                                        <span class="col-sm-3 input-placeholder   pull-right">%</span>


                                    </div>

                                    <label for="validity-period" class="col-sm-2 form-label">Validity Period</label>
                                    <div class="col-sm-3">

                                        <input  class="form-control form-input" id="validity-period" name="validity_period" placeholder="">
                                        <span class="col-sm-3 input-placeholder   pull-right calender"></span>


                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label for="funding_history" class="col-sm-4 form-label">Prior Fund Raise History</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control form-input form-area" id="funding_history" name="funding_history" placeholder=""></textarea>
                                    </div>
                                </div>
                            </div>

                            
                            <fieldset>
                                <legend>Use of Funds</legend>
                                <div id="fund-uses">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <input type="text" name="purpose[]" class="form-control form-input"  placeholder="Purpose">
                                            </div>
                                            <div class="col-sm-6">
                                                <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                                <input type="text" name="purpose_amount[]" class="form-control form-input rupee-input"  placeholder="Amount Ex. 10,00,000 ">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <input type="text" name="purpose[]" class="form-control form-input"  placeholder="Purpose">
                                            </div>
                                            <div class="col-sm-6">
                                                <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                                <input type="text" name="purpose_amount[]" class="form-control form-input rupee-input"  placeholder="Amount Ex. 10,00,000 ">
                                     
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <input type="text" name="purpose[]" class="form-control form-input"  placeholder="Purpose">
                                            </div>
                                            <div class="col-sm-6">
                                                <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                                <input type="text" name="purpose_amount[]" class="form-control form-input rupee-input"  placeholder="Amount Ex. 10,00,000 ">
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <input type="text" name="purpose[]" class="form-control form-input"  placeholder="Purpose">
                                            </div>
                                            <div class="col-sm-6">
                                                <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                                <input type="text" name="purpose_amount[]" class="form-control form-input rupee-input"  placeholder="Amount Ex. 10,00,000 ">
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <input type="text" name="purpose[]" class="form-control form-input"  placeholder="Purpose">
                                            </div>
                                            <div class="col-sm-6">
                                                <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                                <input type="text" name="purpose_amount[]" class="form-control form-input rupee-input"  placeholder="Amount Ex. 10,00,000 ">
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--                        <div class="btn join-btn col-sm-12 pull-right add-purpose-btn">Add More</div>-->
                            </fieldset>    


                        </div>
                        <div class="col-sm-1 pull-right">
                            <span class="glyphicon glyphicon-cog"></span>
                            <div class="privacy-options" style="display: none;">
                                <label >Visible to</label>
                                <select name="raise">
                                    <option value="0">Public</option>
                                    <option value="1">All Investors & Partners</option>
                                    <option  value="2">Pledged Investors</option>
                                    <option  value="3">Followers</option>
                                    <option  value="4">On Request</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="display: none;">
                    <div class="col-sm-6">
                        <input type="button" id="raise-next" class="join-btn pull-right" value="Save" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12"><hr></div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="lato-bold pull-left section-head">Financials<span class="glyphicon glyphicon-chevron-down dropicon"></span></div>
                    </div>
                </div>
                <div class="container financial-info section wrap" style="display: none;">
                    <div class="row">
                        <div class="col-sm-7 col-sm-offset-2">
                            <fieldset>
                                <legend>Monthly Financial Indicators</legend>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="monthly-revenue" class="col-sm-4 form-label">Revenues</label>
                                        <div class="col-sm-8">
                                            <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                            <input type="text" name="monthly_revenue" class="form-control form-input rupee-input" id="monthly-revenue" placeholder="Amount Ex. 10,00,000 ">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="fixed-opex" class="col-sm-4 form-label">Fixed Cost (OPEX)</label>
                                        <div class="col-sm-8">
                                            <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                            <input type="text" name="fixed_opex" class="form-control form-input rupee-input" id="fixed-opex" placeholder="Amount Ex. 10,00,000 ">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="cash-burn" class="col-sm-4 form-label">Cash Burn</label>
                                        <div class="col-sm-8">
                                            <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                            <input type="text" name="cash_burn" class="form-control form-input rupee-input" id="cash-burn" placeholder="Amount Ex. 10,00,000 ">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <label for="debt" class="col-sm-4 form-label">Debt</label>
                                        <div class="col-sm-8">
                                            <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                            <input type="text" name="debt"  class="form-control form-input rupee-input" id="debt" placeholder="Amount Ex. 10,00,000 ">
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col-sm-1 pull-right">
                            <span class="glyphicon glyphicon-cog"></span>
                            <div class="privacy-options" style="display: none;">
                                <label >Visible to</label>
                                <select name="financials">
                                    <option value="0">Public</option>
                                    <option value="1">All Investors & Partners</option>
                                    <option  value="2">Pledged Investors</option>
                                    <option  value="3">Followers</option>
                                    <option  value="4">On Request</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="display: none;">
                    <div class="col-sm-6">
                        <input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
                        <input type="submit" class="join-btn pull-right" name="submit" value="Save"/>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>    
<div style="display: none;">
    <div id="fund-uses-fields">
        <div class="row">
            <div class="form-group">
                <div class="col-sm-6">
                    <input type="text" name="purpose[]" class="form-control form-input"  placeholder="Purpose">
                </div>
                <div class="col-sm-6">
                    <input type="text" name="purpose_amount[]" class="form-control form-input"  placeholder="Amount Ex. 10,00,000 ">
                    <span class="col-sm-2 input-placeholder   pull-right">(INR)</span>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#investee-info-form textarea').htmlarea({
            toolbar: [
                ["bold", "italic", "underline"],
                ["p"],
                ["link", "unlink"],
                ["orderedList", "unorderedList"],
                ["indent", "outdent"],
                ["justifyleft", "justifycenter", "justifyright"]
            ]
        });
        
        $('#validity-period').datetimepicker({
            timepicker:false,
            format:'d-m-Y'
        });

        $('#investee-info-form').on('change', '.country', function(){
            $.ajax({
                url: "<?php echo base_url(); ?>user/states",
                type: 'POST',
                data : {countryId : $(this).val()}
            })
            .done(function( data ) {
                $('#investee-info-form .state').html(data);
            });
        });
        
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");
        
         jQuery.validator.addMethod("rupeeonly", function(value, element) {
            return this.optional(element) || /^[-,0-9]+$/i.test(value);
        }, "Only digits and comma");
        
        var validator =$('#investee-info-form').validate({
            rules: {
                        
                company_name: { required : true,
                    minlength:4
                },
                company_url:'required',
                product_url:'required',
                year:'required',
                country:'required',
                state:'required',
                city:{ required : true,
                    lettersonly:true
                },
                image:'required',
                products_services:'required',
                investment_required: { required : true,
                                       rupeeonly: true
                },
                funding_history:'required',
                commitment_per_investor: { required : true,
                                           rupeeonly: true
                },
                valuation: { required : true,
                    number: true
                },
                equity_offered: { 
                    number: true
                },
                team_summary:'required',
                customer_traction:'required',
                how_different:'required',
                how_we_make_money:'required',
                addressable_market: { 
                    number: true
                },
                competition:'required',
                uses_of_funds:'required',
                costs_margins:'required',
                validity_period:'required'
            }
            ,
            messages: {
                city: {
                    required: "This field is required."
                }
            } 
            ,
            errorPlacement: function (error, element) {
                if($(element).parent().hasClass("select-style")){
                    $(element).parents('.col-sm-8').append(error);
                }else if($(element).attr("type") == 'file'){
                    $(element).parents('.col-sm-8').append(error);
                }else{
                    error.insertAfter(element); 
                } 
            }
        });
        
        $('textarea').keydown(function(event){
            if (event.keyCode == 13) {
                event.preventDefault();
                this.value = this.value + "\n";
            }
        });
        
        $('.add-purpose-btn').on('click',function(){
            $('#fund-uses').append($('#fund-uses-fields').html());
        });
        
//        $('.section-head').on('click',function(){
//            $(this).parents('.row').next('.container').toggle();
//            $(this).parents('.row').next('.container').next('.row').toggle();
//        });
        
        $('.glyphicon-cog').on('click',function(){
            $(this).next('.privacy-options').toggle();
        });
        
        $('.privacy-options select').on('change',function(){
             $.ajax({
                url: "<?php echo base_url(); ?>investee/save_privacy",
                type: 'POST',
                data : {optionVal : $(this).val(),optionKey :$(this).attr('name') }
            })
            .done(function( data ) {
                
            });
        });
        
        $('#company-next').on('click',function(){
            if($('#investee-info-form').valid()){
                $(this).hide();
                $('.investor-register-form .container').hide();
                $('.container.team-info').show();
                $('.container.team-info').next('.row').show();
            }
            
        });
        
        $('#team-next').on('click',function(){
            if($('#investee-info-form').valid()){
                $(this).hide();
                $('.investor-register-form .container').hide();
                $('.container.business-info').show();
                $('.container.business-info').next('.row').show();
            }
            
        });
        
        $('#business-next').on('click',function(){
            if($('#investee-info-form').valid()){
                $(this).hide();
                $('.investor-register-form .container').hide();
                $('.container.revenue-info').show();
                $('.container.revenue-info').next('.row').show();
            }
            
        });
        
         $('#revenue-next').on('click',function(){
            if($('#investee-info-form').valid()){
                $(this).hide();
                $('.investor-register-form .container').hide();
                $('.container.market-info').show();
                $('.container.market-info').next('.row').show();
            }
            
        });
        
        $('#market-next').on('click',function(){
            if($('#investee-info-form').valid()){
                $(this).hide();
                $('.investor-register-form .container').hide();
                $('.container.raise-info').show();
                $('.container.raise-info').next('.row').show();
            }
            
        });
        
        $('#raise-next').on('click',function(){
            if($('#investee-info-form').valid()){
                $(this).hide();
                $('.investor-register-form .container').hide();
                $('.container.financial-info').show();
                $('.container.financial-info').next('.row').show();
            }
            
        });
        
        
    });
</script>