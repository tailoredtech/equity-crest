<div class="container">
    <div class="row">
        <div class="col-xs-12 block-title">
          <h1 class="page-title">Company Information</h1>
        </div>
    </div>
</div>
<div class="container">
    <form id="investee-info-form" action="<?php echo base_url(); ?>user/edit_investee_info_process" enctype="multipart/form-data" method="post" >
     <input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />

        <div>
        <!--Slider Accordion Starts here-->
            <!-- Start Company Info panel -->
            <div class="accordion2">
                <div class="accordion-heading  accordion-opened" >
                    <a class="accordion-toggle <?php echo ($activePanel == '') ? '' : 'collapsed' ?>" data-toggle="collapse" href="#company-info"><h4>Company Info</h4></a>
                </div>
                <div id="company-info" class="accordion-body collapse <?php echo ($activePanel == '') ? 'in' : '' ?>">
                    <div class="accordion-inner">
                        <div class="form-group row">
                            <label for="founder-name" class="col-sm-5 form-label">Founder Name</label>
                            <div class="col-sm-7">
                                <input type="text" name="name" value="<?php echo $user['name']; ?>" class="form-control form-input" id="founder-name" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="sign-in-as" class="col-sm-5 form-label">Name of the Company</label>
                            <div class="col-sm-7">
                                <input type="text" name="company_name" class="form-control form-input" id="company_name" value="<?php echo $user['company_name']; ?>" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="company_url" class="col-sm-5 form-label">Company URL</label>
                            <div class="col-sm-7">
	                            
                                <input type="text" name="company_url" value="<?php echo isset($user['company_url'])? $user['company_url']:''; ?>" class="form-control form-input" id="company_url" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="product_url" class="col-sm-5 form-label">Product Website URL</label>
                            <div class="col-sm-7">
                                <input type="text" name="product_url" value="<?php echo isset($user['product_url'])? $user['product_url']:''; ?>" class="form-control form-input" id="product_url" placeholder="www">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="country" class="col-sm-5 form-label">Country</label>
                            <div class="col-sm-7">
                                <div class="select-style">
                                    <select class="form-control form-input country" name="country"  placeholder="">
                                        <option value="">Select Country</option>
                                        <?php foreach ($countries as $country) { ?>
                                            <option value="<?php echo $country['id']; ?>" <?php echo ($country['id'] == $user['country'] ) ? 'selected' : '' ?>><?php echo $country['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="state" class="col-sm-5 form-label">State</label>
                            <div class="col-sm-7">
                                <div class="select-style">
                                    <select class="form-control form-input state" name="state"  placeholder="">
                                        <?php foreach ($states as $state) { ?>
                                            <option value="<?php echo $state['id']; ?>" <?php echo ($state['id'] == $user['state'] ) ? 'selected' : '' ?>><?php echo $state['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="city" class="col-sm-5 form-label">City</label>
                            <div class="col-sm-7">
                                <input type="text" name="city" value="<?php echo $user['city']; ?>" class="form-control form-input" id="city" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="year" class="col-sm-5 form-label">Founding Year</label>
                            <div class="col-sm-7">
                                <input type="text" name="year" value="<?php echo isset($user['year']) ? $user['year']: ''; ?>" class="form-control form-input" id="year" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="stage" class="col-sm-5 form-label">Stage</label>
                            <div class="col-sm-7">
                                <div class="select-style">
                                    <select class="form-control form-input" id="stage" name="stage" placeholder="Select Startup Stage">
                                        <?php foreach ($stages as $stage) { 
	                                        	
	                                        	$stage_selected = '';
	                                        	
	                                        	if(isset($stage['id']))
	                                        	{
		                                        	if($stage['id'] == $user['stage']) {
			                                        	$stage_selected = 'selected';
		                                        	}
	                                        	}
                                        ?>
                                            <option value='<?php echo isset($stage['id']) ? $stage['id'] : ''; ?>' <?php echo $stage_selected ?>><?php echo $stage['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="sector" class="col-sm-5 form-label">Sector</label>
                            <div class="col-sm-7">
                                <div class="select-style">
                                    <select class="form-control form-input" id="sector" name="sector" placeholder="Select Sector">
                                        <?php foreach ($sectors as $sector) { 
	                                        
	                                        $sector_selected = '';
	                                        	
                                        	if(isset($sector['id']))
                                        	{
	                                        	if($sector['id'] == $user['sector']) {
		                                        	$sector_selected = 'selected';
	                                        	}
                                        	}
	                                        
                                        ?>
                                        
                                        
                                            <option value='<?php echo isset($sector['id']) ? $sector['id'] : ''; ?>' <?php echo $sector_selected;?>><?php echo $sector['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="business_type" class="col-sm-5 form-label">Type of Business</label>
                            <div class="col-sm-7">
                                <div class="select-style">
                                    <select class="form-control form-input" id="business_type" name="business_type" placeholder="Select Business Type">
                                        <?php foreach ($types as $type) { 
	                                        
	                                        $type_selected = '';
	                                        	
                                        	if(isset($type['id']))
                                        	{
	                                        	if($type['id'] == $user['business_type']) {
		                                        	$type_selected = 'selected';
	                                        	}
                                        	}
	                                        
                                        ?>
                                            <option value='<?php echo isset($type['id']) ? $type['id'] : ''; ?>' <?php echo $type_selected; ?>><?php echo $type['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fb-url" class="col-sm-5 form-label">Facebook</label>
                            <div class="col-sm-7">
                                <input type="text" name="fb_url" value="<?php echo $user['fb_url']; ?>" class="form-control form-input" id="fb-url" placeholder="Facebook URL">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="linkedin-url" class="col-sm-5 form-label">Linkedin</label>
                            <div class="col-sm-7">
                                <input type="text" name="linkedin_url" value="<?php echo $user['linkedin_url']; ?>" class="form-control form-input" id="linkedin-url" placeholder="Linkedin URL">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="twitter-handle" class="col-sm-5 form-label">Twitter</label>
                            <div class="col-sm-7">
                                <input type="text" name="twitter_handle" value="<?php echo $user['twitter_handle']; ?>" class="form-control form-input" id="twitter-handle" placeholder="Twitter handle">
                            </div>
                        </div>
                         <div class="form-group row">
                            <label for="image" class="col-sm-5 form-label">Company Logo</label>
                            <div class="col-sm-7" id="company-logo">
                            <?php if($user['image']==''){ ?>
                                <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput">
                                    <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput">
                                    <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                                    <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" id="image" name="image"></span>
                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                                </div>
                            <?php }else{ ?>
                               <img src="<?php echo base_url() ?>uploads/users/<?=$user_id?>/<?=$user['image']?>" height="100px" width="150px">                            
                               <a  href="javascript:void(0)" data-toggle="modal" data-target="#profileimgModal" id="change-user-img">Change</a>


                            <?php } ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <input type="submit" name="company_submit"  class="btn btn-eq-common pull-right" value="Save" />
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
            <!-- End Company Info panel -->

            <!-- Start Team Summary panel -->
            <div class="accordion2">
                <div class="accordion-heading  accordion-opened" >                    
                        <div class="row">
                            <div class="col-xs-8"><a class="accordion-toggle <?php echo ($activePanel == 'team') ? '' : 'collapsed' ?>" data-toggle="collapse" href="#team_summary_div"><h4>Team Summary</h4></a></div>
                            <div class="col-xs-4 pull-right">
                                <span class="glyphicon glyphicon-cog privacy-icon"></span>
                                <div class="privacy-options" style="display:none">
                                    <select name="team" class="form-control form-input">
                                        <option value="0" <?php echo ($privacy['team'] && $privacy['team'] == 0) ? 'selected' : ''; ?>>All</option>
                                            <option value="1" <?php echo ($privacy['team'] && $privacy['team'] == 1) ? 'selected' : ''; ?>>All Investors & Partners</option>
                                            <option  value="2" <?php echo ($privacy['team'] && $privacy['team'] == 2) ? 'selected' : ''; ?>>Pledged Investors</option>
                                            <option  value="3" <?php echo ($privacy['team'] && $privacy['team'] == 3) ? 'selected' : ''; ?>>Followers</option>
                                            <option  value="4" <?php echo ($privacy['team'] && $privacy['team'] == 4) ? 'selected' : ''; ?>>On Request</option>
                                    </select>
                                </div>                                
                            </div>
                        </div>
                </div>
                <div id="team_summary_div" class="accordion-body collapse <?php echo ($activePanel == 'team') ? 'in' : '' ?>">
                    <div class="accordion-inner">
                        <div class="form-group row">
                            <label for="team-summary" class="col-sm-5 form-label">Team Summary</label>
                            <div class="col-sm-7">
                                <textarea class="form-control form-input form-area" cols="50" rows="15" id="team-summary" name="team_summary" placeholder=""><?php echo isset($user['team_summary']) ? $user['team_summary'] : ''; ?></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12">
                                <input type="submit" name="team_submit"  class="btn btn-eq-common pull-right" value="Save" />
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <!-- End Team Summary panel -->

            <!-- Start Business panel -->
            <div class="accordion2">
                <div class="accordion-heading  accordion-opened" >
                    <div class="row">
                        <div class="col-xs-8"> <a class="accordion-toggle <?php echo ($activePanel == 'business') ? '' : 'collapsed' ?>" data-toggle="collapse" href="#business"><h4>Business</h4></a> </div>
                        <div class="col-xs-4 pull-right">
                            <span class="glyphicon glyphicon-cog privacy-icon"></span>
                            <div class="privacy-options" style="display:none">
                                <select name="business" class="form-control form-input" >
                                    <option value="0" <?php if ($privacy['business']) { echo ($privacy['business'] == 0) ? 'selected' : '';} ?>>All</option>
                                    <option value="1" <?php if ($privacy['business']) { echo ($privacy['business'] == 1) ? 'selected' : '';} ?>>All Investors & Partners</option>
                                    <option  value="2" <?php if ($privacy['business']) { echo ($privacy['business'] == 2) ? 'selected' : '';} ?>>Pledged Investors</option>
                                    <option  value="3" <?php if ($privacy['business']) { echo ($privacy['business'] == 3) ? 'selected' : '';} ?>>Followers</option>
                                    <option  value="4" <?php if ($privacy['business']) { echo ($privacy['business'] == 4) ? 'selected' : '';} ?>>On Request</option>
                                </select>
                            </div>                                
                        </div>
                    </div>
                </div>
                <div id="business" class="accordion-body collapse <?php echo ($activePanel == 'business') ? 'in' : '' ?>">
                    <div class="accordion-inner">
                        <div class="form-group row">
                            <label for="products-services" class="col-sm-5 form-label">Business Description</label>
                            <div class="col-sm-7">
                                <textarea class="form-control form-input form-area" cols="50" rows="15"  id="products-services" name="products_services"  placeholder=""><?php echo isset($user['products_services']) ? $user['products_services'] : ''; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="how-different" class="col-sm-5 form-label">Unique Selling Proposition</label>
                            <div class="col-sm-7">
                                <textarea class="form-control form-input form-area" cols="50" rows="15" id="how-different" name="how_different"  placeholder=""><?php echo isset($user['how_different']) ? $user['how_different'] : ''; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <input type="submit" name="business_submit"  class="btn btn-eq-common pull-right" value="Save" />
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
            <!-- End Business panel -->

            <!-- Start Revenue Model panel -->
            <div class="accordion2">
                <div class="accordion-heading  accordion-opened" >
                      <div class="row">
                          <div class="col-xs-8"> <a class="accordion-toggle <?php echo ($activePanel == 'revenue') ? '' : 'collapsed' ?>" data-toggle="collapse" href="#Revenue_Model"><h4>Revenue Model</h4></a> </div>
                          <div class="col-xs-4 pull-right">
                              <span class="glyphicon glyphicon-cog privacy-icon"></span>
                              <div class="privacy-options" style="display:none">
                                <select name="monetization" class="form-control form-input" >
                                    <option value="0" <?php echo ($privacy['monetization'] == 0) ? 'selected' : ''; ?>>All</option>
                                    <option value="1" <?php echo ($privacy['monetization'] == 1) ? 'selected' : ''; ?>>All Investors & Partners</option>
                                    <option  value="2" <?php echo ($privacy['monetization'] == 2) ? 'selected' : ''; ?>>Pledged Investors</option>
                                    <option  value="3" <?php echo ($privacy['monetization'] == 3) ? 'selected' : ''; ?>>Followers</option>
                                    <option  value="4" <?php echo ($privacy['monetization'] == 4) ? 'selected' : ''; ?>>On Request</option>
                                </select>
                              </div>                                
                          </div>
                      </div>
                </div>
                <div id="Revenue_Model" class="accordion-body collapse <?php echo ($activePanel == 'revenue') ? 'in' : '' ?>">
                    <div class="accordion-inner">
                        <div class="form-group row">
                            <label for="how-we-make-money" class="col-sm-5 form-label">Revenue Model</label>
                            <div class="col-sm-7">
                                <textarea class="form-control form-input form-area" id="how-we-make-money" name="how_we_make_money"  placeholder=""><?php echo isset($user['how_we_make_money']) ? $user['how_we_make_money'] : ""; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="customer-traction" class="col-sm-5 form-label">Revenue Traction Summary</label>
                            <div class="col-sm-7">
                                <textarea class="form-control form-input form-area" id="customer-traction" name="customer_traction"  placeholder=""><?php echo isset($user['customer_traction']) ? $user['customer_traction'] : ""; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <input type="submit" name="revenue_submit"  class="btn btn-eq-common pull-right" value="Save" />
                            </div>
                        </div>                    
                    </div>
                </div>
            </div>
            <!-- End Revenue Model panel -->

            <!-- Start Market panel -->
            <div class="accordion2">
                <div class="accordion-heading  accordion-opened">
                      <div class="row">
                          <div class="col-xs-8"> <a class="accordion-toggle <?php echo ($activePanel == 'market') ? '' : 'collapsed' ?>" data-toggle="collapse" href="#Market"><h4>Market</h4></a> </div>
                          <div class="col-xs-4 pull-right">
                              <span class="glyphicon glyphicon-cog privacy-icon"></span>
                              <div class="privacy-options" style="display:none">
                               <select name="market" class="form-control form-input" >
                                    <option value="0" <?php echo ($privacy['market'] == 0) ? 'selected' : ''; ?>>All</option>
                                    <option value="1" <?php echo ($privacy['market'] == 1) ? 'selected' : ''; ?>>All Investors & Partners</option>
                                    <option  value="2" <?php echo ($privacy['market'] == 2) ? 'selected' : ''; ?>>Pledged Investors</option>
                                    <option  value="3" <?php echo ($privacy['market'] == 3) ? 'selected' : ''; ?>>Followers</option>
                                    <option  value="4" <?php echo ($privacy['market'] == 4) ? 'selected' : ''; ?>>On Request</option>
                                </select>
                              </div>                                
                          </div>
                      </div>
                </div>
                <div id="Market" class="accordion-body collapse <?php echo ($activePanel == 'market') ? 'in' : '' ?>">
                    <div class="accordion-inner">
                        <div class="form-group row">
                            <label for="addressable-market" class="col-sm-5 form-label">Total Addressable Market</label>
                            <div class="col-sm-7">
                                <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                <input type="text" class="form-control form-input rupee-input" id="addressable-market" name="addressable_market" value="<?php echo isset($user['addressable_market']) ? rupeeFormat($user['addressable_market']) : ''; ?>" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="competition" class="col-sm-5 form-label">Competition</label>
                            <div class="col-sm-7">
                                <textarea class="form-control form-input form-area" id="competition" name="competition"  placeholder="">
                                	<?php echo isset($user['competition']) ? $user['competition'] : ""; ?>
                                </textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <input type="submit" name="market_submit"  class="btn btn-eq-common pull-right" value="Save" />
                            </div>
                        </div>                          
                    </div>
                </div>
            </div>
            <!-- End Market panel -->

            <!-- Start Raise panel -->
            <div class="accordion2">
                <div class="accordion-heading  accordion-opened" >
                      <div class="row">
                          <div class="col-xs-8 "> <a class="accordion-toggle <?php echo ($activePanel == 'raise') ? '' : 'collapsed' ?>" data-toggle="collapse" href="#Raise"><h4>Raise</h4></a> </div>
                          <div class="col-xs-4 pull-right">
                              <span class="glyphicon glyphicon-cog privacy-icon"></span>
                              <div class="privacy-options" style="display:none">
                                <select name="raise" class="form-control form-input">
                                    <option value="0" <?php echo ($privacy['raise'] == 0) ? 'selected' : ''; ?>>All</option>
                                    <option value="1" <?php echo ($privacy['raise'] == 1) ? 'selected' : ''; ?>>All Investors & Partners</option>
                                    <option value="2" <?php echo ($privacy['raise'] == 2) ? 'selected' : ''; ?>>Pledged Investors</option>
                                    <option value="3" <?php echo ($privacy['raise'] == 3) ? 'selected' : ''; ?>>Followers</option>
                                    <option value="4" <?php echo ($privacy['raise'] == 4) ? 'selected' : ''; ?>>On Request</option>
                                </select>
                              </div>                                
                          </div>
                      </div>
                </div>
                <div id="Raise" class="accordion-body collapse <?php echo ($activePanel == 'raise') ? 'in' : '' ?>">
                    <div class="accordion-inner">
                        <div class="form-group row">
                            <label for="investment-required" class="col-sm-5 form-label">Investment Required</label>
                            <div class="col-sm-7">
                                <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                <input  class="form-control form-input rupee-input" id="investment-required" name="investment_required" value="<?php echo isset($user['investment_required']) ? rupeeFormat($user['investment_required']) : ""; ?>" placeholder="Ex. 5,00,00,000">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="commitment-per-investor" class="col-sm-5 form-label">Minimum Commitment Per User</label>
                            <div class="col-sm-7">
                                <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                <input  class="form-control form-input rupee-input" id="commitment-per-investor" name="commitment_per_investor" value="<?php echo isset($user['commitment_per_investor']) ? rupeeFormat($user['commitment_per_investor']) : ""; ?>" placeholder="Ex. 5,00,000">
                                
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="equity-offered" class="col-sm-5 form-label">Equity Offered</label>
                            <div class="col-sm-7">

                                <input  class="form-control form-input" id="equity-offered" name="equity_offered" value="<?php echo isset($user['equity_offered']) ? $user['equity_offered'] : ""; ?>" maxlength="5" placeholder="">
                                <span class="col-sm-2 input-placeholder   pull-right">%</span>


                            </div>
                        </div>    

                        <div class="form-group row">
                            <label for="validity-period" class="col-sm-5 form-label">Validity Period</label>
                            <div class="col-sm-7">

                                <input  class="form-control form-input" style="padding: 6px" id="validity-period" name="validity_period" value="<?php echo isset($user['validity_period']) ? $user['validity_period'] : ""; ?>" placeholder="">
                                <span class="col-sm-1 input-placeholder   pull-right calender"></span>


                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="funding_history" class="col-sm-5 form-label">Prior Fund Raise History</label>
                            <div class="col-sm-7">
                                <textarea class="form-control form-input form-area" id="funding_history" name="funding_history"  placeholder=""><?php echo isset($user['funding_history']) ? $user['funding_history'] : ""; ?></textarea>
                            </div>
                        </div>

                        <fieldset >
                            <legend >Use of Funds</legend>
                            <div id="fund-uses">
                                <?php
	                           
	                            $purposes_count = isset($purposes) ? count($purposes) : 5;    
                               
                                for ($i = 0; $i < $purposes_count; $i++) {

                                 ?>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <input type="text" name="purpose[]" value="<?php echo isset($purposes[$i]['purpose']) ? $purposes[$i]['purpose'] : ""; ?>" class="form-control form-input"  placeholder="Purpose">
                                    </div>
                                    <div class="col-sm-6">
                                        <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                        <input type="text" name="purpose_amount[]" value="<?php echo (isset($purposes[$i]['amount']) && $purposes[$i]['amount']!='0') ? rupeeFormat($purposes[$i]['amount']) : ""; ?>"  class="form-control form-input rupee-input"  placeholder="Amount Ex. 1000000 ">
                                        
                                    </div>
                                </div>
                            <?php
                                }  ?>
                            <?php $j = 5 - $i;
                            for ($i = 1; $i <= $j; $i++) {
                            ?>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <input type="text" name="purpose[]" class="form-control form-input"  placeholder="Purpose">
                                    </div>
                                    <div class="col-sm-6">
                                        <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                        <input type="text" name="purpose_amount[]" class="form-control form-input rupee-input"  placeholder="Amount Ex. 1000000 ">
                                        
                                    </div>
                                </div>
                            <?php } ?>
                            </div>
                            <!--<div class="btn eq-btn col-sm-12 pull-right add-purpose-btn">Add More</div>-->
                        </fieldset>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <input type="submit" name="raise_submit"  class="btn btn-eq-common pull-right" value="Save" />
                            </div>
                        </div> 
                        
                    </div>
                </div>
            </div>
            <!-- End Raise panel -->

            <!-- Start Financial panel -->
            <div class="accordion2">
                <div class="accordion-heading  accordion-opened" >
                      <div class="row">
                          <div class="col-xs-8"> <a class="accordion-toggle <?php echo ($activePanel == 'financial') ? '' : 'collapsed' ?>" data-toggle="collapse" href="#Financials"><h4>Monthly Financial Indicators</h4></a> </div>
                          <div class="col-xs-4 pull-right">
                              <span class="glyphicon glyphicon-cog privacy-icon"></span>
                              <div class="privacy-options" style="display:none">
                                <select name="financial" class="form-control form-input">
                                    <option value="0" <?php echo ($privacy['financial'] == 0) ? 'selected' : ''; ?>>All</option>
                                    <option value="1" <?php echo ($privacy['financial'] == 1) ? 'selected' : ''; ?>>All Investors & Partners</option>
                                    <option  value="2" <?php echo ($privacy['financial'] == 2) ? 'selected' : ''; ?>>Pledged Investors</option>
                                    <option  value="3" <?php echo ($privacy['financial'] == 3) ? 'selected' : ''; ?>>Followers</option>
                                    <option  value="4" <?php echo ($privacy['financial'] == 4) ? 'selected' : ''; ?>>On Request</option>
                                </select>
                              </div>                                
                          </div>
                      </div>
                </div>
                <div id="Financials" class="accordion-body collapse <?php echo ($activePanel == 'financial') ? 'in' : '' ?>">
                    <div class="accordion-inner">
                        <div class="form-group row">
                            <label for="monthly-revenue" class="col-sm-5 form-label">Revenues</label>
                            <div class="col-sm-7">
                                <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                <input type="text" name="monthly_revenue" value="<?php echo isset($user['monthly_revenue']) ? rupeeFormat($user['monthly_revenue']) : ""; ?>"  class="form-control form-input rupee-input" id="monthly-revenue" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fixed-opex" class="col-sm-5 form-label">Fixed Cost (OPEX)</label>
                            <div class="col-sm-7">
                                <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                <input type="text" name="fixed_opex" value="<?php echo isset($user['fixed_opex']) ? rupeeFormat($user['fixed_opex']) : ""; ?>"  class="form-control form-input rupee-input" id="fixed-opex" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cash-burn" class="col-sm-5 form-label">Cash Burn</label>
                            <div class="col-sm-7">
                                <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                <input type="text" name="cash_burn" value="<?php echo isset($user['cash_burn']) ?  rupeeFormat($user['cash_burn']) : ""; ?>"  class="form-control form-input rupee-input" id="cash-burn" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="debt" class="col-sm-5 form-label">Debt</label>
                            <div class="col-sm-7">
                                <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                <input type="text" name="debt" value="<?php echo isset($user['debt']) ?  rupeeFormat($user['debt']) : ""; ?>"  class="form-control form-input rupee-input" id="debt" placeholder="">
                            </div>
                        </div>
                         <div class="form-group row">
                            <div class="col-sm-12">
                                <input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
                                <input type="submit" class="btn btn-eq-common pull-right" name="financial_submit" value="Save"/>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
            <!-- End Financial panel -->
         
        </div>
        <div class="row">
            <div class="col-sm-offset-5 col-md-3">
                <a href="#" class="btn btn-eq-common" id="next_submit">Next</a>
            </div>
        </div>
    </form>
</div>

<div style="display: none;">
    <div id="fund-uses-fields">
        <div class="row">
            <div class="form-group">
                <div class="col-sm-6">
                    <input type="text" name="purpose[]" class="form-control form-input"  placeholder="Purpose">
                </div>
                <div class="col-sm-6">
                    <input type="text" name="purpose_amount[]" class="form-control form-input"  placeholder="Amount Ex. 1000000 ">
                    <span class="col-sm-2 input-placeholder   pull-right">(INR)</span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- POPUP company logo Change CONTAINER -->
<div class="modal fade" id="profileimgModal" tabindex="-1" role="dialog" aria-labelledby="profileimgModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="profileimgModalLabel">Change Company Logo</h4>
        </div>
        <div class="modal-body">
            <form id="upload-profile-image" action="<?php echo base_url(); ?>user/ajax_upload_image" method="post" enctype="multipart/form-data">
                <div class="form-group">
                        <label class="col-sm-2 form-label">Image</label>
                        <div class="col-sm-10">
                            <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput" style="margin-bottom:0;">
                                <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                                <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="userfile"></span>
                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                            </div>
                            <label id="logo_upload_error" for="question" class="error" style="height: 23px;padding: 0 0 0 1px;white-space: normal;">&nbsp;</label>
                        </div>
                </div>
                <div class="text-right form-group">
                    <input class="btn btn-eq-common" type="submit" value="Submit" />
                </div>
            </form>
        </div>
    </div>
  </div>
</div>
<script>
    $(document).ready(function(){

 $('#next_submit').on('click',function() {  
     $('#investee-info-form').submit();
});
        $('textarea').htmlarea({
            toolbar: [
                ["bold", "italic", "underline"],
                ["p"],
                ["link", "unlink"],
                ["orderedList", "unorderedList"],
                ["indent", "outdent"],
                ["justifyleft", "justifycenter", "justifyright"]
            ]
        });
        $('#validity-period').datetimepicker({
            timepicker:false,
            format:'d-m-Y'
        });

        $('#investee-info-form').on('change', '.country', function(){
            $.ajax({
                url: "<?php echo base_url(); ?>user/states",
                type: 'POST',
                data : {countryId : $(this).val()}
            })
            .done(function( data ) {
                $('#investee-info-form .state').html(data);
            });
        });
        
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");
        
        jQuery.validator.addMethod("rupeeonly", function(value, element) {
            return this.optional(element) || /^[-,0-9]+$/i.test(value);
        }, "Only digits and comma");
        
        $('#investee-info-form').validate({
            rules: {
                        
                company_name: { required : true,
                    minlength:4
                },
                company_url:'required',
                product_url:'required',
                year:'required',
                country:'required',
                state:'required',
                city:{ required : true,
                    lettersonly:true
                },
                image:'required',
                products_services:'required',
                investment_required: { required : true,
                                       rupeeonly: true
                },
                funding_history:'required',
                commitment_per_investor: { required : true,
                                           rupeeonly: true
                },
                valuation: { required : true,
                    number: true
                },
                equity_offered: { 
                    number: true
                },
                team_summary:'required',
                customer_traction:'required',
                how_different:'required',
                how_we_make_money:'required',
                addressable_market: {
                    rupeeonly: true
                },
                competition:'required',
                uses_of_funds:'required',
                costs_margins:'required'
            },
            messages: {
                city: {
                    required: "This field is required."
                }
            } ,
            errorPlacement: function (error, element) {
                if($(element).parent().hasClass("select-style")){
                    $(element).parents('.col-sm-8').append(error);
                }else if($(element).attr("type") == 'file'){
                    $(element).parents('.col-sm-8').append(error);
                }else{
                    error.insertAfter(element); 
                } 
            }
        });
        
        $('textarea').keydown(function(event){
            if (event.keyCode == 13) {
                event.preventDefault();
                this.value = this.value + "\n";
            }
        });
        
        $('.add-purpose-btn').on('click',function(){
            $('#fund-uses').append($('#fund-uses-fields').html());
        });
        
        $('.section-head').on('click',function(){
            $(this).parents('.row').next('.container').toggle();
            //$(this).parents('.row').next('.container').next('.row').toggle();
        });
        
        $('.glyphicon-cog').on('click',function(){
            $(this).next('.privacy-options').toggle();
        });
        
        $('.privacy-options select').on('change',function(){
             $.ajax({
                url: "<?php echo base_url(); ?>investee/save_privacy",
                type: 'POST',
                data : {optionVal : $(this).val(),optionKey :$(this).attr('name') }
            })
            .done(function( data ) {
                
            });
        });

        $("#upload-profile-image").on('submit',(function(e){
            e.preventDefault();
            $.ajax({
                    url: "<?php echo base_url(); ?>user/ajax_image_upload",
                    type: "POST",
                    data:  new FormData(this),
                    dataType :'json',
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(data){
                        
                           if (data.error == ''){
                               $(".modal").modal("hide");  
                               $('#company-logo img').attr('src','<?php echo base_url(); ?>'+data.img);
                               $('#user-img img').attr('src','<?php echo base_url(); ?>'+data.img);
                                                       
                           } else {
                                $('#logo_upload_error').html(data.error);
                           }                        
                    },
                    error: function(){

                    } 
            });         
        }));



    });
</script>