<div id="notificationTitle">Notifications</div>
<?php if (count($notifications) > 0) { ?>
<div id="notificationsBody" class="notifications">
    <?php foreach($notifications as $notification){ ?>
        <div class="notification">
            <a class="notify-url" href="<?php echo base_url().$notification['link']; ?>" >
            <span class="notification-thumb img-notification">
                <?php if (!empty($notification['image'])) { ?>
                    <img src="<?php echo base_url(); ?>uploads/users/<?php echo $notification['sender_id'] ?>/<?php echo $notification['image']; ?>">
                <?php } else { ?>
                    <img src="<?php echo base_url(); ?>uploads/investor.jpg" >
                <?php } ?>
            </span> 
            <span class="notification-text"><?php echo $notification['display_text']; ?></span><br>
            <span class="notification-time"><?php echo date('M, d H:i A',strtotime($notification['date_created'])); ?></span>
            </a>
			<span class="border"></span>
        </div>
    <?php } ?>
</div>        
<div id="notificationFooter"><a href="<?php echo base_url().$this->session->userdata('role')  ?>/notifications">See All</a></div>       
<?php } else {?>
<div id="notificationsBody" class="notifications">
    <div class="notification">
        <span class="notification-text">No notifications yet</span>
        <span class="border"></span>
    </div>
</div>        
<?php } ?>
