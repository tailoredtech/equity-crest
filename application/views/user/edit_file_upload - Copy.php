<div class="main">
    <div class="container">
        <div class="row">
            <h4 class="form-header  col-xs-12 ">Upload</h4>
        </div>
        <form id="investee-file-form" action="<?php echo base_url(); ?>user/edit_file_upload_process" enctype="multipart/form-data" method="post" >
            <div class="container investor-register-form">
                <div class="row">
                    <div class="col-md-12 section-head">
                        <span class="glyphicon glyphicon-chevron-down dropicon"></span><span class="lato-bold">Product Info </span>
                    </div>
                </div>
                <div class="container  col-sm-7 col-sm-offset-2 ">
                    <div class="row">
                        <div class="form-group">
                            <label for="company_presentation" class="col-sm-4 form-label black lato-bold ">Company Presentation</label>
                            <div class="col-sm-8">
                                <textarea rows="30" cols="50" class="form-control form-input form-area" id="company_presentation" name="company_presentation" placeholder="Paste Slideshare Embed Code. Example: <iframe src='//www.slideshare.net/slideshow/embed_code/xxxxxx'></iframe>">
                                <?php echo $user['presentation']; ?>
                                </textarea>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="proposal-video" class="col-sm-4 form-label">Proposal Video</label>
                            <div class="col-sm-8">
                                <input type="text" name="proposal_video" value="<?php echo $user['video_link']; ?>" class="form-control form-input" id="proposal-video" placeholder="Youtube link">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 pull-right">
                    <span class="glyphicon glyphicon-cog privacy-icon"></span>
                    <div class="privacy-options" style="display: none;">
                       
                        <div class="select-style-privacy">
                        <select name="business">
                            <option value="" disabled selected>Visible to</option>
                            <option value="0" <?php echo ($privacy['business'] == 0) ? 'selected' : ''; ?>>View to All</option>
                            <option value="1" <?php echo ($privacy['business'] == 1) ? 'selected' : ''; ?>>All investors & partners</option>
                            <option  value="2" <?php echo ($privacy['business'] == 2) ? 'selected' : ''; ?>>Pledged Investors</option>
                            <option  value="3" <?php echo ($privacy['business'] == 3) ? 'selected' : ''; ?>>Followers</option>
                            <option  value="4" <?php echo ($privacy['business'] == 4) ? 'selected' : ''; ?>>Only Me</option>
                        </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12"><hr></div>
                </div>
                <div class="row">
                    <div class="col-md-12 section-head">
                        <span class="glyphicon glyphicon-chevron-down dropicon"></span><span class="lato-bold ">Financial Info</span>
                    </div>
                </div>
                <div class="container  col-sm-7 col-sm-offset-2 ">
                    <div class="row">
                        <div class="form-group">
                            <label for="financial-forecast" class="col-sm-4 form-label">Financial Forecast</label>
                            <?php if($user['financial_forecast']){ ?>
                                <div class="col-sm-8">
                                    <a href="<?php echo base_url(); ?>uploads/users/<?php echo $user['user_id'] ?>/files/<?php echo $user['financial_forecast'] ?>"><?php echo $user['financial_forecast'] ?></a>
                                    <span class="glyphicon glyphicon-remove remove-file" data-file="financial_forecast"></span>
                                </div>
                            <?php }else{ ?>
                            <div class="col-sm-8">
                                <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput">
                                    <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                                    <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" id="financial-forecast" name="financial_forecast"></span>
                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="financial-statement" class="col-sm-4 form-label">Financial Statements</label>
                            <?php if($user['financial_statement']){ ?>
                                <div class="col-sm-8">
                                    <a href="<?php echo base_url(); ?>uploads/users/<?php echo $user['user_id'] ?>/files/<?php echo $user['financial_statement'] ?>"><?php echo $user['financial_statement'] ?></a>
                                    <span class="glyphicon glyphicon-remove remove-file" data-file="financial_statement"></span>
                                </div>
                            <?php }else{ ?>
                            <div class="col-sm-8">
                                <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput">
                                    <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                                    <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" id="financial-statement" name="financial_statement"></span>
                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="other-statement" class="col-sm-4 form-label">Others (if any )</label>
                             <?php if($user['other']){ ?>
                                <div class="col-sm-8">
                                    <a href="<?php echo base_url(); ?>uploads/users/<?php echo $user['user_id'] ?>/files/<?php echo $user['other'] ?>"><?php echo $user['other'] ?></a>
                                    <span class="glyphicon glyphicon-remove remove-file" data-file="other"></span>
                                </div>
                            <?php }else{ ?>
                            <div class="col-sm-8">
                                <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput">
                                    <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                                    <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" id="other-statement" name="other"></span>
                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>

                </div>
                <div class="col-sm-3 pull-right">
                    <span class="glyphicon glyphicon-cog privacy-icon"></span>
                    <div class="privacy-options" style="display: none;">
                        
                        <div class="select-style-privacy">
                        <select name="financial">
                            <option value="" disabled selected>Visible to</option>
                            <option value="0" <?php echo ($privacy['financial'] == 0) ? 'selected' : ''; ?>>View to All</option>
                            <option value="1" <?php echo ($privacy['financial'] == 1) ? 'selected' : ''; ?>>All investors & partners</option>
                            <option  value="2" <?php echo ($privacy['financial'] == 2) ? 'selected' : ''; ?>>Pledged Investors</option>
                            <option  value="3" <?php echo ($privacy['financial'] == 3) ? 'selected' : ''; ?>>Followers</option>
                            <option  value="4" <?php echo ($privacy['financial'] == 4) ? 'selected' : ''; ?>>Only Me</option>
                        </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12"><hr></div>
                </div>
                <div class="row">
                    <div class="col-md-12 section-head">
                        <span class="glyphicon glyphicon-chevron-down dropicon"></span><span class="lato-bold"> Achievements</span>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-7 col-sm-offset-2">
                            <div class="row">
                                <div class="form-group">
                                    <label for="financial-forecast" class="col-sm-4 form-label">Awards / Recognition</label>
                                    
                                        <div class="col-sm-8">
                                            <textarea class="form-control form-input form-area" cols="50" rows="15" id="awards" name="awards"  placeholder=""><?php echo $user['awards'] ?></textarea>
                                        </div>
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label for="financial-forecast" class="col-sm-4 form-label">Testimonials</label>
                                      <div class="col-sm-8">
                                            <textarea class="form-control form-input form-area" cols="50" rows="15" id="testimonials" name="testimonials"  placeholder=""><?php echo $user['testimonials'] ?></textarea>
                                       </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label for="financial-forecast" class="col-sm-4 form-label">Media Coverage</label>
                                    
                                        <div class="col-sm-8">
                                            <textarea class="form-control form-input form-area" cols="50" rows="15" id="media" name="media" placeholder=""><?php echo $user['media'] ?></textarea>
                                        </div>
                                   
                                </div>
                            </div>
                            <div class="row">
                                <div class="join-now col-sm-4 col-sm-offset-3 pull-left">
                                    <input type="submit" class="join-btn col-sm-12 pull-right" name="submit" value="Submit" />
                                </div>
                                <div class="join-now col-sm-4  pull-left">
                                    <a href="<?php echo base_url(); ?>user/investee_info" class="join-btn col-sm-12" style="padding: 6px 30px;"   >Back</a>
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-3 pull-right">
                            <span class="glyphicon glyphicon-cog privacy-icon"></span>
                            <div class="privacy-options" style="display: none;">
                                
                                <div class="select-style-privacy">
                                <select name="achievements">
                                    <option value="" disabled selected>Visible to</option>
                                    <option value="0" <?php echo ($privacy['achievements'] == 0) ? 'selected' : ''; ?>>View to All</option>
                                    <option value="1" <?php echo ($privacy['achievements'] == 1) ? 'selected' : ''; ?>>All investors & partners</option>
                                    <option  value="2" <?php echo ($privacy['achievements'] == 2) ? 'selected' : ''; ?>>Pledged Investors</option>
                                    <option  value="3" <?php echo ($privacy['achievements'] == 3) ? 'selected' : ''; ?>>Followers</option>
                                    <option  value="4" <?php echo ($privacy['achievements'] == 4) ? 'selected' : ''; ?>>Only Me</option>
                                </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<div style="display: none;">
    <div id="file-input-field">
        <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput">
            <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
            <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" ></span>
            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
      
        $('.glyphicon-cog').on('click',function(){
            $(this).next('.privacy-options').toggle();
        });
        
        $('.privacy-options select').on('change',function(){
             $.ajax({
                url: "<?php echo base_url(); ?>investee/save_privacy",
                type: 'POST',
                data : {optionVal : $(this).val(),optionKey :$(this).attr('name') }
            })
            .done(function( data ) {
                
            });
        });
        
        $('textarea').htmlarea({
            toolbar: [
                ["bold", "italic", "underline"],
                ["p"],
                ["link", "unlink"],
                ["orderedList", "unorderedList"],
                ["indent", "outdent"],
                ["justifyleft", "justifycenter", "justifyright"]
            ]
        });
        
         $("#company_presentation").htmlarea("dispose");
        
        $('#investee-file-form').on('click','.remove-file',function(){
            var file = $(this).attr('data-file');
            var current = $(this).parent('.col-sm-8');
            $('#file-input-field').find('input').attr('name',file);
            $('#file-input-field').find('input').attr('id',file);
            $(current).html($('#file-input-field'));
        });
    });
</script>