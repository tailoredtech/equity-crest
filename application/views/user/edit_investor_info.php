<div class="container">
    <div class="row">
        <div class="col-lg-10">
            <h1 class="page-title">
            <?php if($this->session->userdata('role')!='admin'){ 
                    echo 'My'; 
                 }else{ 
                    echo $user['name'].'&#39s';
                }
                ?> Profile</h1> 
            <?php //echo "<pre>"; print_r($user); echo "activepanel:-".$activePanel; ?>
        </div>
    </div>

    <div class="row common-profile-edit">
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 sidebar">           
           <div class="sidebar-filter-box">
                <div class="sidebar-filter-padding20 my-profile-box">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6 remove_right_padding my-profile-left">
                            <div class="profile-image">
                            <?php if ($user['image']!= '') { ?>
                            <img class="img-circle" src="<?php echo base_url(); ?>uploads/users/<?=$user_id?>/<?=$user['image']?>">                            
                        <?php } else { ?>
                            <img class="img-circle" src="<?php echo base_url(); ?>assets/images/gfxAvatarLarge.jpg">
                        <?php } ?>
                            <a  href="javascript:void(0)" data-toggle="modal" data-target="#profileimgModal" id="change-user-img">Edit</a>
                            </div>
                        </div>

                        <div class="col-sm-6 col-xs-6 my-profile-right">
                            <h3><?php echo $user['name']; ?></h3>
                            <h6><?php //echo $user['role']; ?></h6>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <ul class="my-profile-list">
                                <li class="<?php echo ($activePanel == 'personal-info' || $activePanel == '') ? 'active' : '' ?>" target-tab="#personal-info"><a href="#">Personal Information</a></li>
                                <li class="<?php echo ($activePanel == 'investment') ? 'active' : '' ?>" target-tab="#investment"><a href="#">Investment Interest</a></li>
                                 <!-- <li class="<?php echo ($activePanel == 'startup-portfolio') ? 'active' : '' ?>" target-tab="#startup-portfolio"><a href="#">Portfolio</a></li>
                                <li class="<?php echo ($activePanel == 'interest') ? 'active' : '' ?>" target-tab="#interest"><a href="#">Interest in Mentoring</a></li>     -->                      
                            </ul>
                        </div>
                    </div>

                    <?php if($this->session->userdata('role')=='admin'){ ?>
                    <div class="row" style="border-top: 1px solid #e6e6e6; margin-top: 15px; padding-top: 5px;">                        
                        <form id="investor-update-form" action="<?php echo base_url(); ?>user/investor_info" method="post" >

                                <div class="col-xs-12">
                                    <h4>See profile of:</h4>
                                </div>
                           
                                <div class="col-xs-12 form-group">
                                    <div class="select-style">
                                        <select class="form-control form-input country" name="id" id="id" placeholder="">
                                            <option value="">Select Investor</option>
                                            <?php foreach ($investors_list as $investor) { ?>
                                                <option value="<?php echo $investor['id']; ?>" <?php echo ($investor['id'] == $user_id ? 'selected' : ''); ?> ><?php echo $investor['name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                           
                            
                                <div class="col-sm-12 ">
                                    <input type="submit" name="investor-update-form-submit"  class="btn btn-eq-common pull-right" value="Go" />
                                </div>
                            
                        </form>
                    </div>
                    <?php } ?>
                </div>
           </div>
        </div> <!-- /.sidebar -->

        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 col-main">
            <div class="profile-content-box">
                <form id="investor-info-form" action="<?php echo base_url(); ?>user/edit_investor_info_process" enctype="multipart/form-data" method="post" >
                 <input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
                    <!--Slider Accordion Starts here-->
                    <!-- Start Personal Information panel -->
                    <div class="accordion3">
                       <div class="accordion-heading  accordion-opened" >
                            <a class="accordion-toggle <?php echo ($activePanel == 'personal-info' || $activePanel == '') ? '' : 'collapsed' ?>" data-toggle="collapse" href="#personal-info"><h3>Personal Information</h3></a>
                        </div>
                        <div id="personal-info" class="accordion-body collapse <?php echo ($activePanel == 'personal-info' || $activePanel == '') ? 'in' : '' ?>">
                            <div class="accordion-inner">
                                <div class="profile-content-sub-section">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                            <h5 class="profile-content-sub-title">Personal</h5>
                                        </div>
                                    </div>
                                    <div  class="form-group row">
                                        <label for="sign-in-as" class="col-xs-12 form-label" >Investor type</label>
                                        <div id="radio-inline" class="col-sm-12">
                                            <span class="radio-inline radio">
                                                <input id="radio1" type="radio" name="type"  value="Angel"  <?php echo ($user['investor_type'] == 'Angel') ? 'checked' : ''; ?>>
                                                <label for="radio1">Angel</label>
                                            </span>
                                            <span class="radio-inline radio">
                                                <input id="radio2" type="radio" name="type"  value="Family Office" <?php echo ($user['investor_type'] == 'Family Office') ? 'checked' : ''; ?>>
                                                <label for="radio2">Family Office</label>
                                            </span>
                                            <span class="radio-inline radio">
                                                <input id="radio3" type="radio" name="type"  value="Venture Fund" <?php echo ($user['investor_type'] == 'Venture Fund') ? 'checked' : ''; ?>>
                                                <label for="radio3">Venture Fund</label>
                                            </span>
                                            <span class="radio-inline radio">
                                                <input id="radio4" type="radio" name="type"  value="Angel Fund" <?php echo ($user['investor_type'] == 'Angel Fund') ? 'checked' : ''; ?>>
                                                <label for="radio4">Angel Fund</label>
                                            </span>
                                            <span class="radio-inline radio">
                                                <input id="radio4" type="radio" name="type"  value="Accelarator" <?php echo ($user['investor_type'] == 'Accelarator') ? 'checked' : ''; ?>>
                                                <label for="radio4">Accelarator/ Incubators</label>
                                            </span>
                                            <div class="clear-fix"></div>
                                        </div>
                                    </div>  

                                     <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group row">
                                                        <label for="name" class="col-xs-12 form-label">Name</label>
                                                        <div class="col-xs-12">
                                                            <input type="text" name="name" class="form-control form-input" id="name" value="<?php echo $user['name']; ?>" placeholder="" />
                                                        </div>
                                                    </div>
                                          
                                               <!--  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> -->
                                                    <div class="form-group row">
                                                        <label for="mobilemumber" class="col-xs-12 form-label">Mobile Number</label>
                                                        <div class="col-xs-12">
                                                            <input type="text" name="mobileno" class="form-control form-input" id="mobileno" value="<?php echo $user['mob_no']; ?>" placeholder="" />
                                                        </div>
                                                    </div>
                                                <!-- </div> -->
                                                
                                               <!--  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> -->
                                                    <div class="form-group row">
                                                        <label for="email" class="col-xs-12 form-label">Email</label>
                                                        <div class="col-xs-12">
                                                            <input type="text" name="email" class="form-control form-input" id="email" value="<?php echo $user['email']; ?>" placeholder="" readonly />
                                                        </div>
                                                    </div>
                                                 <!-- </div> -->
                                            

                                             <div class="form-group row">
                                                <label for="linkedin-url" class="col-xs-12 form-label">Linkedin</label>
                                                <div class="col-xs-12">
                                                    <input type="text" name="linkedin_url" value="<?php echo $user['linkedin_url']; ?>" class="form-control form-input" id="linkedin-url" placeholder="Please enter your URL">
                                                </div>
                                             </div>

                                              <div class="form-group row">
                                                <label for="twitter-handle" class="col-xs-12 form-label">Twitter</label>
                                                <div class="col-xs-12">
                                                    <input type="text" name="twitter_handle" value="<?php echo $user['twitter_handle']; ?>" class="form-control form-input" id="twitter-handle" placeholder="Please enter your URL">
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                    </div>                                       

                                  <!--   <div class="row">
                                        <div class="col-lg-6 col-sm-6 col-xs-12">
                                         
                                        </div>
                                        <div class="col-lg-6 col-sm-6 col-xs-12">
                                            <label for="experience" class="col-xs-12 form-label">Experience Summary</label>
                                            <div class="col-xs-12">
                                                <textarea class="form-control form-input form-area" style="height:80px;" name="experience"  id="experience" placeholder=""><?php echo $user['experience']; ?></textarea>
                                            </div>
                                        </div>
                                    </div> -->

                                </div>    <!-- /.profile-content-sub-section -->
                                <div class="profile-content-sub-section">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                         <div class="row">
                                   
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <h5 class="profile-content-sub-title">Company</h5>
                                        </div>
                                    </div>
                                    <div  class="form-group row">
                                        <label for="company_name" class="col-xs-12 form-label">Current Organization </label>
                                        <div class="col-xs-12">
                                            <input type="text" name="company_name" class="form-control form-input" id="company_name" value="<?php echo $user['company_name']; ?>" placeholder="Current Organization or 'Independent Investor'">
                                        </div>
                                    </div>
                                            
                                    <div  class="form-group row">    
                                         <label for="designation" class="col-xs-12 form-label">Designation </label>
                                        <div class="col-xs-12">
                                            <input type="text" name="role" class="form-control form-input" id="role" value="<?php if($user['role']!='0') echo $user['role']; ?>" placeholder="">
                                        </div>
                                    </div>

                                  
                                      <div class="form-group row">
                                       
                                                <label for="city" class="col-xs-12 form-label">City</label>
                                                <div class="col-xs-12">
                                                    <input type="text" name="city" value="<?php echo $user['city']; ?>" class="form-control form-input" id="city" placeholder="">
                                                </div>
                                   
                                    </div>

	                                    <div class="form-group row">
	                                        <label for="country" class="col-xs-12 form-label">Country</label>
	                                        <div class="col-sm-12 select-wrap">
	                                            <div class="select-style">
	                                                <select class="form-control form-input country" name="country" id="country"  placeholder="">
	                                                    <option value="">Select Country</option>
	                                                    <?php foreach ($countries as $country) { ?>
	                                                        <option value="<?php echo $country['id']; ?>" <?php echo ($country['id'] == $user['country'] ) ? 'selected' : '' ?> ><?php echo $country['name']; ?></option>
	                                                    <?php } ?>
	                                                </select>
	                                            </div>
	                                        </div>
	                                    </div>  
	                                    
	                                     <div class="form-group row">
	                                     <label for="state" class="col-xs-12 form-label">State</label>
	                                        <div class="col-xs-12">
	                                            <div class="select-style">
	                                                <select class="form-control form-input state" name="state"  placeholder="">
	                                                    <?php foreach ($states as $state) { ?>
	                                                        <option value="<?php echo $state['id']; ?>" <?php echo ($state['id'] == $user['state'] ) ? 'selected' : '' ?>><?php echo $state['name']; ?></option>
	                                                    <?php } ?>
	                                                </select>
	                                            </div>
	                                      </div>
	                                    </div> 
                                      
                                      </div>
                                </div>                                 
                             

                                    <div class="row">
                                      <div class="row remove_margin">
                                            <div class="col-sm-12">
                                                <input type="submit" name="personal_info_submit"  class="btn btn-eq-common pull-right" value="Save" />
                                            </div>
                                        </div> 
                                    </div>
                                 
                            </div> <!-- /.profile-content-sub-section -->

                                <div class="profile-content-sub-section">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <h5 class="profile-content-sub-title">Change password</h5>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <div class="form-group row">
                                                        <label for="newpassword" class="col-xs-12 form-label">New password</label>
                                                        <div class="col-xs-12">
                                                            <input type="password" name="user_password" class="form-control form-input" id="user_password" value="" placeholder="" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <div class="form-group row">
                                                        <label for="confirmpassword" class="col-xs-12 form-label">Confirm password</label>
                                                        <div class="col-xs-12">
                                                            <input type="password" name="confirm_password" class="form-control form-input" id="confirm_password" value="" placeholder="" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row remove_margin">
                                            <div class="col-sm-12">
                                                <input type="submit" name="account-setting-form-submit"  class="btn btn-eq-common pull-right" value="Save" />
                                            </div>
                                        </div> 
                                    </div>
                                </div> <!-- /.profile-content-sub-section --> 
                            </div>
                        </div>
                    </div>
                    <!-- End Personal Information panel -->    

                    <!-- Start Investment Philosophy panel -->
                    <div class="accordion3">
                        <div class="accordion-heading  accordion-opened" >
                            <a class="accordion-toggle <?php echo ($activePanel == 'investment') ? '' : 'collapsed' ?>" data-toggle="collapse" href="#investment"><h3>Investment Interest</h3></a>
                        </div>
                        <div id="investment" class="accordion-body collapse <?php echo ($activePanel == 'investment') ? 'in' : '' ?>">
                            <div class="accordion-inner">
                                <div class="profile-content-sub-section">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                           <div class="form-group row">
                                                <label for="team-summary" class="col-xs-12 form-label">Sector Of Interest</label>
                                                <div class="col-xs-12">
                                                    <div class="select-style">
                                                        <select class="form-control form-input" id="sector_expertise[]" name="sector_expertise[]"  placeholder="" multiple>
                                                        <?php
                                                            $sector_autocomplete_string = '';
                                                            $sector_expertise = explode(",", $user['sector_expertise']); ?>
                                                            <option value="<?php echo "Sector Agnostic" ?>" <?php echo (in_array('Sector Agnostic', $sector_expertise)) ? 'selected' : ''; ?> ><?php echo "Sector Agnostic" ?></option>
                                                          <?php  foreach ($sectors as $k=>$v) {
                                                               
                                                                $sector_autocomplete_string .= '"' . $v['name'] . '",';

                                                                if($v['name']!='Others' && $v['name']!='Sector Agnostic'){
                                                                ?>
                                                                <option value="<?php echo $v['name']; ?>" <?php echo (in_array($v['name'], $sector_expertise)) ? 'selected' : ''; ?> ><?php echo $v['name']; ?></option>
                                                            <?php
                                                            } } ?>

                                                             <option value="<?php echo "Others" ?>" <?php echo (in_array('Others', $sector_expertise)) ? 'selected' : ''; ?> ><?php echo "Others" ?></option>
                                                            $sector_autocomplete_string = trim($sector_autocomplete_string, ",");
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="startup_portfolio" class="col-xs-12 form-label">Startup Portfolio</label>
                                                <div class="col-xs-12">
                                                    <div class="select-style">
                                                        <select class="form-control form-input" id="startup_portfolio" name="startup_portfolio"  placeholder="">
                                                            <option value="" selected disabled>Select Startup Potfolio...</option>
                                                            <!-- option value="0">Add New...</option -->
                                                            <?php $portfolio_count = count($portfolios); foreach ($portfolios as $portfolio) { ?>
                                                                <option value="<?php echo $portfolio['id'];  ?>"><?php echo $portfolio['name'];  ?></option>
                                                            <?php } ?>
                                                        </select>
                                                        <span id="startup-portfolio-add" class="btn col-sm-2 input-placeholder pull-right">Add</span>
                                                        <span id="startup-portfolio-remove" style="display:none;right: 19px;" class="btn col-sm-2 input-placeholder pull-right">Remove</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="portfolio_container">
                                                <?php $count=0; foreach ($portfolios as $portfolio) { ++$count; ?>
                                                <div class="portfolio-box" portfolio-id="<?php echo $portfolio['id'];  ?>" id="portfolio_<?php echo $portfolio['id'];  ?>" style="display: none;">
                                                    <div class="form-group row">
                                                        <label class="col-xs-12 form-label">Name of Company</label>
                                                        <div class="col-xs-12">
                                                            <input type="text" id="portfolio_name_<?php echo $count;  ?>" name="portfolio_name_<?php echo $count;  ?>" class="form-control form-input" value="<?php echo $portfolio['name'] ?>" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xs-12 form-label">Website URL</label>
                                                        <div class="col-xs-12">
                                                            <input type="text" id="portfolio_url_<?php echo $count;  ?>" name="portfolio_url_<?php echo $count;  ?>" value="<?php echo $portfolio['url'] ?>" class="form-control form-input" placeholder="">
                                                        </div>
                                                    </div>

                                                     <div class="form-group row">
                                                    <div class="col-xs-12">
                                                          <input type="hidden" id="action_<?php echo $count; ?>" id="action_<?php echo $count; ?>" name="action_<?php echo $count; ?>" value="edit" />
                                                        </div>
                                                    </div>
                                             </div>
                                             
                                            <?php } ?>

                                      
                                               
                                                    
                                            </div>

                                            <div class="form-group row portfolio-startup-save pull-right" style="display: none;">
                                                <div class="col-sm-12 ">
                                                    <input type="button" id="startup_portfolio_add_more" name="startup_portfolio_add_more"  class="btn btn-eq-common" value="Add More" />
                                                    <input type="submit" id="startup_portfolio_submit" name="startup_portfolio_submit"  class="btn btn-eq-common" value="Save" style="margin-left:10px;" />
                                                </div>
                                            </div>
                                          
                                        </div>
                                    </div>

                                       <div class="form-group row">
                                                <div class="col-sm-12">
                                                    <input type="submit" name="interest_submit"  class="btn btn-eq-common pull-right" value="Save" />
                                                </div>
                                            </div> 
                                        </div>
                                 
                                    <?php if($this->session->userdata('role')=='admin'){ ?>
                                  <div class="profile-content-sub-section">
                                     <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group row">
                                                        <label for="name" class="col-xs-12 form-label">Cheque Size</label>
                                                      
                                                        <div class="col-xs-12">
                                                            <input type="text" name="cheque_size" class="form-control form-input" id="name" value="<?php echo $user['cheque_size']; ?>" placeholder="" />
                                                        </div>
                                                    </div>


                                                 <div  class="form-group row">
                                                        <label for="preference-as" class="col-xs-12 form-label" >Investor Preference</label>
                                                        <div id="radio-inline" class="col-sm-12">
                                                            <span class="radio-inline radio">
                                                                <input id="radio33" type="radio" name="preference_type"  value="dont_know"  <?php echo ($user['preference_type'] == 'dont_know') ? 'checked' : ''; ?>>
                                                                <label for="radio33">Don't know</label>
                                                            </span>
                                                            <span class="radio-inline radio">
                                                                <input id="radio11" type="radio" name="preference_type"  value="Lead"  <?php echo ($user['preference_type'] == 'Lead') ? 'checked' : ''; ?>>
                                                                <label for="radio11">Lead</label>
                                                            </span>
                                                            <span class="radio-inline radio">
                                                                <input id="radio22" type="radio" name="preference_type"  value="No Lead" <?php echo ($user['preference_type'] == 'No Lead') ? 'checked' : ''; ?>>
                                                                <label for="radio22">No Lead</label>
                                                            </span>
                                                            
                                                            <div class="clear-fix"></div>
                                                        </div>
                                                </div> 

                                                <div class="form-group row">
                                                <label for="preference" class="col-xs-12 form-label">How you got to know about equity crest? </label>
                                                <div class="col-xs-12">
                                                    <div class="select-style">
                                                        <select  class="form-control form-input" id="got_to_know" name="got_to_know"  placeholder="">
                                                            <option value="" selected disabled>Select</option>
                                                            <!-- option value="0">Add New...</option -->
                                                           <!--  <?php // $portfolio_count = count($portfolios); foreach ($portfolios as $portfolio) { ?>
                                                                <option value="<?php echo $portfolio['id'];  ?>"><?php echo $portfolio['name'];  ?></option>
                                                            <?php //} ?> -->
                                                            <option value="Press" <?php echo ($user['got_to_know'] == 'Press' ? "selected" : '') ?> > <?php echo "Press";?> </option>
                                                            <option value="Social Media" <?php echo ($user['got_to_know'] == 'Social Media' ? "selected" : '') ?> ><?php echo "Social Media"; ?></option>
                                                            <option value="Google Search" <?php echo ($user['got_to_know'] == 'Google Search' ? "selected" : '') ?> ><?php echo "Google Search"; ?></option>
                                                            <option value="Channel Partner" <?php echo ($user['got_to_know'] == 'Channel Partner' ? "selected" : '') ?> ><?php echo "Channel Partner/ Reference with name of reference";?></option>
                                                            <option value="Others" <?php echo ($user['got_to_know'] == 'Others' ? "selected" : '') ?> ><?php echo "Others";?></option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                                    <div id="others-info" style="display: <?php echo ($user['got_to_know'] == 'Others' ? "block" : 'none') ?>;">
                                                        <div id="others-field">
                                                           <div class="form-group row">
                                                                 <div class="col-xs-12">
                                                                    <input type="text" id="others-value" name="others-value" class="form-control form-input" value="<?php echo $user['others_value']; ?>" placeholder="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                        <div class="row remove_margin">
                                            <div class="col-sm-12">
                                                <input type="submit" name="account-setting-form-submit"  class="btn btn-eq-common pull-right" value="Save" />
                                            </div>
                                        </div> 
                                            </div>                              
                                              </div>
                                        </div>
                                                        <!-- <span id="startup-portfolio-add" class="btn col-sm-2 input-placeholder pull-right">Add</span>
                                                        <span id="startup-portfolio-remove" style="display:none;right: 19px;" class="btn col-sm-2 input-placeholder pull-right">Remove</span> -->
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                     </div>
                                </div>
                                <?php } ?>

                                             
                                 <input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
                                <input type="hidden" id="portfolio_count" name="portfolio_count" value="<?php echo $count; ?>" />
                            </div>
                                    
                                </div> <!-- /.profile-content-sub-section --> 
                            </div>
                        </div>
                    </div>
                
                </form>
            </div>
        </div>
    </div> <!-- / .col-main -->
    <?php if($this->session->userdata('role')=='admin'){ ?>
    <div class="row pull-right sidebar-filter-padding20">
        <div class="col-sm-12 ">
            <input type="button" id="send_mail" name="send_mail"  class="btn btn-eq-common" value="Send Mail" />
        </div>
    </div>
    <?php } ?>
</div>


<div style="display: none;">
    <div id="portfolio-fields">
        <div class="portfolio-box" id="id-0" portfolio-id="0" >
            <div class="form-group row">
                <label class="col-xs-12 form-label">Name of Company</label>
                <div class="col-xs-12">
                    <input type="text" id="portfolio_name" name="portfolio_name" class="form-control form-input" value="" placeholder="">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-xs-12 form-label">Website URL</label>
                <div class="col-xs-12">
                    <input type="text" id="portfolio_url" name="portfolio_url" value="" class="form-control form-input" placeholder="">
                </div>
            </div>
        </div>
    </div>
</div>
<!---Portfolio change logo popup    -->
<div class="modal fade" id="portfolioimgModal" tabindex="-1" role="dialog" aria-labelledby="portfolioimgModalLabel" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="portfolioModalLabel">Upload Image</h4>
            </div>
            <div class="modal-body">
                <form id="upload-image" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-2 form-label">Image</label>
                        <div class="col-sm-10">
                        <input type="hidden" name="portfolio_id" id="portfolio_id" value="" />
                            <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput" style="margin-bottom:0;">
                                <div class="form-input uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                                <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" id="userfile" name="userfile"></span>
                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                            </div>
                            <label id="profile_upload_error" for="question" class="error" style="height: 23px;padding: 0 0 0 1px;white-space: normal;">&nbsp;</label>
                   	</div>
		    </div>
                    <div class="text-right form-group">
                        <input class="btn btn-eq-common" type="submit" value="Submit" />
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- POPUP profile image Change CONTAINER -->
<div class="modal fade" id="profileimgModal" tabindex="-1" role="dialog" aria-labelledby="profileimgModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="profileimgModalLabel">Change Profile Image</h4>
        </div>
        <div class="modal-body">
            <form id="upload-profile-image" action="<?php echo base_url(); ?>user/ajax_upload_image" method="post" enctype="multipart/form-data">
                <div class="form-group">
                        <input type="hidden" name="id" id="id" value="<?php echo $user_id ?>" />
                        <label class="col-sm-2 form-label">Image</label>
                        <div class="col-sm-10">
                            <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput" style="margin-bottom:0;">
                                <div class="form-input uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                                <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="userfile"></span>
                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                            </div>
                            <label id="profile_upload_error" for="question" class="error" style="height: 23px;padding: 0 0 0 1px;white-space: normal;">&nbsp;</label>
                        </div>
                </div>
                <div class="text-right form-group">
                    <input class="btn btn-eq-common" type="submit" value="Submit" />
                </div>
            </form>
        </div>
    </div>
  </div>
</div>

<?= js('jquery-ui.min.js') ?>
<?= js('tag-it.js') ?>
<script type="text/javascript">
 $('#got_to_know').on('change', function(){
   
    var type = $(this).val();
    
  if(type == 'Others') {
           $('#others-info').css('display',"block");
        }
        else
         {
             $('#others-info').css('display',"none");
         }

      });

    $('document').ready(function(){
        var portfolio_count = '<?php echo $count; ?>';

       //  var  portfolio_count = $('#portfolio_count').val();
        $('.my-profile-list').on('click','li',function(e){
          
            var target=$(this).attr('target-tab');
               e.preventDefault();
            $('.accordion3').find('.accordion-body').removeClass('in');
            $('.accordion3').find('.accordion-heading a').addClass('collapsed');
            $(target).parent().find('.accordion-heading a').removeClass('collapsed');
            $('.accordion3 '+target).addClass('in');

            $("html, body").animate({
                 scrollTop:$(target).offset().top-150,
                 },"slow");
            //$(window).scrollTop($(target).offset().top-150);  

            $(this).addClass('active').siblings().removeClass('active');
           // alert(target);
        });

        $('#investor-info-form').on('change', '.country', function(){
            $.ajax({
                url: "<?php echo base_url(); ?>user/states",
                type: 'POST',
                data : {countryId : $(this).val()}
            })
            .done(function( data ) {
                $('#investor-info-form .state').html(data);
            });
        });
        
        $('#investor-info-form textarea').htmlarea({
            
            toolbar: [
                        ["bold", "italic", "underline"],
                        ["p"],
                        ["link", "unlink"],
                        ["orderedList","unorderedList"],
                        ["indent","outdent"],
                        ["justifyleft", "justifycenter", "justifyright"] 
                    ]
        });
        
        $('#investor-info-form').on('change', '#type', function(){

            var type = $(this).val();
            if(type == 'angel'){
                $('.company-name-input').html($('#company-name-field').html());
                $('.company-name-input').show();
            }else{
                $('.company-name-input').html('');
                $('.company-name-input').hide();
            }
        });
     
      $('#account-setting-form-submit').on('click', function(){
         $("#account-setting-form").valid();

      });
        /*
        $('#investor-info-form').on('submit', function(){
            $.ajax({
                url: "<?php echo base_url(); ?>user/edit_investor_info_process",
                type: 'POST',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false
            }).done(function(data) {
                if (data != 'success'){
                    alert(data);
                }
            });            
        });
        */

        $('#investor-info-form').on('change', '#startup_portfolio', function(){
           
            var portfolioId = $(this).val();
            $('.portfolio-box').hide();
            $('#portfolio_' + portfolioId ).show();
            $('.portfolio-startup-save').show();

            if(portfolioId == '0' || portfolioId == ''){
                $('#startup-portfolio-remove').hide();
                //clear the new form
            } else {
                $('#startup-portfolio-remove').show();
            }
            var portfolio_count = $('#portfolio_count').val();

            $('#id-' + portfolio_count).hide(); 
        });


         $.validator.addMethod('urlCheck', function (value, element) {
                return this.optional(element) ||  /^(http(s)?:\/\/)?([\w]+\.)?linkedin\.com\//.test(value);
            }, "Please enter a valid LinkedIn Url");


    $.validator.addMethod('urlCheck_twitter', function (value, element) {
                return this.optional(element) || /http(s)?:\/\/twitter\.com\/(#!\/)?[a-zA-Z0-9_]+/.test(value);
            }, "Please enter a valid Twitter Url");
         

        $('#investor-info-form').validate({
            rules: {
                name: { required : true,
                    minlength:4
                },
                experience: { required: false},    
                website:{ required: false},
                country:{ required: false},
                state:{ required: false},
                city:{ required: false},
                year:{ required: false},
                company_name:{ required: true},
                about_company:{ required: false},
                linkedin_url: { required: false,
                                urlCheck: true
                            },
                twitter_handle: { required: false,
                                urlCheck_twitter: true
                            },
                'optional_name[]':{ required: false},
                confirm_password:{ 
                                   equalTo: "#user_password"
                                 },

            } ,
            messages: {
                name: {
                    required: "Please provide name",
                    minlength: "Name must be at least 4 characters long"
                },
                experience: {
                    required: "Please enter your experience summary"
                },
                website: {
                    required: "Please provide your website"
                },
                country: {required: 'Please select country' },
                state: {required: 'Please select state'},
                city: {required: 'Please enter city'},
                year:{required: 'Please enter year of establishment'},
                company_name: {required: 'Please enter the current organization'},
                about_company: {required: 'Please enter about your company'},
               password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 6 characters long"
                },
                confirm_password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 6 characters long",
                    equalTo: "Please enter the same password as new"
                },
            },
            errorPlacement: function (error, element) {
                if($(element).parent().hasClass("select-style")){
                    $(element).parents('.select-wrap').append(error);
                }else if($(element).attr("type") == 'file'){
                    $(element).parents('.select-wrap').append(error);
                }else{
                    error.insertAfter(element); 
                } 
            }
        });
        
        
        $('#startup-portfolio-add, #startup_portfolio_add_more').on('click',function(){

            portfolio_count = $('#portfolio_count').val();
            ++portfolio_count;

        prev_portfolio_count = portfolio_count-1;
        $('#startup_portfolio_add_more').on('click',function(){
         
          if((document.getElementById('portfolio_name_'+prev_portfolio_count).value) == "")
          {
            alert("Please enter a company name");
          //  e.preventDefault();
          }
      });
           
       
            //console.log(portfolio_count);
            $('#portfolio_count').val(portfolio_count);
            $('#portfolio-fields').find('input[name*="portfolio_name"]').attr('name','portfolio_name_'+portfolio_count);
            $('#portfolio-fields').find('input[name*="portfolio_url"]').attr('name','portfolio_url_'+portfolio_count);
            $('#portfolio-fields').find('input[name*="portfolio_location"]').attr('name','portfolio_location_'+portfolio_count);
            $('#portfolio-fields').find('select[name*="portfolio_sector"]').attr('name','portfolio_sector_'+portfolio_count);
            $('#portfolio-fields').find('input[name*="portfolio_role"]').attr('name','portfolio_role_'+portfolio_count);
            $('#portfolio-fields').find('input[name*="portfolio_image"]').attr('name','portfolio_image_'+portfolio_count);
            $('#portfolio-fields').find('textarea[name*="portfolio_remarks"]').attr('name','portfolio_remarks_'+portfolio_count);
            

            $('#portfolio-fields').find('input[id*="portfolio_name"]').attr('id','portfolio_name_'+portfolio_count);
            $('#portfolio-fields').find('input[id*="portfolio_url"]').attr('id','portfolio_url_'+portfolio_count);
            $('#portfolio-fields').find('input[id*="portfolio_location"]').attr('id','portfolio_location_'+portfolio_count);
            $('#portfolio-fields').find('select[id*="portfolio_sector"]').attr('id','portfolio_sector_'+portfolio_count);
            $('#portfolio-fields').find('input[id*="portfolio_role"]').attr('id','portfolio_role_'+portfolio_count);
            $('#portfolio-fields').find('input[id*="portfolio_image"]').attr('id','portfolio_image_'+portfolio_count);
            $('#portfolio-fields').find('textarea[id*="portfolio_remarks"]').attr('id','portfolio_remarks_'+portfolio_count);

            $('#portfolio-fields').find('input[name*="id_"]').attr('name','id_'+portfolio_count);
            $('#portfolio-fields').find('input[name*="action_"]').attr('name','action_'+portfolio_count);
            $('#portfolio-fields').find('input[id*="id_"]').attr('id','id_'+portfolio_count);
            $('#portfolio-fields').find('input[id*="action_"]').attr('id','action_'+portfolio_count);
            
 
            $('#portfolio-fields .portfolio-box').attr('portfolio-id',(-1 * portfolio_count));
            $('#portfolio-fields .portfolio-box').attr('id',"portfolio-" + portfolio_count);
//alert($('#portfolio-fields').html());

            $('#portfolio_container').append($('#portfolio-fields').html());


            $('.portfolio-box').hide();
            $('#portfolio-' + portfolio_count ).show();
            $('.portfolio-startup-save').show();            
            $('#startup-portfolio-add').hide();

            $('#startup_portfolio option[value=""]').prop('selected', true);
            $('#startup-portfolio-remove').hide();

       
        
});
       
        $('.portfolio-box').on('click','.change-img',function(){

            var portfolio_id = $(this).parents('.portfolio-box').attr('portfolio-id');
            //portfolio_id = portfolio_id.slice(-1);
            $('#portfolioimgModal').find("input[name*='portfolio_id']").val(portfolio_id);
           // $.colorbox({inline: true, href: '#ch-img', innerWidth: '80%', maxWidth: '475px'});//, innerHeight: '175px'
            //$.colorbox({inline: true, href: '#ch-img', innerWidth: '400px', innerHeight: '175px'});
        });
        
       $("#upload-image").on('submit',(function(e){
            e.preventDefault();
            $.ajax({
                    url: "<?php echo base_url(); ?>investor/ajax_portfolio_image_upload",
                    type: "POST",
                    data:  new FormData(this),
                    dataType :'json',
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(data){
                      //  alert(data);
                        $('#portfolio-image_'+data.id+' img').attr('src','<?php echo base_url(); ?>'+data.img);
                        $(".modal").modal("hide");
                    },
                    error: function(){

                    } 
            });         
        }));
        
        $('#key_points').tagit({
            availableTags: ['Team','Market','Traction','Production','Business Model','Others'],
            minLength: 2
        });
        
        $('#mentoring_sectors').tagit({
            availableTags: [<?php echo $sector_autocomplete_string; ?>],
            minLength: 2
        });

        $('#expertise').tagit({
            allowSpaces : true
        });
        
        $('.section-head').on('click',function(){
            $(this).parents('.row').next('.container').toggle();
            //$(this).parents('.row').next('.container').next('.row').toggle();
            
            if($(this).hasClass('companies')){
                $('.more-portfolio-btn').toggle();
                $('.portfolio-save').toggle();
                if ( $('#add-portfolios .new-portfolio-box' ).length ) {
 
                    $('#add-portfolios .new-portfolio-box').toggle();

                }
            }
        });
        
        $('.company').on('click',function(){
            var company_id = $(this).attr('id');
            company_id = company_id.split('-');
            company_id = company_id[1];
            $('.portfolio-box').hide();
            $('#portfolio-'+company_id).show();
        });
        
        $('#startup-portfolio-remove').on('click',function(){
            var portfolioId = $('#startup_portfolio').val();
            var id = "#portfolio_" + portfolioId;
            $.ajax({
                    url: "<?php echo base_url(); ?>investor/delete_portfolio",
                    type: "POST",
                    data:  {company_id:portfolioId},
                    dataType :'html',
                    success: function(data){
                       $('#startup_portfolio option[value=""]').prop('selected', true);
                       $('#startup-portfolio-remove').hide();
                       $('#startup-portfolio-add').show();
                       $('.portfolio-startup-save').hide();
                       $("#startup_portfolio option[value='" + portfolioId + "']").remove();
                       $('#portfolio_' + portfolioId ).hide();
                    },
                    error: function(){

                    } 
            });
        });
        
        $('#add-portfolios').on('click','.cancle-portfolio-btn',function(){
              $(this).parents('.new-portfolio-box').remove();
              portfolio_count--;
              $('#portfolio_count').val(portfolio_count);
        });

           $("#upload-profile-image").on('submit',(function(e){
            e.preventDefault();
            $.ajax({
                    url: "<?php echo base_url(); ?>user/ajax_image_upload",
                    type: "POST",
                    data:  new FormData(this),
                    dataType :'json',
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(data){
                        
                           if (data.error == ''){
                               $('.profile-image img').attr('src','<?php echo base_url(); ?>'+data.img);
                               if(data.role!='admin')
                               {
                                    $('#user-img img').attr('src','<?php echo base_url(); ?>'+data.img);
                                }

                                $('.profile-content-box').before('<div class="alert alert-success fade-in"><i class="fa fa-check-circle"></i> ' + 'Profile Picture Updated Successfully' + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                                $(".modal").modal("hide"); 


                           } else {
                                   
                                  $(".modal").modal("hide");
                            $('.profile-content-box').before('<div class="alert alert-danger fade-in"><i class="fa fa-check-circle"></i> ' + 'Please Upload an Image upto 1MB in Size' + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                                $('#profile_upload_error').html(data.error);
                           } 

                         
                           fade_alert();
                                                
                    },
                    error: function(){

                    } 
            });         
        }));

        $("#send_mail").on('click',(function(e){
            e.preventDefault();
            $.ajax({
                    url: "<?php echo base_url(); ?>admin/update_investor_mail",
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(data){
                            alert(data);                       
                    },
                    error: function(){

                    } 
            });         
        }));


         function fade_alert()
             {
              window.setTimeout(function() {
                $(".alert").fadeTo(1000, 0).slideUp(500, function(){
                    $(this).remove();
                });
              }, 5000);
             }      
        
});
</script>

