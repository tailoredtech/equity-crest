<?php parse_str($_SERVER['QUERY_STRING'],$_GET);
    $user = '';
    if(isset($_GET['user']) && $_GET['user'] != ''){
       $user = $_GET['user'];
    }
?>

<!-- POPUP REGISTER -->
<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">        
                <h4 class="modal-title text-center" id="registerModalLabel">Register an account</h4>
            </div>
            <div class="modal-body">
                <div class="register-box"> 
                    <div class="row remove_margin failure-msg" style="display:none">
                    </div>
                    <div class="register-box-innner register-box-1">
                        <div class="row">
                            <div  class="form-group">
                                <label for="sign-in-as" class="col-xs-12 col-sm-12 join-label text-center">Join Equity Crest as a</label>
                                <ul id="radio-inline" class="col-sm-12">
                                    <li>
                                        <a href="<?php echo base_url().'user/register_user?role=investor';?>">
                                            <span class="radio-inline radio joinInvestor"></span>
                                            <span class="join-text">Investor</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url().'user/register_user?role=investee';?>" >
                                            <span class="radio-inline radio joinStartUp"></span>
                                            <span class="join-text">Start Up</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url().'user/register_user?role=channel_partner';?>">
                                            <span class="radio-inline radio joinPartner"></span>
                                            <span class="join-text">Partner</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url().'user/register_user?role=media';?>">
                                            <span class="radio-inline radio joinMedia"></span>
                                            <span class="join-text">Media</span>
                                        </a>
                                    </li>
                                    <!-- div class="clear-fix"></div -->
                                </li>
                            </div>
                        </div>
                    </div> <!-- /.register-box-1 -->                    
                </div>  <!-- /.register-box -->
            </div> <!-- /.modal-body -->
        </div>
    </div>
</div>

<?php echo js('icheck.min.js')?>