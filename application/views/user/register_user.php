<?php 
if(null != ($this->input->get('entrepreneur')) && $this->input->get('entrepreneur') =='1')
{
    $entrepreneur = true;
}
else
$entrepreneur = false;
if($role != "investee") { ?>
<div id="registerUserModal">
    <div class="container">
        <div class="row">
            <div class="col-lg-offset-2 col-lg-8">
                <h3 class="text-center">Register an account</h3>
                <h4 class="modal-title text-center" id="registerUserModalLabel"></h4>
                <div class="row remove_margin failure-msg" style="display:none">
                    </div>
                <form method="post" id="register-form" >
                    <input type="hidden" name="role" id="role" value="<?php echo $role; ?>">
                    <div class="register-box-innner register-box-2">
                        <div class="row">
                            
                            <div id="name-fields">
                               
                            </div>

                            <div class="col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <label for="inputEmail3" class="register-field-label">Mobile No *</label>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <input type="text" name="mobileno" class="form-control form-input customphone " id="mobile" placeholder="" >
                                </div>
                            </div>

                            <div class="col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <label for="inputEmail3" class="register-field-label">Email Id *</label>                                
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <input type="text" name="email" class="form-control form-input" id="email" placeholder="" >                                    
                                </div>
                            </div>

                            <div class="col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <label for="inputEmail3" class="register-field-label">Password *</label>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <input type="password" name="user_password" class="form-control form-input" id="user_password" placeholder="">
                                </div>
                            </div>

                            <div class="col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <label for="inputEmail3" class="register-field-label">Confirm Password *</label>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <input type="password" name="confirm_password" class="form-control form-input" id="confirm-password" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="row register-agree">
                            <div  id="checkbox" class="col-sm-12 text-center">
                                <div class="checkbox" style="margin-top:0">
                                    <label class="terms_and_condition text-center">
                                        <input type="checkbox" name="agree" >
                                        I agree to EQ <a target="_blank" href="<?php echo base_url(); ?>home/terms_of_use" class="form-link">Terms of Use</a>, <a target="_blank" href="<?php echo base_url(); ?>home/privacy_policy" class="form-link">Privacy policy</a> & <a target="_blank" href="<?php echo base_url(); ?>home/nda" class="form-link">NDA</a>
                                    </label>
                                </div>                                    
                            </div>
                        </div>
                        <div class="row remove_margin register-agree text-center">

                            


                            <div class="join-now">
                                <input type="submit" id="register_submit" value="<?php "Register and Login Now" ?>" name="submit" class="btn btn-eq-common col-sm-4 col-sm-offset-4" >
                            </div> 
                        </div>
                        <div class="row text-center">
                            <div class="col-xs-12">
                                <div class="terms_and_condition text-center">

                           
                                        <a href="<?php echo base_url()?>user/login">Already Registered? Click here to Login</a><br />
                                        
                                   
                                </div>
                            </div>
                        </div> 
                    </div> <!-- /.register-box-2 -->            
                </form>
            </div>
        </div>
    </div>
</div>

<div style="display: none;">
    <div id="investor-name-fields">
        <div id="name-fields">
            <div class="col-sm-3 col-xs-12">
                <div class="form-group">
                    <label for="investor_type" class="register-field-label">Investor Type *</label>
                </div>
            </div>
        </div>
        <div id="name-fields">
            <div class="col-sm-3 col-xs-12">
                <div class="form-group">
                    <select name="investor_type" class="form-control form-input" id="investor_type">
                    <option value="">Select Type</option>
                        <option value="Angel">Angel</option>
                        <option value="Family Office">Family Office</option>
                        <option value="Venture Fund">Venture Fund</option>
                        <option value="Angel Fund">Angel Fund</option>
                        <option value="Accelarator">Accelarator/ Incubators</option>
                    </select>
                </div>
            </div>
        </div>
        <div id = "inv_company-name-fields" style="display: none;">
            <div class="col-sm-3 col-xs-12">
                <div class="form-group">
                    <label for="name" class="register-field-label">Company Name *</label>
                </div>
            </div>
            <div class="col-sm-3 col-xs-12">
                <div class="form-group">
                    <input type="text" name="company_name" class="form-control form-input" id="company-name" placeholder="" required>
                </div>
            </div>
        </div>

        <div class="col-sm-3 col-xs-12">
            <div class="form-group">
                <label for="name" class="register-field-label">Full Name *</label>
            </div>
        </div>
        <div class="col-sm-3 col-xs-12">
            <div class="form-group">
                <input type="text" name="name" class="form-control form-input" id="name" placeholder="" required>
            </div>
        </div>

        <div class="col-sm-3 col-xs-12">
            <div class="form-group">
                <label for="linkedin_url" class="register-field-label">LinkedIn URL</label>
            </div>
        </div>
        <div class="col-sm-3 col-xs-12">
            <div class="form-group">
                <input type="text" name="linkedin_url" class="form-control form-input" id="linkedin_url" >                                   
            </div>
        </div>
    </div>

    <div id="channel-name-fields">
        <div class="col-sm-3 col-xs-12">
            <div class="form-group">
                <label for="company-name" class="register-field-label">Company Name *</label>                                        
            </div>
        </div>
        <div class="col-sm-3 col-xs-12">
            <div class="form-group">
                <input type="text" name="company_name" class="form-control form-input" id="company-name" placeholder="" required>
            </div>
        </div>

        <div class="col-sm-3 col-xs-12">
            <div class="form-group">
                <label for="name" class="register-field-label">Representative Name *</label>
            </div>
        </div>
        <div class="col-sm-3 col-xs-12">
            <div class="form-group">
                <input type="text" name="name" class="form-control form-input" id="name" placeholder="" required>
               
            </div>
        </div>
    </div>

    <div id="media-name-fields">
        <div class="col-sm-3 col-xs-12">
            <div class="form-group">
                <label for="company-name" class="register-field-label">Company Name *</label>
            </div>
        </div>
        <div class="col-sm-3 col-xs-12">
            <div class="form-group">
                <input type="text" name="company_name" class="form-control form-input" id="company-name" placeholder="" required>
            </div>
        </div>

        <div class="col-sm-3 col-xs-12">
            <div class="form-group">
                <label for="name" class="register-field-label">Representative Name *</label>
            </div>
        </div>
        <div class="col-sm-3 col-xs-12">
            <div class="form-group">
                <input type="text" name="name" class="form-control form-input" id="name" placeholder="" required>
            </div>
        </div>
    </div>
</div>
<?php }
else
{?>
<div id="registerStartUpModal">
    <div class="container">
        <div class="row">
            <div class="col-lg-offset-2 col-lg-8">

                <?php if($entrepreneur){ ?>
                 <h3 class="text-center">Start-up Evaluation Questionnaire</h3>
                 <?php } else { ?>
                <h3 class="text-center">Register an account</h3>
                 <?php } ?>
                <h4 class="modal-title text-center" id="registerStartUpModalLabel">Join Equity Crest as a Startup
                    <?php if($entrepreneur){ ?>  and get an assessment for your fund raise
                     <?php } ?>
                </h4>
                <div class="row remove_margin failure-msg" style="display:none">
                    </div>
                <form id="register-startup-form" method="post" enctype="multipart/form-data">
                <input type="hidden" name="role" id="role" value="investee">
                <p>Personal</p>
                <div class="row">
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <label for="founder_name" class="register-field-label">Founder Name *</label>                                        
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <input type="text" name="name" class="form-control form-input" id="founder_name" placeholder="" required>
                        </div>
                    </div>

                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <label for="mobile_number" class="register-field-label">Mobile Number *</label>                                        
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <input type="text" name="mobileno" class="form-control form-input" id="mobile_number" placeholder="" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <label for="email_id" class="register-field-label">Email ID *</label>                                        
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <input type="text" name="email" class="form-control form-input" id="email_id" placeholder="" required>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <label for="linkedin_url" class="register-field-label">Linkedin URL *</label>                                        
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <input type="text" name="linkedin_url" class="form-control form-input" id="linkedin_url" placeholder="" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <label for="user_password" class="register-field-label">Password *</label>                                        
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <input type="password" name="user_password" class="form-control form-input" id="user_password" placeholder="" required>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <label for="confirm_password" class="register-field-label">Confirm Password *</label>                                        
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <input type="password" name="confirm_password" class="form-control form-input" id="confirm_password" placeholder="" required>
                        </div>
                    </div>
                </div>
                <hr />
                <p>Co-founders</p>
                <div id="divTeamInfo">
                    <div class="row">
                        <div class="col-sm-3 col-xs-12">
                            <div class="form-group">
                                <label for="team_name" class="register-field-label">Name</label>                                        
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-12">
                            <div class="form-group">
                                <input type="text" name="team_name[]" class="form-control form-input" id="team_name" placeholder="">
                            </div>
                        </div>

                        <div class="col-sm-3 col-xs-12">
                            <div class="form-group">
                                <label for="team_linked_in[]" class="register-field-label">Linkedin URL</label>                                        
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-12">
                            <div class="form-group">
                                <input type="text" name="team_linked_in[]" class="form-control form-input" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
                <a href="javascript:void(0)" onclick="addTeamData()">Add more</a> | <a href="javascript:void(0)" onclick="removeTeamData()">Delete</a> 
                <hr />
                <p>Business</p>
                <div class="row">
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <label for="company_name" class="register-field-label">Name of the Company *</label>                                        
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <input type="text" name="company_name" class="form-control form-input" id="company_name" placeholder="" required>
                        </div>
                    </div>

                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <label for="city" class="register-field-label">Location *</label>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <input type="text" name="city" class="form-control form-input" id="city" placeholder="" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <label for="year" class="register-field-label">Founding Year *</label>                                        
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <input type="text" name="year" class="form-control form-input" id="year" placeholder="" required>
                        </div>
                    </div>
                    
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <label for="company_url" class="register-field-label">Link to Website </label>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <input type="text" name="company_url" class="form-control form-input" id="company_url" placeholder="" >
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <label for="business_problem_being_solved" class="register-field-label">Define the Problem Solved *</label>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <textarea name="business_problem_being_solved" class="form-control form-textarea" id="business_problem_being_solved" placeholder="" required></textarea>
                        </div>
                    </div>

                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <label for="app_link" class="register-field-label">Link to App </label>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <input type="text" name="app_link" class="form-control form-input" id="app_link" placeholder="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <label for="how_different" class="register-field-label">Unique Selling Proposition *</label>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <textarea name="how_different" class="form-control form-textarea" id="how_different" placeholder="" required></textarea>
                        </div>
                    </div>

                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <label for="products_services_business" class="register-field-label">Describe your Product/Service *</label>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <textarea name="products_services_business" class="form-control form-textarea" id="products_services_business" placeholder="" required></textarea>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <label for="market_size_commentary" class="register-field-label">Describe your Target Audience *</label>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <textarea name="market_size_commentary" class="form-control form-textarea" id="market_size_commentary" placeholder="" required></textarea>
                        </div>
                    </div>                  

                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <label for="competition" class="register-field-label">Who are your competitors? *</label>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <textarea name="competition" class="form-control form-textarea" id="competition" placeholder="" required></textarea>
                        </div>
                    </div>                     
                </div>
                <div class="row">
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <label for="customer_traction" class="register-field-label">Traction in last 6/12 months *(any metric)</label>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <textarea name="customer_traction" class="form-control form-textarea" id="customer_traction" placeholder=""></textarea>
                        </div>
                    </div>


                    <div class="col-sm-6 col-xs-12">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="stage" class="register-field-label">Stage of Business *</label>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <select class="form-control form-input valid" id="stage" name="stage" placeholder="Select Startup Stage">
                                        <option value="">Select Startup Stage</option>
                                        <option value="1">Ideation</option>
                                        <option value="2">Proof of Concept</option>
                                        <option value="3">Beta Launched</option>
                                        <option value="4">Early Revenue</option>
                                        <option value="5">Steady Revenue</option>
                                    </select>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="investment_required" class="register-field-label">Proposed fund raise *</label>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <span class="col-sm-1 rupee-placeholder "> <img src="<?php echo base_url();?>assets/images/rupee.png"></span>
                                    <input type="text" name="investment_required" class="form-control form-input rupee-input valid" id="investment_required" placeholder="" required></textarea>
                                </div>
                            </div>
                        </div>
                        
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <label for="hear_about_us" class="register-field-label">How did you hear about us? * </label>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <select class="form-control form-input valid" id="hear_about_us" name="hear_about_us" placeholder="Select how did you hear about us">
                                <option value="">-- Select how did you hear about us --</option>
                                <option value="media">Media</option>
                                <option value="referral">Referral</option>
                                <option value="others">Others</option>
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <label for="investor_deck" class="register-field-label">Investor Deck</label>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <input type="file" name="investor_deck" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div id="divHearAboutUs" style="display:none;">
                        <div class="col-sm-3 col-xs-12">
                            <div class="form-group">
                                <label for="others" class="register-field-label" id="lblHearAboutUs">Others (please specify)</label>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-12">
                            <div class="form-group">
                                <input type="text" name="others" class="form-control form-textarea" id="others" placeholder=""></textarea>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row register-agree">
                    <div  id="checkbox" class="col-sm-12 text-center">
                        <div class="checkbox" style="margin-top:0">
                            <label class="terms_and_condition text-center">
                                <input type="checkbox" name="agree" >
                                I agree to EQ <a target="_blank" href="<?php echo base_url(); ?>home/terms_of_use" class="form-link">Terms of Use</a>, <a target="_blank" href="<?php echo base_url(); ?>home/privacy_policy" class="form-link">Privacy policy</a> & <a target="_blank" href="<?php echo base_url(); ?>home/nda" class="form-link">NDA</a>
                            </label>
                        </div>                                    
                    </div>
                </div>
                <div class="row remove_margin register-agree text-center">
                      <?php if($entrepreneur){ 
                                $btn_value =  "Submit and Register";
                            }
                        else { 
                            $btn_value = "Register and Login Now";
                            } ?>
                    <div class="join-now">
                        <input type="submit" id="register_startup_submit" value="<?php echo isset($btn_value)? $btn_value:"Register and Login Now" ?>" name="submit" class="btn btn-eq-common col-sm-4 col-sm-offset-4" >
                    </div> 
                </div>
                <div class="row text-center">
                    <div class="col-xs-12">
                      
                             <div class="terms_and_condition text-center">

                                    <?php if(empty($entrepreneur)){ ?>
                                        <a href="<?php echo base_url()?>user/login">Already Registered? Click here to Login</a><br />
                                        <?php } ?>
                                        <a href="<?php echo base_url();?>"> Cancel and go back</a> 
                                   
                                </div>
                    </div>
                </div>

                  <?php if($entrepreneur){ ?>
                    <input type="hidden" name="entrepreneur" value="1" />
                  <?php } ?> 
            </form>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<?php echo js('icheck.min.js')?>

<script type="text/javascript">

function addTeamData()
{
    var appendBody = '<div id="addTeamDt'+window.j+'"><div class="row"> <div class="col-sm-3 col-xs-12"> <div class="form-group"> <label for="team_name" class="register-field-label">Name</label> </div> </div> <div class="col-sm-3 col-xs-12"> <div class="form-group"> <input type="text" name="team_name[]" class="form-control form-input" id="team_name" placeholder=""> </div> </div> <div class="col-sm-3 col-xs-12"> <div class="form-group"> <label for="team_linked_in[]" class="register-field-label">Linkedin URL</label> </div> </div> <div class="col-sm-3 col-xs-12"> <div class="form-group"> <input type="text" name="team_linked_in[]" class="form-control form-input" placeholder=""> </div> </div> </div></div>';
        $( "#divTeamInfo" ).append(appendBody);
    window.j = window.j + 1;
}

function removeTeamData()
{
    window.j = window.j - 1;
    $('#addTeamDt'+window.j).remove(); 
}


$(document).ready(function() {

    <?php if($role){
        if($role == 'investor'){ ?>
            $('H4#registerUserModalLabel').text('Join Equity Crest as an Investor');
            $('#name-fields').html($('#investor-name-fields').html());
    <?php }  
        if($role == 'channel_partner'){ ?>
            $('H4#registerUserModalLabel').text('Join Equity Crest as a Partner');
            $('#name-fields').html($('#channel-name-fields').html());
    <?php }
        if($role == 'media'){ ?>
            $('H4#registerUserModalLabel').text('Join Equity Crest as a Media');
            $('#name-fields').html($('#channel-name-fields').html());
    <?php }
    } ?>      
    window.j = 0;
    $('#hear_about_us').change(function() {

        var intype = $(this).val();
        if(intype == 'media'){
            $('#lblHearAboutUs').text('Media (please specify)');
            $('#divHearAboutUs').show();
        } else if(intype == 'referral'){
            $('#lblHearAboutUs').text('Referred By');
            $('#divHearAboutUs').show();
        }  else if(intype == 'others'){
            $('#lblHearAboutUs').text('Others (please specify)');
            $('#divHearAboutUs').show();
        } else {
            $('#lblHearAboutUs').text('');
            $('#divHearAboutUs').hide();
        }
    });

    $('#investor_type').change(function() {
        // alert('here'); 
        var intype = $(this).val();
        if (intype != 'Angel') {
            $('#inv_company-name-fields').show();
        } else {
            $('#inv_company-name-fields').hide();
        }
    });

    $.validator.addMethod('customphone', function(value, element) {
        return this.optional(element) || /^((\+)?[1-9]{1,2})?([-\s\.])?((\(\d{1,4}\))|\d{1,4})(([-\s\.])?[0-9]{1,12}){1,2}$/.test(value);
    }, "Please enter a valid mobile number");

    $.validator.addMethod('urlCheck', function(value, element) {
        return this.optional(element) || /^(http(s)?:\/\/)?([\w]+\.)?linkedin\.com\//.test(value);
    }, "Please enter a valid LinkedIn Url");


    //validate="required:'input[name=investor_type][value != 'Family Office']'";

    $('#register-form').validate({
        rules: {

            role: {
                required: true
            },
            name: {
                required: true,
                minlength: 4
            },
            investor_type: {
                required: true
            },
            company_name: {
                required: {
                    depends: function(element) {
                        return ($('#investor_type').val() != "Family Office" && $('#investor_type').val() != "Angel Fund");
                    }
                }
            },

            linkedin_url: {
                required: false,
                urlCheck: true
            },
            mobileno: {
                required: true,
                customphone: true
            },
            email: {
                required: true,
                email: true
            },
            user_password: {
                required: true,
                minlength: 6
            },
            confirm_password: {
                required: true,
                minlength: 6,
                equalTo: "#user_password"
            },
            agree: {
                required: true
            },
            entrepreneur : {
                required : false,
            }
        },

        messages: {
            role: {
                required: 'Please select your role'
            },
            name: {
                required: "Please provide your full name",
                minlength: "Your full name must be at least 4 characters long"
            },
            investor_type: {
                required: 'Please select Investor Type'
            },
            company_url: {
                required: 'Please enter the company url'
            },
            //  linkedin_url: { required: "Please provide your linkedin Url"},
            mobileno: {
                required: "Please enter your mobile number",
                number: "Please enter a valid mobile number",
                customphone: "Please enter a valid mobile number",
                minlength: "Please enter atleast 10  digit mobile number"
            },
            user_password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 6 characters long"
            },
            confirm_password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 6 characters long",
                equalTo: "Please enter the same password as above"
            },
            email: {
                required: "Please provide your email address",
                email: "Please enter a valid email address"
            },
            agree: {
                required: "Please agree to EQ Terms of Use, Privacy policy & NDA"
            }
        },
        errorPlacement: function(error, element) {
            if ($(element).parent().hasClass("select-style")) {
                $(element).parents('.col-sm-8').append(error);
            } else if ($(element).attr("type") == 'file') {
                $(element).parents('.col-sm-8').append(error);
            } else if ($(element).parent().hasClass("radio-inline")) {
                $('#radio-inline').append(error);
            } else if ($(element).parents("label").hasClass("terms_and_condition")) {
                $('#checkbox').append(error);
            } else {
                error.insertAfter(element);
            }

        }
    });

    $('#register-form').submit(function() {

        if ($("#register-form").valid() == false) {

        } else {
            //  return true;
            $.ajax({
                url: '<?php echo base_url(); ?>user/register_process',
                type: 'post',
                data: $('#register-form').serialize(),
                dataType: 'json',
                beforeSend: function() {
                    $('#register_submit').button('loading');
                },
                complete: function() {
                    $('#register_submit').button('reset');
                },
                success: function(json) {
                    $('.failure-msg').hide();
                    $('.failure-msg').html('');
                    if (json['success']) {
                        location = json['redirect'];
                        //$('.failure-msg').html('<div class="col-xs-12"><p class="alert alert-success login-error-alert text-center" role="alert">'+json['success-msg']+'.</p></div>');
                        $('#register-form')[0].reset();
                    } else {
                        $('.failure-msg').html('<div class="col-xs-12"><p class="alert alert-danger login-error-alert text-center" role="alert">' + json['error-msg'] + '.</p></div>');
                    }
                    $('.failure-msg').show();
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });

        }
        return false;
    });

    $('#register-startup-form').validate({
        rules: {
            role: {
                required: true
            },
            name: {
                required: true,
                minlength: 4
            },
            mobileno: {
                required: true,
                customphone: true
            },
            email: {
                required: true,
                email: true
            },
            location: {
                required: true
            },
            linkedin_url: {
                required: true,
                urlCheck: true
            },
            user_password: {
                required: true,
                minlength: 6
            },
            confirm_password: {
                required: true,
                minlength: 6,
                equalTo: "#register-startup-form #user_password"
            },
            company_name: {
                required: true
            },
            year:{
                required: true,
                number: true,
                minlength: 4
            },
            business_problem_being_solved:{
                required: true
            },
            products_services_business:{
                required: true
            },
            how_different:{
                required: true
            },
            competition:{
                required: true
            },
            market_size_commentary:{
                required: true
            },
            stage:{
                required: true
            },
            investment_required:{
                required: true
            },
            customer_traction:{
                required: true
            },
            hear_about_us:{
                required: true
            },
            agree: {
                required: true
            }
        },

        messages: {
            role: {
                required: 'Please select your role'
            },
            name: {
                required: "Please provide the Founder Name",
                minlength: "Your Founder name must be at least 4 characters long"
            },
            mobileno: {
                required: "Please enter your mobile number",
                number: "Please enter a valid mobile number",
                customphone: "Please enter a valid mobile number",
                minlength: "Please enter atleast 10  digit mobile number"
            },
            email: {
                required: "Please provide your email address",
                email: "Please enter a valid email address"
            },
            linkedin_url: { 
                required: "Please provide your linkedin Url"
            },
            user_password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 6 characters long"
            },
            confirm_password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 6 characters long",
                equalTo: "Please enter the same password as above"
            },
            company_name: {
                required: "Please provide the Company Name"
            },
            year: {
                required: "Please enter Founding Year",
                number: "Please enter a valid Founding Year",
                minlength: "Please enter a 4  digit Founding Year"
            },
            business_problem_being_solved:{
                required: "Please enter the problem solved"
            },
            products_services_business:{
                required: "Please describe your product service"
            },
            how_different:{
                required: "Please enter the unique selling preposition"
            },
            competition:{
                required: "Please enter your competetors "
            },
            market_size_commentary:{
                required: "Please describe your target audience"
            },
            stage:{
                required: "Please select stage of business "
            },
            investment_required:{
                required: "Please enter proposed fund raise "
            },
            customer_traction:{
                required: "Please enter the traction in last metric"
            },
            hear_about_us:{
                required: "Please select how did you hear about us"
            },
            agree: {
                required: "Please agree to EQ Terms of Use, Privacy policy & NDA"
            }
        },
        errorPlacement: function(error, element) {
            if ($(element).parent().hasClass("select-style")) {
                $(element).parents('.col-sm-8').append(error);
            } else if ($(element).attr("type") == 'file') {
                $(element).parents('.col-sm-8').append(error);
            } else if ($(element).parent().hasClass("radio-inline")) {
                $('#radio-inline').append(error);
            } else if ($(element).parents("label").hasClass("terms_and_condition")) {
                $('#checkbox').append(error);
            } else {
                error.insertAfter(element);
            }
        }
    });

    $('#register-startup-form').submit(function() {
       
        if ($("#register-startup-form").valid() == false) {
           // alert("false");
        } else {
            var formData = new FormData($('#register-startup-form')[0]);
            
            $.ajax({
                url: '<?php echo base_url(); ?>user/register_process',
                type: 'POST',
                xhr: function() {  // Custom XMLHttpRequest
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){ // Check if upload property exists
                    }
                    return myXhr;
                },
                // data: $('#register-startup-form').serialize(),
                dataType: 'json',
                data: formData,
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $('#register_startup_submit').button('loading');
                },
                complete: function() {
                    $('#register_startup_submit').button('reset');
                },
                success: function(json) {
                        console.log(json);
                    $('.failure-msg').hide();
                    $('.failure-msg').html('');
                    if (json['success']) {
                        location = json['redirect'];
                        //$('.failure-msg').html('<div class="col-xs-12"><p class="alert alert-success login-error-alert text-center" role="alert">'+json['success-msg']+'.</p></div>');
                        $('#register-startup-form')[0].reset();
                    } else {
                        $('.failure-msg').html('<div class="col-xs-12"><p class="alert alert-danger login-error-alert text-center" role="alert">' + json['error-msg'] + '.</p></div>');
                    }
                    $('.failure-msg').show();
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            }); 
        }
        return false;
    });
});
</script>
