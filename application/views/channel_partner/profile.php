<div class="container">
  <div class="row">
    <div class="col-sm-9 col-xs-12">
        <h1 class="page-title">Profile</h1>         
    </div>
    <div class="col-sm-3 col-xs-12">
        <div class="breadcrumb row remove_margin">
            <a href="<?php echo base_url(); ?>user/channel_partner_info" class="btn btn-eq-common btn-eq-small pull-right">Edit Profile</a>
        </div>
    </div>
  </div>

        <div class="row content-box">

            <div class="col-sm-12">
                <div class="investor-profile-box">
                    <div class="row shadow-border">
                        <div class="user-image text-center col-sm-2 ">
                            <?php if($partner['image']){ ?>
                            <img class="img-thumbnail" src="<?php echo base_url(); ?>uploads/users/<?php echo $partner['user_id'] ?>/<?php echo $partner['image']; ?>" width="77" height="77"/>
                            <?php }else{ ?>
                            <img class="img-thumbnail" src="<?php echo base_url(); ?>uploads/investor.jpg">
                            <?php } ?>
                            
                            <span class="change-img">Change</span>
                        </div>
                        <div class="col-sm-6">
                            <div class="user-name lato-bold">
                                <?php echo $partner['name']; ?>
                            </div>
                            <div class="user-designation">
                                <?php echo $partner['role']; ?>
                            </div>
                            <div class="user-location">
                                <?php echo $partner['city']; ?>
                            </div>
<!--                                    <div class="user-area">
                                <span class="lato-bold">Areas I can hep </span>: Finance, Fund Rising, Strategic advice
                            </div>-->
                        </div>
                        <div class="col-sm-2 social-ico space-20 pull-right">
							<?php if ($partner['linkedin_url'] != '') { ?>
                            <a target="_blank" href="<?php echo $partner['linkedin_url']; ?>" class="social-link" id="ln">Linked-in</a>
                            <?php } ?>
							<?php if ($partner['twitter_handle'] != '') { ?>
							<a target="_blank" href="<?php echo $partner['twitter_handle']; ?>" class="social-link" id="tw">Twitter</a>
                            <?php } ?>
							<?php if ($partner['fb_url'] != '') { ?>
                            <a target="_blank" href="<?php echo $partner['fb_url']; ?>" class="social-link" id="fb">Facebook</a>
							<?php } ?>
                        </div> 
                    </div>
                    <div class="tab-content row">
                        <!-- tab 1 -->
                        <div class="profile-content col-sm-12 tab-pane active" id="overview">
                            <div class="row">

                                <div class="col-sm-9">
                                    <h5 class="tab-header">Overview</h5>
                                    <?php if($partner['experience']){ ?>
                                    <div class="row">
                                        <div class="col-sm-12 ">
                                            <h6 class="tab-subheader lato-bold">Experience Summary</h6>
                                            <p >
                                                <?php echo strip_tags($partner['experience'], '<p><i><br><b><div><table><td><tr><th><ul><ol><li>');  ?>
                                            </p>
                                            <p class="dotted-border">
                                        </div>
                                    </div>
                                    <?php } ?>
                                    
                                    <?php if($partner['sector_expertise']){ ?>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h6 class="tab-subheader lato-bold">Sector Expertise</h6>
                                            <div class="row">
                                                <?php $sec_expertise = explode(",",$partner['sector_expertise']);
                                                   foreach($sec_expertise as $expert){
                                                ?>
                                                <div class="col-sm-4 list-item">
                                                    <div class="item-border">
                                                        <span class="tick-icon"></span><?php echo $expert; ?>
                                                    </div>
                                                </div>
                                                <?php } ?>

                                            </div>
                                            <p class="dotted-border"></p>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    
                                
                                    <?php if($partner['area_of_expertise']){ ?>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h6 class="tab-subheader lato-bold">Area of Expertise</h6>
                                            <div class="row">
                                                <?php $expertise = explode(",",$partner['area_of_expertise']);
                                                   foreach($expertise as $expert){
                                                ?>
                                                <div class="col-sm-4 list-item">
                                                    <div class="item-border">
                                                        <span class="tick-icon"></span><?php echo $expert; ?>
                                                    </div>
                                                </div>
                                                <?php } ?>

                                            </div>
                                            <p class="dotted-border"></p>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>

<!--                                <div class="col-sm-3">
                                    <div class="row">
                                        <h6 class="tab-header ">Followers (10)</h6>
                                        <div class="col-sm-11">
                                            <div class="row followers">
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>

                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                            </div>
                                            <a href="#" class="pull-right">See all <span class="glyphicon glyphicon-chevron-down"></span></a>
                                        </div>
                                    </div>

                                </div>-->
                            </div>
                        </div>
                        <!-- end tab 1 -->

                    </div>
                </div>
            </div>
        </div>

</div> <!-- /.container -->


<div style="display: none">
    <div id="ch-img" class="container pop-content">
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <h3 class="form-header top-margin-10">Upload Image</h3>
            </div>
        </div>
        <div class="row">
            <div class="top-margin-10">
            <form id="upload-image" action="<?php echo base_url(); ?>user/ajax_upload_image" method="post" enctype="multipart/form-data">
                <div class="col-xs-12 col-sm-12">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2 form-label">Image</label>
                            <div class="col-sm-10">
                                <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput" style="margin-bottom:0;">
                                    <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                                    <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="userfile"></span>
                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                                <label id="upload_error" for="question" class="error">&nbsp;</label>
                            </div>
                        </div>
                    </div>
                    <input class="eq-btn col-sm-12 pull-right" type="submit" value="Submit" />
                </div>
            </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
      
        
    
        $(".change-img").colorbox({inline: true, href: '#ch-img', innerWidth: '80%', maxWidth: '400px'});//, innerHeight: '175px'
       $("#upload-image").on('submit',(function(e){
            e.preventDefault();
            $.ajax({
                    url: "<?php echo base_url(); ?>user/ajax_image_upload",
                    type: "POST",
                    data:  new FormData(this),
                    dataType :'json',
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(data){
                           $('.user-image img').attr('src','<?php echo base_url(); ?>'+data.img);
                           $.colorbox.close()
                    },
                    error: function(){

                    } 
            });         
        }));
   });
             
</script>