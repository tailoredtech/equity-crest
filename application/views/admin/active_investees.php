
<script src="scripts/jquery-1.4.3.min.js" type="text/javascript"></script>

<style type="text/css">

.web_dialog_overlay
{
   position: fixed;
   top: 0;
   right: 0;
   bottom: 0;
   left: 0;
   height: 100%;
   width: 100%;
   margin: 0;
   padding: 0;
   background: #000000;
   opacity: .15;
   filter: alpha(opacity=15);
   -moz-opacity: .15;
   z-index: 101;
   display: none;
}
.web_dialog
{
   display: none;
   position: fixed;
   width: 380px;
   height: 200px;
   top: 50%;
   left: 50%;
   margin-left: -190px;
   margin-top: -100px;
   background-color: #ffffff;
   border: 2px solid #336699;
   padding: 0px;
   z-index: 102;
   font-family: Verdana;
   font-size: 10pt;
}
.web_dialog_title
{
   border-bottom: solid 2px #336699;
   background-color: #336699;
   padding: 4px;
   color: White;
   font-weight:bold;
}
.web_dialog_title a
{
   color: White;
   text-decoration: none;
}
.align_right
{
   text-align: right;
}

</style>



<div id="page-title">
    <h3>
        Active Investees
        <small>

        </small>
    </h3>
</div>
<div id="page-content">
    <form id="update-investees-form" action="<?php echo base_url(); ?>admin/update_investees" method="post">
        <div class="fg-toolbar ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix">
            <div id="example1_length" class="dataTables_length">
                <label>Status <select name="status" id="change-status">
                        <option value="" selected="selected">--Select to change --</option>
                        <option value="inactive">Inactive</option>
                        <option value="semi_active">Semi Active</option>
                        <option value="feature">Feature</option>
                        <option value="funded">Funded</option>
                    </select></label>
            </div>
        </div>
        <table class="table text-center">
            <thead>
                <tr>
                    <th></th>
                    <th class="text-left">Name</th>
                    <th class="text-center">Company Name</th>
                    <th class="text-center">Email</th>
                    <th class="text-center">Mobile</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($investees as $investee) { ?>
                    <tr>
                        <td><input type="checkbox" name="id[]" value="<?php echo $investee['id'] ?>" /></td>

                        <td class="font-bold text-left"><?php echo $investee['name'] ?></td>
                        <td><?php echo $investee['company_name'] ?></td>
                        <td><?php echo $investee['email'] ?></td>
                        <td><?php echo $investee['mob_no'] ?></td>
                        <td>
                        <a href="<?php echo base_url(); ?>investee/edit/active/<?php echo $investee['id'] ?>" class="btn small bg-blue-alt tooltip-button" data-placement="top" title="Edit">
                                <i class="glyph-icon icon-pencil"></i>
                            </a>
                            <a href="<?php echo  base_url()."admin/investee/".$investee['id'];?>" class="btn small bg-blue-alt tooltip-button" data-placement="top" title="View">
                                <i class="glyph-icon icon-flag"></i>
                            </a>
                            <a href="#" class="btn small bg-blue-alt tooltip-button send_mail"  data-placement="top" title="Send" investee="<?php echo $investee['id']; ?>" >
                                Send Mail
                            </a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>

<div id="dialog" class="web_dialog" style="display:none;">
    
   <table align="center" width="100%" cellpadding="3" cellspacing="0">
      <tr>
         <td class="web_dialog_title"></td>
         <td class="web_dialog_title align_right">
            <a href="#" id="btnClose">Close</a>
         </td>
      </tr>    
      <tr>
         <td colspan="2">
            <div id="brands" class="radio-select-style1-section">
              <div class="radio-select-style1">Send Email To ?</div>
              <div class="radio-select-style1">
                <div class="radio-select-style1-text">Investors</div>
                <div class="radio-select-style1-input"><input id="investors" name="emailto" type="radio" checked="checked" value="investors" /></div>
              </div>
              <div class="radio-select-style1">
                <div class="radio-select-style1-text">Investee</div>
                <div class="radio-select-style1-input"><input id="investee" name="emailto" type="radio" value="investee" /></div>
              </div>                
                <input id="checked_investor_id" name="checked_investor_id" type="hidden" />
                <div class="radio-select-style1"><input class="btn small bg-blue-alt tooltip-button" id="btnSubmit" type="button" value="Submit" /></div> 
            </div>
         </td>
      </tr>


   </table>
   
</div>

    </form>  
</div>
<script>
$('document').ready(function(){
    $('#change-status').on('change',function(){
        if($(this).val() != ''){
            $('#update-investees-form').submit();
        } 
    });


    $('.send_mail').on('click', function(){

        $('#checked_investor_id').val($(this).attr('investee'));
        $('#dialog').show();
    });


      $("#btnClose").click(function (e)
      {
         HideDialog();
         e.preventDefault();
      });

      $("#btnSubmit").click(function (e)
      {
        var r = confirm("Are you sure you want to send this mail?");
        if(r){
         var brand = $("#brands input:radio:checked").val();
         var inv_id = $("#checked_investor_id").val();
          formdata = {id:inv_id};

     if(brand == 'investee')
         var url1 = "<?php echo base_url(); ?>admin/send_investee_email/";
       else
           var url1 = "<?php echo base_url(); ?>admin/send_investors_email/";

            $.ajax({
                    url: url1,
                     type:"POST",
                     data:formdata,
                   
                    success: function(data){
                        
                          
                    },
                   
                     
        });
      }
       //  $("#output").html("<b>Your favorite mobile brand: </b>" + brand);
         HideDialog();
         e.preventDefault();
      });

  

   function ShowDialog(modal)
   {
      $("#overlay").show();
      $("#dialog").fadeIn(300);

      if (modal)
      {
         $("#overlay").unbind("click");
      }
      else
      {
         $("#overlay").click(function (e)
         {
            HideDialog();
         });
      }
   }

   function HideDialog()
   {
      $("#overlay").hide();
      $("#dialog").fadeOut(300);
   }

});
       
</script>

