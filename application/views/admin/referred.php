<div id="page-title">
    <h3>
        Companies
        <small>
            
        </small>
    </h3>
</div>
<div id="page-content">
    <table class="table text-center">
        <thead>
            <tr>
                <th>Type</th>
                <th class="text-center">Referred by</th>
                <th class="text-center">Name</th>
                <th class="text-center">Company Name</th>
                <th class="text-center">Email</th>
                <th class="text-center">Mobile</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach( $companies as $company){ ?>
            <tr>
                <td class="font-bold text-left"><?php echo $company['type'] ?></td>
                <td><?php echo $company['referred_by'] ?></td>
                <td><?php echo $company['name'] ?></td>
                <td><?php echo $company['company_name'] ?></td>
                <td><?php echo $company['email'] ?></td>
                <td><?php echo $company['mob_no'] ?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>