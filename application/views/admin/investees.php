<div id="page-title">
    <h3>
        Investees
        <small>
            
        </small>
    </h3>
</div>
<div id="page-content">
    <table class="table text-center">
        <thead>
            <tr>
                <th>Name</th>
                <th class="text-center">Company Name</th>
                <th class="text-center">Email</th>
                <th class="text-center">Mobile</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach( $investees as $investee){ ?>
            <tr>
                <td class="font-bold text-left"><?php echo $investee['name'] ?></td>
                <td><?php echo $investee['company_name'] ?></td>
                <td><?php echo $investee['email'] ?></td>
                <td><?php echo $investee['mob_no'] ?></td>
                <td>
                    <a href="<?php echo base_url(); ?>investee/view/<?php echo $investee['id'] ?>" class="btn small bg-blue-alt tooltip-button" data-placement="top" title="View">
                        <i class="glyph-icon icon-flag"></i>
                    </a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>