<div id="page-title">
    <h3>
        Investor
        <small>
            
        </small>
    </h3>
</div>
<?php //print_r($investor); ?>
<div id="page-content">
    <div class="form-row">
        <div class="col-md-4">
            Name :
        </div>
        <div class="col-md-8">
            <?php if(isset($investor['name']))
            echo $investor['name']; ?>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-4">
            City :
        </div>
        <div class="col-md-8">
            <?php if(isset($investor['city']))
                echo $investor['city']; ?>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-4">
            Email :
        </div>
        <div class="col-md-8">
            <?php if(isset($investor['email']))
                echo $investor['email']; ?>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-4">
            Contact :
        </div>
        <div class="col-md-8">
            <?php if(isset($investor['mob_no']))
                echo $investor['mob_no']; ?>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-4">
            Type :
        </div>
        <div class="col-md-8">
            <?php if(isset($investor['type']))
                echo $investor['type']; ?>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-4">
            Role :
        </div>
        <div class="col-md-8">
            <?php if(isset($investor['role']))
            echo $investor['role']; ?>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col-md-4">
            Experience :
        </div>
        <div class="col-md-8">
            <?php if(isset($investor['experience']))
                echo $investor['experience']; ?>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col-md-4">
            About Company :
        </div>
        <div class="col-md-8">
            <?php if(isset($investor['about_company']))
                echo $investor['about_company']; ?>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col-md-4">
            Preferred Location :
        </div>
        <div class="col-md-8">
            <?php if(isset($investor['preferred_location']))
                echo $investor['preferred_location']; ?>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col-md-4">
            Preferred Location :
        </div>
        <div class="col-md-8">
            <?php if(isset($investor['preferred_location']))
                echo $investor['preferred_location']; ?>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col-md-4">
            Investment To :
        </div>
        <div class="col-md-8">
            <?php if(isset($investor['investment_from']))
                echo $investor['investment_from']; ?>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-4">
            Investment To :
        </div>
        <div class="col-md-8">
            <?php if(isset($investor['investment_to']))
                echo $investor['investment_to']; ?>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col-md-4">
            Invested :
        </div>
        <div class="col-md-8">
            <?php if(isset($investor['invested']))
                echo $investor['invested']; ?>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col-md-4">
            Key Points in Company I look :
        </div>
        <div class="col-md-8">
            <?php if(isset($investor['key_points']))
                echo $investor['key_points']; ?>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col-md-4">
            Sector Expertise :
        </div>
        <div class="col-md-8">
            <?php if(isset($investor['sector_expertise']))
                echo $investor['sector_expertise']; ?>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col-md-4">
           Interest in Mentoring
        </div>
        
    </div>
    
    <div class="form-row">
        <div class="col-md-4">
            Sectors  :
        </div>
        <div class="col-md-8">
            <?php if(isset($investor['mentoring_sectors']))
                echo $investor['mentoring_sectors']; ?>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col-md-4">
            Area of Expertise :
        </div>
        <div class="col-md-8">
            <?php if(isset($investor['expertise']))
                echo $investor['expertise']; ?>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col-md-4">
            duration  :
        </div>
        <div class="col-md-8">
            <?php if(isset($investor['duration']))
                echo $investor['duration']; ?>
        </div>
    </div>
    
</div>    