<div id="page-title">
    <h3>
        Active Users
        <small>

        </small>
    </h3>
</div>
<div id="page-content">
    <form id="search-user-form" action="<?php echo base_url(); ?>admin/active_users" method="get">
        <div class="fg-toolbar ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix float-right">
            <div class="dataTables_filter float-left">
                <label><input type="text" name="name" value='<?php echo $filter['name']; ?>'  placeholder="Search by Name"></label>
            </div>
            <div class="dataTables_filter  float-left">
                <label><input type="text" name="company_name" value='<?php echo $filter['company_name']; ?>'  placeholder="Search by Company Name"></label>
            </div>
            <div class="dataTables_filter float-left">
                <label><select name="role">
                        <option value="" selected="selected">Select Type</option>
                        <option value="investee" <?php echo ($filter['role'] == 'investee')? 'selected' : ''; ?>>Investee</option>
                        <option value="investor" <?php echo ($filter['role'] == 'investor')? 'selected' : ''; ?>>Investor</option>
                        <option value="channel_partner" <?php echo ($filter['role'] == 'channel_partner')? 'selected' : ''; ?>>Channel Partner</option>
                    </select></label>
            </div>
            <div class="dataTables_filter float-left">
                <label><input type="submit" value="Search"></label>
            </div>
        </div> 
    </form>

    <form id="update-users-form" action="<?php echo base_url(); ?>admin/update_users" method="post">
        <div class="fg-toolbar ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix">


            <div id="example1_length" class="dataTables_length">
                <label>Status <select name="status" id="change-status">
                        <option value="" selected="selected">--Select to change --</option>
                        <option value="inactive">Inactive</option>
                    </select></label>
            </div>
        </div>
        <table class="table text-center">
            <thead>
                <tr>
                    <th></th>
                    <th class="text-left">Name</th>
                    <th class="text-center">Company Name</th>
                    <th class="text-center">Email</th>
                    <th class="text-center">Email Verified</th>
                    <th class="text-center">Mobile</th>
                    <th class="text-center">Type</th>
                     <th class="text-center">Registration from Pop-up</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($users as $user) { ?>
                    <tr>
                        <td><input style="width: 25px" type="checkbox" name="id[]" value="<?php echo $user['id'] ?>" /></td>
                        <td class="font-bold text-left"><?php echo $user['name'] ?></td>
                        <td><?php echo $user['company_name'] ?></td>
                        <td><?php echo $user['email'] ?></td>
                        <td><?php echo ($user['active']) ? 'YES' : 'No'; ?></td>
                        <td><?php echo $user['mob_no'] ?></td>
                        <td><?php echo $user['role'] ?></td>

                         <td><?php echo ($user['pop_up_registration'])=='1'?'Yes':'No' ?></td>
                        <td>
                            <a href="<?php echo base_url() . "admin/user/" . $user['id']; ?>" class="btn small bg-blue-alt tooltip-button" data-placement="top" title="View">
                                <i class="glyph-icon icon-flag"></i>
                            </a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </form>  
</div>
<script>
    $('document').ready(function(){
        $('#change-status').on('change',function(){
            if($(this).val() != ''){
                $('#update-users-form').submit();
            } 
        });
    });
</script>