<div id="page-title">
    <h3>
        Edit Investee
    </h3>
</div>
<div class="example-box">
    <div class="example-code clearfix">
        <?php if($this->session->flashdata('success-msg')){ ?>
            <p class="alert alert-success" role="alert"><?php echo $this->session->flashdata('success-msg'); ?></p>
        <?php } ?>
        <?php if($this->session->flashdata('error-msg')){ ?>
            <p class="alert alert-danger"><?php echo $this->session->flashdata('error-msg'); ?></p>
        <?php } ?>
        <form class="form-bordered" action="<?php echo base_url();?>investee/edit" method="post" enctype="multipart/form-data" />
            <input type="hidden" name="id" id="" value="<?php echo $investee['user_id']; ?>"/>
            <input type="hidden" name="type" id="" value="<?php echo $type; ?>"/>
            <div class="form-row">
                <div class="form-label col-md-2">
                    <label for="">
                        Investee Company Name:
                    </label>
                </div>
                <div class="form-input col-md-10">
                    <h4><?php echo isset($investee['company_name'])?$investee['company_name']:''; ?></h4>
                </div>
            </div>
            <div class="form-row">
                <div class="form-label col-md-2">
                    <label for="">
                    Company Banner Image:
                    </label>
                </div>
                <div class="col-md-10">
                    <div class="col-md-5"> 
                        <input type="file" name="banner_image" id="banner_image" />
                    </div>
                    <div class="col-md-5"> 
                        <button id="remove_banner" class="remove_banner" name="remove_banner">Remove</button>
                    </div>
                    <div class="col-md-10">
                        <p class="alert alert-danger">(Use Resolution 280px X 160px for better view)</p>
                        <?php if($investee['banner_image'] != ''): ?>
                            <img src="<?php echo base_url().$investee['banner_image']; ?>" width="150px" height="100px" id="banner_img">
                        <?php endif; ?>
                        <input type="hidden" name="hidden_banner" value="<?php echo $investee['banner_image']; ?>" id="hidden_banner">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-label col-md-2">
                        <label for="">
                            Company SnapShot:
                        </label>
                    </div>
                    <div class="col-md-10">
                        <div class="col-md-5"> 
                            <input type="file" name="snapshot" id="snapshot" class="form-control form-input" />
                            <?php if($investee['snapshot'] != '' && end(explode(".", $investee['snapshot'])) != 'pdf'){ ?>
                                <img src="<?php echo base_url().$investee['snapshot']; ?>" width="150px" height="100px" id="snapshot_img">
                            <?php }
                            else if(end(explode(".", $investee['snapshot'])) == 'pdf')
                            {
                                ?>
                                <img src="<?php echo base_url().'assets/images/pdf_icon.gif';?>" id="snapshot_img">
                                <a id="snapshot_href" href="<?php echo base_url().$investee['snapshot']; ?>" target="_blank">View file</a>
                                <?php
                            }
                             ?>
                            <input type="hidden" name="hidden_snapshot" value="<?php echo $investee['snapshot']; ?>" id="hidden_snapshot">
                        </div>
                        <div class="col-md-5"> 
                            <button id="remove_snapshot" class="remove_snapshot" name="remove_snapshot">Remove</button>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-label col-md-2">
                        <label for="">
                            Total Fundraise:
                        </label>
                    </div>
                    <div class="form-input col-md-10">
                        <input type="text" name="investment_required" id="investment_required" value="<?php echo isset($investee['investment_required'])?$investee['investment_required']:'';?>" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-label col-md-2">
                        <label for="">
                            Minimum Investment:
                        </label>
                    </div>
                    <div class="form-input col-md-10">
                        <input type="text" name="commitment_per_investor" id="commitment_per_investor" value="<?php echo isset($investee['commitment_per_investor'])?$investee['commitment_per_investor']:'';?>" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-label col-md-2">
                        <label for="">
                            Equity Offered:
                        </label>
                    </div>
                    <div class="form-input col-md-10">
                        <input type="text" name="equity_offered" id="equity_offered" value="<?php echo isset($investee['equity_offered'])?$investee['equity_offered']:'';?>" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-label col-md-2">
                        <label for="">
                            Amount Pledged:
                        </label>
                    </div>
                    <div class="form-input col-md-4">
                        <input type="text" name="fund_raise" id="fund_raise" value="<?php echo isset($investee['fund_raise'])?$investee['fund_raise']:'';?>" />
                    </div>
                    <div class="form-input col-md-6">
                        <?php echo isset($pledged_amt)?$pledged_amt:'';?> <?php echo !empty($pledged_by)?'('.$pledged_by.')':'';?>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-label col-md-2">
                        <label for="">
                        Company Presentation:
                        </label>
                    </div>
                    <div class="col-md-10">
                        <div class="col-md-5"> 
                            <input type="file" name="presentation_url" id="presentation_url" />
                        </div>
                        <div class="col-md-5"> 
                            <button id="remove_presentation_url" class="remove_presentation_url" name="remove_presentation_url">Remove</button>
                        </div>
                        <div class="col-md-10">
                            <?php if($investee['presentation_url'] != ''): ?>
                                <iframe id="presentation_url_iframe" src="http://docs.google.com/gview?url=<?php echo base_url().$investee['presentation_url']; ?>&embedded=true" style="width:150px; height:100px;" frameborder="0"></iframe>
                            <?php endif; ?>
                            <input type="hidden" name="hidden_presentation_url" value="<?php echo $investee['presentation_url']; ?>" id="hidden_presentation_url">
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-label col-md-2">
                        <label for="">
                            Investor Deck: <?php echo $investee['investor_deck']; ?>
                        </label>
                    </div>
                    <div class="col-md-10">
                        <div class="col-md-5"> 
                            <input type="file" name="investor_deck" id="investor_deck" />
                        </div>
                        <div class="col-md-5"> 
                            <button id="remove_investor_deck" class="remove_investor_deck" name="remove_investor_deck">Remove</button>
                        </div>
                        <div class="col-md-10">
                        <?php if($investee['investor_deck'] != '' ){
                            if(end(explode('.', $investee['investor_deck'])) == 'ppt' OR end(explode('.', $investee['investor_deck'])) == 'pptx' ) { ?>
                                <iframe id="investor_deck_iframe" src="http://docs.google.com/gview?url=<?php echo base_url().'uploads/users/'.$investee['user_id'].'/deck_files/'.$investee['investor_deck']; ?>&embedded=true" style="width:150px; height:100px;" frameborder="0"></iframe>
                            <?php }
                            else if(end(explode('.', $investee['investor_deck'])) == 'pdf' ) { ?>
                            <img src="<?php echo base_url().'assets/images/pdf_icon.gif';?>" id="snapshot_img">
                                <a id="investor_deck_href" href="<?php echo base_url().'uploads/users/'.$investee['user_id'].'/deck_files/'.$investee['investor_deck']; ?>" target="_blank">View file</a>
                        <?php
                            }
                        } ?>
                            <input type="hidden" name="hidden_investor_deck" value="<?php echo $investee['investor_deck']; ?>" id="hidden_investor_deck">
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-label col-md-2">
                        <label for="">
                            Link viewable upto:
                        </label>
                    </div>
                    <div class="form-input col-md-4">
                        <input type="text" name="accessible_date" id="accessible_date" value="<?php echo isset($investee['accessible_date'])?$investee['accessible_date']:'';?>" />
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-label col-md-2">
                        <label for="">
                            Updates:
                        </label>
                    </div>
                    <div class="form-input col-md-4">
                        <textarea name="updates" id="updates"><?php echo isset($investee['updates'])?$investee['updates']:'';?></textarea>
                    </div>
                </div>
                <?php if (isset($type) && $type == 'funded') {?>
                <div class="form-row">
                    <div class="form-label col-md-2">
                        <label for="">
                            Show details on homepage to guests:
                        </label>
                    </div>
                    <div class="form-input col-md-2">
                        <input type="radio" name="show_details" id="" class="inpRdo" value="1" <?php echo $investee['show_details'] == '1' ? 'checked' : ''; ?>>Yes
                    </div>
                    <div class="form-input col-md-2">
                        <input type="radio" name="show_details" id="" class="inpRdo" value="0" <?php echo $investee['show_details'] == '0' ? 'checked' : ''; ?>>No
                    </div>                          
                </div>
                <?php } ?>
                <div class="form-input col-md-2 col-md-offset-2">
                    <input class="btn btn-eq-common primary-bg medium" type="submit" value="Save"/>
                </div>
            </div>
        </form>
    </div>

<!-- Load jQuery UI CSS  -->
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<!-- Load jQuery UI Main JS  -->
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

<script>
    $(function() {
        $( "#accessible_date" ).datepicker();
        $( "#accessible_date" ).datepicker( "option", "dateFormat", 'yy-mm-dd' );
        $('#accessible_date').datepicker('setDate', '<?php echo $investee["accessible_date"] != "" ? $investee["accessible_date"] : ""; ?>');
    });
</script>

<script type="text/javascript">
$('#remove_banner').click(function(e){

  
    $('#banner_image').val("");
      
      var $image = $('#banner_img');
    $image.removeAttr('src').replaceWith($image.clone());
    $('#hidden_banner').val('');
    e.preventDefault();
});

$('#remove_snapshot').click(function(e){

    $('#snapshot').val("");
    var $image = $('#snapshot_img');
    $image.removeAttr('src').replaceWith($image.clone());
    $('#hidden_snapshot').val('');
    $('#snapshot_href').attr("href", "#");
    $('#snapshot_href').text('Removed');
    $('#snapshot_href').attr("target", "");
    e.preventDefault();
});
$('#remove_presentation_url').click(function(e){  
    $('#presentation_url_image').val("");
      
    // var $image = $('#presentation_url_img');
    // $image.removeAttr('src').replaceWith($image.clone());
    $('#presentation_url_banner').val('');
    $('#presentation_url_iframe').remove();
    e.preventDefault();
});

$('#remove_investor_deck').click(function(e){  
    $('#investor_deck_image').val("");
      
    // var $image = $('#investor_deck_img');
    // $image.removeAttr('src').replaceWith($image.clone());
    $('#investor_deck_banner').val('');
    $('#investor_deck_iframe').remove();
    $('#investor_deck_href').attr("href", "#");
    $('#investor_deck_href').text('Removed');
    $('#investor_deck_href').attr("target", "");
    $('#hidden_investor_deck').val('');
    e.preventDefault();
});
</script>