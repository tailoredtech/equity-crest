<div id="page-title">
    <h3>
        Banners
    </h3>
</div>
<div id="page-content">
    <table class="table text-center">
        <thead>
            <tr>
                <th>No</th>
                <th class="text-center">Banner Heading</th>
                <th class="text-center">Banner Text</th>
                <th class="text-center">Banner Color</th>
                <th class="text-center">Shown To</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach( $banners as $banner){ ?>
            <tr>
                <td class="font-bold text-left"><?php echo $banner['id'] ?></td>
                <td><?php echo $banner['banner_title'] ?></td>
                <td><?php echo $banner['banner_text'] ?></td>
                <td><?php echo $banner['background_color'] ?></td>
                <td><?php echo $banner['flag']==1? 'User': ($banner['flag']==0?'Guest':'None'); ?></td>
                <td>
                    <a href="<?php echo base_url(); ?>admin/edit_banner/<?php echo $banner['id'] ?>" class="btn small bg-blue-alt tooltip-button" data-placement="top" title="Edit">
                        <i class="glyph-icon icon-pencil"></i>
                    </a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>