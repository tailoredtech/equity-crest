<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>Equity Crest Admin</title>
        <meta name="description" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

        

        <!--[if lt IE 9]>
          <script src="assets/js/minified/core/html5shiv.min.js"></script>
          <script src="assets/js/minified/core/respond.min.js"></script>
        <![endif]-->

        <!-- Fides Admin CSS Core -->

        <?=css('admin/aui-production.min.css')?>

        <!-- Theme UI -->

        <?=css('admin/dark-blue.min.css')?>

        <!-- Fides Admin Responsive -->

        <?=css('admin/common.min.css')?>
        <?=css('admin/responsive.min.css')?>

        <!-- Fides Admin JS -->

        <?=js('admin/aui-production.min.js')?>

        <script>
            jQuery(window).load(
                function(){

                    var wait_loading = window.setTimeout( function(){
                      $('#loading').slideUp('fast');
                      jQuery('body').css('overflow','auto');
                    },1000
                    );

                });

        </script>

       <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    </head>
    <body style="overflow: hidden;">
        

        <div id="loading" class="ui-front loader ui-widget-overlay bg-white opacity-100">
            <img src="<?php echo base_url(); ?>assets/images/loader-dark.gif" alt="" />
        </div>

        <div id="page-wrapper" class="demo-example">
          
            <div id="page-content-wrapper" style="margin: 0 auto;">
                <div>
                    <?php echo $this->session->flashdata('error-msg'); ?>
                </div>
                <div class="clear"></div>
                <form action="<?php echo base_url(); ?>admin/login_process" id="login-validation" class="col-md-3 center-margin form-vertical mrg25T" method="post" />

                    <div id="login-form" class="content-box">
                        <h3 class="content-box-header ui-state-default">
                            <div class="glyph-icon icon-separator">
                                <i class="glyph-icon icon-user"></i>
                            </div>
                            <span>Login example</span>
                        </h3>
                        <div class="content-box-wrapper pad20A pad0B">
                            <div class="form-row">
                                <div class="form-label col-md-2">
                                    <label for="login_email">
                                        Username:
                                        <span class="required">*</span>
                                    </label>
                                </div>
                                <div class="form-input col-md-10">
                                    <div class="form-input-icon">
                                        <i class="glyph-icon icon-envelope-alt ui-state-default"></i>
                                        <input placeholder="Email address" data-type="email" data-trigger="change" data-required="true" type="text" name="email" id="login_email" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-label col-md-2">
                                    <label for="login_pass">
                                        Password:
                                    </label>
                                </div>
                                <div class="form-input col-md-10">
                                    <div class="form-input-icon">
                                        <i class="glyph-icon icon-unlock-alt ui-state-default"></i>
                                        <input placeholder="Password" data-trigger="keyup" data-rangelength="[3,25]" type="password" name="password" id="login_pass" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-checkbox-radio col-md-6">
                                    <input type="checkbox" class="custom-checkbox" name="remember-password" id="remember-password" />
                                    <label for="remember-password" class="pad5L">Remember password?</label>
                                </div>
                                <div class="form-checkbox-radio text-right col-md-6">
                                    <a href="#" class="toggle-switch" switch-target="#login-forgot" switch-parent="#login-form" title="Recover password">Forgot your password?</a>
                                </div>
                            </div>
                        </div>
                        <div class="button-pane text-center">
                            <button type="submit" class="btn large primary-bg text-transform-upr font-size-11" id="demo-form-valid" title="Validate!">
                                <span class="button-content">
                                    Login
                                </span>
                            </button>
                        </div>
                    </div>

                    <div class="divider"></div>
                    <div class="form-row text-center">
                        <a href="javascript:;" data-layout="center" data-type="warning" class="ui-state-default btn medium noty radius-all-100" title="Register">
                            <span class="button-content pad20L pad20R">
                                Register a new account
                            </span>
                        </a>
                    </div>
                    <div class="ui-dialog mrg5T no-shadow hide" id="login-forgot" style="position: relative !important;">
                        <div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix">
                            <span class="ui-dialog-title">Recover password</span>
                        </div>
                        <div class="pad20A ui-dialog-content ui-widget-content">
                            <div class="form-row">
                                <div class="form-label col-md-2">
                                    <label for="">
                                        Email address:
                                    </label>
                                </div>
                                <div class="form-input col-md-10">
                                    <div class="form-input-icon">
                                        <i class="glyph-icon icon-envelope-alt ui-state-default"></i>
                                        <input placeholder="Email address" type="text" name="" id="" />
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="ui-dialog-buttonpane text-center">
                            <button type="submit" class="btn large primary-bg" id="demo-form-valid" onclick="javascript:$(&apos;#demo-form&apos;).parsley( &apos;validate&apos; );" title="Validate!">
                                <span class="button-content">
                                    Recover Password
                                </span>
                            </button>
                            <a href="javascript:;" switch-target="#login-form" switch-parent="#login-forgot" class="btn large transparent no-shadow toggle-switch font-bold font-size-11 radius-all-4" id="login-form-valid" onclick="javascript:$(&apos;#login-validation&apos;).parsley( &apos;validate&apos; );" title="Validate!">
                                <span class="button-content">
                                    Cancel
                                </span>
                            </a>
                        </div>
                    </div>
                </form>
            </div>
         </div><!-- #page-wrapper -->    
    </body>
</html>