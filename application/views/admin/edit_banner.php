<div id="page-title">
    <h3>
        Edit Banner
    </h3>
</div>
<div class="example-box">
    <div class="example-code clearfix">

        <form class="form-bordered" action="<?php echo base_url();?>admin/edit_banner" method="post" enctype="multipart/form-data" />
        <input type="hidden" name="id" id="" value="<?php echo $banner['id']; ?>"/>
        <div class="form-row">
            <div class="form-label col-md-2">
                <label for="">
                    Banner Title:
                </label>
            </div>
            <div class="form-input col-md-10">
                <input type="text" name="banner_title" id="" value="<?php echo $banner['banner_title']; ?>"/>
            </div>
        </div>

        
        <div class="form-row">
            <div class="form-label col-md-2">
                <label for="">
                    Banner Text:
                </label>
            </div>
            <div class="form-input col-md-10">
                <input type="text" name="banner_text" id="" value="<?php echo $banner['banner_text']; ?>"/>
            </div>
        </div>

         <div class="form-row">
            <div class="form-label col-md-2">
                <label for="">
                    Banner Font Colour:
                </label>
            </div>
            <div class="form-input col-md-10">
                <input type="text" name="banner_text_font" id="" value="<?php echo $banner['banner_text_font']; ?>"/>
            </div>
        </div>
        <div class="form-row">
            <div class="form-label col-md-2">
                <label for="">
                    Banner Color:
                </label>
            </div>
            <div class="form-input col-md-10">
                <input type="text" name="background_color" id="" value="<?php echo $banner['background_color']; ?>"/>
            </div>
        </div>
        <div class="form-row">
            <div class="form-label col-md-2">
                <label for="">
                    Banner Backgroung Image:
                </label>
            </div>
            <div class="col-md-10">
                <input type="file" name="background_image" id="background_image" />
                <?php if($banner['background_image']): ?>
                    <img src="<?php echo base_url().$banner['background_image']; ?>" width="100px" height="50px">
                <?php endif; ?>
            </div>
        </div>
        <div class="form-row">
            <div class="form-label col-md-2">
                <label for="">
                    Banner Link:
                </label>
            </div>
            <div class="form-input col-md-10">
                <input type="text" name="banner_link" id="" value="<?php echo $banner['banner_link']; ?>"/>
            </div>
        </div>
        <div class="form-row">
            <div class="form-label col-md-2">
                <label for="">
                    Show to:
                </label>
            </div>
            <div class="form-input col-md-2">
                <input type="radio" name="flag" id="" class="inpRdo" value="1" <?php echo $banner['flag']==1?'checked=checked':''; ?> >Logged In User
            </div>
            <div class="form-input col-md-2">
                <input type="radio" name="flag" id="" class="inpRdo" value="0" <?php echo $banner['flag']==0?'checked=checked':''; ?> >Guest
            </div>
            <div class="form-input col-md-2">
                <input type="radio" name="flag" id="" class="inpRdo" value="-1" <?php echo $banner['flag']==-1?'checked=checked':''; ?> >None
            </div>
        </div>
        <div class="form-input col-md-2 col-md-offset-2">
            <input class="btn primary-bg medium" type="submit" value="Save"/>
        </div>

        </form>

    </div>

</div>