<div id="page-title">
    <h3>
        Dashboard
        <small>

        </small>
    </h3>
</div>
<div id="page-content">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <table class="table text-center">
                <thead>
                    <tr>
                        <th class="text-center">Parameter</th>
                        <th class="text-center">Value</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Total Users</td>
                        <td><?php echo $users_count; ?></td>
                    </tr>
                    <tr>
                        <td>Total Startups</td>
                        <td><?php echo $investees_count; ?></td>
                    </tr>
                    <tr>
                        <td>Total Investors</td>
                        <td><?php echo $investors_count; ?></td>
                    </tr>
                    <tr>
                        <td>Total Channel Partners</td>
                        <td><?php echo $channel_partners_count; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>



