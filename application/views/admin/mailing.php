
<div id="page-title">
    <h3>
        Mailing Settings
    </h3>
</div>


<div class="example-box">
    <div class="example-code clearfix">

        <form class="form-bordered" method="post" enctype="multipart/form-data" />
   
        <div class="form-row">
            <div class="form-label col-md-2">
                <label for="">
                    Automated Emails for: 
                </label>
            </div>
             <div class="form-input col-md-10 ">
            <?php if($this->session->flashdata('success') != "") {?>
           <div class="alert alert-success">
              <?php echo $this->session->flashdata('success'); ?>
             </div>
             <?php } ?>  
          </div>  
        </div>

        
        <div class="form-row">
            <div class="form-label col-md-2" style="border-right: 0">
                <label for="">
                    Sign Up : 
                </label>
            </div>
          <div class="form-input col-md-10" style="border-left: 1px solid #dddddd">
            <table class="mailing-box1">
                <tr>
                    <td>ON</td>
                    <td><input type="radio" name="signup_email" id="signup_email" value="on" <?php if($e_settings->signup_email == 'on') { echo "checked"; } ?>></td>                    
                </tr>
                <tr>
                    <td>OFF</td>
                    <td><input type="radio" name="signup_email" id="signup_email" value="off" <?php if($e_settings->signup_email == 'off') { echo "checked"; } ?>></td>                    
                </tr>
            </table>               
                
            <!--     <input checked data-toggle="toggle"  data-onstyle="primary" type="checkbox" name="signup_email" id="signup_email"> -->
          </div> 
        </div>

         <div class="form-row">
            <div class="form-label col-md-2" style="border-right: 0">
                <label for="">
                    Follow/Pledge : 
                </label>
            </div>
            <div class="form-input col-md-10" style="border-left: 1px solid #dddddd">
           <!--   <input checked data-toggle="toggle"  data-onstyle="primary" type="checkbox" name="pledge_email" id="pledge_email"> -->
           <table class="mailing-box1">
                <tr>
                    <td>ON</td>
                    <td><input type="radio" name="pledge_email" id="pledge_email" value="on" <?php if($e_settings->pledge_email == 'on') { echo "checked"; } ?>></td>
                </tr>
                <tr>
                    <td>OFF</td>
                    <td><input type="radio" name="pledge_email" id="pledge_email" value="off" <?php if($e_settings->pledge_email == 'off') { echo "checked"; } ?>></td>
                </tr>
           </table>            
                
            </div>
        </div>
        
       <!--  <div class="form-row">
            <div class="form-label col-md-2">
                <label for="">
                   Activate : 
                </label>
            </div>
            <div class="form-input col-md-10">
               <input checked data-toggle="toggle" data-onstyle="primary" type="checkbox" name="activate_email" id="activate_email">
             <input type="radio" name="activate_email" id="activate_email" value="on">ON
                <input type="radio" name="activate_email" id="activate_email" value="off">OFF
            </div>
        </div> -->
      
       
        <div class="form-input col-md-2 col-md-offset-2">
            <input class="btn primary-bg medium" type="submit" value="Save"/>
        </div>

        </form>

    </div>

</div>