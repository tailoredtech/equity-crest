<div id="page-title">
    <h3>
        Investee
        
    </h3>
    <div>
        <?php if(isset($investee['user_id'])){?>
    <a href="<?php echo base_url(); ?>admin/pledges/<?php echo $investee['user_id'];?>">Pledges</a>
    <a href="<?php echo base_url(); ?>admin/qna/<?php echo $investee['user_id'];?>">Q & A</a>
    <?php }?>
</div>
</div>
<pre><?php //print_r($investee); ?></pre>
<div id="page-content">
    <div class="form-row">
        <div class="col-md-4">
            Name :
        </div>
        <div class="col-md-8">
            <?php if(isset($investee['name']))
           echo $investee['name']; 
           ?>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-4">
            Email :
        </div>
        <div class="col-md-8">
            <?php if(isset($investee['email']))
            echo $investee['email']; ?>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-4">
            Contact :
        </div>
        <div class="col-md-8">
            <?php if(isset($investee['mob_no']))
                echo $investee['mob_no']; ?>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-4">
            Company Name :
        </div>
        <div class="col-md-8">
            <?php if(isset($investee['company_name']))
                echo $investee['company_name']; ?>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-4">
            Company Url :
        </div>
        <div class="col-md-8">
            <?php if(isset($investee['company_url']))
                echo $investee['company_url']; ?>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-4">
            Product URL :
        </div>
        <div class="col-md-8">
            <?php if(isset($investee['product_url']))
                echo $investee['product_url']; ?>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-4">
            Year :
        </div>
        <div class="col-md-8">
            <?php if(isset($investee['year']))
                echo $investee['year']; ?>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-4">
            City :
        </div>
        <div class="col-md-8">
            <?php if(isset($investee['city']))
                echo $investee['city']; ?>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-4">
            Fund Raise:
        </div>
        <div class="col-md-8">
            <?php if(isset($investee['investment_required']))
                echo format_money($investee['investment_required']);?>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col-md-4">
            Minimum commitment :
        </div>
        <div class="col-md-8">
            <?php if(isset($investee['commitment_per_investor']))
                echo format_money($investee['commitment_per_investor']);?>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-4">
            Equity Offered :
        </div>
        <div class="col-md-8">
            <?php if(isset($investee['equity_offered']))
                echo $investee['equity_offered']; ?>
        </div>
    </div>
    
    
    <div class="form-row">
        <div class="col-md-4">
            Team Summary :
        </div>
        <div class="col-md-8">
            <?php if(isset($investee['team_summary']))
                echo $investee['team_summary']; ?>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col-md-4">
            Product/Service :
        </div>
        <div class="col-md-8">
            <?php if(isset($investee['products_services']))
                echo $investee['products_services']; ?>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col-md-4">
            Unique Selling Proposition :
        </div>
        <div class="col-md-8">
            <?php if(isset($investee['how_different']))
                echo nl2br($investee['how_different']); ?>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col-md-4">
            Competition :
        </div>
        <div class="col-md-8">
            <?php if(isset($investee['competition']))
                echo nl2br($investee['competition']); ?>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col-md-4">
            Revenue Model :
        </div>
        <div class="col-md-8">
            <?php if(isset($investee['how_we_make_money']))
                echo nl2br($investee['how_we_make_money']);?>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col-md-4">
            Revenue Traction :
        </div>
        <div class="col-md-8">
            <?php if(isset($investee['customer_traction']))
                echo nl2br($investee['customer_traction']);?>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col-md-4">
            Total Addressable Market:
        </div>
        <div class="col-md-8">
            <?php if(isset($investee['addressable_market']))
                echo nl2br($investee['addressable_market']);?>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col-md-4">
            Valuation:
        </div>
        <div class="col-md-8">
           <?php if(isset($investee['equity_offered'])){
     
                                       $post_money_val = '';
                                       $pre_money_val = '';
                                       $invst_required = $investee['investment_required'];
                                        $eqty_offered  = $investee['equity_offered']/100;
                                        if($eqty_offered>0){
                                        $post_money_val = $invst_required/$eqty_offered;
                                        $pre_money_val = $post_money_val - $invst_required; 
                                        
                                 ?>
                                        <p>
                                            Pre Money Valuation : <?php echo format_money($pre_money_val); ?>
                                        </p>
                                        <p>
                                            Post Money Valution : <?php echo format_money($post_money_val) ?>
                                        </p>
                                        <?php }}else { ?>
                                       <p>
                                            Pre Money Valuation :  Not Available
                                        </p>
                                        <p>
                                            Post Money Valution :  Not Available
                                        </p>
                                <?php } ?>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col-md-4">
            Use of Funds :
        </div>
        <div class="col-md-8">
            <table class="table  investee-table">
                                    <tr>
                                        <th>Purpose</th>
                                        <th>Amount</th>
                                    </tr>
                                    <?php
                                    $total = 0;
                                    $purposes=array();
                                    foreach ($purposes as $purpose) {
                                        if ($purpose['purpose'] != '') {
                                            ?>
                                            <tr>
                                                <td><?php echo $purpose['purpose']; ?></td>
                                                <td><img class="rupee-icon" src="<?php echo base_url(); ?>assets/images/rupee.png"><?php echo format_money($purpose['amount']); ?></td>
                                            </tr>
        <?php $total += $purpose['amount'];
    }
} ?>
                                    <tr>
                                        <td>Total</td>
                                        <td><img class="rupee-icon" src="<?php echo base_url(); ?>assets/images/rupee.png"><?php echo format_money($total); ?></td>
                                    </tr>
                                </table>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col-md-4">
            Prior Fund raise details :
        </div>
        <div class="col-md-8">
            <?php if(isset($investee['funding_history'])){ 
                                        echo $investee['funding_history']; ?>
                                    <?php }else{ ?>
                                        Not Available
                                    <?php } ?>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col-md-4">
            Current Monthly Financial Indicators :
        </div>
        <div class="col-md-8">
            <table class="table investee-table">
                                    <tr>
                                        <td>Revenues</td>
                                        <td>
                                             <?php if(isset($investee['monthly_revenue'])){ ?>
                                               <?php if($investee['monthly_revenue']){?><img class="rupee-icon" src="<?php echo base_url(); ?>assets/images/rupee.png"><?php echo format_money($investee['monthly_revenue']); } ?>
                                            <?php }else{ ?>
                                                Not Available
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Fixed Cost (OPEX)</td>
                                        <td>
                                            <?php if(isset($investee['fixed_opex'])){ ?>
                                                <?php if($investee['fixed_opex']){ ?><img class="rupee-icon" src="<?php echo base_url(); ?>assets/images/rupee.png"><?php echo format_money($investee['fixed_opex']); } ?>
                                            <?php }else{ ?>
                                                Not Available
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Cash Burn</td>
                                        <td>
                                            <?php if(isset($investee['cash_burn'])){ ?>
                                                <?php if($investee['cash_burn']){ ?><img class="rupee-icon" src="<?php echo base_url(); ?>assets/images/rupee.png"><?php echo format_money($investee['cash_burn']); } ?>
                                            <?php }else{ ?>
                                                Not Available
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Debt</td>
                                        <td>
                                            <?php if(isset($investee['debt'])){ ?>
                                                <?php if($investee['debt']){ ?><img class="rupee-icon" src="<?php echo base_url(); ?>assets/images/rupee.png"><?php echo format_money($investee['debt']); } ?>
                                            <?php }else{ ?>
                                                Not Available
                                            <?php } ?>
                                        </td>
                                    </tr>
                                </table>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col-md-4">
            Financial Forecast :
        </div>
        <div class="col-md-8">
            <?php if(isset($investee['financial_forecast'])){ ?>
                                        <a href='<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id']; ?>/files/<?php echo $investee['financial_forecast']; ?>' target="_blank"><?php echo $investee['financial_forecast']; ?></a>
                                    <?php }else{ ?>
                                        Not Available
                                    <?php } ?>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col-md-4">
            Financial Statements :
        </div>
        <div class="col-md-8">
            <?php if(isset($investee['financial_statement'])){ ?>
                                        <a href='<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id']; ?>/files/<?php echo $investee['financial_statement']; ?>' target="_blank"><?php echo $investee['financial_statement']; ?></a>
                                    <?php }else{ ?>
                                        Not Available
                                    <?php } ?>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col-md-4">
            Others (if any) :
        </div>
        <div class="col-md-8">
            <?php if(isset($investee['other'])){ ?>
                                        <a href='<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id']; ?>/files/<?php echo $investee['other']; ?>' target="_blank"><?php echo $investee['other']; ?></a>
                                    <?php }else{ ?>
                                        Not Available
                                    <?php } ?>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col-md-4">
            Media :
        </div>
        <div class="col-md-8">
            <?php if(isset($investee['media'])){ 
                                        echo $investee['media']; ?>
                                    <?php }else{ ?>
                                        Not Available
                                    <?php } ?>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col-md-4">
            Awards & Recognition :
        </div>
        <div class="col-md-8">
            <?php if(isset($investee['awards'])){ 
                                        echo $investee['awards']; ?>
                                    <?php }else{ ?>
                                        Not Available
                                    <?php } ?>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col-md-4">
            Testimonials :
        </div>
        <div class="col-md-8">
            <?php if(isset($investee['testimonials'])){ 
                                        echo $investee['testimonials']; ?>
                                    <?php }else{ ?>
                                        Not Available
                                    <?php } ?>
        </div>
    </div>

    <div class="form-row">
        <div class="col-md-4">
            Investor Deck :
        </div>
        <div class="col-md-8">
            <?php if($investee['investor_deck'] != '' ){
                            if(end(explode('.', $investee['investor_deck'])) == 'ppt' OR end(explode('.', $investee['investor_deck'])) == 'pptx' ) { ?>
                                <iframe id="investor_deck_iframe" src="http://docs.google.com/gview?url=<?php echo base_url().'uploads/users/'.$investee['user_id'].'/deck_files/'.$investee['investor_deck']; ?>&embedded=true" style="width:150px; height:100px;" frameborder="0"></iframe>
                            <?php }
                            else if(end(explode('.', $investee['investor_deck'])) == 'pdf' ) { ?>
                            <img src="<?php echo base_url().'assets/images/pdf_icon.gif';?>" id="snapshot_img">
                                <a id="investor_deck_href" href="<?php echo base_url().'uploads/users/'.$investee['user_id'].'/deck_files/'.$investee['investor_deck']; ?>" target="_blank">View file</a>
                        <?php
                            }
                        } ?>
        </div>
    </div>

    <div class="form-row">
        <div class="col-md-4">
            Updates :
        </div>
        <div class="col-md-8">
            <?php if(isset($investee['updates'])){ 
                                        echo $investee['updates']; ?>
                                    <?php }else{ ?>
                                        Not Available
                                    <?php } ?>
        </div>
    </div>
    
</div>    
<?php //print_r($investee)?>