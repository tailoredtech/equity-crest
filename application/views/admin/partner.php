<div id="page-title">
    <h3>
        Partner
        <small>
            
        </small>
    </h3>
</div>
<?php //print_r($partner); ?>
<div id="page-content">
    <div class="form-row">
        <div class="col-md-4">
            Name :
        </div>
        <div class="col-md-8">
            <?php if(isset($partner['name']))
            echo $partner['name']; ?>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-4">
            City :
        </div>
        <div class="col-md-8">
            <?php if(isset($partner['city']))
                echo $partner['city']; ?>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-4">
            Email :
        </div>
        <div class="col-md-8">
            <?php if(isset($partner['email']))
                echo $partner['email']; ?>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-4">
            Contact :
        </div>
        <div class="col-md-8">
            <?php if(isset($partner['mob_no']))
                echo $partner['mob_no']; ?>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-4">
            Type :
        </div>
        <div class="col-md-8">
            <?php if(isset($partner['type']))
                echo $partner['type']; ?>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-4">
            City :
        </div>
        <div class="col-md-8">
            <?php if(isset($partner['role']))
            echo $partner['role']; ?>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col-md-4">
            Experience :
        </div>
        <div class="col-md-8">
            <?php if(isset($partner['sector_expertise']))
                echo $partner['sector_expertise']; ?>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col-md-4">
            Experience :
        </div>
        <div class="col-md-8">
            <?php if(isset($partner['area_of_expertise']))
                echo $partner['area_of_expertise']; ?>
        </div>
    </div>
    
    
    
</div>    