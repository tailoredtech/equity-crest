<section class="how_it_works">

	<div class="container">

		<h2 class="section-title text-center">How it works</h2>
		
		<div class="text-center how_it_works_button_group">
			<ul id="myTab" class="btn-group" role="tablist">
				<li role="presentation" class="btn active btn-default">
			  		<a href="#how_it_works_raising_funds" id="how_it_works_raising_funds-tab" role="tab" data-toggle="tab" aria-controls="how_it_works_raising_funds" aria-expanded="true">
				  		Raising
					</a>
				</li>
				
				<li role="presentation" class="btn btn-default">
					<a href="#how_it_works_investors" role="tab" id="how_it_works_investors-tab" data-toggle="tab" aria-controls="how_it_works_investors" aria-expanded="false">
						Investing
					</a>
				</li>
			
			</ul>
		</div>
		
		<div id="myTabContent" class="tab-content">
			<div role="tabpanel" id="how_it_works_raising_funds" class="tab-pane fade active in row how_it_work_row" aria-labelledby="how_it_works_raising_funds-tab">
								
				<div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 how_it_work_col">
					<div class="how_it_work_item text-center">
						<div class="how_it_work_img">
							<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/registration-dark-blue.png" alt="Registration"/>
						</div>
						<h4>Registration</h4>
						<p>Create your profile and upload your proposal</p>
					</div>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 how_it_work_col">
					<div class="how_it_work_item text-center">
						<div class="how_it_work_img">
							<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/screening-listing-dark-blue.png" alt="Screening & Listing"/>
						</div>
						<h4>Screening & Listing</h4>
						<p>Equity Crest will analyse your proposal and engage with you to publish updated profile</p>
					</div>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 how_it_work_col">
					<div class="how_it_work_item text-center">
						<div class="how_it_work_img">
							<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/track-investor-interest-dark-blue.png" alt="Track Interest"/>
						</div>
						<h4>Track Investor Interest</h4>
						<p>Engage with investors and respond to interested inventors</p>
					</div>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 how_it_work_col">
					<div class="how_it_work_item text-center">
						<div class="how_it_work_img">
							<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/finalize-terms-dark-blue.png" alt="Finalize Terms"/>
						</div>
						<h4>Finalize Terms</h4>
						<p>Discussions with investors to finalize the term sheet</p>
					</div>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 how_it_work_col">
					<div class="how_it_work_item text-center">
						<div class="how_it_work_img">
							<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/diligence-closure-dark-blue.png" alt="Closure"/>
						</div>
						<h4>Diligence & Closure</h4>
						<p>Complete due diligence & investment agreements</p>
					</div>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 how_it_work_col">
					<div class="how_it_work_item text-center">
						<div class="how_it_work_img">
							<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/share-updates-dark-blue.png" alt="Share Updates"/>
						</div>
						<h4>Share Updates</h4>
						<p>Business & Financial updates shared with investors</p>
					</div>
				</div>			
			</div> <!-- how_it_works_raising_funds -->
		
			<div role="tabpanel" class=" tab-pane fade row how_it_work_row" id="how_it_works_investors" aria-labelledby="how_it_works_investors-tab">

				<div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 how_it_work_col">
					<div class="how_it_work_item text-center">
						<div class="how_it_work_img">
							<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/registration-dark-blue.png" alt="Registration"/>
						</div>
						<h4>Registration</h4>
						<p>Create a login</p>
					</div>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 how_it_work_col">
					<div class="how_it_work_item text-center">
						<div class="how_it_work_img">
							<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/screening-listing-dark-blue.png" alt="Setup Profile"/>
						</div>
						<h4>Setup Profile</h4>
						<p>Fill out your profile and preferences</p>
					</div>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 how_it_work_col">
					<div class="how_it_work_item text-center">
						<div class="how_it_work_img">
							<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/track-investor-interest-dark-blue.png" alt="Access Deals"/>
						</div>
						<h4>Access Deals</h4>
						<p>Search or access deals on the platform</p>
					</div>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 how_it_work_col">
					<div class="how_it_work_item text-center">
						<div class="how_it_work_img">
							<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/finalize-terms-dark-blue.png" alt="Evaluate Deals"/>
						</div>
						<h4>Evaluate Deals</h4>
						<p>Engage with founders and other investors</p>
					</div>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 how_it_work_col">
					<div class="how_it_work_item text-center">
						<div class="how_it_work_img">
							<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/diligence-closure-dark-blue.png" alt="Commit to Deals"/>
						</div>
						<h4>Commit to Deals</h4>
						<p>Pledge your interest in a deal</p>
					</div>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 how_it_work_col">
					<div class="how_it_work_item text-center">
						<div class="how_it_work_img">
							<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/share-updates-dark-blue.png" alt="Closure"/>
						</div>
						<h4>Closure</h4>
						<p>Diligence and documentation process</p>
					</div>
				</div>


		       
		    </div> <!-- /.how_it_works_investors -->
					
		</div>

	</div>

</section> <!-- ./how_it_works -->