<section class="trending_opportunities list-grid-view"> 

<div class="container">
  <div class="row">
    
    <div class="col-xs-12 col-sm-8">
      <h2 class="section-title">Tracked</h2>
    </div>
    
    <?php if(count($tracked_users)):?>
    <div class="col-xs-12 col-sm-4 hidden-sm hidden-xs">
      <ul id="trackedTab" class="pull-right list-grid-view-tab" role="tablist">
        <li role="presentation">
            <a href="#tracked_list_view" id="tracked_list_view-tab" role="tab" data-toggle="tab" aria-controls="tracked_list_view" aria-expanded="true" class="list-view-tab-icon">
              List
          </a>
        </li>
        
        <li role="presentation" class="active">
          <a href="#tracked_grid_view" role="tab" id="tracked_grid_view-tab" data-toggle="tab" aria-controls="tracked_grid_view" aria-expanded="false" class="grid-view-tab-icon">
            Grid
          </a>
        </li>     
      </ul>
    </div>
    <?php endif; ?>
    
  </div>
</div>


<div id="trackedTabContent" class="tab-content">

<div role="tabpanel" id="tracked_grid_view" class="tab-pane fade active container in" aria-labelledby="tracked_grid_view-tab">
    
    <div class="row">

        <?php $max = count($tracked_users);
        if ($max) { ?>  

                        <?php
                        $count = 0;
                        foreach ($tracked_users as $user) {
                            $data['user'] = $user;
                            $data['colsize'] = 3;
                            $data['other_classes'] = "";
                            
                            if($count == 3)
                            {
                            	$data['other_classes'] = 'hidden-md'; 
                            }
                            
                            $this->load->view('front-end/grid-single-item', $data);
                            if($count == 3){break;}
                            $count++;
                        }
                        ?>

        <?php } else { ?>
             <div class="row remove_margin">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="empty-preferences-box">There are no tracked companies available. To track a company, follow it!</div>
                </div>
              </div>
        <?php } ?>
    </div>


</div>


<div role="tabpanel" class="tab-pane fade container hidden-sm hidden-xs" id="tracked_list_view" aria-labelledby="tracked_list_view-tab" >

  <div class="list-view-deals">

    <div class="row remove_margin list-view-deal-header">

      <div class="col-md-3">Opportunity</div>
      <div class="col-md-2">Sector</div>
      <div class="col-md-2">Minimum</div>
      <div class="col-md-2">Funds Raised</div>
      <div class="col-md-3">Total</div>

    </div>
  

        <?php $max = count($tracked_users);
        if ($max) { ?>  

                        <?php
                        $count = 0;
                        foreach ($tracked_users as $user) {
                            $data['user'] = $user;
                            $this->load->view('front-end/list-single-item', $data);
                            if($count == 3){break;}
                            $count++;
                        }
                        ?>

        <?php } else { ?>
            <div class="row remove_margin">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="empty-preferences-box">There are no tracked companies available. To track a company, follow it!</div>
                </div>
              </div>
        <?php } ?>

   </div> <!-- ./list-view-deal -->     
    
</div>
<?php if($tracked_total_result>4): ?>
<div class="container">
  <div class="row">
    <div class="col-xs-12"><a href="<?php echo base_url(); ?>investor/tracked" class="view_more">View more in Tracked</a></div>
  </div>
</div>
<?php endif; ?>
</div> <!-- #/opportunitiesTabContent -->

</section> <!-- ./trending_opportunities -->