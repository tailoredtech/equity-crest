      <div class="col-md-3 col-sm-4">
 
          <div class="profile-box">
         <?php if ($this->session->userdata('user_id')) { ?> 
            <?php if (!empty($user['user_id']) && $this->session->userdata('role') != 'channel_partner') {    ?>
                <a href="<?php echo base_url(); ?>investor/view/<?php echo $user['user_id'] ?>"></a>
            <?php } } ?>
            
            <div class="investor-single">  
               
            <div class="user-info" >            

                <div class="row remove_margin user-image-box">
	              
	              <?php
		              $image_class = "square-image-outer";
		             
		              if($user['investor_type'] == "Venture Fund")
		              {
			              $image_class = "three-by-five-image-outer fund-image-box";
		              }
		              if($user['investor_type'] == "Angel Fund")
		              {
			              $image_class = "three-by-five-image-outer fund-image-box angel_fund_logo";
		              }
		              
	              ?>
	                
                  <div class="col-xs-12 remove_padding image-box-outer <?= $image_class;?>">
                    <div class="user-image img-responsive image-box-inner vertical-align-parent-box"> 
                        <?php if(!empty($user['image'])) { ?>
                            <img class="equity-lazy investor-grid-image vertical-align-child-box" src="<?php echo base_url(); ?>uploads/users/<?php echo $user['user_id'] ?>/<?php echo $user['image']; ?>" data-original = "<?php echo base_url(); ?>uploads/users/<?php echo $user['user_id'] ?>/<?php echo $user['image']; ?>" >
                        <?php } elseif($user['investor_type'] == "Angel") { ?>
                            <img class="investor-grid-image investor-grid-image vertical-align-child-box" src="<?php echo base_url(); ?>uploads/angel-investor.png" >
                        <?php } else { ?>
                            <img class="investor-grid-image" src="<?php echo base_url(); ?>uploads/investor.jpg" >
                        <?php } ?>
                    </div>
                  </div>
                  
                  <div class="user-type text-right"> 
	                  <?php 
		                  if (isset($user['investor_type']) && ($user['investor_type'] !="") ) { 
		                  	echo $user['investor_type']; 
		                  } 
		                  else {
			                echo "";
			            } ?>
			            &nbsp;|&nbsp;
	              </div> 
                  
                  <div class="user-city text-left"> 
	                  <?php 
		                  if (isset($user['city']) && ($user['city'] !="") ) { 
		                  	echo $user['city']; 
		                  } 
		                  else {
			                echo "";
			            } ?>
	              </div> 
	              
                </div>

                <?php if($user['investor_type'] == 'Angel') {?>
                <div class="user-profile-box row remove_margin">  
                
                	<div class="user-company-name text-center"> 
                      <!-- <a href = "<?php echo base_url(); ?>user/investor_info/NULL/<?php echo $user['id']?>" target="_blank"> <h4> <?php if (isset($user['name'])) echo $user['name']; else echo "Not available"; ?></h4></a> -->
	                  <a href = "<?php echo base_url(); ?>user/investor_view/<?php echo $user['id']?>" target="_blank"> 
		                  <h2 class="investor-single-title"> 
			                  <?php if (isset($user['name'])) echo $user['name']; else echo "Not available"; ?>
			              </h2>
		              </a>
                    </div> 

                	<div class="user-name text-center"> 
                    	<?php 
	                    if ((isset($user['company_name']) && $user['company_name']!='')) { 
	                    	echo '<b>'.$user['company_name'].'</b>'; 
	                    }
	                    else {
		                    echo ""; 
		                }    
		                ?>
	                </div>          
                    <div class="user-company-designation text-center"> 
                    	<?php 
	                    if ((isset($user['designation']) && $user['designation']!='')) { 
	                    	echo $user['designation']; 
	                    }
	                    else {
		                    echo ""; 
		                }    
		                ?>
	                </div> 
                    
                    <div class="user-sector text-center"> 
	                  
						<?php 
							if (!empty($user['sector_expertise'])) {
		                  		
		                  		if($user['sector_expertise'] == "Sector Agnostic"){
			                  		echo "Invests in: Multiple Sectors";
		                  		}
		                  		
		                  		else
		                  		{
		                  			echo "Invests in: ".str_replace(",",", ",$user['sector_expertise']);
		                  		}
		                  	} 
						  	
						  	else {
							  	echo '';	  	
							} 
						?>
						
	              </div>
                 
                </div>
                <?php }
                else if($user['investor_type'] == 'Angel Fund') {?>
                <div class="user-profile-box row remove_margin">  
                
                	<div class="user-company-name text-center"> 
                    	<?php 
	                    if ((isset($user['company_name']) && $user['company_name']!='')) { 
	                    	echo $user['company_name']; 
	                    }
	                    else {
		                    echo ""; 
		                }    
		                ?>
	                </div> 
	                <br>
                    <div class="user-name text-center"> 
                      <!-- <a href = "<?php echo base_url(); ?>user/investor_info/NULL/<?php echo $user['id']?>" target="_blank"> <h4> <?php if (isset($user['name'])) echo $user['name']; else echo "Not available"; ?></h4></a> -->
	                  <a href = "<?php echo base_url(); ?>user/investor_view/<?php echo $user['id']?>" target="_blank"> 
		                  <h2 class="investor-single-title"> 
			                  <?php if (isset($user['name'])) echo $user['name']; else echo "Not available"; ?>
			              </h2>
		              </a>
                    </div> 
                    
                    <div class="user-company-designation text-center"> 
                    	<?php 
	                    if ((isset($user['designation']) && $user['designation']!='')) { 
	                    	echo $user['designation']; 
	                    }
	                    else {
		                    echo ""; 
		                }    
		                ?>
	                </div> 
                    
                    <div class="user-sector text-center"> 
	                  
						<?php 
							if (!empty($user['sector_expertise'])) {
		                  		
		                  		if($user['sector_expertise'] == "Sector Agnostic"){
			                  		echo "Invests in: Multiple Sectors";
		                  		}
		                  		
		                  		else
		                  		{
		                  			echo "Invests in: ".str_replace(",",", ",$user['sector_expertise']);
		                  		}
		                  	} 
						  	
						  	else {
							  	echo '';	  	
							} 
						?>
						
	              </div>
                 
                </div>
                <?php }
                else
                {
                	?>
                	<div class="user-profile-box row remove_margin">  
                
                	<div class="user-company-name text-center"> 
                    	<?php 
	                    if ((isset($user['company_name']) && $user['company_name']!='')) { 
	                    	echo $user['company_name']; 
	                    }
	                    else {
		                    echo ""; 
		                }    
		                ?>
	                </div> 

                    <div class="user-name text-center"> 
                      <!-- <a href = "<?php echo base_url(); ?>user/investor_info/NULL/<?php echo $user['id']?>" target="_blank"> <h4> <?php if (isset($user['name'])) echo $user['name']; else echo "Not available"; ?></h4></a> -->
	                  <a href = "<?php echo base_url(); ?>user/investor_view/<?php echo $user['id']?>" target="_blank"> 
		                  <h2 class="investor-single-title"> 
			                  <?php if (isset($user['name'])) echo $user['name']; else echo "Not available"; ?>
			              </h2>
		              </a>
                    </div> 

                	<div class="user-company-designation text-center"> 
                    	<?php 
	                    if ((isset($user['designation']) && $user['designation']!='')) { 
	                    	echo $user['designation']; 
	                    }
	                    else {
		                    echo ""; 
		                }    
		                ?>
	                </div> 
	                <br>
                	

                    <div class="user-sector text-center"> 
	                  
						<?php 
							if (!empty($user['sector_expertise'])) {
		                  		
		                  		if($user['sector_expertise'] == "Sector Agnostic"){
			                  		echo "Invests in: Multiple Sectors";
		                  		}
		                  		
		                  		else
		                  		{
		                  			echo "Invests in: ".str_replace(",",", ",$user['sector_expertise']);
		                  		}
		                  	} 
						  	
						  	else {
							  	echo '';	  	
							} 
						?>
						
	              </div>
                 
                </div>
                	<?php
                }
                ?>
                <?php /* ?>
                <div class="user-interests row remove_margin">
                  	<div class="user-sector text-center"> 
	                  
						<?php 
							if (!empty($user['sector_expertise'])) {
		                  		
		                  		if($user['sector_expertise'] == "Sector Agnostic"){
			                  		echo "Invests in: Multiple Sectors";
		                  		}
		                  		
		                  		else
		                  		{
		                  			echo "Invests in: ".str_replace(",",", ",$user['sector_expertise']);
		                  		}
		                  	} 
						  	
						  	else {
							  	echo '';	  	
							} 
						?>
						
	              </div>
                 
                </div> 
                <?php */ ?>
                <!-- <div class="more-image"><img src="<?php echo base_url(); ?>assets/images/investor-more.jpg"></div>  -->
         
             
            </div>
          
        </div> <!-- /.investor-single -->
    </div> <!-- ./profile-box -->
    <!-- </div> -->
          
        </div>
