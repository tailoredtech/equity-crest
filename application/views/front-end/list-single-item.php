<div id="i-<?php echo $user['user_id'] ?>" class="row list-view-deal remove_margin">
    <?php if ($this->session->userdata('role') == 'media' || $this->session->userdata('role') == 'investee' || $user['status'] == 'funded') { ?>
    <?php  } elseif ($this->session->userdata('user_id') && $user['status'] != 'funded') { ?>

    <a href="<?php echo base_url(); ?>investee/view/<?php echo $user['user_id'] ?>">
    <?php }else { ?>
    <a href="<?php echo base_url(); ?>user/login">
    <?php } ?>

    <div class="col-md-3">
        <div class="deal-image">
            <div class="deal-image-inner">
                <span>

                    <?php if ($this->session->userdata('user_id')) { ?> 

                                <img src="<?php echo base_url(); ?>uploads/users/<?php echo $user['user_id'] ?>/<?php echo $user['image'] ?>">
                            <?php } else { ?>
                                <img class="img-responsive" src="<?php echo base_url().'assets/images/company_default_50x50.jpg';?>">
                            <?php } ?>

              

                </span>
            </div>            
        </div>

        <div class="d-title">            
            <div class="deal-title-block">
                <?php if ($this->session->userdata('user_id')) { ?> 
                    <h2 class="deal-title"><?php echo $user['company_name'] ?></h2>
                <?php } else { ?>
                    <h2 class="deal-title">Company</h2>
                <?php } ?>          
                <div class="deal-location"><?php echo $user['city'] ?></div>
                               
            </div>
        </div>

    </div>

    </a>

    <div class="col-md-2 deal-sector"><?php echo $user['sector_name'] ?></div>

    <?php if ($user['status'] == 'active' || $user['status'] == 'funded') {?>

    <div class="col-md-2 deal-min">&#8377; <strong><?php echo format_money($user['commitment_per_investor']); ?></strong> </div>

        <?php 
     $fund_raise = $user['fund_raise'];
     $investment_required = $user['investment_required'];
     if($investment_required){
         $percent = ($fund_raise/$investment_required)*100;
     }else{
         $percent = 0;
     }     
    ?> 

    <div class="col-md-2 deal-progress">

        <div class="row remove_margin">
            <div class="col-xs-9 remove_padding">
                <div class="progress" style="height: 6px;">
                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $percent; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $percent,'%'; ?>; background: rgb(0,166,50);"></div>
                     
                    <span class="sr-only">60% Complete</span>
                </div>
            </div>
            <div class="col-xs-3 remove_padding">
                <div class="deal-progress-percent"><?php echo floor($percent); ?>%</div>
            </div>
        </div>
    </div>

    <div class="col-md-3 deal-total-lv">

        <div class="funds-req-amount pull-left">
             &#8377; <?php echo format_money($user['investment_required']); ?>                  
        </div>
          <?php if($this->session->userdata('role') == 'investor' && $user['status'] != 'funded') { ?>
                    <div class="funds-req-status pull-right">
                        <?php $followed = $this->session->userdata('followed'); 
                            if(is_array($followed)){
                         if(in_array($user['user_id'],$followed)){ ?>
                               <span class="fund_follow_icon follow-<?=$user['user_id']?> unfollow" data-user-id="<?php echo $user['user_id']; ?>">
                               <img src="<?php echo base_url(); ?>assets/images/fund_follow_active_icon.png" alt="Unfollow">                
                              </span>               
                        <?php }else{ ?>
                               <span class="fund_follow_icon follow-<?=$user['user_id']?> follow" data-user-id="<?php echo $user['user_id']; ?>">          
                                  <img src="<?php echo base_url(); ?>assets/images/fund_follow_icon.png" alt="Follow">
                              </span>
                        <?php }  }else{ ?>  
                            <span class="fund_follow_icon follow-<?=$user['user_id']?> follow" data-user-id="<?php echo $user['user_id']; ?>">          
                                <img src="<?php echo base_url(); ?>assets/images/fund_follow_icon.png" alt="Follow">
                              </span>
                            <?php } ?>
                        
                        <?php $pledged = $this->session->userdata('pledged'); 
                         if(is_array($pledged)){
                                
                         if(in_array($user['user_id'],$pledged)){ ?>
                            <span class="fund_follow_pledge_icon pledge-<?=$user['user_id']?> pledged" data-user-id="<?php echo $user['user_id']; ?>">          
                              <img src="<?php echo base_url(); ?>assets/images/fund_follow_pledge_active_icon.png" alt="Pledged">
                            </span>
                        <?php }else{ ?>
                            <span class="fund_follow_pledge_icon pledge-<?=$user['user_id']?> pledge" data-user-id="<?php echo $user['user_id']; ?>">
                              <img src="<?php echo base_url(); ?>assets/images/fund_follow_pledge_icon.png" alt="Pledge">         
                            </span> 
                        <?php }  }else{ ?>
                            <span class="fund_follow_pledge_icon pledge-<?=$user['user_id']?> pledge" data-user-id="<?php echo $user['user_id']; ?>">
                              <img src="<?php echo base_url(); ?>assets/images/fund_follow_pledge_icon.png" alt="Pledge">          
                            </span>
                        <?php } ?>    
                    </div>
                  <?php } ?>
    </div>  


    <?php } else { ?>
        <div class="semi_active col-md-7 col-sm-7 comingsoonstrip">
                    Coming Soon                  
                </div>
    <?php }  ?>    

</div> <!-- ./list-view-deal -->