<section class="testimonial">

    <div id="testimonial_slider" class="owl-carousel owl-theme">

      <div class="item">

        <img class="img-responsive owl_testimonial_img" src="<?php echo base_url().'assets/images/home_slider_blank_bg.png'; ?>" alt="banner"/>

          <div class="owl_testimonial_slide">

            <div class="container">

              <div class="row">

                <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">

                  <div class="testimonial_slide_wrap">

                    <div class="testimonial_slide_inner">

                      <p class="testimonial-text">“Equity Crest's unique engagement model, deep investor connect, sharp business insights and strong sense of deal ownership was exactly what we needed to transition to the next level” <br><span class="testimonial-person">Nimit Bavishi, </span><span class="testimonial-company-name">Happy2Refer</span></p>
                        <!-- <p class="testimonial-view-more"><a href="#" target="_blank"><button class="btn" type="button">View more testimonials</button></a></p> -->

                    </div>  

                  </div>                                      

                </div>

              </div>

            </div>

          </div>

      </div> <!-- / owl slider item1 --> 
   
      <div class="item">

        <img class="img-responsive owl_testimonial_img" src="<?php echo base_url().'assets/images/home_slider_blank_bg.png'; ?>" alt="banner"/>

          <div class="owl_testimonial_slide">

            <div class="container">

              <div class="row">

                <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">

                  <div class="testimonial_slide_wrap">

                    <div class="testimonial_slide_inner">

                      <p class="testimonial-text">"The Equity Crest partnership and their business approach really came handy at the right time for us to ride our next growth curve" <br><span class="testimonial-person">Shailesh Mehta, </span><span class="testimonial-company-name">Founder – Joybynature</span></p>
                        <!-- <p class="testimonial-view-more"><a href="#" target="_blank"><button class="btn" type="button">View more testimonials</button></a></p> -->

                    </div>  

                  </div>                                      

                </div>

              </div>

            </div>

          </div>

      </div> <!-- / owl slider item2 -->  
    </div> <!-- /owl_home_slider -->

</section>  

