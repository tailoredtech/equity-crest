<?php
if(isset($panels)){
    $sector = json_encode($panels['sector']);
    $location = json_encode($panels['location']);
}
?>

<section id="chart-section">
    <div>
        <?php if(isset($panels)){ ?>
            
            <h3 id="chart-title" class="section-title text-center">Funding Statistics<span class="distribution_statistics"><sup><sup>*</sup></sup></span></h3>
            
            <div id="chart-container" class="container">
                <div class="row distribution_statistics">
                <div class="col-lg-3 col-sm-6 col-xs-12 col-md-6 text-center">
                  <p><em>Startup Selection Rate</em></p>
                  <h2>2%</h2>
                </div>
                <div class="col-lg-3 col-sm-6 col-xs-12 col-md-6 text-center">
                  <p><em># of Startups funded</em></p>
                  <h2>5</h2>
                </div>
                <div class="col-lg-3 col-sm-6 col-xs-12 col-md-6 text-center">
                  <p><em>Funding Success Rate</em></p>
                  <h2>55%</h2>
                </div>
                <div class="col-lg-3 col-sm-6 col-xs-12 col-md-6 text-center">
                  <p><em>Total funds raised</em></p>
                  <h2>19</h2>
                  <em>(Rs. cr)</em>
                  <br><br>
                </div>
                <div class="text-right imp_note"><em>*For the year ended 31st March 2016</em></div>

              </div>
					
                <div class="labeled-chart-container chart-box chart-box-left col-lg-6 col-md-6 col-sm-6 col-xs-12">
	                <div id="chart1-legend" class="custom-legend custom-legend-left hidden-lg hidden-md hidden-sm hidden-xs"></div>
                        <canvas id="chart1" width="0" height="0"></canvas>
                    
                </div>
                
                <div class="labeled-chart-container chart-box chart-box-right col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <canvas id="chart2" width="0" height="0"></canvas>
                    <div id="chart2-legend" class="custom-legend custom-legend-right hidden-lg hidden-md hidden-sm hidden-xs"></div>
                    
                </div>

                
            </div>    
        
 <?php } ?>
    </div>
</section>

<!-- DC Toggle 4 Start -->
<section class="home_media_section">
 
 <div class="container">

    <div class="row">
     
        <!--   - - - In media section - - - - -   -->
        <div class="col-md-6 col-sm-12 col-xs-12 home_in_the_media">

          <h2 class="section-title">In the Media</h2>

          <div class="in_the_media_list">
              <div class="row in_the_media_item">
                <div class="col-sm-6 col-xs-6 col-mobile">
                  <div class="in-media-img"> <img class="img-responsive" src="<?php echo base_url(); ?>assets/images/image.png"></div>
                </div>
                <div class="col-sm-6 col-xs-6 col-mobile">
                  <h4>The Economic Times</h4>
                  <p>14th January, 2016. Column: Why startups are now swearing by crowdfunding platforms like Grex and Equity Crest to raise money</p>

                  <span class="read_more"><a href="<?php echo base_url()?>home/news_top_stories" target = "_blank">Read More</a></span>

                </div>
              </div>   

              <div class="row in_the_media_item">
                <div class="col-sm-6 col-xs-6 col-mobile">
                  <div class="in-media-img"><img class="img-responsive" src="<?php echo base_url(); ?>assets/images/YourStory-Drivojoy.jpg"></div>
                </div>
                <div class="col-sm-6 col-xs-6 col-mobile">
                  <h4>Drivojoy funding</h4>
                  <p>It raises Rs.4 Crores from IAN, Tessellate Ventures, Equity Crest and Traxcn Labs</p>
                  <span class="read_more"><a href="http://yourstory.com/2016/03/drivojoy-funding" target="_blank">Read More</a></span>
                </div>
              </div> 

              <div class="row in_the_media_item">
                <div class="col-sm-6 col-xs-6 col-mobile">
                  <div class="in-media-img"> <img class="img-responsive" src="<?php echo base_url(); ?>assets/images/in_media1.jpg"></div>
                </div>
                <div class="col-sm-6 col-xs-6 col-mobile">
                  <h4>The Economic Times</h4>
                  <p>29 April, 2015. Column: Guru Gyaan – Amit Banka talks about starting up in the showbiz sector</p>

                  <span class="read_more"><a href="<?php echo base_url()?>home/news_top_stories" target = "_blank">Read More</a></span>

                </div>
              </div>   
        
              <div class="row in_the_media_item">
                <div class="col-sm-6 col-xs-6 col-mobile">
                  <div class="in-media-img"><img class="img-responsive" src="<?php echo base_url(); ?>assets/images/in_media2.jpg"></div>
                </div>
                <div class="col-sm-6 col-xs-6 col-mobile">
                  <h4>Bloomberg TV</h4>
                  <p>27 April, 2015. Show: Wealth Manager – Amit Banka discusses the ‘Risks & Rewards of Angel Investing’</p>
                  <span class="read_more"><a href="https://www.youtube.com/watch?v=_JlLjKKzmig" target="_blank">Read More</a></span>
                </div>
              </div>

              <div class="row in_the_media_item">
                <div class="col-sm-6 col-xs-6 col-mobile">
                  <div class="in-media-img"><img class="img-responsive" src="<?php echo base_url(); ?>assets/images/in_media3.jpg"></div>
                </div>
                <div class="col-sm-6 col-xs-6 col-mobile">
                  <h4>The Economic Times</h4>
                  <p>14 October, 2014. Coverage : Anchor article about ‘How deal discovery tools built by Equity Crest help investors connect with start-ups’</p>
                  <span class="read_more"><a href="http://articles.economictimes.indiatimes.com/2014-10-14/news/55014300_1_investors-angel-investing-amit-banka" target="_blank">Read More</a></span>
                </div>
              </div> 

              <div class="row remove_margin in_the_media_item view_more"><a href="<?php echo base_url(); ?>home/news_top_stories">View more media articles</a></div>

          </div> <!-- ./the_media_list -->
    
        </div>
        <!--   - - - End In media section - - - - -   -->


        <!--   - - - Blog section start- - - - -   -->
        <div class="col-md-6 col-sm-12 col-xs-12 home_in_the_media home_blog_list">

          <?php $this->load->view('front-end/blog');?>

    
        </div>
        <!--   - - - Blog section end - - - - -   -->            

      </div> <!-- /.row -->
  </div>

</section>
<!-- DC Toggle 4 End -->
<script>
 $(document).ready(function() {
    //$(".deal-link").delegate('a', 'click', function(e){ e.stopImmediatePropagation(); })
  })
</script>

    <?php if(isset($panels)){ ?>
      <?= js('Chart.min.js') ?>
    <?php } ?>  
<script>
    <?php 
    if(isset($panels)){
    ?>
    
	    $(document).ready(function(){
	    
	      $id = function(id){
	        return document.getElementById(id);
	      },
	      
	      helpers = Chart.helpers;
	      Chart.defaults.global.responsive = true; 
		  
		  createDoughnutChartWithId('chart1', <?php echo $sector;?>);	
		  
		  createDoughnutChartWithId('chart2', <?php echo $location;?>);	
		
	    });
	    
	    function createDoughnutChartWithId(id, data)
	    { 
		    var canvas = $id(id);
	
	        var moduleData = <?php echo $sector ?>;
	        console.log(moduleData);
	        
	        var chartCutout = 90;
	        
	        var moduleDoughnut = new Chart(canvas.getContext('2d')).Doughnut(data, { tooltipTemplate : "<%if (label){%><%=label%><%}%>", animation: false, percentageInnerCutout:chartCutout});
	        
	        
	        var legendHolder = document.getElementById(id+'-legend');
	        legendHolder.innerHTML = moduleDoughnut.generateLegend();
	        
	        // Include a html legend template after the module doughnut itself
	        helpers.each(legendHolder.firstChild.childNodes, function(legendNode, index){
	          helpers.addEvent(legendNode, 'mouseover', function(){
	            var activeSegment = moduleDoughnut.segments[index];
	            activeSegment.save();
	            activeSegment.fillColor = activeSegment.highlightColor;
	            moduleDoughnut.showTooltip([activeSegment]);
	            activeSegment.restore();
	          });
	        });
	        helpers.addEvent(legendHolder.firstChild, 'mouseout', function(){
	          moduleDoughnut.draw();
	        });
	        $(id+'-legend').append(legendHolder.firstChild);
	        $('#'+id+'-legend-mobile').append($('#'+id+'-legend').html());
	    }
    
    <?php 
    }
    ?>
</script>
