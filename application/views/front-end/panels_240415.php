<?php
if(isset($panels)){
    $sector = json_encode($panels['sector']);
    $location = json_encode($panels['location']);
}
?>
<!-- DC Toggle 4 Start -->
<section class="dc_ac-container" style="width: 100%;">
  <div>
    <input id="toggle-ac-1" name="accordion-1" type="checkbox" />
    <label for="toggle-ac-1" class="top-left"><img src="<?php echo base_url(); ?>assets/images/top-menu-arrow.png" /> Platform Statistics</label>
        <!-- p>Coming Soon !</p -->

          <?php if(isset($panels)){ ?>
            <article class="content-main ac-hauto">
              <!-- div class="chart-container" -->
                <div class="features chart-container row">
                <div class="col-sm-6 article pull-left platform-stat-divide">
                  <!--h3></h3-->
                  <div class="labeled-chart-container">
                    <div class="canvas-holder">
                      <canvas id="myChart" width="150" height="150"></canvas>
                      <!-- ul class="doughnut-legend" -->
                      </ul>
                    </div>
                  </div>  
                  <div class="platform-stat-title"><h4>Sector-wise Distribution</h4></div>
                </div>
                <div class="col-sm-6 article pull-left">
                  <!--h3></h3-->
                  <div class="labeled-chart-container">
                    <div class="canvas-holder">
                      <canvas id="myChart2" width="150" height="150"></canvas>
                      <!-- ul class="doughnut-legend" -->
                      </ul>
                    </div>
                  </div>  
                  <div class="platform-stat-title"><h4>Location-wise Distribution</h4></div>
                </div>
                </div>
              <!-- /div -->
            </article>
        <?php }  else { ?>
              <article class="ac-small">
              <div class="panel-body">
             <div class="row">
             <p class="text-center">Coming Soon</p>
             </div> 
            </div>
            </article>
        <?php } ?>
        
 </div>
  <!-- div>
    <input id="toggle-ac-2" name="accordion-1" type="checkbox" />
    <label for="toggle-ac-2"><img src="<?php echo base_url(); ?>assets/images/top-menu-arrow.png" /> Bids & Offers</label>
    <article class="ac-small" style="height: auto;">
      <div class="panel-body">
        <p>Coming Soon !</p>
      </div>
    </article>
  </div>
  <div>
    <input id="toggle-ac-3" name="accordion-1" type="checkbox" />
    <label for="toggle-ac-3"><img src="<?php echo base_url(); ?>assets/images/top-menu-arrow.png" /> Portfolio in Media</label>
    <article class="ac-small">
      <div class="panel-body">
        <p>Coming Soon !</p>
      </div>
    </article>
  </div -->
  <div>
    <input id="toggle-ac-4" name="accordion-1" type="checkbox" />
    <label for="toggle-ac-4"><img src="<?php echo base_url(); ?>assets/images/top-menu-arrow.png" /> EQ in Media</label>
    <article class="ac-xxlarge">
        <div class="panel-body">
            <h5 class="lato-bold">Equity Crest featured in The Economic Times</h5>
            <p>As we work hard on building the platform, we recently got a boost to our morale when The Economic Times covered Equity Crest in their article. <a href="http://epaperbeta.timesofindia.com/Article.aspx?eid=31815&articlexml=Deal-Discovery-Tools-Turn-Angels-Wands-14102014001083" target="_blank">Click Here</a> to read the article</p>
            <img class="text-center img-responsive center-block" style="margin-right:auto;" src="<?php echo base_url(); ?>assets/images/et.jpg">
        </div>
    </article>
  </div>
</section>
<!-- DC Toggle 4 End -->
<script>
 $(document).ready(function() {
    //$(".deal-link").delegate('a', 'click', function(e){ e.stopImmediatePropagation(); })
  })
</script>

    <?php if(isset($panels)){ ?>
      <?= js('Chart.min.js') ?>
    <?php } ?>  
<script>
    <?php 
    if(isset($panels)){
    ?>
    $(document).ready(function(){
        // Colour variables
      var red = "#bf616a",
          blue = "#5B90BF",
          orange = "#d08770",
          yellow = "#ebcb8b",
          green = "#a3be8c",
          teal = "#96b5b4",
          pale_blue = "#8fa1b3",
          purple = "#b48ead",
          brown = "#ab7967";

      $id = function(id){
        return document.getElementById(id);
      },
      
      helpers = Chart.helpers;
      Chart.defaults.global.responsive = true; 

      // Modular doughnut
      (function(){
        var canvas = $id('myChart'),
          colours = {
            "Core": blue,
            "Line": orange,
            "Bar": teal,
            "Polar Area": purple,
            "Radar": brown,
            "Doughnut": green
          };

        var moduleData = <?php echo $sector ?>;
        console.log(moduleData);
        
        var moduleDoughnut = new Chart(canvas.getContext('2d')).Doughnut(moduleData, { tooltipTemplate : "<%if (label){%><%=label%><%}%>", animation: false });
        // 
        var legendHolder = document.createElement('div');
        legendHolder.innerHTML = moduleDoughnut.generateLegend();
        // Include a html legend template after the module doughnut itself
        helpers.each(legendHolder.firstChild.childNodes, function(legendNode, index){
          helpers.addEvent(legendNode, 'mouseover', function(){
            var activeSegment = moduleDoughnut.segments[index];
            activeSegment.save();
            activeSegment.fillColor = activeSegment.highlightColor;
            moduleDoughnut.showTooltip([activeSegment]);
            activeSegment.restore();
          });
        });
        helpers.addEvent(legendHolder.firstChild, 'mouseout', function(){
          moduleDoughnut.draw();
        });
        canvas.parentNode.parentNode.appendChild(legendHolder.firstChild);
      })();


      (function(){
        var canvas = $id('myChart2'),
          colours = {
            "Core": blue,
            "Line": orange,
            "Bar": teal,
            "Polar Area": purple,
            "Radar": brown,
            "Doughnut": green
          };

        var moduleData = <?php echo $location ?>;
        
        var moduleDoughnut = new Chart(canvas.getContext('2d')).Doughnut(moduleData, { tooltipTemplate : "<%if (label){%><%=label%><%}%>", animation: false });
        // 
        var legendHolder = document.createElement('div');
        legendHolder.innerHTML = moduleDoughnut.generateLegend();
        // Include a html legend template after the module doughnut itself
        helpers.each(legendHolder.firstChild.childNodes, function(legendNode, index){
          helpers.addEvent(legendNode, 'mouseover', function(){
            var activeSegment = moduleDoughnut.segments[index];
            activeSegment.save();
            activeSegment.fillColor = activeSegment.highlightColor;
            moduleDoughnut.showTooltip([activeSegment]);
            activeSegment.restore();
          });
        });
        helpers.addEvent(legendHolder.firstChild, 'mouseout', function(){
          moduleDoughnut.draw();
        });
        canvas.parentNode.parentNode.appendChild(legendHolder.firstChild);
      })();
    });
    <?php 
    }
    ?>
</script>  