  <!--this menu will be on top of the push wrapper-->
  <nav class="st-menu st-effect-2 hidden" id="menu-2">
    <ul>
      <li><a class="icon icon-data" href="<?php echo base_url(); ?>investee/index">Profile</a></li>
      <li><a class="icon icon-location" id="expert_navbar" name="expert_navbar" href="#">Ask for Expert Advice</a></li>
      <li><a class="icon icon-study" href="<?php echo base_url(); ?>investee/meeting">Meeting Scheduler</a></li>
      <li><a class="icon icon-photo" href="<?php echo base_url(); ?>investee/offers">Bids & offers</a></li>
      <li>
      <ul id="navres">
        <li id="notification_lires">
          <?php if ($unseen_notifications > 0) { ?>  
          <span id="notification_countres"><?php echo $unseen_notifications; ?></span>
          <?php } ?>
          <a href="#" id="notificationLinkres"><span class="notification-icon"></span></a>
          <div id="notificationContainerres">
              <div id="notificationTitleres">Notifications</div>
              <?php if (count($notifications) > 0) { ?>
              <div id="notificationsBodyres" class="notificationsres">
                  <?php foreach($notifications as $notification){ ?>
                      <!-- a class="notify-url" href="<?php echo base_url().$notification['link']; ?>" -->
                      <div class="notificationres">
                          <!-- span class="notification-thumb img-notificationres"><img src="<?php echo base_url(); ?>uploads/users/<?php echo $notification['sender_id'] ?>/<?php echo $notification['image']; ?>')"></span --> 
                          <span class="notification-textres"><?php echo $notification['display_text']; ?></span><br>
                          <span class="notification-timeres"><?php echo date('M, d H:i A',strtotime($notification['date_created'])); ?></span>
                          <span class="border"></span>
                      </div>
                      <!-- /a -->          
                  <?php } ?>
              </div>        
              <div id="notificationFooterres"><a href="<?php echo base_url().$this->session->userdata('role')  ?>/notifications">See All</a></div>       
              <?php } else {?>
              <div id="notificationsBodyres" class="notificationsres">
                  <div class="notificationres">
                      <span class="notification-textres">No notifications yet</span>
                      <span class="border"></span>
                  </div>
              </div>        
              <?php } ?>
          </div>
        </li>
      </ul>
      </li>
     
      <script type="text/javascript" >
      $(document).ready(function()
      {
        $("#notificationLinkres").click(function()
        {
          $("#notificationContainerres").fadeToggle(300);
          $("#notification_countres").fadeOut("slow");
          return false;
          });

        //Document Click
        $(document).click(function()
        {
          $("#notificationContainerres").hide();
        });
        
        //Popup Click
        $("#notificationContainerres").click(function()
        {
          return false
        });

      });
      </script>
    </ul>
  </nav>

