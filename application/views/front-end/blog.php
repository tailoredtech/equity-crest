     <h2 class="section-title">Blog</h2>

      <div class="in_the_media_list">
        <?php foreach($blogs as $blog_data) {
         ?>
          <div class="row in_the_media_item">
            <div class="col-sm-6 col-xs-6 col-mobile">
              <div class="in-media-img"> 
              <?php if(isset($blog_data['image_link'])){ ?>
              <img class="img-responsive" src="<?=$blog_data['image_link']?>"></div>
              <?php }else{ ?>
              <img class="img-responsive" src="<?php echo base_url(); ?>assets/images/testimonial_bg.jpg"></div>
              <?php } ?>
            </div>
            <div class="col-sm-6 col-xs-6 col-mobile">
              <h4><?=$blog_data['title']?></h4>
              <p><?=$blog_data['description']?></p>
              <span class="read_more"><a href="<?php echo $blog_data['link']?>" target="_blank">Read More</a></span>
            </div>
          </div>   
        <?php } ?>    
          
          <div class="row remove_margin in_the_media_item view_more"><a target="_blank" href="http://www.equitycrest.wordpress.com/">View more blog articles</a></div>

      </div> <!-- ./the_media_list -->

