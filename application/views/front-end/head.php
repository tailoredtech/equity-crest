
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="height=device-height,width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <title>Fund Startups | Angel/VC Investing | Equity Crest</title>

    <meta name="description=" content="Equity Crest is one-of-its-kind platform for startups & entrepreneurs to get funding for their ventures. Startups/Entrepreneurs can connect with investors from across the world through Equity Crest.
          We have built a world class ecosystem that promotes growth of new and young businesses, and creates long term value for all our stakeholders.">
    <?php include_once("analyticstracking.php") ?>

        <?= css('bootstrap.min.css') ?>
        <?= css('stylesheet.css') ?> 
        

        <!-- ?= css('normalize.css') ?  no need -->
        <!-- ?= css('component.css') ? -->        
        <?= css('jquery.slick.css') ?>
        <!--?= css('slick.css') ? -->
        <?= css('flyoutmenu.css') ?>
        <?= css('drop-down-menu-css.css') ?>
        <?= css('../tabs/dc_accordion_toggle.css') ?>
        <?= css('jquery.datetimepicker.css') ?>        
        <?= css('colorbox.css') ?>
        <!-- ?= css('style.css') ? -->
        <!-- ?= css('style1.css') ? -->
		<?= css('owl.carousel.css') ?>
		<?= css('owl.theme.css') ?>
        <?= css('view.css') ?> 
        <?= css('../custom-scrollbar/jquery.mCustomScrollbar.css') ?>

        <?= js('jquery-1.11.0.min.js') ?>
        <?= js('modernizr.custom.js') ?>
        <?= js('jquery.min.js') ?>       
        <?= js('bootstrap.min.js') ?>
        <?= js('jquery.datetimepicker.js') ?>
        <?= js('jquery.validate.min.js') ?>          
        <?= js('jquery.colorbox.js') ?>
	    <?= js('owl.carousel.js') ?>
	    <?= js('jquery.lazyload.min.js') ?>	





