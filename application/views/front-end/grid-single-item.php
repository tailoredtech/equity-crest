<?php 
	
	$md_size = 6;
	if($colsize == 3) {
		$md_size = 4;
	}
	
	else {
		$md_size = 6;
	}
	
	$box_classes = "";
	if(isset($other_classes)) {
		$box_classes = $other_classes;
	}	
?>
<div class="item col-lg-<?=$colsize?> col-md-<?=$md_size;?> col-sm-6 col-xs-6 col-mobile <?= $box_classes;?>">
   <div id="i-<?php echo $user['user_id'] ?>" class="deal-single">
	   
	    <?php if (($this->session->userdata('role') == 'investor' || $this->session->userdata('role') == 'channel_partner' || $this->session->userdata('user_id') == $user['user_id']) && $user['status'] != 'funded') 
         {

         ?>
	    <a href="<?php echo base_url(); ?>investee/investee_profile/<?php echo $user['slug_name'] ?>" class="company_click" cname="<?php echo $user['company_name']?>">
		    
    <?php }
	   
	   else if ($this->session->userdata('role') == 'media' || $this->session->userdata('role') == 'investee' || $user['status'] == 'funded') 
       {
	          
	   }
   
	   else
	   { 
   ?>
      <a href="<?php echo base_url(); ?>user/login">
    <?php } ?>  
      <div class="deal-link">
         <div class="deal-logo text-center">
            <div class="deal-image"> 
              <?php if($user['banner_image']){ ?>   
               <img class="equity-lazy" src="<?php echo base_url().$user['banner_image'];?>" data-original="<?php echo base_url().$user['banner_image'];?>"> 
               <?php }else{ ?>          
               <img src="<?php echo base_url().'assets/images/company_default.jpg';?>">  
               <?php } ?>                  
            </div>
            <div class="deal-category "><?php echo $user['sector_name'] ?></div> 
         </div>

         <div class="deal-middel-box">
            <div class="row remove_margin">
               <div class="deal-company-logo vertical-align-parent-box">                  
                  <?php if ($this->session->userdata('user_id') || $user['show_details'] == '1') { ?> 
                     <img src="<?php echo base_url(); ?>uploads/users/<?php echo $user['user_id'] ?>/<?php echo $user['image'] ?>" class="img-responsive vertical-align-child-box">
                  <?php } else { ?>
                     <img class="" src="<?php echo base_url().'assets/images/company_default_50x50.jpg';?>">
                  <?php } ?>                   
               </div>

               <div class="deal-company-title">
                  <div class="title">            
                     <div class="deal-title-block">
                     <?php if ($this->session->userdata('user_id') || $user['show_details'] == '1') { ?> 
                        <h2 class="deal-title"><?php echo $user['company_name'] ?></h2>
                     <?php } else { ?>
                        <h2 class="deal-title">Company</h2>
                     <?php } ?>          
                        <div class="deal-location"><?php echo $user['city'] ?></div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="deal-description">
               <p><?php echo character_limiter(strip_tags($user['products_services']), 60); ?></p>
            </div>
         </div>

         <div class="deal-funds row remove_margin">
		 	
            <?php if ($user['status'] == 'active' || $user['status'] == 'funded') { ?>
            <div class="fund-requested col-lg-12 col-md-12 col-sm-12 col-xs-12 remove_padding">                
                <div class="funds-req-amount pull-left">
                     &#8377; <?php echo format_money($user['investment_required']); ?>                  
                </div>

                <?php if ($this->session->userdata('role') == 'channel_partner'){
                 ?>
                  <!--   <div class="funds-req-status pull-right">
                      <span class="fund_follow_icon">
                          <!-- <img src="<?php echo base_url(); ?>assets/images/fund_follow_icon.png" alt="Follow"> -->
                         <!-- <img src="<?php echo base_url(); ?>assets/images/fund_follow_active_icon.png" alt="Follow">
                      </span>
                      <span class="fund_follow_pledge_icon">
                          <img src="<?php echo base_url(); ?>assets/images/fund_follow_pledge_icon.png" alt="Pledge">
                          <!-- <img src="<?php echo base_url(); ?>assets/images/fund_follow_pledge_active_icon.png" alt="Pledge"> -->
                    <!--  </span>                                       
                    </div> -->

                  <?php } elseif($this->session->userdata('role') == 'investor' && $user['status'] != 'funded') { ?>
                    <?php  ?>
                    <div class="funds-req-status pull-right">
                        <?php $followed = $this->session->userdata('followed'); 
                            if(is_array($followed)){
                         if(in_array($user['user_id'],$followed)){ ?>
                               <span class="fund_follow_icon follow-<?=$user['user_id']?>">
                                <img src="<?php echo base_url(); ?>assets/images/fund_follow_active_icon.png" alt="Unfollow">                
                              </span>               
                        <?php }else{ ?>
                               <span class="fund_follow_icon follow-<?=$user['user_id']?>">          
                                  <img src="<?php echo base_url(); ?>assets/images/fund_follow_icon.png" alt="Follow">
                              </span>
                        <?php }  }else{ ?>  
                            <span class="fund_follow_icon follow-<?=$user['user_id']?>">          
                                  <img src="<?php echo base_url(); ?>assets/images/fund_follow_icon.png" alt="Follow">
                              </span>
                            <?php } ?>
                        
                        <?php $pledged = $this->session->userdata('pledged'); 
                         if(is_array($pledged)){
                                
                         if(in_array($user['user_id'],$pledged)){ ?>
                            <span class="fund_follow_pledge_icon pledge-<?=$user['user_id']?>">          
                              <img src="<?php echo base_url(); ?>assets/images/fund_follow_pledge_active_icon.png" alt="Pledged">
                            </span> 
                        <?php }else{ ?>
                            <span class="fund_follow_pledge_icon pledge-<?=$user['user_id']?>">
                              <img src="<?php echo base_url(); ?>assets/images/fund_follow_pledge_icon.png" alt="Pledge">          
                            </span>
                        <?php }  }else{ ?>
                            <span class="fund_follow_pledge_icon pledge-<?=$user['user_id']?>">
                              <img src="<?php echo base_url(); ?>assets/images/fund_follow_pledge_icon.png" alt="Pledge">          
                            </span>
                        <?php } ?>    
                    </div>
                  <?php } ?>

            </div>
            <?php 
             $fund_raise = $user['fund_raise'];
             $investment_required = $user['investment_required'];
             if($investment_required){
                 $percent = ($fund_raise/$investment_required)*100;
             }else{
                 $percent = 0;
             }
             
            ?>          
            <div class="funds-bar col-lg-12 col-md-12 col-sm-12 col-xs-12 remove_padding">
                <div class="progress" style="height: 6px;">
                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $percent; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $percent,'%'; ?>; background: rgb(0,166,50);"></div>
                    <span class="sr-only">60% Complete</span>
                </div>
            </div>
            <div class="fund-stats col-lg-12 col-md-12 col-sm-12 col-xs-12 remove_padding">
               
                <div class="funds-raised middle col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <span><strong>&#8377; <?php echo format_money($user['commitment_per_investor']); ?></strong></span>
                    <p>Min. Investment</p>
                </div>

                <div class="funds-raised col-lg-6 col-md-6 col-xs-6">
                    <div id="raised-percent"><strong><?php echo floor($percent); ?>%</strong></div>
                    <p>of funds raised</p>
                </div>
               
            </div>
            <?php } else { ?>
                <div class="semi_active col-lg-12 col-md-12 col-sm-12 col-xs-12 comingsoonstrip">
                    Coming Soon                  
                </div>
            <?php }  ?>

        </div> <!-- ./deal-funds -->
    </div>
    </a>



    <?php if ($this->session->userdata('role') == 'channel_partner'){ ?>
		<div class="deal-footer-links">
			<div class="col-xs-6 text-center">&nbsp;</div>
			<!-- div class="col-xs-4 text-center">&nbsp;</div -->
			<div class="col-xs-6 text-center">&nbsp;</div>
		</div>
	<?php } elseif($this->session->userdata('role') == 'investor'  && $user['status'] != 'funded') { ?>
    <?php  ?>
    <div class="deal-footer-links">
        <?php 

        $followed = $this->session->userdata('followed'); 
        if(is_array($followed)){
         if(in_array($user['user_id'],$followed)){ ?>
               <div class="col-xs-6 text-center"><a href="javascript:void(0);" class="text-center unfollow" data-user-id="<?php echo $user['user_id']; ?>" data-cname = "<?php echo "unfollow_".$user['company_name']; ?>" ><img src="<?php echo base_url(); ?>assets/images/fund_follow_active_40x40.png" alt="Unfollow">
                <h4>Unfollow</h4></a></div>
        <?php }else{ ?>
               <div class="col-xs-6 text-center"><a href="javascript:void(0);" class="text-center follow" data-user-id="<?php echo $user['user_id']; ?>" data-cname = "<?php echo "follow_".$user['company_name']; ?>" ><img src="<?php echo base_url(); ?>assets/images/fund_follow_40x40.png" alt="Follow">
                <h4>Follow</h4></a></div>
        <?php }  }else{ ?>  
            <div class="col-xs-6 text-center"><a href="javascript:void(0);" class="text-center follow" data-user-id="<?php echo $user['user_id']; ?>" data-cname = "<?php echo "follow_".$user['company_name']; ?>"><img src="<?php echo base_url(); ?>assets/images/fund_follow_40x40.png" alt="Follow">
                <h4>Follow</h4></a></div>
            <?php } ?> 
            
        <!-- div class="col-xs-4 text-center"><a href="javascript:void(0);" class="text-center ignore" data-user-id="<?php echo $user['user_id']; ?>">Ignore</a></div -->
        
        <?php $pledged = $this->session->userdata('pledged'); 
         if(is_array($pledged)){
                
         if(in_array($user['user_id'],$pledged)){ ?>
            <div class="col-xs-6 text-center"><a href="javascript:void(0);" class="text-center pledged" data-user-id="<?php echo $user['user_id']; ?>" data-cname = "<?php echo "pledged_".$user['company_name']; ?>" ><img src="<?php echo base_url(); ?>assets/images/fund_follow_pledge_active_40x40.png" alt="Pledged">
                <h4>Pledged</h4></a></div>
        <?php }else{ ?>
            <div class="col-xs-6 text-center"><a href="javascript:void(0);" class="text-center pledge" data-user-id="<?php echo $user['user_id']; ?>" data-cname = "<?php echo "pledge_".$user['company_name']; ?>"><img src="<?php echo base_url(); ?>assets/images/fund_follow_pledge_40x40.png" alt="Pledge">
                <h4>Pledge</h4></a></div>
        <?php }  }else{ ?>
            <div class="col-xs-6 text-center"><a href="javascript:void(0);" class="text-center pledge" data-user-id="<?php echo $user['user_id']; ?>" data-cname = "<?php echo "pledge_".$user['company_name']; ?>"><img src="<?php echo base_url(); ?>assets/images/fund_follow_pledge_40x40.png" alt="Pledge">
                <h4>Pledge</h4></a></div>
        <?php } ?>    
    </div>
	<?php } ?>



</div>
</div><!-- post #1 -->