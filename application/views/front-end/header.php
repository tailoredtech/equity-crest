<style type="text/css">
#mini_entrepreneur {
     background: #0C458A none repeat scroll 0 0;
    bottom: 0;
    padding: 1px 10px;
    position: fixed;
    right: 0;
    text-align: center;
    width: 480px;
    z-index: 99999;
    color: white;
    opacity: 0.75;
    border-radius: 17px 17px 0px 0px;
    height: 120px;
    font-size: 16px;
}
.register{
     width: 500px;
     height: 200px;
}

</style>

        <?php 
   
        if(null !=($this->session->userdata('user_id')))
        {
           $this->session->set_userdata('sticky_pop_up' , '0') ;
        }

       /* if( null ==($this->session->userdata('home_page_visit_count')))
        {
             $this->session->set_userdata('home_page_visit_count' , 1) ;
        }
        else
        {
            $home_page_visit_count = $this->session->userdata('home_page_visit_count');
            $home_page_visit_count++ ;
            $this->session->set_userdata('home_page_visit_count' , $home_page_visit_count) ;
        }

        $home_page_visit_count = $this->session->userdata('home_page_visit_count');*/

        if(null !=  $this->session->userdata('sticky_pop_up') )
        {
            $sticky_pop_up = $this->session->userdata('sticky_pop_up');
        }
        else
          $sticky_pop_up = "";

        if(($this->uri->segment(2) == 'index' || $this->uri->segment(2) == '') && $this->session->userdata('role') != 'investee' && $this->session->userdata('role') != 'investor'){ ?>

        <style type="text/css">

        footer{ margin-top: 0}

        @media (max-width: 767px) { .section-title, .page-title{ text-align: center;} }
        </style>

        <?php } ?> 




<div class="modal fade" id="registerModalEntrepreneur" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel1">
    <div class="modal-dialog register" role="document">
        <div class="modal-content">
            <div class="modal-header">        
                <h2 class="modal-title text-center" id="registerModalLabel1">Entrepreneur?</h2>
            </div>
            <div class="modal-body">
                <div class="register-box"> 
                    <div class="row remove_margin failure-msg" style="display:none">
                    </div>
                    <div class="register-box-innner register-box-1">
                        <div class="row">
                            <div  class="form-group" style="text-align: center">
                               Want to get an assessment of your business for a fund raise? 
                               <a href="<?php echo base_url().'user/register_user?role=investee&entrepreneur=1';?>">Click here</a>
                            </div>
                        </div>
                    </div> <!-- /.register-box-1 -->                    
                </div>  <!-- /.register-box -->
            </div> <!-- /.modal-body -->
        </div>
    </div>
</div>
<div id="mini_entrepreneur" style="display:none" >
   

   <button type="button" id="closeButton" class="close"  aria-label="Close"><span style="color:white" aria-hidden="true">&times;</span></button>
<div style="">
<h3 style="font-size:25px; color:white ;"> Entrepreneur ? </h3>
  <span>Want to get an assessment of your business for a fund raise?  <br><a  style="color:white ; text-decoration:underline" href="<?php echo base_url().'user/register_user?role=investee&entrepreneur=1';?>">Click here</a>

 </span>
</div>
</div>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">

        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#eq-navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>

          <?php 
          if($this->session->userdata('user_id')){ ?>
                    <a href="<?php echo base_url(); ?>" ><img class="img-responsive" src="<?php echo base_url(); ?>assets/images/eq_new_logo.png" alt="equitycrest"/></a>
                <?php }else{ ?>
                    <a href="<?php echo base_url(); ?>"><img class="img-responsive" src="<?php echo base_url(); ?>assets/images/eq_new_logo.png" alt="equitycrest"/></a>
            <?php } ?>
          
        </div>


        <div class="collapse navbar-collapse" id="eq-navbar-collapse">
          <ul class="nav navbar-nav navbar-left">

             <?php if(!$this->session->userdata('user_id')){ ?>
                           
<!--                 <li>
                    <a href="<?php // echo base_url(); ?>user/login?user=investor">
                        Investor
                    </a>
                </li> -->
              
          <?php  } else {
           if($this->session->userdata('role') !='channel_partner')
            { ?>
                <li>
                 <form id="header_search_frm" action="<?php echo base_url(); ?>home/search" method="get">
                    <div class="form-group">
                      <input type="text" name="name" value="" class="form-control sidebar-filter-search"  placeholder="Search for an opportunity" >
                    </div>
                </form>

                 </li>

                  <?php } ?>


                <li <?php if($this->uri->segment(2) == 'search'  || $this->uri->segment(2) == 'portfolio_all'){ ?>class="active" <?php } ?>>
                    <a href="<?php echo base_url(); ?>home/portfolio_all" id="Portfolio">
                   <?php   if($this->session->userdata('role') == 'channel_partner') {?>
                            View Proposals
                          <?php } else {?>
                        Portfolio
                        <?php } ?>
                    </a>
                </li>
                
                <?php if($this->session->userdata('role') == 'channel_partner'): ?>
                <li <?php
                if ($this->uri->segment(2) == 'refer') {
                    echo "class='active'";
                }
                ?>><a href="<?php echo base_url(); ?>channel_partner/refer" id="channel_partner">Refer a Partner</a></li>
                <?php endif; ?>

				<li <?php if($this->uri->segment(2) == 'investor_all'){ ?>class="active" <?php } ?> >
                    <a href="<?php echo base_url(); ?>home/investor_all" id="investor">
                        Investors
                    </a>
                </li>
                
        <?php if($this->uri->segment(1) == 'notifications'){ ?>
                <li class="dropdown active"> 
                  <?php } else {?>
                  <li class="dropdown"><?php } ?>
              
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                       Notifications   
                     <?php  
                        if ($unseen_notifications > 0) { ?>
                       <span class="badge badge-absolute bg-blue"><?php echo $unseen_notifications?></span>
                            <i class="glyph-icon icon-lightbulb"></i>
                            <?php } ?>              
                    </a>

                    <ul class="dropdown-menu notifications-dropdown">
                      <?php 
                    
                      if (count($notifications) > 0) { 
                         ?>
                              <div id="notificationsBodyres" class="notificationsres">
                                  <?php foreach($notifications as $notification){
                                  ?>
                                       <div class="notificationres">
                                        <?php 
                                      
                                        if($notification['status'] != 'funded') {?>
                                        <a class="notify-url" href="<?php echo base_url().$notification['link']; ?>"> <?php } ?>
                                          <span class="notification-textres">
                                            <?php echo $notification['display_text']; ?></span><br>
                                          <span class="notification-timeres">
                                            
                                              <?php echo date('M, d H:i A',strtotime($notification['date_created'])); ?> 

                                          </span>
                                           </a> 
                                          <span class="border"></span>
                                     
                                      </div>
                                        
                                  <?php }  ?>
                              </div>        
                              <div id="notificationFooterres">
                                <a href="<?php echo base_url().$this->session->userdata('role')  ?>/notifications">See All</a>
                              </div>       
                              <?php } else {?>
                              <div id="notificationsBodyres" class="notificationsres">
                                  <div class="notificationres">
                                      <span class="notification-textres">No notifications yet</span>
                                      <span class="border"></span>
                                  </div>
                              </div>        
                              <?php } ?>
                    </ul>
                 </li>



                </li>

                            
            <?php  } //else ?>
            
          </ul>

          <ul class="nav navbar-nav navbar-right">
            <?php if ($this->session->userdata('user_id') != '') { ?>
     
                <li class="dropdown logged-in">
                    <a id="user-img" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        <?php echo $this->session->userdata('name'); ?>

                        <?php if ($this->session->userdata('image') != '') { ?>
                            <img class="img-circle" height="30" width="30" src="<?php echo base_url(); ?>uploads/users/<?=$this->session->userdata('user_id');?>/<?=$this->session->userdata('image')?>">                            
                        <?php } else { ?>
                            <img class="img-circle" height="30" width="30" src="<?php echo base_url(); ?>assets/images/gfxAvatar.jpg">
                        <?php } ?>
                    </a>

                    <ul class="dropdown-menu" role="menu">
                    <?php if($this->session->userdata('role')=='admin'){?>
                        <li><a href="<?php echo base_url(); ?>user/update_investors">Update Investors</a></li>
                   <?php }else { ?>
                    <li><a href="<?php echo base_url(); ?>user/<?=$this->session->userdata('role')?>_info">Profile</a></li>
                    <?php } ?>                                  
                      <!-- <li><a href="<?php echo base_url(); ?>user/account_setting">Change Password</a></li> -->
                      <li><a class="logout" href="<?php echo base_url(); ?>user/logout">Sign Out</a></li>
                    </ul>
                </li>

            <?php } else { ?>
              
                <li class="dropdown <?php echo $this->uri->segment(2)=='login'?'open':''; ?>" id="login-register-link">
                  <a href="<?php echo base_url(); ?>user/login" class="dropdown-toggle <?php echo $this->uri->segment(2)=='login'?'open':''; ?>" data-toggle="dropdown" role="button" aria-expanded="<?php echo $this->uri->segment(2)=='login'?'true':'false'; ?>">Login or Register</a>
                    <div class="dropdown-menu login-drop-down" role="menu">
                                                       
                      <?php 
                      $this->load->view('user/login'); ?>
                      
                    </div>
                </li>
                <!-- <li><a class="login-or hidden-xs" href="#">or</a></li>
                <li><a href="<?php echo base_url(); ?>user/register">Register</a></li> -->
                        
            <?php } ?>

          </ul>

        </div><!-- /.navbar-collapse -->

    </div> <!-- ./container -->

</nav>
<script type="text/javascript">

document.getElementById('closeButton').addEventListener('click', function(e) {
      $.ajax({
        url:"<?php echo base_url('home/sticky_pop_up') ?>",
        type:'POST',
        data:'sticky_pop_up=0',
        success:function(response)
        {
           if(response == 1)
           {
               $("#mini_entrepreneur").show(); 
           }
           else
           {
             $("#mini_entrepreneur").hide(); 
           }
        }
      });
    e.preventDefault();
    this.parentNode.style.display = 'none';
}, false);


$(document).ready(function(){
//var home_page_visit_count = "<?php //echo $home_page_visit_count ?>";
var sticky_pop_up = "<?php echo $sticky_pop_up ?>";
var  show_sticky = "<?php echo $show_sticky ?>";
   /* if(home_page_visit_count == 1)
    {
        $("#registerModalEntrepreneur").modal('show');
    }
   */
   if($.isNumeric(sticky_pop_up))
   {
      if(sticky_pop_up.trim() == '1' && show_sticky)
      {
      
          $("#mini_entrepreneur").show(); 
      }
       
      else
        $("#mini_entrepreneur").hide(); 
   }
   if(sticky_pop_up.trim() == "")
   {
       $.ajax({
        url:"<?php echo base_url('home/sticky_pop_up') ?>",
        type:'POST',
        data:'sticky_pop_up=1',
        success:function(response)
        {
           if(response == 1)
           {
               $("#mini_entrepreneur").show(); 
           }
           else
           {
             $("#mini_entrepreneur").hide(); 
           }
        }
      });
   }



      /* $('#registerModalEntrepreneur').on('hidden.bs.modal', function () {    
       $.ajax({
        url:"<?php echo base_url('home/sticky_pop_up') ?>",
        type:'POST',
        data:'sticky_pop_up=1',
        success:function(response)
        {
           if(response == 1)
           {
               $("#mini_entrepreneur").show(); 
           }
           else
           {
             $("#mini_entrepreneur").hide(); 
           }
        }
      });
           
      /*  }); */


})
</script>

