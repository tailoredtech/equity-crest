
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 sidebar">                    

            <form id="search_frm" action="<?php echo base_url(); ?>home/portfolio_all" method="get">

                <div>
                        <h3 class="page-title2">Filter opportunities</h3>
                </div>


                <div class="sidebar-filter-box">



                    <div class="sidebar-filter-section">

                        <div class="form-group">
                            <input type="text" name="name" value="<?php echo $searchOptions['name']; ?>" class="form-control sidebar-filter-search"  placeholder="Search for an opportunity" >
                        </div>

                        <div>

                            <select  name="sector" class="selectpicker show-tick form-control">
                                <option value="">ALL</option>
                               <?php foreach ($sectors as $sector) { ?>
                                <option value='<?php echo $sector['id']; ?>' <?php echo (isset($searchOptions['sector']) && $searchOptions['sector'] == $sector['id'] ) ? 'selected'  : ''; ?>><?php echo $sector['name']; ?></option>
                            <?php } ?>
                            </select>

                        </div>



                    </div>

                    <div class="sidebar-filter-section">

                        <h4>By stage</h4>
                        <label><input type="checkbox" name="stage[]" value="All" <?php echo (isset($searchOptions['stage']) && in_array('All',$searchOptions['stage'])) ? 'checked'  : ''; ?> />All</label>
                         <?php foreach ($stages as $stage) { ?>
                         <label><input type="checkbox" name="stage[]" value='<?php echo $stage['id']; ?>' <?php echo (isset($searchOptions['stage']) && in_array($stage['id'],$searchOptions['stage'])) ? 'checked'  : ''; ?> ><?php echo $stage['name']; ?></label>
                        <?php } ?>
                    </div>

                    <div class="sidebar-filter-section">

                        <h4>Investment Size</h4>
                        <label><input type="checkbox" name="size[]" value="All" <?php echo (isset($searchOptions['size']) && in_array('All',$searchOptions['size'])) ? 'checked'  : ''; ?> /> All</label>
                       <label><input type="checkbox" name="size[]" value="1" <?php echo (isset($searchOptions['size']) && in_array(1,$searchOptions['size'])) ? 'checked'  : ''; ?> >Upto 10 lakhs</label>
                       <label><input type="checkbox" name="size[]" value="2" <?php echo (isset($searchOptions['size']) && in_array(2,$searchOptions['size'])) ? 'checked'  : ''; ?> >10 lakhs - 50 lakhs</label>
                      <label><input type="checkbox" name="size[]" value="3" <?php echo (isset($searchOptions['size']) && in_array(3,$searchOptions['size'])) ? 'checked'  : ''; ?> >50 lakhs - 1 Crore</label>
                      <label><input type="checkbox" name="size[]" value="4" <?php echo (isset($searchOptions['size']) && in_array(4,$searchOptions['size'])) ? 'checked'  : ''; ?> >1 Crore - 5 Crore</label>
                      <label><input type="checkbox" name="size[]" value="5" <?php echo (isset($searchOptions['size']) && in_array(5,$searchOptions['size'])) ? 'checked'  : ''; ?> >5 Crore & Above</label>
                     </div>

                    <div class="sidebar-filter-section">

                        <h4>Location</h4>

                        <label><input type="checkbox" name="location[]" value="All" <?php echo (isset($searchOptions['location']) && in_array('All',$searchOptions['location'])) ? 'checked'  : ''; ?> /> All</label>
                        <label><input type="checkbox"  name="location[]" value="Mumbai" <?php echo (isset($searchOptions['location']) && in_array('Mumbai',$searchOptions['location'])) ? 'checked'  : ''; ?> >Mumbai</label>
                        <label><input type="checkbox"  name="location[]" value="Delhi" <?php echo (isset($searchOptions['location']) && in_array('Delhi',$searchOptions['location'])) ? 'checked'  : ''; ?> >Delhi</label>
                        <label><input type="checkbox"  name="location[]" value="Chennai" <?php echo (isset($searchOptions['location']) && in_array('Chennai',$searchOptions['location'])) ? 'checked'  : ''; ?> >Chennai</label>
                        <label><input type="checkbox"  name="location[]" value="Bengaluru" <?php echo (isset($searchOptions['location']) && in_array('Bengaluru',$searchOptions['location'])) ? 'checked'  : ''; ?> >Bengaluru</label>
                        <label><input type="checkbox"  name="location[]" value="Hyderabad" <?php echo (isset($searchOptions['location']) && in_array('Hyderabad',$searchOptions['location'])) ? 'checked'  : ''; ?> >Hyderabad</label>
                        <label><input type="checkbox"  name="location[]" value="6" <?php echo (isset($searchOptions['location']) && in_array(6,$searchOptions['location'])) ? 'checked'  : ''; ?> >Others</label>
                    </div>

                </div>


                <div class="sidebar-filter-submit">

                    <a href="#" id="filter-clear">Clear filter</a>

                    <button type="submit" class="btn btn-default">Submit</button>


                </div>  

            </form>

        </div>