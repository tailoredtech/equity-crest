<section class="trending_opportunities list-grid-view"> 

<div class="container">
  <div class="row">
    <div class="col-xs-12 col-sm-8">
      <h2 class="section-title">All</h2>
    </div>
    <div class="col-xs-12 col-sm-4 hidden-sm hidden-xs">
      <ul id="allTab" class="pull-right list-grid-view-tab" role="tablist">
        <li role="presentation">
            <a href="#all_list_view" id="all_list_view-tab" role="tab" data-toggle="tab" aria-controls="all_list_view" aria-expanded="true" class="list-view-tab-icon">
              List
          </a>
        </li>
        
        <li role="presentation" class="active">
          <a href="#all_grid_view" role="tab" id="all_grid_view-tab" data-toggle="tab" aria-controls="all_grid_view" aria-expanded="false" class="grid-view-tab-icon">
            Grid
          </a>
        </li>     
      </ul>
    </div>
  </div>
</div>


<div id="allTabContent" class="tab-content">

<div role="tabpanel" id="all_grid_view" class="tab-pane fade active container in" aria-labelledby="all_grid_view-tab">
    
    <div class="row">

        <?php $max = count($all_users);
        if ($max) { ?>  

                        <?php
                        $count = 0;
                        foreach ($all_users as $user) {
                            $data['user'] = $user;
                            $data['colsize'] = 3;
                            $this->load->view('front-end/grid-single-item', $data);
                            if($count == 3){break;}
                            $count++;
                        }
                        ?>

        <?php } else { ?>
            <div id="w" >
                <div class="alert alert-success" role="alert">There are no portfolios available</div>
            </div>
        <?php } ?>
    </div>


</div>


<div role="tabpanel" class="tab-pane fade container hidden-sm hidden-xs" id="all_list_view" aria-labelledby="all_list_view-tab" >

  <div class="list-view-deals">

    <div class="row remove_margin list-view-deal-header">

      <div class="col-md-3">Opportunity</div>
      <div class="col-md-2">Sector</div>
      <div class="col-md-2">Minimum</div>
      <div class="col-md-2">Funds Raised</div>
      <div class="col-md-3">Total</div>

    </div>
  

        <?php $max = count($all_users);
        if ($max) { ?>  

                        <?php
                        $count = 0;
                        foreach ($all_users as $user) {
                            $data['user'] = $user;
                            $this->load->view('front-end/list-single-item', $data);
                            if($count == 3){break;}
                            $count++;
                        }
                        ?>

        <?php } else { ?>
            <div id="w" >
                <div class="alert alert-success" role="alert">There are no portfolios available</div>
            </div>
        <?php } ?>

   </div> <!-- ./list-view-deal -->     
    
</div>
<?php if($all_total_result): ?>
  <div class="container">
  <div class="row">
    <div class="col-xs-12"><a href="<?php echo base_url(); ?>home/portfolio_all" class="view_more">View more in All</a></div>
  </div>
</div>
<?php endif; ?>
</div> <!-- #/opportunitiesTabContent -->

</section> <!-- ./trending_opportunities -->