<section class="trending_opportunities list-grid-view"> 

<div class="container">
  <div class="row">
    <div class="col-xs-12 col-sm-8">
      <h2 class="section-title">Recommended</h2>
    </div>
    <div class="col-xs-12 col-sm-4 hidden-sm hidden-xs">
      <ul id="recommendedTab" class="pull-right list-grid-view-tab" role="tablist">
        <li role="presentation">
            <a href="#recommended_list_view" id="recommended_list_view-tab" role="tab" data-toggle="tab" aria-controls="recommended_list_view" aria-expanded="true" class="list-view-tab-icon">
              List
          </a>
        </li>
        
        <li role="presentation" class="active">
          <a href="#recommended_grid_view" role="tab" id="recommended_grid_view-tab" data-toggle="tab" aria-controls="recommended_grid_view" aria-expanded="false" class="grid-view-tab-icon">
            Grid
          </a>
        </li>     
      </ul>
    </div>
  </div>
</div>


<div id="recommendedTabContent" class="tab-content">

<div role="tabpanel" id="recommended_grid_view" class="tab-pane fade active container in" aria-labelledby="recommended_grid_view-tab">
    
    <div class="row">

        <?php $max = count($recommended_users);
        if ($max) {  ?>  

                        <?php
                        $count = 0;
                        foreach ($recommended_users as $user) {
                            $data['user'] = $user;
                            $data['colsize'] = 3;
                            $data['other_classes'] = "";
                            
                            if($count == 3)
                            {
                            	$data['other_classes'] = 'hidden-md'; 
                            }
                            
                            $this->load->view('front-end/grid-single-item', $data);
                            if($count == 3){break;}
                            $count++;
                        }
                        ?>

        <?php } else { ?>
            <div id="w" >
                <div class="alert alert-success" role="alert">There are no recommended companies available</div>
            </div>
        <?php } ?>
    </div>


</div>


<div role="tabpanel" class="tab-pane fade container hidden-sm hidden-xs" id="recommended_list_view" aria-labelledby="recommended_list_view-tab" >

  <div class="list-view-deals">

    <div class="row remove_margin list-view-deal-header">

      <div class="col-md-3">Opportunity</div>
      <div class="col-md-2">Sector</div>
      <div class="col-md-2">Minimum</div>
      <div class="col-md-2">Funds Raised</div>
      <div class="col-md-3">Total</div>

    </div>
  

        <?php $max = count($recommended_users);
        if ($max) { ?>  

                        <?php
                        $count = 0;
                        foreach ($recommended_users as $user) {
                            $data['user'] = $user;
                            $this->load->view('front-end/list-single-item', $data);
                            if($count == 3){break;}
                            $count++;
                        }
                        ?>

        <?php } else { ?>
            <div id="w" >
                <div class="alert alert-success" role="alert">There are no recommended companies available</div>
            </div>
        <?php } ?>

   </div> <!-- ./list-view-deal -->     
    
</div>
<?php if($recommended_total_result): ?>
<div class="container">
  <div class="row">
    <div class="col-xs-12"><a href="<?php echo base_url(); ?>investor/recommended" class="view_more">View more in Recommended</a></div>
  </div>
</div>
<?php endif; ?>
</div> <!-- #/opportunitiesTabContent -->

</section> <!-- ./trending_opportunities -->