<section class="trending_opportunities list-grid-view"> 

<div class="container">
  <div class="row">
    <div class="col-xs-12 col-sm-8">
      <h2 class="section-title">Trending opportunities</h2>
    </div>
    <div class="col-xs-12 col-sm-4 hidden-sm hidden-xs">
      <ul id="opportunitiesTab" class="pull-right list-grid-view-tab" role="tablist">
        <li role="presentation">
            <a href="#opportunities_list_view" id="opportunities_list_view-tab" role="tab" data-toggle="tab" aria-controls="opportunities_list_view" aria-expanded="true" class="list-view-tab-icon">
              List
          </a>
        </li>
        
        <li role="presentation" class="active">
          <a href="#opportunities_grid_view" role="tab" id="opportunities_grid_view-tab" data-toggle="tab" aria-controls="opportunities_grid_view" aria-expanded="false" class="grid-view-tab-icon">
            Grid
          </a>
        </li>     
      </ul>
    </div>
  </div>
</div>


<div id="opportunitiesTabContent" class="tab-content">

<div role="tabpanel" id="opportunities_grid_view" class="tab-pane fade active container in" aria-labelledby="opportunities_grid_view-tab">
    
    <div class="row">

        <?php $max = count($users);
        if ($max) { ?>  

                        <?php
                        $count = 0;
                        foreach ($users as $user) {
                            $data['user'] = $user;
                            $data['colsize'] = 3;
                            if($count == 3){$data['other_classes'] = "hidden-md";}
                            $this->load->view('front-end/grid-single-item', $data);
                            if($count == 3){break;}
                            $count++;
                        }
                        ?>

        <?php } else { ?>
            <div id="w" > 
                <div class="alert alert-success" role="alert">There are no opportunities available as per your search criteria - <a href="<?php echo base_url(); ?>home/index">Please click here to go back</a></div>
            </div>
        <?php } ?>
    </div>


</div>


<div role="tabpanel" class="tab-pane fade container hidden-sm hidden-xs" id="opportunities_list_view" aria-labelledby="opportunities_list_view-tab" >

  <div class="list-view-deals">

    <div class="row remove_margin list-view-deal-header">

      <div class="col-md-3">Opportunity</div>
      <div class="col-md-2">Sector</div>
      <div class="col-md-2">Minimum</div>
      <div class="col-md-2">Funds Raised</div>
      <div class="col-md-3">Total</div>

    </div>
  

        <?php $max = count($users);
        if ($max) { ?>  

                        <?php
                        $count = 0;
                        foreach ($users as $user) {
                            $data['user'] = $user;
                            $this->load->view('front-end/list-single-item', $data);
                            if($count == 3){break;}
                            $count++;
                        }
                        ?>

        <?php } else { ?>
            <div id="w" >
                <div class="alert alert-success" role="alert">There are no opportunities available as per your search criteria - <a href="<?php echo base_url(); ?>home/index">Please click here to go back</a></div>
            </div>
        <?php } ?>

   </div> <!-- ./list-view-deal -->     
    
</div>

<!-- <div class="container">
  <div class="row">
    <div class="col-xs-12"><a href="home/portfolio_all" class="view_more">View 12 more opportunities</a></div>
  </div>
</div> -->

</div> <!-- #/opportunitiesTabContent -->

</section> <!-- ./trending_opportunities -->