  <!--this menu will be on top of the push wrapper-->
  <nav class="st-menu st-effect-2" id="menu-2">
    <ul>
      <li><a class="icon icon-data" href="<?php echo base_url(); ?>investor/my_investment">My Investments</a></li>
      <li><a class="icon icon-location" href="<?php echo base_url(); ?>investor/tracked">My Tracker </a></li>
      <li><a class="icon icon-study" href="<?php echo base_url(); ?>investor/preferred">Preferences</a></li>
      <li><a class="icon icon-photo" href="<?php echo base_url(); ?>investor/recommended">Recommended</a></li>
      <li><a class="icon icon-wallet" href="<?php echo base_url(); ?>investor/profile">Profile</a></li>
      <li><a class="icon icon-location" href="<?php echo base_url(); ?>investor/offers">Offers</a></li>
      <li>
      <ul id="navres">
        <li id="notification_lires">
          <?php if ($unseen_notifications > 0) { ?>  
          <span id="notification_countres"><?php echo $unseen_notifications; ?></span>
          <?php } ?>
          <a href="#" id="notificationLinkres"><span class="notification-icon"></span></a>
          <div id="notificationContainerres">
              <div id="notificationTitleres">Notifications</div>
              <?php if (count($notifications) > 0) { ?>
              <div id="notificationsBodyres" class="notificationsres">
                  <?php foreach($notifications as $notification){ ?>
                      <!-- a class="notify-url" href="<?php echo base_url().$notification['link']; ?>" -->
                      <div class="notificationres">
                          <!-- span class="notification-thumb img-notificationres"><img src="<?php echo base_url(); ?>uploads/users/<?php echo $notification['sender_id'] ?>/<?php echo $notification['image']; ?>')"></span --> 
                          <span class="notification-textres"><?php echo $notification['display_text']; ?></span><br>
                          <span class="notification-timeres"><?php echo date('M, d H:i A',strtotime($notification['date_created'])); ?></span>
                          <span class="border"></span>
                      </div>
                      <!-- /a -->          
                  <?php } ?>
              </div>        
              <div id="notificationFooterres"><a href="<?php echo base_url().$this->session->userdata('role')  ?>/notifications">See All</a></div>       
              <?php } else {?>
              <div id="notificationsBodyres" class="notificationsres">
                  <div class="notificationres">
                      <span class="notification-textres">No notifications yet</span>
                      <span class="border"></span>
                  </div>
              </div>        
              <?php } ?>
          </div>
        </li>
      </ul>
      </li>
     
      <script type="text/javascript" >
      $(document).ready(function()
      {
        $("#notificationLinkres").click(function()
        {
          $("#notificationContainerres").fadeToggle(300);
          $("#notification_countres").fadeOut("slow");
          return false;
        });

        //Document Click
        $(document).click(function()
        {
          $("#notificationContainerres").hide();
        });
        
        //Popup Click
        $("#notificationContainerres").click(function()
        {
          return false;
        });

      });
      </script>
      
      <li><a>Search</a>
    <form action="<?php echo base_url(); ?>investor/search" method="get">
                <div class="row">
                  <div class="search-input">
                    <input type="text" name="name" value="" class="form-control form-input" placeholder="Name" style="margin: 8px 30px; width: 180px;">
                  </div>
                  <div class="search-input">
                    <div class="select-style" style="margin: 8px 30px; width: 180px;">
                      <select class="form-control form-input" name="sector" placeholder="" style="width:177px;">
                        <option value="">By Sector</option>
                        <option value="1">Aerospace &amp; Defence</option>
                        <option value="2">Agriculture</option>
                        <option value="3">Art &amp; Design</option>
                        <option value="4">Automotive</option>
                        <option value="5">Aviation</option>
                        <option value="6">Banking</option>
                        <option value="7">Biotechnology</option>
                        <option value="8">Chemicals</option>
                        <option value="9">Communication &amp; Publishing</option>
                        <option value="10">Computer Hardware</option>
                        <option value="11">Computer Software</option>
                        <option value="12">Construction</option>
                        <option value="13">Consumer Durable &amp; Non Durable</option>
                        <option value="14">Drugs</option>
                        <option value="15">E-Commerce</option>
                        <option value="16">Education &amp; Training</option>
                        <option value="17">Electronics</option>
                        <option value="18">Energy</option>
                        <option value="19">Entertainment</option>
                        <option value="20">Fashion</option>
                        <option value="21">Film</option>
                        <option value="22">Financial Services</option>
                        <option value="23">Food &amp; Beverages</option>
                        <option value="24">Gaming</option>
                        <option value="25">Health Services</option>
                        <option value="26">Insurance</option>
                        <option value="27">Internet</option>
                        <option value="28">Leisure</option>
                        <option value="29">Media</option>
                        <option value="30">Medical Healthcare</option>
                        <option value="31">Mobile App - Web Application</option>
                        <option value="32">Nano Technology</option>
                        <option value="33">Oil Gas Production</option>
                        <option value="34">Property</option>
                        <option value="35">Retail</option>
                        <option value="36">Service - Biz &amp; Personal</option>
                        <option value="37">Social Media</option>
                        <option value="38">Tourism</option>
                        <option value="39">Transportation</option>
                        <option value="40">Utilities</option>
                        <option value="41">BFSI</option>
                        <option value="42">Big Data</option>
                        <option value="43">Data Analytics</option>
                        <option value="44">E Learning</option>
                        <option value="45">Saas</option>
                        <option value="46">Others</option>
                      </select>
                    </div>
                  </div>
                  <div class="search-input">
                    <div class="select-style"  style="margin: 8px 30px; width: 180px;">
                      <select class="form-control form-input" name="stage" placeholder="" style="width:177px;">
                        <option value="">By Stage</option>
                        <option value="1">Ideation</option>
                        <option value="2">Proof of Concept</option>
                        <option value="3">Beta Launched</option>
                        <option value="4">Early Revenue</option>
                        <option value="5">Steady Revenue</option>
                      </select>
                    </div>
                  </div>
                  <div class="search-input">
                    <div class="select-style"  style="margin: 8px 30px; width: 180px;">
                      <select class="form-control form-input" name="size" placeholder="" style="width:177px;">
                        <option value="">By Investment Size</option>
                        <option value="1">Upto 10 lakhs</option>
                        <option value="2">10 lakhs - 50 lakhs</option>
                        <option value="3">50 lakhs - 1 Crore</option>
                        <option value="4">1 Crore - 5 Crore</option>
                        <option value="5">5 Crore &amp; Above</option>
                      </select>
                    </div>
                  </div>
                  <div class="search-input">
                    <div class="select-style"  style="margin: 8px 30px; width: 180px;">
                      <select class="form-control form-input" name="location" placeholder="" style="width:177px;">
                        <option value="">By Location</option>
                        <option value="Mumbai">Mumbai</option>
                        <option value="Delhi">Delhi</option>
                        <option value="Chennai">Chennai</option>
                        <option value="Bengaluru">Bengaluru</option>
                        <option value="Hyderabad">Hyderabad</option>
                        <option value="6">Others</option>
                      </select>
                    </div>
                  </div>
                  <div class="search-input"  style="margin: 8px 30px;">
                    <input type="submit" class="btn search-btn" value="Search">
                  </div>
                </div>
              </form>
      </li>
      
     
      
    </ul>
  </nav>