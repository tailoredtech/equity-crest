<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="height=device-height,width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <title>Angel/VC Investors, Investment Opportunity, Accelerators, Incubators | Equity Crest</title>
        <meta name="description=" content="Investors get the opportunity to invest in high quality, curated deals, where they can be absolutely sure of their investments. The Equity Crest platform is synonymous with credibility, accountability, transparency, quality and top class expertise.
          Equity Crest will keep you abreast with how things are moving within the startup, through regular updates and reports.">
       <?php include_once("analyticstracking.php") ?> 
        <?= css('bootstrap.min.css') ?>
        <?= css('stylesheet.css') ?>

        <?= css('jquery.slick.css') ?>
        <?= css('slick.css') ?>
        <?= css('flyoutmenu.css') ?>
        <?= css('drop-down-menu-css.css') ?>
        <?= css('../tabs/dc_accordion_toggle.css') ?>
        <?= css('jquery.datetimepicker.css') ?>        
        <?= css('calendar.css') ?>
        <?= css('colorbox.css') ?>
        <?= css('jHtmlArea.css') ?>
        <?= css('jquery.tagit.css') ?>
        <?= css('tagit.ui-zendesk.css') ?>        
        <?= css('../custom-scrollbar/jquery.mCustomScrollbar.css') ?>
	    <?= css('owl.carousel.css') ?>
	    <?= css('owl.theme.css') ?>
		
        <?= css('view.css') ?> 
        
        <?= js('jquery-1.11.0.min.js') ?>        
        <?= js('modernizr.custom.js') ?>
        <?= js('jquery.min.js') ?>
        <?= js('bootstrap.min.js') ?>
        <?= js('jquery.datetimepicker.js') ?>
        <?= js('jquery.validate.min.js') ?>          
        <?= js('jquery.colorbox.js') ?>
        <?= js('jHtmlArea-0.8.min.js') ?>               
	    <?= js('owl.carousel.js') ?> 
	    <?= js('jquery.lazyload.min.js') ?>	
		  
</head>
