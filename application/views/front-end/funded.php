<section class="trending_opportunities list-grid-view"> 

<div class="container">
  <div class="row">
    <div class="col-xs-12 col-sm-8">
      <h2 class="section-title">Recently Funded</h2>
    </div>
    <div class="col-xs-12 col-sm-4 hidden-sm hidden-xs">
      <ul id="fundedTab" class="pull-right list-grid-view-tab" role="tablist">
        <li role="presentation">
            <a href="#funded_list_view" id="funded_list_view-tab" role="tab" data-toggle="tab" aria-controls="funded_list_view" aria-expanded="true" class="list-view-tab-icon">
              List
          </a>
        </li>
        
        <li role="presentation" class="active">
          <a href="#funded_grid_view" role="tab" id="funded_grid_view-tab" data-toggle="tab" aria-controls="funded_grid_view" aria-expanded="false" class="grid-view-tab-icon">
            Grid
          </a>
        </li>     
      </ul>
    </div>
  </div>
</div>


<div id="fundedTabContent" class="tab-content">

<div role="tabpanel" id="funded_grid_view" class="tab-pane fade active container in" aria-labelledby="funded_grid_view-tab">
    
    <div class="row">

        <?php $max = count($funded_users);
        if ($max) { ?>  

                        <?php
                        $count = 0;
                        foreach ($funded_users as $user) {
                            $data['user'] = $user;
                            $data['colsize'] = 3;
                            $data['other_classes'] = "";
                            
                            if($count == 3)
                            {
                            	$data['other_classes'] = 'hidden-md'; 
                            }

                            
                            $this->load->view('front-end/grid-single-item', $data);
                            if($count == 3){break;}
                            $count++;
                        }
                        ?>

        <?php } else { ?>

            <div class="row remove_margin">
	            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	              <div class="empty-preferences-box">There are no funded companies available</div>
	            </div>
	          </div>

        <?php } ?>
    </div>


</div>


<div role="tabpanel" class="tab-pane fade container hidden-sm hidden-xs" id="funded_list_view" aria-labelledby="funded_list_view-tab" >

  <div class="list-view-deals">

    <div class="row remove_margin list-view-deal-header">

      <div class="col-md-3">Opportunity</div>
      <div class="col-md-2">Sector</div>
      <div class="col-md-2">Minimum</div>
      <div class="col-md-2">Funds Raised</div>
      <div class="col-md-3">Total</div>

    </div>
  

        <?php $max = count($funded_users);
        if ($max) { ?>  

                        <?php
                        $count = 0;
                        foreach ($funded_users as $user) {
                            $data['user'] = $user;
                            $this->load->view('front-end/list-single-item', $data);
                            if($count == 3){break;}
                            $count++;
                        }
                        ?>

        <?php } else { ?>
            <div class="row remove_margin">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="empty-preferences-box">There are no funded companies available</div>
                </div>
              </div>
        <?php } ?>

   </div> <!-- ./list-view-deal -->     
    
</div>
<?php if($funded_total_result && $this->session->userdata('role') != ''): ?>
<div class="container">
  <div class="row">
    <div class="col-xs-12"><a href="<?php echo base_url(); ?>investor/funded" class="view_more">View more in Funded</a></div>
  </div>
</div>
<?php endif; ?>
</div> <!-- #/opportunitiesTabContent -->

</section> <!-- ./trending_opportunities -->
