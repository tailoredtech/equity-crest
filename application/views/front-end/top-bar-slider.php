<!-- PAGE BANNER -->
<div id="Outerbanner">

    <div id="owl_home_slider" class="owl-carousel owl-theme">

<?php 
                    $count = 0; 
                    foreach ($banners as $banner){
                        $count++;
                        // if ($count == 1)continue;
                        if ($count > 11)break;    
                        ?>
      
     
      <div class="item owl_home_slider_item" style="background-image: url(<?php echo $banner->background_image!=''?base_url().$banner->background_image:base_url().'assets/images/home_slider_bg.jpg'; ?>);">

        <img class="img-responsive owl_home_slider_img" src="<?php echo base_url().'assets/images/home_slider_blank_bg.png'; ?>" alt="banner"/>

          <div class="owl_home_slider_banner">

            <div class="container">

              <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <div class="row">

                          <div class="col-lg-8 col-md-8 col-sm-11 col-xs-12 home-slider-item">

                            <div class="home-slider-title">
                              <h2><font color="<?php echo $banner->banner_text_font; ?>"><?php echo $banner->banner_title; ?></font></h2>
                            </div>                           

                            <div class="home-slider-content">
                              <p><font color="<?php echo $banner->banner_text_font; ?>"><?php echo $banner->banner_text; ?></font></p>
                            </div>
                          <?php if($banner->banner_link && $this->session->userdata('user_id')): ?>
                            <div class="home-slider-btn">
                              <a href="<?php echo $banner->banner_link; ?>" target="_blank"><button class="btn" type="button">Discover more</button></a>
                            </div> 
                            <?php endif; ?>
                          </div>

                        </div>                     

                </div>

              </div>

            </div>

          </div>

      </div> <!-- / owl slider item -->


<?php } ?>

     
    </div> <!-- /owl_home_slider -->

</div>  
<!-- PAGE BANNER END -->
