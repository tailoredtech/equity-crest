<div class="footer">
    <div class="footer-middle">
        <div class="container wrap">
            <div class="section group example">
                <div class="col_1_of_f_1 span_1_of_f_1">
                    <div class="section group example">
                        <div class="col_1_of_f_2 span_1_of_f_2">
       
                            <ul class="f-list1">
                                <li>
                                    <a href="<?php echo base_url(); ?>home/about_us">About Us</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>home/management_team" class="">Management Team</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>home/partners" class="">Channel Partners</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>home/advisory_board" class="">Advisory Board</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>home/how_it_works" class="">How Equity Crest Works</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>home/faqs" class="">FAQ</a>
                                </li>
                            </ul>
                    
                
                        </div>
                        <div class="col_1_of_f_2 span_1_of_f_2">
            
                            <ul class="f-list1">
                            <?php if(!$this->session->userdata('user_id')){ ?>
                                <li>
                                    <a href="<?php echo base_url(); ?>user/register?user=investor" class="">Investor</a>
                                </li>
                                <li >
                                    <a href="<?php echo base_url(); ?>user/register?user=investee" class="">Portfolio</a>
                                </li>
                                <li >
                                    <!--<a href="<?php echo base_url(); ?>user/register?user=channel_partner" class="">Become A Channel Partner</a>-->
                                </li>
                             <?php }else {?>
                                <li>
                                    <a href="<?php echo base_url(); ?>home/investor_all" class="">Investor</a>
                                </li>
                                <li >
                                    <a href="<?php echo base_url(); ?>home/portfolio_all" class="">Portfolio</a>
                                </li>
                                <li >
                                    <!--<a href="javascript:void(0);" class="">Become A Channel Partner</a>-->
                                </li>
                            <?php } ?>
                            
                                <li >
                                    <!--<a href="<?php echo base_url(); ?>home/women_entrepreneurship" class="">Women Entrepreneurship</a>-->
                                </li>                                
                            </ul>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="col_1_of_f_1 span_1_of_f_1">
                    <div class="section group example">
                        <div class="col_1_of_f_2 span_1_of_f_2">
                            <ul class="f-list1">
                                <li ><a href="<?php echo base_url(); ?>home/news_top_stories" class="">Media/Press centre</a></li>
                                <li ><a href="<?php echo base_url(); ?>home/events" class="">Events</a></li>
                                <li ><a href="<?php echo base_url(); ?>home/blogs" class="">Blogs</a></li>
                                <li ><a href="javascript:void(0)" id="subscribe">Subscribe</a></li>
                            </ul>
                        </div>
                        <div class="col_1_of_f_2 span_1_of_f_2" style="border-right: none;">
                            <ul class="f-list1">
                                <li><a href="<?php echo base_url(); ?>home/contact_us">Contact Us</a></li>
                                <li><a href="<?php echo base_url(); ?>home/careers">Careers</a></li>
                            </ul>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="container bottom-footer">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul class="socia-links">
                    <li class="pull-right-social">
                        <span>Follow Us : </span>
                        <!-- a target="_blank" href="#"><img src="<?php echo base_url(); ?>assets/images/socia1.png"></a -->
                        <a target="_blank" href="https://www.linkedin.com/company/3807730?trk=vsrp_companies_res_name&trkInfo=VSRPsearchId%3A365969681411721454547%2CVSRPtargetId%3A3807730%2CVSRPcmpt%3Aprimary"><img src="<?php echo base_url(); ?>assets/images/socia2.png"></a>
                        <a target="_blank" href="https://www.facebook.com/pages/Equity-Crest/1436546399937498?fref=ts"><img src="<?php echo base_url(); ?>assets/images/socia3.png"></a>
                        <a target="_blank" href="https://twitter.com/EquityCrest"><img src="<?php echo base_url(); ?>assets/images/socia4.png"></a>
                    </li>
                </ul>
            </div>
        </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="wrap">
            <div class="copy">
                <p>© Copyrights of EQ Advisors Pvt. Ltd , 2014</p>
            </div>
            <div class="f-list2">
                <ul>
                    <li class="active"><a href="<?php echo base_url(); ?>home/terms_of_use">Terms of Use</a></li> |
                    <li><a href="<?php echo base_url(); ?>home/privacy_policy">Security & Privacy</a></li> |
                    <li><a href="<?php echo base_url(); ?>home/cookie_policy">Cookie</a></li> 
                </ul>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>

<!-- POPUP CONTAINER -->
<div style="display: none;">
    <div id="subsciber-box" class="container pop-content">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="form-header top-margin-10">Subscribe to our newsletter</h3>

                <div class="sendto">  Enter your email address to receive all news from our website</div>
            </div>
            <form id="subscriber-form">
                <div class="col-sm-12">
                    <input type="text" name="email" class="form-control form-input" placeholder="your email address">
                    <div class="grey-font subscribe-line">Don't worry you'll not be spammed</div>
                    
                    <div class="text-center pull-right">
                        <input class="eq-btn" type="submit" value="Subscribe" />
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        /*, innerWidth: '450px'*/
        $("#subscribe").colorbox({inline: true, href: '#subsciber-box', maxWidth: '450px', innerWidth: '80%'});
        
        $("#subscriber-form").on('submit', (function(e) {
            e.preventDefault();
            $.ajax({
                url: "<?php echo base_url(); ?>user/subscribe_process",
                type: 'POST',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false
            }).done(function() {
                $.colorbox.close();
            });

        }));
    });
 </script>