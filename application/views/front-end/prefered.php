<section class="trending_opportunities list-grid-view"> 

<div class="container">
  <div class="row">
    <div class="col-xs-12 col-sm-8">
      <h2 class="section-title">Preferred <span class="section-link"><a href="<?php echo base_url(); ?>investor/manage_preferences">Manage</a></span></h2>
    </div>

<?php if( count($prefered_users) > 0 ) { ?>

    <div class="col-xs-12 col-sm-4 hidden-sm hidden-xs">
      <ul id="preferedTab" class="pull-right list-grid-view-tab" role="tablist">
        <li role="presentation">
            <a href="#prefered_list_view" id="prefered_list_view-tab" role="tab" data-toggle="tab" aria-controls="prefered_list_view" aria-expanded="true" class="list-view-tab-icon">
              List
          </a>
        </li>
        
        <li role="presentation" class="active">
          <a href="#prefered_grid_view" role="tab" id="prefered_grid_view-tab" data-toggle="tab" aria-controls="prefered_grid_view" aria-expanded="false" class="grid-view-tab-icon">
            Grid
          </a>
        </li>     
      </ul>
    </div>

<?php } ?>

  </div>
</div>


<div id="preferedTabContent" class="tab-content">

<div role="tabpanel" id="prefered_grid_view" class="tab-pane fade active container in" aria-labelledby="prefered_grid_view-tab">
    
    <div class="row">

        <?php $max = count($prefered_users);
        if ($max) { ?>  

                        <?php
                        $count = 0;
                        foreach ($prefered_users as $user) {
                            $data['user'] = $user;
                            $data['colsize'] = 3;
                            $data['other_classes'] = "";
                            
                            if($count == 3)
                            {
                            	$data['other_classes'] = 'hidden-md'; 
                            }
                            
                            $this->load->view('front-end/grid-single-item', $data);
                            if($count == 3){break;}
                            $count++;
                        }
                        ?>

        <?php } else { ?>

        <div class="row remove_margin">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="empty-preferences-box">You have not setup your preferences yet<br /><span><a href="<?php echo base_url(); ?>investor/manage_preferences">Click here to manage them now.</a> </span></div>
          </div>

        </div>


            
                
           
        <?php } ?>
    </div>


</div>


<div role="tabpanel" class="tab-pane fade container hidden-sm hidden-xs" id="prefered_list_view" aria-labelledby="prefered_list_view-tab" >

  <div class="list-view-deals">

    <div class="row remove_margin list-view-deal-header">

      <div class="col-md-3">Opportunity</div>
      <div class="col-md-2">Sector</div>
      <div class="col-md-2">Minimum</div>
      <div class="col-md-2">Funds Raised</div>
      <div class="col-md-3">Total</div>

    </div>
  

        <?php $max = count($prefered_users);
        if ($max) { ?>  

                        <?php
                        $count = 0;
                        foreach ($prefered_users as $user) {
                            $data['user'] = $user;
                            $this->load->view('front-end/list-single-item', $data);
                            if($count == 3){break;}
                            $count++;
                        }
                        ?>

        <?php } else { ?>
            <div id="w" >
                <div class="alert alert-success" role="alert">You have not setup your preferences yet<br /><span><a href="<?php echo base_url(); ?>investor/manage_preferences">Click here</a> to manage them now.</span></div>
            </div>
        <?php } ?>

   </div> <!-- ./list-view-deal -->     
    
</div>
<?php if($prefered_total_result): ?>
<div class="container">
  <div class="row">
    <div class="col-xs-12"><a href="<?php echo base_url(); ?>investor/preferred" class="view_more">View more in Preferred</a></div>
  </div>
</div>
<?php endif; ?>

</div> <!-- #/opportunitiesTabContent -->

</section> <!-- ./trending_opportunities -->