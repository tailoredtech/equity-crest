
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 sidebar">                    

            <form id="search_frm" action="<?php echo base_url(); ?>home/search" method="get">

                <div>
                        <h3 class="page-title2">Filter opportunities</h3>
                </div>


                <div class="sidebar-filter-box">
                    <div class="sidebar-filter-section sidebar-filter-padding20">
                        <?php  $max = isset($users) ? count($users) : count($users_funded + $users_raised);?>
                        <div class="form-group" style="margin-bottom:0px;">
                            <input type="text" name="name" value="<?php if($max > 0){echo isset($searchOptions['name'])?$searchOptions['name']:"";} else echo ""; ?>" class="form-control sidebar-filter-search"  placeholder="Search for an opportunity" >
                        </div>
                    </div>
                    <div class="accordion sidebar-filter-section">
                        <div class="accordion-heading  accordion-opened">
                            <a class="accordion-toggle <?php echo empty($searchOptions['sector'])?'collapsed':'';?>" data-toggle="collapse" href="#collapseSector"><h4>By Sector</h4></a>
                        </div>
                        <div id="collapseSector" class="accordion-body collapse <?php echo !empty($searchOptions['sector'])?'in':'';?>">
                          <div class="accordion-inner  mCustomScrollbar">    
                            <label class="selectorcheckbox"><input type="checkbox" id="selectAllSectors" name="sector[]" value="All" <?php echo (isset($searchOptions['sector']) && in_array('All',$searchOptions['sector'])) ? 'checked'  : ''; ?> />All</label>
                             <?php $i = 0;//foreach ($sectors as $sector)
                             foreach($sectors as $k=>$v) {
                            
                              if($v['id']!='51' && $v['id']!='52')
                              { 

                              ?>
                               <label class="selectorcheckbox">
                              <input type="checkbox" name="sector[]" value='<?php echo $v['id']; ?>' <?php echo (isset($searchOptions['sector']) && in_array($v['id'],$searchOptions['sector'])) ? 'checked'  : ''; ?> ><?php echo $v['name']; ?>
                            </label>
                            <?php }
                          }
                        ?>

                         <label class="selectorcheckbox">
                              <input type="checkbox" name="sector[]" value='51' <?php echo (isset($searchOptions['sector']) && in_array('51',$searchOptions['sector'])) ? 'checked'  : ''; ?> ><?php echo "Others"; ?>
                          </label>
                            </div>
                        </div>
                    </div>

                    <div class="accordion sidebar-filter-section">
                        <div class="accordion-heading accordion-opened">
                            <a class="accordion-toggle <?php echo empty($searchOptions['stage'])?'collapsed':'';?>" data-toggle="collapse" href="#collapseStage"><h4>By stage</h4></a>
                        </div>
                        <div id="collapseStage" class="accordion-body collapse <?php echo !empty($searchOptions['stage'])?'in':'';?>">
                          <div class="accordion-inner">
                            <label class="selectorcheckbox"><input type="checkbox" name="stage[]" id="selectAllStage" value="All" <?php echo (isset($searchOptions['stage']) && in_array('All',$searchOptions['stage'])) ? 'checked'  : ''; ?> />All</label>
                             <?php foreach ($stages as $stage) { ?>
                             <label class="selectorcheckbox"><input type="checkbox" name="stage[]" value='<?php echo $stage['id']; ?>' <?php echo (isset($searchOptions['stage']) && in_array($stage['id'],$searchOptions['stage'])) ? 'checked'  : ''; ?> ><?php echo $stage['name']; ?></label>
                            <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="accordion sidebar-filter-section">
                        <div class="accordion-heading accordion-opened">
                            <a class="accordion-toggle <?php echo empty($searchOptions['size'])?'collapsed':'';?>" data-toggle="collapse" href="#collapseSize"><h4>Investment Size</h4></a>
                        </div>
                        <div id="collapseSize" class="accordion-body collapse <?php echo !empty($searchOptions['size'])?'in':'';?>">
                          <div class="accordion-inner">
                            <label class="selectorcheckbox"><input type="checkbox" name="size[]" id="selectAllSize" value="All" <?php echo (isset($searchOptions['size']) && in_array('All',$searchOptions['size'])) ? 'checked'  : ''; ?> /> All</label>
                           <label class="selectorcheckbox"><input type="checkbox" name="size[]" value="1" <?php echo (isset($searchOptions['size']) && in_array(1,$searchOptions['size'])) ? 'checked'  : ''; ?> >Upto &#8377; 10 lakhs</label>
                           <label class="selectorcheckbox"><input type="checkbox" name="size[]" value="2" <?php echo (isset($searchOptions['size']) && in_array(2,$searchOptions['size'])) ? 'checked'  : ''; ?> >&#8377; 10 lakhs - &#8377; 50 lakhs</label>
                          <label class="selectorcheckbox"><input type="checkbox" name="size[]" value="3" <?php echo (isset($searchOptions['size']) && in_array(3,$searchOptions['size'])) ? 'checked'  : ''; ?> >&#8377; 50 lakhs - &#8377; 1 Crore</label>
                          <label class="selectorcheckbox"><input type="checkbox" name="size[]" value="4" <?php echo (isset($searchOptions['size']) && in_array(4,$searchOptions['size'])) ? 'checked'  : ''; ?> >&#8377; 1 Crore - &#8377; 5 Crore</label>
                          <label class="selectorcheckbox"><input type="checkbox" name="size[]" value="5" <?php echo (isset($searchOptions['size']) && in_array(5,$searchOptions['size'])) ? 'checked'  : ''; ?> >&#8377; 5 Crore & Above</label>
                          </div>
                        </div>
                     </div>

                    <div class="accordion sidebar-filter-section">
                        <div class="accordion-heading accordion-opened">
                            <a class="accordion-toggle <?php echo empty($searchOptions['location'])?'collapsed':'';?>" data-toggle="collapse" href="#collapseLocation"><h4>Location</h4></a>
                        </div>
                        <div id="collapseLocation" class="accordion-body collapse <?php echo !empty($searchOptions['location'])?'in':'';?>">
                          <div class="accordion-inner">

                            <label class="selectorcheckbox"><input type="checkbox" id="selectAllLocation" name="location[]" value="All" <?php echo (isset($searchOptions['location']) && in_array('All',$searchOptions['location'])) ? 'checked'  : ''; ?> /> All</label>
                            <label class="selectorcheckbox"><input type="checkbox"  name="location[]" value="Mumbai" <?php echo (isset($searchOptions['location']) && in_array('Mumbai',$searchOptions['location'])) ? 'checked'  : ''; ?> >Mumbai</label>
                            <label class="selectorcheckbox"><input type="checkbox"  name="location[]" value="Delhi" <?php echo (isset($searchOptions['location']) && in_array('Delhi',$searchOptions['location'])) ? 'checked'  : ''; ?> >Delhi</label>
                            <label class="selectorcheckbox"><input type="checkbox"  name="location[]" value="Chennai" <?php echo (isset($searchOptions['location']) && in_array('Chennai',$searchOptions['location'])) ? 'checked'  : ''; ?> >Chennai</label>
                            <label class="selectorcheckbox"><input type="checkbox"  name="location[]" value="Bengaluru" <?php echo (isset($searchOptions['location']) && in_array('Bengaluru',$searchOptions['location'])) ? 'checked'  : ''; ?> >Bengaluru</label>
                            <label class="selectorcheckbox"><input type="checkbox"  name="location[]" value="Hyderabad" <?php echo (isset($searchOptions['location']) && in_array('Hyderabad',$searchOptions['location'])) ? 'checked'  : ''; ?> >Hyderabad</label>
                            <label class="selectorcheckbox"><input type="checkbox"  name="location[]" value="6" <?php echo (isset($searchOptions['location']) && in_array(6,$searchOptions['location'])) ? 'checked'  : ''; ?> >Others</label>
                           </div>
                        </div>
                    </div>

                    <div class="sidebar-filter-section sidebar-filter-padding20">

                        <button type="submit" class="btn btn-default btn-eq-common col-xs-12">Apply and update</button>

                    </div>

                </div>



                <div class="sidebar-filter-submit">

                    <a href="#" id="filter-clear">Clear filter</a>

                </div>  

            </form>

        </div>
<script>
$(document).ready(function() {
  
});
      </script>