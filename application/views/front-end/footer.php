<?php include_once("analyticstracking.php") ?>

<footer id="footer" class="footer">

    <div class="container"> 

        <div class="row remove_margin">

            <div class="col-md-3 col-sm-3 col-xs-12 footer-social-area hidden-xs"> <!-- footer social area start -->

               <p class="footer-logo"><img src="<?php echo base_url(); ?>assets/images/footer_logo.png" class="img-responsive" alt="equitycrest"/></p>

               <p class="footer-copyright">© 2015 of EQ Advisors Pvt. Ltd</p>

               <ul class="list-inline social-icon-32">
                    <li class="lin"><a target="_blank" href="https://www.linkedin.com/company/3807730?trk=vsrp_companies_res_name&trkInfo=VSRPsearchId%3A365969681411721454547%2CVSRPtargetId%3A3807730%2CVSRPcmpt%3Aprimary">LinkedIn</a></li>
                    <li class="fb"><a target="_blank" href="https://www.facebook.com/pages/Equity-Crest/1436546399937498?fref=ts">Facebook</a></li>
                    <li class="tw"><a target="_blank" href="https://twitter.com/EquityCrest">Twitter</a></li>
                </ul>


            </div> <!-- footer social area end -->

            <div class="col-md-9 col-sm-9 col-xs-12 footer-menus list-unstyled-area"> <!-- footer menus area  start -->

                <div class="row">

                    <div class="col-md-3 col-sm-3 col-xs-12">

                        <ul class="footer-menus list-unstyled">
                            <li>
                                <a href="<?php echo base_url(); ?>home/about_us">About Us</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>home/management_team" class="">Management Team</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>home/partners" class="">Channel Partners</a>
                            </li>
                          <!--  <li>
                                <a href="<?php echo base_url(); ?>home/advisory_board" class="">Advisory Board</a>
                            </li>-->
                            <li>
                                <a href="<?php echo base_url(); ?>home/how_it_works" class="">How Equity Crest Works</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>home/faqs" class="">Frequently Asked Questions</a>
                            </li>
                        </ul>

                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-12">

                            <ul class="footer-menus list-unstyled">
                                <?php if(!$this->session->userdata('user_id')){ ?>
                                    <li>
                                         <a href="javascript:void(0);" data-toggle="modal" data-target="#registerModal" class="">Investor</a>
                                    </li>
                                    <li >
                                        <a href="javascript:void(0);" data-toggle="modal" data-target="#registerModal" class="">Portfolio</a>
                                    </li>                              
                                    
                                 <?php }else {?>

                                    <li>
                                        <a href="<?php echo base_url(); ?>home/investor_all" class="">Investors</a>
                                    </li>

                                    <li >
                                        <a href="<?php echo base_url(); ?>home/portfolio_all" class="">Portfolio</a>
                                    </li>
                                    <!-- <li>
                                        <a href="javascript:void(0);" class="">Become A Channel Partner</a>
                                    </li> -->
                                <?php } ?>
                                    <li><a href="<?php echo base_url(); ?>home/news_top_stories" class="">Media/Press centre</a></li>
                                    <!--li><a href="<?php echo base_url(); ?>home/events" class="">Events</a></li>
                                
                                    <!-- <li>
                                        <a href="<?php echo base_url(); ?>home/women_entrepreneurship" class="">Women Entrepreneurship</a>
                                    </li> -->                                
                                </ul>

                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-12">

                        <ul class="footer-menus list-unstyled">                                
                            <li><a href="http://www.equitycrest.wordpress.com" target="_blank" class="">Blog</a></li>
                            <li><a href="javascript:void(0)" data-toggle="modal" data-target="#subscribeModal" id="subscribe">Subscribe</a></li>
                            <li><a href="<?php echo base_url(); ?>home/contact_us">Contact Us</a></li>
                            <!--li><a href="<?php echo base_url(); ?>home/careers">Careers</a></li-->
                        </ul>

                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <ul class="footer-menus list-unstyled">
                            <li><a href="<?php echo base_url(); ?>home/terms_of_use">Terms of Use</a></li>
                            <li><a href="<?php echo base_url(); ?>home/privacy_policy">Security & Privacy</a></li>
                            <li><a href="<?php echo base_url(); ?>home/cookie_policy">Cookie</a></li>
                        </ul>   
                    </div>                

                </div>

            </div> <!-- footer menus area  end -->

            <div class="col-md-3 col-sm-3 col-xs-12 footer-social-area visible-xs"> <!-- footer social area start -->

               <p class="footer-logo"><img src="<?php echo base_url(); ?>assets/images/footer_logo.png" class="img-responsive" alt="equitycrest"/></p>

               <p class="footer-copyright">© 2015 of EQ Advisors Pvt. Ltd</p>

               <ul class="list-inline social-icon-32">
                    <li class="lin"><a target="_blank" href="https://www.linkedin.com/company/3807730?trk=vsrp_companies_res_name&trkInfo=VSRPsearchId%3A365969681411721454547%2CVSRPtargetId%3A3807730%2CVSRPcmpt%3Aprimary">LinkedIn</a></li>
                    <li class="fb"><a target="_blank" href="https://www.facebook.com/pages/Equity-Crest/1436546399937498?fref=ts">Facebook</a></li>
                    <li class="tw"><a target="_blank" href="https://twitter.com/EquityCrest">Twitter</a></li>
                </ul>


            </div> <!-- footer social area end -->


        </div>

    </div> <!-- ./container -->

</footer>

<!-- POPUP CONTAINER -->
<div class="modal fade" id="subscribeModal" tabindex="-1" role="dialog" aria-labelledby="subscribeModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="subscribeModalLabel">Subscribe to our Mailers</h4>
      </div>
      <div class="modal-body">
        <div class="sendto">  Enter your email address to receive all news from our website</div><br/>
        <form id="subscriber-form" method="post">
          <div id="email_success" for="email" class="success">&nbsp;</div>
          <div id="email_error" for="email" class="error">&nbsp;</div>
          <div class="form-group">              
            <input type="text" name="email" class="form-control form-input" placeholder="your email address">
            <div class="grey-font subscribe-line" style="font-size: 0.9em; margin-top: 5px;">Don't worry you'll not be spammed</div>
          </div>
          <div class="text-right form-group">
            <input class="btn btn-eq-common" type="submit" value="Subscribe"/>
          </div> 
        </form> 
       
      </div>
    </div>
  </div>
</div>


<!-- POPUP FORGOT PASSWORD-->
<div class="modal fade" id="forgotPasswordModal" tabindex="-1" role="dialog" aria-labelledby="forgotPasswordModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="forgotPasswordModalLabel">Forgot Password</h4>
      </div>
      <div class="modal-body">
        <form id="forgot-psw-form" method="post">
          <div class="form-group">
           <div id="email_success" for="email" class="success">&nbsp;</div>
            <label for="founder-name" class="control-label">Email:</label>           
            <input type="text" name="email" id="email" value="" class="form-control form-input"  placeholder="Email">
            <input type="hidden" name="investee_id" value="" id="investee_id" />
            <input type="hidden" name="pledged_amount" value="0" id="pledged_amount" />
            <label id="email_error" for="email" class="error">&nbsp;</label>

          </div>

          <div class="text-right form-group">
                <input class="btn btn-eq-common" type="submit" value="Reset my Account" />
          </div>
        </form>
      </div>
    </div>
  </div>
</div>


<!-- POPUP VERIFY -->
<div class="modal fade" id="verifyPopModal" tabindex="-1" role="dialog" aria-labelledby="verifyPopModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="verifyPopModalLabel">Verification Mail</h4>
            </div>
            <div class="modal-body">                
                <div class="askexpert">
                    <h5>New verification mail has been sent. Please also check your spam folder for mail</h5>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('user/register'); ?>

<?= js('../custom-scrollbar/jquery.mCustomScrollbar.js') ?>

<script>
   
        /*, innerWidth: '450px'*/
    //    $("#subscribe").colorbox({inline: true, href: '#subsciber-box', maxWidth: '450px', innerWidth: '80%'});
       
      //  $(document).delegate('#subscriber-form', 'submit', function(e) { 

      // alert('hihi'); 

      //      e.preventDefault();
      //      $.ajax({
      //           url: "user/subscribe_process",
      //           type: 'POST',
      //           data: new FormData(this),
      //           contentType: false,
      //           cache: false,
      //           processData: false
      //       }).done(function(data) {
      //       if (data == 'success') {
      //         // $.colorbox.close();
      //         //$('#forgotPasswordModal').modal("hide");
      //         $('#subscriber-form input[type="text"]').val('');
      //         $('#email_success').html('<div class="alert alert-success">dfgdgdfgdg</div>');
      //          clearTimeout($('#subscribeModal').data('hideInterval'));
      //           $('#subscribeModal').data('hideInterval', setTimeout(function(){
      //               $('#subscribeModal').modal('hide');
      //          $('#email_success').html('');
      //           }, 5000));
      //           } else {
      //         $('#email_error').html(data);
      //       }
      //     });
           
          
      //   }));

$(document).ready(function(){

   $("#subscriber-form").on('submit', (function(e) {
    
            e.preventDefault();
            $.ajax({
                url: "<?php echo base_url(); ?>user/subscribe_process",
                type: 'POST',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false
            }).done(function(data) {

          if(data.trim() =='success') {
            $('#subscriber-form input[type="text"]').val('');
              $('#email_success').html('<div class="alert alert-success">You have successfully subscribed for our mailers!</div>');
               clearTimeout($('#subscribeModal').data('hideInterval'));
                $('#subscribeModal').data('hideInterval', setTimeout(function(){
                    $('#subscribeModal').modal('hide');
               $('#email_success').html('');
                }, 5000));
                } else {
                //  alert('error');
              //$('#email_error').html(data);
               $('#subscriber-form input[type="text"]').val('');
              $('#email_error').html('<div class="alert alert-error">Subscription Failed!</div>');
               clearTimeout($('#subscribeModal').data('hideInterval'));
                $('#subscribeModal').data('hideInterval', setTimeout(function(){
                    $('#subscribeModal').modal('hide');
               $('#email_error').html('');
                }, 5000));

            }
                
            });

        }));
    });

/* Login **********************************************************************/

 $('#login-form').validate({
  rules: {

    email: {
      required: true,
      email: true
    },
    password: {
      //required : true
    }
  },
  messages: {
    email: {
      required: "Please enter you email id.",
      email: "Please enter a valid email id."
    },
    password: {
      required: "Password cannot be blank."
    }
  }
});

$(document).delegate('#forgot-psw-form', 'submit', function(e) {

  e.preventDefault();
  $.ajax({
    url: "user/send_pass_reset_link",
    type: 'POST',
    data: new FormData(this),
    contentType: false,
    cache: false,
    processData: false
  }).done(function(data) {


    if (data == 'success') {
      // $.colorbox.close();
      //$('#forgotPasswordModal').modal("hide");
      $('#forgot-psw-form input[type="text"]').val('');
      $('#email_success').html('<div class="alert alert-success">Reset password link has been sent on registered email.</div>');
       clearTimeout($('#forgotPasswordModal').data('hideInterval'));
        $('#forgotPasswordModal').data('hideInterval', setTimeout(function(){
            $('#forgotPasswordModal').modal('hide');
	     $('#email_success').html('');
        }, 5000));
        } else {
      $('#email_error').html(data);
    }
  });

});

$('.forgot-password').on('click', function() {

  $('#login-register-link').removeClass('open');

});

//$.colorbox({inline: true, href: '#verify-pop', innerWidth: '400px', innerHeight: '100px'});

$('#verify').on('click', function() {
  var id = $(this).data("id");
  console.log(id);
  $.post("user/verify_again", {
    id: id
  }).done(function() {
  $('#login-register-link .failure-msg').remove();
  $('#verifyPopModal').modal("show");

  });

});

  $('#Portfolio').click(function(){
    ga('send', 'event', 'button','portfolio_click', 'header'); 
   });  

  $('#channel_partner').click(function(){
    ga('send', 'event', 'button', 'channel_partner_click', 'header'); 
   });  

   $('#investor').click(function(){
    ga('send', 'event', 'button', 'investor_click', 'header'); 
   }); 

    $('#notifications').click(function(){
    ga('send', 'event', 'button', 'notifications_click', 'header'); 
   });


    $('.company_click').click(function(){
    ga('send', 'event', 'button','click',$(this).attr('cname'));
     
   }); 


    $('.unfollow').click(function(){
    ga('send', 'event', 'button', 'click',$(this).attr('data-cname'));
     
   });

    $('.follow').click(function(){
    ga('send', 'event', 'button', 'click',$(this).attr('data-cname'));
     
   }); 


     $('.pledged').click(function(){
    ga('send', 'event', 'button', 'click',$(this).attr('data-cname'));
     
   });  


      $('.pledge').click(function(){
    ga('send', 'event', 'button', 'click',$(this).attr('data-cname'));
     
   });  


   $('.accordion-toggle').click(function(){ 
    if($(this).hasClass('collapsed'))
    ga('send', 'event', 'button', 'click',$(this).attr('tab-name'));
   });  
   
 </script>
