<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="height=device-height,width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <title>Angel/VC Investors, Investment Opportunity, Accelerators, Incubators | Equity Crest</title>
        <meta name="description=" content="If you have a promising business, you don’t need to worry about funding. Equity Crest has onboard both national and international investors who are looking for promising ventures to invest in. 
        Equity Crest will make the connection and help you fulfill your funding needs.">
       <?php include_once("analyticstracking.php") ?>
        <?= css('bootstrap.min.css') ?>
        <?= css('bootstrap-select.min.css') ?>
        <?= css('stylesheet.css') ?>        
        
        
        <?= css('jasny-bootstrap.css') ?>
        <!-- ?= css('jquery.slick.css') ? -->
        <!--?= css('slick.css') ? -->
        <?= css('flyoutmenu.css') ?>
        <?= css('drop-down-menu-css.css') ?>
        <?= css('../tabs/dc_accordion_toggle.css') ?>
        <?= css('jquery.datetimepicker.css') ?>        
        <?= css('calendar.css') ?>
        <?= css('colorbox.css') ?>
        <?= css('jHtmlArea.css') ?>
        <?= css('jquery.tagit.css') ?>
        <?= css('tagit.ui-zendesk.css') ?>
        
        <!--?= css('style.css') ?-->
        
    		<?= css('owl.carousel.css') ?>
    		<?= css('owl.theme.css') ?>
		        <?= css('../custom-scrollbar/jquery.mCustomScrollbar.css') ?>

        <!-- ?= css('view.css') ? -->
        <!-- ?= css('font.css') ? -->
         <?= js('jquery-1.11.0.min.js') ?>
          <?= js('modernizr.custom.js') ?>
          <?= js('jquery.min.js') ?>
          <?= js('bootstrap.min.js') ?>
          <?= js('jquery.datetimepicker.js') ?>
          <?= js('jquery.validate.min.js') ?>          
          <?= js('jquery.colorbox.js') ?>
          <?= js('jHtmlArea-0.8.min.js') ?>
          <?= js('jasny-bootstrap.min.js') ?>
		      <?= js('owl.carousel.js') ?>

          <?= js('bootstrap-select.min.js') ?>
		  
	            
</head>
