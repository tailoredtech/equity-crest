<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 sidebar">
      <div class="sidebar-filter-box">
        <div class="sidebar-filter-section <?php echo $activePanel ==''?'active':''?>" >
            <a href="<?php echo base_url().'user/investee_info';?>"><h4>Company Information</h4></a>
        </div>
        <div class="sidebar-filter-section <?php echo $activePanel =='team'?'active':''?>">
          <a href="<?php echo base_url().'user/investee_info/team';?>"><h4>Team Summary</h4></a>
        </div>
        <div class="sidebar-filter-section <?php echo $activePanel =='business'?'active':''?>">
          <a href="<?php echo base_url().'user/investee_info/business';?>"><h4>Business</h4></a>
        </div>
        <div class="sidebar-filter-section <?php echo $activePanel =='revenue'?'active':''?>">
          <a href="<?php echo base_url().'user/investee_info/revenue';?>"><h4>Revenue Model</h4></a>
        </div>
        <div class="sidebar-filter-section <?php echo $activePanel =='market'?'active':''?>">
          <a href="<?php echo base_url().'user/investee_info/market';?>"><h4>Market</h4></a>
        </div>
        <div class="sidebar-filter-section <?php echo $activePanel =='raise'?'active':''?>">
          <a href="<?php echo base_url().'user/investee_info/raise';?>"><h4>Raise</h4></a>
        </div>
        <div class="sidebar-filter-section <?php echo $activePanel =='financial'?'active':''?>">
          <a href="<?php echo base_url().'user/investee_info/financial';?>"><h4>Monthly Financial Indicators</h4></a>
        </div>
        <div class="sidebar-filter-section <?php echo $activePanel =='product_info'?'active':''?>">
          <a href="<?php echo base_url().'user/investee_info/product_info';?>"><h4>Product Information</h4></a>
        </div>
        <div class="sidebar-filter-section <?php echo $activePanel =='financial_info'?'active':''?>">
          <a href="<?php echo base_url().'user/investee_info/financial_info';?>"><h4>Financial Information</h4></a>
        </div>
        <div class="sidebar-filter-section <?php echo $activePanel =='achievements'?'active':''?>">
          <a href="<?php echo base_url().'user/investee_info/achievements';?>"><h4>Achievements</h4></a>
        </div>
        <div class="sidebar-filter-section <?php echo $activePanel =='updates'?'active':''?>">
          <a href="<?php echo base_url().'user/investee_info/updates';?>"><h4>Updates</h4></a>
        </div>
      </div>
      <button id="investeeSaveClose" type="submit" class="btn btn-default btn-gray col-xs-12">Save &amp; Close</button>
</div>