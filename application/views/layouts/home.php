<!DOCTYPE html>
<html>

    <!-- PAGE HEAD -->
    <?php $this->load->view('front-end/head'); ?>
    <!-- PAGE HEAD END -->

    <body>
    	
    	<div id="st-container" class="st-container">
	        <div class="st-pusher">
	        <!--this menu will be under the push wrapper-->
	            <div class="st-content">
	            <!-- this is the wrapper for the content -->
	                <div class="st-content-inner">
	
	                    <!-- HEADER CONTAINER ENDS-->
	                    <?php $this->load->view('front-end/header'); ?>
	                    <!-- HEADER CONTAINER ENDS-->
	
	                    <?php if($this->uri->segment(2) == 'index' || $this->uri->segment(2) == '' || $this->uri->segment(2) == 'login'){ ?>
	                    <!-- PAGE BANNER -->
	                    <?php $this->load->view('front-end/top-bar-slider', $banners); ?>
	                    <!-- PAGE BANNER END -->
	                    <?php } ?>
	                    <!-- div class="main-page" -->
	                    <div class="main">
	                        <div class="wrap">                           
	                            <?= $content ?>
	                            <?php $this->load->view('front-end/panels'); ?>
	
	                            <?php $this->load->view('front-end/testimonial'); ?>
	                        </div>  
	                    </div>    
	
	
	
	
	                <!-- FOOTER CONTAINER -->
	                    <?php $this->load->view('front-end/footer'); ?>
	                <!-- FOOTER CONTAINER ENDS -->
	
	
	                </div>
	            </div>
        	</div>
    	</div>

    <!-- ?= js('slick.min.js') ? -->

    <!-- ?= js('Chart.min.js') ? -->

    <!--[if IE]>
            <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <?= js('app.js') ?>     

    <?= js('responsiveCarousel.min.js') ?>

    <?= js('common.js') ?>

    </body>
    
</html>