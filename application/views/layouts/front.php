<!DOCTYPE html>
<html>
    <!-- PAGE HEAD -->
    <?php $this->load->view('front-end/head'); ?>
    <!-- PAGE HEAD END -->

    <body>
    <div id="st-container" class="st-container">
    <div class="st-pusher">
    <!--this menu will be under the push wrapper-->
    <div class="st-content">
    <!-- this is the wrapper for the content -->
    <div class="st-content-inner">
        <!-- HEADER CONTAINER ENDS-->
        <?php $this->load->view('front-end/header'); ?>
        <!-- HEADER CONTAINER ENDS-->

        <?php if($this->uri->segment(2) == 'index' || $this->uri->segment(2) == ''){ ?>
        <!-- PAGE BANNER -->
        <?php $this->load->view('front-end/top-bar-slider', $banners); ?>
        <!-- PAGE BANNER END -->
        <?php } ?>
        <!-- div class="main-page" -->
        <div class="main">
            <div class="wrap">
                <?= $content ?>
            </div>
        </div>    




        <!-- FOOTER CONTAINER -->
        <?php $this->load->view('front-end/footer'); ?>
        <!-- FOOTER CONTAINER ENDS -->
    </div>
    </div>
    </div>
    </div>


    <!-- ?= js('slick.min.js') ? -->
    <?= js('jquery.colorbox.js') ?>
    <?= js('jquery.validate.min.js') ?>
    <?= js('app.js') ?>
    <?= js('isotope.pkgd.min.js') ?>

    <!--[if IE]>
            <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

<!-- Slider Script -->
    <?= jsdefer('jquery.flexslider.js') ?>

    <script type="text/javascript">
        //$(function(){
    //  var SyntaxHighlighter;
    //      SyntaxHighlighter.all();
    //    });
        $(window).load(function(){
          $('.flexslider').flexslider({
            animation: "slide",
            start: function(slider){
              $('body').removeClass('loading');
            }
          });
        });
    </script>



    <script type="text/javascript">

      // isotope managment team
  $('#management_team').isotope({
      itemSelector : '.team_member'
    });



    
    $(function(){
      $('.crsl-items').carousel({
        visible: 3,
        itemMinWidth: 290,
        itemEqualHeight: 370,
        itemMargin:25
      });
	  
		$("#owl-demo").owlCarousel({
			items : 3,
			lazyLoad : true,
			navigation : true
		});
	  
      
      $("a[href=#]").on('click', function(e) {
        e.preventDefault();
      });
    });
    </script>       

    <?= js('responsiveCarousel.min.js') ?>

    <?= js('common.js') ?>

    </body>
</html>