<!DOCTYPE html>
<html>
    <!-- PAGE HEAD -->
    <?php $this->load->view('front-end/investee-head'); ?>
    <!-- PAGE HEAD END -->

<body>
<div id="st-container" class="st-container">
    
    <?php // $this->load->view("front-end/investee-navbar"); ?>    
        
    <div class="st-pusher">
        <!--this menu will be under the push wrapper-->
        <div class="st-content">
        <!-- this is the wrapper for the content -->
            <div class="st-content-inner">

<!--               <div class="clearfix">
                  <div id="st-trigger-effects" class="column">
                    <button class="menu-btn" data-effect="st-effect-2"><img src="<?php echo base_url(); ?>assets/images/toggle.png"></button>
                  </div>
              </div> -->

                <!-- HEADER CONTAINER -->
                <?php $this->load->view("front-end/header"); ?>
                <!-- HEADER CONTAINER ENDS-->
                <?php if($this->uri->segment(2) == 'index' || $this->uri->segment(2) == ''){ ?>
                <?php $this->load->view('front-end/top-bar-slider', $banners); ?>
                <?php } ?>

                <div class="container-fluid investee-nav">
                    <!--Toggle search panel-->
                    <script>
                        $(document).ready(function(){
                          $(".search-icon").click(function(){
                            $("#Searchfilelds").toggle();
                          });
                        });
                    </script>
                    <script type="text/javascript" >
                        $(document).ready(function()
                        {
                            $("#notificationLink").click(function(){
                                $("#notificationContainer").fadeToggle(300);
                                $("#notification_count").fadeOut("slow");
                                return false;
                            });

                            //Document Click
                            $(document).click(function()
                            {
                                $("#notificationContainer").hide();
                            });
                        
                            //Popup Click
                            $("#notificationContainer").click(function()
                            {
                                //return false
                            });

                        });
                    </script>

                    <div class="row hidden">
                        <div class="container wrap">
                            <div class="row">
                                <div class="col-sm-12">
                                    <ul class="investee-navbar">
                                        <?php if (!isset($active)) $active = 0; ?>
                                        <li <?php
                                        if ($active == 1) {
                                            echo "class='link-active'";
                                        }
                                        ?>><a href="<?php echo base_url(); ?>investee/index">Profile</a></li>
                                        <li><a href="#" id="expert" name="expert" class="cboxElement" >Ask for Expert Advice</a></li>
                                        <li <?php
                                        if ($active == 2) {
                                            echo "class='link-active'";
                                        }
                                        ?>><a href="<?php echo base_url(); ?>investee/meeting" id="meetingAndSchedule" >Meeting Scheduler</a></li>
                                        <li <?php
                                        if ($active == 3) {
                                            echo "class='link-active'";
                                        }
                                        ?>><a href="<?php echo base_url(); ?>investee/offers">Bids & offers</a></li>
                                        <li>
                                          <ul id="nav">
                                            <li id="notification_li">
                                                <?php if ($unseen_notifications > 0) { ?>  
                                                <span id="notification_count"><?php echo $unseen_notifications; ?></span>
                                                <?php } ?>
                                                <a href="#" id="notificationLink"><span class="notification-icon"></span></a>
                                                <div id="notificationContainer">
                                                    <?php $this->load->view("user/notifications"); ?>
                                                </div>
                                             </li>
                                         </ul>
                                        </li>    
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="main">
                    <div class="wrap">
                        <?= $content; ?>                        
                    </div>    
                </div>

                <!-- FOOTER CONTAINER -->
                <?php $this->load->view('front-end/footer'); ?>
                <!-- FOOTER CONTAINER ENDS -->
            </div>

        </div>
    </div>
</div>


<!-- POPUP CONTAINER -->
<div style="display: none;">
    <div id="advice_popup" class="container pop-content">
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <h3 class="form-header top-margin-10">Ask for Expert Advice</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <div class="sendto">  To: Equity Crest</div>
            </div>
        </div>
        <div class="row">
            <form id="expert-advice-form">
                <div class="col-xs-12 col-sm-12">
                    <div class="row">
					<div class="col-sm-12">
						<textarea name="query" id="query" class="form-control form-area" placeholder="your query" rows="10"></textarea>
						<input type="hidden" class="q-id" name="question_id" value="" />
					</div>
                    </div>
                    <div class="row">
						<div class="col-sm-12">
                        <label id="query_error" for="query" class="error" style="float:left;">&nbsp;</label>
						<input class="eq-btn pull-right" type="submit" value="OK" />
						</div> 
					</div>
                </div>
            </form>
        </div>
    </div>
</div>
        
<?= jsdefer('jquery.flexslider.js') ?>
<?= js('jquery.flexslider.js') ?>
<?= js('responsiveCarousel.min.js') ?>
<?= js('app.js') ?>

<?= js('isotope.pkgd.min.js') ?>

<?= js('common.js') ?>



<script>
    $(function(){
      $('.crsl-items').carousel({
            visible: 3,
            itemMinWidth: 290,
            itemEqualHeight: 370,
            itemMargin:25
        });
    });

    $(document).ready(function() {
        //$('.dropdown-toggle').dropdown();
        /*
        slider = $('#top-bar-slider').slick({
            dots: true,
            autoplay: true,
            autoplaySpeed: 5000,
            arrows: false
        });
        */

        $("#expert, #expert_navbar").colorbox({inline: true, href: '#advice_popup', innerWidth: '80%', maxWidth: '450px'});//, innerHeight: '335px'

        $('.box-close').on('click', function() {
            $.colorbox.close()
        });

        $("#expert-advice-form").on('submit', (function(e) {
            e.preventDefault();
            $.ajax({
                    url: "<?php echo base_url(); ?>investee/ask_advice",
                    type: 'POST',
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false
                }).done(function(data) {
                    if (data == "success") {
                        $.colorbox.close();    
                    } else {
                        $("#query_error").text(data);
                    }
            });

        }));

        
        $('.notification-btn').on('click',function(){
              $('.notification-num').css('visibility', 'hidden'); 
              $('.notification-num').html('0');
             $.ajax({
                url: "<?php echo base_url(); ?>user/get_notifications"
            }).done(function(html) {
                  if (html.trim() == "") {
                      data = "<li class='zero-notification'>no notifications yet</li>";
                        $('.notifications').css('display', 'block');
                        $('.notifications').html(data);
                    } else {
                        $('.notifications').css('display', 'block');
                        $('.notifications').html(html);
                    }                    
            });  

        });

        $("a[href=#]").on('click', function(e) {
            e.preventDefault();
        });

        $(document).click(function() {
            if ($('.notifications').is(':visible')) {
                $('.notifications').slideToggle('slow');
            }
        });
    });
</script>
        <?= js('classie.js') ?>
        <?= js('sidebarEffects.js') ?>
    </body>
</html>    
