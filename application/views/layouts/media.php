<!DOCTYPE html>
<html>
    <!-- PAGE HEAD -->
    <?php $this->load->view('front-end/media_head'); ?>
    <!-- PAGE HEAD END -->

<body>
<div id="st-container" class="st-container">
    
    <div class="st-pusher">
        <!--this menu will be under the push wrapper-->
        <div class="st-content">
        <!-- this is the wrapper for the content -->
            <div class="st-content-inner">

                <!-- HEADER CONTAINER -->
                <?php $this->load->view("front-end/header"); ?>
                <!-- HEADER CONTAINER ENDS-->

                <?php $this->load->view('front-end/top-bar-slider', $banners); ?>
                

                <div class="main">
                    <div class="wrap">
                        <?= $content; ?>
                    </div>    
                </div>

                <!-- FOOTER CONTAINER -->
                <?php $this->load->view('front-end/footer'); ?>
                <!-- FOOTER CONTAINER ENDS -->
            </div>

        </div>
    </div>
</div>
       
<?= jsdefer('jquery.flexslider.js') ?>
<?= js('jquery.flexslider.js') ?>
<?= js('responsiveCarousel.min.js') ?>
<?= js('app.js') ?>

<script type="text/javascript">
    $(window).load(function(){
      $('.flexslider').flexslider({
        animation: "slide",
        start: function(slider){
          $('body').removeClass('loading');
        }
      });
    });
</script>

<script>
    $(function(){
      $('.crsl-items').carousel({
            visible: 3,
            itemMinWidth: 290,
            itemEqualHeight: 370,
            itemMargin:25
        });
    });

    $(document).ready(function() {
        //$('.dropdown-toggle').dropdown();
        /*
        slider = $('#top-bar-slider').slick({
            dots: true,
            autoplay: true,
            autoplaySpeed: 5000,
            arrows: false
        });
        */


        $("#expert-advice-form").on('submit', (function(e) {
            e.preventDefault();
            $.ajax({
                    url: "<?php echo base_url(); ?>investee/ask_advice",
                    type: 'POST',
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false
                }).done(function(data) {
                    if (data == "success") {
                        $.colorbox.close();    
                    } else {
                        $("#query_error").text(data);
                    }
            });

        }));

        
        $('.notification-btn').on('click',function(){
              $('.notification-num').css('visibility', 'hidden'); 
              $('.notification-num').html('0');
             $.ajax({
                url: "<?php echo base_url(); ?>user/get_notifications"
            }).done(function(html) {
                  if (html.trim() == "") {
                      data = "<li class='zero-notification'>no notifications yet</li>";
                        $('.notifications').css('display', 'block');
                        $('.notifications').html(data);
                    } else {
                        $('.notifications').css('display', 'block');
                        $('.notifications').html(html);
                    }                    
            });  

        });
        $(document).click(function() {
            if ($('.notifications').is(':visible')) {
                $('.notifications').slideToggle('slow');
            }
        });
    });
</script>
        <?= js('classie.js') ?>
        <?= js('sidebarEffects.js') ?>
    </body>
</html>    
