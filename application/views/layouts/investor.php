<!DOCTYPE html>
<html>
    <!-- PAGE HEAD -->
    <?php $this->load->view('front-end/investor-head'); ?>
    <!-- PAGE HEAD END -->

    <body>
    <div id="st-container" class="st-container">
    
    <?php //$this->load->view("front-end/investor-navbar"); ?>    
        
    <div class="st-pusher">
    <!--this menu will be under the push wrapper-->
    <div class="st-content">
    <!-- this is the wrapper for the content -->
    <div class="st-content-inner">

    <!--   <div class="clearfix">
          <div id="st-trigger-effects" class="column">
            <button class="menu-btn" data-effect="st-effect-2"><img src="<?php echo base_url(); ?>assets/images/toggle.png"></button>
          </div>
      </div> -->

        <!-- HEADER CONTAINER -->
        <?php $this->load->view("front-end/header"); ?>
        <!-- HEADER CONTAINER ENDS-->
        <?php if($this->uri->segment(2) == 'index' || $this->uri->segment(2) == ''){ ?>
        <?php $this->load->view('front-end/top-bar-slider', $banners); ?>
        <?php } ?>
       
        <div class="main">
            <div class="wrap">
                <?= $content; ?>               
            </div>    
        </div>

        <!-- Popup Pledge -->
        <div class="modal fade" id="pledgeModal" tabindex="-1" role="dialog" aria-labelledby="pledgeModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="pledgeModalLabel">Pledge</h4>
              </div>
              <div class="modal-body">
                <form id="pledge-form" method="post">                   
                       
                    <div class="form-group">
                        <label for="founder-name">Amount</label>
                    </div>

                    <div class="form-group">
                        
                        <input type="text" name="amount" id="amount" value="" class="form-control form-input"  placeholder="Ex. 1,00,000">
                        <input type="hidden" name="investee_id" value="" id="investee_id" />
                        <input type="hidden" name="pledged_amount" value="0" id="pledged_amount" />
                        <label id="amount_error" for="amount" class="error">&nbsp;</label>
                        
                    </div>

                    <div class="form-group">

                        <input class="btn btn-eq-common" type="submit" value="OK" />

                    </div>                    

                </form>

              </div>
            </div>
          </div>
        </div>

        <!-- FOOTER CONTAINER -->
        <?php $this->load->view('front-end/footer'); ?>
        <!-- FOOTER CONTAINER ENDS -->
    </div>
    </div>
    </div>
    </div>   

            <?= jsdefer('jquery.flexslider.js') ?>
            <?= js('responsiveCarousel.min.js') ?>
            <?= js('app.js') ?>
<?= js('common.js') ?>
            <script type="text/javascript">
                //$(function(){
            //  var SyntaxHighlighter;
            //      SyntaxHighlighter.all();
            //    });
                $(window).load(function(){
                  $('.flexslider').flexslider({
                    animation: "slide",
                    start: function(slider){
                      $('body').removeClass('loading');
                    }
                  });
                });
            </script>



            <script type="text/javascript">
            $(function(){
			
			  $("#owl-demo").owlCarousel({
				items : 3,
				lazyLoad : true,
				navigation : true
			  });
			  $("#owl-demo-f").owlCarousel({
				items : 3,
				lazyLoad : true,
				navigation : true
			  });
			  
              $('#crsl-items-f').carousel({
                navigation: 'navbtns',
                visible: 3,
                itemMinWidth: 290,
                itemEqualHeight: 370,
                itemMargin:25
              });

              $('#crsl-items-p').carousel({
                navigation: 'navbtnsPledged',
                visible: 3,
                itemMinWidth: 290,
                itemEqualHeight: 370,
                itemMargin:25
              });
              
              $('.crsl-items').carousel({
                navigation: 'navbtns',
                visible: 3,
                itemMinWidth: 290,
                itemEqualHeight: 370,
                itemMargin:25
              });

              $("a[href=#]").on('click', function(e) {
                e.preventDefault();
              });
            });

            $(document).ready(function() {

                $('.tracker-panel').slideUp();
                $('#pref').click(function() {
                    $('.pref-panel').slideToggle();
                });

                $('.glyphicon-remove').click(function() {
                    $('.pref-panel').slideUp();
                });
                $('#tracker').click(function() {

                    $('.tracker-panel').slideToggle();
                });

                $('.deal-footer-links, .funds-req-status').on('click', '.follow', function() {
                    var invste_id = $(this).attr('data-user-id');
                    var this_var = $(this);
                    $.ajax({
                        url: "<?php echo base_url(); ?>user/follow",
                        type: 'POST',
                        data: {followId: invste_id}
                    })
                            .done(function(data) {

                        if (data.trim()=='success') {
                            
                            if(this_var.hasClass('deal-footer-links'))
                            {

                              this_var.html('<img src="<?php echo base_url(); ?>assets/images/fund_follow_active_40x40.png" alt="Unfollow"><h4>Unfollow</h4>');  
                            }
                            else
                            {
                                $('.deal-footer-links').find('.follow[data-user-id="'+invste_id+'"]').html('<img src="<?php echo base_url(); ?>assets/images/fund_follow_active_40x40.png" alt="Unfollow"><h4>Unfollow</h4>').removeClass('follow').addClass('unfollow'); 
                            }
                            
                            $('.follow-'+invste_id).html('<img src="<?php echo base_url(); ?>assets/images/fund_follow_active_icon.png" alt="Unfollow">').removeClass('follow').addClass('unfollow');

                            this_var.removeClass('follow');
                             this_var.addClass('unfollow');
                        } else {
                            
                            console.log('error');
                        }
                    });
                });

                $('.deal-footer-links, .funds-req-status').on('click', '.unfollow', function() {
                    var invste_id = $(this).attr('data-user-id');
                    var this_var = $(this);
                    $.ajax({
                        url: "<?php echo base_url(); ?>user/unfollow",
                        type: 'POST',
                        data: {followId: invste_id}
                    })
                            .done(function(data) {
                        if (data.trim() == 'success') {
                            if(this_var.hasClass('deal-footer-links'))
                            {
                              this_var.html('<img src="<?php echo base_url(); ?>assets/images/fund_follow_40x40.png" alt="Follow"><h4>Follow</h4>');
                            }
                            else
                            {
                                $('.deal-footer-links').find('.unfollow[data-user-id="'+invste_id+'"]').html('<img src="<?php echo base_url(); ?>assets/images/fund_follow_40x40.png" alt="Follow"><h4>Follow</h4>').removeClass('unfollow').addClass('follow');
                            }

                            $('.follow-'+invste_id).html('<img src="<?php echo base_url(); ?>assets/images/fund_follow_icon.png" alt="Follow">').removeClass('unfollow').addClass('follow');
                             this_var.removeClass('unfollow');
                             this_var.addClass('follow');
                        } else {
                            console.log('error');
                        }
                    });
                });

                $('#amount').on('focus',function(){
                    $('#amount_error').html("&nbsp;");
                })

                $('.deal-footer-links, .funds-req-status').on('click', '.pledge', function() {
                    var invste_id = $(this).attr('data-user-id');
                    $("#pledge-form #investee_id").val(invste_id);
                    if($(this).hasClass('deal-footer-links'))
                    {
                        $(this).addClass('pledging');
                    }
                    else
                    {
                        $('.deal-footer-links').find('.pledge[data-user-id="'+invste_id+'"]').addClass('pledging');
                    }

                    $('#pledgeModal').modal("show");


                    //$.colorbox({inline: true, href: '#pledge-pop', maxWidth: '450px', innerWidth: '80%'});//, innerHeight: '185px'
                });

                $('.deal-footer-links, .funds-req-status').on('click', '.pledged', function(e) {
                    var invste_id = $(this).attr('data-user-id');
                    e.preventDefault();
                    $.get("<?php echo base_url(); ?>investee/getpledgedamount/" +  invste_id,function(data) {
                            $(this).addClass('pledging');
                            $("#pledge-form #investee_id").val(invste_id);
                            $("#pledge-form #pledged_amount").val(data);
                              if ($("#pledge-form #pledged_amount").val() != 0) {
                                $("#pledge-form #amount").val($("#pledge-form #pledged_amount").val());
                              }

                            $('#pledgeModal').modal("show");

                            // $.colorbox({inline: true, href: '#pledge-pop', maxWidth: '450px', innerWidth: '80%'});//, innerHeight: '185px'                            
                    });
                 });   

                $("#pledge-form").on('submit', (function(e) {
                    e.preventDefault();
                    $.ajax({
                        url: "<?php echo base_url(); ?>investee/pledge",
                        type: 'POST',
                        dataType: 'json',
                        data: new FormData(this),
                        contentType: false,
                        cache: false,
                        processData: false
                    })
                            .done(function(data) {
                        if (data.result == 'success') {
                            var lID = "#i-" + data.investeeId;

                           // $(lID + ' .deal-footer-links .pledging').text('Pledged');
                             $(lID + ' .deal-footer-links .pledging').html('<img src="<?php echo base_url(); ?>assets/images/fund_follow_pledge_active_40x40.png" alt="Pledged"><h4>Pledged</h4>');

                            $('.pledge-'+data.investeeId).html('<img src="<?php echo base_url(); ?>assets/images/fund_follow_pledge_active_icon.png" alt="Pledged">');

                            $(lID + ' .deal-funds .funds-bar .progress .progress-bar').width(Math.floor(data.raised) + "%");
                            $(lID + ' .fund-stats .funds-raised #raised-percent').text(Math.floor(data.raised) + "%")
                            $(lID + ' .deal-footer-links .pledging').addClass('pledged');
                            $(lID + ' .deal-footer-links .pledging').removeClass('pledge pledging');

                        } else {
                            $('#amount_error').text(data.result);
                            return;
                            //console.log('error');
                        }
                         $("#pledge-form input[type=hidden]").val("");
                        $('#pledgeModal').modal("hide");

                    });

                }));

                $(document).bind('show.bs.modal', function(){
                   
                      $('#amount_error').text("");
                      // $("#pledge-form #amount").val("");
                   
                });
                

                $(".search-icon").click(function() {
                    $(this).parents("li").addClass("link-active");
                    $(this).parents("li").siblings("li").removeClass("link-active");
                    $(".Searchfilelds").toggle();
                });

            /*
            $('.notification').on('click',function(){
                      $('#notification_count').css('visibility', 'hidden'); 
                      $('#notification_count').html('0');
                     $.ajax({
                        url: "http://equitycrest.com/user/get_notifications"
                    }).done(function(html) {
                        $('#notificationContainer').html(html);
                    });  
 
                });
              */

                $("#notificationLink").click(function()
                {
                  //$("#notificationContainer").fadeToggle(300);
                  //$("#notification_count").fadeOut("slow");
                  return false;
                });

                $(document).click(function() {
                    if ($('.notifications').is(':visible')) {
                        $('.notifications').slideToggle('slow');
                    }
                });

                if ($('.deals-slider').length) {
                    slider = $('.deals-slider').slick({
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        dots: false,
                        autoplay: false,
                        autoplaySpeed: 5000,
                        arrows: true,
                        nextArrow: '<button type="button" class="slick-next"></button>',
                        responsive: [
                            {
                                breakpoint: 1170,
                                settings: {
                                    slidesToShow: 3,
                                    slidesToScroll: 3,
                                    infinite: true,
                                    dots: true
                                }
                            },
                            {
                                breakpoint: 1024,
                                settings: {
                                    slidesToShow: 3,
                                    slidesToScroll: 2
                                }
                            },
                            {
                                breakpoint: 768,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1
                                }
                            }
                        ]
                    });
                }

            });
        </script>
        <?= js('classie.js') ?>
        <?= js('sidebarEffects.js') ?>
    </body>
</html>