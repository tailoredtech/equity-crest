<div id="page-sidebar" class="scrollable-content">

                <div id="sidebar-menu">
                    <ul>
                        <li>
                            <a href="<?php echo base_url(); ?>admin/index" title="Dashboard">
                                <i class="glyph-icon icon-dashboard"></i>
                                Dashboard
                            </a>
                        </li>
                        
                        <li>
                            <a href="<?php echo base_url(); ?>admin/notifications" title="Notifications">
                                <i class="glyph-icon icon-folder-open"></i>
                                Notifications
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>admin/meetings" title="Meetings">
                                <i class="glyph-icon icon-folder-open"></i>
                                Meetings
                            </a>
                        </li>
                        
                        <li>
                            <a href="javascript:;" title="Users">
                                <i class="glyph-icon icon-folder-open"></i>
                                Users
                            </a>
                            <ul>
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/active_users" title="Active Users">
                                        <i class="glyph-icon icon-chevron-right"></i>
                                        Active
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/inactive_users" title="Inactive Users">
                                        <i class="glyph-icon icon-chevron-right"></i>
                                        Inactive
                                    </a>
                                </li>
                            </ul>
                        </li>
                        
                        <li>
                            <a href="javascript:;" title="Investors">
                                <i class="glyph-icon icon-folder-open"></i>
                                Investors
                            </a>
                            <ul>
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/active_investors" title="Listing">
                                        <i class="glyph-icon icon-chevron-right"></i>
                                        Active
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/inactive_investors" title="Listing">
                                        <i class="glyph-icon icon-chevron-right"></i>
                                        Inactive
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/download/investor" title="Listing">
                                        <i class="glyph-icon icon-chevron-right"></i>
                                        Download
                                    </a>
                                </li>
                            </ul>
                        </li>
                        
                        <li>
                            <a href="javascript:;" title="Investees">
                                <i class="glyph-icon icon-folder-open"></i>
                                Investees
                            </a>
                            <ul>
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/active_investees" title="Listing">
                                        <i class="glyph-icon icon-chevron-right"></i>
                                        Active
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/semiactive_investees" title="Listing">
                                        <i class="glyph-icon icon-chevron-right"></i>
                                        Semiactive
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/inactive_investees" title="Listing">
                                        <i class="glyph-icon icon-chevron-right"></i>
                                        Inactive
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/funded_investees" title="Listing">
                                        <i class="glyph-icon icon-chevron-right"></i>
                                        Funded
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/featured_investees" title="Listing">
                                        <i class="glyph-icon icon-chevron-right"></i>
                                        Featured
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/advice_requests" title="Fields">
                                        <i class="glyph-icon icon-chevron-right"></i>
                                        Advice Requests
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/download/investee" title="Listing">
                                        <i class="glyph-icon icon-chevron-right"></i>
                                        Download
                                    </a>
                                </li>
                            </ul>
                        </li>
                        
                        <li>
                            <a href="javascript:;" title="Channel Partner">
                                <i class="glyph-icon icon-folder-open"></i>
                                Channel Partner
                            </a>
                            <ul>
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/active_partners" title="Listing">
                                        <i class="glyph-icon icon-chevron-right"></i>
                                        Active
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/inactive_partners" title="Listing">
                                        <i class="glyph-icon icon-chevron-right"></i>
                                        Inactive
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url() ?>admin/referred" title="Listing">
                                        <i class="glyph-icon icon-chevron-right"></i>
                                        Companies Referred
                                    </a>
                                </li>
                                
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;" title="Other">
                                <i class="glyph-icon icon-folder-open"></i>
                                Other
                            </a>
                            <ul>
                                <li>
                                    <a href="<?php echo base_url() ?>admin/subscribed_users" title="Subscribed Users">
                                        <i class="glyph-icon icon-chevron-right"></i>
                                        Subscribed Users
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url() ?>admin/banners" title="Banners">
                                        <i class="glyph-icon icon-chevron-right"></i>
                                        Banners
                                    </a>
                                </li>
                                
                            </ul>

                            <li>
                            <a href="<?php echo base_url() ?>admin/mailing" title="Mailing">
                                <i class="glyph-icon icon-folder-open"></i>
                                  Mailing
                            </a>
                            
                          </li>
                        </li>
                    </ul>
                    <div class="divider mrg5T mobile-hidden"></div>
                    <div class="text-center mobile-hidden">
                        <div class="button-group display-inline">
                            <a href="javascript:;" class="btn medium bg-green tooltip-button" data-placement="top" title="Messages">
                                <i class="glyph-icon icon-flag"></i>
                            </a>
                            <a href="javascript:;" class="btn medium bg-green tooltip-button" data-placement="top" title="Mailbox">
                                <i class="glyph-icon icon-inbox"></i>
                            </a>
                            <a href="javascript:;" class="btn medium bg-green tooltip-button" data-placement="top" title="Content">
                                <i class="glyph-icon icon-hdd"></i>
                            </a>
                        </div>

                    </div>
                </div>

            </div><!-- #page-sidebar -->