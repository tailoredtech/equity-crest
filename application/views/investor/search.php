<div class="container">
  <div class="row">
    <div class="col-xs-6 col-sm-8 block-title">
      <h2>Results</h2>
    </div>
        <?php $max = count($users);
        if ($max > 0) { ?>		
            <div class="col-xs-6 col-sm-4 block-title pull-right">
                <h2>
                    <nav class="slidernav">
                      <div id="navbtns" class="clearfix">
                        <a href="#" title="Previous" class="previous"><img src="<?php echo base_url(); ?>assets/images/slick-prev.png"></img></a>
                        <a href="#" title="Next" class="next"><img src="<?php echo base_url(); ?>assets/images/slick-next.png"></img></a>
                      </div>
                    </nav>
                </h2>
            </div>
        <?php } ?>  		  
  </div>
</div>
<?php $max = count($users);
if ($max) { ?>	
    <div class="container">
        <div class="row">
            <div id="w">
                <div class="crsl-items" data-navigation="navbtns">
                    <div class="crsl-wrap">
                        <?php
                        $count = 0;
                        foreach ($users as $user) {
                            $data['user'] = $user;
                            $this->load->view('front-end/grid-single-item', $data);
                            $count++;
                        }
                        ?>
                    </div><!-- @end .crsl-wrap -->
                </div><!-- @end .crsl-items -->
            </div>
        </div>   
    </div>
<?php } else { ?>
    <div class="row content-box">
        <div>
            <h2>No investee found for the given criteria</h2>
        </div>
    </div>        
<?php }  ?>  
<script>
    $(document).ready(function(){
       if($('.deals-slider').length){
            slider=$('.deals-slider').slick({
                slidesToShow: 3,
                slidesToScroll: 3,
                dots: false,
                autoplay: false,
                autoplaySpeed:5000,
                arrows:true,
                nextArrow:'<button type="button" class="slick-next glyphicon glyphicon-chevron-right"><span class="glyphicon glyphicon-chevron-right"></span></button>'
            }); 
       }
        $(".search-bar").toggle();
    });
</script>