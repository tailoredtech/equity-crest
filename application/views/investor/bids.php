<div class="main">
    <div class="container wrap">
        <div class="row">
            <div class="col-sm-10 block-title">
                <h2>Bids & Offers</h2>
            </div>
            <div class="btn-group profile-btn-group col-sm-2">
                <a href="<?php echo base_url(); ?>investor/bids" class="btn btn-default active">Bids</a>
                <a href="<?php echo base_url(); ?>investor/offers" class="btn btn-default ">Offers</a>
            </div>
        </div>
    </div>
    <div class="container wrap">
        <div class=" row content-box">

            <div class="row">
                <div class="col-sm-12">    
                    <div class="row terms-margin">
                        <table class="table table-striped table-bordered bids-font">
                            <tbody>
                                <tr>
                                    <td>Sr. No</td>
                                    <td>Date posted</td>
                                    <td>Company Name</td>
                                    <td>Sector</td>
                                    <td>No of <br>shares</td>
                                    <td>Holdings</td>
                                    <td>Amt per <br>share</td>
                                    <td>Total <br>Amount</td>
                                    <td></td>
                                </tr>
                            </tbody>
                            <?php
                            $i = 0;
                            foreach ($bids as $bid) {
                                ++$i
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo date('M, d H:i A', strtotime($bid['date_created'])); ?></td>
                                    <td><?php echo $bid['company_name'] ?></td>
                                    <td><?php echo $bid['sector_name'] ?></td>
                                    <td><?php echo $bid['shares'] ?></td>
                                    <td><b><?php echo $bid['holdings'] ?></b>%</td>
                                    <td><?php echo $bid['amount'] ?></td>
                                    <td><b><?php echo $bid['total_amount'] ?></b></td>
                                    <td>
                                        <input type="submit" value="Interested" name="submit" class="join-btn col-sm-12 center lato-regular bids-interested"></td>
                                </tr>
<?php } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div style="display: none">
    <div class="pop-content" id="bid-pop">
        <h3 class="form-header">Bid </h3>
        <div class="askexpert">
            <form id="schedule-meeting-form" method="post">
                <input type="hidden" name="offer_id"  value="100" class="form-control form-input"  placeholder="">
                <div class="form-group">
                    <label for="Shares Interested" class="col-sm-3 lato-regular black">Shares Interested</label>
                    <div class="col-sm-9">
                        <input type="text" name="shares"  value="" class="form-control form-input"  placeholder="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="founder-name" class="col-sm-3 lato-regular black">Remark</label>
                    <div class="col-sm-9">
                        <textarea name="remark"  class="form-control form-input form-area"  placeholder=""></textarea>

                    </div>
                </div>
                <div class="buttons">
                    <input type="submit" value="Send" class="btn btn-green" />
                </div>
            </form>
        </div>

    </div>
    
    <div id ="bid-success">
        <h5>Thank you for your interest. We will get back to you shortly</h5>
    </div>
</div>
<script>
    $(document).ready(function() {
        $(".bids-interested").on('click', function() {
            $.colorbox({inline: true, href: '#bid-pop', innerWidth: '400px', innerHeight: '300px'});
        });
    });

    $("#schedule-meeting-form").on('submit', (function(e) {
        e.preventDefault();
        $.ajax({
            url: "<?php echo base_url(); ?>investor/bid_interested",
            type: 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false
        })
                .done(function(data) {
            if (data == 'success') {
                 $.colorbox({inline: true, href: '#bid-success', innerWidth: '400px', innerHeight: '150px'});
            } else {
                console.log('error');
            }
            $.colorbox.close();
        });

    }));
</script>
