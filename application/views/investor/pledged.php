<div class="main">

    <div class="container">                            
        <!-- start find deals -->
        <div id="hot-deals" class="container-fluid deals-block ">
            <div class="row" style="margin-bottom: 25px;">
                <div class="container">
                    <div class="row block-header">
                        <div class="col-sm-4 block-title">
                            <h5 class="heading">Pledged</h5>
                        </div>
                        <div class="filter-options col-sm-2 col-sm-offset-2">
                            <!--<a class=" refine-details text-center">Refine Deals <span class="glyphicon glyphicon-chevron-down"></span></a>-->
                        </div>
                        <div class="right-options  col-sm-2  pull-right">
                            <div class="slider-marker pull-right">
                                <a class="text-center" href="#">
                                    <img src="<?php echo base_url(); ?>assets/images/dot.png">
                                </a>
                            </div>
                            <div class="extra-options pull-right">
                                <a class="extra-options-link text-center" href="#">=</a>
                            </div>
                        </div>
                    </div>
                    <div class="row deals-slider">
                        <div class="col-sm-12">
                            <div class="row">
                                <?php foreach ($users as $user) { ?>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="deal-single">
                                            <div class="title">
                                                <div class="deal-logo col-lg-5 col-md-5 col-sm-5 col-xs-5 text-center">
                                                    <img src="<?php echo base_url(); ?>uploads/users/<?php echo $user['user_id'] ?>/<?php echo $user['image'] ?>" width="100" height="100">
                                                </div>
                                                <div class="deal-title-block col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                    <h2 class="deal-title lato-bold"><?php echo $user['company_name'] ?></h2>
                                                    <div class="deal-location "><?php echo $user['city'] ?></div>
                                                    <div class="deal-category "><?php echo $user['sector_name'] ?></div>
                                                </div>
                                            </div>
                                            <div class="deal-description">
                                                <p class="text-center center-block ">
                                                    <?php echo word_limiter($user['products_services'], 12); ?> 
                                                </p>
                                            </div>
                                            <div class="deal-funds">
                                                <div class="fund-requested col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="funds-req pull-left">Funds Requested</div>
                                                    <div class="funds-req-amount pull-right"><?php echo $user['investment_required'] ?> INR</div>
                                                </div>
                                                <div class="funds-bar col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="fund-stats col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="funds-raised col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                        <h4 class="text-center lato-bold">60%</h4>
                                                        <p class="text-center">Raised</p>
                                                    </div>
                                                    <div class="funds-raised middle col-lg-6 col-md-6 col-sm-6 col-xs-6  col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
                                                        <h4 class="text-center lato-bold"><?php echo $user['commitment_per_investor'] ?> INR</h4>
                                                        <p class="text-center">Mininmum Investment</p>
                                                    </div>
                                                    <div class="funds-raised col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                        <h4 class="text-center lato-bold"><?php echo DayDifference(date('Y-m-d'), $user['validity_period']); ?></h4>
                                                        <p class="text-center">Days Left</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="deal-footer-links">
                                                <div class="row">
                                                    <div class="col-xs-4 text-center"><a href="#" class="text-center">Follow</a></div>
                                                    <div class="col-xs-4 text-center"><a href="#" class="text-center">Ignore</a></div>
                                                    <div class="col-xs-4 text-center"><a href="#" class="text-center">Pledge</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end find deals -->
    </div>
</div>
<script>
    $(document).ready(function(){
        
    });
</script>