<div class="container">
  <div class="row">
    <div class="col-xs-12 col-sm-12 block-title">
        <h2>Profile <a href="<?php echo base_url(); ?>user/investor_info" class="btn_a "><span class="glyphicon glyphicon-edit"></span> Edit Profile</a></h2> 
    </div>
  </div>
</div> fhsdfh
        <div class="row content-box">

            <div class="col-sm-12">
                <div class="investor-profile-box">
                    <div class="row shadow-border">
                        <div class="user-image text-center col-sm-2 ">
                            <?php if($investor['image']){ ?>
                            <img class="img-thumbnail" src="<?php echo base_url(); ?>uploads/users/<?php echo $investor['user_id'] ?>/<?php echo $investor['image']; ?>" width="77" height="77"/>
                            <?php }else{ ?>
                            <img class="img-thumbnail" src="<?php echo base_url(); ?>uploads/investor.jpg">
                            <?php } ?>
                            
                            <span class="change-img">Change</span>
                        </div>
                        <div class="col-sm-6">
                            <div class="user-name lato-bold">
                                <?php echo $investor['name']; ?>
                            </div>
                            <div class="user-designation">
                                <?php echo $investor['role']; ?>
                            </div>
                            <div class="user-location">
                                <?php echo $investor['city']; ?>
                            </div>
<!--                                    <div class="user-area">
                                <span class="lato-bold">Areas I can hep </span>: Finance, Fund Rising, Strategic advice
                            </div>-->
                        </div>
                        <div class="col-sm-2 social-ico space-20 pull-right">
							<?php if ($investor['linkedin_url'] != '') { ?>
                            <a target="_blank" href="<?php echo $investor['linkedin_url']; ?>" class="social-link" id="ln">Linked-in</a>
                            <?php } ?>
							<?php if ($investor['twitter_handle'] != '') { ?>
							<a target="_blank" href="<?php echo $investor['twitter_handle']; ?>" class="social-link" id="tw">Twitter</a>
                            <?php } ?>
							<?php if ($investor['fb_url'] != '') { ?>
                            <a target="_blank" href="<?php echo $investor['fb_url']; ?>" class="social-link" id="fb">Facebook</a>
							<?php } ?>
                        </div> 
                    </div>
                    <div class="profile-tabs row">
                        <div class="col-sm-12">
                            <div class="row">
                                <ul role="tablist" class="nav">
                                    <li>
                                        <div class=" col-sm-1">
                                            <a data-toggle="tab" role="tab" href="#overview">Overview</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="col-sm-1">
                                            <a data-toggle="tab" role="tab" href="#portfolio">Portfolio</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="col-sm-1">
                                            <a data-toggle="tab" role="tab" href="#activity">Activity</a>
                                        </div>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="tab-content row">
                        <!-- tab 1 -->
                        <div class="profile-content col-sm-12 tab-pane active" id="overview">
                            <div class="row">

                                <div class="col-sm-9">
                                    <h5 class="tab-header">Overview</h5>
                                    <?php if($investor['experience']){ ?>
                                    <div class="row">
                                        <div class="col-sm-12 ">
                                            <h6 class="tab-subheader lato-bold">Experience Summary</h6>
                                            <p style="word-wrap: break-word;">
                                                <?php echo strip_tags($investor['experience'], '<p><i><br><b><div><table><td><tr><th><ul><ol><li>');  ?>
                                            </p>
                                            <p class="dotted-border">
                                        </div>
                                    </div>
                                    <?php } ?>
                                    
                                     <?php if($investor['key_points']){ ?>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h6 class="tab-subheader lato-bold">In startup I look for</h6>
                                            <div class="row">
                                                <?php $points = explode(",",$investor['key_points']);
                                                   foreach($points as $point){
                                                ?>
                                                <div class="col-sm-4 list-item">
                                                    <div class="item-border">
                                                        <span class="tick-icon"></span><?php echo $point; ?>
                                                    </div>
                                                </div>
                                                <?php } ?>

                                            </div>
                                            <p class="dotted-border"></p>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    
                                    <?php if($investor['sector_expertise']){ ?>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h6 class="tab-subheader lato-bold">Sector Expertise</h6>
                                            <div class="row">
                                                <?php $sec_expertise = explode(",",$investor['sector_expertise']);
                                                   foreach($sec_expertise as $expert){
                                                ?>
                                                <div class="col-sm-4 list-item">
                                                    <div class="item-border">
                                                        <span class="tick-icon"></span><?php echo $expert; ?>
                                                    </div>
                                                </div>
                                                <?php } ?>

                                            </div>
                                            <p class="dotted-border"></p>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    
                                    <?php if($investor['mentoring_sectors']){ ?>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h6 class="tab-subheader lato-bold">Interests in Mentoring Sectors</h6>
                                            <div class="row">
                                                <?php $men_sectors = explode(",",$investor['mentoring_sectors']);
                                                   foreach($men_sectors as $men_sector){
                                                ?>
                                                <div class="col-sm-4 list-item">
                                                    <div class="item-border">
                                                        <span class="tick-icon"></span><?php echo $men_sector; ?>
                                                    </div>
                                                </div>
                                                <?php } ?>

                                            </div>
                                            <p class="dotted-border"></p>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    
                                    <?php if($investor['expertise']){ ?>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h6 class="tab-subheader lato-bold">Area of Expertise</h6>
                                            <div class="row">
                                                <?php $expertise = explode(",",$investor['expertise']);
                                                   foreach($expertise as $expert){
                                                ?>
                                                <div class="col-sm-4 list-item">
                                                    <div class="item-border">
                                                        <span class="tick-icon"></span><?php echo $expert; ?>
                                                    </div>
                                                </div>
                                                <?php } ?>

                                            </div>
                                            <p class="dotted-border"></p>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    
                                    <?php if($investor['duration']){ ?>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h6 class="tab-subheader">Duration </h6>
                                            <p><?php echo $investor['duration']; ?> hours/week</p>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>

<!--                                <div class="col-sm-3">
                                    <div class="row">
                                        <h6 class="tab-header ">Followers (10)</h6>
                                        <div class="col-sm-11">
                                            <div class="row followers">
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>

                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                            </div>
                                            <a href="#" class="pull-right">See all <span class="glyphicon glyphicon-chevron-down"></span></a>
                                        </div>
                                    </div>

                                </div>-->
                            </div>
                        </div>
                        <!-- end tab 1 -->

                        <!-- tab 2 -->
                        <div class="profile-content col-sm-12 tab-pane" id="portfolio">


                            <div class="row">

                                <div class="col-sm-12">
                                    <h6 class="tab-header ">Portfolio</h6>
                                    <div class="row center-block">
                                      <?php foreach($portfolios as $portfolio){ ?>
                                        <div class="col-sm-5 col-xs-12 deal-single" style="margin:25px;" >         
                                            <div class="title">
                                                <div class="deal-logo col-sm-5 text-center">
                                                    <div class="deal-image"><p><img src="<?php echo base_url() ?>uploads/portfolios/<?php echo $portfolio['id']; ?>/<?php echo $portfolio['image'] ?>"></p></div>
                                                </div>
                                                <div class="deal-title-block col-sm-7">
                                                                        <h2 class="deal-title lato-bold"><?php echo $portfolio['name']; ?></h2>
                                                              
                                                    <div class="deal-location "><?php echo $portfolio['location']; ?></div>
                                                    <div class="deal-location "><?php echo $portfolio['url']; ?></div>
                                                    <div class="deal-category "><?php echo $portfolio['sector']; ?></div>                
                                                </div>
                                            </div>
                                            <div class="deal-description">
                                                <p class="center-block ">
                                                    Role : <?php echo strip_tags($portfolio['role'], '<p><i><br><b><div><table><td><tr><th><ul><ol><li>'); ?> 
                                                </p>
                                            </div>

                                        </div>                                      
                                        <?php } ?>
<!--                                        <div class="col-sm-5 col-xs-10 small-box">
                                            <div class="deal-single">
                                                <div class="title">
                                                    <div class="deal-logo col-xs-5 text-center">
                                                        <img src="http://localhost/equitycrest/assets/images/temp-logo-4.jpg">
                                                    </div>
                                                    <div class="deal-title-block col-xs-6">
                                                        <h2 class="deal-title lato-bold">Agile Communications</h2>
                                                        <div class="deal-location ">London East Face</div>
                                                        <div class="deal-category ">media Publishing</div>
                                                    </div>
                                                </div>
                                                <div class="deal-description">
                                                    <p class="text-center center-block ">
                                                        Role : Board of Director
                                                    </p>
                                                </div>
                                            </div>
                                        </div>-->
                                    </div>
                                </div>

<!--                                <div class="col-sm-3">

                                    <div class="row">
                                        <h6 class="tab-header ">Followers (10)</h6>
                                        <div class="col-sm-11">
                                            <div class="row followers">
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>

                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                            </div>
                                            <a href="#" class="pull-right">See all <span class="glyphicon glyphicon-chevron-down"></span></a>
                                        </div>
                                    </div>

                                </div>-->

                            </div>

                        </div>
                        <!-- end tab 2 -->

                        <!-- tab 3 -->
                        <div class="profile-content col-sm-12 tab-pane" id="activity">

                            <div class="row">

                                <div class="col-sm-9">
                                    <h6 class="tab-header ">Activity</h6>
                                    <div class="row">

                                        <div class="col-xs-12">
                                            <?php foreach($activities as $activity){ ?>
                                                <div class="activity-item">
                                                   <?php echo $activity['activity']; ?> - <span class="activity-time"><?php echo date('M, d H:i A',strtotime($activity['date_created'])); ?></span>
                                                </div>
                                            <?php } ?>
                                        </div>


                                    </div>
                                </div>

<!--                                <div class="col-sm-3">

                                    <div class="row">
                                        <h6 class="tab-header ">Followers (10)</h6>
                                        <div class="col-sm-11">
                                            <div class="row followers">
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>

                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                            </div>
                                            <a href="#" class="pull-right">See all <span class="glyphicon glyphicon-chevron-down"></span></a>
                                        </div>
                                    </div>

                                </div>-->

                            </div>

                        </div>
                        <!-- end tab 3 -->
                    </div>
                </div>
            </div>
        </div>

<div style="display: none">
    <div id="ch-img" class="container pop-content">
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <h3 class="form-header top-margin-10">Upload Image</h3>
            </div>
        </div>
        <div class="row">
            <div class="top-margin-10">
            <form id="upload-image" action="<?php echo base_url(); ?>user/ajax_upload_image" method="post" enctype="multipart/form-data">
                <div class="col-xs-12 col-sm-12">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2 form-label">Image</label>
                            <div class="col-sm-10">
                                <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput" style="margin-bottom:0;">
                                    <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                                    <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="userfile"></span>
                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                                <label id="upload_error" for="question" class="error" style="height: 23px;padding: 0 0 0 1px;white-space: normal;">&nbsp;</label>
                            </div>
                        </div>
                    </div>
                    <input class="eq-btn col-sm-12 pull-right" type="submit" value="Submit" />
                </div>
            </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
      
        
    
        $(".change-img").colorbox({inline: true, href: '#ch-img', innerWidth: '80%', innerHeight: '175px', maxWidth: '400px'});//, innerHeight: '175px'
       $("#upload-image").on('submit',(function(e){
            e.preventDefault();
            $.ajax({
                    url: "<?php echo base_url(); ?>user/ajax_image_upload",
                    type: "POST",
                    data:  new FormData(this),
                    dataType :'json',
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(data){
                           if (data.error == ''){
                               $('.user-image img').attr('src','<?php echo base_url(); ?>'+data.img);
                               $.colorbox.close()                            
                           } else {
                                $('#upload_error').html(data.error);
                           }                        
                    },
                    error: function(){

                    } 
            });         
        }));
   });
             
</script>