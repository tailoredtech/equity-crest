<div class="main">
    <div class="container form-contain">
        <div class="row">
            <h4 class="form-header col-sm-12 lato-regular">Bid Registration</h4>
        </div>
    </div>
    <form id="bids-offer-form" action="<?php echo base_url(); ?>investor/post_bid_process" enctype="multipart/form-data" method="post" >
        <div class="container">
            <div class="row content-box">
                <div class="container">
                    <div clas="row">
                        <div class="col-sm-7 col-sm-offset-2">


                            <div class="name-fields">

                                <div class="row">
                                    <div class="form-group">
                                        <label for="name" class="col-sm-3 form-label">Company Name *</label>
                                        <div class="col-sm-9">
                                            <div class="select-style">
                                                <select class="form-control form-input" id="company" name="company" placeholder="Select Company">
                                                    <option value="">Select Company</option>
                                                    <?php foreach ($companies as $company) { ?>
                                                        <option value='<?php echo $company['id']; ?>'><?php echo $company['company_name']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="sector" class="col-sm-3 form-label">Sector</label>
                                        <div class="col-sm-9">
                                            <input type="hidden" name="sector" class="form-control " id="sector">
                                            <input type="text" name="sector_name" class="form-control " id="sectorName" readonly>
                                        </div>
                                    </div>
                                </div>               
                                <div class="row">
                                    <div class="form-group">
                                        <label for="shares" class="col-sm-3 form-label">No of shares</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="shares" class="form-control share-input" id="shares" placeholder="" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="holdings" class="col-sm-3 form-label">% holdings</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="holdings" class="form-control " id="holdings" placeholder="" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="amt" class="col-sm-3 form-label">Amount / Share</label>
                                        <div class="col-sm-9">
                                            <span class="col-sm-1 rupee-placeholder-bids "> <img src='<?php echo base_url(); ?>assets/images/rupee.png'style="margin-top: 9px; margin-left: -2px;"></span>
                                            <input type="text" name="amt" class="form-control rupee-input share-input" id="amt"  placeholder="" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="total_amt" class="col-sm-3 form-label">Total Amount</label>
                                        <div class="col-sm-9">
                                            <span class="col-sm-1 rupee-placeholder-bids "> <img src='<?php echo base_url(); ?>assets/images/rupee.png' style="margin-top: 9px; margin-left: -2px;"></span>
                                            <input type="text" name="total_amt" class="form-control rupee-input" id="total" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="join-now col-sm-6">
                                <input type="submit" value="Submit" name="submit" class="join-btn col-sm-12 pull-right lato-regular">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<?php
$sectors = array();
$i=0;
foreach ($companies as $company):
    
    $sectors[$i]['id'] = $company['id'];
    $sectors[$i]['sector'] = $company['sector_name'];
    $sectors[$i]['sector_id'] = $company['sector_id'];
    $i++;
endforeach;
//echo"<pre>";
//print_r($sectors);
//echo"</pre>";
$sectorList = json_encode($sectors);
?>
<script>
    $(document).ready(function() {
        $(".share-input").on("change", function() {
            var shares = $("#shares").val();
            var amt = $("#amt").val();

            var total = shares * amt;
            $("#total").val(total);
        });



        $('#company').on("change", function() {
            var selectId = $(this).val();
            var list = <?php echo $sectorList; ?>;
             console.log(list);
            for (var i = 0, len = list.length; i < len; i++) {

                if ( list[i].id== selectId) {
                    var selectSector = list[i].sector;
                    var selectSectorId = list[i].sector_id;
                    console.log(selectSector);
                }
            }
            $("#sector").val(selectSectorId);
            $("#sectorName").val(selectSector);
        });

    });
</script>
