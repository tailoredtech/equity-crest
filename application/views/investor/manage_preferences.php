
<div class="container">
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url();?>home/portfolio_all">Preferences</a></li>
      <li class="active"><a href="#">Manage</a></li>
    </ol>
    <form id="set-preferences-form" action="<?php echo base_url(); ?>investor/set_preferences" method="post">
    <div class="row">
    <div class="col-xs-12"><h1 class="page-title2" style="float:left">My Preferences</h1><div style="float:right"><input id="save-preff" class="btn btn-default btn-eq-common btn-eq-small" type="submit" value="Save" ></div></div>
    </div>
    <div class="row">
         <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 sidebar">                    
            <div class="sidebar-filter-box box-bottom-shadow manage-preferences-box">
                    
                    <div class="sidebar-filter-section">
                        <h4>By stage</h4>
                            <label><input type="checkbox" id="selectAllStage" name="stage[]" value="0" <?php echo (isset($preferred_stages) && in_array(6,$preferred_stages)) ? 'checked'  : ''; ?> />All</label>
                             <?php foreach ($stages as $stage) { ?>
                             <label><input type="checkbox" name="stage[]" value='<?php echo $stage['id']; ?>' <?php echo (isset($preferred_stages) && in_array($stage['id'],$preferred_stages)) ? 'checked'  : ''; ?> ><?php echo $stage['name']; ?></label>
                            <?php } ?>
                    </div>

                    <div class="sidebar-filter-section">
                      <h4>Investment Size</h4>
                      <label><input type="checkbox" id="selectAllSize" name="size[]" value="0" <?php echo (isset($preferred_sizes) && in_array(0,$preferred_sizes)) ? 'checked'  : ''; ?> /> All</label>
                           <label><input type="checkbox" name="size[]" value="1" <?php echo (isset($preferred_sizes) && in_array(1,$preferred_sizes)) ? 'checked'  : ''; ?> >Upto 10 lakhs</label>
                           <label><input type="checkbox" name="size[]" value="2" <?php echo (isset($preferred_sizes) && in_array(2,$preferred_sizes)) ? 'checked'  : ''; ?> >10 lakhs - 50 lakhs</label>
                          <label><input type="checkbox" name="size[]" value="3" <?php echo (isset($preferred_sizes) && in_array(3,$preferred_sizes)) ? 'checked'  : ''; ?> >50 lakhs - 1 Crore</label>
                          <label><input type="checkbox" name="size[]" value="4" <?php echo (isset($preferred_sizes) && in_array(4,$preferred_sizes)) ? 'checked'  : ''; ?> >1 Crore - 5 Crore</label>
                          <label><input type="checkbox" name="size[]" value="5" <?php echo (isset($preferred_sizes) && in_array(5,$preferred_sizes)) ? 'checked'  : ''; ?> >5 Crore & Above</label>
                    </div>

                    <div class="sidebar-filter-section">
                       <h4>Location</h4>
                            <label><input type="checkbox" id="selectAllLocation" name="location[]" value="0" <?php echo (isset($preferred_locations) && in_array(7,$preferred_locations)) ? 'checked'  : ''; ?> /> All</label>
                            <label><input type="checkbox"  name="location[]" value="1" <?php echo (isset($preferred_locations) && in_array(1,$preferred_locations)) ? 'checked'  : ''; ?> >Mumbai</label>
                            <label><input type="checkbox"  name="location[]" value="2" <?php echo (isset($preferred_locations) && in_array(2,$preferred_locations)) ? 'checked'  : ''; ?> >Delhi</label>
                            <label><input type="checkbox"  name="location[]" value="3" <?php echo (isset($preferred_locations) && in_array(3,$preferred_locations)) ? 'checked'  : ''; ?> >Chennai</label>
                            <label><input type="checkbox"  name="location[]" value="4" <?php echo (isset($preferred_locations) && in_array(4,$preferred_locations)) ? 'checked'  : ''; ?> >Bengaluru</label>
                            <label><input type="checkbox"  name="location[]" value="5" <?php echo (isset($preferred_locations) && in_array(5,$preferred_locations)) ? 'checked'  : ''; ?> >Hyderabad</label>
                            <label><input type="checkbox"  name="location[]" value="6" <?php echo (isset($preferred_locations) && in_array(6,$preferred_locations)) ? 'checked'  : ''; ?> >Others</label>
                    </div>
                </div>
        </div>

        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 col-main">
            <div class="sidebar-filter-box box-bottom-shadow manage-preferences-box">
                <div class="row">
                    <div class="col-sm-12 col-xs-12">
                        <div class="sidebar-filter-section">
                        <h4>By Sector</h4>
                        <div class="col-sm-4 col-xs-6 col-mobile remove_left_padding">
                            <label><input type="checkbox" id="selectAllSectors" name="sector[]" value="All" <?php echo (isset($preferred_sectors) && in_array(0,$preferred_sectors)) ? 'checked'  : ''; ?> />All</label>
                        </div>
                         <?php foreach ($sectors as $k=>$v) {
                           if($v['id']!='51' && $v['id']!='52')
                              { 
                          ?>
                         <div class="col-sm-4 col-xs-6 col-mobile remove_left_padding"><label><input type="checkbox" name="sector[]" value='<?php echo $v['id']; ?>' <?php echo (isset($preferred_sectors) && in_array($v['id'],$preferred_sectors)) ? 'checked'  : ''; ?> ><?php echo $v['name']; ?></label></div>
                        <?php } } ?>

                         <div class="col-sm-4 col-xs-6 col-mobile remove_left_padding"><label><input type="checkbox" name="sector[]" value='51' <?php echo (isset($preferred_sectors) && in_array('51',$preferred_sectors)) ? 'checked'  : ''; ?> ><?php echo "Others"; ?></label></div>
                        </div>        
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
<div class="row"><div class="col-xs-12">&nbsp;</div></div>
</div>
      
<!-- end find deals -->
