<div class="container">
  <div class="row">
  <div class="col-xs-9">
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url();?>home/portfolio_all">Portfolio</a></li>
      <li class="active">Preferred</li>
    </ol>
  </div>
  <div class="col-xs-3">
    <div class="breadcrumb pull-right"><a href="<?php echo base_url();?>investor/manage_preferences"><input class="btn btn-default btn-eq-common btn-eq-small" type="button" value="Manage" ></a></div>
  </div>
  </div>
    <div class="row">
      <?php  $this->load->view("front-end/filter_options"); ?>

      <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 col-main">
        <div class="row">
          <div class="col-xs-12 col-sm-9">
            <h1 class="page-title2"><?php echo $total_result; ?> Preferred</h1> 
          </div>
          
          <div class="col-sm-3 hidden-md hidden-xs">
           
            <?php if( count($users) > 0 ) { ?>
            <ul id="preferedTab" class="pull-right list-grid-view-tab" role="tablist" style="margin-top:0px"> 
              <li></li>
              <li role="presentation">
                  <a href="#prefered_list_view" id="prefered_list_view-tab" role="tab" data-toggle="tab" aria-controls="prefered_list_view" aria-expanded="true" class="list-view-tab-icon">
                    List
                </a>
              </li>
              
              <li role="presentation" class="active">
                <a href="#prefered_grid_view" role="tab" id="prefered_grid_view-tab" data-toggle="tab" aria-controls="prefered_grid_view" aria-expanded="false" class="grid-view-tab-icon">
                  Grid
                </a>
              </li>     
            </ul>
            <?php } ?>
          </div>
        </div>
      
       
          <div id="preferedTabContent" class="tab-content">
            <div role="tabpanel" id="prefered_grid_view" class="tab-pane fade active in" aria-labelledby="prefered_grid_view-tab">
              <div class="row">
                <?php
                    $count = 0;
                    if(!empty($users)){
                    foreach ($users as $user) {
                    ?>
                    <?php
                          $data['user'] = $user;
                          $data['colsize'] = 4;
                          $this->load->view('front-end/grid-single-item', $data);
    
                    }
                   }else{
                ?>
                 <div id="w" >
                    <div class="alert alert-success" role="alert">There are no tracked available</div>
                </div>
                    <?php
                    }
                  ?>
                    <!-- /div --><!-- @end .crsl-wrap -->
              </div>
            </div>
                 
            <div role="tabpanel" class="tab-pane fade hidden-sm hidden-xs" id="prefered_list_view" aria-labelledby="prefered_list_view-tab" >
              <div class="list-view-deals">
                <div class="row remove_margin list-view-deal-header">
                  <div class="col-md-4">Opportunity</div>
                  <div class="col-md-2">Sector</div>
                  <div class="col-md-1">Minimum</div>
                  <div class="col-md-2">Funds Raised</div>
                  <div class="col-md-3">Total</div>
                </div>
                <?php $max = count($users);
                  if ($max) { 
                      foreach ($users as $user) {
                          $data['user'] = $user;
                          $this->load->view('front-end/list-single-item', $data);
                      }
                   } else { ?>
                  <div id="w" >
                      <div class="alert alert-success" role="alert">There are no Preferred available</div>
                  </div>
                    <?php } ?>
              </div> <!-- ./list-view-deal -->     
            </div>
          </div>
        

      </div>
    </div>

</div>
