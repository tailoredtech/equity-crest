<div class="container">
  <div class="row">
    <div class="col-xs-9 col-sm-8 block-title">
      <h2>My Investments</h2>
    </div>
  </div>
</div>
<div class="row content-box">
    <div>
        <h2>This section will display information on the investments that you make through Equity Crest</h2>
    </div>
</div>
<script>
    $(document).ready(function(){
       if($('.deals-slider').length){
            slider=$('.deals-slider').slick({
                slidesToShow: 3,
                slidesToScroll: 3,
                dots: false,
                autoplay: false,
                autoplaySpeed:5000,
                arrows:true,
                nextArrow:'<button type="button" class="slick-next glyphicon glyphicon-chevron-right"><span class="glyphicon glyphicon-chevron-right"></span></button>'
            }); 
       }
    });
</script>