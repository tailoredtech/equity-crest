<div class="container">
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url();?>home/portfolio_all">Portfolio</a></li>
      <li class="active">Tracked</li>
    </ol>

    <div class="row">
      <?php  $this->load->view("front-end/filter_options"); ?>

      <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 col-main">
        <div class="row">
          <div class="col-xs-12 col-sm-8 remove_margin remove_padding">
            <h1 class="page-title2"><?php echo $total_result; ?> Tracked</h1>
          </div>
          <?php if( count($users) > 0 ) { ?>
          <div class="col-sm-4 hidden-md hidden-xs remove_margin remove_padding">
            <ul id="recommendedTab" class="pull-right list-grid-view-tab" role="tablist" style="margin-top:0px"> 
              <li role="presentation">
                  <a href="#recommended_list_view" id="recommended_list_view-tab" role="tab" data-toggle="tab" aria-controls="recommended_list_view" aria-expanded="true" class="list-view-tab-icon">
                    List
                </a>
              </li>
              
              <li role="presentation" class="active">
                <a href="#recommended_grid_view" role="tab" id="recommended_grid_view-tab" data-toggle="tab" aria-controls="recommended_grid_view" aria-expanded="false" class="grid-view-tab-icon">
                  Grid
                </a>
              </li>     
            </ul>
          </div>
          <?php } ?>
        </div>

        <div class="row">
        <div id="recommendedTabContent" class="tab-content">
          <div role="tabpanel" id="recommended_grid_view" class="tab-pane fade active in" aria-labelledby="recommended_grid_view-tab">
            <div class="row">
              <?php
                  $count = 0;
                  if(!empty($users)){
                  foreach ($users as $user) {
                  ?>
                  <?php
                        $data['user'] = $user;
                        $data['colsize'] = 4;
                        $this->load->view('front-end/grid-single-item', $data);
                  ?>
                  <?php
                        }
                    }else{
                ?>
                 <div id="w" >
                    <div class="alert alert-success" role="alert">There are no tracked available</div>
                </div>
                    <?php
                    }
                  ?>
                    <!-- /div --><!-- @end .crsl-wrap -->
            </div>
          </div>

          <div role="tabpanel" class="tab-pane fade hidden-sm hidden-xs" id="recommended_list_view" aria-labelledby="recommended_list_view-tab" >
            <div class="list-view-deals">
              <div class="row remove_margin list-view-deal-header">
                <div class="col-md-4">Opportunity</div>
                <div class="col-md-2">Sector</div>
                <div class="col-md-1">Minimum</div>
                <div class="col-md-2">Funds Raised</div>
                <div class="col-md-3">Total</div>
              </div>
              <?php $max = count($users);
                if ($max) { 
                    foreach ($users as $user) {
                        $data['user'] = $user;
                        $this->load->view('front-end/list-single-item', $data);
                    }
                 } else { ?>
                      <div id="w" >
                          <div class="alert alert-success" role="alert">There are no Tracked available</div>
                      </div>
                  <?php } ?>
            </div> <!-- ./list-view-deal -->     
          </div>
        </div>
        </div>
      </div>
    </div>
</div>
