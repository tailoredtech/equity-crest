<div class="main">
    <div class="container form-contain">
        <div class="row">
            <h4 class="form-header col-lg-12 col-md-12 col-sm-12 col-xs-12 lato-regular">Account Setting</h4>
        </div>
    </div>
    <div class="container">
        <div class="row content-box2">
            <div class="container   col-md-7 col-sm-7 col-xs-8  col-md-offset-2 col-xs-offset-2 ">
                
                <div class="row">
                    <div class="form-group">
                        <label for="password" class="col-sm-3 form-label">Change Password</label>
                        <div class="col-sm-9">
                            <input type="password" name="password" class="form-control form-input" id="mobile" placeholder="">
                            
                            <div class="row pswd-description">Password should be 8 characters</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label for="change_password" class="col-sm-3 form-label">Confirm Password</label>
                        <div class="col-sm-9">
                            <input type="password" name="confirm_password" class="form-control form-input" id="confirm-password" placeholder="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label for="contact_no" class="col-sm-3 form-label">Change Contact no</label>
                        <div class="col-sm-9">
                            <input type="text" name="mobileno" class="form-control form-input" id="mobile" placeholder="">
                        </div>
                    </div>
                </div>
                <div class="join-now col-sm-6">
                    <input type="submit" value="Submit" name="submit" class="join-btn col-sm-12 pull-right lato-regular">
                </div>
            </div>
        </div>
    </div>
</div>
