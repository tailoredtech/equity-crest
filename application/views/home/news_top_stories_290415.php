<div class="container">
    <div class="row">
        <div class="col-sm-5 block-title" >
        <h2>Media / Press Centre</h2>
        </div>
    </div>
</div>
<div class="container">
    <div class="row content-box2">
        <div class="col-sm-12">    
            <div class="row terms-margin">
                <div class="panel-body">
                    <h5 class="lato-bold">Equity Crest featured in The Economic Times</h5>
                    <p>As we work hard on building the platform, we recently got a boost to our morale when The Economic Times covered Equity Crest in their article. <a href="http://epaperbeta.timesofindia.com/Article.aspx?eid=31815&articlexml=Deal-Discovery-Tools-Turn-Angels-Wands-14102014001083" target="_blank">Click Here</a> to read the article</p>
                    <img class="text-center img-responsive center-block" style="margin-right:auto;" src="<?php echo base_url(); ?>assets/images/et.jpg">
                </div>
            </div>
        </div>
    </div>
</div>
