<div class="container">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <h1 class="page-title">Management Team</h1>
    </div>               
  </div>

  <div class="row team_heading_section">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <h2 class="team_heading1">Experienced, imaginative and diverse.</h1>
      <h3 class="team_heading2">We are EquityCrest.</h3> 
    </div>     
  </div>


  <div class="row" id="management_team">

    


     <div class="col-md-6 col-sm-6 col-xs-12 team_member"> 
        <div class="row">
          <div class="col-sm-3 col-xs-3"> <img class="img-responsive" src='<?php echo base_url(); ?>assets/images/Deepak_gupta.jpg' /> </div>
          <div class="col-sm-9 col-xs-9">
            <h3 class="team-member-name">Deepak Gupta</h3>
            <h4 class="team-member-post">Co-Founder & CEO</h4>
            <div class="team-member-share hidden"> <a target="_blank" href="https://www.linkedin.com/pub/deepak-gupta/4/233/166"><img style="border: 1px none; background-color: #04469f; border-radius: 5px;padding:1px;" src="<?php //echo base_url(); ?>assets/images/socia2.png" /></a> <a target="_blank" href="https://twitter.com/DeepakG606"><img style="border: 1px none; background-color: #04469f; border-radius: 5px; padding:1px 2px;" src="<?php //echo base_url(); ?>assets/images/socia4.png" /></a> </div>

            <div class="team-member-details">
              <p>Deepak has about 19 years experience in investments and finance across angel investing, venture capital, fixed income management and financial management. </p>
              <p> Over the past year he has been an active angel investor, after spending 14 years with Intel Corp where his last role was as Investment Director with Intel Capital in India. </p>
              <p>His previous venture investments include Sasken (IPO), Future Software (Acquired by Flextronics), Omnesys (acquired by Thomson Reuters), Career Launcher and Savaari Car Rentals. Deepak holds a B.Tech from IIT Bombay and MBA/MS (Finance) degrees from the United States.</p>
            </div> 
            
          </div>
        </div>
    </div> 

    <div class="col-md-6 col-sm-6 col-xs-12 team_member">      
      <div class="row">
        <div class="col-sm-3 col-xs-3"> <img class="img-responsive" src='<?php echo base_url(); ?>assets/images/AmitWadhwa.jpg' /> </div>
        <div class="col-sm-9 col-xs-9">
          <h3 class="team-member-name">Amit Wadhwa</h3>
          <h4 class="team-member-post">Co-Founder</h4>
          <div class="team-member-share hidden"> <a target="_blank" href="http://in.linkedin.com/pub/amit-wadhwa/5/63b/446"><img style="border: 1px none; background-color: #04469f; border-radius: 5px;padding:1px;" src="<?php echo base_url(); ?>assets/images/socia2.png" /></a> <a target="_blank" href="https://twitter.com/_amitwadhwa"><img style="border: 1px none; background-color: #04469f; border-radius: 5px;padding:1px 2px;" src="<?php echo base_url(); ?>assets/images/socia4.png" /></a> </div>

          <div class="team-member-details">
            <p>Amit has rich experience in Corporate Finance, Strategy, Investor Management, Fund Raising and M&A across broadcast television, motion pictures, internet and consumer products, among others.</p> 
            <p>He has been on both sides of the table; has a wealth of experience in acquiring and raising funds for established as well as new businesses.His diverse experience includes working with top management, founder / promoters of leading family run organizations like Zee / Essel Group and Times Group; and professionally managed conglomerates like The Walt Disney Company.</p> 
            <p>Amit holds a Master of Management Studies degree from Narsee Monjee Institute of Management Studies (NMIMS) in Mumbai.</p>
            

          </div>
        </div>
      </div>
    </div>  

    <div class="col-md-6 col-sm-6 col-xs-12 team_member">    
        <div class="row">
          <div class="col-sm-3 col-xs-3"> <img class="img-responsive" src='<?php echo base_url(); ?>assets/images/AmitBanka.jpg' /> </div>
          <div class="col-sm-9 col-xs-9">
            <h3 class="team-member-name">Amit Banka</h3>
            <h4 class="team-member-post">Founder</h4>
            <div class="team-member-share hidden"> <a target="_blank" href="https://www.linkedin.com/profile/view?id=29877297&authType=NAME_SEARCH&authToken=9Mj8&locale=en_US&trk=tyah2&trkInfo=tarId%3A1416979713524%2Ctas%3Aamit%20banka%20%2Cidx%3A1-1-1"><img style="border: 1px none; background-color: #04469f; border-radius: 5px;padding:1px;" src="<?php echo base_url(); ?>assets/images/socia2.png" /></a> <a target="_blank" href="https://twitter.com/amitbanka75"><img style="border: 1px none; background-color: #04469f; border-radius: 5px;padding:1px 2px;" src="<?php echo base_url(); ?>assets/images/socia4.png" /></a> </div>
            <div class="team-member-details">
              <p>Amit has rich experience in Fund Raising, Fund Management, Private Equity, Venture/ Early stage Investments, Impact Investments, Unlisted investments opportunities and Real Estate.</p>
              <p> He worked as Managing Director of Unilazer Ventures before starting-up Equity Crest. In his previous role as an angel investor, Amit has made investments in companies in Media & Entertainment, FMCG, E-Commerce, Agriculture etc. Prior to Unilazer, he has worked as an internal consultant on Mergers and Acquisitions with UTV for more than seven years. </p>
              <p>Amit has done his Masters in Finance from the Institute of Management Studies. </p>
            </div>
          </div>
        </div>
    </div>  
    

    <!-- div class="col-md-6 col-sm-6 col-xs-12 team_member">
      <div class="row">
        <div class="col-sm-3 col-xs-3"> <img class="img-responsive" src='<?php echo base_url(); ?>assets/images/rahulsinghal.jpg' /> </div>
        <div class="col-sm-9 col-xs-9">
          <h3 class="team-member-name">Rahul Singhal</h3>
          <h4 class="team-member-post">Senior Associate</h4>
          <div class="team-member-share hidden"> <a target="_blank" href="https://www.linkedin.com/in/rahulsinghaliima"><img style="border: 1px none; background-color: #04469f; border-radius: 5px;padding:1px;" src="<?php echo base_url(); ?>assets/images/socia2.png" /></a> <a target="_blank" href="https://twitter.com/Rahul_BaBa"><img style="border: 1px none; background-color: #04469f; border-radius: 5px;padding:1px 2px;" src="<?php echo base_url(); ?>assets/images/socia4.png" /></a> </div>
          <div class="team-member-details">
            <p> Rahul is a Investment Banking professional with vast experience in debt syndication,private equity syndication, transaction advisory, IT and business development. </p>
            <p>Rahul has excellent relationship network across corporate & banking circles and PE & VC funds. Rahul is a Master of Management Studies from Indian Institute of Management, Ahmedabad (IIMA). </p>
          </div>
        </div>
      </div>
    </div -->  

    <div class="col-md-6 col-sm-6 col-xs-12 team_member"> 
        <div class="row">
          <div class="col-sm-3 col-xs-3"> <img class="img-responsive" src='<?php echo base_url(); ?>assets/images/Rohit_Krishna.jpg' /> </div>
          <div class="col-sm-9 col-xs-9">
            <h3 class="team-member-name">Rohit Krishna</h3>
            <h4 class="team-member-post">Senior Associate</h4>
            <div class="team-member-share hidden"> <a target="_blank" href="https://www.linkedin.com/pub/deepak-gupta/4/233/166"><img style="border: 1px none; background-color: #04469f; border-radius: 5px;padding:1px;" src="<?php echo base_url(); ?>assets/images/socia2.png" /></a> <a target="_blank" href="https://twitter.com/DeepakG606"><img style="border: 1px none; background-color: #04469f; border-radius: 5px; padding:1px 2px;" src="<?php echo base_url(); ?>assets/images/socia4.png" /></a> </div>

            <div class="team-member-details">
              <p>Rohit has over four years experience as an investment analyst across different sectors including packaging, auto and auto ancillary, heavy engineering, chemical and real estate. Rohit’s earlier stints include Spark Capital and Moody’s, where he has closely tracked over 25 listed companies in India. </p>
              <p>Rohit is a Chartered Accountant, a Chartered Financial Analyst and has graduated in finance from University of Madras.</p>
            </div> 
            
          </div>
        </div>
    </div>
    
  </div>
</div>