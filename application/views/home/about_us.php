<div class="container">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <h1 class="page-title">About Us</h1>
    </div>               
  </div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">

            <div class="company-accordion">
                <div class="accordion-heading">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseAbout"><h3>About Equity Crest</h3></a>
                </div>
                <div id="collapseAbout" class="accordion-body collapse in">
                  <div class="accordion-inner clearfix">                                        
                      <p>Running a startup is tough work in itself.  Fund-raising is key to success and is at times taken for granted.</p>
                        <p><b>Why not put your best foot forward when it comes to raising money?</b></p>
                        <p><b>Guided by real experts…through an efficient process</b></p>
                        <p></p>
                        <p>Equity Crest is  about helping promising young companies connect with high quality investors in order to fulfil their capital needs with the best possible outcomes. Its also about discovering the diamonds in the rough - which got passed over in the deluge of startups in India.</p>
                        <p>The founders at Equity Crest have deep expertise with 40+ years of  experience ($2B + raised/invested) across multiple equity stages as well as a wide range of sectors.</p>
                        <p>We help promising entrepreneurs  better understand their business possibilities, articulate them crisply to investors and  navigate the fast evolving early stage eco-system in India. Our platform enables efficient discovery and engagement between investors and startups. We enable an efficient process with  documentation appropriate for the seed stage.</p>
                        <p>We add value to investors in the following way:
                        <ol type="a">
                            <li>By  posting only select startups which we believe are fundamentally investible and aligning our incentives for the longer term with investors.</li>
                            <li>Enabling syndication of rounds for startups which pass our high bar.</li>
                        </ol>
                   </div>
                </div>
            </div>

            <div class="company-accordion">
                <div class="accordion-heading">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseVision"><h3>Mission & Philosophy</h3></a>
                </div>
                <div id="collapseVision" class="accordion-body collapse">
                  <div class="accordion-inner clearfix">
                    <p>We aim to be the go to destination for funding for high quality founders and investors.</p>
                    <p>We know credibility is critical for such a platform to succeed, hence protection of both investor, investee is of prime importance to us and we use our processes, deal experience and incentive  alignment to enable this.</p>
                    <p>We only support strong companies, who have good scalable business models, run by committed entrepreneurs. A strong appraisal system ensures the robustness of the companies that we put up on our platform.</p>
                   </div>
                </div>
            </div>

            <div class="company-accordion">
                <div class="accordion-heading">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseScreening"><h3>Startup Screening Process</h3></a>
                </div>
                <div id="collapseScreening" class="accordion-body collapse">
                  <div class="accordion-inner clearfix">
                    <p>We combine a parametric scorecarding systems with our vast deal experience to come to an evaluation of a startup for it to be listed on our platform for fund-raising. The appraisal for start-ups prior to their listing involves a detailed analysis of its business prospects by our team. This involves multiple interactions with the founding team apart from just an analysis of the pitch presentation and the business metrics.</p>
                    <p>Some of the parameters along which the assessment is done are:
                        <ol type="a">
                            <li><b>Team</b> – Apart from assessing parameters like experience and educational background, other parameters like age of the founders, relevance of experience and assessment of multi- functional expertise available within the team needed for the business.</li>
                            <li><b>Product / service idea</b> – This includes an assessment of parameters like how compelling the need is for such an idea and how strong is the value proposition.</li>
                            <li><b>Traction</b> – Includes assessment of the stage of the business and how strong the traction shown by the business is since inception.</li>
                            <li><b>Market</b> – Assessment of the market size and target audience.</li>
                            <li><b>Competition</b> – Evaluating the landscape of competitors, how the start-up compares vis a vis competitors for product / service delivery and its USP.</li>
                        </ol>
                        </p>
                        <p>Although there may be a lot of subjectivity in such assessment, we believe that with the experience on board such assessment helps significantly in the screening of start ups.</p>
                   </div>
                </div>
            </div>
        </div>               
    </div>

</div>
