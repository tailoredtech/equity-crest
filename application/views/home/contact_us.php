<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <h1 class="page-title">Contact Us</h1>
        </div>
    </div>

    <div class="row content-box2">
        <div class="col-sm-9">
            <div class="embed-responsive embed-responsive-16by9">
              <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3769.8736750499183!2d72.865545!3d19.113197000000003!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7c83a25825fd3%3A0x53a35e8fc56e469c!2sAcme+Plaza!5e0!3m2!1sen!2sin!4v1406706217444"></iframe>
            </div>
            <!-- div>
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3769.8736750499183!2d72.865545!3d19.113197000000003!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7c83a25825fd3%3A0x53a35e8fc56e469c!2sAcme+Plaza!5e0!3m2!1sen!2sin!4v1406706217444" width="581" height="340" frameborder="0" style="border:0"></iframe>
            </div -->
        </div>
         <div class="col-sm-3">
            <b>Address</b><br>
                505, ACME Plaza,<br>
                Andheri Kurla Road, Chakala<br>
                Andheri East <br>
                Mumbai 400059, India <br><br>
                <b>Contact Us: </b> +91 98922 11438<br><br>
                Email: info@equitycrest.com<br>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <h1 class="page-title">For Inquiry</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 col-xs-12">
			<form id="inquiry-form">
                    <div class="form-group row">
                       <label for="name" class="col-sm-2 form-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" class="form-control form-input" id="name" placeholder="" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="mobileno" class="col-sm-2 form-label">Email</label>
                        <div class="col-sm-10">
                            <input type="text" name="mobileno" class="form-control form-input" id="mobileno" placeholder="">
                        </div>
                    </div>
               
                    <div class="form-group row">
                        <label for="email" class="col-sm-2 form-label">Subject</label>
                        <div class="col-sm-10">
                            <input type="text" name="email" class="form-control form-input" id="subject" placeholder="">
                        </div>
                    </div>
               
                    <div class="form-group row">
                        <label for="message" class="col-sm-2 form-label">Message</label>
                        <div class="col-sm-10">
                            <textarea name="message"  class="form-control form-input" id="message" style="height:80px;" placeholder=""></textarea>
                        </div>
                    </div>
               
                    <div class="col-sm-2 row">
                        <input type="submit" value="Submit" id="submit" name="submit" class="btn btn-eq-common">
                    </div>
               
			</form>
        </div>
    </div>
</div>
<div style="display: none;">
	<div id="inquiry-popup" class="container pop-content">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<h3 class="form-header top-margin-10">Inquiry</h3>
			</div>
		</div>
		<div class="row">
			<div class="top-margin-10">
				<div class="col-xs-12 col-sm-12">
					<div class="row">
						<div class="col-sm-10">
							<p id="alert_message" class="error" style="">&nbsp;</p>
						</div>
					</div>
					<div class="text-center pull-right">
						<input class="eq-btn" type="button" value="OK" />
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
        $("#inquiry-form").on('submit', (function(e) {
			e.preventDefault();
			$.ajax({
				url: "<?php echo base_url(); ?>user/inquiry",
				type: 'POST',
				data: new FormData(this),
				contentType: false,
				cache: false,
				processData: false
			})
			.done(function(data) {
				if (data == 'success') {
				   	alert("You inquiry has been send. EQ team will respond to your inquiry as soon as possible.");		
				} else {
					alert(data);
				}
			});

		}));	
	});
</script> 		
