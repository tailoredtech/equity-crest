<div class="container">
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>">Home</a></li>
      <li class="active"><a href="#">Portfolio All</a></li>
    </ol>

    <div class="row">
        <?php  

        if($this->session->userdata['role'] != 'channel_partner'){
        $this->load->view("front-end/filter_options"); ?>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 col-main">
        <?php } else {?>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-main">
        <?php } ?>
            <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <h1 class="page-title2">Portfolio</h1>

                </div>

            </div>

                <?php 
                //echo $this->db->last_query(); 
                $max = isset($users) ? count($users) : count($users_funded + $users_raised);
                if ($max) {
                    ?>  
                    <!-- <div class="col-sm-1"></div> -->
                    <div class="col-sm-12">
                    <div class="row">
                        <div class="w">
                           
                            <?php
                            $count = 0;
                            if(isset($users))
                            {
                            foreach ($users as $user) { ?>
                                <?php //if(DayDifference(date('Y-m-d'), $user['validity_period']) > 0 || $user['status'] == 'semi_active'){ ?>
                                
                                    <?php $data['user'] = $user;
                                    if($this->session->userdata('role') == 'channel_partner')
                                        $data['colsize'] = 3;
                                    else
                                        $data['colsize'] = 4;
                                        
                                    	$this->load->view('front-end/grid-single-item', $data);
										$count++;
										
                                    ?>
                                    
                                
                                <?php //} ?>
                            <?php } 
                            }
                            else
                            {
                                ?>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                    <h1 class="page-title2">Raising</h1>

                                </div>
                                <?php
                                foreach ($users_raised as $raised_user) {
                                    $data['user'] = $raised_user;
                                    if($this->session->userdata('role') == 'channel_partner')
                                        $data['colsize'] = 3;
                                    else
                                        $data['colsize'] = 4;
                                        
                                        $this->load->view('front-end/grid-single-item', $data);
                                        $count++;
                                 } 
                                 ?>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                    <h1 class="page-title2">Funded</h1>

                                </div>
                                <?php
                                foreach ($users_funded as $funded_user) {
                                    $data['user'] = $funded_user;
                                    if($this->session->userdata('role') == 'channel_partner')
                                        $data['colsize'] = 3;
                                    else
                                        $data['colsize'] = 4;
                                        
                                        $this->load->view('front-end/grid-single-item', $data);
                                        $count++;
                                 } 
                                 
                            }
                        if($count==0){ ?>
                         <div class="col-sm-12">    
                            <div class="row terms-margin">
                                <h4 class="text-center">There are No Opportunities available as per your search criteria</h4>
                            </div>
                        </div>
                <?php } ?>
                           
                        </div>
                    </div>
                    <?php } else { ?>
                    <div class="row content-box">
                        <div class="col-sm-12">    
                            <div class="row terms-margin">
                                <h4 class="text-center">There are No Opportunities available as per your search criteria</h4>
                            </div>
                        </div>
                    </div>                  
                <?php }  ?>
</div>


    </div> <!-- ./row -->
</div> <!-- ./container -->

