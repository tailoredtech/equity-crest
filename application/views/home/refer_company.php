<div class="main">
    <div class="container form-contain">
        <div class="row">
            <h4 class="form-header col-lg-12 col-md-12 col-sm-12 col-xs-12 lato-regular">Refer a Company</h4>
        </div>
    </div>
    <div class="container">
        <div class="row content-box">
            <div class="container   col-md-7 col-sm-7 col-xs-8  col-md-offset-2 col-xs-offset-2 ">
                <div id="name-fields">
                    <div class="row">
                        <div class="form-group">
                            <label for="type" class="col-sm-3 form-label">Refferal type</label>
                            <div class="col-sm-9">
                                <div class="select-style">
                                    <select class="form-control form-input country" name="type"  placeholder="">
                                        <option value="">Investor</option>
                                        <option value="">Start up</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="founder_name" class="col-sm-3 form-label">Founder Name</label>
                            <div class="col-sm-9">
                                <input type="text" name="founder_name" class="form-control form-input"  placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="company_name" class="col-sm-3 form-label">Confirm Password</label>
                            <div class="col-sm-9">
                                <input type="text" name="company_name" class="form-control form-input" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="email-id" class="col-sm-3 form-label">Email Id</label>
                            <div class="col-sm-9">
                                <input type="text" name="email" class="form-control form-input" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="contact_no" class="col-sm-3 form-label">Mobile no</label>
                            <div class="col-sm-9">
                                <input type="text" name="mobileno" class="form-control form-input" id="mobile" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                                    <div class="col-sm-12 col-sm-offset-3">
                                            <input type="checkbox" name="agree" class="pull-left icheckbox"> 
                                            <p class="pull-left form-agree col-sm-11 lato-regular grey-font">
                                                    I give my consent to Equity Crest directly contacting my reffered Investor / Company.
                                            </p>
                                    </div>
                    </div>
                    <div class="row">
                        <div class="join-now col-sm-12 text-center">
                            <input type="submit" value="Submit" name="submit" class="btn-green btn-padding btn-space lato-regular">
                            <input type="button" value="Cancel" name="cancel" class="btn-gray btn-padding btn-space lato-regular">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
