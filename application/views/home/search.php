<div class="container">
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url();?>home/portfolio_all">Portfolio</a></li>
      <li><a href="#">Search</a></li>
      <li class="active"><?php echo isset($searchOptions['name'])?$searchOptions['name']:""; ?></li>
    </ol>


    <div class="row">
        <?php  $this->load->view("front-end/filter_options"); ?>

        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 col-main">

            <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <h1 class="page-title2"><?php echo $total_result; ?> result<?php echo $total_result>1?"s":""; ?> found</h1>

                </div>

            </div>

                <?php 
               $max = count($users);
                if ($max) {
                    ?>  
                    <div class="row">
                        <div class="w">
                           
                            <?php
                            $count = 0;
                            foreach ($users as $user) { ?>
                                <?php //if(DayDifference(date('Y-m-d'), $user['validity_period']) > 0 || $user['status'] == 'semi_active'){ ?>
                                
                                    <?php $data['user'] = $user;
                                        $data['colsize'] = 4;
                                        $this->load->view('front-end/grid-single-item', $data);
                                        $count++; 
                                    ?>
                                    
                                
                                <?php //} ?>
                            <?php } 
                        if($count==0){ ?>
                         <div class="col-sm-12">    
                            <div class="row terms-margin">
                                <h4 class="text-center">There are No Opportunities available as per your search criteria</h4>
                            </div>
                        </div>
                <?php } ?>
                            
                        </div>
                    </div>
                    <?php } else { ?>
                    <div class="row content-box">
                        <div class="col-sm-12">    
                            <div class="row terms-margin">
                                <h4 class="text-center">There are No Opportunities available as per your search criteria</h4>
                            </div>
                        </div>
                    </div>                  
                <?php }  ?>


        </div>

    </div> <!-- ./row -->
</div> <!-- ./container -->

