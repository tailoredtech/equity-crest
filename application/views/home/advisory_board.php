<div class="container">
    <div class="row">
        <div class="col-sm-5 block-title" >
        <h2>Advisory Board</h2>
        </div>
    </div>
</div>
<div class="container">
    
    <div class="row content-box2">

  
        <div class="col-sm-12">
     
                <p>The Equity Crest Advisory Board are a team of reputed, knowledgeable, movers & shakers from across industries. These individuals have excelled in their respected fields, and at present/in the past have been responsible for running multi-million dollar businesses.</p>
                <p>Our Advisory Board will work with our investment team to evaluate businesses that we partner with. The Board is therefore responsible for vetting investments that we make through the platform.</p>
                <p>Our Advisory Board will assist entrepreneurs and companies make their business investment-ready. When businesses approach us for investment, but don’t have a clear road-map for long term growth, or are not very differentiated in their offering, our Advisory Board can give them valuable inputs on how to build a sustainable and successful business. Our Advisory Board will work with the Equity Crest investment team, and guide companies to make their business investor friendly for short, as well as long term.</p>

         The following eminent personalities are a part of the Equity Crest Advisory Board<br />
         
            <hr>
            <div class="row">
                <div class="col-sm-3">
                    <img class="img-thumbnail" src='<?php echo base_url(); ?>assets/images/vijay-talreja.jpg' />
                </div>
                
                <div class="col-sm-9">
                 <div class="row team-member-name">Vijay Talreja<div class="team-member-share">
                        
                        <a target="_blank" href="https://www.linkedin.com/in/vijaytalreja"><img style="border: 1px none; background-color: #04469f; border-radius: 5px;padding:1px;" src="<?php echo base_url(); ?>assets/images/socia2.png" /></a>
                        <a target="_blank" href="https://twitter.com/vjtrocks"><img style="border: 1px none; background-color: #04469f; border-radius: 5px;padding:1px 2px;" src="<?php echo base_url(); ?>assets/images/socia4.png" /></a>
                    </div></div>
                    <div class="row"><b>Director at Adapty Inc</b></div><br />
                    <div class="row"></div>
                    <div class="row">
                        <p>Vijay has close to two decades of Technology, Global Delivery and Business Operations experience with IT MNCs and Fortune 500 companies across geographies. He has also served some of the large US based financial services companies and retailers. He is presently the Co-Founder and Director at Adapty Inc, a firm specializing in digital commerce platforms that helps customers adapt to the rapidly changing business environment. He is also an active business advisor and Angel Investor to retail and analytics startups .
                        </p></div>
                    
                </div>
                
            </div>
                               
        </div>
    </div>
</div>
                    
