<!--
<div class="container">
  <div class="row">
    <div class="col-sm-2 block-title">
      <h1 class="page-title">Investors</h1>
    </div>
  </div>
 
</div>
-->

<div class="container">
    <?php $max = count($investors);
        if ($max) {
    ?>
	<div class="row remove_margin">	
    	 
    	<?php
	        $count = 0;
	       
	        foreach ($investors as $user) {
/*
		         echo "<pre>";
		         print_r($user);
		         echo "</pre>";
*/
		        $new_investor_type = $user['investor_type'];
		        		        
		       	if($count == 0)
		       	{
			       	$old_investor_type = $new_investor_type;
		       	}
		       	
		    	if($new_investor_type != $old_investor_type || $count == 0) {
		        	
		        ?>
		        	<div class="row remove_margin">
					    <div class="col-sm-12 col-xs-12">
					      <h2 class="section-title"><?= $new_investor_type;?></h2>
					    </div>
					</div>
		        <?php
			        $old_investor_type = $new_investor_type;	
		        }    

		        $data['user'] = $user;
				$this->load->view('front-end/investor_single_box', $data);
	          
				$count++; 
	       	
	        }
        ?>        
    
	</div>
    <?php 


} else { ?>
	<div class="row content-box">
		<div class="col-sm-12">    
			<div class="row terms-margin">
				<h4 class="text-center">Investor verification and update in progress.</h4>
			</div>
		</div>
	</div>	
    <?php } ?>
</div>
