<div class="container">
    <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <h1 class="page-title">Partners</h1>
    </div>               
  </div>

    <div class="row">
        <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0 text-center channel-partner-content">
           <h2 class="channel-partner-heading">“We believe in PARTNERSHIP”</h2>
           <p>At Equity Crest, we believe that the startup eco-system will progress not just through the efforts of a few but through active partnerships with the universe of stakeholders.</p>
           
        </div>
    </div>
           
	<div class="row">
   		<div class="col-xs-12">
	      <!-- Creative Commons Licence (by-nc-nd). See worditout.com for details -->
	      <div style='width:auto;height:auto;'><!-- You may use this wrapping div to restrict the height or width -->
	        <script type='text/javascript' charset='utf-8'  src='http://worditout.com/word-cloud/912337/private/5f47a1aebcd620e4a2f6215bd41d7067/embed.js'></script>
	        <noscript><p style='text-align:center;font-size:xx-small;overflow:auto;height:100%;'><a href='http://worditout.com/word-cloud/912337/private/5f47a1aebcd620e4a2f6215bd41d7067' title='Click to go to this word&nbsp;cloud on WordItOut.com'>&quot;Channel Partner Wordcloud&quot;</a><br />Click on the link above to see this word&nbsp;cloud at <a href='http://worditout.com' title='Transform your text into word&nbsp;clouds!'>WordItOut</a>. You may also view it on this website if you enable JavaScript (see your web browser settings).</p></noscript>
	      </div>

    	</div>
  	</div>
    <br><br>       
	<div class="row">
        <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0 text-center channel-partner-content">
           <p>
	           If you believe you have something to offer to startups which can leverage our platform or accelerate their journey, we are happy to listen.
	        </p>
           <p><a href="mailto:info@equitycrest.com"><button class="btn btn-eq-green">Please click here to contact us</button></a></p>
            <!--<p>info@equitycrest.com</p>-->
        </div>
    </div>
</div>