<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <h1 class="page-title">Privacy Policy</h1>
        </div>
    </div>
</div>
<div class="container">
    <div class="row content-box2">
        <div class="col-md-12">
            <div class="row terms-margin">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
               
                
                <b>EQ Advisory Pvt. Ltd.</b><br><br>
                <b>Welcome</b>
                <p>This Privacy Policy pertains to the website www.equitycrest.com (hereinafter referred to as “the Website” or “this Website”).</p>
                <p>EQ Advisory Private Limited respects your privacy on the Internet. This Website collects certain types of information about our contacts and strictly maintains the privacy of the information so collected. We also maintain a certain transparency on what we do with it and how to correct or change your information. This Privacy Policy is designed to inform you about our practices regarding collection, use, and disclosure of information that you may provide via this Website. Please be sure to read this entire Privacy Policy before using or submitting information to this Website. Use of this Website constitutes your consent to our Privacy Policy.</p>
                <br><b>Personal Information and its Usage</b><br>
                <p>Like many other websites, this Website collects information from its visitors/customers by permitting you to communicate directly with us via e-mail and feedback / contact us forms. Some of the information that you submit may be personally identifiable information (information that can be uniquely identified with you, such as your full name, address, e-mail address, phone number, and so on). Certain information collected from your end may be mandatory whereas certain information may be optional.</p>
                <p>We collect personal information for following purposes:</p>
                <ul>                
                    <li>to provide information about our products and services;</li>
                    <li>to seek your feedback or to contact you in relation to the products and services offered on the Website;</li>
                    <li>to process orders or applications submitted by you;</li>
                    <li>to administer or otherwise carry out our obligations in relation to any agreement you have with us;</li>
                    <li>to anticipate and resolve problems with any goods or services supplied/rendered to you;</li>
                    <li>to create products or services that may meet your needs;</li>
                    <li>to process and respond to requests, improve our operations, and communicate with visitors about our products, services and businesses.</li>
                    <li>to allow you to subscribe to our newsletter.</li>
                    <p>This Website may disclose your personally identifiable information to other Website affiliates that agree to treat it in accordance with this Privacy Policy. In addition, we may disclose your personally identifiable information to third parties, located in India and/or any other country, but only for following purposes:</p>
                    <li>to contractors we use to support our business (e.g. fulfillment services, technical support, delivery services, financial institutions, etc), in which case we will require such third parties to agree to treat it in accordance with this Privacy Policy;</li>
                    <li>in connection with the sale, assignment, or other transfer of the business of this Website to which the information relates, in which case we will require any such buyer, investor or user to agree to treat it in accordance with this Privacy Policy; or</li>
                    <li>where required by applicable laws, court orders, or government regulations.</li>
                </ul>    

                    <p><b>Information and its Usage</b></p>                    
                    <p><b>Disclosure</b><br>As you navigate through this Website, certain information can be passively collected (collected without your actively providing the information) using various technologies and means, such as navigational data collection.</p>
                    <p>We may send cookies to your computer. Cookies are strings of information that are sent out by a website and saved in your computer. We may use session specific cookies to automatically collect data about website usage when you visit this Website, including your IP address, portions of the Website that you visit and the information or other services you obtain or input (including domain name, Internet service provider, protocol, browser type, operating system etc.). This information is collected primarily for purposes of administering the Website, compiling demographic information and monitoring usage and performance of the Website. If you turn off the cookie function you may not receive all the information on the Website. We may use your IP address to diagnose problems with our server, report aggregate information, determine the fastest route for your computer to use in connecting to our Website, and administer and improve this Website. EQ Advisory believes that by collecting certain information of its visitors/customers it is able to use your information to improve the content of this Website, to customize the Website to user’s preferences, to communicate information to you, for our marketing and research purposes, and for providing you with the latest information for direct marketing purposes where we think you may be interested in the products and services of EQ Advisory.</p>
                    <p>We will make full use of all information acquired through this Website that is not in personally identifiable form.</p>
                    <br><b>Links to Third Party Websites</b><br>
                    <p>Other websites that have links on our Website may collect personally identifiable information about you. The data protection practices of those websites linked to this Website are not covered by this Privacy Policy. You are advised to check the data protection policies of these third party websites yourself before using those sites.</p>
                    <br><b>Security</b><br>
                    <p>EQ Advisory is committed to protecting your privacy and utilizing technology that gives you the most powerful, safe, online experience that you can get anywhere else. EQ Advisory shall ensure to safeguard the security and confidentiality of any information you share with us. Any personally identifiable information of the visitors/customers obtained by us shall not be used or shared other than for the purposes to which the visitors/customers consent. However security and confidentiality of information cannot be guaranteed to be 100% secure. Hence despite our utmost efforts to protect your personal information, EQ Advisory cannot warrant the security of any information you transmit to us through our online services. Such transmission of your personal information is done at your own risk.</p>
                    <br><b>Changes to Privacy Policy</b><br>
                    <p>EQ Advisory reserves the right to change this Privacy Policy at any time. Such changes will become effective and binding after their posting on this section of the Website. The visitor/customer is responsible to review the Privacy Policy regularly and be aware of the changes made. By continuing to use our products and services and this Website after any posted revision, the visitor/customer accepts to abide by it.</p>
                    <p>This Privacy Policy is not intended to and does not create any contractual or other legal rights in or on behalf of any party.</p>
                    <br><b>Contact Us</b><br>
                    <p>If you have any questions, comments, or concerns about this Privacy Policy or the information practices of this Website, please write to us at info@equitycrest.com</p>
               
                </div>
            </div>
        </div>
    </div>
</div>
