<div class="container">

  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <h1 class="section-title">Media / Press Centre</h1>
    </div>
  </div>

  <div class="media_news_listing">
  
      <div class="row media_news_item">
        <div  class="col-sm-12 col-xs-12">
          <h3>The Economic Times</h3>
        </div>
        <div class="col-sm-8 col-xs-8 col-mobile">          
          <p class="first_para">Why startups are now swearing by crowdfunding platforms like Grex and Equity Crest to raise money</p>
          <p><a href="http://economictimes.indiatimes.com/small-biz/startups/why-startups-are-now-swearing-by-crowdfunding-platforms-like-grex-and-equity-crest-to-raise-money/articleshow/50569566.cms" target="_blank">Click Here</a> to read the article</p>
        </div>
        <div class="col-sm-4 col-xs-4 col-mobile">
          <div class="in-media-img">
            <img class="img-responsive" src="<?php echo base_url(); ?>assets/images/image.png" alt="The Economic Times">
          </div>
        </div>
      </div>  <!-- /.media_news_item -->

      <div class="row media_news_item">
        <div  class="col-sm-12 col-xs-12">
          <h3>Drivojoy funding</h3>
        </div>
        <div class="col-sm-8 col-xs-8 col-mobile">          
          <p> The company raised Rs.4 Crores from IAN, Tessellate Ventures, Equity Crest and Traxcn Labs. <a href="http://yourstory.com/2016/03/drivojoy-funding" target="_blank">Click Here</a> to read the article</p>          
        </div>
        <div class="col-sm-4 col-xs-4 col-mobile">
          <div class="in-media-img"> 
            <img class="img-responsive" src="<?php echo base_url(); ?>assets/images/YourStory-Drivojoy.jpg" alt="Drivojoy Team" style="height:auto !important;">
          </div>
        </div>
      </div>  <!-- /.media_news_item --> 

      <div class="row media_news_item">
        <div  class="col-sm-12 col-xs-12">
          <h3>The Economic Times</h3>
        </div>
        <div class="col-sm-8 col-xs-8 col-mobile">          
          <p class="first_para">Amit Banka, founder, Equity Crest shares his expert guidance in ET column ‘Guru Gyaan’.
          Amit talks about starting up in the showbiz sector. Excerpts:</p>
          <p>Amit Banka is the founder of Equity Crest, a curated funding platform for a long-term partnership with early-stage ventures. Prior to Equity Crest, he spearheaded operations as managing director of Unilazer Ventures. </p>
          <p class="questions"><strong>Q.Is entertainment a good sector to start a company?</strong></p>
          <p>AB: Yes, it is a recession-proof sector, largely. India with its diversity in languages across geographies, provides huge opportunity for various interesting business models in entertainment, digital content and associated distribution platforms. </p>
          <p class="questions"><strong>Q. What are the gaps in the sector that can be filled by start-ups?</strong></p>
          <p>AB: I foresee a business model that is tech-enabled which can make the industry collaborate on content, distribution, delivery platforms and global syndication of content. In addition, there is scope for alternative and new-age entertainment platforms which challenges traditional theories of content and distribution, catering to the audience looking for an augmented experience.</p>          
        </div>
        <div class="col-sm-4 col-xs-4 col-mobile">
          <div class="in-media-img">
            <img class="img-responsive" src="<?php echo base_url(); ?>assets/images/in_media1.jpg" alt="The Economic Times">
          </div>
        </div>
      </div>  <!-- /.media_news_item -->

      <div class="row media_news_item">
        <div  class="col-sm-12 col-xs-12">
          <h3>Equity Crest featured on Bloomberg TV</h3>
        </div>
        <div class="col-sm-8 col-xs-8 col-mobile">          
          <p>Bloomberg TV's show Wealth Manager recently discussed the <strong>"Risks & Rewards of Angel Investing"</strong>. Hosted by Avni Raja, the show spoke at length to Equity Crest, Founder, Amit Banka on his views of investing in startups and becoming an angel investor.</p>          
        </div>
        <div class="col-sm-4 col-xs-4 col-mobile">
          <div class="in-media-img">             
            <iframe width="100%" height="200" src="https://www.youtube.com/embed/_JlLjKKzmig?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
          </div>
        </div>
      </div>  <!-- /.media_news_item -->

      <div class="row media_news_item">
        <div  class="col-sm-12 col-xs-12">
          <h3>Equity Crest featured in The Economic Times</h3>
        </div>
        <div class="col-sm-8 col-xs-8 col-mobile">          
          <p>As we work hard on building the platform, we recently got a boost to our morale when The Economic Times covered Equity Crest in their article. <a href="http://epaperbeta.timesofindia.com/Article.aspx?eid=31815&articlexml=Deal-Discovery-Tools-Turn-Angels-Wands-14102014001083" target="_blank">Click Here</a> to read the article</p>          
        </div>
        <div class="col-sm-4 col-xs-4 col-mobile">
          <div class="in-media-img"> 
            <img class="img-responsive" src="<?php echo base_url(); ?>assets/images/et.jpg" alt="The Economic Times">
          </div>
        </div>
      </div>  <!-- /.media_news_item --> 

      

  </div> <!-- ./media_news_listing -->

</div>  <!-- /.container -->
