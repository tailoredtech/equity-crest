<div class="container">
    <div class="row">
        <div class="col-sm-3 block-title" >
        <h2>About Us</h2>
        </div>
    </div>
</div>
<div class="container">
    <div class="row content-box2">
        <div class="col-md-12">    
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                    <!--Slider Accordion Starts here-->
                  <div id="accordion-about">
                    <!-- Start panel -->
                    <h5 class="first accordion-about accordion-close-about" id="body-section1">about equity crest<span></span></h5>
                    <div style="display:none;" class="containers">
                      <div class="content-about">
                        <p>Equity Crest is the first club for entrepreneurship in India. We call it a club, because our objective is to make a success of everyone joining the club. We ‘Partner Businesses’ and build value for all our stakeholders -- be it Investee, Investors and Partners.</p>
                        <ol>
                          <li>Investee/ Company/ Businesses – can raise capital through Equity Crest</li>
                          <li>Investors – Can invest in the companies/ businesses listed on Equity Crest, by partnering with us</li>
                          <li>Partners – Partners of Equity Crest are Mentors, Advisors, anyone whom an Entrepreneur requires to make his business successful</li>
                        </ol>
                        <p>The entry into the club happens through the first deal you make through us, and this deal acts as your lifetime membership guarantee. Equity Crest will help you raise funds from seed stage, different Series round of funding, and also help you ultimately exit.</p>
                        <p>The Equity Crest platform is synonymous with credibility, accountability, transparency, quality and top class expertise. We are a group of professionals who between us, have a great deal of experience in areas of business operations, fund raising, fund management, private equity, public markets, mergers and acquisitions.</p>
                        <p>Some of the sectors covered by us are Media and Entertainment, FMCG, E-Commerce, Food & Agriculture, Impact investing and Financial Services to name a few. With our wide breadth of experience, we can help businesses at their early stages not just raise funds but also prepare themselves to become leading business ventures and generate value in the long term.</p>
                      </div>
                    </div>
                    <!-- end panel -->
                    <!-- Start pane2 -->
                    <h5 class="accordion-about accordion-close-about" id="body-section2">vision & philosophy<span></span></h5>
                    <div style="display:none;" class="containers">
                      <div class="content-about">
                        <p>We want to build a world class ecosystem that promotes growth of new and young businesses, so as to create long term value for all our stakeholders -- entrepreneur, investors, partners and the Equity Crest team.</p>
                        <p>We want to take entrepreneurship to masses in India - hence a collaborative approach is needed to ensure all stakeholders participate fully in this drive of entrepreneurship.</p>
                        <p>We know credibility is critical for such a platform to succeed, hence protection of both investor, investee is of prime importance to us, and through our processes and techniques we will ensure it happens well.</p>
                        <p>We only support strong companies, who have good scalable business models, run by committed entrepreneurs. A strong appraisal system, with strict guidelines, regular checks and balances, will ensure that everything we do is always in the open, and there is no ambiguity.</p>
                        <p>Our advisory board comprises of people with domain expertise, thought leadership, and are among the strongest business professionals in their respective areas.</p>
                      </div>
                    </div>
                    <!-- end pane2 -->
                    <!-- Start pane3 -->
                    <h5 class="last accordion-about accordion-close-about" id="body-section3">objective<span></span></h5>
                    <div style="display:none;" class="containers">
                      <div class="content-about">
                        <ul>
                          <li>To drive entrepreneurship and enable creation of a positive social and economic impact</li>
                          <li>To create businesses who will be market leaders tomorrow, by identifying them today</li>
                          <li>To create value for Investors by helping them make prudent investment decisions</li>
                          <li>To explore new avenues that are opportunities for sound investment</li>
                        </ul>
                      </div>
                    </div>
                    <!-- end pane3 -->
                  </div>
                    <!--Slider Accordion ends here-->

                </div>
            </div>
        </div>
    </div>
</div>
<!-- Accordion Slider scripts-->
<?= js('jquery_003.js') ?>
<?= js('highlight.js') ?>
<script type="text/javascript">
            $(document).ready(function() {

                //syntax highlighter
                hljs.tabReplace = '    ';
                hljs.initHighlightingOnLoad();

                //accordion
                $('h5.accordion-about').accordion({
                    defaultOpen: 'section1',
                    cookieName: 'accordion_nav',
                    speed: 'slow',
                    animateOpen: function (elem, opts) { //replace the standard slideUp with custom function
                        elem.next().slideFadeToggle(opts.speed);
                    },
                    animateClose: function (elem, opts) { //replace the standard slideDown with custom function
                        elem.next().slideFadeToggle(opts.speed);
                    }
                });
    
                $('h5.accordion2').accordion({
                    defaultOpen: 'sample-1',
                    cookieName: 'accordion2_nav',
                    speed: 'slow',
                    cssClose: 'accordion2-close-about', //class you want to assign to a closed accordion header
                    cssOpen: 'accordion2-open-about',
                    
                });

                //custom animation for open/close
                $.fn.slideFadeToggle = function(speed, easing, callback) {
                    return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
                };

            });
        </script>
