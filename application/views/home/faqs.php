<?php 
	
	$faqsForInvesteeQuestions[0] = "What is seed capital and why do startups need seed capital?";
	$faqsForInvesteeAnswers[0] = "<p>Seed Capital is the initial capital used to start a business. The amount of money is usually relatively small, because the business is still in the idea or conceptual stage. Startups are generally at a pre-revenue stage and need seed capital for research & development, to cover initial operating expenses until a product or service can start generating revenue, and to attract the attention of venture capitalists.</p>";

	$faqsForInvesteeQuestions[1] = "What kind of capital should a startup raise and where should it be raised from? ";
	$faqsForInvesteeAnswers[1] = "<p>Mainly two types of capital are available for businesses at any stage: Debt and Equity.
Debt capital, is the capital where the investor provides you capital at a certain interest rate for a fixed period of time, and generally asks you for collaterals to secure his investments.</p> 
<p>In comparison, while providing Equity, the investor takes a certain percentage of ownership in the company -- which means that if the business is successful both participants make profit (either through appreciation in share price or dividends, or both). And if the business fails, both of them lose their respective investments.</p> 
<p>Startups are unsure about the future and financial projections -- however smartly made -- are not certain. At an early stage most startups face lot of risk, do not have predictable cashflows and there are significant chances of failure, thus it is always good to share the risk at initial stage. In Equity capital, the promoter shares the risk with the investor, and it is in the investor’s interest to see that the firm grows -- thus he is incented to add  value to the startup.</p>";

	$faqsForInvesteeQuestions[2] = "From whom should the startup raise capital?";
	$faqsForInvesteeAnswers[2] = "<p>There are many ways a startup can raise seed capital. The significant ones are:</p>
								<ul>
								  <li>Through family and friends </li>
								  <li>Through venture capital fund -- but not many VC funds provide seed capital </li>
								  <li>Through angel investors,and seed capital funds</li>
								</ul>";

	$faqsForInvesteeQuestions[3] = "Does Equity Crest advise registered investors to invest in a startup?";
	$faqsForInvesteeAnswers[3] = "<p>Equity Crest is a platform which provides a credible channel to investors to invest in startups, where they get significant  information about the startup and access to the founders. Investors are free to interact with the startup, to understand their business idea and learn more about them and ultimately decide whether or not to invest. There is no obligation for an investor to registered on Equity Crest to invest in one or more startups registered on the platform.</p>";

	$faqsForInvesteeQuestions[4] = "What is Crowd-funding? ";
	$faqsForInvesteeAnswers[4] = "<p>Crowd-funding is solicitation of funds (in small amounts) from multiple investors through a web-based platform or social networking site for a specific project, business venture or social cause.</p>
<p>Crowd-funding can be divided into four categories: donation crowd-funding, reward crowd-funding, peer-to-peer lending and equity crowd-funding.</p>
<p>In equity based crowd-funding, in consideration of funds solicited from investors, equity shares of the company are issued. It refers to fund-raising by a business, particularly early-stage funding, through offer of equity interest in the business to these investors. Businesses seeking to raise capital through this mode typically advertise online through a crowd-funding platform, which serves as an intermediary between them and the investors.</p>";

	$faqsForInvesteeQuestions[5] = "How is a crowd-funding platform more beneficial as compared to an offline mode of fund raise? ";
	$faqsForInvesteeAnswers[5] = "<p>Crowd-funding platform has a lot of benefits:</p>
									<ul>
									  <li>It allows a startup to raise funds at a lower cost of capital without undergoing rigorous procedures</li>
									  <li>Crowd-funding provides a fresh mode of financing for startups in the form of investor money. And by attracting investors, it provides them a new investment avenue for portfolio diversification</li>
									  <li>Crowd-funding gives startups access to different investors having interests in different sectors at the same time</li>
									  <li>As many small investors come together to fund a project and diversify risk, a crowd-funding platform provides a better chance to the startup to get funded.</li>
									</ul>";

	$faqsForInvesteeQuestions[6] = "Is Equity Crest a crowd-funding platform? ";
	$faqsForInvesteeAnswers[6] = "<p>Equity Crest is a  curated platform combined with offline expertise and network that helps the entrepreneur raise funds. Although the platform resembles a crowd-funding platform – it is actually a lot more than that.</p>";

	$faqsForInvesteeQuestions[7] = "How can Equity Crest help me raise the required capital? ";
	$faqsForInvesteeAnswers[7] = "<p>Equity Crest has a significant number of investors across VCs, angels and family offices actively looking for investment opportunities. The process for a startup to get funded through this platform is as follows:</p>
			<ul>
			  <li>Startup gets registered with Equity Crest</li> 
			  <li>Startup has to upload its business plan, financials and other required documents</li> 
			  <li>A team from Equity Crest will screen the information, engage with the startupand evaluate their proposal. If the decision is to onboard the startup, then there is a deeper engagement between the Equity Crest team and the Startup post which it getsthe funding proposal gets listed on the platform</li>
			  <li>Once listed on Equity Crest, the proposal can be seen by registered investors. Based on their interest levels, investors  will get in touch with the startupfor further diligence.</li>
			  <li>The investor/s will then negotiate the terms and conditions of his investment, post which Equitycrest can assist parties to engage with appropriate counsel/diligence vendors and follow the due process including drafting of SHAs and take the deal to closure.  Equity Crest has standard term sheets and documents available to facilitate speedy deal closure with balanced terms for investors and founders.</li>
			</ul>";

	$faqsForInvesteeQuestions[8] = "What kind of investors can Startups access with Equity Crest?";
	$faqsForInvesteeAnswers[8] = "<p>We have a variety of investors registered with Equity Crest, including</p>
									<ul>
									  <li>Angel investors, who have expertise of specific sectors and want to invest in quality startups in such sectors</li>
									  <li>Many of the top venture capital funds</li>
									  <li>The major Angel Networks</li>
									  <li>Family Offices</li>
									  <li>Seed investment funds</li>
									</ul>";
	$faqsForInvesteeQuestions[9] = "Is Equity Crest regulated by any Government body?";
	$faqsForInvesteeAnswers[9] = "No. Currently, Equity Crest is not regulated by any Government body.";
	
	
	$faqsForInvestorQuestions[0] = "What is investing in startups about? ";
	$faqsForInvestorAnswers[0] = " <p>Investing in start-ups generally means to invest in businesses that are being built on new ideas, or those who are executing an existing idea in a different way. These business have tremendous potential to grow at high rate and can grow manifold over a short period of time. The investor invests his money in such business for a portion of equity. If the business succeeds, the investor's equity value tends toincrease substantially. </p>";
	
	$faqsForInvestorQuestions[1] = "Are investments in startups profitable?";
	$faqsForInvestorAnswers[1] = "<p>Startups are generally business that are executing new ideas, or executing already existing ideas in a different way. Seed stage investments are particularly risky and investors should be aware that they could lose all or substantially all of their money. On the other hand, more recently we have seen double digit and triple digit multiple returns on the money invested in the most successful startups in India..</p>";
	
	$faqsForInvestorQuestions[2] = "What are other benefits of investing in startups?";
	$faqsForInvestorAnswers[2] = " <p>Apart from potential monetary profits if the startup succeeds, investors can enjoy a few additional benefits by investing in startups. </p>
                                        <p>The investor can be part of the next big invention or next big thing. Investing in a startup, allows the investor to participate in the growth of a company from the beginning. He can assist them with his valuable inputs at the early stage, which can be instrumental in their growth story. By investing in startups, the investor is participating in building the entrepreneurial culture in the society, and thereby helping build the nation.</p>";

	$faqsForInvestorQuestions[3] = "How do I use Equity Crest to invest in startups? ";
	$faqsForInvestorAnswers[3] = "<p>Equity Crest follows a rigorous system of evaluating businesses that eventually get listed on its platform. We have on our platform startups with good growth potential from different sectors. We provide our registered investors with detailed information required to learn more about the startups, interact with them, and know what other investors think of them.. Once they decide to invest, Equity Crest assists in the necessary closing formalities and in effective follow-ups subsequent to such investment. </p>";

	$faqsForInvestorQuestions[4] = "Does Equity Crest review investee companies?";
	$faqsForInvestorAnswers[4] = "<p>We encourage investors to make their own assessment of each investment opportunity. The team at Equity Crest does review each opportunity and believes that each company listed on Equity Crest is fundamentally investible. Investors can also derive comfort from the incentive alignment between them and Equity Crest principals.</p>";

	$faqsForInvestorQuestions[5] = "Will Equity Crest advise me on which startups to invest in?";
	$faqsForInvestorAnswers[5] = "<p>The Equity Crest platform provides a different channel to investors to invest in startups, where they can getdetailed  information to enable their investment decision. To the extent that investors have sector preferences, we are happy to direct startups in specific sectors to them. Ultimately,  investors need to understand the riskiness of seed stage investments and make their own investment decisions.</p>";

	$faqsForInvestorQuestions[6] = "Is there a minimum or a maximum I can invest in a startup registered with Equity Crest?";
	$faqsForInvestorAnswers[6] = "<p>As an individual investor it is possible to do seed stage investments with just a few lakhs invested per deal. We would generally recommend investors diversify their portfolio over 8-10 deals over time. Also if a company does begin to perform, it may worth putting in some money in the subsequent round to capitalize on improving success probability.</p>";

	$faqsForInvestorQuestions[7] = "Is there a minimum or a maximum amount that a startup can raise through Equity Crest?";
	$faqsForInvestorAnswers[7] = "<p>There is no minimum or maximum investment limit. However, our focus is primarily on early-stage businesses, and so most campaigns on Equity Crest are raising amounts of money appropriate to business that are in the stage of development.</p>";

	$faqsForInvestorQuestions[8] = "How much equity will I receive for my investment?";
	$faqsForInvestorAnswers[8] = "<p>Each startup decides how much money it wants to raise in exchange for what percentage of its equity. The final valuation numbers will depend on the negotiation between the investor and the startup- market determined.</p>";
?>


<section class="faqs">

	<div class="container">

		<h2 class="section-title text-center">Frequently Asked Questions</h2>
		
		<div class="text-center how_it_works_button_group">
			<ul id="myTab" class="btn-group" role="tablist">
				<li role="presentation" class="btn active btn-default">
			  		<a href="#faqs_investees" id="faqs_investees-tab" role="tab" data-toggle="tab" aria-controls="faqs_investees" aria-expanded="true">
				  		Investees
					</a>
				</li>
				
				<li role="presentation" class="btn btn-default">
					<a href="#faqs_investors" role="tab" id="faqs_investors-tab" data-toggle="tab" aria-controls="faqs_investors" aria-expanded="false">
						Investors
					</a>
				</li>
			
			</ul>
		</div>
		
		<div id="myTabContent" class="tab-content">
			<div role="tabpanel" id="faqs_investees" class="tab-pane fade active in row faq_row" aria-labelledby="faqs_investees">
				
				 <div class="containers1">
                    <div class="investee-faq-data" style="">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
	                            
	                            <?php for($i=0; $i<count($faqsForInvesteeQuestions); $i++) {?>
	                             <div class="company-accordion">
					                <div class="accordion-heading">
					                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapse<?= $i;?>">
						                    <h3><?= $faqsForInvesteeQuestions[$i];?></h3>
						                </a>
					                </div>
					                <div id="collapse<?= $i;?>" class="accordion-body collapse">
					                  <div class="accordion-inner clearfix"> 
					                  		<?= $faqsForInvesteeAnswers[$i];?>                 					                    
					                   </div>
					                </div>
					            </div>
					            <?php } ?>
	                           
                            </div>
                        </div>
                    </div>
                </div>				
						
			</div> <!-- faqs-Investee -->
		
			<div role="tabpanel" class=" tab-pane fade row faq_row" id="faqs_investors" aria-labelledby="faqs_investors">

				<div class="containers1">
                    <div class="investor-faq-data" style="">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                               
                               <?php for($i=0; $i<count($faqsForInvestorQuestions); $i++) {?>
	                             <div class="company-accordion">
					                <div class="accordion-heading">
					                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseInvestor<?= $i;?>">
						                    <h3><?= $faqsForInvestorQuestions[$i];?></h3>
						                </a>
					                </div>
					                <div id="collapseInvestor<?= $i;?>" class="accordion-body collapse">
					                  <div class="accordion-inner clearfix"> 
					                  		<?= $faqsForInvestorAnswers[$i];?>                 					                    
					                   </div>
					                </div>
					            </div>
					            <?php } ?>

                            </div>
                        </div>
                    </div>
                </div>	
		
		    </div> <!-- /.faqs_investors -->
					
		</div>

	</div>

</section> <!-- ./faqs -->

<!-- Accordion Slider scripts-->
<!-- ?= js('jquery_003.js') ? -->
<?= js('highlight.js') ?>
<script type="text/javascript">
            $(document).ready(function() {

                //syntax highlighter
                hljs.tabReplace = '    ';
                hljs.initHighlightingOnLoad();

                 var flipinvestee = 0;
                $( "#investee-faq" ).click(function() {
                    //console.log('hi');
                    flipinvestee++; 
                    $( "#investee-faq .investee-faq-data" ).toggle();
                    
                    if (flipinvestee % 2 === 0) {
                        $( ".investee-faq" ).removeClass('collapse-open-faq');
                        $( ".investee-faq" ).addClass('collapse-close-faq');
                    } else {
                        $( ".investee-faq" ).removeClass('collapse-close-faq');
                        $( ".investee-faq" ).addClass('collapse-open-faq');
                    }
                });

                var flipinvestor = 0;
                $( "#investor-faq" ).click(function() {
                    //console.log('hi');
                    flipinvestor++; 
                    $( "#investor-faq .investor-faq-data" ).toggle();
                    
                    if (flipinvestor % 2 === 0) {
                        $( ".investor-faq" ).removeClass('collapse-open-faq');
                        $( ".investor-faq" ).addClass('collapse-close-faq');
                    } else {
                        $( ".investor-faq" ).removeClass('collapse-close-faq');
                        $( ".investor-faq" ).addClass('collapse-open-faq');
                    }

                }); 

                $( ".accordion-about1 .accordion-about" ).click(function(event) {
                    //console.log('hi');
                    var lClose = $(this).hasClass('accordion-open-about');

                    $('.accordion-about1 .accordion-about').removeClass('accordion-close-about');
                    $('.accordion-about1 .accordion-about').addClass('accordion-close-about');


                    if ($(this).hasClass('accordion-open-about')) {
                        $(this).removeClass('accordion-open-about');
                        $(this).addClass('accordion-close-about');
                    } else {
                        $(this).removeClass('accordion-close-about');
                        if (lClose == false) $(this).addClass('accordion-open-about');
                    }

                    $('.accordion-about1 .accordion-about').next().hide();
                    if (lClose == false) $(this).next().slideFadeToggle('fast');                   
                    
                    event.stopPropagation();
                }); 

                $( ".accordion-about2 .accordion-about" ).click(function(event) {
                    
                    var lClose = $(this).hasClass('accordion-open-about'); 
                    //console.log('hi');
                    $('.accordion-about2 .accordion-about').removeClass('accordion-close-about');
                    $('.accordion-about2 .accordion-about').addClass('accordion-close-about');


                    if ($(this).hasClass('accordion-open-about')) {
                        $(this).removeClass('accordion-open-about');
                        $(this).addClass('accordion-close-about');
                    } else {
                        $(this).removeClass('accordion-close-about');
                        if (lClose == false) $(this).addClass('accordion-open-about');
                    }

                    $('.accordion-about2 .accordion-about').next().hide();
                    if (lClose == false) $(this).next().slideFadeToggle('fast');                   
                    
                    event.stopPropagation();
                }); 

                //accordion
                /*
                $('h5.accordion-faq').accordion({
                    defaultOpen: 'section1',
                    cookieName: 'accordion_nav',
                    speed: 'fast',
                    animateOpen: function (elem, opts) { //replace the standard slideUp with custom function
                        elem.next().slideFadeToggle(opts.speed);
                    },
                    animateClose: function (elem, opts) { //replace the standard slideDown with custom function
                        elem.next().slideFadeToggle(opts.speed);
                    }
                });
                *//*

                $('h5.accordion-about').accordion({
                    defaultOpen: 'section1',
                    cookieName: 'accordion_nav',
                    speed: 'fast',
                    animateOpen: function (elem, opts) { //replace the standard slideUp with custom function
                        elem.next().slideFadeToggle(opts.speed);
                    },
                    animateClose: function (elem, opts) { //replace the standard slideDown with custom function
                        elem.next().slideFadeToggle(opts.speed);
                    }
                });
    
                $('h5.accordion2').accordion({
                    defaultOpen: 'sample-1',
                    cookieName: 'accordion2_nav',
                    speed: 'fast',
                    cssClose: 'accordion2-close-about', //class you want to assign to a closed accordion header
                    cssOpen: 'accordion2-open-about',
                    
                });
                */
                //custom animation for open/close
                $.fn.slideFadeToggle = function(speed, easing, callback) {
                    return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
                };
                
            });
        </script>