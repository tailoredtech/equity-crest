<div class="main">
    <div class="container form-contain">
        <div class="row">
            <h4 class="form-header col-lg-12 col-md-12 col-sm-12 col-xs-12 lato-regular">Events Coming Up</h4>
        </div>
    </div>
        <div class="container">
            <div class="content-box">
                
                
                    <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10 events-bluebanner events-padding">
                    <div class="row text-center lato-regular events-headline">Startup Funding Made Easy</div>
                    <div class=" row text-center">
                        <div class="col-md-3"></div>
                        <div class="col-md-2">
                        <img src="<?php echo base_url(); ?>assets/images/funding_rising.png">
                        <div class="lato-regular events-name">Funding Rising</div>
                        </div>
                        <div class="col-md-2">
                            
                        <img src="<?php echo base_url(); ?>assets/images/product_launch.png">
                        <div class="lato-regular events-name">Product Launch</div>
                        </div>
                        <div class="col-md-2">
                        
                        <img src="<?php echo base_url(); ?>assets/images/market_insights.png">
                        <div class="lato-regular events-name">Market Insights</div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                    </div>
                    <div class="col-md-1"></div>
                    </div>
                
                
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-2 events-nav">
                            Date:<br>
                            <b>26th Aug 2014</b>
                        </div>
                        <div class="col-md-2 events-nav">
                            TIme:<br>
                            <b>10:20 am - 3:30 pm</b>
                        </div>
                        <div class="col-md-2 events-nav">
                            Place:<br>
                            <b>Mumbai</b>
                        </div>
                        <div class="col-md-2 events-nav">
                            Entry Fee (till 17th July)<br>
                            <b>Rs 18,600</b>
                        </div>
                        <div class="col-md-2 events-bottom">
                            Entry Fee (After 17th July)<br>
                            <b>Rs 20,000</b>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                
                <div class="container">
                    <div class="row">
                        <div class="col-md-9"></div>
                        <div class="col-md-1 text-center events-padding">
                            <input type="button" class="join-btn" value="Know More">
                        </div>
                        <div class="col-md-1 text-center events-padding">
                            <input type="submit" class="join-btn" value="Register">
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
                    
                </div>
                <div class="content-box">
                
                
                    <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10 events-greenbanner events-padding">
                    <div class="row text-center lato-regular events-headline">Startup Funding Made Easy</div>
                    <div class=" row text-center">
                        <div class="col-md-3"></div>
                        <div class="col-md-2">
                        <img src="<?php echo base_url(); ?>assets/images/funding_rising_g.png">
                        <div class="lato-regular events-name">Funding Rising</div>
                        </div>
                        <div class="col-md-2">
                            
                        <img src="<?php echo base_url(); ?>assets/images/product_launch_g.png">
                        <div class="lato-regular events-name">Product Launch</div>
                        </div>
                        <div class="col-md-2">
                        
                        <img src="<?php echo base_url(); ?>assets/images/market_insights_g.png">
                        <div class="lato-regular events-name">Market Insights</div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                    </div>
                    <div class="col-md-1"></div>
                    </div>
                
                
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-2 events-nav">
                            Date:<br>
                            <b>26th Aug 2014</b>
                        </div>
                        <div class="col-md-2 events-nav">
                            TIme:<br>
                            <b>10:20 am - 3:30 pm</b>
                        </div>
                        <div class="col-md-2 events-nav">
                            Place:<br>
                            <b>Mumbai</b>
                        </div>
                        <div class="col-md-2 events-nav">
                            Entry Fee (till 17th July)<br>
                            <b>Rs 18,600</b>
                        </div>
                        <div class="col-md-2 events-bottom">
                            Entry Fee (After 17th July)<br>
                            <b>Rs 20,000</b>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                
                <div class="container">
                    <div class="row">
                        <div class="col-md-9"></div>
                        <div class="col-md-1 text-center events-padding">
                            <input type="button" class="join-btn" value="Know More">
                        </div>
                        <div class="col-md-1 text-center events-padding">
                            <input type="submit" class="join-btn" value="Register">
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
                    
                </div>
            </div>
           
        </div>
        