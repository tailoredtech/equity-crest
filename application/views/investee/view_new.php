<div class="container">
    <div class="row">
        <div class="col-sm-5 block-title" >
        <h2><?php echo $investee['company_name'] ?></h2>
        </div>
    </div>
</div>
<div class="row content-box">
    <div class="profile-basic">
        <div class="company-logo">
            <div class="company-image company-border">
                <img src="<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id'] ?>/<?php echo $investee['image']; ?>" width="77" height="77" />
            </div>
        </div>
        <div class="company-title">
            <div class="company-name">
                <?php echo $investee['company_name']; ?>
            </div>
            <div class="company-location">
                <?php echo $investee['city']; ?>
            </div>
            <div class="company-rating">
                <?php echo $investee['sector']; ?>
            </div>
        </div>
        <div class="company-investment">
            <div class="company-border"> Fund Raise</div>
            <div class="fund-info company-border"><img style="height: 11px;margin-top: -2px;" src='<?php echo base_url(); ?>assets/images/rupee.png' /> <?php  echo format_money($investee['investment_required']); ?></div>
        </div>
        <div class="company-offered">
            <div class="company-border"> Stage</div>
            <div class="fund-info company-border"><?php echo $investee['stage']; ?></div>
        </div>
        <div class="company-validity">
            <div class="company-border"> Validity Period</div>
            <div class="fund-info company-border"><?php echo $investee['validity_period']; ?></div>
        </div>
        <div class="company-commit">
            <div> Minimum Investment</div>
            <div class="fund-info"><img style="height: 11px;margin-top: -2px;" src='<?php echo base_url(); ?>assets/images/rupee.png' /> <?php echo format_money($investee['commitment_per_investor']); ?></div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    <div id="tab-wrapper">
        <?= js('tabs_old.js') ?>
        <div id="tabContainer">
            <div class="tabs-wrap">
                <div id="tabs">
                  <ul>
                    <li id="tabHeader_1"><a href="#" class="active">Teams</a></li>
                    <li id="tabHeader_2"><a href="#">Business</a></li>
                    <li id="tabHeader_3"><a href="#">Revenue Model</a></li>
                    <li id="tabHeader_4"><a href="#">Market</a></li>
                    <li id="tabHeader_5"><a href="#">Raise</a></li>
                    <li id="tabHeader_6"><a href="#">Financials</a></li>
                    <li id="tabHeader_7"><a href="#">Achievements</a></li>
                  </ul>
                </div>
            </div>
            
            <?= js('jquery-1.8.2.js') ?>
            <?= js('jquery-ui.js') ?>
            
            <!-- lefnav starts here-->
            <div class="leftnavw">
                <div id="vertical-tab">
                  <ul>
                    <li> <a onClick="swap('one','two','three','four')">Company Presentation</a></li>
                    <li> <a onClick="swap('two','one','three','four')">Proposal Video</a></li>
                    <li> <a onClick="swap('three','two','one','four')">Q &amp; A</a></li>
                    <li> <a onClick="swap('four','two','one','three')" id="schedule-meeting-btn">Schedule Meeting</a></li>
                  </ul>
                </div>
                <div class="clear"></div>
                <?php if ($this->session->userdata('role') == 'investor') { ?>
                <div id="profile-sidenav" style="width:100%;float:left;">
                    <?php
                    $followed = $this->session->userdata('followed');
                    if (is_array($followed)) {
                        if (in_array($investee['user_id'], $followed)) {
                            ?>
                            <div class="unfollow action-btn">Unfollow</div>
                        <?php } else { ?>
                            <div class="follow action-btn">Follow</div>
                        <?php } ?>
                    <?php } else { ?>
                        <div class="follow action-btn">Follow</div>
                    <?php } ?>                
                  <!-- div class="action-btn">Ignore</div -->
                    <?php
                    $pledged = $this->session->userdata('pledged');
                    if (is_array($pledged)) {
                        if (in_array($investee['user_id'], $pledged)) {
                            ?>
                            <div class="action-btn">Pledged</div>
                        <?php } else { ?>
                            <div class="pledge action-btn">Pledge</div>
                        <?php } ?>
                    <?php } else { ?>
                        <div class="pledge action-btn">Pledge</div>
                    <?php } ?>
                </div>
                <?php } ?>
                <div class="clear"></div>
                <div class="social-icoWrap"> 
                    <?php if($investee['linkedin_url'] != '') { ?> 
                        <a href="<?php echo $investee['linkedin_url']; ?>" class="social-link" id="ln">Linked-in</a>
                    <?php } ?>
                    <?php if($investee['twitter_handle'] != '') { ?>
                    <a href="<?php echo $investee['twitter_handle']; ?>" class="social-link" id="tw">Twitter</a>
                    <?php } ?>
                    <?php if($investee['fb_url'] != '') { ?>
                    <a href="<?php echo $investee['fb_url']; ?>" class="social-link" id="fb">Facebook</a>
                    <?php } ?>
                </div>
                <script>
                $('#vertical-tab')
                    .tabs()
                    .addClass('ui-tabs-vertical ui-helper-clearfix');
                </script>
            </div>  
            <!-- lefnav end here-->
            
            <!--Middle contents starts-->
            <div id="vertical-tabContainer">
                <div id="one" class="vert-tab-content" style="display:none;">
                    <p>
                        <iframe src="" width="550" height="350" frameborder="0" scrolling="no" allowfullscreen> </iframe> 
                    </p>
                    <div id="slideshare-iframe" style="display: none;">
                        <?php echo $investee['presentation']; ?>
                    </div>                        
                </div>
                <div id="two" class="vert-tab-content" style="display:none;"> 
                     <?php $video_key = end(explode("=", $investee['video_link'])); ?>
                     <iframe style="margin: 11px 0px;" width="550" height="350" src="//youtube.com/embed/<?php echo $video_key;  ?>" frameborder="0" allowfullscreen></iframe>
                </div>
                
                <div id="three" class="vert-tab-content" style="display:none;">
                  <div style="width:60%; border-bottom:1px solid #ddd; margin-left:30px; padding-left:30px;">
                    <h5>Questions</h5>
                    <div style="float:right;">
                      <input style="width: 100px; margin-right: 0px;" class="btn eq-btn " id="ask-question-btn" type="button" value="Ask Question">
                    </div>
                    <div class="clear"></div>
                    <?php foreach($questions as $question){ ?>
                    <div class="qn-ans" id="<?php echo $question['id']; ?>">
                        <div class="qa-text qn">
                            <span class="q pull-left">Q.</span>
                            <p class="pull-right"><?php echo $question['question']; ?></p>
                            <p class="pull-right small-txt">Added by <span><?php echo $question['name']; ?></span>, <?php echo date("j M Y, g:i A",strtotime($question['qn_created_date']))  ?> 
                                
                                
                            </p>
                        </div>
                        <?php if($question['answer']){ ?>
                        <div class="qa-text ans">
                            <span class="a pull-left">A.</span>
                            <p class="pull-right"><?php echo $question['answer']; ?></p>
                            <p class="pull-right small-txt">By <span><?php echo $investee['company_name']; ?></span>, <?php echo date("j M Y, g:i A",strtotime($question['an_created_date']))  ?></p>
                        </div>
                        <?php } ?>
                    </div>
                    <?php } ?>
                    <div class="clear"></div>
                  </div>
                </div>

                <!-- div id="four" class="vert-tab-content" style="display:none;">
                  <div style="width:60%; border-bottom:1px solid #ddd; margin-left:30px; padding-left:30px;">
                    <h5>Schedule Meeting </h5>
                    <div style="float:right;">
                      <input style="width: 100px; margin-right: 0px;" class="btn-green" id="schedule-meeting-btn" type="button" value="Schedule">
                    </div>
                    <div class="clear"></div>
                  </div>
                </div -->
            </div>

            <div id="tab1-Content">
                <div id="tabscontent">
                    <div class="tabpage" id="tabpage_1" >
                        <?php if($teamPrivacy){ ?>
                        <div class="content-text">
                            <h5 class="lato-bold">Team</h5>
                            <p style="font-size: 14px;margin-top:10px;">
                                <?php echo strip_tags($investee['team_summary'], '<p><i><br><b><div><table><td><tr><th><ul><ol><li>'); ?>
                            </p>
                        </div>
                        <?php }else{ ?>
                        <div class="content-text">
                            <p>
                            <div class="alert alert-danger" role="alert">The company has restricted access to these information. <br><a href="javascript:void(0);" data-section-id="1" data-section="Team"  class="alert-link access-request-link">Click here to request for access</a>.</div>
                            </p>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="tabpage" id="tabpage_2" >
                        <?php if($businessPrivacy){ ?>
                        <div class="content-text">
                            <h5 class="lato-bold">Product/Service Description</h5>
                            <p style="font-size: 14px;margin-top:10px;">
                                <?php echo strip_tags($investee['products_services'], '<p><i><br><b><div><table><td><tr><th><ul><ol><li>'); ?>
                            </p>
                        </div>
                        <div class="content-text">
                            <h5 class="lato-bold">Unique Selling Proposition</h5>
                            <p style="font-size: 14px;margin-top:10px;">
                                <?php echo strip_tags($investee['how_different'], '<p><i><br><b><div><table><td><tr><th><ul><ol><li>'); ?>
                            </p>
                        </div>
                        <?php }else{ ?>
                        <div class="content-text">
                            <p>
                                <div class="alert alert-danger" role="alert">The company has restricted access to these information. <br><a href="javascript:void(0);" data-section-id="2" data-section="Business" class="alert-link access-request-link">Click here to request for access</a>.</div>
                            </p>
                            
                        </div>
                        <?php } ?>  
                    </div>
                    <div class="tabpage" id="tabpage_3" >
                        <?php if($monetizationPrivacy){ ?>
                        <div class="content-text">
                            <h5 class="lato-bold">Revenue Model</h5>
                            <p style="font-size: 14px;margin-top:10px;">
                                <?php echo strip_tags($investee['how_we_make_money'], '<p><i><br><b><div><table><td><tr><th><ul><ol><li>'); ?>
                            </p>
                        </div>
                        <div class="content-text">
                            <h5 class="lato-bold">Revenue Traction</h5>
                            <p style="font-size: 14px;margin-top:10px;">
                                <?php echo strip_tags($investee['customer_traction'], '<p><i><br><b><div><table><td><tr><th><ul><ol><li>'); ?>
                            </p>
                        </div>
                        <?php }else{ ?>
                        <div class="content-text">
                            <p>
                                <div class="alert alert-danger" role="alert">The company has restricted access to these information. <br><a href="javascript:void(0);" data-section-id="3" data-section="Monetization" class="alert-link access-request-link">Click here to request for access</a>.</div>
                            </p>
                        </div>
                        <?php } ?>
                    </div>    
                    <div class="tabpage" id="tabpage_4" >
                        <?php if($marketPrivacy){ ?>
                        <div class="content-text">
                            <h5 class="lato-bold">Total Addressable Market</h5>
                            <p>
                                <?php if($investee['addressable_market']){ ?>
                                   <img class="rupee-icon" src='<?php echo base_url(); ?>assets/images/rupee.png' /> <?php echo format_money($investee['addressable_market']); ?>
                                <?php }else{ ?>
                                    Not Available
                                <?php } ?>
                            </p>

                        </div>

                        <div class="content-text">
                            <h5 class="lato-bold">Competition</h5>
                            <p style="font-size: 14px;margin-top:10px;">
                                <?php echo strip_tags($investee['competition'], '<p><i><br><b><div><table><td><tr><th><ul><ol><li>'); ?>
                            </p>
                                <!-- ?php echo nl2br($investee['competition']); ? -->

                        </div>
                        <?php }else{ ?>
                            <div class="content-text">
                                <p>
                                    <div class="alert alert-danger" role="alert">The company has restricted access to these information. <br><a href="javascript:void(0);" data-section-id="4" data-section="Market" class="alert-link access-request-link">Click here to request for access</a>.</div>
                                </p>

                            </div>
                        <?php } ?>
                    </div>    
                    <div class="tabpage" id="tabpage_5" >
                        <?php if($raisePrivacy){ ?>
                        <div class="content-text">
                            <h5 class="lato-bold">Raise</h5>
                            <p>
                                <?php if($investee['equity_offered']){ ?>
                                Equity Offered : <?php echo $investee['equity_offered']; ?>%
                                <?php }else{ ?>
                                Equity Offered : Not Available
                                <?php } ?>
                            </p>
                        </div>
                        <div class="content-text">
                            <h5 class="lato-bold">Valuation</h5>

                            <?php if($investee['equity_offered']){ 
                                   $post_money_val = '';
                                   $pre_money_val = '';
                                   $invst_required = $investee['investment_required'];
                                    $eqty_offered  = $investee['equity_offered']/100;
                                    $post_money_val = $invst_required/$eqty_offered;
                                    $pre_money_val = $post_money_val - $invst_required; 
                             ?>
                                    <p>
                                        Pre Money Valuation : <?php echo format_money($pre_money_val); ?>
                                    </p>
                                    <p>
                                        Post Money Valution : <?php echo format_money($post_money_val) ?>
                                    </p>
                            <?php }else { ?>
                                   <p>
                                        Pre Money Valuation :  Not Available
                                    </p>
                                    <p>
                                        Post Money Valution :  Not Available
                                    </p>
                            <?php } ?>
                        </div>
                        <div class="content-text">
                            <h5 class="lato-bold">Use of Funds</h5>
                            <table class="table  investee-table ">
                                <tr>
                                    <th><b>Purpose</b></th>
                                    <th><b>Amount</b></th>
                                </tr>
                                <?php $total = 0; foreach($purposes as $purpose){
                                    if($purpose['purpose'] != ''){
                                    ?>
                                
                                <tr>
                                    <td><?php echo $purpose['purpose'];  ?></td>
                                    <td><img class="rupee-icon" src="<?php echo base_url(); ?>assets/images/rupee.png"><?php echo format_money($purpose['amount']);  ?></td>
                                </tr>
                                <?php $total += $purpose['amount']; 

                                    }
                                    } ?>
                                <tr>
                                    <td>Total</td>
                                    <td><img class="rupee-icon" src="<?php echo base_url(); ?>assets/images/rupee.png"><?php echo format_money($total);  ?></td>
                                </tr>
                            </table>

                        </div>
                        <div class="content-text">
                            <h5 class="lato-bold">Prior Fund raise details</h5>
                            <p>
                                <?php if($investee['funding_history']){ 
                                    echo $investee['funding_history']; ?>
                                <?php }else{ ?>
                                    Not Available
                                <?php } ?>
                            </p>
                        </div>
                        <?php }else{ ?>
                        <div class="content-text">
                            <p>
                                <div class="alert alert-danger" role="alert">The company has restricted access to these information. <br><a href="javascript:void(0);" data-section-id="5" data-section="Raise" class="alert-link access-request-link">Click here to request for access</a>.</div>
                            </p>

                        </div>
                        <?php } ?>
                    </div>    
                    <div class="tabpage" id="tabpage_6" >
                        <?php if($financialtPrivacy){ ?>
                        <div class="content-text">
                            <h5 class="lato-bold">Current Monthly Financial Indicators</h5>
                            <table class="table investee-table">
                                <tr>
                                    <td>Revenues</td>
                                    <td>
                                         <?php if($investee['monthly_revenue']){ ?>
                                           <?php if($investee['monthly_revenue']){?><img class="rupee-icon" src="<?php echo base_url(); ?>assets/images/rupee.png"><?php echo format_money($investee['monthly_revenue']); } ?>
                                        <?php }else{ ?>
                                            Not Available
                                        <?php } ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Fixed Cost (OPEX)</td>
                                    <td>
                                        <?php if($investee['fixed_opex']){ ?>
                                            <?php if($investee['fixed_opex']){ ?><img class="rupee-icon" src="<?php echo base_url(); ?>assets/images/rupee.png"><?php echo format_money($investee['fixed_opex']); } ?>
                                        <?php }else{ ?>
                                            Not Available
                                        <?php } ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Cash Burn</td>
                                    <td>
                                        <?php if($investee['cash_burn']){ ?>
                                            <?php if($investee['cash_burn']){ ?><img class="rupee-icon" src="<?php echo base_url(); ?>assets/images/rupee.png"><?php echo format_money($investee['cash_burn']); } ?>
                                        <?php }else{ ?>
                                            Not Available
                                        <?php } ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Debt</td>
                                    <td>
                                        <?php if($investee['debt']){ ?>
                                            <?php if($investee['debt']){ ?><img class="rupee-icon" src="<?php echo base_url(); ?>assets/images/rupee.png"><?php echo format_money($investee['debt']); } ?>
                                        <?php }else{ ?>
                                            Not Available
                                        <?php } ?>
                                    </td>
                                </tr>
                            </table>
                            
                        </div>
                        <div class="content-text">
                            <h5 class="lato-bold">Financial Forecast</h5>
                            <p>
                                 <?php if($investee['financial_forecast']){ ?>
                                    <?php if($financialForecastPermission) { ?>
                                        <a href='<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id']; ?>/files/<?php echo $investee['financial_forecast']; ?>' target="_blank"><?php echo $investee['financial_forecast']; ?></a>
                                    <?php }else{ ?>
                                        <?php echo $investee['financial_forecast']; ?>&nbsp;(<span class="download-request-link" data-section-id="8" data-section="Financial Forecast">Click here to request for access</span>)
                                    <?php } ?>
                                <?php }else{ ?>
                                    Not Available
                                <?php } ?>
                            </p>

                        </div>

                        <div class="content-text">
                            <h5 class="lato-bold">Financial Statements</h5>
                            <p>
                                <?php if($investee['financial_statement']){ ?>
                                   <?php if($financialStatementPermission) { ?>
                                    <a href='<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id']; ?>/files/<?php echo $investee['financial_statement']; ?>' target="_blank"><?php echo $investee['financial_statement']; ?></a>
                                    <?php }else{ ?>
                                        <?php echo $investee['financial_statement']; ?>&nbsp;(<span class="download-request-link" data-section-id="9" data-section="Financial Statements">Click here to request for access</span>)
                                    <?php } ?>
                                <?php }else{ ?>
                                    Not Available
                                <?php } ?>
                            </p>

                        </div>

                        <div class="content-text">
                            <h5 class="lato-bold">Others (if any)</h5>
                            <p>
                                <?php if($investee['other']){ ?>
                                    <a href='<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id']; ?>/files/<?php echo $investee['other']; ?>' target="_blank"><?php echo $investee['other']; ?></a>
                                <?php }else{ ?>
                                    Not Available
                                <?php } ?>
                            </p>

                        </div>
                        <?php }else{ ?>
                            <div class="content-text">
                                <p>
                                    <div class="alert alert-danger" role="alert">The company has restricted access to these information. <br><a href="javascript:void(0);" data-section-id="6" data-section="Financials" class="alert-link access-request-link">Click here to request for access</a>.</div>
                                </p>

                            </div>
                        <?php } ?>
                    </div>    
                    <div class="tabpage" id="tabpage_7" >
                        <?php if($achievementsPrivacy){ ?>
                        <div class="content-text">
                            <h5 class="lato-bold">Media</h5>
                            <p>
                                <?php if($investee['media']){ 
                                    echo $investee['media']; ?>
                                <?php }else{ ?>
                                    Not Available
                                <?php } ?>
                            </p>

                        </div>
                        <div class="content-text">
                            <h5 class="lato-bold">Awards & Recognition</h5>
                            <p>
                                <?php if($investee['awards']){ 
                                    echo $investee['awards']; ?>
                                <?php }else{ ?>
                                    Not Available
                                <?php } ?>
                            </p>

                        </div>

                        <div class="content-text">
                            <h5 class="lato-bold">Testimonials</h5>
                            <p>
                                <?php if($investee['testimonials']){ 
                                    echo $investee['testimonials']; ?>
                                <?php }else{ ?>
                                    Not Available
                                <?php } ?>
                            </p>

                        </div>

                        <?php }else{ ?>
                            <div class="content-text">
                                <p>
                                    <div class="alert alert-danger" role="alert">The company has restricted access to these information. <br><a href="javascript:void(0);" data-section-id="7" data-section="Achievements" class="alert-link access-request-link">Click here to request for access</a>.</div>
                                </p>

                            </div>
                        <?php } ?> 
                    </div>    
                </div>
            </div>    
            <!--Middle contents end here-->

            <div class="content-infographic-rght">
                <div class="content-infographic">
                    <div class="item" style="display: none">
                        <span class="market-icon"></span>
                        <div> Stage</div>
                        <div class="lato-bold"><?php echo $investee['stage']; ?></div>
                    </div>
                    <div class="item">
                        <span class="value-icon"></span>
                        <div> Founding Year</div>
                        <div class="tab-lato-bold"><?php echo $investee['year']; ?></div>
                    </div>
                    <div class="item">
                        <span class="competition-icon"></span>
                        <div> Sector</div>
                        <div class="tab-lato-bold"><?php echo $investee['sector']; ?></div>
                    </div>
                    <div class="item">
                        <span class="cost-icon"></span>
                        <div> Business Type</div>
                        <div class="tab-lato-bold"><?php echo $investee['business']; ?></div>
                    </div>
                    <div class="item">
                        <span class="fund-icon"></span>
                        <div> Company URL</div>
                        <div class="tab-lato-bold"><?php echo anchor(prep_url($investee['company_url']),$investee['company_url'],'target="_blank"'); ?></div>
                    </div>
                    <div class="item">
                        <span class="product-icon"></span>
                        <div> Product URL</div>
                        <div class="tab-lato-bold"><?php echo anchor(prep_url($investee['product_url']),$investee['product_url'],'target="_blank"'); ?></div>
                    </div>
                
                </div>
            </div>
        </div>
    </div>
    <div id="ResOuterbanner">
        <div id="Resbanner">
            <div class="flexslider">
                <ul class="slides">
                </ul>
            </div>
        </div>
    </div>
    <div class="slide-social-Wrap">
        <?php if ($this->session->userdata('role') == 'investor') { ?>
        <div style="width:100%;float:left;">
            <?php
            $followed = $this->session->userdata('followed');
            if (is_array($followed)) {
                if (in_array($investee['user_id'], $followed)) {
                    ?>
                    <div class="unfollow action-btn">Unfollow</div>
                <?php } else { ?>
                    <div class="follow action-btn">Follow</div>
                <?php } ?>
            <?php } else { ?>
                <div class="follow action-btn">Follow</div>
            <?php } ?>                
          <!-- div class="action-btn">Ignore</div -->
            <?php
            $pledged = $this->session->userdata('pledged');
            if (is_array($pledged)) {
                if (in_array($investee['user_id'], $pledged)) {
                    ?>
                    <div class="action-btn">Pledged</div>
                <?php } else { ?>
                    <div class="pledge action-btn">Pledge</div>
                <?php } ?>
            <?php } else { ?>
                <div class="pledge action-btn">Pledge</div>
            <?php } ?>

          <div class="action-btn">Pledged</div>
        </div>
        <?php } ?>
        <div class="clear"></div>
        <div class="social-icoWrap"> 
            <?php if($investee['linkedin_url'] != '') { ?> 
                <a href="<?php echo $investee['linkedin_url']; ?>" class="social-link" id="ln">Linked-in</a>
            <?php } ?>
            <?php if($investee['twitter_handle'] != '') { ?>
            <a href="<?php echo $investee['twitter_handle']; ?>" class="social-link" id="tw">Twitter</a>
            <?php } ?>
            <?php if($investee['fb_url'] != '') { ?>
            <a href="<?php echo $investee['fb_url']; ?>" class="social-link" id="fb">Facebook</a>
            <?php } ?>
        </div>
    </div>    
</div>

<!-- POPUP CONTAINER -->
<div style="display: none;">
    <div id="ask-question" class="container pop-content">
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <h3 class="form-header top-margin-10">Ask Question</h3>
            </div>
        </div>
        <div class="row">
            <div class="top-margin-10">
            <form id="ask-qn-form" method="post">
                <div class="col-xs-12 col-sm-12">
                    <div class="row">
						<div class="col-sm-12">
							<textarea name="question" id="question" class="form-control form-area" placeholder="your query" rows="10"></textarea>
							<input type="hidden" name="investee_id" value="<?php echo $investee['user_id']; ?>" />
						</div>
                    </div>
                    <div class="row">
						<div class="col-sm-12">
                        <label id="question_error" for="question" class="error" style="float:left;">&nbsp;</label>
                        <input class="eq-btn pull-right" type="submit" value="OK" />
						</div> 
					</div>
                </div>
            </form>
            </div>
        </div>
    </div>
    
    <div id="new-qn-txt">
      <div class="qn-ans">  
        <div class="qa-text qn">
            <span class="q pull-left">Q.</span>
            <p class="pull-right qun"></p>
            <p class="pull-right small-txt">Added by <span><?php echo $this->session->userdata('name'); ?></span>, 26 Jan 2014, 12:00PM</p>
        </div>
      </div>    
    </div>
    
    <div id="pledge-pop" class="container pop-content">
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <h3 class="form-header top-margin-10">Pledge</h3>
            </div>
        </div>
        <div class="row">
            <div class="top-margin-10">
            <form id="form-pledge" method="post">
                <div class="col-xs-12 col-sm-12">
                    <div class="row">
                    <div class="form-group">
                        <label for="founder-name" class="col-sm-2">Amount</label>
                        <div class="col-sm-10">
                            <input type="text" name="amount" id="amount" value="" class="form-control form-input"  placeholder="Ex. 1,00,000">
                            <input type="hidden" name="investee_id" value="" class="invste_id" />
                            <label id="amount_error" for="amount" class="error">&nbsp;</label>
                        </div>
                    </div>
                    </div>
                    <div class="text-center pull-right">
                        <input class="eq-btn" type="submit" value="OK" />
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>

    <div id="schedule-meeting-pop" class="container pop-content">
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <h3 class="form-header top-margin-10">Schedule Meeting</h3>
            </div>
        </div>
        <div class="row">
            <div class="top-margin-10">
            <form id="schedule-meeting-form" method="post">
                <div class="col-xs-12 col-sm-12">
                    <div class="row">
                        <div class="form-group">
                            <label for="meeting_time" class="col-sm-2">Date</label>
                            <div class="col-sm-10">
                                <input type="text" id="meeting-time" name="meeting-time" value="" class="form-control form-input"  placeholder="">
                                <input type="hidden" id="investee_id" name="investee_id" value="<?php echo $investee['user_id']; ?>"  />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="title" class="col-sm-2">Subject</label>
                            <div class="col-sm-10">
                                <input type="text" name="title" id="title"  value="" class="form-control form-input"  placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="remark" class="col-sm-2 ">Message</label>
                            <div class="col-sm-10">
                                <textarea name="remark" id="remark"  class="form-control form-area"  placeholder=""></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="text-center pull-right">
                        <input class="eq-btn" type="submit" value="OK" />
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
      
        var embed_id = $('#one #slideshare-iframe  iframe').attr('src');
        $('#one p iframe').attr('src',embed_id);
        
        var investeeId = '<?php echo $investee['user_id']; ?>';
        
        $('#profile-sidenav').on('click', '.follow', function() {
            <?php if ($this->session->userdata('user_id')) { ?>
                var this_var = $(this);        
            $.ajax({
                    url: "<?php echo base_url(); ?>user/follow",
                    type: 'POST',
                    data : {followId : investeeId}
                })
                .done(function( data ) {
                     if( data == 'success'){
                        this_var.text('Unfollow');
                        this_var.removeClass('follow')
                    }else{
                        console.log(data);
                    }
                });
            <?php } else { ?>            
              window.location.href = '<?php echo base_url(); ?>user/login';
            <?php } ?>
        });
        
         $('#profile-sidenav').on('click', '.unfollow', function() {
                var this_var = $(this);
                $.ajax({
                    url: "<?php echo base_url(); ?>user/unfollow",
                    type: 'POST',
                    data: {followId: investeeId}
                })
                        .done(function(data) {
                    if (data == 'success') {
                        this_var.text('Follow');
                        this_var.removeClass('unfollow')
                    } else {
                        console.log('error');
                    }
                });
            });
        
        $('#profile-sidenav').on('click', '.pledge', function() {
            var invste_id = investeeId;
            $("#form-pledge .invste_id").val(invste_id);
            $(this).addClass('pledging');
            $.colorbox({inline: true, href: '#pledge-pop', maxWidth: '450px', innerWidth: '80%'});//, innerHeight: '185px'
        });

        $("#form-pledge").on('submit', (function(e) {
            e.preventDefault();
            $.ajax({
                url: "<?php echo base_url(); ?>investee/pledge",
                type: 'POST',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false
            })
            .done(function(data) {
                if (data == 'success') {
                    $('#profile-sidenav .pledging').text('Pledged');
                    $('#profile-sidenav .pledging').removeClass('pledge pledging');
                } else {
                    $('#amount_error').text(data.result);
                    return;
                }
                $.colorbox.close();
            });
        }));
        
        
        $("#ask-question-btn").on('click',function(){
            $.colorbox({inline: true, href: '#ask-question', innerWidth: '80%', maxWidth: '550px'});//, innerHeight: '360px'
        })

        $('#question').on('focus',function(){
            $('#question_error').html("&nbsp;");
        })

        $('#ask-qn-form').submit(function(event){
            event.preventDefault();
            if($('#ask-qn-form textarea').val() != ''){
                var question = $('#ask-qn-form textarea').val();
                    $.ajax({
                    url: "<?php echo base_url(); ?>investor/ask_question",
                    type: 'POST',
                    data : {question : question,investeeId:investeeId}
                    })
                    .done(function() {
                        $('#new-qn-txt .qun').text(question);
                        $('.questions-head').after($('#new-qn-txt').html());
                        $.colorbox.close();
                    });
            }else{
                //$('#ask-qn-form textarea').css('border-color', '#a94442');
                $('#question_error').text("Please enter the question you want to ask.");
            }
        });
        
        $('.box-close').on('click',function(){
            $.colorbox.close();
        });
        
        /*
        $(".tab-item").click(function() {
            $(".profile-tabs .tab-active").removeClass("tab-active");
            $(this).addClass("tab-active");
            $('a .sidelink').css('background', '#878686');
        });
        */

        
        $('#meeting-time').datetimepicker({
            format:'d-m-Y H:i',
            step:30
        });
        
        $("#profile-sidenav ").on('click','#schedule-meeting-btn',function(){
            $.colorbox({inline: true, href: '#schedule-meeting-pop', innerWidth: '80%', maxWidth: '400px'});//, innerHeight: '325px'
        });
        
        $("#schedule-meeting-form").on('submit', (function(e) {
            e.preventDefault();
            if($('#meeting_time').val() != ''){
                $.ajax({
                    url: "<?php echo base_url(); ?>investor/schedule_meeting_process",
                    type: 'POST',
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false
                })
                .done(function(data) {
                    if (data !== 'success') {
                        console.log('error');
                        return;
                    }

                    $.colorbox.close();
                });
            } else {
                //

            }

        }));
        
        $(".tab-content").on('click','.access-request-link', (function() {
            var sectionId = $(this).attr('data-section-id');
            var sectionName = $(this).attr('data-section');
            var section = $(this);
            $.ajax({
                url: "<?php echo base_url(); ?>user/access_request",
                type: 'POST',
                data : {investeeId:investeeId,sectionId : sectionId,section : sectionName}
            })
            .done(function(data) {
                if (data == 'success') {
                    section.parent('.alert').html('Successfully sent the request.');
                } else {
                    section.parent('.alert').html('Already sent the request.');
                }
            });

        }));
        
        $(".tab-content").on('click', '.download-request-link', (function() {
            var sectionId = $(this).attr('data-section-id');
            var sectionName = $(this).attr('data-section');
            var section = $(this);
            $.ajax({
                url: "<?php echo base_url(); ?>user/download_request",
                type: 'POST',
                data : {investeeId:investeeId,sectionId : sectionId,section : sectionName}
            })
            .done(function(data) {
                if (data == 'success') {
                    section.html('Successfully sent the request.');
                } else {
                    section.html('Already sent the request.');
                }
            });

        }));   
    });
</script>
<!-- script language="javascript1.5">
function swap(one, two, three, four) {
    document.getElementById(one).style.display = 'block';
    document.getElementById(two).style.display = 'none';
    document.getElementById(three).style.display = 'none';
    //document.getElementById(four).style.display = 'none';
}
</script -->
