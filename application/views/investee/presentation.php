<div class="main">
    <div class="container">
        <div class="row">
            <h4 class="form-header  col-xs-12 ">Company Profile</h4>
        </div>

        <div class="content-box row">
            <div class="profile-basic col-sm-12">
                <div class="row">
                    <div class="company-logo">
                        <div class="company-image company-border">
                            <img src="<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id'] ?>/<?php echo $investee['image']; ?>" width="77" height="77" />
                        </div>
                    </div>
                    <div class="company-title">
                        <div class="company-name">
                            <?php echo $investee['name']; ?>
                        </div>
                        <div class="company-location">
                            <?php echo $investee['city']; ?>
                        </div>
                        <div class="company-rating">
                            <img src="<?php echo base_url(); ?>assets/images/rating.jpg">
                        </div>
                    </div>
                    <div class="company-investment">
                        <div class="company-border"> Investment Required</div>
                        <div class="lato-bold company-border"><?php echo $investee['investment_required']; ?> INR</div>
                    </div>
                    <div class="company-offered">
                        <div class="company-border"> Equity Offered</div>
                        <div class="lato-bold company-border"><?php echo $investee['equity_offered']; ?>%</div>
                    </div>
                    <div class="company-validity">
                        <div class="company-border"> Validity Period</div>
                        <div class="lato-bold company-border"><?php echo $investee['validity_period']; ?></div>
                    </div>
                    <div class="company-commit">
                        <div> Min. Commitment Per Investor</div>
                        <div class="lato-bold"><?php echo $investee['commitment_per_investor']; ?> INR</div>
                    </div>
                </div>
            </div>

            <div class="profile-detail col-sm-12">
                <div class="row">
                    <div class="col-sm-2 profile-item company-border">
                        <div> Stage</div>
                        <div class="lato-bold"><?php echo $investee['stage']; ?></div>
                    </div>
                    <div class="col-sm-2 profile-item company-border">
                        <div> Founding Year</div>
                        <div class="lato-bold"><?php echo $investee['year']; ?></div>
                    </div>
                    <div class="col-sm-2 profile-item company-border">
                        <div> Sector</div>
                        <div class="lato-bold"><?php echo $investee['sector']; ?></div>
                    </div>
                    <div class="col-sm-2 profile-item company-border">
                        <div> Type of Business</div>
                        <div class="lato-bold"><?php echo $investee['business']; ?></div>
                    </div>
                    <div class="col-sm-2 profile-item company-border">
                        <div> Comapany URL</div>
                        <div class="lato-bold"><?php echo $investee['company_url']; ?></div>
                    </div>
                    <div class="col-sm-2 profile-item">
                        <div> Product URL</div>
                        <div class="lato-bold"><?php echo $investee['product_url']; ?></div>
                    </div>
                </div>
            </div>

            <div class="profile-tabs col-sm-12">
                <div class="row">
                    <ul class="nav" role="tablist">
                        <li>
                            <div class="col-sm-offset-4 col-sm-1">
                                <a href="#team" role="tab" data-toggle="tab">Team</a>
                            </div>
                        </li>
                        <li>
                            <div class="col-sm-1">
                                <a href="#product" role="=tab" data-toggle="tab">Product</a>
                            </div>
                        </li>
                        <li>
                            <div class="col-sm-1">
                                <a href="#customer" role="=tab" data-toggle="tab">Customer</a>
                            </div>
                        </li>
                        <li>
                            <div class="col-sm-1">
                                <a href="#market" role="=tab" data-toggle="tab">Market</a>
                            </div>
                        </li>
                        <li>
                            <div class="col-sm-2" >
                                <a href="#funding" role="=tab" data-toggle="tab">Funding History</a>
                            </div>
                        </li>
                        <li>
                            <div class="col-sm-1">
                                <a href="#finance" role="=tab" data-toggle="tab">Financial</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="tab-content">
                <div class="profile-content col-sm-12 tab-pane active" id="team">
                    <div class="row">
                        <div class="col-sm-3 profile-sidenav">
                            <div class="row">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <a href="<?php echo base_url(); ?>investee/presentation"><div class="sidelink" style="background: #a7e41e;">Company Presentation</div></a>
                                    <a href="<?php echo base_url(); ?>investee/proposal_video"><div class="sidelink" >Proposal Video</div></a>
                                    <div class="sidelink">Contact Equity Crest</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="content-text">
                                <a href='<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id'];  ?>/files/<?php echo $investee['presentation']; ?>'><?php echo $investee['presentation']; ?></a>
                            </div>
                           

                        </div>
                        <div class="col-sm-4">
                            <div class="content-infographic">
                                <div class="item">
                                    <span class="market-icon"></span>
                                    <div>Total Addresable Market </div>
                                    <div class="lato-bold"><?php echo $investee['addressable_market']; ?> INR</div>
                                </div>
                                <div class="item">
                                    <span class="value-icon"></span>
                                    <div>Valuation </div>
                                    <div class="lato-bold"><?php echo $investee['valuation']; ?> INR</div>
                                </div>
                                <div class="item">
                                    <span class="competition-icon"></span>
                                    <div>Competition </div>
                                    <div class="lato-bold"><?php echo $investee['competition']; ?></div>
                                </div>
                                <div class="item">
                                    <span class="cost-icon"></span>
                                    <div>Company Cost & Margins</div>
                                    <div class="lato-bold"><?php echo $investee['costs_margins']; ?></div>
                                </div>
                                <div class="item">
                                    <span class="fund-icon"></span>
                                    <div>Uses of funds</div>
                                    <div class="lato-bold"><?php echo $investee['uses_of_funds']; ?></div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>


                <div class="profile-content col-sm-12 tab-pane" id="product">
                    <div class="row">
                        <div class="col-sm-3 profile-sidenav">
                            <div class="row">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <a href="<?php echo base_url(); ?>investee/presentation"><div class="sidelink">Company Presentation</div></a>
                                    <a href="<?php echo base_url(); ?>investee/proposal_video"><div class="sidelink">Proposal Video</div></a>
                                    <div class="sidelink">Contact Equity Crest</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-9">

                            <div class="content-text">
                                <h4 class="lato-bold">Product/Service Summary</h4>
                                <p>
                                    <?php echo $investee['products_services']; ?>
                                </p>

                            </div>
                            <div class="content-text">
                                <h4 class="lato-bold">Competition</h4>
                                <p>
                                    <?php echo $investee['competition']; ?>
                                </p>

                            </div>
                        </div>
                    </div>
                </div>
                
                
                <div class="profile-content col-sm-12 tab-pane" id="customer">
                    <div class="row">
                        <div class="col-sm-3 profile-sidenav">
                            <div class="row">
                                <div class="col-sm-10 col-sm-offset-2">
                                   <a href="<?php echo base_url(); ?>investee/presentation"><div class="sidelink">Company Presentation</div></a>
                                    <a href="<?php echo base_url(); ?>investee/proposal_video"><div class="sidelink">Proposal Video</div></a>
                                    <div class="sidelink">Contact Equity Crest</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-9">

                            <div class="content-text">
                                <h4 class="lato-bold">Customer Traction</h4>
                                <p>
                                    <?php echo $investee['customer_traction']; ?>
                                </p>

                            </div>
                            <div class="content-text">
                                <h4 class="lato-bold">Unique Selling Points</h4>
                                <p>
                                    <?php echo $investee['how_different']; ?>
                                </p>
                            </div>
                            <div class="content-text">
                                <h4 class="lato-bold">Revenue Model</h4>
                                <p>
                                    <?php echo $investee['how_we_make_money']; ?>
                                </p>
                            </div>

                        </div>
                        

                    </div>
                </div>
                
                <div class="profile-content col-sm-12 tab-pane" id="market">
                    <div class="row">
                        <div class="col-sm-3 profile-sidenav">
                            <div class="row">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <a href="<?php echo base_url(); ?>investee/presentation"><div class="sidelink">Company Presentation</div></a>
                                    <a href="<?php echo base_url(); ?>investee/proposal_video"><div class="sidelink">Proposal Video</div></a>
                                    <div class="sidelink">Contact Equity Crest</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-9">

                            <div class="content-text">
                                <h4 class="lato-bold">Total Addresable Market</h4>
                                <p>
                                    <?php echo $investee['addressable_market']; ?>
                                </p>

                            </div>
                            <div class="content-text">
                                <h4 class="lato-bold">Valuation</h4>
                                <p>
                                    <?php echo $investee['valuation']; ?>
                                </p>

                            </div>

                            <div class="content-text">
                                <h4 class="lato-bold">Competition</h4>
                                <p>
                                    <?php echo $investee['competition']; ?>
                                </p>

                            </div>
                            
                            <div class="content-text">
                                <h4 class="lato-bold">Company Cost & Margins</h4>
                                <p>
                                    <?php echo $investee['costs_margins']; ?>
                                </p>

                            </div>
                            
                            <div class="content-text">
                                <h4 class="lato-bold">Use of Funds</h4>
                                <p>
                                    <?php echo $investee['costs_margins']; ?>
                                </p>

                            </div>

                        </div>
                        

                    </div>
                </div>
                        
                <div class="profile-content col-sm-12 tab-pane" id="funding">
                    <div class="row">
                        <div class="col-sm-3 profile-sidenav">
                            <div class="row">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <a href="<?php echo base_url(); ?>investee/presentation"><div class="sidelink">Company Presentation</div></a>
                                    <a href="<?php echo base_url(); ?>investee/proposal_video"><div class="sidelink">Proposal Video</div></a>
                                    <div class="sidelink">Contact Equity Crest</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="content-text">
                                <h4 class="lato-bold"> Funding History</h4>
                                <p>
                                    <?php echo $investee['funding_history']; ?>
                                </p>
                            </div>
                        </div>
                        

                    </div>
                </div>
                
                <div class="profile-content col-sm-12 tab-pane" id="finance">
                    <div class="row">
                        <div class="col-sm-3 profile-sidenav">
                            <div class="row">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <a href="<?php echo base_url(); ?>investee/presentation"><div class="sidelink">Company Presentation</div></a>
                                    <a href="<?php echo base_url(); ?>investee/proposal_video"><div class="sidelink">Proposal Video</div></a>
                                    <div class="sidelink">Contact Equity Crest</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-9">

                            <div class="content-text">
                                <h4 class="lato-bold">Financial Forecast</h4>
                                <p>
                                    <a href='<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id'];  ?>/files/<?php echo $investee['financial_forecast']; ?>'><?php echo $investee['financial_forecast']; ?></a>
                                </p>

                            </div>
                            <div class="content-text">
                                <h4 class="lato-bold">Annual Report of last 3 years</h4>
                                <p>
                                    <a href='<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id'];  ?>/files/<?php echo $investee['annual_report']; ?>'><?php echo $investee['annual_report']; ?></a>
                                </p>

                            </div>

                            <div class="content-text">
                                <h4 class="lato-bold">Top Risk Factors</h4>
                                <p>
                                    <a href='<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id'];  ?>/files/<?php echo $investee['riskfactors']; ?>'><?php echo $investee['riskfactors']; ?></a>
                                </p>

                            </div>
                            
                            <div class="content-text">
                                <h4 class="lato-bold">Source of funds</h4>
                                <p>
                                    <a href='<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id'];  ?>/files/<?php echo $investee['fund_sources']; ?>'><?php echo $investee['fund_sources']; ?></a>
                                </p>

                            </div>
                            
                            <div class="content-text">
                                <h4 class="lato-bold">P&L Report</h4>
                                <p>
                                    <a href='<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id'];  ?>/files/<?php echo $investee['pl_report']; ?>'><?php echo $investee['pl_report']; ?></a>
                                </p>

                            </div>
                            
                            <div class="content-text">
                                <h4 class="lato-bold">Bankers</h4>
                                <p>
                                    <a href='<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id'];  ?>/files/<?php echo $investee['bankers']; ?>'><?php echo $investee['bankers']; ?></a>
                                </p>

                            </div>
                            
                            <div class="content-text">
                                <h4 class="lato-bold">Lawyers</h4>
                                <p>
                                    <a href='<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id'];  ?>/files/<?php echo $investee['lawyers']; ?>'><?php echo $investee['lawyers']; ?></a>
                                </p>

                            </div>
                            
                            <div class="content-text">
                                <h4 class="lato-bold">Auditors</h4>
                                <p>
                                    <a href='<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id'];  ?>/files/<?php echo $investee['auditors']; ?>'><?php echo $investee['auditors']; ?></a>
                                </p>

                            </div>
                            
                            <div class="content-text">
                                <h4 class="lato-bold">Share valuation</h4>
                                <p>
                                    <a href='<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id'];  ?>/files/<?php echo $investee['share_valuation']; ?>'><?php echo $investee['share_valuation']; ?></a>
                                </p>

                            </div>
                            
                            <div class="content-text">
                                <h4 class="lato-bold">Financial Statements</h4>
                                <p>
                                    <a href='<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id'];  ?>/files/<?php echo $investee['financial_statement']; ?>'><?php echo $investee['financial_statement']; ?></a>
                                </p>

                            </div>

                        </div>
                    </div>
                </div>
                
            </div>                <!-- end of tabpanes -->


        </div>
    </div>
</div>
