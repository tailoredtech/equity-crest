<!-- Start Revenue Model panel -->
<div class="row">
    <div class="col-xs-4 block-title">
        <h2>Revenue Model</h2>
    </div>
    <div class="col-xs-offset-2 col-xs-6 block-title">
        <span class="glyphicon glyphicon-cog privacy-icon"></span>
        <div class="privacy-options">
        <div class="input-group">
                    <span class="input-group-addon">Visibility:</span>
            <select name="monetization" class="form-control form-input" >
                <option value="0" <?php echo (isset($privacy['monetization']) && $privacy['monetization'] == 0) ? 'selected' : ''; ?>>All</option>
                <option value="1" <?php echo (isset($privacy['monetization']) && $privacy['monetization'] == 1) ? 'selected' : ''; ?>>All Investors & Partners</option>
                <option  value="2" <?php echo (isset($privacy['monetization']) && $privacy['monetization'] == 2) ? 'selected' : ''; ?>>Pledged Investors</option>
                <option  value="3" <?php echo (isset($privacy['monetization']) && $privacy['monetization'] == 3) ? 'selected' : ''; ?>>Followers</option>
                <option  value="4" <?php echo (isset($privacy['monetization']) && $privacy['monetization'] == 4) ? 'selected' : ''; ?>>On Request</option>
            </select>
            </div>
        </div>
    </div>
</div>
<div class="row margin-top-10">
    <form id="investee-info-form" action="<?php echo base_url(); ?>user/edit_investee_info_process" enctype="multipart/form-data" method="post" >
    <input type="hidden" name="user_id" value="<?php echo $investee_id; ?>" />
    <input type="hidden" name="investee_id" value="<?php echo $user_id; ?>" />
    <div id="Revenue_Model" class="accordion2">
        <div class="accordion-inner">
            <div class="form-group row">
                <div class="col-sm-12">
                    <input type="submit" name="revenue_submit"  class="btn btn-eq-common pull-right" value="Save & Continue" />
                </div>
            </div>                    
            <div class="form-group row">
                <label for="how-we-make-money" class="col-sm-5 form-label">Revenue Model</label>
                <div class="col-sm-7">
                    <textarea class="form-control form-input form-area" id="how-we-make-money" name="how_we_make_money"  placeholder=""><?php echo isset($user['how_we_make_money']) ? $user['how_we_make_money'] : ""; ?></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label for="customer-traction" class="col-sm-5 form-label">Traction</label>
                <div class="col-sm-7">
                    <textarea class="form-control form-input form-area" id="customer-traction" name="customer_traction"  placeholder=""><?php echo isset($user['customer_traction']) ? $user['customer_traction'] : ""; ?></textarea>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <input type="submit" name="revenue_submit"  class="btn btn-eq-common pull-right" value="Save & Continue" />
                </div>
            </div>                    
        </div>
    </div>
</div>
<!-- End Revenue Model panel -->