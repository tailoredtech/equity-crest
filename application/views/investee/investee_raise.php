<!-- Start Raise panel -->
<div class="row">
    <div class="col-xs-4 block-title">
        <h2>Raise</h2>
    </div>
    <div class="col-xs-offset-2 col-xs-6 block-title">
        <span class="glyphicon glyphicon-cog privacy-icon"></span>
        <div class="privacy-options">
        <div class="input-group">
                    <span class="input-group-addon">Visibility:</span>
            <select name="raise" class="form-control form-input">
                <option value="0" <?php echo (isset($privacy['raise']) && $privacy['raise'] == 0) ? 'selected' : ''; ?>>All</option>
                <option value="1" <?php echo (isset($privacy['raise']) && $privacy['raise'] == 1) ? 'selected' : ''; ?>>All Investors & Partners</option>
                <option value="2" <?php echo (isset($privacy['raise']) && $privacy['raise'] == 2) ? 'selected' : ''; ?>>Pledged Investors</option>
                <option value="3" <?php echo (isset($privacy['raise']) && $privacy['raise'] == 3) ? 'selected' : ''; ?>>Followers</option>
                <option value="4" <?php echo (isset($privacy['raise']) && $privacy['raise'] == 4) ? 'selected' : ''; ?>>On Request</option>
            </select>
        </div>  
        </div>
    </div>
</div>
<div class="row margin-top-10">
    <form id="investee-info-form" action="<?php echo base_url(); ?>user/edit_investee_info_process" enctype="multipart/form-data" method="post" >
    <input type="hidden" name="user_id" value="<?php echo $investee_id; ?>" />
    <input type="hidden" name="investee_id" value="<?php echo $user_id; ?>" />
    <div id="Raise" class="accordion2">
        <div class="accordion-inner">
            <div class="form-group row">
                <div class="col-sm-12">
                    <input type="submit" name="raise_submit"  class="btn btn-eq-common pull-right" value="Save & Continue" />
                </div>
            </div> 
            <div class="form-group row">
                <label for="investment-required" class="col-sm-5 form-label">Investment Required *</label>
                <div class="col-sm-7">
                    <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                    <input  class="form-control form-input rupee-input" id="investment-required" name="investment_required" value="<?php echo isset($user['investment_required']) ? rupeeFormat($user['investment_required']) : ""; ?>" placeholder="Ex. 5,00,00,000">
                </div>
            </div>

            <div class="form-group row">
                <label for="commitment_per_investor" class="col-sm-5 form-label">Minimum Commitment Per User *</label>
                <div class="col-sm-7">
                    <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                    <input  class="form-control form-input rupee-input" id="commitment_per_investor" name="commitment_per_investor" value="<?php echo isset($user['commitment_per_investor']) ? rupeeFormat($user['commitment_per_investor']) : ""; ?>" placeholder="Ex. 5,00,00,000">
                </div>
            </div>

            <div class="form-group row">
                <label for="equity-offered" class="col-sm-5 form-label">Equity Offered</label>
                <div class="col-sm-7">

                    <input  class="form-control form-input" id="equity-offered" name="equity_offered" value="<?php echo isset($user['equity_offered']) ? $user['equity_offered'] : ""; ?>" maxlength="5" placeholder="">
                    <span class="col-sm-2 input-placeholder   pull-right">%</span>


                </div>
            </div>    

            <div class="form-group row">
                <label for="funding_history" class="col-sm-5 form-label">Prior Fund Raise History</label>
                <div class="col-sm-7">
                    <textarea class="form-control form-input form-area" id="funding_history" name="funding_history"  placeholder=""><?php echo isset($user['funding_history']) ? $user['funding_history'] : ""; ?></textarea>
                </div>
            </div>

            <fieldset >
                <legend >Use of Funds</legend>
                <div id="fund-uses">
                    <?php
                    $purposes_count = isset($purposes) ? count($purposes) : 4;    
                   
                    for ($i = 0; $i < $purposes_count; $i++) {

                     ?>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <input type="text" name="purpose[]" value="<?php echo isset($purposes[$i]['purpose']) ? $purposes[$i]['purpose'] : ""; ?>" class="form-control form-input"  placeholder="Purpose">
                        </div>
                        <div class="col-sm-6">
                            <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                            <input type="text" name="purpose_amount[]" value="<?php echo (isset($purposes[$i]['amount']) && $purposes[$i]['amount']!='0') ? rupeeFormat($purposes[$i]['amount']) : ""; ?>"  class="form-control form-input rupee-input"  placeholder="Amount Ex. 1000000 ">
                            
                        </div>
                    </div>
                <?php
                    }  ?>
                <?php $j = 4 - $i;
                for ($i = 1; $i <= $j; $i++) {
                ?>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <input type="text" name="purpose[]" class="form-control form-input"  placeholder="Purpose">
                        </div>
                        <div class="col-sm-6">
                            <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                            <input type="text" name="purpose_amount[]" class="form-control form-input rupee-input"  placeholder="Amount Ex. 1000000 ">
                            
                        </div>
                    </div>
                <?php } ?>
                </div>
                <!--<div class="btn eq-btn col-sm-12 pull-right add-purpose-btn">Add More</div>-->
            </fieldset>
            <div class="form-group row">
                <div class="col-sm-12">
                    <input type="submit" name="raise_submit"  class="btn btn-eq-common pull-right" value="Save & Continue" />
                </div>
            </div> 
            
        </div>
    </div>
</div>
<!-- End Raise panel -->