<div class="container">
  <div class="row">
    <div class="col-sm-5 block-title" style="margin-left:10px; margin-bottom:10px;">
      <h2>Investors From Your Sector</h2>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <?php $max = count($investors);
        if ($max) {
    ?>     
    <div class="slider-markup col-sm-12 ">
        <?php
        $count = 0;
        foreach ($investors as $user) { ?>
            <?php $data['user'] = $user;
            $this->load->view('front-end/investor_single_box', $data);
            $count++; ?>
       <?php }
        ?>        
    </div>
    <?php } else { ?>
        <div class="row content-box">
            <div>
                <h2>No one is from your sector.</h2>
            </div>
        </div>    
    <?php } ?>
  </div>
</div>
