<div class="container">
    
      <h1 class="page-title">Notifications</h1>
       

<div class="content-box">
<?php if (count($notifications) > 0) { ?>
<div class="notifications_p row">
    <?php foreach($notifications as $notification){ ?>
        <div class="notification col-xs-12">
            <span class="notification-thumb img-notification">
                <?php if (!empty($notification['image'])) { ?>
                    <img class="img-responsive" src="<?php echo base_url(); ?>uploads/users/<?php echo $notification['sender_id'] ?>/<?php echo $notification['image']; ?>">
                <?php } else { ?>
                    <img class="img-responsive" src="<?php echo base_url(); ?>uploads/investor.jpg" >
                <?php } ?>
            </span> 
            <span class="notification-text"><a href = "../<?php echo $notification['link'] ?>" target="_blank"><?php echo $notification['display_text']; ?></a></span><br>
            <span class="notification-time"><?php echo date('M, d H:i A',strtotime($notification['date_created'])); ?></span>
			<?php if($notification['type'] == 'access' || $notification['type'] == 'meeting'){ ?>
			
			<div class="notification-action-btns">
				<div class="small-msg" style="display:none;"></div>
				<?php if($notification['type'] == 'access'){ ?>
					<span class="btn btn-primary access-accept" data-notification-id="<?php echo $notification['id']; ?>" data-record-id="<?php echo $notification['record_id']; ?>">Accept</span>
					<span class="btn btn-default access-reject" data-notification-id="<?php echo $notification['id']; ?>" data-record-id="<?php echo $notification['record_id']; ?>">Reject</span>
				<?php } ?>
				
				<?php if($notification['type'] == 'meeting'){ ?>
				   
				   <span class="btn btn-primary meeting-accept" data-notification-id="<?php echo $notification['id']; ?>" data-record-id="<?php echo $notification['record_id']; ?>">Accept</span>
				   <span class="btn btn-default meeting-reject" data-notification-id="<?php echo $notification['id']; ?>" data-record-id="<?php echo $notification['record_id']; ?>">Reject</span>
				   <a class="view-msg-link" data-record-id="<?php echo $notification['record_id']; ?>" href="javascript:void(0);">View Message</a>
				<?php } ?>
				
			</div>
			<?php } ?>

			<span class="border"></span>
        </div>
    <?php } ?>
</div>        
<?php } else {?>
<div class="notifications">
    <div class="notification">
        <span class="notification-text">No notifications yet</span>
        <span class="border"></span>
    </div>
</div>        
<?php } ?>
</div>
<div style="display: none;">
    <div id="reject-remark" class="container pop-content">
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <h3 class="form-header top-margin-10">Reject Remark</h3>
            </div>
        </div>
        <div class="row">
            <form id="reject-remark-form">
                <div class="col-xs-12 col-sm-12">
                    <div class="row">
                    <div class="col-sm-12">
                        <textarea name="reject_remark" id="reject_remark" class="form-control form-area" placeholder="Reject Remark" rows="10"></textarea>
                        <input type="hidden" name="meeting_id" class="meeting-id" value="" />
                        <input type="hidden" name="notification_id" class="notification-id" value="" />
                    </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                        <label id="reject_error" for="query" class="error" style="float:left;">&nbsp;</label>
                        <input class="eq-btn pull-right" type="submit" value="OK" />
                        </div> 
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
<script>
    $(document).ready(function(){
        $('.access-accept').on('click',function(){
            var record_id = $(this).attr('data-record-id');
            var notification_id = $(this).attr('data-notification-id');
            var notification = $(this).parents('.notification-action-btns');
            $.ajax({
                url: "<?php echo base_url(); ?>investee/accept_access_request",
                type: 'POST',
                data : {recordId : record_id,notificationId : notification_id}
            })
            .done(function( data ) {
                 if( data.trim() == 'success'){
                   
                    notification.remove();
                }else{
                    
                }
            });
        });
        
        $('.meeting-accept').on('click',function(){
            var record_id = $(this).attr('data-record-id');
            var notification_id = $(this).attr('data-notification-id');
            var notification = $(this).parents('.notification-action-btns');
            $.ajax({
                url: "<?php echo base_url(); ?>investee/accept_meeting_request",
                type: 'POST',
                data : {recordId : record_id,notificationId : notification_id}
            })
            .done(function( data ) {
                 if( data.trim() == 'success'){
                    notification.remove();
                }else{
                    
                }
            });
        });
        
        $('.access-reject').on('click',function(){
            var record_id = $(this).attr('data-record-id');
            var notification_id = $(this).attr('data-notification-id');
            var notification = $(this).parents('.notification-action-btns');
            $.ajax({
                url: "<?php echo base_url(); ?>investee/reject_access_request",
                type: 'POST',
                data : {recordId : record_id,notificationId : notification_id}
            })
            .done(function( data ) {
                 if( data.trim() == 'success'){
                    notification.remove();
                }else{
                    
                }
            });
        });
        
        $('.meeting-reject').on('click',function(){
            var record_id = $(this).attr('data-record-id');
            var notification_id = $(this).attr('data-notification-id');
            $('#reject-remark-form .meeting-id').val(record_id);
            $('#reject-remark-form .notification-id').val(notification_id);
            $.colorbox({inline: true, href: '#reject-remark', innerWidth: '80%', maxWidth: '450px'});//, innerHeight: '335px'
        });
        
        $("#reject-remark-form").on('submit', (function(e) {
                    e.preventDefault();
                    $.ajax({
                        url: "<?php echo base_url(); ?>investee/reject_meeting_request",
                        type: 'POST',
                        data: new FormData(this),
                        contentType: false,
                        cache: false,
                        processData: false
                    }).done(function(data) {
                        if (data.trim() == "success") {
                            $.colorbox.close();    
                            location.reload();
                        } else {
                            $("#reject_error").text(data);
                        }

                        
                        
                    });

                }));
        
//        $('.meeting-reject').on('click',function(){
//            var record_id = $(this).attr('data-record-id');
//            var notification_id = $(this).attr('data-notification-id');
//            var notification = $(this).parents('.list-group-item');
//            $.ajax({
//                url: "<?php echo base_url(); ?>investee/reject_meeting_request",
//                type: 'POST',
//                data : {recordId : record_id,notificationId : notification_id}
//            })
//            .done(function( data ) {
//                 if( data == 'success'){
//                    notification.remove();
//                }else{
//                    
//                }
//            });
//        });
        
        $('.view-msg-link').on('click',function(){
            var record_id = $(this).attr('data-record-id');
            var notification = $(this).parents('.notification-action-btns');
            $.ajax({
                url: "<?php echo base_url(); ?>investor/meeting_msg",
                type: 'POST',
                dataType: 'html',
                data : {recordId : record_id}
            })
            .done(function( data ) {
                 if( data != ' '){
                     notification.find('.small-msg').toggle();
                    notification.find('.small-msg').html(data);
                }
            });
        });
    });
</script>    