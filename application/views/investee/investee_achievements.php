<!-- Start Financial panel -->
<div class="row">
    <div class="col-xs-4 block-title">
        <h2>Achievements</h2>
    </div>
    <div class="col-xs-offset-2 col-xs-6 block-title">
        <span class="glyphicon glyphicon-cog privacy-icon"></span>
        <div class="privacy-options">
        <div class="input-group">
                    <span class="input-group-addon">Visibility:</span>
            <select name="achievements" class="form-control form-input">
                <option value="0" <?php echo (isset($privacy['achievements']) && $privacy['achievements'] == 0) ? 'selected' : ''; ?>>All</option>
                <option value="1" <?php echo (isset($privacy['achievements']) && $privacy['achievements'] == 1) ? 'selected' : ''; ?>>All Investors & Partners</option>
                <option  value="2" <?php echo (isset($privacy['achievements']) && $privacy['achievements'] == 2) ? 'selected' : ''; ?>>Pledged Investors</option>
                <option  value="3" <?php echo (isset($privacy['achievements']) && $privacy['achievements'] == 3) ? 'selected' : ''; ?>>Followers</option>
                <option  value="4" <?php echo (isset($privacy['achievements']) && $privacy['achievements'] == 4) ? 'selected' : ''; ?>>On Request</option>
            </select>
            </div>
        </div>
    </div>
</div>
<div class="row margin-top-10">
    <form id="investee-info-form" action="<?php echo base_url(); ?>user/edit_investee_info_process" enctype="multipart/form-data" method="post" >
    <input type="hidden" name="investee_id" value="<?php echo $user_id; ?>" />
                <div id="Achievements" class="accordion2">
                    <div class="accordion-inner">
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <input type="submit" class="btn btn-eq-common pull-right" name="achievements_submit" value="Save & Continue"/>
                            </div>
                        </div>                        

                        <div class="form-group row">
                            <label for="awards" class="col-sm-5 form-label">Awards/Recognition</label>
                            <div class="col-sm-7">
                                <textarea class="form-control form-input form-area" cols="50" rows="15" id="awards" name="awards" placeholder=""><?php echo isset($user['awards']) ? $user['awards'] : ''; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="testimonials" class="col-sm-5 form-label">Testimonials</label>
                            <div class="col-sm-7">
                            	<textarea class="form-control form-input form-area" cols="50" rows="15" id="testimonials" name="testimonials" placeholder=""><?php echo isset($user['testimonials']) ? $user['testimonials'] : ''; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="media" class="col-sm-5 form-label">Media Coverage</label>
                            <div class="col-sm-7">
                                <textarea class="form-control form-input form-area" cols="50" rows="15" id="media" name="media" placeholder=""><?php echo isset($user['media']) ? $user['media'] : ''; ?></textarea>
                            </div>
                        </div>
                         <div class="form-group row">
                            <div class="col-sm-12">
                                <input type="submit" class="btn btn-eq-common pull-right" name="achievements_submit" value="Save & Continue"/>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
            <!-- End Financial panel -->