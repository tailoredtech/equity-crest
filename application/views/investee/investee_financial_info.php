<div class="row">        
    <div class="col-xs-6 block-title">
      <h2>Financial Information</h2>
    </div>
    <div class="col-xs-6 block-title">
      <span class="glyphicon glyphicon-cog privacy-icon"></span>
      <div class="privacy-options">
        <div class="input-group">
            <span class="input-group-addon">Visibility:</span>
            <select name="financial_info" class="form-control form-input" >
                <option value="0" <?php echo (isset($privacy['financial_info']) && $privacy['financial_info'] == 0) ? 'selected' : ''; ?>>All</option>
                <option value="1" <?php echo (isset($privacy['financial_info']) && $privacy['financial_info'] == 1) ? 'selected' : ''; ?>>All Investors & Partners</option>
                <option  value="2" <?php echo (isset($privacy['financial_info']) && $privacy['financial_info'] == 2) ? 'selected' : ''; ?>>Pledged Investors</option>
                <option  value="3" <?php echo (isset($privacy['financial_info']) && $privacy['financial_info'] == 3) ? 'selected' : ''; ?>>Followers</option>
                <option  value="4" <?php echo (isset($privacy['financial_info']) && $privacy['financial_info'] == 4) ? 'selected' : ''; ?>>On Request</option>
            </select>
        </div>  
      </div>
    </div>
</div> 
<div class="row margin-top-10">       
        <form id="investee-info-form" action="<?php echo base_url(); ?>user/edit_investee_info_process" enctype="multipart/form-data" method="post" >
            <input type="hidden" name="user_id" value="<?php echo $investee_id; ?>" />
			<input type="hidden" name="investee_id" value="<?php echo $user_id; ?>" />
        <!--Slider Accordion Starts here-->
            <!-- Start Financial Info panel -->
                <div id="financial-info" class="accordion2">
                    <div class="accordion-inner">
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <input type="submit" name="financial_info_submit"  class="btn btn-eq-common pull-right" value="Save & Continue" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="financial_forecast" class="col-sm-5 form-label">Financial Forecast</label>
                            <div class="col-sm-7" id="financial_forecast-img">
                            <?php if(!isset($user['financial_forecast']) || $user['financial_forecast']==''){ ?>
                                <div class="fileinput fileinput-new uploader input-group full-width" data-provides="fileinput">
                                    <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput">
                                    <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                                    <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" id="financial_forecast" name="financial_forecast"></span>
                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                                </div>
                            <?php }else{
                                $extension = pathinfo($user['financial_forecast'], PATHINFO_EXTENSION);
                                switch($extension)
                                {
                                    case 'pdf':
                                        $file = 'assets/images/pdf_icon.gif';
                                        $link = end(explode('/', $user['financial_forecast']));
                                        break;
                                    case 'doc':
                                    case 'docx':
                                        $file = 'assets/images/doc_icon.gif';
                                        $link = end(explode('/', $user['financial_forecast']));
                                        break;
                                    case 'ppt':
                                    case 'pptx':
                                        $file = 'assets/images/ppt_icon.gif';
                                        $link = end(explode('/', $user['financial_forecast']));
                                        break;
                                    case 'xls':
                                    case 'xlsx':
                                        $file = 'assets/images/xsl_icon.gif';
                                        $link = end(explode('/', $user['financial_forecast']));
                                        break;
                                    default:
                                        $file = 'uploads/users/'.$user_id.'/'.$user['financial_forecast'];
                                        $link = '';
                                }
                            ?>
                               <img src="<?php echo base_url().$file ?>" height="100px" width="150px">                            
                               <a  href="javascript:void(0)" data-toggle="modal" data-target="#financial_forecastimgModal" id="change-financial-forecast">Change</a>
                               &nbsp;&nbsp;<?php echo $link;?>
                            <?php } ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="financial_statement" class="col-sm-5 form-label">Financial Statements</label>
                            <div class="col-sm-7" id="financial_statement-img">
                            <?php if(!isset($user['financial_statement']) || $user['financial_statement']==''){ ?>
                                <div class="fileinput fileinput-new uploader input-group full-width" data-provides="fileinput">
                                    <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput">
                                    <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                                    <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" id="financial_statement" name="financial_statement"></span>
                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                                </div>
                            <?php }else{ 
                                $extension = pathinfo($user['financial_statement'], PATHINFO_EXTENSION);
                                switch($extension)
                                {
                                    case 'pdf':
                                        $file = 'assets/images/pdf_icon.gif';
                                        $link = end(explode('/', $user['financial_statement']));
                                        break;
                                    case 'doc':
                                    case 'docx':
                                        $file = 'assets/images/doc_icon.gif';
                                        $link = end(explode('/', $user['financial_statement']));
                                        break;
                                    case 'ppt':
                                    case 'pptx':
                                        $file = 'assets/images/ppt_icon.gif';
                                        $link = end(explode('/', $user['financial_statement']));
                                        break;
                                    case 'xls':
                                    case 'xlsx':
                                        $file = 'assets/images/xsl_icon.gif';
                                        $link = end(explode('/', $user['financial_statement']));
                                        break;
                                    default:
                                        $file = 'uploads/users/'.$user_id.'/'.$user['financial_forecast'];
                                        $link = '';
                                }
                                ?>
                               <img src="<?php echo base_url().$file; ?>" height="100px" width="150px">                            
                               <a  href="javascript:void(0)" data-toggle="modal" data-target="#financial_statementimgModal" id="change-financial-statement">Change</a>
                               &nbsp;&nbsp;<?php echo $link;?>
                            <?php } ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="other" class="col-sm-5 form-label">Others (if any)</label>
                            <div class="col-sm-7" id="other-img">
                            <?php if(!isset($user['other']) || $user['other']==''){ ?>
                                <div class="fileinput fileinput-new uploader input-group full-width" data-provides="fileinput">
                                    <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput">
                                    <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                                    <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" id="other" name="other"></span>
                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                                </div>
                            <?php }else{ 
                                $extension = pathinfo($user['other'], PATHINFO_EXTENSION);
                                switch($extension)
                                {
                                    case 'pdf':
                                        $file = 'assets/images/pdf_icon.gif';
                                        $link = end(explode('/', $user['other']));
                                        break;
                                    case 'doc':
                                    case 'docx':
                                        $file = 'assets/images/doc_icon.gif';
                                        $link = end(explode('/', $user['other']));
                                        break;
                                    case 'ppt':
                                    case 'pptx':
                                        $file = 'assets/images/ppt_icon.gif';
                                        $link = end(explode('/', $user['other']));
                                        break;
                                    case 'xls':
                                    case 'xlsx':
                                        $file = 'assets/images/xsl_icon.gif';
                                        $link = end(explode('/', $user['other']));
                                        break;
                                    default:
                                        $file = 'uploads/users/'.$user_id.'/'.$user['other'];
                                        $link = '';
                                }
                                ?>
                               <img src="<?php echo base_url().$file; ?>" height="100px" width="150px">                            
                               <a  href="javascript:void(0)" data-toggle="modal" data-target="#otherimgModal" id="change-other">Change</a>
                               &nbsp;&nbsp;<?php echo $link;?>
                            <?php } ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <input type="submit" name="financial_info_submit"  class="btn btn-eq-common pull-right" value="Save & Continue" />
                            </div>
                        </div>
                    </div>
                </div>
            <!-- End Company Info panel -->
      </form>
    </div>
  