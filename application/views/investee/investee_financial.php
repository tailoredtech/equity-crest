 <!-- Start Financial panel -->
<div class="row">
    <div class="col-xs-6 block-title">
        <h2>Monthly Financial Indicators</h2>
    </div>
    <div class="col-xs-6">
        <span class="glyphicon glyphicon-cog privacy-icon"></span>
        <div class="privacy-options">
        <div class="input-group">
                    <span class="input-group-addon">Visibility:</span>
            <select name="financial" class="form-control form-input">
                <option value="0" <?php echo (isset($privacy['financial']) && $privacy['financial'] == 0) ? 'selected' : ''; ?>>All</option>
                <option value="1" <?php echo (isset($privacy['financial']) && $privacy['financial'] == 1) ? 'selected' : ''; ?>>All Investors & Partners</option>
                <option  value="2" <?php echo (isset($privacy['financial']) && $privacy['financial'] == 2) ? 'selected' : ''; ?>>Pledged Investors</option>
                <option  value="3" <?php echo (isset($privacy['financial']) && $privacy['financial'] == 3) ? 'selected' : ''; ?>>Followers</option>
                <option  value="4" <?php echo (isset($privacy['financial']) && $privacy['financial'] == 4) ? 'selected' : ''; ?>>On Request</option>
            </select>
            </div>
        </div>  
    </div>
</div>
<div class="row margin-top-10">
    <form id="investee-info-form" action="<?php echo base_url(); ?>user/edit_investee_info_process" enctype="multipart/form-data" method="post" >
    <input type="hidden" name="user_id" value="<?php echo $investee_id; ?>" />
    <input type="hidden" name="investee_id" value="<?php echo $user_id; ?>" />
    <div id="Financials" class="accordion2">
        <div class="accordion-inner">
                <div class="form-group row">
                    <div class="col-sm-12">
                        <input type="submit" class="btn btn-eq-common pull-right" name="financial_submit" value="Save & Continue"/>
                    </div>
                </div>                      
                        <div class="form-group row">
                            <label for="monthly-revenue" class="col-sm-5 form-label">Revenues</label>
                            <div class="col-sm-7">
                                <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                <input type="text" name="monthly_revenue" value="<?php echo isset($user['monthly_revenue']) ? rupeeFormat($user['monthly_revenue']) : ""; ?>"  class="form-control form-input rupee-input" id="monthly-revenue" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fixed-opex" class="col-sm-5 form-label">Fixed Cost (OPEX)</label>
                            <div class="col-sm-7">
                                <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                <input type="text" name="fixed_opex" value="<?php echo isset($user['fixed_opex']) ? rupeeFormat($user['fixed_opex']) : ""; ?>"  class="form-control form-input rupee-input" id="fixed-opex" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cash-burn" class="col-sm-5 form-label">Cash Burn</label>
                            <div class="col-sm-7">
                                <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                <input type="text" name="cash_burn" value="<?php echo isset($user['cash_burn']) ?  rupeeFormat($user['cash_burn']) : ""; ?>"  class="form-control form-input rupee-input" id="cash-burn" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="debt" class="col-sm-5 form-label">Debt</label>
                            <div class="col-sm-7">
                                <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                <input type="text" name="debt" value="<?php echo isset($user['debt']) ?  rupeeFormat($user['debt']) : ""; ?>"  class="form-control form-input rupee-input" id="debt" placeholder="">
                            </div>
                        </div>
                         <div class="form-group row">
                            <div class="col-sm-12">
                                <input type="submit" class="btn btn-eq-common pull-right" name="financial_submit" value="Save & Continue"/>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
            <!-- End Financial panel -->