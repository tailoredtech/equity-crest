    <div class="row">
        <div class="col-xs-4 block-title">
          <h2>Team Summary</h2>
        </div>
        <div class="col-xs-offset-2 col-xs-6">
            <span class="glyphicon glyphicon-cog privacy-icon"></span>
            <div class="privacy-options">
                <div class="input-group">
                    <span class="input-group-addon">Visibility:</span>
                <select name="team" class="form-control form-input">
                    <option value="0" <?php echo (isset($privacy['team']) && $privacy['team'] == 0) ? 'selected' : ''; ?>>All</option>
                    <option value="1" <?php echo (isset($privacy['team']) && $privacy['team'] == 1) ? 'selected' : ''; ?>>All Investors & Partners</option>
                    <option  value="2" <?php echo (isset($privacy['team']) && $privacy['team'] == 2) ? 'selected' : ''; ?>>Pledged Investors</option>
                    <option  value="3" <?php echo (isset($privacy['team']) && $privacy['team'] == 3) ? 'selected' : ''; ?>>Followers</option>
                    <option  value="4" <?php echo (isset($privacy['team']) && $privacy['team'] == 4) ? 'selected' : ''; ?>>On Request</option>
                </select>
                </div>
            </div>                      
        </div>
    </div>
    <div class="row margin-top-10">
        <form id="investee-info-form" action="<?php echo base_url(); ?>user/edit_investee_info_process" enctype="multipart/form-data" method="post" >
            <input type="hidden" name="user_id" value="<?php echo $investee_id; ?>" />
            <div class="accordion2">
                    <div class="accordion-inner" id="divTeamData">

            <div class="form-group row">
                <div class="col-sm-12">
                    <input type="submit" name="team_submit"  class="btn btn-eq-common pull-right" value="Save & Continue" />
                </div>
            </div>
        <!--Slider Accordion Starts here-->
            <!-- Start Company Info panel -->
                        <?php 
                        if(isset($investee_data) && !empty($investee_data))
                        {
                            $i = 0;
                        ?>
                            <script>
                                $( document ).ready(function() {
                                    window.i = 1;
                                });
                            </script>
                        <?php
                            foreach ($investee_data as $investee_data) 
                            {
                                $id_arr[] = $investee_data['id'];
                                if($i>0)
                                {
                                    ?>
                                    <script>
                                        $( document ).ready(function() {
                                            window.i = window.i + 1;
                                        });
                                    </script>
                                    <?php
                                }
                                ?>
                                <div id="addTeam<?php echo $i;?>">
                                <?php if($i>0){ ?><hr style="background-color: #ccc; height: 1.5px">
                                     <div class="form-group row text-right"><div class="col-xs-12"><a href="javascript:void(0)" title="Remove" onclick="removeTeam(<?php echo $i;?>)"><span aria-hidden="true" class="glyphicon glyphicon-trash"></span></a></div></div>
                                <?php } ?>

                                <div class="form-group row">
                                    <label for="name" class="col-sm-5 form-label">Name *</label>
                                    <div class="col-sm-7">
                                       <input type="text" name="name[]" value="<?php echo $investee_data['name']; ?>" class="form-control form-input" id="name" placeholder="Name" required="required">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="designation" class="col-sm-5 form-label">Designation/Role *</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="designation[]" value="<?php echo $investee_data['designation']; ?>" class="form-control form-input" id="designation" placeholder="Designation" required="required">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="experience" class="col-sm-5 form-label">Experience Summary *</label>
                                    <div class="col-sm-7">
                                        <textarea class="form-control form-input form-area" cols="50" rows="15" id="experience" name="experience[]" placeholder=""  required="required" ><?php echo isset($investee_data['experience']) ? $investee_data['experience'] : ''; ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="linkedin" class="col-sm-5 form-label">LinkedIn *</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="linkedin[]" value="<?php echo $investee_data['linkedin']; ?>" class="form-control form-input" id="linkedin" placeholder="LinkedIn" required="required">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="twitter" class="col-sm-5 form-label">Twitter</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="twitter[]" value="<?php echo $investee_data['twitter']; ?>" class="form-control form-input" id="twitter" placeholder="Twitter">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <input type="hidden" name="id[]" value="<?php echo $investee_data['id']; ?>" class="form-control form-input" id="id">
                                </div>
                                <?php if($i>0){ ?>
                               
                                <?php } ?>
                                </div>
                                <?php 
                                $i++;
                            }
                        }
                        else
                        {
                        ?>
                        <div id="addTeam0">
                            <div class="form-group row">
                                <label for="name" class="col-sm-5 form-label">Name</label>
                                <div class="col-sm-7">
                                   <input type="text" name="name[]" value="" class="form-control form-input" id="name" placeholder="Name" required="required">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="designation" class="col-sm-5 form-label">Designation/Role</label>
                                <div class="col-sm-7">
                                    <input type="text" name="designation[]" value="" class="form-control form-input" id="designation" placeholder="Designation" required="required">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="experience" class="col-sm-5 form-label">Experience Summary</label>
                                <div class="col-sm-7">
                                    <textarea class="form-control form-input form-area" cols="50" rows="15" id="experience" name="experience[]" placeholder="" required="required"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="linkedin" class="col-sm-5 form-label">LinkedIn</label>
                                <div class="col-sm-7">
                                    <input type="text" name="linkedin[]" value="" class="form-control form-input" id="linkedin" placeholder="LinkedIn" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="twitter" class="col-sm-5 form-label">Twitter</label>
                                <div class="col-sm-7">
                                    <input type="text" name="twitter[]" value="" class="form-control form-input" id="twitter" placeholder="Twitter">
                                </div>
                            </div>
                        </div>
                        <?php
                            }
                        ?>
                    </div>
                    <div class="form-group row">
                            <div class="col-sm-12">
                                <input type="submit" name="team_submit"  class="btn btn-eq-common pull-right" value="Save & Continue" />
                            </div>
                        </div>
                    <a href="javascript:void(0)" onclick="addTeam()">Add more</a> | <a href="javascript:void(0)" onclick="removeTeam()">Delete</a> 
                </div>
            </div>
            <!-- End Company Info panel -->
            <input type="hidden" name="investee_exist_ids" value="<?php echo isset($id_arr) && $id_arr != '' ? implode(',', $id_arr) : '';?>" /> 
        </div>
      </form>
    </div>
  

<script>
    function addTeam()
    {
        var appendBody = '<div id="addTeam'+window.i+'"><hr style="background-color: #ccc; height: 1.5px"><div class="form-group row text-right"><div class="col-xs-12"><a href="javascript:void(0)" title="Remove" onclick="removeTeam('+window.i+')"><span aria-hidden="true" class="glyphicon glyphicon-trash"></span></a></div></div><div class="form-group row"> <label for="name" class="col-sm-5 form-label">Name</label> <div class="col-sm-7"> <input type="text" name="name[]" value="" class="form-control form-input" id="name" placeholder="Name" required="required"> </div> </div> <div class="form-group row"> <label for="designation" class="col-sm-5 form-label">Designation/Role</label> <div class="col-sm-7"> <input type="text" name="designation[]" value="" class="form-control form-input" id="designation" placeholder="Designation" required="required"> </div> </div> <div class="form-group row"> <label for="experience" class="col-sm-5 form-label">Experience Summary</label> <div class="col-sm-7"> <textarea class="form-control form-input form-area" cols="50" rows="15" id="experience" name="experience[]" placeholder="" required="required"></textarea> </div> </div> <div class="form-group row"> <label for="linkedin" class="col-sm-5 form-label">LinkedIn</label> <div class="col-sm-7"> <input type="text" name="linkedin[]" value="" class="form-control form-input" id="linkedin" placeholder="LinkedIn" required="required"> </div> </div> <div class="form-group row"> <label for="twitter" class="col-sm-5 form-label">Twitter</label> <div class="col-sm-7"> <input type="text" name="twitter[]" value="" class="form-control form-input" id="twitter" placeholder="Twitter"> </div> </div></div>';
        $( "#divTeamData" ).append(appendBody);
        window.i = window.i + 1;
    }

    function removeTeam(id)
    {
        if(window.i == 1)
        {
            alert('Aleast 1 Team member details are required!!');
        }
        else
        {
            //window.i = window.i - 1;
           // $('#addTeam'+window.i).remove(); 
           $('#addTeam'+id).remove(); 
        }
    }
</script>



