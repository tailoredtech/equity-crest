<div class="deal-single">
    <div class="deal-link">
        <div class="deal-logo text-center">
            <div class="deal-image">
	             <?php if($investee['banner_image']){ ?>   
	               <img src="<?php echo base_url().$investee['banner_image'];?>"> 
	               <?php }else{ ?>          
	               <img src="<?php echo base_url().'assets/images/company_default.jpg';?>">  
	               <?php } ?> 
            </div>
            <div class="deal-category "><?php echo $investee['sector']; ?></div>
        </div>
        <?php // print_r($investee['name']); ?>
        <?php if($investee['snapshot']){ ?>
            <div class="deal-middel-box">
                <ul class="deal-list-style">
                    <li>
                        <a href="#" data-toggle="modal" data-target="#companySnapshotModal">View company snapshot</a>
                        <?php
                        if($this->session->userdata('user_id') != ''){
                        ?>
                        <div class="downloadSnp">
                            <a href="#" data-toggle="modal" data-target="#myModal2" class="no_style">
                                <img src="<?php echo base_url().'assets/images/download_icon.png';?>" width="35">  
                            </a>
                        </div>
                        <?php } ?>
                    </li>
                    <!-- <li><a href="#">Schedule a meeting</a></li> -->
                </ul>

            </div>
            <?php } ?>

            <div class="deal-funds row remove_margin">
                <div class="fund-requested col-lg-12 col-md-12 col-sm-12 col-xs-12 remove_padding">
                    <div class="funds-req-amount pull-left">
                     &#8377; <?php echo format_money($investee['investment_required']); ?>               
                    </div>

                </div>

                <?php 
                 $fund_raise = $investee['fund_raise'];
                 $investment_required = $investee['investment_required'];
                 if($investment_required){
                     $percent = ($fund_raise/$investment_required)*100;
                 }else{
                     $percent = 0;
                 }
                 
                ?>          
                <div class="funds-bar col-lg-12 col-md-12 col-sm-12 col-xs-12 remove_padding">
                    <div class="progress" style="height: 6px;">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $percent; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $percent,'%'; ?>; background: rgb(0,166,50);"></div>
                        <span class="sr-only">60% Complete</span>
                    </div>
                </div>
                <div class="fund-stats col-lg-12 col-md-12 col-sm-12 col-xs-12 remove_padding">
                   
                    <div class="funds-raised middle col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <span><strong>&#8377; <?php echo format_money($investee['commitment_per_investor']); ?></strong></span>
                        <p>Min. Investment</p>
                    </div>

                    <div class="funds-raised col-lg-6 col-md-6 col-xs-6">
                        <div id="raised-percent"><strong><?php echo floor($percent); ?>%</strong></div>
                        <p>of funds raised</p>
                    </div>
                   
                </div>
            </div>
    </div>


    <?php if ($this->session->userdata('role') == 'channel_partner'){ ?>
        <div class="deal-footer-links">
            <div class="col-xs-6 text-center">&nbsp;</div>
            <!-- div class="col-xs-4 text-center">&nbsp;</div -->
            <div class="col-xs-6 text-center">&nbsp;</div>
        </div>
    <?php } elseif($this->session->userdata('role') == 'investor') { ?>
    <?php  ?>
    <div class="deal-footer-links">
        <?php 

        $followed = $this->session->userdata('followed'); 
        if(is_array($followed)){
         if(in_array($investee['user_id'],$followed)){ ?>
               <div class="col-xs-6 text-center"><a href="javascript:void(0);" class="text-center unfollow" data-user-id="<?php echo $investee['user_id']; ?>"><img src="<?php echo base_url(); ?>assets/images/fund_follow_active_40x40.png" alt="Unfollow">
                <h4>Unfollow</h4></a></div>
        <?php }else{ ?>
               <div class="col-xs-6 text-center"><a href="javascript:void(0);" class="text-center follow" data-user-id="<?php echo $investee['user_id']; ?>"><img src="<?php echo base_url(); ?>assets/images/fund_follow_40x40.png" alt="Follow">
                <h4>Follow</h4></a></div>
        <?php }  }else{ ?>  
            <div class="col-xs-6 text-center"><a href="javascript:void(0);" class="text-center follow" data-user-id="<?php echo $investee['user_id']; ?>"><img src="<?php echo base_url(); ?>assets/images/fund_follow_40x40.png" alt="Follow">
                <h4>Follow</h4></a></div>
            <?php } ?> 
            
        <!-- div class="col-xs-4 text-center"><a href="javascript:void(0);" class="text-center ignore" data-user-id="<?php echo $investee['user_id']; ?>">Ignore</a></div -->
        
        <?php $pledged = $this->session->userdata('pledged'); 
         if(is_array($pledged)){
                
         if(in_array($investee['user_id'],$pledged)){ ?>
            <div class="col-xs-6 text-center"><a href="javascript:void(0);" class="text-center pledged" data-user-id="<?php echo $investee['user_id']; ?>"><img src="<?php echo base_url(); ?>assets/images/fund_follow_pledge_active_40x40.png" alt="Pledged">
                <h4>Pledged</h4></a></div>
        <?php }else{ ?>
            <div class="col-xs-6 text-center"><a href="javascript:void(0);" class="text-center pledge" data-user-id="<?php echo $investee['user_id']; ?>"><img src="<?php echo base_url(); ?>assets/images/fund_follow_pledge_40x40.png" alt="Pledge">
                <h4>Pledge</h4></a></div>
        <?php }  }else{ ?>
            <div class="col-xs-6 text-center"><a href="javascript:void(0);" class="text-center pledge" data-user-id="<?php echo $investee['user_id']; ?>"><img src="<?php echo base_url(); ?>assets/images/fund_follow_pledge_40x40.png" alt="Pledge">
                <h4>Pledge</h4></a></div>
        <?php } ?>    
    </div>
    <?php } ?>


</div> <!-- /.deal-single -->



<div class="row">
    <div class="col-xs-12">
        <div class="content-infographic"> 
<!--
            <div class="item">
                <span class="stage-icon"></span>
                <div class="ci2-text"><?php echo $investee['stage']; ?></div>
                <div>Stage</div>
            </div>
            <div class="item">
                <span class="validity-icon"></span>
                <div class="ci2-text"><?php echo $investee['validity_period']; ?></div>
                <div>Validity period</div>
            </div>
-->
            <div class="item">
                <span class="founding-year-icon"></span>
                <div class="ci2-text"><?php echo $investee['year']; ?></div>
                <div>Founding Year</div>
            </div>
            <div class="item">
                <span class="sector-icon"></span>
                <div class="ci2-text"><?php echo $investee['sector']; ?></div>
                <div>Sector</div>
            </div>         
<!--
            <div class="item">
                <span class="business-type-icon"></span>
                <div class="ci2-text"><?php echo $investee['business']; ?></div>
                <div>Business Type</div>
            </div>
-->
            <div class="item">
                <span class="company-url-icon"></span>
                <div class="ci2-text"><?php echo anchor(prep_url($investee['company_url']),$investee['company_url'],'target="_blank"'); ?></div>
                <div>Company URL</div>
            </div>
<!--
            <div class="item">
                <span class="company-url-icon"></span>
                <div class="ci2-text"><?php echo anchor(prep_url($investee['product_url']),$investee['product_url'],'target="_blank"'); ?></div>
                <div>Product URL</div>
            </div>
-->
            <div class="item">
                <ul class="login-social-icon">
                    <?php if($investee['linkedin_url'] != '') { ?>
                        <li><a id="linkedin" href="<?php echo $investee['linkedin_url']; ?>">Linked-in</a></li>
                    <?php } ?>
<!--                         <li><a id="google" href="#">Google</a></li> -->
                    <?php if($investee['fb_url'] != '') { ?>
                        <li><a id="facebook" href="<?php echo $investee['fb_url']; ?>">Facebook</a></li>
                    <?php } ?>
                    <?php if($investee['twitter_handle'] != '') { ?>
                        <li><a id="twitter" href="<?php echo $investee['twitter_handle']; ?>">Twitter</a></li>
                     <?php } ?>
                </ul>
            </div>
            
            <?php if($investee['status'] != 'active'):?>
	            
	                <div class="semi_active col-lg-12 col-md-12 col-sm-12 col-xs-12 comingsoonstrip" style="line-height: 45px;">
	                    Coming Soon                  
	                </div>
	           
            <?php endif; ?>
            
        </div>
    </div>
</div>

<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <p class="text-center">
                Downloading the snapshot will send a notification to the founder/s informing them of the download. <br> <a href="<?php echo base_url().'investee/download_file?filepath='.urlencode($investee['snapshot']).'&investee_name='.$investee['name'].'&email='.$investee['email'].'&comp='.$investee['company_name'].'&type_doc=pdf';?>" >Click here</a> to confirm download of snapshot or <span class="closeBtn" data-dismiss="modal">cancel</span> to go back
            </p>
      </div>
    </div>
  </div>
</div>