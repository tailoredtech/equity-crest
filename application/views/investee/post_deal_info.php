<div class="main">
    <div class="container wrap">
        <div class="row">
            <h4 class="form-header col-lg-12 col-md-12 col-sm-12 col-xs-12 ">Post Deals</h4>
        </div>

        <form id="post-deal-info-form" action="<?php echo base_url(); ?>investee/post_deal_info_process" enctype="multipart/form-data" method="post" >
            <div class="container investor-register-form">
                <div class="row">
                    <div class="col-md-12 section-head">
                        <span class="glyphicon glyphicon-chevron-down dropicon"></span><span class="lato-bold ">Financial Updates</span>
                    </div>
                </div>
                <div class="container financial-update-info  col-sm-7 col-sm-offset-2">
                    <div class="add-financial-updates">
                        <div class="financial-update-box">
                            <div class="row">
                                <div class="form-group">
                                    <label  class="col-sm-4 form-label">File Name</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="financial_file_name_1" class="form-control form-input"  placeholder="File Name">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label  class="col-sm-4 form-label">File</label>
                                    <div class="col-sm-8">
                                        <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput">
                                            <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                                            <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file"  name="file_financial_1"></span>
                                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="financial-update-more-btn add-more-btn pull-right">Add More</div>
                </div>
<!--                <div class="row">
                    <div class="col-sm-6">
                        <input type="button" id="investment-next" class="save-btn pull-right" value="Save" />
                    </div>
                </div>-->
                <div class="row">
                    <div class="col-sm-12"><hr></div>
                </div>
                <div class="row">
                    <div class="col-md-12 section-head">
                        <span class="glyphicon glyphicon-chevron-down dropicon"></span><span class="lato-bold">Business Updates</span>
                    </div>
                </div>

                <div class="container col-sm-7 col-sm-offset-2">
                    <div class="add-business-updates">
                        <div class="business-update-box">
                            <div class="row">
                                <div class="form-group">
                                    <label  class="col-sm-4 form-label">Meeting Updates</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="business_file_name_1" class="form-control form-input"  placeholder="File Name">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label  class="col-sm-4 form-label">Meeting Updates File</label>
                                    <div class="col-sm-8">
                                        <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput">
                                            <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                                            <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file"  name="file_business_1"></span>
                                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label for="msg-to-investors" class="col-sm-4 form-label">Messages to Investors</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control form-input form-area" cols="50" rows="15" id="msg-to-investors" name="msg_1" placeholder=""></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                    <div class="businesss-update-more-btn add-more-btn pull-right">Add More</div>
                </div>
<!--                <div class="row">
                    <div class="col-sm-6">
                        <input type="button" id="portfolio-next" class="save-btn pull-right" value="Save" />
                    </div>
                </div>-->

                <div class="row">
                    <div class="col-sm-12"><hr></div>
                </div>
                <div class="row">
                    <div class="col-md-12 section-head">
                        <span class="glyphicon glyphicon-chevron-down dropicon"></span><span class="lato-bold ">Achievements</span>
                    </div>
                </div>
                <div class="container col-sm-7 col-sm-offset-2">
                    <div class="row">
                        <div class="form-group">
                            <label for="company_presentation" class="col-sm-4 form-label black lato-bold ">Company Presentation</label>
                            <div class="col-sm-8">
                                <textarea rows="30" cols="50" class="form-control form-input form-area" id="company_presentation" name="presentation" placeholder="Paste Slideshare Embed Code. Example: <iframe src='//www.slideshare.net/slideshow/embed_code/xxxxxx'></iframe>">
                                
                                </textarea>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="proposal-video" class="col-sm-4 form-label">Proposal Video</label>
                            <div class="col-sm-8">
                                <input type="text" name="proposal_video" value="" class="form-control form-input" id="proposal-video" placeholder="Youtube link">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="media" class="col-sm-4 form-label">Media</label>
                            <div class="col-sm-8">
                                <textarea class="form-control form-input form-area" cols="50" rows="15" id="media" name="media" placeholder=""></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="awards" class="col-sm-4 form-label">Awards / Recognition</label>
                            <div class="col-sm-8">
                                <textarea class="form-control form-input form-area" cols="50" rows="15" id="awards" name="awards" placeholder=""></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="testimonials" class="col-sm-4 form-label">Testimonials</label>
                            <div class="col-sm-8">
                                <textarea class="form-control form-input form-area" cols="50" rows="15" id="testimonials" name="testimonials" placeholder=""></textarea>
                            </div>
                        </div>
                    </div>


                </div> 
                <div class="row">
                    <div class="col-sm-6">
                        <input type="hidden" id="business_update_count" name="business_update_count" value="1" />
                        <input type="hidden" id="financial_update_count" name="financial_update_count" value="1" />
                        <input type="submit" class="save-btn pull-right" name="submit" value="Save" />
                    </div>
                </div>
            </div>      
        </form> 
    </div>
</div>
<div style="display: none;">
    <div id="business-update-box">
        <div class="business-update-box">
            <div class="row">
                <div class="form-group">
                    <label  class="col-sm-4 form-label">Meeting Updates</label>
                    <div class="col-sm-8">
                        <input type="text" name="business_file_name_1" class="form-control form-input"  placeholder="File Name">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label  class="col-sm-4 form-label">Meeting Updates File</label>
                    <div class="col-sm-8">
                        <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput">
                            <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                            <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file"  name="file_business_1"></span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label for="msg-to-investors" class="col-sm-4 form-label">Messages to Investors</label>
                    <div class="col-sm-8">
                        <textarea class="form-control form-input form-area" cols="50" rows="15" id="msg-to-investors" name="msg_1" placeholder=""></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="financial-update-box">
        <div class="financial-update-box">
            <div class="row">
                <div class="form-group">
                    <label  class="col-sm-4 form-label">File Name</label>
                    <div class="col-sm-8">
                        <input type="text" name="financial_file_name_1" class="form-control form-input"  placeholder="File Name">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label  class="col-sm-4 form-label">File</label>
                    <div class="col-sm-8">
                        <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput">
                            <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                            <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file"  name="file_financial_1"></span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('document').ready(function(){
        $('#post-deal-info-form textarea').htmlarea({
            toolbar: [
                ["bold", "italic", "underline"],
                ["p"],
                ["link", "unlink"],
                ["orderedList", "unorderedList"],
                ["indent", "outdent"],
                ["justifyleft", "justifycenter", "justifyright"]
            ]
        });
        var business_update_count = 1;
        var financial_update_count = 1;
        $('.businesss-update-more-btn').on('click',function(){
             ++business_update_count
            $('#business_update_count').val(business_update_count);
            $('#business-update-box').find('input[name*="business_file_name"]').attr('name','business_file_name_'+business_update_count);
            $('#business-update-box').find('input[name*="file_business"]').attr('name','file_business_'+business_update_count);
            $('#business-update-box').find('textarea[name*="msg"]').attr('name','msg_'+business_update_count);
            $('.add-business-updates').append($('#business-update-box').html());
            $("#post-deal-info-form textarea").htmlarea("dispose");
            $('#post-deal-info-form textarea').htmlarea({
                toolbar: [
                    ["bold", "italic", "underline"],
                    ["p"],
                    ["link", "unlink"],
                    ["orderedList", "unorderedList"],
                    ["indent", "outdent"],
                    ["justifyleft", "justifycenter", "justifyright"]
                ]
            });
        });
        
        $('.financial-update-more-btn').on('click',function(){
            ++financial_update_count
            $('#financial_update_count').val(financial_update_count);
            $('#financial-update-box').find('input[name*="financial_file_name"]').attr('name','financial_file_name_'+financial_update_count);
            $('#financial-update-box').find('input[name*="file_financial"]').attr('name','file_financial_'+financial_update_count);
            $('.add-financial-updates').append($('#financial-update-box').html());
        });
    });
</script>