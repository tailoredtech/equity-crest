<div class="container">
  <div class="row">
    <div class="col-xs-6 col-sm-8 block-title">
      <h2>Similar Companies</h2>
    </div>
    <div class="col-xs-6 col-sm-4 block-title pull-right">
      <h2>
        <nav class="slidernav">
        <?php $max = count($users);
        if ($max > 0) { ?>
          <div id="navbtns" name="navbtns" class="clearfix">
            <a href="#" title="Previous" class="previous"><img src="<?php echo base_url(); ?>assets/images/slick-prev.png"></img></a>
            <a href="#" title="Next" class="next"><img src="<?php echo base_url(); ?>assets/images/slick-next.png"></img></a>
          </div>
        <?php } ?>  
        </nav>
      </h2>
    </div>
  </div>
</div>

        <?php $max = count($users);
        if ($max) { ?>  
        <div class="container">
        <div class="row">
            <div id="w">
                <div id="crsl-items" name="crsl-items" class="crsl-items" data-navigation="navbtns">
                    <div class="crsl-wrap">
                        <?php
                        $count = 0;
                        foreach ($users as $user) {
                            $data['user'] = $user;
                            $this->load->view('front-end/grid-single-item', $data);
                            $count++;
                        }
                        ?>
                    </div><!-- @end .crsl-wrap -->
                </div><!-- @end .crsl-items -->
            </div>
        </div>
        </div>
        <?php } else { ?>
        <div class="row content-box">    
            <div>
                <h2>There are no similar companies available.</h2>
            </div>
        </div>    
        <?php } ?>

