<!-- Start Financial panel -->
<div class="row">
    <div class="col-xs-4 block-title">
        <h2>Updates</h2>
    </div>
    <div class="col-xs-offset-2 col-xs-6 block-title">
        <span class="glyphicon glyphicon-cog privacy-icon"></span>
        <div class="privacy-options">
        <div class="input-group">
                    <span class="input-group-addon">Visibility:</span>
            <select name="updates" class="form-control form-input">
                <option value="0" <?php echo (isset($privacy['updates']) && $privacy['updates'] == 0) ? 'selected' : ''; ?>>All</option>
                <option value="1" <?php echo (isset($privacy['updates']) && $privacy['updates'] == 1) ? 'selected' : ''; ?>>All Investors & Partners</option>
                <option  value="2" <?php echo (isset($privacy['updates']) && $privacy['updates'] == 2) ? 'selected' : ''; ?>>Pledged Investors</option>
                <option  value="3" <?php echo (isset($privacy['updates']) && $privacy['updates'] == 3) ? 'selected' : ''; ?>>Followers</option>
                <option  value="4" <?php echo (isset($privacy['updates']) && $privacy['updates'] == 4) ? 'selected' : ''; ?>>On Request</option>
            </select>
            </div>
        </div>
    </div>
</div>
<div class="row margin-top-10">
    <form id="investee-info-form" action="<?php echo base_url(); ?>user/edit_investee_info_process" enctype="multipart/form-data" method="post" >
    <input type="hidden" name="investee_id" value="<?php echo $user_id; ?>" />
                <div id="Updates" class="accordion2">
                    <div class="accordion-inner">
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <input type="submit" class="btn btn-eq-common pull-right" name="updates_submit" value="Save & Continue"/>
                            </div>
                        </div>                        

                        <div class="form-group row">
                            <label for="updates" class="col-sm-5 form-label">Updates</label>
                            <div class="col-sm-7">
                                <textarea class="form-control form-input form-area" cols="50" rows="15" id="updates" name="updates" placeholder=""><?php echo isset($user['updates']) ? $user['updates'] : ''; ?></textarea>
                            </div>
                        </div>
                         <div class="form-group row">
                            <div class="col-sm-12">
                                <input type="submit" class="btn btn-eq-common pull-right" name="updates_submit" value="Save & Continue"/>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
            <!-- End Financial panel -->