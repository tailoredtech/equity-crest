<div class="main">
    <div class="container">
        <div class="row">
            <h4 class="form-header  col-xs-12 ">Company Profile</h4>
        </div>
        <div class="content-box row">
            <div class="profile-basic col-sm-12">
                <div class="row">
                    <div class="company-logo">
                        <div class="company-image company-border">
                            <img src="<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id'] ?>/<?php echo $investee['image']; ?>" width="77" height="77" />
                        </div>
                    </div>
                    <div class="company-title">
                        <div class="company-name">
                            <?php echo $investee['name']; ?>
                        </div>
                        <div class="company-location">
                            <?php echo $investee['city']; ?>
                        </div>
                        <div class="company-rating">
                            <img src="<?php echo base_url(); ?>assets/images/rating.jpg">
                        </div>
                    </div>
                    <div class="company-investment">
                        <div class="company-border"> Investment Required</div>
                        <div class="lato-bold company-border"><img style="height: 11px;margin-top: -2px;" src='<?php echo base_url(); ?>assets/images/rupee.png' /> <?php  echo format_money($investee['investment_required']); ?></div>
                    </div>
                    <div class="company-offered">
                        <div class="company-border"> Stage</div>
                        <div class="lato-bold company-border"><?php echo $investee['stage']; ?>%</div>
                    </div>
                    <div class="company-validity">
                        <div class="company-border"> Validity Period</div>
                        <div class="lato-bold company-border"><?php echo $investee['validity_period']; ?></div>
                    </div>
                    <div class="company-commit">
                        <div> Min. Commitment Per Investor</div>
                        <div class="lato-bold"><img style="height: 11px;margin-top: -2px;" src='<?php echo base_url(); ?>assets/images/rupee.png' /> <?php echo format_money($investee['commitment_per_investor']); ?></div>
                    </div>
                </div>
            </div>

<!--            <div class="profile-detail col-sm-12">
                <div class="row">
                    <div class="col-sm-2 profile-item company-border">
                        <div> Stage</div>
                        <div class="lato-bold"><?php echo $investee['stage']; ?></div>
                    </div>
                    <div class="col-sm-2 profile-item company-border">
                        <div> Founding Year</div>
                        <div class="lato-bold"><?php echo $investee['year']; ?></div>
                    </div>
                    <div class="col-sm-2 profile-item company-border">
                        <div> Sector</div>
                        <div class="lato-bold"><?php echo $investee['sector']; ?></div>
                    </div>
                    <div class="col-sm-2 profile-item company-border">
                        <div> Type of Business</div>
                        <div class="lato-bold"><?php echo $investee['business']; ?></div>
                    </div>
                    <div class="col-sm-2 profile-item company-border">
                        <div> Comapany URL</div>
                        <div class="lato-bold"><?php echo $investee['company_url']; ?></div>
                    </div>
                    <div class="col-sm-2 profile-item">
                        <div> Product URL</div>
                        <div class="lato-bold"><?php echo $investee['product_url']; ?></div>
                    </div>
                </div>
            </div>-->

            <!-- LEFT COLUMN -->
            <?php $this->load->view('investee/profile-left',$investee);?>
            <!-- LEFT COLUMN END -->

            <div class="profile-tabs col-sm-12">
                <div class="row">
                    <ul class="nav" role="tablist">
                        <li>
                            <div class="col-sm-offset-3 col-sm-1">
                                <a href="#team" role="tab" data-toggle="tab">Team</a>
                            </div>
                        </li>
                        <li>
                            <div class="col-sm-1">
                                <a href="#product" role="=tab" data-toggle="tab">Business</a>
                            </div>
                        </li>
                        <li>
                            <div class="col-sm-2" style="text-align: center;">
                                <a href="#customer" role="=tab" data-toggle="tab" >Monetization</a>
                            </div>
                        </li>
                        <li>
                            <div class="col-sm-1">
                                <a href="#market" role="=tab" data-toggle="tab">Market</a>
                            </div>
                        </li>
                        <li>
                            <div class="col-sm-1" >
                                <a href="#funding" role="=tab" data-toggle="tab">Raise</a>
                            </div>
                        </li>
                        <li>
                            <div class="col-sm-1">
                                <a href="#finance" role="=tab" data-toggle="tab">Financial</a>
                            </div>
                        </li>
                        <li>
                            <div class="col-sm-1">
                                <a href="#achievement" role="=tab" data-toggle="tab">Achievements</a>
                            </div>
                        </li>
                        
                    </ul>
                </div>
            </div>

            <div class="tab-content">
                <div class="profile-content col-sm-9 tab-pane active" id="team">
                    <div class="row">
                        <div class="col-sm-8">

                            <div class="content-text">
                                <h4 class="lato-bold">Team Summary</h4>
                                <p>
                                    <?php echo strip_tags($investee['team_summary'], '<p><i><br><b><div><table><td><tr><th><ul><ol><li>'); ?>
                                </p>

                            </div>
                            

                        </div>
                        <!-- RIGHT COLUMN -->
                        <?php $this->load->view('investee/right-panel',$investee);?>
                        <!-- RIGHT COLUMN END -->

                    </div>
                </div>


                <div class="profile-content col-sm-9 tab-pane" id="product">
                    <div class="row">
                        <div class="col-sm-8">

                            <div class="content-text">
                                <h4 class="lato-bold">Product/Service Description</h4>
                                <p>

                                   <?php echo strip_tags($investee['products_services'], '<p><i><br><b><div><table><td><tr><th><ul><ol><li>'); ?>
                                </p>

                            </div>
                            <div class="content-text">
                                <h4 class="lato-bold">Unique Selling Proposition</h4>
                                <p>

                                    <?php echo nl2br(strip_tags($investee['how_different'], '<p><i><br><b><div><table><td><tr><th><ul><ol><li>');); ?>
                                </p>
                            </div>
                            
                        </div>
                        <!-- RIGHT COLUMN -->
                        <?php $this->load->view('investee/right-panel',$investee);?>
                        <!-- RIGHT COLUMN END -->
                    </div>
                </div>
                
                
                <div class="profile-content col-sm-9 tab-pane" id="customer">
                    <div class="row">
                        <div class="col-sm-8">
                           <div class="content-text">
                                <h4 class="lato-bold">Revenue Model</h4>
                                <p>

                                    <?php echo strip_tags($investee['how_we_make_money'], '<p><i><br><b><div><table><td><tr><th><ul><ol><li>');; ?>
                                </p>
                            </div>
                            <div class="content-text">
                                <h4 class="lato-bold">Revenue Traction</h4>
                                <p>

                                    <?php echo strip_tags($investee['customer_traction'], '<p><i><br><b><div><table><td><tr><th><ul><ol><li>'); ?>
                                </p>

                            </div>
                            
                            

                        </div>
                        <!-- RIGHT COLUMN -->
                        <?php $this->load->view('investee/right-panel',$investee);?>
                        <!-- RIGHT COLUMN END -->

                    </div>
                </div>
                
                <div class="profile-content col-sm-9 tab-pane" id="market">
                    <div class="row">
                        <div class="col-sm-8">

                            <div class="content-text">
                                <h4 class="lato-bold">Total Addresable Market</h4>
                                <p>
                                    <img style="height: 11px;margin-top: -2px;" src='<?php echo base_url(); ?>assets/images/rupee.png' /> <?php echo format_money($investee['addressable_market']); ?>
                                </p>

                            </div>

                            <div class="content-text">
                                <h4 class="lato-bold">Competition</h4>
                                <p>

                                    <?php echo strip_tags($investee['competition'], '<p><i><br><b><div><table><td><tr><th><ul><ol><li>'); ?>
                                </p>

                            </div>

                        </div>
                        <!-- RIGHT COLUMN -->
                        <?php $this->load->view('investee/right-panel',$investee);?>
                        <!-- RIGHT COLUMN END -->

                    </div>
                </div>
                        
                <div class="profile-content col-sm-9 tab-pane" id="funding">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="content-text">
                                <h4 class="lato-bold">Raise</h4>
                                <p>
                                    Equity Offered % - <?php echo $investee['equity_offered']; ?>
                                </p>
                            </div>
                            <div class="content-text">
                                <h4 class="lato-bold">Valuation</h4>
                                <p>
                                    Pre Money Valuation : <?php echo $investee['pre_valuation']  ?>
                                </p>
                                <p>
                                    Post Money Valution : <?php echo $investee['post_valuation']  ?>
                                </p>

                            </div>
                            <div class="content-text">
                                <h4 class="lato-bold">Use of Funds</h4>
                                <table class="table table-striped">
                                    <tr>
                                        <th>Purpose</th>
                                        <th>Amount</th>
                                    </tr>
                                    <?php $total = 0; foreach($purposes as $purpose){ ?>
                                    <tr>
                                        <td><?php echo $purpose['purpose'];  ?></td>
                                        <td><?php echo $purpose['amount'];  ?></td>
                                    </tr>
                                    <?php $total += $purpose['amount']; } ?>
                                    <tr>
                                        <td><b>Total</b></td>
                                        <td><b><?php echo $total;  ?></b></td>
                                    </tr>
                                </table>

                            </div>
                            <div class="content-text">
                                <h4 class="lato-bold">Prior Fund raise details</h4>
                                <p>
                                    <?php echo $investee['funding_history']; ?>
                                </p>
                            </div>
                        </div>
                        <!-- RIGHT COLUMN -->
                        <?php $this->load->view('investee/right-panel',$investee);?>
                        <!-- RIGHT COLUMN END -->

                    </div>
                </div>
                
                <div class="profile-content col-sm-9 tab-pane" id="finance">
                    <div class="row">
                        <div class="col-sm-8">

                            <div class="content-text">
                                <h4 class="lato-bold">Current Monthly Financial Indicators</h4>
                                <p>
                                    Revenues : <?php echo $investee['monthly_revenue']  ?>
                                </p>
                                <p>
                                    Fixed Cost (OPEX): <?php echo $investee['fixed_opex']  ?>
                                </p>
                                <p>
                                    Cash Burn: <?php echo $investee['cash_burn']  ?>
                                </p>
                                <p>
                                    Debt: <?php echo $investee['debt']  ?>
                                </p>
                                

                            </div>
                            

                            
                            
                            
                            
                            <div class="content-text">
                                <h4 class="lato-bold">Financial Forecast</h4>
                                <p>
                                    <a href='<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id'];  ?>/files/<?php echo $investee['financial_forecast']; ?>'><?php echo $investee['financial_forecast']; ?></a>
                                </p>

                            </div>
                            
                            <div class="content-text">
                                <h4 class="lato-bold">Financial Statements</h4>
                                <p>
                                    <a href='<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id'];  ?>/files/<?php echo $investee['financial_statement']; ?>'><?php echo $investee['financial_statement']; ?></a>
                                </p>

                            </div>
                            
                            <div class="content-text">
                                <h4 class="lato-bold">Others (if any)</h4>
                                <p>
                                    <a href='<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id'];  ?>/files/<?php echo $investee['other']; ?>'><?php echo $investee['other']; ?></a>
                                </p>

                            </div>

                        </div>
                        <!-- RIGHT COLUMN -->
                        <?php $this->load->view('investee/right-panel',$investee);?>
                        <!-- RIGHT COLUMN END -->
                    </div>
                </div>
                
                <!-- achievement tab -->
                
                <div class="profile-content col-sm-9 tab-pane" id="achievement">
                    <div class="row">
                        <div class="col-sm-8">

                            <div class="content-text">
                                <h4 class="lato-bold">Media</h4>
                                <p>
                                    <a href='<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id'];  ?>/files/<?php echo $investee['media']; ?>'><?php echo $investee['media']; ?></a>
                                </p>

                            </div>
                            <div class="content-text">
                                <h4 class="lato-bold">Awards & Recognition</h4>
                                <p>
                                    <a href='<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id'];  ?>/files/<?php echo $investee['awards']; ?>'><?php echo $investee['awards']; ?></a>
                                </p>

                            </div>

                            <div class="content-text">
                                <h4 class="lato-bold">Testimonials</h4>
                                <p>
                                    <a href='<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id'];  ?>/files/<?php echo $investee['testimonials']; ?>'><?php echo $investee['testimonials']; ?></a>
                                </p>

                            </div>
                            
                            

                        </div>
                        <!-- RIGHT COLUMN -->
                        <?php $this->load->view('investee/right-panel',$investee);?>
                        <!-- RIGHT COLUMN END -->
                    </div>
                </div>
                <!-- end achievement tab -->
                
                <div class="profile-content col-sm-9 tab-pane" id="presentation">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="content-text presentation-area">
                                <h4 class="lato-bold">Presentation</h4>
                                <p>
                                    <iframe src="" width="450" height="450" frameborder="0" marginwidth="undefined" marginheight="undefined" scrolling="no" allowfullscreen> </iframe> 
                                </p>
                                <div id="slideshare-iframe" style="display: none;">
                                    <?php echo $investee['presentation']; ?>
                            </div>
                        </div>
                        </div>
                        <!-- RIGHT COLUMN -->
                        <?php $this->load->view('investee/right-panel',$investee);?>
                        <!-- RIGHT COLUMN END -->
                    </div>
                </div>
                
                <div class="profile-content col-sm-9 tab-pane" id="video">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="content-text">
                                <?php $video_key = end(explode("=", $investee['video_link'])); ?>
                                 <iframe style="margin: 11px 0px;" width="450" height="270" src="//youtube.com/embed/<?php echo $video_key;  ?>" frameborder="0" allowfullscreen></iframe>

                            </div>
                        </div>
                        <!-- RIGHT COLUMN -->
                        <?php $this->load->view('investee/right-panel',$investee);?>
                        <!-- RIGHT COLUMN END -->
                    </div>
                </div>
                <div class="profile-content col-sm-9 tab-pane" id="questions">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="content-text">
                                <div class="questions-head item-border">
                                    <h4 class="lato-bold pull-left">Questions</h4>
                                     <input class="join-btn btn col-sm-12 pull-right" type="button" value="Reply">
                                </div>
                                <div class="qn-ans">
                                    <div class="qa-text qn">
                                        <span class="q pull-left">Q.</span>
                                        <p class="pull-right">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
                                        <p class="pull-right small-txt">Added by <span>Rahul Talar</span>, 26 Jan 2014, 12:00PM</p>
                                    </div>
                                    <div class="qa-text ans">
                                        <span class="a pull-left">A.</span>
                                        <p class="pull-right">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
                                        <p class="pull-right small-txt">By <span>EQ Expert</span>, 26 Jan 2014, 12:00PM</p>
                                    </div>
                                </div>
                                <div class="qn-ans">
                                    <div class="qa-text qn">
                                        <span class="q pull-left">Q.</span>
                                        <p class="pull-right">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
                                        <p class="pull-right small-txt">Added by <span>Rahul Talar</span>, 26 Jan 2014, 12:00PM</p>
                                    </div>
                                    <div class="qa-text ans">
                                        <span class="a pull-left">A.</span>
                                        <p class="pull-right">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
                                        <p class="pull-right small-txt">By <span>EQ Expert</span>, 26 Jan 2014, 12:00PM</p>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <!-- RIGHT COLUMN -->
                        <?php $this->load->view('investee/right-panel',$investee);?>
                        <!-- RIGHT COLUMN END -->
                    </div>
                </div>
                
            </div>                <!-- end of tabpanes -->


        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
      
        var embed_id = $('.presentation-area #slideshare-iframe iframe').attr('src');
        $('.presentation-area p iframe').attr('src',embed_id);
        
        var investeeId = '<?php echo $investee['user_id']; ?>';
        $('a[href="#presentation"]').on('click', function () {
            $('a[href="#presentation"] .sidelink').css('background','#a7e41e');
            $('a[href="#video"] .sidelink').css('background','#878686');
        })
        
        $('a[href="#video"]').on('click', function () {
            $('a[href="#video"] .sidelink').css('background','#a7e41e');
            $('a[href="#presentation"] .sidelink').css('background','#878686');
        })
        
        $('.nav a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $('a[href="#video"] .sidelink').css('background','#878686');
            $('a[href="#presentation"] .sidelink').css('background','#878686');
        })
        
    });
</script>