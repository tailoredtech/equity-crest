<!-- Start Market panel -->
<div class="row">
    <div class="col-xs-4 block-title">
        <h2>Market</h2>
    </div>
    <div class="col-xs-offset-2 col-xs-6 block-title">
        <span class="glyphicon glyphicon-cog privacy-icon"></span>
        <div class="privacy-options">
        <div class="input-group">
                    <span class="input-group-addon">Visibility:</span>
            <select name="market" class="form-control form-input" >
                <option value="0" <?php echo (isset($privacy['market']) && $privacy['market']== 0) ? 'selected' : ''; ?>>All</option>
                <option value="1" <?php echo (isset($privacy['market']) && $privacy['market'] == 1) ? 'selected' : ''; ?>>All Investors & Partners</option>
                <option  value="2" <?php echo (isset($privacy['market']) && $privacy['market'] == 2) ? 'selected' : ''; ?>>Pledged Investors</option>
                <option  value="3" <?php echo (isset($privacy['market']) && $privacy['market'] == 3) ? 'selected' : ''; ?>>Followers</option>
                <option  value="4" <?php echo (isset($privacy['market']) && $privacy['market'] == 4) ? 'selected' : ''; ?>>On Request</option>
            </select>
        </div>    
        </div>
    </div>
</div>
<div class="row margin-top-10">
    <form id="investee-info-form" action="<?php echo base_url(); ?>user/edit_investee_info_process" enctype="multipart/form-data" method="post" >
    <input type="hidden" name="user_id" value="<?php echo $investee_id; ?>" />
    <input type="hidden" name="investee_id" value="<?php echo $user_id; ?>" />
    <div id="Market" class="accordion2">
        <div class="accordion-inner">
            <div class="form-group row">
                <div class="col-sm-12">
                    <input type="submit" name="market_submit"  class="btn btn-eq-common pull-right" value="Save & Continue" />
                </div>
            </div>       
            <div class="form-group row">
                <label for="addressable-market" class="col-sm-5 form-label">Total Addressable Market</label>
                <div class="col-sm-7">
                    <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                    <input type="text" class="form-control form-input rupee-input" id="addressable-market" name="addressable_market" value="<?php echo isset($user['addressable_market']) ? rupeeFormat($user['addressable_market']) : ''; ?>" placeholder="">
                </div>
            </div>
            <div class="form-group row">
                <label for="market_size_commentary" class="col-sm-5 form-label">Commentary on Target Market and Market Size</label>
                <div class="col-sm-7">
                    <textarea class="form-control form-input form-area" id="market_size_commentary" name="market_size_commentary"  placeholder="">
                      <?php echo isset($user['market_size_commentary']) ? $user['market_size_commentary'] : ""; ?>
                    </textarea>
                </div>
            </div>
            <div class="form-group row">
                <label for="competition" class="col-sm-5 form-label">Competition</label>
                <div class="col-sm-7">
                    <textarea class="form-control form-input form-area" id="competition" name="competition"  placeholder="">
                    	<?php echo isset($user['competition']) ? $user['competition'] : ""; ?>
                    </textarea>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <input type="submit" name="market_submit"  class="btn btn-eq-common pull-right" value="Save & Continue" />
                </div>
            </div>                          
        </div>
    </div>
</div>
<!-- End Market panel -->