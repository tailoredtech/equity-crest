<div class="container company-details-page">
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>home/portfolio_all">Portfolio</a></li>
      <li class="active"><a href="#"><?php echo $investee['company_name'] ?></a></li>
    </ol>

    <div class="row">

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 company-details-header">

            <div class="company-logo">
                <span>
                    <?php if($investee['image']){ ?>
                    <img src="<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id'] ?>/<?php echo $investee['image']; ?>" />
                    <?php }else{ ?>
                    <img src="<?php echo base_url().'assets/images/company_default_50x50.jpg';?>" />
                    <?php } ?>
                </span>
                
            </div>

            <div class="company-name">
                <h1 class="page-title2"><?php echo $investee['company_name'] ?> <span class="company-location"><?php echo $investee['city']; ?></span></h1>
            </div>
			
			<?php if($investee['user_id'] == $this->session->userdata('user_id')): ?>
            <div class="pull-right">
                <a href="<?php echo base_url(); ?>user/investee_info" class="btn btn-default btn-eq-common btn-eq-small">Edit</a>
            </div>
            <?php endif; ?>

        </div>


    </div>

    <div class="row company-details-main">

        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 sidebar">       
            <?php $this->load->view('investee/left-sidebar',$investee);?>

        </div> <!-- /.sidebar -->

        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 col-main">

            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <h3 class="company-tab-title">
                        Company Presentation
                        <?php if($investee['presentation_url'] != '' && $this->session->userdata('user_id') != '') { ?>
                        <div id="pptDownload" data-toggle="modal" data-target="#myModal" >
                            <img src="<?php echo base_url().'assets/images/download_icon.png';?>" width="35">
                        </div>
                        <?php } ?>
                    </h3>
                    
                    
                    <div class="content-text presentation-area">
			<?php echo $investee['presentation'] != ''? $investee['presentation']: ''; ?> 
                        <!-- p><iframe src="" width="100%" height="200" frameborder="0" marginwidth="undefined" marginheight="undefined" scrolling="no" allowfullscreen></iframe></p -->
                        <div id="slideshare-iframe" style="display: none;">
                            <?php echo $investee['presentation']; ?>
                        </div>
                    </div>
                </div>
                
                <div class="col-sm-6 col-xs-12">
                    <h3 class="company-tab-title">
                        Proposal Video
                        <?php /* if ($investee['video_link'] != '' && $this->session->userdata('user_id') != ''):?>
                            <div id="videoDownload"  data-toggle="modal" data-target="#myModal1">
                                <img src="<?php echo base_url().'assets/images/download_icon.png';?>" width="35">
                            </div>
                        <?php endif; */ ?>
                    </h3>
                    <div class="content-text">
                        <?php if ($investee['video_link'] != '' && $this->session->userdata('user_id') != ''):?>
                            <?php $video_key = end(explode("=", $investee['video_link'])); ?>
                            <iframe width="100%" height="200" src="https://www.youtube.com/embed/<?php echo $video_key;  ?>" frameborder="0" allowfullscreen></iframe>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <div class="company-accordion">
                <div class="accordion-heading">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseTeam" tab-name="<?php echo $investee['company_name']."_Team_tab"; ?>"><h3>Team</h3></a>
                </div>
                <div id="collapseTeam" class="accordion-body collapse">
                  <div class="accordion-inner clearfix"> 
                      <?php if($teamPrivacy){ 
                            $i = 1;

			    echo $investee['team_summary'];

                            foreach ($team_info as $team) {
                        ?>
                            <p><?php echo $i.')'.$team['name'];?></p>
                            <p><?php echo $team['designation'];?></p>
                            <p><?php echo '<a href="'.$team['linkedin'].'" target="_blank">'.$team['linkedin'].'</a>';?></p>
                            <p><?php echo strip_tags(preg_replace('#(<[a-z ]*)(style=("|\')(.*?)("|\'))([a-z ]*>)#', '\\1\\6', $team['experience']), '<p><i><br><b><div><table><td><tr><th><ul><ol><li>'); ?></p>
                            <br />
                        <?php
                            $i++;
                            }
                        ?>
                      <?php }else{ ?>
                        
                            <div class="alert alert-danger" role="alert">The company has restricted access to these information.<br>
                            <?php if($privacy_team == 4) { ?>    
                            <a href="javascript:void(0);" data-section-id="1" data-section="Team"  class="alert-link access-request-link">Click here to request for access</a>
                            <?php } ?>
                            </div>
                       
                      <?php } ?>

                   </div>
                </div>
            </div>

            <div class="company-accordion">
                <div class="accordion-heading">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseBusiness" tab-name="<?php echo $investee['company_name']."_Business_tab"; ?>"><h3>Business</h3></a>
                </div>
                <div id="collapseBusiness" class="accordion-body collapse">
                  <div class="accordion-inner clearfix">

                   
                       <?php if($businessPrivacy){ ?>
                       
                            <p><strong>Product/Service Description</strong></p>

                            <div class="company-accordion-text-editor">
                                <?php echo strip_tags(preg_replace('#(<[a-z ]*)(style=("|\')(.*?)("|\'))([a-z ]*>)#', '\\1\\6', $investee['products_services']), '<p><i><br><b><div><table><td><tr><th><ul><ol><li>'); ?>
                            </div>                        
                       
                       
                            <p><strong>Unique Selling Proposition</strong></p>


                            <div class="company-accordion-text-editor">
                                <?php echo strip_tags(preg_replace('#(<[a-z ]*)(style=("|\')(.*?)("|\'))([a-z ]*>)#', '\\1\\6', $investee['how_different']), '<p><i><br><b><div><table><td><tr><th><ul><ol><li>'); ?>
                            </div>
                        
                      <?php }else{ ?>
                       
                                <div class="alert alert-danger" role="alert">The company has restricted access to these information.<br>
                                <?php if($privacy_business == 4) { ?>    
                                <a href="javascript:void(0);" data-section-id="2" data-section="Business" class="alert-link access-request-link">Click here to request for access</a>.
                                <?php } ?>
                                </div>
                        
                      <?php } ?>  
                   

                   </div>
                </div>
            </div>

            <div class="company-accordion">
                <div class="accordion-heading">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseRevenue" tab-name="<?php echo $investee['company_name']."_Revenue_model_tab"; ?>"><h3>Revenue model and traction</h3></a>
                </div>
                <div id="collapseRevenue" class="accordion-body collapse">
                  <div class="accordion-inner clearfix">
                                       
                      <?php if($monetizationPrivacy){ ?>
                      
                            <p><strong>Revenue Model</strong></p>
                            <div class="company-accordion-text-editor">
                                <?php echo strip_tags(preg_replace('#(<[a-z ]*)(style=("|\')(.*?)("|\'))([a-z ]*>)#', '\\1\\6', $investee['how_we_make_money']), '<p><i><br><b><div><table><td><tr><th><ul><ol><li>'); ?>
                            </div>
                       
                            <p><strong>Traction</strong></p>
                            <div class="company-accordion-text-editor">
                                <?php echo strip_tags(preg_replace('#(<[a-z ]*)(style=("|\')(.*?)("|\'))([a-z ]*>)#', '\\1\\6', $investee['customer_traction']), '<p><i><br><b><div><table><td><tr><th><ul><ol><li>'); ?>
                            </div>
                       
                      <?php }else{ ?>
                      
                            <div class="alert alert-danger" role="alert">The company has restricted access to these information. <br>
                            <?php if($privacy_monetization == 4) { ?>    
                            <a href="javascript:void(0);" data-section-id="3" data-section="Monetization" class="alert-link access-request-link">Click here to request for access</a>.
                            <?php } ?>
                            </div>
                      
                      <?php } ?>                 

                   </div>
                </div>
            </div>

             <div class="company-accordion">
                <div class="accordion-heading">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseMarket" tab-name="<?php echo $investee['company_name']."_Market_tab"; ?>"><h3>Market</h3></a>
                </div>
                <div id="collapseMarket" class="accordion-body collapse">
                  <div class="accordion-inner clearfix">

                        <?php if($marketPrivacy){ ?>
                       
                            <p><strong>Total Addressable Market</strong></p>
                            <div class="company-accordion-text-editor">
                                <?php if($investee['addressable_market']){ ?>
                                   <span class="cate-fund">&#8377; <?php echo format_money($investee['addressable_market'],2); ?></span> 
                                <?php }else{ ?>
                                    Not Available
                                <?php } ?>
                            </div>

                            <p><strong>Commentary on Target Market and Market Size</strong></p>
                            <div class="company-accordion-text-editor">
                                <?php if($investee['market_size_commentary']){ ?>
					<?php echo $investee['market_size_commentary']; ?>                                   
                                <?php }else{ ?>
                                    Not Available
                                <?php } ?>
                            </div>
                       
                            <p><strong>Competition</strong></p>
                            <div class="company-accordion-text-editor">
                                <?php echo strip_tags(preg_replace('#(<[a-z ]*)(style=("|\')(.*?)("|\'))([a-z ]*>)#', '\\1\\6', $investee['competition']), '<p><i><br><b><div><table><td><tr><th><ul><ol><li>'); ?>
                            </div>
                                <!-- ?php echo nl2br($investee['competition']); ? -->

                       
                      <?php }else{ ?>
                           
                            <div class="alert alert-danger" role="alert">The company has restricted access to these information. <br>
                            <?php if($privacy_market == 4) { ?>    
                            <a href="javascript:void(0);" data-section-id="4" data-section="Market" class="alert-link access-request-link">Click here to request for access</a>.
                            <?php } ?>
                            </div>
                           
                      <?php } ?>


                   </div>
                </div>
            </div>
            
             <div class="company-accordion">
                <div class="accordion-heading">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseRaise" tab-name="<?php echo $investee['company_name']."_Raise_tab"; ?>"><h3>Raise</h3></a>
                </div>
                <div id="collapseRaise" class="accordion-body collapse">
                  <div class="accordion-inner clearfix">

                    <?php if($raisePrivacy){ ?>                    

                    <div class="row">

                        <div class="col-sm-6 col-xs-12">                          
                       
                            <p><strong>Raise</strong></p>

                            <div class="company-accordion-text-editor">
                           
                                <?php if($investee['equity_offered']){ ?>
                                Equity Offered : <?php echo $investee['equity_offered']; ?>%
                                <?php }else{ ?>
                                Equity Offered : To be discussed
                                <?php } ?>

                            </div>
                        
                            <p><strong>Valuation</strong></p>

                            <div class="company-accordion-text-editor">
                           
                            <?php if($investee['equity_offered']){ 
                                   $post_money_val = '';
                                   $pre_money_val = '';
                                   $invst_required = $investee['investment_required'];
                                    $eqty_offered  = $investee['equity_offered']/100;
                                    $post_money_val = $invst_required/$eqty_offered;
                                    $pre_money_val = $post_money_val - $invst_required; 
                             ?>
                                    <p>
                                        Pre Money Valuation : <?php echo format_money($pre_money_val,2); ?> <br>                                   
                                        Post Money Valuation : <?php echo format_money($post_money_val,2) ?>
                                    </p>
                            <?php }else { ?>
                                   <p>
                                        Pre Money Valuation :  To be discussed <br>                                    
                                        Post Money Valuation :  To be discussed
                                    </p>
                            <?php } ?>

                            </div>


                            <p><strong>Prior Fund raise details</strong></p>

                            <div class="company-accordion-text-editor">

                                <?php if($investee['funding_history']){ 
                                    echo strip_tags(preg_replace('#(<[a-z ]*)(style=("|\')(.*?)("|\'))([a-z ]*>)#', '\\1\\6', $investee['funding_history']), '<p><i><br><b><table><div><td><tr><th><ul><ol><li>');
                                    ?>
                                <?php }else{ ?>
                                    Not Available
                                <?php } ?>
                            </div>
                            
                        </div> <!-- /.col-xs-12 -->


                        <div class="col-sm-6 col-xs-12">

                            <div class="cate-table">
                                
                                <div class="row cate-table-heading">
                                    <div class="pull-left col-xs-7"><strong>Use of Funds</strong></div>
                                    <div class="pull-right text-right col-xs-5"><strong>Amount</strong></div>
                                </div>

                                <?php $total = 0; foreach($purposes as $purpose){
                                    if($purpose['purpose'] != ''){
                                    ?> 

                                <div class="row">
                                    <div class="pull-left col-xs-7"><?php echo $purpose['purpose'];  ?></div>
                                    <div class="pull-right text-right col-xs-5">&#8377; <?php echo format_money($purpose['amount']);  ?></div>
                                </div>

                                <?php $total += $purpose['amount']; 
                          
                                         }
                                    } ?>

                                <div class="row cate-table-footer">
                                    <div class="pull-left col-xs-7"><strong>Total</strong></div>
                                    <div class="pull-right text-right col-xs-5 cate-table-amount"><strong>&#8377; <?php echo format_money($total);  ?></strong></div>
                                </div>

                            </div> <!-- cate-table -->

                        </div> <!-- /.col-xs-12 -->

                    </div> <!-- /.row -->                            
                        
                        <?php } else { ?>
                            
                                    <div class="alert alert-danger" role="alert">The company has restricted access to these information. <br>
                                    <?php if($privacy_raise == 4) { ?>    
                                    <a href="javascript:void(0);" data-section-id="5" data-section="Raise" class="alert-link access-request-link">Click here to request for access</a>.
                                    <?php } ?>
                                    </div>
                          
                        <?php } ?>

                   </div>
                </div>
            </div>
            
            <div class="company-accordion">
                <div class="accordion-heading">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseFinancials" tab-name="<?php echo $investee['company_name']."_Financials_tab"; ?>"><h3>Financials</h3></a>
                </div>
                <div id="collapseFinancials" class="accordion-body collapse">
                  <div class="accordion-inner clearfix">

                    <?php if($financialtPrivacy){ ?>

                    <div class="row">

                        <div class="col-sm-6 col-xs-12">

                            <p><strong>Financial Forecast</strong></p>
                            <div class="company-accordion-text-editor">
                                 <?php if($investee['financial_forecast']){ ?>
                                    <?php if($financialForecastPermission) { ?>
                                        <a href='<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id']; ?>/files/<?php echo $investee['financial_forecast']; ?>' target="_blank"><?php echo (($investee['financial_forecast']) ? $investee['financial_forecast'] : 'Click here to download'); ?></a>
                                    <?php }else if($privacy_financial == 4) { ?>
                                        <?php echo $investee['financial_forecast']; ?>&nbsp;(<span class="download-request-link" data-section-id="8" data-section="Financial Forecast">Click here to request for access</span>)
                                    <?php }else{ ?>
                                        <div class="alert alert-danger" role="alert">The company has restricted access to these information. <br></div>
                                    <?php } ?>
                                <?php }else{ ?>
                                    Not Available
                                <?php } ?>
                            </div>   
                       
                            <p><strong>Financial Statements</strong></p>
                            <div class="company-accordion-text-editor">
                                <?php if($investee['financial_statement']){ ?>
                                   <?php if($financialStatementPermission) { ?>
                                        <a href='<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id']; ?>/files/<?php echo $investee['financial_statement']; ?>' target="_blank"><?php echo (($investee['financial_statement']) ? $investee['financial_statement'] : 'Click here to download'); ?></a>
                                    <?php }else if($privacy_financial == 4) { ?>
                                        <?php echo $investee['financial_statement']; ?>&nbsp;(<span class="download-request-link" data-section-id="9" data-section="Financial Statements">Click here to request for access</span>)
                                    <?php }else{ ?>
                                        <div class="alert alert-danger" role="alert">The company has restricted access to these information. <br></div>
                                    <?php } ?>
                                <?php }else{ ?>
                                    Not Available
                                <?php } ?>
                            </div>                        
                       
                            <p><strong>Others (if any)</strong></p>
                            <div class="company-accordion-text-editor">
                                <?php if($investee['other']){ ?>
                                    <a href='<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id']; ?>/files/<?php echo $investee['other']; ?>' target="_blank"><?php echo $investee['other']; ?></a>
                                <?php }else{ ?>
                                    Not Available
                                <?php } ?>
                            </div>

                        </div> <!-- /.col-sm-6 col-xs-12 -->

                        <div class="col-sm-6 col-xs-12">


                            <div class="cate-table">

                                <div class="row cate-table-heading">
                                    <div class="pull-left col-xs-12"><strong>Current Monthly Financial Indicators</strong></div>                                   
                                </div>

                                <div class="row">
                                    <div class="pull-left col-xs-7">Revenues</div>
                                    <div class="pull-right text-right col-xs-5">
                                         <?php if($investee['monthly_revenue']){ ?>
                                           &#8377; <?php if($investee['monthly_revenue']){ ?><?php echo format_money($investee['monthly_revenue']); }  else { echo '0'; } ?>
                                        <?php }else{ ?>
                                            Not Available
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="pull-left col-xs-7">Fixed Cost (OPEX)</div>
                                    <div class="pull-right text-right col-xs-5">
                                        <?php if(!is_null($investee['fixed_opex'])){ ?>
                                            &#8377; <?php if($investee['fixed_opex']){ ?><?php echo format_money($investee['fixed_opex'],2); }  else { echo '0'; } ?>
                                        <?php }else{ ?>
                                            Not Available
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="pull-left col-xs-7">Cash Burn</div>
                                    <div class="pull-right text-right col-xs-5">
                                        <?php if(!is_null($investee['cash_burn'])){ ?>
                                            &#8377; <?php if($investee['cash_burn']){ ?><?php echo format_money($investee['cash_burn'],2); }  else { echo '0'; } ?>
                                        <?php }else{ ?>
                                            Not Available
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="pull-left col-xs-7">Debt</div>
                                    <div class="pull-right text-right col-xs-5">
                                        <?php if(!is_null($investee['debt'])){ ?>
                                            &#8377; <?php if($investee['debt']){ ?><?php echo format_money($investee['debt'],2); }  else { echo '0'; } ?>
                                        <?php }else{ ?>
                                            Not Available
                                        <?php } ?>
                                    </div>
                                </div>
                            </div> <!-- /.cate-table -->

                        </div> <!-- /.col-sm-6 col-xs-12 -->

                    </div>

                    <?php }else{ ?>
                        
                        <div class="alert alert-danger" role="alert">The company has restricted access to these information. <br>
                        <?php if($privacy_financial == 4) { ?>    
                        <a href="javascript:void(0);" data-section-id="6" data-section="Financials" class="alert-link access-request-link">Click here to request for access</a>.
                        <?php } ?>
                        </div>
                        
                    <?php } ?> 

                   </div>
                </div>
            </div>

             <div class="company-accordion">
                <div class="accordion-heading">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseAchievements" tab-name="<?php echo $investee['company_name']."_Achievements_tab"; ?>"><h3>Achievements</h3></a>
                </div>
                <div id="collapseAchievements" class="accordion-body collapse">
                  <div class="accordion-inner clearfix">

                       <?php if($achievementsPrivacy){ ?>
                        <div class="content-text">
                            <p><strong>Media</strong></p>
                            <div class="company-accordion-text-editor">
                                <?php if($investee['media']){ 
                                    echo strip_tags(preg_replace('#(<[a-z ]*)(style=("|\')(.*?)("|\'))([a-z ]*>)#', '\\1\\6', $investee['media']), '<i><br><b><table><td><tr><th><ul><ol><li>'); 
                                      ?>
                                <?php }else{ ?>
                                    Not Available
                                <?php } ?>
                            </div>

                        </div>
                        <div class="content-text">
                            <p><strong>Awards & Recognition</strong></p>
                            <div class="company-accordion-text-editor">
                                <?php if($investee['awards']){ 
                                    echo strip_tags(preg_replace('#(<[a-z ]*)(style=("|\')(.*?)("|\'))([a-z ]*>)#', '\\1\\6', $investee['awards']), '<i><br><b><table><td><tr><th><ul><ol><li>'); 
                                     ?>
                                <?php }else{ ?>
                                    Not Available
                                <?php } ?>
                            </div>

                        </div>

                        <div class="content-text">
                            <p><strong>Testimonials</strong></p>
                            <div class="company-accordion-text-editor">
                                <?php if($investee['testimonials']){
                                    echo strip_tags(preg_replace('#(<[a-z ]*)(style=("|\')(.*?)("|\'))([a-z ]*>)#', '\\1\\6', $investee['testimonials']), '<i><br><b><table><td><tr><th><ul><ol><li>');  
                                     ?>
                                <?php }else{ ?>
                                    Not Available
                                <?php } ?>
                            </div>

                        </div>
                        
                       <?php }else{ ?>
                            <div class="content-text">
                                    <div class="alert alert-danger" role="alert">The company has restricted access to these information. <br>
                                    <?php if($privacy_achievements == 4) { ?>    
                                    <a href="javascript:void(0);" data-section-id="7" data-section="Achievements" class="alert-link access-request-link">Click here to request for access</a>.
                                    <?php } ?>
                                    </div>
                            </div>
                       <?php } ?> 

                   </div>
                </div>
            </div>  

            <?php // print_r($investee); exit;?>


            <?php if(isset($investee['updates']) && $investee['updates'] != '') { ?>
            <div class="company-accordion">
                <div class="accordion-heading">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseUpdates" tab-name="<?php echo $investee['company_name']."_Updates_tab"; ?>"><h3>Updates</h3></a>
                </div>
                <div id="collapseUpdates" class="accordion-body collapse">
                  <div class="accordion-inner clearfix">

                       <?php if($updatesPrivacy){ ?>
                        <div class="content-text">
                            <p><strong>Updates</strong></p>
                            <div class="company-accordion-text-editor">
                                <?php if($investee['updates']){ 
                                    echo strip_tags(preg_replace('#(<[a-z ]*)(style=("|\')(.*?)("|\'))([a-z ]*>)#', '\\1\\6', $investee['updates']), '<i><br><b><table><td><tr><th><ul><ol><li>'); 
                                      ?>
                                <?php }else{ ?>
                                    Not Available
                                <?php } ?>
                            </div>
                        </div>
                       <?php }else{ ?>
                            <div class="content-text">
                                    <div class="alert alert-danger" role="alert">The company has restricted access to these information. <br>
                                    <?php if($privacy_updates == 4) { ?>    
                                    <a href="javascript:void(0);" data-section-id="7" data-section="Updates" class="alert-link access-request-link">Click here to request for access</a>.
                                    <?php } ?>
                                    </div>
                            </div>
                       <?php } ?> 

                   </div>
                </div>
            </div>
            <?php } ?>

        </div> <!-- /.main -->

    </div>

    <div  class="row content-box">

       
        <!-- LEFT COLUMN -->
        <?php  // $this->load->view('investee/view-left',$investee);?>
        <!-- LEFT COLUMN END -->

                </div>
            </div>
            





<!--             <div class="profile-content col-sm-12 tab-pane <?php echo ($active_tab == 'questions') ? 'active' : ''; ?>" id="questions">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="content-text">
                            <div class="questions-head item-border">
                                <h5 class="lato-bold pull-left"><b>Questions</b></h5>
                                <input style="width: 100px;margin-right: 0px" class="btn eq-btn col-sm-12 pull-right" id="ask-question-btn" type="button" value="Ask Question">
                            </div>
                            <?php foreach($questions as $question){ ?>
                            <div class="qn-ans" id="<?php echo $question['id']; ?>">
                                <div class="qa-text qn">
                                    <span class="q pull-left">Q.</span>
                                    <p class="pull-right"><?php echo $question['question']; ?></p>
                                    <p class="pull-right small-txt">Added by <span><?php echo $question['name']; ?></span>, <?php echo date("j M Y, g:i A",strtotime($question['qn_created_date']))  ?> 
                                        
                                        
                                    </p>
                                </div>
                                <?php if($question['answer']){ ?>
                                <div class="qa-text ans">
                                    <span class="a pull-left">A.</span>
                                    <p class="pull-right"><?php echo $question['answer']; ?></p>
                                    <p class="pull-right small-txt">By <span><?php echo $investee['company_name']; ?></span>, <?php echo date("j M Y, g:i A",strtotime($question['an_created_date']))  ?></p>
                                </div>
                                <?php } ?>
                            </div>
                            <?php } ?>
                            
                        </div>
                    </div>                   
                </div>
            </div> -->
        </div>                
    </div> <!-- /.content-box -->

</div> <!-- /.container -->


<!-- POPUP CONTAINER -->
<div style="display: none;">
    <div id="ask-question" class="container pop-content">
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <h3 class="form-header top-margin-10">Ask Question</h3>
            </div>
        </div>
        <div class="row">
            <div class="top-margin-10">
            <form id="ask-qn-form" method="post">
                <div class="col-xs-12 col-sm-12">
                    <div class="row">
						<div class="col-sm-12">
							<textarea name="question" id="question" class="form-control form-area" placeholder="your query" rows="10"></textarea>
							<input type="hidden" name="investee_id" value="<?php echo $investee['user_id']; ?>" />
						</div>
                    </div>
                    <div class="row">
						<div class="col-sm-12">
                        <label id="question_error" for="question" class="error" style="float:left;">&nbsp;</label>
                        <input class="eq-btn pull-right" type="submit" value="OK" />
						</div> 
					</div>
                </div>
            </form>
            </div>
        </div>
    </div>
    
    <div id="new-qn-txt">
      <div class="qn-ans">  
        <div class="qa-text qn">
            <span class="q pull-left">Q.</span>
            <p class="pull-right qun"></p>
            <p class="pull-right small-txt">Added by <span><?php echo $this->session->userdata('name'); ?></span>, 26 Jan 2014, 12:00PM</p>
        </div>
      </div>    
    </div>
    
    <div id="pledge-pop" class="container pop-content">
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <h3 class="form-header top-margin-10">Pledge</h3>
            </div>
        </div>
        <div class="row">
            <div class="top-margin-10">
            <form id="view-form-pledge" name="view-form-pledge" method="post">
                <div class="col-xs-12 col-sm-12">
                    <div class="row">
                    <div class="form-group">
                        <label for="amount" class="col-sm-2">Amount</label>
                        <div class="col-sm-10">
                            <input type="text" name="amount" id="amount" value="" class="form-control form-input"  placeholder="Ex. 1,00,000">
                            <input type="hidden" name="investee_id" value="" id="investee_id" />
							<input type="hidden" name="pledged_amount" value="0" id="pledged_amount" />
                            <label id="p_amount_error" for="amount" class="error">&nbsp;</label>
                        </div>
                    </div>
                    </div>
                    <div class="text-center pull-right">
                        <input class="eq-btn" type="submit" value="OK" />
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>

    <div id="schedule-meeting-pop" class="container pop-content">
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <h3 class="form-header top-margin-10">Schedule Meeting</h3>
            </div>
        </div>
        <div class="row">
            <div class="top-margin-10">
            <form id="schedule-meeting-form" method="post">
                <div class="col-xs-12 col-sm-12">
                    <div class="row">
                        <div class="form-group">
                            <label for="meeting_time" class="col-sm-2">Date</label>
                            <div class="col-sm-10">
                                <input type="text" id="meeting-time" name="meeting-time" value="" class="form-control form-input"  placeholder="">
                                <input type="hidden" id="investee_id" name="investee_id" value="<?php echo $investee['user_id']; ?>"  />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="title" class="col-sm-2">Subject</label>
                            <div class="col-sm-10">
                                <input type="text" name="title" id="title"  value="" class="form-control form-input"  placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="remark" class="col-sm-2 ">Message</label>
                            <div class="col-sm-10">
                                <textarea name="remark" id="remark"  class="form-control form-area"  placeholder=""></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="text-center pull-right">
                        <input class="eq-btn" type="submit" value="OK" />
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


<?php // if($investee['snapshot']){ ?>
<!-- POPUP -->
<div class="modal fade" id="companySnapshotModal" tabindex="-1" role="dialog" aria-labelledby="companySnapshotModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="companySnapshotModalLabel"><?php echo $investee['company_name'] ?></h4>
            </div>
            <div class="modal-body text-center">                
                    <?php 
                    if(strtolower(end(explode('.',$investee['snapshot']))) == 'pdf')
                    {
                        ?>
                        <object data="<?php echo base_url().$investee['snapshot'];?>#toolbar=1&amp;navpanes=0&amp;scrollbar=1&amp;page=1&amp;view=FitH" 
                            type="application/pdf" 
                            width="100%" 
                            height="500px"></object>
                        <?php
                    }
                    else
                    {
                        ?>
                        <img src="<?php echo base_url().$investee['snapshot'];?>" class="img-responsive" style="display:inline-block" />
                        <?php
                    }
                    ?>
            </div>
        </div>
    </div>
</div>
<?php // } ?>
<?php if($investee['presentation_url']){ ?>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <p class="text-center">
                Downloading the presentation will send a notification to the founder/s informing them of the download. <br> <a href="#" onclick="send_email('ppt','<?php echo base_url().$investee['presentation_url'];?>')">Click here</a> to confirm download of presentation or <span class="closeBtn" data-dismiss="modal">cancel</span> to go back
            </p>
      </div>
    </div>
  </div>
</div>
<?php } ?>
<?php /* if($investee['video_link']){ ?>
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <p class="text-center">
                Downloading the video will send a notification to the founder/s informing them of the download. <br> <a href="#" onclick="send_email('video','<?php echo $investee['video_link'];?>')">Click here</a> to confirm download of video or <span class="closeBtn" data-dismiss="modal">cancel</span> to go back
            </p>
      </div>
    </div>
  </div>
</div>
<?php  } */?>
<script type="text/javascript">
    $(window).load(function(){
        $('#companySnapshotModal').modal('show');
    });
</script>

<script>
    $(document).ready(function(){


        var embed_id = $('.presentation-area #slideshare-iframe iframe').attr('src');
        $('.presentation-area p iframe').attr('src',embed_id);
        
        var investeeId = '<?php echo $investee['user_id']; ?>';
        $('a[href="#presentation"]').on('click', function () {
            $('a[href="#presentation"] .sidelink').css('background','#a7e41e');
            $('a[href="#video"] .sidelink').css('background','#878686');
            $('a[href="#questions"] .sidelink').css('background','#878686');
        })
        
        $('a[href="#video"]').on('click', function () {
            $('a[href="#video"] .sidelink').css('background','#a7e41e');
            $('a[href="#presentation"] .sidelink').css('background','#878686');
            $('a[href="#questions"] .sidelink').css('background','#878686');
        })
        
         $('a[href="#questions"]').on('click', function () {
            $('a[href="#questions"] .sidelink').css('background','#a7e41e');
            $('a[href="#video"] .sidelink').css('background','#878686');
            $('a[href="#presentation"] .sidelink').css('background','#878686');
        })
        
        $('.nav a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $('a[href="#video"] .sidelink').css('background','#878686');
            $('a[href="#presentation"] .sidelink').css('background','#878686');
        })
        
        $('.profile-sidenav').on('click', '.follow', function() {
            <?php if ($this->session->userdata('user_id')) { ?>
                var this_var = $(this);        
            $.ajax({
                    url: "<?php echo base_url(); ?>user/follow",
                    type: 'POST',
                    data : {followId : investeeId}
                })
                .done(function( data ) {
                     if( data == 'success'){
                        this_var.text('Unfollow');
                        this_var.removeClass('follow')
                    }else{
                        console.log(data);
                    }
                });
            <?php } else { ?>            
              window.location.href = '<?php echo base_url(); ?>user/login';
            <?php } ?>
        });
        
         $('.profile-sidenav').on('click', '.unfollow', function() {
                var this_var = $(this);
                $.ajax({
                    url: "<?php echo base_url(); ?>user/unfollow",
                    type: 'POST',
                    data: {followId: investeeId}
                })
                        .done(function(data) {
                    if (data == 'success') {
                        this_var.text('Follow');
                        this_var.removeClass('unfollow')
                    } else {
                        console.log('error');
                    }
                });
            });
        
                $('.profile-sidenav').on('click', '.pledged', function(e) {
                    var invste_id = investeeId;
                    e.preventDefault();
                    $.get("<?php echo base_url(); ?>investee/getpledgedamount/" +  invste_id,function(data) {
                            $(this).addClass('pledging');
                            $("#view-form-pledge #investee_id").val(invste_id);
                            $("#view-form-pledge #pledged_amount").val(data);
                              if ($("#view-form-pledge #pledged_amount").val() != 0) {
                                $("#view-form-pledge #amount").val($("#view-form-pledge #pledged_amount").val());
                              }
                            $.colorbox({inline: true, href: '#pledge-pop', maxWidth: '450px', innerWidth: '80%'});//, innerHeight: '185px'                            
                    });
                 }); 
				 
        $('.profile-sidenav').on('click', '.pledge', function() {
            var invste_id = investeeId;
            $("#view-form-pledge .invste_id").val(invste_id);
            $(this).addClass('pledging');
            $.colorbox({inline: true, href: '#pledge-pop', maxWidth: '450px', innerWidth: '80%'});//, innerHeight: '185px'
        });

        $("#view-form-pledge").on('submit', (function(e) {
            e.preventDefault();
            $.ajax({
                url: "<?php echo base_url(); ?>investee/pledge",
                type: 'POST',
                dataType: 'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false
            })
            .done(function(data) {
                if (data.result == 'success') {
                    $('.profile-sidenav .pledging').text('Pledged');
                    $('.profile-sidenav .pledging').addClass('pledged');
                    $('.profile-sidenav .pledging').removeClass('pledge pledging');

                } else {
                    $('#p_amount_error').text(data.result);
                    return;
                }
                $.colorbox.close();
            });

        }));
        
        
        $("#ask-question-btn").on('click',function(){
            $.colorbox({inline: true, href: '#ask-question', innerWidth: '80%', maxWidth: '550px'});//, innerHeight: '360px'
        });

        $('#question').on('focus',function(){
            $('#question_error').html("&nbsp;");
        })

        $('#ask-qn-form').submit(function(event){
            event.preventDefault();
            if($('#ask-qn-form textarea').val() != ''){
                var question = $('#ask-qn-form textarea').val();
                    $.ajax({
                    url: "<?php echo base_url(); ?>investor/ask_question",
                    type: 'POST',
                    data : {question : question,investeeId:investeeId}
                    })
                    .done(function() {
                        $('#new-qn-txt .qun').text(question);
                        $('.questions-head').after($('#new-qn-txt').html());
                        $.colorbox.close();
                    });
            }else{
                //$('#ask-qn-form textarea').css('border-color', '#a94442');
                $('#question_error').text("Please enter the question you want to ask.");
            }
        });
        
        $('.box-close').on('click',function(){
            $.colorbox.close();
        });
        
        $(".tab-item").click(function() {
            $(".profile-tabs .tab-active").removeClass("tab-active");
            $(this).addClass("tab-active");
            $('a .sidelink').css('background', '#878686');
        });
        
        $('#meeting-time').datetimepicker({
            format:'d-m-Y H:i',
            step:30
        });
        
		$(document).bind('cbox_cleanup', function(){
		   if ($('#pledge-pop').is(":visible")){
			  $('#amount_error').text("");
			  $("#view-form-pledge #amount").val("");
		   }
		}); 
				
        $(".profile-sidenav ").on('click','#schedule-meeting-btn',function(){
            $.colorbox({inline: true, href: '#schedule-meeting-pop', innerWidth: '80%', maxWidth: '400px'});//, innerHeight: '325px'
        });
        
        $("#schedule-meeting-form").on('submit', (function(e) {
            e.preventDefault();
            if($('#meeting_time').val() != ''){
                $.ajax({
                    url: "<?php echo base_url(); ?>investor/schedule_meeting_process",
                    type: 'POST',
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false
                })
                .done(function(data) {
                    if (data !== 'success') {
                        console.log('error');
                        return;
                    }

                    $.colorbox.close();
                });
            } else {
                //

            }

        }));
        


        $(".company-details-main").on('click','.access-request-link', (function() {

            
            var sectionId = $(this).attr('data-section-id');
            var sectionName = $(this).attr('data-section');
            var section = $(this);
            $.ajax({
                url: "<?php echo base_url(); ?>user/access_request",
                type: 'POST',
                data : {investeeId:investeeId,sectionId : sectionId,section : sectionName}
            })
            .done(function(data) {
                if (data == 'success') {
                    section.parent('.alert').html('Successfully sent the request.');
                } else {
                    section.parent('.alert').html('Already sent the request.');
                }
            });

        }));
        
        $(".company-details-main").on('click', '.download-request-link', (function() {
            var sectionId = $(this).attr('data-section-id');
            var sectionName = $(this).attr('data-section');
            var section = $(this);
            $.ajax({
                url: "<?php echo base_url(); ?>user/download_request",
                type: 'POST',
                data : {investeeId:investeeId,sectionId : sectionId,section : sectionName}
            })
            .done(function(data) {
                if (data == 'success') {
                    section.html('Successfully sent the request.');
                } else {
                    section.html('Already sent the request.');
                }
            });

        }));        
    });

function send_email(doc_type,url) {
    // alert("<?php echo $investee['company_name'];?>"); 
    var data = 'ppt_url=' + url; 
    $.ajax({
        url: "<?php echo base_url(); ?>investee/download_file",
        type: 'GET',
        data: { comp : "<?php echo $investee['company_name'];?>", investee_name : "<?php echo $investee['name'];?>", email : "<?php echo $investee['email'];?>", type_doc : doc_type },
        cache: false
    })
    .done(function(data) {
        if (data == 'success') {
            if(doc_type == 'video')
            {
                <?php 
                    $mail_to = $this->session->userdata['email'];
                    // $mail_to = "rohini.b@tailoredtech.in";
		    // $message = "Hello ".$this->session->userdata['name'].','."\n";
                    $message = $investee['video_link'];
                    // $message .= "Regards,"."\n".$investee['company_name'];
                    // mail($mail_to, 'Youtube link', $message); 
                ?>

                $.ajax({
                    url: "<?php echo base_url(); ?>investee/download_link_mail",
                    type: 'GET',
		    data: { mail_message : "<?php echo ' - '.$message;?>", mail_to : "<?php echo $mail_to;?>", comp : "<?php echo $investee['company_name'];?>, username: $this->session->userdata['name']" },
                    cache: false
                })
                .done(function(data) {
                    if (data == 'success') {
                        alert("You have been emailed this link.");
                    }
                });
            }
            else if(doc_type == 'ppt')
            {
                window.location.href = url;
            }
        }
        
        $('#myModal').modal('hide');
        $('#myModal1').modal('hide');
        // console.log(data);
    });
}
</script>
