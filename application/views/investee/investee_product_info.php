<!-- Start Market panel -->
<div class="row">
  <div class="col-xs-6 block-title">
      <h2>Product Information</h2>
  </div>
  <div class="col-xs-6">
      <span class="glyphicon glyphicon-cog privacy-icon"></span>
      <div class="privacy-options">
      <div class="input-group">
                    <span class="input-group-addon">Visibility:</span>
        <select name="market" class="form-control form-input" >
            <option value="0" <?php echo (isset($privacy['prod_info']) && $privacy['prod_info'] == 0) ? 'selected' : ''; ?>>All</option>
            <option value="1" <?php echo (isset($privacy['prod_info']) && $privacy['prod_info'] == 1) ? 'selected' : ''; ?>>All Investors & Partners</option>
            <option  value="2" <?php echo (isset($privacy['prod_info']) && $privacy['prod_info'] == 2) ? 'selected' : ''; ?>>Pledged Investors</option>
            <option  value="3" <?php echo (isset($privacy['prod_info']) && $privacy['prod_info'] == 3) ? 'selected' : ''; ?>>Followers</option>
            <option  value="4" <?php echo (isset($privacy['prod_info']) && $privacy['prod_info'] == 4) ? 'selected' : ''; ?>>On Request</option>
        </select>
      </div>
      </div>
  </div>
</div>
<div class="row margin-top-10">
<form id="investee-info-form" action="<?php echo base_url(); ?>user/edit_investee_info_process" enctype="multipart/form-data" method="post" >
<input type="hidden" name="user_id" value="<?php echo $investee_id; ?>" />
<input type="hidden" name="investee_id" value="<?php echo $user_id; ?>" />

    <div id="Market" class="accordion2">
        <div class="accordion-inner">
                      <div class="form-group row">
                <div class="col-sm-12">
                    <input type="submit" name="product_info_submit"  class="btn btn-eq-common pull-right" value="Save & Continue" />
                </div>
            </div>                          

            <div class="form-group row">
                <label for="presentation" class="col-sm-5 form-label">Company presentation (slideshare link)</label>
                <div class="col-sm-7">
                    <input type="text" class="form-control form-input" id="presentation" name="presentation"  placeholder="" value="<?php echo isset($user['presentation']) ? $user['presentation'] : ''; ?>" />
                </div>
            </div>
            <div class="form-group row">
                <label for="video_link" class="col-sm-5 form-label">Video</label>
                <div class="col-sm-7">
                    <input type="text" name="video_link" value="<?php echo $user['video_link']; ?>" class="form-control form-input" id="video_link" placeholder="">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <input type="submit" name="product_info_submit"  class="btn btn-eq-common pull-right" value="Save & Continue" />
                </div>
            </div>                          
        </div>
    </div>
</div>
<!-- End Market panel -->