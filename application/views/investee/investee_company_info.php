<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" />
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>
<script type="text/javascript">
  $('select').select2();
</script>

<div class="row">        
        <div class="col-xs-12 block-title">
          <h2>Company Information</h2>
        </div>
</div>
        <div class="row margin-top-10">
        <form id="investee-info-form" action="<?php echo base_url(); ?>user/edit_investee_info_process" enctype="multipart/form-data" method="post" >
            <input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
                <div id="company-info" class="accordion2">
                    <div class="accordion-inner">            
            <div class="form-group row">
                <div class="col-sm-12">
                    <input type="submit" name="company_submit"  class="btn btn-eq-common pull-right" value="Save & Continue" />
                </div>
            </div>
        <!--Slider Accordion Starts here-->
            <!-- Start Company Info panel -->

                        <!-- div class="form-group row">
                            <label for="founder-name" class="col-sm-5 form-label">Founder Name *</label>
                            <div class="col-sm-7">
                                <input type="text" name="name" value="<?php // echo $user['name']; ?>" class="form-control form-input" id="founder-name" placeholder="" required>
                            </div>
                        </div -->
                        <div class="form-group row">
                            <label for="sign-in-as" class="col-sm-5 form-label">Company Name *</label>
                            <div class="col-sm-7">
                                <input type="text" name="company_name" class="form-control form-input" id="company_name" value="<?php echo $user['company_name']; ?>" placeholder="" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="company_url" class="col-sm-5 form-label">URL *</label>
                            <div class="col-sm-7">
                              
                                <input type="text" name="company_url" value="<?php echo isset($user['company_url'])? $user['company_url']:''; ?>" class="form-control form-input" id="company_url" placeholder="" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="country" class="col-sm-5 form-label">Country *</label>
                            <div class="col-sm-7">
                                <div class="select-style">
                                    <select class="form-control form-input country" name="country"  placeholder="">
                                        <option value="">Country</option>
                                        <?php foreach ($countries as $country) { ?>
                                            <option value="<?php echo $country['id']; ?>" <?php echo ($country['id'] == $user['country'] ) ? 'selected' : '' ?>><?php echo $country['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="state" class="col-sm-5 form-label">State</label>
                            <div class="col-sm-7">
                                <div class="select-style">
                                    <select class="form-control form-input state" name="state"  placeholder="" required>
                                        <?php foreach ($states as $state) { ?>
                                            <option value="<?php echo $state['id']; ?>" <?php echo ($state['id'] == $user['state'] ) ? 'selected' : '' ?>><?php echo $state['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="city" class="col-sm-5 form-label">City *</label>
                            <div class="col-sm-7">
                                <input type="text" name="city" value="<?php echo $user['city']; ?>" class="form-control form-input" id="city" placeholder="" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="year" class="col-sm-5 form-label">Founding Year *</label>
                            <div class="col-sm-7">
                                <input type="text" name="year" value="<?php echo isset($user['year']) ? $user['year']: ''; ?>" class="form-control form-input" id="year" placeholder="" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="stage" class="col-sm-5 form-label">Stage</label>
                            <div class="col-sm-7">
                                <div class="select-style">
                                    <select class="form-control form-input" id="stage" name="stage" placeholder="Select Startup Stage">
                                        <?php foreach ($stages as $stage) { 
                                            
                                            $stage_selected = '';
                                            
                                            if(isset($stage['id']))
                                            {
                                              if($stage['id'] == $user['stage']) {
                                                $stage_selected = 'selected';
                                              }
                                            }
                                        ?>
                                            <option value='<?php echo isset($stage['id']) ? $stage['id'] : ''; ?>' <?php echo $stage_selected ?>><?php echo $stage['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="sector" class="col-sm-5 form-label">Sector *</label>
                            <div class="col-sm-7">
                                <div class="select-style">
                                    <select class="form-control form-input" id="sector" name="sector[]" placeholder="Select Sector" multiple required>
                                        <?php foreach ($sectors as $sector) { 
                                          
                                          $sector_selected = '';
                                            
                                          if(isset($sector['id']))
                                          {
                                            if(in_array($sector['id'], explode(',',$user['sector'])))
                                            {
                                              $sector_selected = 'selected';
                                            }
                                          }
                                          
                                        ?>
                                            <option value='<?php echo isset($sector['id']) ? $sector['id'] : ''; ?>' <?php echo $sector_selected;?>><?php echo $sector['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fb-url" class="col-sm-5 form-label">Facebook</label>
                            <div class="col-sm-7">
                                <input type="text" name="fb_url" value="<?php echo $user['fb_url']; ?>" class="form-control form-input" id="fb-url" placeholder="Facebook URL">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="linkedin-url" class="col-sm-5 form-label">Linkedin</label>
                            <div class="col-sm-7">
                                <input type="text" name="linkedin_url" value="<?php echo $user['linkedin_url']; ?>" class="form-control form-input" id="linkedin-url" placeholder="Linkedin URL">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="twitter-handle" class="col-sm-5 form-label">Twitter</label>
                            <div class="col-sm-7">
                                <input type="text" name="twitter_handle" value="<?php echo $user['twitter_handle']; ?>" class="form-control form-input" id="twitter-handle" placeholder="Twitter handle">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="image" class="col-sm-5 form-label">Company Logo</label>
                            <div class="col-sm-7" id="company-logo">
                            <?php if($user['image']==''){ ?>
                                <div class="fileinput fileinput-new uploader input-group full-width" data-provides="fileinput">
                                    <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput">
                                    <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                                    <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" id="image" name="image"></span>
                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                                </div>
                            <?php }else{ ?>
                               <img src="<?php echo base_url() ?>uploads/users/<?=$user_id?>/<?=$user['image']?>" height="100px" width="150px">                            
                               <a  href="javascript:void(0)" data-toggle="modal" data-target="#profileimgModal" id="change-user-img">Change</a>


                            <?php } ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="banner-img" class="col-sm-5 form-label">Company Banner</label>
                            <div class="col-sm-7" id="banner-img">
                            <?php if($user['banner_image']==''){ ?>
                                <div class="fileinput fileinput-new uploader input-group full-width" data-provides="fileinput">
                                    <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput">
                                    <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                                    <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" id="banner_image" name="banner_image"></span>
                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                                </div>
                            <?php }else{ ?>
                               <img src="<?php echo base_url() ?>uploads/users/<?=$user_id?>/<?=$user['banner_image']?>" height="100px" width="150px">                            
                               <a href="javascript:void(0)" data-toggle="modal" data-target="#bannerimgModal" id="change-banner-img">Change</a>


                            <?php } ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <input type="submit" name="company_submit"  class="btn btn-eq-common pull-right" value="Save & Continue" />
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
            <!-- End Company Info panel -->
      </form>
    </div>
  