<!-- Start Business panel -->
<div class="row">
    <div class="col-xs-4 block-title">
        <h2>Business</h2>
    </div>
    <div class="col-xs-offset-2 col-xs-6">
        <span class="glyphicon glyphicon-cog privacy-icon"></span>
        <div class="privacy-options">
        <div class="input-group">
                    <span class="input-group-addon">Visibility:</span>
            <select name="business" class="form-control form-input" >
                <option value="0" <?php if (isset($privacy['business'])) { echo ($privacy['business'] == 0) ? 'selected' : '';} ?>>All</option>
                <option value="1" <?php if (isset($privacy['business'])) { echo ($privacy['business'] == 1) ? 'selected' : '';} ?>>All Investors & Partners</option>
                <option  value="2" <?php if (isset($privacy['business'])) { echo ($privacy['business'] == 2) ? 'selected' : '';} ?>>Pledged Investors</option>
                <option  value="3" <?php if (isset($privacy['business'])) { echo ($privacy['business'] == 3) ? 'selected' : '';} ?>>Followers</option>
                <option  value="4" <?php if (isset($privacy['business'])) { echo ($privacy['business'] == 4) ? 'selected' : '';} ?>>On Request</option>
            </select>
        </div>  
        </div>                              
    </div>
</div>
<div class="row margin-top-10">
    <form id="investee-info-form" action="<?php echo base_url(); ?>user/edit_investee_info_process" enctype="multipart/form-data" method="post" >
    <input type="hidden" name="user_id" value="<?php echo $investee_id; ?>" />
    <input type="hidden" name="investee_id" value="<?php echo $user_id; ?>" />
    <div id="business" class="accordion2">
        <div class="accordion-inner">
            <div class="form-group row">
                <div class="col-sm-12">
                    <input type="submit" name="business_submit"  class="btn btn-eq-common pull-right" value="Save & Continue" />
                </div>
            </div>
            <div class="form-group row">
                <label for="business_problem_solved" class="col-sm-5 form-label">Problem Being Solved *</label>
                <div class="col-sm-7">
                    <textarea class="form-control form-input form-area" cols="50" rows="15"  id="business_problem_solved" name="business_problem_solved"  placeholder="" required="required"><?php echo isset($user['business_problem_solved']) ? $user['business_problem_solved'] : ''; ?></textarea>
                </div>
            </div>                        
            <div class="form-group row">
                <label for="products-services" class="col-sm-5 form-label">Product / Service Description</label>
                <div class="col-sm-7">
                    <textarea class="form-control form-input form-area" cols="50" rows="15"  id="products-services" name="products_services_business"  placeholder="" required="required"><?php echo isset($user['products_services']) ? $user['products_services'] : ''; ?></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label for="how-different" class="col-sm-5 form-label">Unique Selling Proposition</label>
                <div class="col-sm-7">
                    <textarea class="form-control form-input form-area" cols="50" rows="15" id="how-different" name="how_different"  placeholder=""><?php echo isset($user['how_different']) ? $user['how_different'] : ''; ?></textarea>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <input type="submit" name="business_submit"  class="btn btn-eq-common pull-right" value="Save & Continue" />
                </div>
            </div>                        
        </div>
    </div>
</div>
<!-- End Business panel -->