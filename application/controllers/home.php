<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
    
        function __construct() {
            parent::__construct();
            $this->load->model('user_model');
            $this->load->helper('common_helper');
            $this->load->helper('text_helper');
            
            $banner_flag = isset($this->session->userdata['user_id']) ? '1' : '0';
            $this->load->model('banners_model');
            $this->layout['banners']=  $this->banners_model->find_benners($banner_flag);
            $this->layout['unseen_notifications']=  $this->user_model->get_unseen_notification_count($this->session->userdata('user_id'));
            $this->layout['panels'] = $this->platform();
            
            $sectors = $this->user_model->sectors();
            $stages = $this->user_model->startup_stages();
            $this->layout['stages'] = $stages;
            $this->layout['sectors']= $sectors;

            $this->load->library("pagination");
        }

	public function index()
	{
      
           if( $this->session->userdata('session_id') ){
                if ($this->session->userdata('role') == 'investee') {
                redirect('investee/index');
            } else if ($this->session->userdata('role') == 'investor') {
                redirect('investor/index');
            } else if ($this->session->userdata('role') == 'channel_partner') {
                redirect('channel_partner/index');
            }
            }
            parse_str($_SERVER['QUERY_STRING'],$_GET);
           // $this->load->model('banners_model');
            //$this->layout['banners']=  $this->banners_model->find_benners();
            
            $sectors = $this->user_model->active_sectors();
            $stages = $this->user_model->startup_stages();
            $locations = $this->user_model->get_locations();
            if(isset($_GET['sector']) && $_GET['sector'] != ''){
                $sector_id = $_GET['sector'];
                $investees = $this->user_model->get_investees_by_sector($sector_id); 
            }elseif(isset($_GET['size']) && $_GET['size'] != ''){
                $size_id = $_GET['size'];
                $investees = $this->user_model->get_investees_by_investement_sizes($size_id); 
            }elseif(isset($_GET['stage']) && $_GET['stage'] != ''){
                $stage_id = $_GET['stage'];
                $investees = $this->user_model->get_investees_by_stage($stage_id); 
            }elseif(isset($_GET['location']) && $_GET['location'] != ''){
                $location = $_GET['location'];
                $investees = $this->user_model->get_investees_by_location($location); 
            }elseif(isset($_GET['status']) && $_GET['status'] != ''){
                $status = $_GET['status'];
                $investees = $this->user_model->get_investees_by_status($status); 
            }
            else{
               $investees = $this->user_model->get_featured_investees(); 
            }
            
            $email_settings = $this->user_model->get_email_settings(); 
            $this->session->set_userdata('signup_email', $email_settings->signup_email);
         $this->session->set_userdata('pledge_email',$email_settings->pledge_email); 
             //$this->session->set_userdata('activate_email', $email_settings->activate_email);
          // echo "<pre>"; print_r($this->session->userdata); die;
            $viewData['users'] = $investees;
            $viewData['sectors'] = $sectors;
            $viewData['stages'] = $stages;
            $viewData['locations'] = $locations;
            $viewData['blogs']=$this->_get_blogs();

            $funded_investees = $this->user_model->get_funded_investees(4);
            $viewData['funded_users'] = $funded_investees;
            $viewData['funded_total_result']= $this->user_model->get_count(); 

           //print_r($viewData['blogs']); die();
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('home/index', $bodyData, true);
            $this->layout['show_sticky'] = true;
            $this->load->view('layouts/home', $this->layout);
	}
    private function _get_blogs()
    {
        $this->load->library('rssparser');                          // load library
        $this->rssparser->set_feed_url('https://equitycrest.wordpress.com/feed/');  // get feed
        $this->rssparser->set_cache_life(30);                       // Set cache life time in minutes
        $blog_list = $this->rssparser->getFeed(3); 
                    
        return $blog_list;
    }

    function search()
    {
         $role = $this->session->userdata('role');
            if($role){
                $userId = $this->session->userdata('user_id');
                $this->user_model->update_notifications($userId, array('seen' => 1));
                $notifications = $this->user_model->get_notifications($userId, 4);
                $viewData['notifications'] = $notifications;
            }
        $sectors = $this->user_model->sectors();
        $stages = $this->user_model->startup_stages();

        $options = array();
        $sector_arr=array();
        $stage_arr=array();
        $location_arr=array();
        $size_arr=array();
        $CriteriaSelected = false;

        if (isset($_GET['sector']) && $_GET['sector'] != '') {
            foreach($_GET['sector'] as $sector)
            {
               array_push($sector_arr,  $sector);
            }
            $CriteriaSelected = true;
        } 
        $options['sector'] = $sector_arr;

        if (isset($_GET['stage']) && $_GET['stage'] != '') {
            foreach($_GET['stage'] as $stage)
            {
               array_push($stage_arr,  $stage);
            }
           
            $CriteriaSelected = true;
        } 
        $options['stage'] = $stage_arr;

        if (isset($_GET['name']) && $_GET['name'] != '') {
            $options['name'] = $_GET['name'];
            $CriteriaSelected = true;
        } else {
            $options['name'] = '';
        }

        if (isset($_GET['size']) && $_GET['size'] != '') {
            foreach($_GET['size'] as $size)
            {
               array_push($size_arr,  $size);
            }
           
            $CriteriaSelected = true;
        } 
         $options['size'] = $size_arr;
        if (isset($_GET['location']) && $_GET['location'] != '') 
        {

            foreach($_GET['location'] as $location)
            {
               array_push($location_arr,  $location);
            }
            $CriteriaSelected = true;
        }  
        $options['location'] = $location_arr;

        if ($CriteriaSelected == true){
            $investees = $this->user_model->search_investees($options, 9); 
            $viewData['total_result']= $this->user_model->get_count(); 
            //  echo $this->db->last_query(); die(); 
        } 
        else 
        {
            $investees = $this->user_model->get_all_investees(); 
            $viewData['total_result']= $this->user_model->get_count(); 

        }
        
        $viewData['users'] = $investees;
        $viewData['stages'] = $stages;
        $viewData['sectors'] = $sectors;
        $viewData['users'] = $investees;
        $viewData['searchOptions'] = $options;
        $bodyData = $viewData; 
            
        $this->layout['content'] = $this->load->view('home/search', $bodyData, true);
        $this->load->view('layouts/'.$role, $this->layout);
    }

        public function add_investee(){
            $data=array();
            $investees = $this->user_model->get_investees(9);
            foreach($investees as $investee){
                $data['user']=$investee;
                $this->load->view('front-end/grid-single-item', $data);
            }
        }
        public function file(){
            $response = array('response' => '');
            $this->load->view('upload_form', $response);
        }
        
        function do_upload()
	{
		$config['allowed_types'] = 'pdf|doc|jpg|png|gif';
		$config['max_size']	= '0';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';

		$this->load->library('upload', $config);
                $file1 = "file1";
                $file2 = "file2";
                $response = array();
                $config['upload_path'] = './uploads/1';
                $this->upload->initialize($config);
                if(!$this->upload->do_upload($file1)){
                    $response['response']['error'][] = $this->upload->display_errors();
                }else{
                    $data = $this->upload->data();
                    $response['response']['success'][] = $data['file_name'];
                }
                $config['upload_path'] = './uploads/2';
                $this->upload->initialize($config);
                if(!$this->upload->do_upload($file2)){
                    $response['response']['error'][] = $this->upload->display_errors();
                }else{
                    $data = $this->upload->data();
                    $response['response']['success'][] = $data['file_name'];
                }
                 $this->load->view('upload_form', $response);
//                $this->upload->do_upload($file2);
//		if ( ! $this->upload->do_upload())
//		{
//			$error = array('error' => $this->upload->display_errors());
//
//			$this->load->view('upload_form', $error);
//		}
//		else
//		{
//			$data = array('upload_data' => $this->upload->data());
//
//			$this->load->view('upload_success', $data);
//		}
	}

        function contact_us(){
            $viewData = array();
            $bodyData = $viewData;
            $role = $this->session->userdata('role');
            if($role){
                $userId = $this->session->userdata('user_id');
                $this->user_model->update_notifications($userId, array('seen' => 1));
                $notifications = $this->user_model->get_notifications($userId, 4);
                $viewData['notifications'] = $notifications;
                $bodyData = $viewData;
            }
            $this->layout['content'] = $this->load->view('home/contact_us', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
            
        }
        function about_us(){
            $viewData = array();
            $bodyData = $viewData;
            $role = $this->session->userdata('role');
            if($role){
                $userId = $this->session->userdata('user_id');
                $this->user_model->update_notifications($userId, array('seen' => 1));
                $notifications = $this->user_model->get_notifications($userId, 4);
                $viewData['notifications'] = $notifications;
                $bodyData = $viewData;
            }
            $this->layout['content'] = $this->load->view('home/about_us', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }
        function news_top_stories(){
            $viewData = array();
            $bodyData = $viewData;
            $role = $this->session->userdata('role');
            if($role){
                $userId = $this->session->userdata('user_id');
                $this->user_model->update_notifications($userId, array('seen' => 1));
                $notifications = $this->user_model->get_notifications($userId, 4);
                $viewData['notifications'] = $notifications;
                $bodyData = $viewData;
            }
            $this->layout['content'] = $this->load->view('home/news_top_stories', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }
        function events(){
            $viewData = array();
            $bodyData = $viewData;
            $role = $this->session->userdata('role');
            if($role){
                $userId = $this->session->userdata('user_id');
                $this->user_model->update_notifications($userId, array('seen' => 1));
                $notifications = $this->user_model->get_notifications($userId, 4);
                $viewData['notifications'] = $notifications;
                $bodyData = $viewData;
            }
            $this->layout['content'] = $this->load->view('home/events', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }
        function faqs(){
            $viewData = array();
            $bodyData = $viewData;
            $role = $this->session->userdata('role');
            if($role){
                $userId = $this->session->userdata('user_id');
                $this->user_model->update_notifications($userId, array('seen' => 1));
                $notifications = $this->user_model->get_notifications($userId, 4);
                $viewData['notifications'] = $notifications;
                $bodyData = $viewData;
            }
            $this->layout['content'] = $this->load->view('home/faqs', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }
        function event_details(){
            $viewData = array();
            $bodyData = $viewData;
            $role = $this->session->userdata('role');
            if($role){
                $userId = $this->session->userdata('user_id');
                $this->user_model->update_notifications($userId, array('seen' => 1));
                $notifications = $this->user_model->get_notifications($userId, 4);
                $viewData['notifications'] = $notifications;
                $bodyData = $viewData;
            }
            $this->layout['content'] = $this->load->view('home/event_details', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }
        function terms_of_use(){
            $viewData = array();
            $bodyData = $viewData;
            $role = $this->session->userdata('role');
            if($role){
                $userId = $this->session->userdata('user_id');
                $this->user_model->update_notifications($userId, array('seen' => 1));
                $notifications = $this->user_model->get_notifications($userId, 4);
                $viewData['notifications'] = $notifications;
                $bodyData = $viewData;
            }
            $this->layout['content'] = $this->load->view('home/terms_of_use', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }
        function cookie_policy(){
            $viewData = array();
            $bodyData = $viewData;
            $role = $this->session->userdata('role');
            if($role){
                $userId = $this->session->userdata('user_id');
                $this->user_model->update_notifications($userId, array('seen' => 1));
                $notifications = $this->user_model->get_notifications($userId, 4);
                $viewData['notifications'] = $notifications;
                $bodyData = $viewData;
            }
            $this->layout['content'] = $this->load->view('home/cookie', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }
        function privacy_policy(){
            $viewData = array();
            $bodyData = $viewData;
            $role = $this->session->userdata('role');
            if($role){
                $userId = $this->session->userdata('user_id');
                $this->user_model->update_notifications($userId, array('seen' => 1));
                $notifications = $this->user_model->get_notifications($userId, 4);
                $viewData['notifications'] = $notifications;
                $bodyData = $viewData;
            }
            $this->layout['content'] = $this->load->view('home/privacy_policy', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }
        function how_it_works(){
            $viewData = array();
            $bodyData = $viewData;
            $role = $this->session->userdata('role');
            if($role){
                $userId = $this->session->userdata('user_id');
                $this->user_model->update_notifications($userId, array('seen' => 1));
                $notifications = $this->user_model->get_notifications($userId, 4);
                $viewData['notifications'] = $notifications;
                $bodyData = $viewData;
            }
            $this->layout['content'] = $this->load->view('home/how_it_works', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }
        function management_team(){
            $this->load->model('banners_model');
            $this->layout['banners']=  $this->banners_model->find_benners();
            $viewData = array();
            $bodyData = $viewData;
            $role = $this->session->userdata('role');
            if($role){
                $userId = $this->session->userdata('user_id');
                $this->user_model->update_notifications($userId, array('seen' => 1));
                $notifications = $this->user_model->get_notifications($userId, 4);
                $viewData['notifications'] = $notifications;
                $bodyData = $viewData;
            }
            $this->layout['content'] = $this->load->view('home/management_team', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);        
        }
        function account_setting(){
            $this->load->model('banners_model');
            $this->layout['banners']=  $this->banners_model->find_benners();
            $viewData = array();
            $bodyData = $viewData;
            $role = $this->session->userdata('role');
            if($role){
                $userId = $this->session->userdata('user_id');
                $this->user_model->update_notifications($userId, array('seen' => 1));
                $notifications = $this->user_model->get_notifications($userId, 4);
                $viewData['notifications'] = $notifications;
                $bodyData = $viewData;
            }
            $this->layout['content'] = $this->load->view('home/account_setting', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }
        function refer_company(){
            $this->load->model('banners_model');
            $this->layout['banners']=  $this->banners_model->find_benners();
            $viewData = array();
            $bodyData = $viewData;
            $role = $this->session->userdata('role');
            if($role){
                $userId = $this->session->userdata('user_id');
                $this->user_model->update_notifications($userId, array('seen' => 1));
                $notifications = $this->user_model->get_notifications($userId, 4);
                $viewData['notifications'] = $notifications;
                $bodyData = $viewData;
            }
            $this->layout['content'] = $this->load->view('home/refer_company', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }
        function investor_all($offset = 0){
            $investors = $this->user_model->get_all_investors();
            
            //uasort($investors,  array($this,"cmp"));
           
            $viewData['investors'] = $this->array_orderby($investors, 'investor_type', SORT_ASC, 'name', SORT_ASC);
            
            $tot_investors = count($investors); 
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

            $bodyData = $viewData;
            $viewData["links"] = $this->pagination->create_links();

            $role = $this->session->userdata('role');
            
            if($role){
                $userId = $this->session->userdata('user_id');
                $this->user_model->update_notifications($userId, array('seen' => 1));
                $notifications = $this->user_model->get_notifications($userId, 4);
                $viewData['notifications'] = $notifications;
                $bodyData = $viewData;
				

				if ($role == 'channel_partner')$this->layout['active'] = 4;

                $this->layout['content'] = $this->load->view('home/investor_all', $bodyData, true);
                $this->load->view('layouts/'.$role, $this->layout);
            }else{
                $this->layout['content'] = $this->load->view('home/investor_all', $bodyData, true);
                $this->load->view('layouts/front', $this->layout);
            }
        }
        
        function array_orderby()
		{
		    $args = func_get_args();
		    $data = array_shift($args);
		    
		    foreach ($args as $n => $field) {
		        if (is_string($field)) {
		            $tmp = array();
		            foreach ($data as $key => $row)
		                $tmp[$key] = $row[$field];
		            $args[$n] = $tmp;
		            }
		    }
		    $args[] = &$data;
		    call_user_func_array('array_multisort', $args);
		    return array_pop($args);
		}
		
		function cmp($a, $b)
		{    
		    return $a['investor_type'] > $b['investor_type'] ? 1 : -1;
		}
  
        function portfolio_all(){
            if ($this->session->userdata('user_id') == '') {
                 redirect('user/login');
            }
            $role = $this->session->userdata('role');
            
            if($role){
                $userId = $this->session->userdata('user_id');
                $this->user_model->update_notifications($userId, array('seen' => 1));
                $notifications = $this->user_model->get_notifications($userId, 4);
                $viewData['notifications'] = $notifications;
            }
            $sectors = $this->user_model->sectors();
            $stages = $this->user_model->startup_stages();

            $options = array();
            $sector_arr=array();
            $stage_arr=array();
            $location_arr=array();
            $size_arr=array();
            $CriteriaSelected = false;

            if (isset($_GET['sector']) && $_GET['sector'] != '') {
                foreach($_GET['sector'] as $sector)
                {
                   array_push($sector_arr,  $sector);
                }
                $CriteriaSelected = true;
            } 
            $options['sector'] = $sector_arr;

            if (isset($_GET['stage']) && $_GET['stage'] != '') {
                foreach($_GET['stage'] as $stage)
                {
                   array_push($stage_arr,  $stage);
                }
               
                $CriteriaSelected = true;
            } 
            $options['stage'] = $stage_arr;

            if (isset($_GET['name']) && $_GET['name'] != '') {
                $options['name'] = $_GET['name'];
                $CriteriaSelected = true;
            } else {
                $options['name'] = '';
            }

            if (isset($_GET['size']) && $_GET['size'] != '') {
                foreach($_GET['size'] as $size)
                {
                   array_push($size_arr,  $size);
                }
               
                $CriteriaSelected = true;
            } 
            $options['size'] = $size_arr;
            if (isset($_GET['location']) && $_GET['location'] != '') {
                foreach($_GET['location'] as $location)
                {
                   array_push($location_arr,  $location);
                }
                $CriteriaSelected = true;
            }  
            $options['location'] = $location_arr;

            if ($CriteriaSelected == true){
                $investees = $this->user_model->search_investees($options, 9);   
                $viewData['users'] = $investees;
              //  echo $this->db->last_query(); die(); 
            } else {
                // $investees = $this->user_model->get_all_investees(); 
                $investees_funded = $this->user_model->get_funded_investees(); 
                $investees_raised = $this->user_model->get_raised_investees(); 

                $viewData['users_funded'] = $investees_funded;
                $viewData['users_raised'] = $investees_raised;
            }

            // echo $this->db->last_query(); die(); 
        
            // $viewData['users'] = $investees;
            $viewData['stages'] = $stages;
            $viewData['sectors'] = $sectors;
            $viewData['searchOptions'] = $options;
            $bodyData = $viewData; 
            
			if ($role)
            {	
				if ($role == 'channel_partner'){
                    $this->layout['active'] = 3;
                }
                $this->layout['content'] = $this->load->view('home/portfolio_all', $bodyData, true); 
                $this->load->view('layouts/'.$role, $this->layout);
            }else{
                $this->layout['content'] = $this->load->view('home/portfolio_all', $bodyData, true);
                $this->load->view('layouts/front', $this->layout);
            }            
        }
        function partners(){    
            $viewData = array();
            $bodyData = $viewData;
            $role = $this->session->userdata('role');
            if($role){
                $userId = $this->session->userdata('user_id');
                $this->user_model->update_notifications($userId, array('seen' => 1));
                $notifications = $this->user_model->get_notifications($userId, 4);
                $viewData['notifications'] = $notifications;
                $bodyData = $viewData;
            }
            $this->layout['content'] = $this->load->view('home/partners', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }
        function advisory_board(){    
            $viewData = array();
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('home/advisory_board', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }
        function women_entrepreneurship(){    
            $viewData = array();
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('home/women_entrepreneurship', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }
        function community(){    
            $viewData = array();
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('home/community', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }
        function careers(){    
            $viewData = array();
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('home/careers', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }

        function nda(){
            $viewData = array();
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('home/nda', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }
        
        function blogs(){    
            $viewData = array();
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('home/blogs', $bodyData, true);
            
            $role = $this->session->userdata('role');
            //if($role){
            //    $this->load->view('layouts/'.$role, $this->layout);
            //}else{
                $this->load->view('layouts/front', $this->layout);
            //}
        }
        
        function platform(){
            $this->load->model('investee_model');
            $data = $this->investee_model->get_stats('sector');
            $data2 = $this->investee_model->get_stats('location');
            $colorArray = array('#F24336','#9C27B0','#2196F3','#4DAF50','#FFC007', '#FF9300', '#F24336','#9C27B0','#2196F3','#4DAF50','#FFC007', '#FF9300',);
            $i = 0;
            foreach($data as $result){
                
                $color = $colorArray[$i]; //'#'.substr(md5(rand()), 0, 6);
                if(!in_array($color, $colorArray)){
                    $colorArray[] = $color;
                }
                ++$i;

                $result['color'] = $color;
                $result['highlight'] = $color;
                 $result['actual'] = $result['value'];
                $result['value'] = $result['value']*10;
                $jsonData[] = $result;
                $finalData['sector'] = $jsonData;
            }

            $colorArray = array('#F24336','#9C27B0','#2196F3','#4DAF50','#FF9300', '#FFC007', '#F24336','#9C27B0','#2196F3','#4DAF50','#FF9300', '#FFC007');
            $i = 0;
            foreach($data2 as $result){
                

                $color = $colorArray[$i];  //$color ='#'.substr(md5(rand()), 0, 6);
                if(!in_array($color, $colorArray)){
                    $colorArray[] = $color;
                }
                ++$i;

                $result['color'] = $color;
                $result['highlight'] = $color;
                $result['actual'] = $result['value'];
                $result['value'] = $result['value']*10;
                $jsonData2[] = $result;
                $finalData['location'] = $jsonData2;
            }
            return $finalData;
        }

        function sticky_pop_up()
        {

            $sticky_pop_up = $this->input->post('sticky_pop_up');
             $this->session->set_userdata('sticky_pop_up' ,$sticky_pop_up);
            echo $this->session->userdata('sticky_pop_up');
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */