<?php

class Admin extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin_model');
        $this->load->model('user_model');
        $this->load->model('investor_model');
        $this->load->model('banners_model');
        $this->load->helper('common');

        if ($this->session->userdata('user_id') == '') {
            if ($this->uri->segment(2) != 'login' && $this->uri->segment(2) != 'login_process') {
                redirect('admin/login');
            }
        }
    }

    public function active_users() {
        parse_str($_SERVER['QUERY_STRING'],$_GET);
        
        $options['status'] = 'active';
        if(isset($_GET['name']) && $_GET['name'] != ''){
            $options['name'] = $_GET['name'];
        }else{
            $options['name'] = '';
        }
            
        if(isset($_GET['company_name']) && $_GET['company_name'] != ''){
           $options['company_name'] = $_GET['company_name'];    
        }else{
            $options['company_name'] = '';
        }

        if(isset($_GET['role']) && $_GET['role'] != ''){
            $options['role'] = $_GET['role'];  
        }else{
            $options['role'] = '';
        }
            
        $users = $this->admin_model->get_users($options);
        $viewData['filter'] = $options;
        $viewData['users'] = $users;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/active_users', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout); 
    }

    public function inactive_users() {
        parse_str($_SERVER['QUERY_STRING'],$_GET);
        
        $options['status'] = 'inactive';
        if(isset($_GET['name']) && $_GET['name'] != ''){
            $options['name'] = $_GET['name'];
        }else{
            $options['name'] = '';
        }
            
        if(isset($_GET['company_name']) && $_GET['company_name'] != ''){
           $options['company_name'] = $_GET['company_name'];    
        }else{
            $options['company_name'] = '';
        }

        if(isset($_GET['role']) && $_GET['role'] != ''){
            $options['role'] = $_GET['role'];  
        }else{
            $options['role'] = '';
        }
            
        $users = $this->admin_model->get_users($options);
        $viewData['filter'] = $options;
        $viewData['users'] = $users;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/inactive_users', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }

    public function index() {
        $users_count = $this->admin_model->get_users_count();
        $investors_count = $this->admin_model->get_investors_count();
        $investees_count = $this->admin_model->get_investees_count();
        $channel_partners_count = $this->admin_model->get_channel_partners_count();

        $viewData['users_count'] = $users_count;
        $viewData['investors_count'] = $investors_count;
        $viewData['investees_count'] = $investees_count;
        $viewData['channel_partners_count'] = $channel_partners_count;

        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/index', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }

    public function investors() {
        $viewData = array();
        $investors = $this->admin_model->get_investors();
        $viewData['investors'] = $investors;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/investors', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }

    public function download($userType) {
        $viewData = array();
        $investors = $this->admin_model->download($userType);
        /*$viewData['investors'] = $investors;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/investors', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);*/

        // print_r($investors); exit;

        $this->load->library('excel');

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->setActiveSheetIndex(0);
        //name the worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Investors');
        
        //set cell A1 content with some text
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Name');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Email');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Role');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Status');
        $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Created Date');
        $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Activated Date');
        
        $i = 2;
        foreach ($investors as $row){
            // print_r($row); exit;
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $row['name']);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $row['email']);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $row['role']);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $row['status']);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $row['created_date']);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $row['activated_on']);

            $i++;
        }
                 
        $filename=$userType.'s.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    public function investor($id) {
        $investor = $this->admin_model->fetch_investor($id);
        $viewData['investor'] = $investor;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/investor', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }

     public function edit_investor($type,$id,$activePanel=null) {
        $viewData['activePanel'] = '';
        if ($activePanel) {
            $viewData['activePanel'] = $activePanel;
        }

        $investor = $this->investor_model->fetch_investor($id);
        $countries = $this->user_model->countries();
        $sectors = $this->user_model->sectors();
        $viewData['sectors'] = $sectors;
        $viewData['countries'] = $countries;
        $viewData['user_id'] = $id;

        $states = $this->user_model->states($investor['country']);
        $portfolios = array();
        $portfolios = $this->investor_model->get_portfolios($id);

        $viewData['user'] = $investor;
        $viewData['portfolios'] = $portfolios;
        $viewData['states'] = $states;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/edit_investor', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }

    public function investees() {

        $investees = $this->admin_model->get_investees();
        $viewData['investees'] = $investees;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/investees', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }

    public function semiactive_investees() {
        $investees = $this->admin_model->get_semiactive_investees();
        $viewData['investees'] = $investees;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/semiactive_investees', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }
    public function funded_investees() {
        $investees = $this->admin_model->get_funded_investees();
        $viewData['investees'] = $investees;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/funded_investees', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }
    public function active_investees() {
        $investees = $this->admin_model->get_active_investees();
        $viewData['investees'] = $investees;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/active_investees', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }

    public function inactive_investees() {
        $investees = $this->admin_model->get_inactive_investees();
        $viewData['investees'] = $investees;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/inactive_investees', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }

    public function active_investors() {
        $investors = $this->admin_model->get_active_investors();
        $viewData['investors'] = $investors;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/active_investors', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }

    public function inactive_investors() {
        $investors = $this->admin_model->get_inactive_investors();
        $viewData['investors'] = $investors;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/inactive_investors', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }
    
    public function active_partners() {
        $partners = $this->admin_model->get_active_partners();
        $viewData['partners'] = $partners;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/active_partners', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }

    public function inactive_partners() {
        $partners = $this->admin_model->get_inactive_partners();
        $viewData['partners'] = $partners;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/inactive_partners', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }
    
    public function partner($id) {
        $partner = $this->admin_model->fetch_partner($id);
        $viewData['partner'] = $partner;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/partner', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }

    public function featured_investees() {
        $investees = $this->admin_model->get_featured_investees();
        $viewData['investees'] = $investees;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/featured_investees', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }
    
    public function update_users() {
        if ($this->input->post()) {
            $ids = $this->input->post('id');
            $status = $this->input->post('status');
            
            $data = array('status' => $status);
            $this->admin_model->update_users($data, $ids);

            redirect('admin/active_users');
            
        }
    }

    public function update_investors() {
        if ($this->input->post()) {
            $ids = $this->input->post('id');
            $status = $this->input->post('status');

            $data = array('status' => $status, 'activated_on' => date('Y-m-d H:i:s'));
            $this->admin_model->update_users($data, $ids);
            redirect('admin/active_investors');
            
        }
    }
    public function update_investor_mail()
    {
	if($this->session->userdata('update_investor_id')!='')
            {
                $user_id=$this->session->userdata('update_investor_id');
            }
            else{
                $user_id=$this->input->post('id');
            }

        $user = $this->investor_model->fetch_investor($user_id);

       $country = $this->investor_model->get_country($user['country']);
       if(empty($country))
       {
        $countrynm = '';
       }
       else
       {
         $countrynm = $country[0]['name'];
       }

      $state = $this->investor_model->get_state($user['state']);
       if(empty($state))
       {
        $statenm = '';
       }
       else
       {
         $statenm = $state[0]['name'];
       }


       $portfolios = $this->investor_model->get_portfolios($user_id); 

      // $states = $this->user_model->states($user['country']);
       
         //email code
        $this->load->library('email');
        $this->email->set_mailtype("html");
        $this->email->from('info@equitycrest.com', 'Equity Crest');
        $this->email->subject('Welcome to Equity Crest');
        $content ='<b>Welcome to Equity Crest</b><br /><br />';
        $content .='<p class="MsoNormal">';
        $content .='<span style="font-size:11pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#444444">Thank you for registering on Equity Crest. Your Equity Crest account has now been activated.</span>';
        $content .='</p><p class="MsoNormal"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#444444">Feel free to log on to Equity Crest to find some interesting investment opportunities.</span></p>';
        $content .='<p class="MsoNormal"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#444444">Please do review your profile in your account settings and update the same when you log in.</span></p>';
        
        // $tab = '<table width="80%" border = "1"><th> Name Of The Company</th><th> Website Url</th>';
        // foreach($portfolios as $port)
        // {
        //   $tab .= '<tr><td>'.$port['name'].'</td><td>'.$port['url'].'</td></tr>'; 
        // }

        // $tab .='</table>';

          $tab = '<ul>';
        foreach($portfolios as $port)
        {
          $tab .= '<li><p class="MsoNormal"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#444444">'.$port['name'].' - <a href="'.$port['url'].'">'.$port['url'].'</a></p></li>'; 
        }

        $tab .='</ul>';
            //email message    

            if($user)
            {

                 $dear_text = '<p class="MsoNormal"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#444444">Dear ' . $user['name'] . ',&nbsp;<br /><br />';
                $this->email->to($user['email']);

               // $this->email->to('gauri.p@tailoredtech.in');

                $content .= '<br><p class="MsoNormal"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#444444">Profile Pic : <img src="'.base_url().'uploads/users/'.$user['user_id'].'/'.$user["image"].'" height="80px" width="80px" ></span></p> ';
                $content .= '<br><p class="MsoNormal"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#444444">1. Type of Investor : '. $user["investor_type"] .' </span></p> ';
                $content .= '<br><p class="MsoNormal"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#444444">2. Name : '. $user['name'] .'</span></p>';
                $content .= '<br><p class="MsoNormal"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#444444">3. Mobile Number : '. $user["mob_no"] .' </span></p> ';
                $content .= '<br><p class="MsoNormal"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#444444">4. Email : '. $user["email"] .' </span></p> ';
                $content .= '<br><p class="MsoNormal"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#444444">5. LinkedIn URL : '. $user["linkedin_url"] .'</span></p>';
                $content .= '<br><p class="MsoNormal"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#444444">6. Twitter URL : '. $user["twitter_handle"] .'</span></p>';
                $content .= '<br><p class="MsoNormal"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#444444">7. Current Organization : '. $user["company_name"] .'</span></p>';
                $content .= '<br><p class="MsoNormal"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#444444">8. Designation  :'. $user["role"] .'</span></p> ';
                $content .= '<br><p class="MsoNormal"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#444444">9. City: '. $user["city"] .' </span></p> ';
                $content .= '<br><p class="MsoNormal"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#444444">10. State: '. $statenm .' </span></p> ';
                $content .= '<br><p class="MsoNormal"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#444444">11. Country: '. $countrynm .' </span></p> ';
                $content .= '<br><p class="MsoNormal"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#444444">12. Sectors of Interest : '. $user['sector_expertise'] .' </span></p> ';
             
                $content .= '<br><p class="MsoNormal"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#444444">13. Past Investment : '. $tab .'</span></p>';
                
    
                $content .='<p></p><p class="MsoNormal"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#444444"><a href="'.site_url().'user/login">Click</a> to login now</span></p><p></p>';
                $content .='<p class="MsoNormal"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#444444">Happy to have you,&nbsp;<br><span style="color:#444444">The Equity Crest Team&nbsp;</span></span></span></p>';
     
                $message= $dear_text.$content;
                $this->email->message($message);
                
                if($this->email->send())
                {
                    echo 'Mail sent successfully';
                }
                else
                {
                   echo 'Error occured while sending Mail'; 
                }
            }
            else
            {
                echo "Invalid User";
            }
     }
        public function update_partners() {
        if ($this->input->post()) {
            $ids = $this->input->post('id');
            $status = $this->input->post('status');
           
            $data = array('status' => $status);
            $this->admin_model->update_users($data, $ids);

            redirect('admin/active_partners');
            
        }
    }

    public function update_investees() {
        if ($this->input->post()) {
            $ids = $this->input->post('id');
            $status = $this->input->post('status');
            if ($status == 'feature') {
                foreach ($ids as $id) {
					//check if the featured investee is already added
                    if ($this->admin_model->isFeatured($id) == 0) {
						$featured = array();
						$featured[] = array('user_id' => $id, 'sort' => 1);
						$this->admin_model->add_featured($featured);
					}
				}
                
                redirect('admin/featured_investees');
            } else if($status == 'active') {
                $data = array('status' => $status, 'activated_on' => date('Y-m-d H:i:s'));
                $this->admin_model->update_users($data, $ids);

                redirect('admin/active_investees');
            } else if($status == 'funded') {
                $data = array('status' => $status);
                $this->admin_model->update_users($data, $ids);

                redirect('admin/funded_investees');
            } 
            else  {
                $data = array('status' => $status);
                $this->admin_model->update_users($data, $ids);

                redirect('admin/semiactive_investees');
            }
        }
    }

    public function remove_featured() {
        if ($this->input->post()) {

            $ids = $this->input->post('id');
            $this->admin_model->delete_featured($ids);

            redirect('admin/featured_investees');
        }
    }

    public function investee($id) {
        $investee = $this->admin_model->fetch_investee($id);
        $viewData['investee'] = $investee;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/investee', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }

    public function login() {
        $viewData = array();
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/login', $bodyData);
    }

    public function login_process() {
        $email = $this->input->post('email');
        $password = md5(trim($this->input->post('password')));
        $user = $this->admin_model->login($email, $password);
        if ($user) {
            $this->session->set_userdata('user_id', $user['id']);
            $this->session->set_userdata('email', $user['email']);
            $this->session->set_userdata('name', $user['name']);
            $this->session->set_userdata('role', $user['role']);

            if ($user['role'] == 'admin') {
                redirect('admin/index');
            } else {
                redirect('home/index');
            }
//            if(isset($this->session->userdata['last_url'])){
//                redirect($this->session->userdata['last_url']);
//            }else{
//                redirect('home/index'); 
//            }
        } else {
            $this->session->set_flashdata('error-msg', ' Please check your Email ID and Password.');
            redirect('admin/login');
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect(base_url() . 'admin/login');
    }

    public function referred() {
        $companies = $this->admin_model->get_referred_companies();
        $viewData['companies'] = $companies;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/referred', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }

    public function advice_requests() {
        $requests = $this->admin_model->get_advice_requests();
        $viewData['requests'] = $requests;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/advice_requests', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }

    public function subscribed_users() {
        $requests = $this->admin_model->get_subscribed_users();
        $viewData['users'] = $requests;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/subscribed_users', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }

    public function banners() {

        $banners = $this->banners_model->get_banners(); //print_r($banners);
        $viewData['banners'] = $banners;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/banners', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }

    public function edit_banner($id = null) {

        if ($this->input->post()) {
            $path = '';
            $config['upload_path'] = $path;
            $config['allowed_types'] = 'jpg|png|gif|jpeg';
            //$config['max_size'] = '2048';

            $this->load->library('upload', $config);


            $banner['id'] = $this->input->post('id', TRUE);
            $banner['banner_title'] = $this->input->post('banner_title', TRUE);
            $banner['banner_text'] = $this->input->post('banner_text', TRUE);
            $banner['background_color'] = $this->input->post('background_color', TRUE);
            $banner['banner_link'] = $this->input->post('banner_link', TRUE);
            $banner['banner_text_font'] = $this->input->post('banner_text_font', TRUE);
            $banner['flag'] = $this->input->post('flag', TRUE) == 0 ? '0' : ($this->input->post('flag', TRUE) == 1?'1':'-1') ;
            // print_r($banner); exit;

                $path = "./uploads/banners";
                $config['upload_path'] = $path;
                $this->upload->initialize($config);

                if (!is_dir($path)) {
                    mkdir($path);
                }
                $file = 'background_image';

                if (!($_FILES['background_image']['size'] == 0)) {
                 if (!$this->upload->do_upload('background_image')) {
                        print_r($this->upload->display_errors());
                    } else {
                        $upload_data = $this->upload->data();
                        $banner['background_image'] = $path.'/'.$upload_data['file_name'];
                    }
                }
                $update = $this->banners_model->update_banner($banner);
            if ($update) {
                redirect('admin/banners');
            } else {
                echo "banner not saved";
            }
        } else {
            $banner = $this->banners_model->get_banner($id);
            $viewData['banner'] = $banner;
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('admin/edit_banner', $bodyData, true);
            $this->load->view('layouts/admin', $this->layout);
        }
    }
    
    public function notifications(){
        $notifications = $this->admin_model->get_all_notifications(); 
        $viewData['notifications'] = $notifications;
      
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/notifications', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }


    public function mailing(){
        $e_settings = $this->user_model->get_email_settings();
         $bodyData['e_settings'] = $e_settings;
      

        if($this->input->post())
        {
             
          $signup_email = $this->input->post('signup_email');
          $pledge_email = $this->input->post('pledge_email');
          $activate_email = $this->input->post('activate_email');
           
            $data['signup_email'] = $signup_email;
            $data['pledge_email'] = $pledge_email;
            $data['activate_email'] = $activate_email;
            $res = $this->admin_model->update_email_settings('email_settings',$data);
            if($res)
                $this->session->set_flashdata('success','E-mail settings updated');
            else
                $this->session->set_flashdata('error','Failed to update E-mail settings');

            redirect('admin/mailing');
        }

        $this->layout['content'] = $this->load->view('admin/mailing', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }
    
    public function meetings(){
        $meetings = $this->admin_model->get_meetings(); 
        $viewData['meetings'] = $meetings;
      
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/meetings', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
        
    }
    
    public function pledges($investeeId){
        
        $pledges = $this->admin_model->get_pledges($investeeId);
        $viewData['pledges'] = $pledges;
      
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/pledges', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
        
        
    }

    public function pledge_approval(){
        $id=$this->input->post('id');
        $investeeId=$this->input->post('investee_id');
        $investment=$this->input->post('investment');
        $approval=$this->input->post('approval');
        $data= array('approval'=>$approval);
        $this->admin_model->add_approval($id,$data);
        
        $currentFundRaise=$this->admin_model->get_current_fundraise($investeeId);
        $newFundRaise=$currentFundRaise['fund_raise']+$investment;
        $data=array('fund_raise'=>$newFundRaise);
        $this->admin_model->update_fundraise($investeeId,$data);
        
    }
    
    public function qna($investeeId){
        $qna = $this->admin_model->get_qna($investeeId);
        $viewData['qna'] = $qna;
      
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/qna', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }
    
    public function verify_user(){
        
        $userId=$this->input->post('user_id');
        $verify=$this->input->post('verify');
        $data= array('active'=>$verify);
        $this->admin_model->verify_user($userId,$data);
    }
    
    public function delete_user($id){
        $this->db->where('id', $id);
        $this->db->delete('user_master');
        
        $this->db->where('user_id', $id);
        $this->db->delete('investee');
        
        $this->db->where('user_id', $id);
        $this->db->delete('investor');
        redirect('admin/inactive_users');
        
    }
    
    public function change_priority(){
        
        $userId=$this->input->post('user_id');
        $priority=$this->input->post('priority');
        $data= array('sort'=>$priority);
        $this->admin_model->change_priority($userId,$data);
    }

    public function send_investee_email()
    {
       $id = $this->input->post('id'); 
       $investees = $this->admin_model->fetch_investee($id);


            $templatename = 'Investee_Activated';
            $email_to   =   $investees['email'];

         //  $email_to   =   'gauri.p@tailoredtech.in';  
            $full_name = $investees['name'];
            $subject    = 'Investee Activated';
            $email_from = 'info@equitycrest.com';
            
            $vars=array(
                    array(
                        'name' => 'SUBJECT',
                        'content' => 'follow'
                    ),
                  
                     array(
                        'name' => 'FIRST_NAME',
                        'content' => $full_name
                    ),
                                
            );
        
    
            $merge=array(
                    array(
                        'rcpt'=> $email_to,
                        'vars'=> $vars
                        )
            );
            
            $email = send_mandrill_mail($templatename,$email_to,$full_name,$merge,$email_from);

            echo "success";
    }

    public function send_investors_email()
    {

       $id = $this->input->post('id'); 
       $investees = $this->admin_model->fetch_investee($id);
       $investors = $this->admin_model->get_active_investors(); 
       $templatename = 'Investee_Company_Activated';
       $email_from = 'info@equitycrest.com';

        foreach($investors as $investor) {

           $email_to   =   $investor['email'];  
         //   $email_to   =   'gauri.p@tailoredtech.in';  
            $full_name = $investor['name'];
            $subject    = 'Investee Company Activated';
          
            
            $vars=array(
                    array(
                        'name' => 'SUBJECT',
                        'content' => 'follow'
                    ),
                  
                     array(
                        'name' => 'FIRST_NAME',
                        'content' => $full_name
                    ),

                      array(
                        'name' => 'COMPANY_NAME',
                        'content' => $investees['company_name']
                    ),

                     array(
                        'name' => 'NAME',
                        'content' => $investees['name']
                    ), 
                     array(
                        'name' => 'LINKED_IN_URL',
                        'content' => $investees['linkedin_url']
                    ), 
                    array(
                        'name' => 'SECTOR',
                        'content' => get_sectors($investees['sector'],'sectors')
                    ), 
                     array(
                        'name' => 'FUND_RAISE_AMOUNT',
                        'content' => $investees['fund_raise']
                    ),                                         
              ); 
      
            $merge=array(
                        array(
                            'rcpt'=> $email_to,
                            'vars'=> $vars
                            ));
            
            $email = send_mandrill_mail($templatename,$email_to,$full_name,$merge,$email_from);
    }
            echo "success";
    }  
        
}

?>
