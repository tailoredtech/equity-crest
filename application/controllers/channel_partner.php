<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Channel_partner extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('investee_model');
        $this->load->model('user_model');
        $this->load->model('banners_model');
        $this->load->helper('common_helper');
        $this->load->helper('text_helper');

		$this->layout['panels'] = $this->platform();
		
        $this->load->model('banners_model');
        $this->layout['banners']=  $this->banners_model->find_benners();
        $this->layout['unseen_notifications'] = $this->user_model->get_unseen_notification_count($this->session->userdata('user_id'));
    }
    
        function platform(){
            $this->load->model('investee_model');
            $data = $this->investee_model->get_stats('sector');
            $data2 = $this->investee_model->get_stats('location');
            $colorArray = array('#9de219','#033939','#004d38','#006634','#068c35','#90cc38','#3f51b5','#03a9f4','#ff9800','#f9ce1d','#4caf50');
            $i = 0;
            foreach($data as $result){
                
                $color = $colorArray[$i]; //'#'.substr(md5(rand()), 0, 6);
                if(!in_array($color, $colorArray)){
                    $colorArray[] = $color;
                }
                ++$i;

                $result['color'] = $color;
                $result['highlight'] = $color;
                 $result['actual'] = $result['value'];
                $result['value'] = $result['value']*10;
                $jsonData[] = $result;
                $finalData['sector'] = $jsonData;
            }

            $colorArray = array('#3f51b5','#03a9f4','#ff9800','#f9ce1d','#4caf50','#9de219','#033939','#004d38','#006634','#068c35','#90cc38');
            $i = 0;
            foreach($data2 as $result){
                

                $color = $colorArray[$i];  //$color ='#'.substr(md5(rand()), 0, 6);
                if(!in_array($color, $colorArray)){
                    $colorArray[] = $color;
                }
                ++$i;

                $result['color'] = $color;
                $result['highlight'] = $color;
                $result['actual'] = $result['value'];
                $result['value'] = $result['value']*10;
                $jsonData2[] = $result;
                $finalData['location'] = $jsonData2;
            }
            return $finalData;
        }
	
    public function index(){
        $userId = $this->session->userdata('user_id');
        $user = $this->user_model->find_channel_partner($userId);	
	
        if ($user) {
            redirect('home/portfolio_all');
			return;
        }        
		
		$investees = $this->user_model->get_investees(3);
        $viewData['investees'] = $investees;
		
        $countries = $this->user_model->countries();
		 $sectors = $this->user_model->sectors();
        $viewData['countries'] = $countries;	
		$viewData['sectors'] = $sectors;		

        $userId = $this->session->userdata('user_id');
        $this->user_model->update_notifications($userId, array('seen' => 1));
        $notifications = $this->user_model->get_notifications($userId, 4);
        $viewData['notifications'] = $notifications;

        $bodyData = $viewData;
		
        $this->layout['content'] = $this->load->view('user/channel_partner_info', $bodyData, true);
        $this->load->view('layouts/channel_partner', $this->layout);
    }
    
    public function channel_partner_profile() {
        $viewData['active'] = 1;
        $userId = $this->session->userdata('user_id');

        $user = $this->user_model->find_channel_partner($userId);
        if (!$user) {
            redirect('user/channel_partner_info');
        }

        $this->user_model->update_notifications($userId, array('seen' => 1));
        $notifications = $this->user_model->get_notifications($userId, 4);
        $viewData['notifications'] = $notifications;
        $viewData['partner'] = $user;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('channel_partner/profile', $bodyData, true);
        $this->load->view('layouts/channel_partner', $this->layout);
    }
	
    public function refer(){
        $viewData['active']='2';

        $userId = $this->session->userdata('user_id');
        $this->user_model->update_notifications($userId, array('seen' => 1));
        $notifications = $this->user_model->get_notifications($userId, 4);
        $viewData['notifications'] = $notifications;

        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('channel_partner/refer', $bodyData, true);
        $this->load->view('layouts/channel_partner', $this->layout);
    }
    
    public function refer_process(){
        $userId = $this->session->userdata('user_id');
        $insertData = array(
            'type' => $this->input->post('type'),
            'name' => $this->input->post('name'),
            'company_name' => $this->input->post('company_name'),
            'mob_no' => $this->input->post('mobileno'),
            'email' => $this->input->post('email'),
            'user_id' => $userId
        );
        
        $userId = $this->user_model->add_refer($insertData);
        
        $this->session->set_flashdata('success-msg', 'Successfully referred to the Equity Crest');
        redirect('channel_partner/refer');
    }
}

/* End of file investee.php */
/* Location: ./application/controllers/investee.php */