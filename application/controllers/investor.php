<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Investor extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('investor_model');
        $this->load->model('investee_model');
        $this->load->helper('common_helper');
        $this->load->helper('text_helper');

		$this->layout['panels'] = $this->platform();
		
        $this->load->model('banners_model');
        $this->layout['banners'] = $this->banners_model->find_benners();
        $this->layout['unseen_notifications'] = $this->user_model->get_unseen_notification_count($this->session->userdata('user_id'));
        $sectors = $this->user_model->sectors();
        $stages = $this->user_model->startup_stages();
        $locations = $this->user_model->get_locations();
        $this->layout['stages'] = $stages;
        $this->layout['sectors'] = $sectors;
        $this->layout['locations'] = $locations;
        

        function toAscii($str) {
        	$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $str);
        	$clean = strtolower(trim($clean, '-'));
        	$clean = preg_replace("/[\/_|+ -]+/", '-', $clean);

        	return $clean;
		}
    }

        function platform(){
            $this->load->model('investee_model');
            $data = $this->investee_model->get_stats('sector');
            $data2 = $this->investee_model->get_stats('location');
            $colorArray = array('#9de219','#033939','#004d38','#006634','#068c35','#90cc38','#3f51b5','#03a9f4','#ff9800','#f9ce1d','#4caf50');
            $i = 0;
            foreach($data as $result){
                
                $color = $colorArray[$i]; //'#'.substr(md5(rand()), 0, 6);
                if(!in_array($color, $colorArray)){
                    $colorArray[] = $color;
                }
                ++$i;

                $result['color'] = $color;
                $result['highlight'] = $color;
                 $result['actual'] = $result['value'];
                $result['value'] = $result['value']*10;
                $jsonData[] = $result;
                $finalData['sector'] = $jsonData;
            }

            $colorArray = array('#3f51b5','#03a9f4','#ff9800','#f9ce1d','#4caf50','#9de219','#033939','#004d38','#006634','#068c35','#90cc38');
            $i = 0;
            foreach($data2 as $result){
                

                $color = $colorArray[$i];  //$color ='#'.substr(md5(rand()), 0, 6);
                if(!in_array($color, $colorArray)){
                    $colorArray[] = $color;
                }
                ++$i;

                $result['color'] = $color;
                $result['highlight'] = $color;
                $result['actual'] = $result['value'];
                $result['value'] = $result['value']*10;
                $jsonData2[] = $result;
                $finalData['location'] = $jsonData2;
            }
            return $finalData;
        }
		
    public function index() {
        $userId = $this->session->userdata('user_id');
        
        $user = $this->user_model->find_investor($userId);
        /*if (!$user) {
            redirect('user/investor_info');
        }*/
        
        $pledged = $this->investor_model->has_pledged($userId);
        $interest = $this->investor_model->has_interest($userId);
        $pref_sectors = $this->investor_model->has_preferred_sectors($userId);
        
        
        $recommended_investees = $this->user_model->get_featured_investees(4);
        $viewData['recommended_users'] = $recommended_investees;
         $viewData['recommended_total_result']= $this->user_model->get_count(); 
         
         $funded_investees = $this->user_model->get_funded_investees(4);
        $viewData['funded_users'] = $funded_investees;
         $viewData['funded_total_result']= $this->user_model->get_count(); 
         
        $preferred_sectors = $this->investor_model->get_preferred_sectors($userId);
        $preferred_sizes = $this->investor_model->get_preferred_investment_size($userId);
        $preferred_locations = $this->investor_model->get_preferred_locations($userId);
        $preferred_stages = $this->investor_model->get_preferred_investment_stage($userId);
        $preferred_others = $this->investor_model->get_other_preferences($userId);

        $preferred_options = array();
        $pref_sector = array();
        foreach ($preferred_sectors as $value) {
            $pref_sector[] = $value['sector_id'];
        }

        $pref_size = array();
        foreach ($preferred_sizes as $value) {
            $pref_size[] = $value['investment_size_id'];
        }

        $pref_location = array();
        foreach ($preferred_locations as $value) {
            $pref_location[] = $value['location_id'];
        }

        $pref_stage = array();
        foreach ($preferred_stages as $value) {
            $pref_stage[] = $value['investment_stage_id'];
        }

        $pref_other = array();
        foreach ($preferred_others as $value) {
            $pref_other[] = $value['other_preference_id'];
        }

       
        $viewData['preferred_sectors'] = $pref_sector;
        $viewData['preferred_sizes'] = $pref_size;
        $viewData['preferred_locations'] = $pref_location;
        $viewData['preferred_stages'] = $pref_stage;
        $viewData['preferred_others'] = $pref_other;

        if (empty($pref_sector) && empty($pref_size) && empty($pref_location) && empty($pref_stage) && empty($pref_other)) {
            $viewData['pref_set'] = false;
            $viewData['prefered_users'] = array();
            $viewData['prefered_total_result'] = 0;
        } else {
            if (!empty($pref_sector)) {
                $preferred_options['sectors'] = $pref_sector;
            }
            if (!empty($pref_stage)) {
                
                if(!in_array(6, $pref_stage))
                $preferred_options['stages'] = $pref_stage;
            }
            if (!empty($pref_size)) {
                $preferred_options['sizes'] = $pref_size;
            }
            if (!empty($pref_location)) {
                $preferred_options['locations'] = $pref_location;
            }
            if (!empty($pref_other)) {
                $preferred_options['others'] = $pref_other;
            }
            
            $prefered_investees = $this->investor_model->get_preferred_investees($preferred_options, 4);
            $viewData['pref_set'] = true;
            $viewData['prefered_users'] = $prefered_investees;
            $viewData['prefered_total_result']= $this->user_model->get_count(); 
        }

        $followedInvestees = $this->investor_model->get_followed_investees($userId, 4);
        $followedInvestees_total= $this->user_model->get_count();
        $pledge_limit=4;
        if($followedInvestees_total<=4)
        {
            $pledge_limit=4-$followedInvestees_total;
        }
        $pledgedInvestees = $this->investor_model->get_pledged_investees($userId, $pledge_limit);
        $pledgedInvestees_total= $this->user_model->get_count();
        $viewData['tracked_users'] = $followedInvestees+$pledgedInvestees;

        $viewData['tracked_total_result']= $pledgedInvestees_total+ $followedInvestees_total;
        $investees = $this->user_model->get_investees(4);
        $viewData['all_users'] = $investees;
        $viewData['all_total_result']= $this->user_model->get_count();
        $this->user_model->update_notifications($userId, array('seen' => 1));
        $notifications = $this->user_model->get_notifications($userId, 4);
        $viewData['notifications'] = $notifications;

        $bodyData = $viewData; 
        $this->layout['content'] = $this->load->view('investor/home', $bodyData, true);
        $this->load->view('layouts/investor', $this->layout);
    }

     public function edit_profile() {
        $userId = $this->session->userdata('user_id');
        $pledged = $this->investor_model->has_pledged($userId);
        $interest = $this->investor_model->has_interest($userId);
        $pref_sectors = $this->investor_model->has_preferred_sectors($userId);
        
        if (($pledged || $interest)) {
            redirect('investor/tracked');
        } elseif ($pref_sectors) {
            redirect('investor/preferred');
        } else {
            redirect('investor/recommended');
        }
        $investees = $this->user_model->get_investees(3);
        $viewData['users'] = $investees;

        $this->user_model->update_notifications($userId, array('seen' => 1));
        $notifications = $this->user_model->get_notifications($userId, 4);
        $viewData['notifications'] = $notifications;

        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('investor/index', $bodyData, true);
        $this->load->view('layouts/investor', $this->layout);
    }


    public function profile() {
        $viewData['active'] = 5;
        $userId = $this->session->userdata('user_id');

        $user = $this->user_model->find_investor($userId);
//         if (!$user) {
            redirect('user/investor_info');
        //}

        $investor = $this->investor_model->fetch_investor($userId);
        $portfolios = $this->investor_model->get_portfolios($userId);
        $activities = $this->user_model->get_investor_activities($userId, 10);
        $this->user_model->update_notifications($userId, array('seen' => 1));
        $notifications = $this->user_model->get_notifications($userId, 4);
        $viewData['notifications'] = $notifications;
        $viewData['portfolios'] = $portfolios;
        $viewData['activities'] = $activities;
        $viewData['investor'] = $investor;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('investor/profile', $bodyData, true);
        $this->load->view('layouts/investor', $this->layout);
    }

    public function view($userId) {
		$user = $this->user_model->find_investor($userId,'LEFT');
		
        $investor = $this->investor_model->fetch_investor($userId);
        $portfolios = $this->investor_model->get_portfolios($userId);
        $activities = $this->user_model->get_investor_activities($userId, 10);
        $this->user_model->update_notifications($userId, array('seen' => 1));
        $notifications = $this->user_model->get_notifications($this->session->userdata('user_id'), 4);
        $viewData['notifications'] = $notifications;		
        $viewData['portfolios'] = $portfolios;
        $viewData['activities'] = $activities;
        $viewData['investor'] = $user;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('investor/view', $bodyData, true);
        $this->load->view('layouts/' . $this->session->userdata('role'), $this->layout);
    }

    public function set_preferences() {
        $userId = $this->session->userdata('user_id');
        if ($this->input->post('sector')) {
            $data = array();
            $sectors = $this->input->post('sector');
            foreach ($sectors as $key => $sector) {
                $data[$key]['user_id'] = $userId;
                $data[$key]['sector_id'] = $sector;
            }
            $this->investor_model->delete_preferred_sectors($userId);
            $this->investor_model->add_preferred_sectors($data);
        } else {
            $this->investor_model->delete_preferred_sectors($userId);
        }

        if ($this->input->post('location')) {
            $data = array();
            $countries = $this->input->post('location');
            foreach ($countries as $key => $country) {
                $data[$key]['user_id'] = $userId;
                $data[$key]['location_id'] = $country;
            }
            $this->investor_model->delete_preferred_locations($userId);
            $this->investor_model->add_preferred_locations($data);
        } else {
            $this->investor_model->delete_preferred_locations($userId);
        }

        if ($this->input->post('stage')) {
            $data = array();
            $stages = $this->input->post('stage');
            foreach ($stages as $key => $stage) {
                $data[$key]['user_id'] = $userId;
                $data[$key]['investment_stage_id'] = $stage;
            }
            $this->investor_model->delete_preferred_investment_stage($userId);
            $this->investor_model->add_preferred_investment_stage($data);
        } else {
            $this->investor_model->delete_preferred_investment_stage($userId);
        }

        if ($this->input->post('other')) {
            $data = array();
            $others = $this->input->post('other');
            foreach ($others as $key => $other) {
                $data[$key]['user_id'] = $userId;
                $data[$key]['other_preference_id'] = $other;
            }
            $this->investor_model->delete_other_preferences($userId);
            $this->investor_model->add_other_preferences($data);
        } else {
            $this->investor_model->delete_other_preferences($userId);
        }

        if ($this->input->post('size')) {
            $data = array();
            $sizes = $this->input->post('size');
            foreach ($sizes as $key => $size) {
                $data[$key]['user_id'] = $userId;
                $data[$key]['investment_size_id'] = $size;
            }
            $this->investor_model->delete_preferred_investment_size($userId);
            $this->investor_model->add_preferred_investment_size($data);
        } else {
            $this->investor_model->delete_preferred_investment_size($userId);
        }

        redirect('investor/preferred');
    }

    public function tracked() {
        $viewData['active'] = 2;
        $userId = $this->session->userdata('user_id');
        $followedInvestees = $this->investor_model->get_followed_investees($userId);
        $pledgedInvestees = $this->investor_model->get_pledged_investees($userId);
        $viewData['sectors'] =  $this->layout['sectors'];
        $viewData['stages'] =$this->layout['stages'];
        $viewData['locations'] =$this->layout['locations'];
        $viewData['searchOptions']='';
        $this->user_model->update_notifications($userId, array('seen' => 1));
        $notifications = $this->user_model->get_notifications($userId, 4);
        $viewData['notifications'] = $notifications;
        $viewData['followedInvestees'] = $followedInvestees;
        $viewData['pledgedInvestees'] = $pledgedInvestees;
        $viewData['users'] = $followedInvestees+$pledgedInvestees;
        $viewData['total_result']= sizeof($viewData['users']);
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('investor/tracked', $bodyData, true);
        $this->load->view('layouts/investor', $this->layout);
    }

    public function interested() {
        $investees = $this->user_model->get_investees(3);

        $viewData['users'] = $investees;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('investor/interested', $bodyData, true);
        $this->load->view('layouts/investor', $this->layout);
    }

    public function pledged() {
        $userId = $this->session->userdata('user_id');
        $investees = $this->user_model->get_investees(3);

        $this->user_model->update_notifications($userId, array('seen' => 1));
        $notifications = $this->user_model->get_notifications($userId, 4);
        $viewData['notifications'] = $notifications;
        $viewData['users'] = $investees;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('investor/pledged', $bodyData, true);
        $this->load->view('layouts/investor', $this->layout);
    }

    public function recommended() {
        $userId = $this->session->userdata('user_id');
        $viewData['active'] = 4;
        $investees = $this->user_model->get_featured_investees();
        $viewData['total_result']= $this->user_model->get_count(); 
        $viewData['sectors'] =  $this->layout['sectors'];
        $viewData['stages'] =$this->layout['stages'];
        $viewData['locations'] =$this->layout['locations'];
        $viewData['searchOptions']='';
        $this->user_model->update_notifications($userId, array('seen' => 1));
        $notifications = $this->user_model->get_notifications($userId, 4);
        $viewData['notifications'] = $notifications;
        $viewData['users'] = $investees;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('investor/recommended', $bodyData, true);
        $this->load->view('layouts/investor', $this->layout);
    }
    
     public function funded() {
        $userId = $this->session->userdata('user_id');
        $viewData['active'] = 4;
        $investees = $this->user_model->get_funded_investees();
        $viewData['total_result']= $this->user_model->get_count(); 
        $viewData['sectors'] =  $this->layout['sectors'];
        $viewData['stages'] =$this->layout['stages'];
        $viewData['locations'] =$this->layout['locations'];
        $viewData['searchOptions']='';
        $this->user_model->update_notifications($userId, array('seen' => 1));
        $notifications = $this->user_model->get_notifications($userId, 4);
        $viewData['notifications'] = $notifications;
        $viewData['users'] = $investees;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('investor/funded', $bodyData, true);
        $this->load->view('layouts/investor', $this->layout);
    }
    
public function manage_preferences() {
        $viewData['active'] = 3;
        $userId = $this->session->userdata('user_id');

        $preferred_sectors = $this->investor_model->get_preferred_sectors($userId);
        $preferred_sizes = $this->investor_model->get_preferred_investment_size($userId);
        $preferred_locations = $this->investor_model->get_preferred_locations($userId);
        $preferred_stages = $this->investor_model->get_preferred_investment_stage($userId);
        $preferred_others = $this->investor_model->get_other_preferences($userId);

        $preferred_options = array();

        $pref_sector = array();
        foreach ($preferred_sectors as $value) {
            $pref_sector[] = $value['sector_id'];
        }

        $pref_size = array();
        foreach ($preferred_sizes as $value) {
            $pref_size[] = $value['investment_size_id'];
        }

        $pref_location = array();
        foreach ($preferred_locations as $value) {
            $pref_location[] = $value['location_id'];
        }

        $pref_stage = array();
        foreach ($preferred_stages as $value) {
            $pref_stage[] = $value['investment_stage_id'];
        }

        $pref_other = array();
        foreach ($preferred_others as $value) {
            $pref_other[] = $value['other_preference_id'];
        }

        $this->user_model->update_notifications($userId, array('seen' => 1));
        $notifications = $this->user_model->get_notifications($userId, 4);
        $viewData['notifications'] = $notifications;
        $viewData['preferred_sectors'] = $pref_sector;
        $viewData['preferred_sizes'] = $pref_size;
        $viewData['preferred_locations'] = $pref_location;
        $viewData['preferred_stages'] = $pref_stage;
        $viewData['preferred_others'] = $pref_other;
        $viewData['sectors'] =  $this->layout['sectors'];
        $viewData['stages'] =$this->layout['stages'];
        $viewData['locations'] =$this->layout['locations'];
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('investor/manage_preferences', $bodyData, true);
        $this->load->view('layouts/investor', $this->layout);
    }
    public function preferred() {
        $viewData['active'] = 3;
        $userId = $this->session->userdata('user_id');
        $sectors = $this->user_model->sectors();

        $preferred_sectors = $this->investor_model->get_preferred_sectors($userId);
        $preferred_sizes = $this->investor_model->get_preferred_investment_size($userId);
        $preferred_locations = $this->investor_model->get_preferred_locations($userId);
        $preferred_stages = $this->investor_model->get_preferred_investment_stage($userId);
        $preferred_others = $this->investor_model->get_other_preferences($userId);

        $preferred_options = array();

        $pref_sector = array();
        foreach ($preferred_sectors as $value) {
            $pref_sector[] = $value['sector_id'];
        }

        $pref_size = array();
        foreach ($preferred_sizes as $value) {
            $pref_size[] = $value['investment_size_id'];
        }

        $pref_location = array();
        foreach ($preferred_locations as $value) {
            $pref_location[] = $value['location_id'];
        }

        $pref_stage = array();
        foreach ($preferred_stages as $value) {
            $pref_stage[] = $value['investment_stage_id'];
        }

        $pref_other = array();
        foreach ($preferred_others as $value) {
            $pref_other[] = $value['other_preference_id'];
        }

        $this->user_model->update_notifications($userId, array('seen' => 1));
        $notifications = $this->user_model->get_notifications($userId, 4);
        $viewData['notifications'] = $notifications;
        $viewData['preferred_sectors'] = $pref_sector;
        $viewData['preferred_sizes'] = $pref_size;
        $viewData['preferred_locations'] = $pref_location;
        $viewData['preferred_stages'] = $pref_stage;
        $viewData['preferred_others'] = $pref_other;

        if (empty($pref_sector) && empty($pref_size) && empty($pref_location) && empty($pref_stage) && empty($pref_other)) {
            $viewData['pref_set'] = false;
            $investees = $this->user_model->get_investees(9);
        } else {
            if (!empty($pref_sector)) {
                $preferred_options['sectors'] = $pref_sector;
            }
            if (!empty($pref_stage)) {
                
                if(!in_array(6, $pref_stage))
                $preferred_options['stages'] = $pref_stage;
            }
            if (!empty($pref_size)) {
                $preferred_options['sizes'] = $pref_size;
            }
            if (!empty($pref_location)) {
                $preferred_options['locations'] = $pref_location;
            }
            if (!empty($pref_other)) {
                $preferred_options['others'] = $pref_other;
            }
            
            $investees = $this->investor_model->get_preferred_investees($preferred_options, 30);
            $viewData['pref_set'] = true;
        }

        $viewData['total_result']= $this->user_model->get_count(); 
        $viewData['sectors'] =  $this->layout['sectors'];
        $viewData['stages'] =$this->layout['stages'];
        $viewData['locations'] =$this->layout['locations'];
        $viewData['searchOptions']='';
        $viewData['users'] = $investees;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('investor/preferred', $bodyData, true);
        $this->load->view('layouts/investor', $this->layout);
    }

    public function my_investment() {
        $userId = $this->session->userdata('user_id');

        $this->user_model->update_notifications($userId, array('seen' => 1));
        $notifications = $this->user_model->get_notifications($userId, 4);
        $viewData['notifications'] = $notifications;
        $viewData['active'] = 1;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('investor/my_investment', $bodyData, true);
        $this->load->view('layouts/investor', $this->layout);
    }

    public function ask_question() {
        $investorId = $this->session->userdata('user_id');
        if ($this->input->post()) {
            $question = $this->input->post('question');
            $investee_id = $this->input->post('investeeId');

            $data = array('investor_id' => $investorId,
                'investee_id' => $investee_id,
                'question' => $question,
                'created_date' => date('Y-m-d H:i:s'));

            $this->investor_model->add_question($data);

            $display_text = $this->session->userdata('name') . ' asked question ';
            $link = 'investee/index?tab=questions';
            $notification = array(
                'sender_id' => $investorId,
                'receiver_id' => $investee_id,
                'display_text' => $display_text,
                'link' => $link,
                'date_created' => date('Y-m-d H:i:s')
            );

            $this->user_model->add_notification($notification);
        }
    }

    public function search() {
		$userId = $this->session->userdata('user_id');
        $viewData['active'] = 7;

        $sectors = $this->user_model->sectors();
        $stages = $this->user_model->startup_stages();

        $options = array();

        $CriteriaSelected = false;

        if (isset($_GET['sector']) && $_GET['sector'] != '') {
            $options['sector'] = $_GET['sector'];
            $CriteriaSelected = true;
        } else {
            $options['sector'] = '';
        }

        if (isset($_GET['stage']) && $_GET['stage'] != '') {
            $options['stage'] = $_GET['stage'];
            $CriteriaSelected = true;
        } else {
            $options['stage'] = '';
        }

        if (isset($_GET['name']) && $_GET['name'] != '') {
            $options['name'] = $_GET['name'];
            $CriteriaSelected = true;
        } else {
            $options['name'] = '';
        }

        if (isset($_GET['size']) && $_GET['size'] != '') {
            $options['size'] = $_GET['size'];
            $CriteriaSelected = true;
        } else {
            $options['size'] = '';
        }

        if (isset($_GET['location']) && $_GET['location'] != '') {
            $options['location'] = $_GET['location'];
            $CriteriaSelected = true;
        } else {
            $options['location'] = '';
        }

        if ($CriteriaSelected == true){
            $investees = $this->user_model->search_investees($options, 9);    
        } else {
            $investees = array();
        }
        

        $viewData['stages'] = $stages;
        $viewData['sectors'] = $sectors;
        $viewData['users'] = $investees;
        $viewData['searchOptions'] = $options;
        $notifications = $this->user_model->get_notifications($userId, 4);
        $viewData['notifications'] = $notifications;		
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('investor/search', $bodyData, true);
        $this->load->view('layouts/investor', $this->layout);
    }

    function ajax_portfolio_image_upload() {
        if ($this->input->post()) {
            $id = $this->input->post('portfolio_id');
            $path = "./uploads/portfolios/" . $id;

            if (!is_dir($path)) {
                mkdir($path);
            }

            $config['upload_path'] = $path;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '1024000';
            $config['max_width'] = '1024000';
            $config['max_height'] = '1024000';

            $this->load->library('upload', $config);

            $return_data = array();
            if (!$this->upload->do_upload()) {
                $return_data['error'] = $this->upload->display_errors();
            } else {
                $upload_data = $this->upload->data();
                $data['image'] = $upload_data['file_name'];
                $this->user_model->update_portfolio($data, $id);
                $img_url = 'uploads/portfolios/' . $id . '/' . $upload_data['file_name'];
                $return_data['img'] = $img_url;
                $return_data['id'] = $id;
            }

            echo json_encode($return_data);
        }
    }

    function delete_portfolio() {
        if ($this->input->post()) {
            $id = $this->input->post('company_id');
            $this->user_model->delete_portfolio($id);
        }
    }

    function schedule_meeting_process() {
        if ($this->input->post()) {
            $investorId = $this->session->userdata('user_id');
            $investeeId = $this->input->post('investee_id');
            $meeting_time = $this->input->post('meeting_time');
            $title = $this->input->post('title');
            $remark = $this->input->post('remark');

            $data = array(
                'investor_id' => $investorId,
                'investee_id' => $investeeId,
                'title' => $title,
                'meeting_time' => date('Y-m-d H:i:s', strtotime($meeting_time)),
                'remark' => $remark,
                'created_date' => date('Y-m-d H:i:s')
            );

            $meetingId = $this->investor_model->add_meeting($data);

            $display_text = $this->session->userdata('name') . ' scheduled meeting on ' . date('M, d H:i A', strtotime($meeting_time));
            $link = 'investee/notifications';
            $notification = array(
                'sender_id' => $investorId,
                'receiver_id' => $investeeId,
                'record_id' => $meetingId,
                'display_text' => $display_text,
                'type' => 'meeting',
                'link' => $link,
                'date_created' => date('Y-m-d H:i:s')
            );

            $this->user_model->add_notification($notification);

            $investee = $this->user_model->find_user($investeeId);

            $display_text = $this->session->userdata('name') . ' scheduled meeting on ' . date('M, d H:i A', strtotime($meeting_time)) . ' with ' . $investee['company_name'];
            $link = 'admin/meetings';
            $notification = array(
                'sender_id' => $investorId,
                'record_id' => $meetingId,
                'display_text' => $display_text,
                'type' => 'meeting',
                'table_name' => 'meetings',
                'link' => $link,
                'date_created' => date('Y-m-d H:i:s')
            );

            $this->user_model->add_admin_notification($notification);

            if ($meetingId) {
                echo 'success';
            } else {
                echo 'error';
            }
        }
    }

    function meeting_msg() {
        if ($this->input->post()) {
            $requestId = $this->input->post('recordId');
            $meeting = $this->investee_model->get_meeting_detail($requestId);

            echo $meeting['remark'];
        }
    }

    function reject_meeting_msg() {
        if ($this->input->post()) {
            $requestId = $this->input->post('recordId');
            $meeting = $this->investee_model->get_meeting_detail($requestId);

            echo $meeting['reject_remark'];
        }
    }

    public function notifications() {
       
        $userId = $this->session->userdata('user_id');

        $notifications = $this->user_model->get_all_notifications($userId);
        $viewData['notifications'] = $notifications;

        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('investor/notifications', $bodyData, true);
        $this->load->view('layouts/investor', $this->layout);
    }

    public function offers() {
        $viewData = array();
        $viewData['active'] = "6";
        $userId = $this->session->userdata('user_id');
        $myOffers = $this->investor_model->get_offers($userId);
        $viewData['myOffers'] = $myOffers;
        
        
        $allOffers = $this->investor_model->get_all_offers();
        $viewData['allOffers'] = $allOffers;

        $this->user_model->update_notifications($userId, array('seen' => 1));
        $notifications = $this->user_model->get_notifications($userId, 4);
        $viewData['notifications'] = $notifications;

        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('investor/offers', $bodyData, true);
        $this->load->view('layouts/investor', $this->layout);
    }

    function post_offer() {

        $viewData = array();
        $companies = $this->investor_model->get_all_companies();          // only name & sector.For Offers
        $viewData['companies'] = $companies;
        //print_r($companies);
        //$sectors = $this->user_model->sectors();
        //$viewData['sectors'] = $sectors;        
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('investor/post_offer', $bodyData, true);
        $this->load->view('layouts/investor', $this->layout);
    }

    function post_offer_process() {
        if ($this->input->post()) {
            //die(print_r($this->input->post()));
            $insertData = array(
                'company_id' => $this->input->post('company'),
                'investor_id' => $this->session->userdata('user_id'),
                'sector' => $this->input->post('sector'),
                'shares' => $this->input->post('shares'),
                'holdings' => $this->input->post('holdings'),
                'amount' => $this->input->post('amt'),
                'total_amount' => $this->input->post('total_amt'),
                'date_created' => date('Y-m-d H:i:s')
            );
            $this->investor_model->add_offer($insertData);
            $this->session->set_flashdata('success-msg', 'Your offer has been successfully placed.');
        }
        redirect('investor/offers');
    }

    public function bids() {
        $viewData = array();
        $viewData['active'] = "6";
        $userId = $this->session->userdata('user_id');
        $myBids = $this->investor_model->get_bids($userId);
        $viewData['myBids'] = $myBids;
        
        $allBids = $this->investor_model->get_all_bids($userId);
        $viewData['allBids'] = $allBids;

        $this->user_model->update_notifications($userId, array('seen' => 1));
        $notifications = $this->user_model->get_notifications($userId, 4);
        $viewData['notifications'] = $notifications;

        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('investor/bids', $bodyData, true);
        $this->load->view('layouts/investor', $this->layout);
    }
    
    function post_bid() {

        $viewData = array();
        $companies = $this->investor_model->get_all_companies();          // only name & sector.For Offers
        $viewData['companies'] = $companies;
        //print_r($companies);
        //$sectors = $this->user_model->sectors();
        //$viewData['sectors'] = $sectors;        
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('investor/post_bid', $bodyData, true);
        $this->load->view('layouts/investor', $this->layout);
    }

    function post_bid_process() {
        if ($this->input->post()) {
            //die(print_r($this->input->post()));
            $insertData = array(
                'company_id' => $this->input->post('company'),
                'investor_id' => $this->session->userdata('user_id'),
                'sector' => $this->input->post('sector'),
                'shares' => $this->input->post('shares'),
                'holdings' => $this->input->post('holdings'),
                'amount' => $this->input->post('amt'),
                'total_amount' => $this->input->post('total_amt'),
                'date_created' => date('Y-m-d H:i:s')
            );
            $this->investor_model->add_bid($insertData);
            $this->session->set_flashdata('success-msg', 'Your offer has been successfully placed.');
        }
        redirect('investor/bids');
    }

    function bid_interested() {
        if ($this->input->post()) {
            $investorId = $this->session->userdata('user_id');
            $offerId = $this->input->post('offer_id');
            $bidId = $this->input->post('bid_id');
            $shares = $this->input->post('shares');
            $remark = $this->input->post('remark');

            $data = array(
                'investor_id' => $investorId,
                'offer_id' => $offerId,
                'bid_id' => $bidId,
                'shares' => $shares,
                'remark' => $remark,
                'date_created' => date('Y-m-d H:i:s')
            );

            $bidId = $this->investor_model->add_interested($data);

//            $display_text = $this->session->userdata('name') . ' scheduled meeting on ' . date('M, d H:i A', strtotime($meeting_time));
//            $link = 'investee/notifications';
//            $notification = array(
//                'sender_id' => $investorId,
//                'receiver_id' => $investeeId,
//                'record_id' => $meetingId,
//                'display_text' => $display_text,
//                'type' => 'meeting',
//                'link' => $link,
//                'date_created' => date('Y-m-d H:i:s')
//            );
//
//            $this->user_model->add_notification($notification);
//
//            $investee = $this->user_model->find_user($investeeId);
//
//            $display_text = $this->session->userdata('name') . ' scheduled meeting on ' . date('M, d H:i A', strtotime($meeting_time)) . ' with ' . $investee['company_name'];
//            $link = 'admin/meetings';
//            $notification = array(
//                'sender_id' => $investorId,
//                'record_id' => $meetingId,
//                'display_text' => $display_text,
//                'type' => 'meeting',
//                'table_name' => 'meetings',
//                'link' => $link,
//                'date_created' => date('Y-m-d H:i:s')
//            );
//
//            $this->user_model->add_admin_notification($notification);

            if ($bidId) {
                echo 'success';
            } else {
                echo 'error';
            }
        }
    }

     function view_profile($slug_name)
    {

        $where = "slug_name ='".$slug_name."'";
        $id = $this->investee_model->get_slug_values('user_master',$where,'id'); 
        redirect('user/investor_view/'.$id[0]['id']);
    }

}

/* End of file investor.php */
/* Location: ./application/controllers/investor.php */