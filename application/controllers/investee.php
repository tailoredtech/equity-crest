<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Investee extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('investee_model');
        $this->load->model('investor_model');
        $this->load->model('user_model');
        $this->load->model('banners_model');
        $this->load->helper('common_helper');
        $this->load->helper('text_helper');

        $this->load->model('banners_model');
        $this->layout['banners'] = $this->banners_model->find_benners();
        $this->layout['unseen_notifications'] = $this->user_model->get_unseen_notification_count($this->session->userdata('user_id'));

        $sectors = $this->user_model->sectors();
        $stages = $this->user_model->startup_stages();
        $this->layout['stages'] = $stages;
        $this->layout['sectors'] = $sectors;
        $this->layout['panels'] = $this->platform();
    }

        function platform(){
            $this->load->model('investee_model');
            $data = $this->investee_model->get_stats('sector');
            $data2 = $this->investee_model->get_stats('location');
            $colorArray = array('#9de219','#033939','#004d38','#006634','#068c35','#90cc38','#3f51b5','#03a9f4','#ff9800','#f9ce1d','#4caf50');
            $i = 0;
            foreach($data as $result){
                
                $color = $colorArray[$i]; //'#'.substr(md5(rand()), 0, 6);
                if(!in_array($color, $colorArray)){
                    $colorArray[] = $color;
                }
                ++$i;

                $result['color'] = $color;
                $result['highlight'] = $color;
                 $result['actual'] = $result['value'];
                $result['value'] = $result['value']*10;
                $jsonData[] = $result;
                $finalData['sector'] = $jsonData;
            }

            $colorArray = array('#3f51b5','#03a9f4','#ff9800','#f9ce1d','#4caf50','#9de219','#033939','#004d38','#006634','#068c35','#90cc38');
            $i = 0;
            foreach($data2 as $result){
                

                $color = $colorArray[$i];  //$color ='#'.substr(md5(rand()), 0, 6);
                if(!in_array($color, $colorArray)){
                    $colorArray[] = $color;
                }
                ++$i;

                $result['color'] = $color;
                $result['highlight'] = $color;
                $result['actual'] = $result['value'];
                $result['value'] = $result['value']*10;
                $jsonData2[] = $result;
                $finalData['location'] = $jsonData2;
            }
            return $finalData;
        }
        
    public function index() {

        $active_tab = 'team';
        parse_str($_SERVER['QUERY_STRING'], $_GET);
        if (isset($_GET['tab']) && $_GET['tab'] != '') {
            $active_tab = $_GET['tab'];
        }
        $userId = $this->session->userdata('user_id');
      
        $user = $this->user_model->find_investee($userId);

        // print_r($user); exit;

        if (!$user) {
            redirect('user/investee_info');
        }
        
        $this->view($userId);
        
    }

    public function edit($type='',$investeeId='') 
    {
        // print_r($_FILES); exit;
        $userId = $this->session->userdata('user_id');
        // print_r($userId); exit;
        if(!$userId) {
            redirect('admin/login');
        }
        if ($this->input->post()) {
            $investee=array();
            $path = '';
            $config['upload_path'] = $path;
            // $config['allowed_types'] = 'jpg|png|gif|jpeg|pdf'; 
            //$config['max_size'] = '2048';
            $this->load->library('upload', $config);
            $investee=array();
            $investee_type =$this->input->post('type', TRUE);
            $investeeId = $this->input->post('id', TRUE);
            $path = "./uploads/users/".$investeeId;
            $config['upload_path'] = $path;
            // $this->upload->initialize($config);

            $investee['show_details'] = $this->input->post('show_details') != '' ? $this->input->post('show_details') : 0;
            // echo $investee['show_details']; exit;

            if (!is_dir($path)) {
                mkdir($path);
            }

            // COMPANY BANNER
            $file = 'banner_image';
            $config['allowed_types'] = 'jpg|png|gif|jpeg'; 
            $this->upload->initialize($config);

            if($this->input->post('hidden_banner') != ""){
                if (!($_FILES['banner_image']['size'] == 0)) {
                    if (!$this->upload->do_upload('banner_image')) {
                        print_r($this->upload->display_errors());
                    } else {
                        $upload_data = $this->upload->data();
                        $investee['banner_image'] = $path.'/'.$upload_data['file_name'];
                    }
                }
            }
            else if (!($_FILES['banner_image']['size'] == 0)) {
                if (!$this->upload->do_upload('banner_image')) {
                    print_r($this->upload->display_errors());
                } else {
                    $upload_data = $this->upload->data();
                    $investee['banner_image'] = $path.'/'.$upload_data['file_name'];
                }
            }
            else
            {
                echo "empty";
                 $investee['banner_image'] = "";
            }

            // SNAPSHOT 
            $config['allowed_types'] = 'jpg|png|gif|jpeg|pdf'; 
            $this->upload->initialize($config);
            if($this->input->post('hidden_snapshot') != ""){
                if (!($_FILES['snapshot']['size'] == 0)) {
                    if (!$this->upload->do_upload('snapshot')) {
                        print_r($this->upload->display_errors());
                    } else {
                        $upload_data = $this->upload->data();
                        $investee['snapshot'] = $path.'/'.$upload_data['file_name'];
                    }
                }
            }
            else if (!($_FILES['snapshot']['size'] == 0)) {
                if (!$this->upload->do_upload('snapshot')) {
                    print_r($this->upload->display_errors());
                } else {
                    $upload_data = $this->upload->data();
                    $investee['snapshot'] = $path.'/'.$upload_data['file_name'];
                }
            }
            else
            {
                $investee['snapshot']="";
            }

            $config['allowed_types'] = 'pdf|ppt|pptx'; 
            $config['max_size'] = '30000';   // 30 MB
            $this->upload->initialize($config);
            // COMPANY PRESENTATION
            // print_r($_FILES['presentation_url']); exit;
            if($this->input->post('hidden_presentation_url') != "" || isset($_FILES['presentation_url'])){
                if (!($_FILES['presentation_url']['size'] == 0)) {
                    if (!$this->upload->do_upload('presentation_url')) {
                        print_r($this->upload->display_errors());
                    } else {
                        $upload_data = $this->upload->data();
                        $investee['presentation_url'] = $path.'/'.$upload_data['file_name'];
                    }
                }
            }
            else if (!($_FILES['presentation_url']['size'] == 0)) {
                if (!$this->upload->do_upload('presentation_url')) {
                    print_r($this->upload->display_errors());
                } else {
                    $upload_data = $this->upload->data();
                    $investee['presentation_url'] = $path.'/'.$upload_data['file_name'];
                }
            }
            else
            {
                $investee['presentation_url']="";
            }


            // print_r($this->input->post('hidden_investor_deck'));
            // print_r($_FILES['investor_deck']); exit;

            $config['allowed_types'] = 'pdf|ppt|pptx'; 
            $config['max_size'] = '30000';  
            $deck_path = "./uploads/users/".$this->input->post('id').'/deck_files';
            $config['upload_path'] = $deck_path;
            $this->upload->initialize($config);
            // $this->upload->initialize($config);

            if (!is_dir($deck_path)) {
                mkdir($deck_path);
            }
            if($this->input->post('hidden_investor_deck') != ""){
                if (!($_FILES['investor_deck']['size'] == 0)) {
                    if (!$this->upload->do_upload('investor_deck')) {
                        print_r($this->upload->display_errors());
                    } else {
                        $upload_data = $this->upload->data();
                        $investee['investor_deck'] = $upload_data['file_name'];
                    }
                }
            }
            else if (!($_FILES['investor_deck']['size'] == 0)) {
                if (!$this->upload->do_upload('investor_deck')) {
                    print_r($this->upload->display_errors());
                } else {
                    $upload_data = $this->upload->data();
                    $investee['investor_deck'] = $upload_data['file_name'];
                }
            }
            else
            {
                $investee['investor_deck']="";
            }

            $investee['fund_raise'] = $this->input->post('fund_raise');
            $investee['investment_required'] = $this->input->post('investment_required');
            $investee['equity_offered'] = $this->input->post('equity_offered');
            $investee['commitment_per_investor'] = $this->input->post('commitment_per_investor');
            $investee['accessible_date'] = $this->input->post('accessible_date');
            $investee['updates'] = $this->input->post('updates') != '' ? $this->input->post('updates') : 0;

            $up = $this->user_model->update_investee($investee, $investeeId);
           
            $update=$this->db->affected_rows();
            if ($update){
              
                $this->session->set_flashdata('success-msg', 'Successfully updated Investee.');
            } else 
            {

                $this->session->set_flashdata('error-msg', 'Some error occurred Or No changes were made');
            }
        
            redirect('investee/edit/'.$investee_type.'/'.$investeeId);
        } 
        else
       {
       //  echo "<pre>";
         $investee = $this->investee_model->fetch_investee($investeeId);
         if($investee['user_id'] == "")
         {
            $investee['user_id'] = $investeeId;
         } 
     //   $investee = $this->admin_model->get_active_investees();
            $investees_data = $this->investee_model->get_pledged_investors($investeeId);
            $investee_name = array();
            foreach ($investees_data as $investee_data) {
                $investee_name[] = $investee_data['name'];
            }
            $viewData['investee'] = $investee;  
            $viewData['investeeId'] = $investeeId;
            $viewData['pledged_amt'] = $this->investor_model->get_total_pledged_amount($investeeId);
            $viewData['pledged_by'] = isset($investee_name)?implode(', ',$investee_name):'';
            $viewData['type']=$type;
            $bodyData=$viewData;
            $this->layout['content'] = $this->load->view('admin/edit_investee', $bodyData, true);
            $this->load->view('layouts/admin', $this->layout);
        }
           
    }
    public function edit_profile() {

        $active_tab = 'team';
        parse_str($_SERVER['QUERY_STRING'], $_GET);
        if (isset($_GET['tab']) && $_GET['tab'] != '') {
            $active_tab = $_GET['tab'];
        }
        $userId = $this->session->userdata('user_id');
        $user = $this->user_model->find_investee($userId);
        if (!$user) {
            redirect('user/investee_info');
        }
        $investee = $this->investee_model->fetch_investee($userId);
        $viewData['investee'] = $investee;
        $purposes = $this->user_model->get_investor_use_of_funds($userId);
        $viewData['purposes'] = $purposes;
        $questions = $this->investee_model->get_questions($userId);
        $viewData['questions'] = $questions;
        $pledged_count = $this->investee_model->get_pledged_investors_count($userId);
        $viewData['pledged_count'] = $pledged_count;
        $following_investors_count = $this->investee_model->get_following_investors_count($userId);
        $viewData['following_investors_count'] = $following_investors_count;
        $investors_from_sector_count = $this->user_model->get_investors_by_sector_count($investee['sector']);
        $viewData['investors_from_sector_count'] = $investors_from_sector_count;
        $similar_companies_count = $this->investee_model->get_similar_companies_count($investee['sector_id'], $userId);
        $viewData['similar_companies_count'] = $similar_companies_count;
        $viewData['active_tab'] = $active_tab;

        $this->user_model->update_notifications($userId, array('seen' => 1));
        $notifications = $this->user_model->get_notifications($userId, 4);
        $viewData['notifications'] = $notifications;

        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('investee/index', $bodyData, true);
        $this->load->view('layouts/investee', $this->layout);
    }

    function investee_profile($slug_name)
    {
        /*
        if($this->session->userdata('role') == '')
        {
            $this->session->set_userdata('redirect','investee/investee_profile/'.$slug_name); 
            redirect('user/login');
        } 
        */

        // if (($this->session->userdata('role') == 'investor' || $this->session->userdata('role') == 'channel_partner' || $this->session->userdata('user_id') == $user['user_id']) && $user['status'] != 'funded') 
        //   {
             $where = "slug_name ='".$slug_name."'";
             $id = $this->investee_model->get_slug_values('user_master',$where,'id'); 
             $this->view($id[0]['id']);    
         // }
      }

    public function view($investeeId) {
        
        $userId = $this->session->userdata('user_id') != '' ? $this->session->userdata('user_id') : ''; 

        $isPermittedView = $this->investee_model->check_access($investeeId);

        // print_r($isPermittedView); exit;

        if ($userId == '' && $isPermittedView == 0) {
            redirect('user/login');
        }

        $active_tab = 'team';
        parse_str($_SERVER['QUERY_STRING'], $_GET);
        if (isset($_GET['tab']) && $_GET['tab'] != '') {
            $active_tab = $_GET['tab'];
        }

        // print_r($investeeId); exit;

        $this->user_model->update_notifications($userId, array('seen' => 1));
        $notifications = $this->user_model->get_notifications($userId, 4);
        $viewData['notifications'] = $notifications;
        $investee = $this->investee_model->fetch_investee($investeeId);
        $viewData['investee'] = $investee; 
        $purposes = $this->user_model->get_investor_use_of_funds($investeeId);
        $viewData['purposes'] = $purposes;
        $questions = $this->investee_model->get_questions($investeeId);
        $viewData['questions'] = $questions;
        $team_info = $this->user_model->get_investee_team($investeeId);
        $viewData['team_info'] = $team_info;

        $teamPrivacy = true;
        $businessPrivacy = true;
        $monetizationPrivacy = true;
        $marketPrivacy = true;
        $raisePrivacy = true;
        $financialtPrivacy = true;
        $achievementsPrivacy = true;
        $updatesPrivacy = true;

        $financialForecastPermission = false;
        $financialStatementPermission = false;

        $pledged = $this->session->userdata('pledged');
        if (empty($pledged)) {
            $isPledged = false;
        } else {
            $isPledged = in_array($userId, $this->session->userdata('pledged'));
        }

        $followed = $this->session->userdata('followed');
        if (empty($followed)) {
            $isFollowed = false;
        } else {
            $isFollowed = in_array($userId, $followed);
        }


        $privacy = $this->investee_model->get_privacy($userId);
        $viewData['privacy_team'] = isset($privacy['team'])?$privacy['team']:0;

        if (isset($privacy['team'])) {
            $sectionId = 1; // For Team Section
            if ($privacy['team'] == 1) {
                if ($this->session->userdata('role') == 'investor' || $this->session->userdata('role') == 'channel_partner') {
                    $teamPrivacy = true;    
                } else {
                    $teamPrivacy = false;
                }
                
            } elseif ($privacy['team'] == 2) {
                if (!$isPledged) {
                    $teamPrivacy = false;
                }
            } elseif ($privacy['team'] == 3) {
                if (!$isFollowed) {
                    $teamPrivacy = false;
                }    
            } elseif ($privacy['team'] == 4) {
                if (!$this->investee_model->is_access_granted($sectionId, $userId, $userId)) {               
                    $teamPrivacy = false;
                }
            }
        }

        $viewData['privacy_business'] = isset($privacy['business'])?$privacy['business']:0;
        if (isset($privacy['business'])) {
            $sectionId = 2; // For Business Section
            if ($privacy['business'] == 1) {
                if ($this->session->userdata('role') == 'investor' || $this->session->userdata('role') == 'channel_partner') {
                    $businessPrivacy = true;    
                } else {
                    $businessPrivacy = false;
                }
            } elseif ($privacy['business'] == 2) {
                if (!$isPledged) {
                    $businessPrivacy = false;
                }
            } elseif ($privacy['business'] == 3) {
                if (!$isFollowed) {
                    $businessPrivacy = false;
                }
            } elseif ($privacy['business'] == 4) {
                if (!$this->investee_model->is_access_granted($sectionId, $userId, $userId)) {               
                    $businessPrivacy = false;
                }
            }
        }

        $viewData['privacy_monetization'] = isset($privacy['monetization'])?$privacy['monetization']:0;
        if (isset($privacy['monetization'])) {
            $sectionId = 3; // For Monetization Section
            if ($privacy['monetization'] == 1) {
                if ($this->session->userdata('role') == 'investor' || $this->session->userdata('role') == 'channel_partner') {
                    $monetizationPrivacy = true;    
                } else {
                    $monetizationPrivacy = false;
                }
            } elseif ($privacy['monetization'] == 2) {
                if (!$isPledged) {
                    $monetizationPrivacy = false;
                }
            } elseif ($privacy['monetization'] == 3) {
                if (!$isFollowed) {
                    $monetizationPrivacy = false;
                }
            } elseif ($privacy['monetization'] == 4) {
                if (!$this->investee_model->is_access_granted($sectionId, $userId, $userId)) {               
                    $monetizationPrivacy = false;
                }
            }
        }

        $viewData['privacy_market'] = isset($privacy['market'])?$privacy['market']:0;
        if (isset($privacy['market'])) {
            $sectionId = 4; // For Market Section
            if ($privacy['market'] == 1) { 
                if ($this->session->userdata('role') == 'investor' || $this->session->userdata('role') == 'channel_partner') {
                    $marketPrivacy = true;    
                } else {
                    $marketPrivacy = false;
                }
            } elseif ($privacy['market'] == 2) {
                if (!$isPledged) {
                    $marketPrivacy = false;
                }
            } elseif ($privacy['market'] == 3) {
                if (!$isFollowed) {
                    $marketPrivacy = false;
                }
            } elseif ($privacy['market'] == 4) {
                if (!$this->investee_model->is_access_granted($sectionId, $userId, $userId)) {               
                    $marketPrivacy = false;
                }
            }
        }

        $viewData['privacy_raise'] = isset($privacy['raise'])?$privacy['raise']:0;
        if (isset($privacy['raise'])) {
            $sectionId = 5; // For Raise Section
            if ($privacy['raise'] == 1) {
                if ($this->session->userdata('role') == 'investor' || $this->session->userdata('role') == 'channel_partner') {
                    $raisePrivacy = true;    
                } else {
                    $raisePrivacy = false;
                }
            } elseif ($privacy['raise'] == 2) {
                if (!$isPledged) {
                    $raisePrivacy = false;
                }
            } elseif ($privacy['raise'] == 3) {
                if (!$isFollowed) {
                    $raisePrivacy = false;
                }
            } elseif ($privacy['raise'] == 4) {
                if (!$this->investee_model->is_access_granted($sectionId, $userId, $userId)) {               
                    $raisePrivacy = false;
                }
            }
        }

        $viewData['privacy_financial'] = isset($privacy['financial'])?$privacy['financial']:0;
        if (isset($privacy['financial'])) {
            $sectionId = 6; // For Financial Section
            if ($privacy['financial'] == 1) {
                if ($this->session->userdata('role') == 'investor' || $this->session->userdata('role') == 'channel_partner') {
                    $financialtPrivacy = true;    
                } else {
                    $financialtPrivacy = false;
                }
            } elseif ($privacy['financial'] == 2) {
                if (!$isPledged) {
                    $financialtPrivacy = false;
                }
            } elseif ($privacy['financial'] == 3) {
                if (!$isFollowed) {
                    $financialtPrivacy = false;
                }
            } elseif ($privacy['financial'] == 4) {
                if (!$this->investee_model->is_access_granted($sectionId, $userId, $userId)) {               
                    $financialtPrivacy = false;
                }
            }
        }

        $viewData['privacy_achievements'] = isset($privacy['achievements'])?$privacy['achievements']:0;
        if (isset($privacys['achievements'])) {

            $sectionId = 7; // For Achievements Section
            if ($privacy['achievements'] == 1) {
                if ($this->session->userdata('role') == 'investor' || $this->session->userdata('role') == 'channel_partner') {
                    $achievementsPrivacy = true;    
                } else {
                    $achievementsPrivacy = false;
                }
            } elseif ($privacy['achievements'] == 2) {
                if (!$isPledged) {
                    $achievementsPrivacy = false;
                }
            } elseif ($privacy['achievements'] == 3) {
                if (!$isFollowed) {
                    $achievementsPrivacy = false;
                }
            } elseif ($privacy['achievements'] == 4) {
                if (!$this->investee_model->is_access_granted($sectionId, $userId, $userId)) {               
                    $achievementsPrivacy = false;
                }
            }
        }

        $viewData['privacy_updates'] = isset($privacy['updates'])?$privacy['updates']:0;
        if (isset($privacy['updates'])) {

            $sectionId = 7; // For updates Section
            if ($privacy['updates'] == 1) {
                if ($this->session->userdata('role') == 'investor' || $this->session->userdata('role') == 'channel_partner') {
                    $updatesPrivacy = true;    
                } else {
                    $updatesPrivacy = false;
                }
            } elseif ($privacy['updates'] == 2) {
                if (!$isPledged) {
                    $updatesPrivacy = false;
                }
            } elseif ($privacy['updates'] == 3) {
                if (!$isFollowed) {
                    $updatesPrivacy = false;
                }
            } elseif ($privacy['updates'] == 4) {
                if (!$this->investee_model->is_access_granted($sectionId, $userId, $userId)) {               
                    $updatesPrivacy = false;
                }
            }
        }

        //-- File DownloadPermissions---//
        
        if ($financialtPrivacy == false && $privacy['financial'] <> 4) {
            $financialForecastPermission = false;
            $financialStatementPermission = false;                
        } else {
            if (isset($privacy['financial']) && $privacy['financial'] == 4 && $financialtPrivacy == true) {
                $sectionId = 8; // For Financial Forecast Section

                if ($this->investee_model->is_access_granted($sectionId, $userId, $userId)) {
                    $financialForecastPermission = true;
                }            
                
                $sectionId = 9; // For Financial Statements Section
                if ($this->investee_model->is_access_granted($sectionId, $userId, $userId)) {
                    $financialStatementPermission = true;
                }                
            } else if (isset($privacy['financial']) && $privacy['financial'] <> 4 && $financialtPrivacy == true) {
                $financialForecastPermission = true;
                $financialStatementPermission = true;                
            } else {
                $financialForecastPermission = false;
                $financialStatementPermission = false;                                
            }
        }

        $viewData['teamPrivacy'] = $teamPrivacy;
        $viewData['businessPrivacy'] = $businessPrivacy;
        $viewData['monetizationPrivacy'] = $monetizationPrivacy;
        $viewData['marketPrivacy'] = $marketPrivacy;
        $viewData['raisePrivacy'] = $raisePrivacy;
        $viewData['financialtPrivacy'] = $financialtPrivacy;
        $viewData['achievementsPrivacy'] = $achievementsPrivacy;
        $viewData['updatesPrivacy'] = $updatesPrivacy;
        $viewData['financialForecastPermission'] = $financialForecastPermission;
        $viewData['financialStatementPermission'] = $financialStatementPermission;
        $viewData['active_tab'] = $active_tab;

        $viewData['privacy_team'] = isset($privacy['team'])?$privacy['team']:0;
        $viewData['privacy_business'] = isset($privacy['business'])?$privacy['business']:0;
        $viewData['privacy_monetization'] = isset($privacy['monetization'])?$privacy['monetization']:0;
        $viewData['privacy_market'] = isset($privacy['market'])?$privacy['market']:0;
        $viewData['privacy_raise'] = isset($privacy['raise'])?$privacy['raise']:0;
        $viewData['privacy_financial'] = isset($privacy['financial'])?$privacy['financial']:0;
        $viewData['privacy_achievements'] = isset($privacy['achievements'])?$privacy['achievements']:0;
        $viewData['privacy_updates'] = isset($privacy['updates'])?$privacy['updates']:0;

        if ($this->session->userdata('role') == "investee") {
            $viewData['active'] = '1';
        } else {
            $this->layout['active'] = '0';
        }

        $bodyData = $viewData;

        // print_r($bodyData); exit;

        $this->layout['content'] = $this->load->view('investee/view', $bodyData, true);
        if($this->session->userdata('role') != '')
            $this->load->view('layouts/' . $this->session->userdata('role'), $this->layout);
        else
            $this->load->view('layouts/investee', $this->layout);
    }

    public function post_deal_view($investeeId) {
        $investee = $this->investee_model->fetch_investee($investeeId);
        $postDeal = $this->investee_model->get_post_deal($investeeId);
        if (empty($postDeal)) {
            redirect('investee/view/' . $investeeId); // if post deal is not present
        }
        $financialUpdates = $this->investee_model->get_post_deal_financial_updates($investeeId);
        $businessUpdates = $this->investee_model->get_post_deal_business_updates($investeeId);

        $viewData['financialUpdates'] = $financialUpdates;
        $viewData['businessUpdates'] = $businessUpdates;
        $viewData['post_deal'] = $postDeal;
        $viewData['investee'] = $investee;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('investee/post_deal_view', $bodyData, true);
        $this->load->view('layouts/' . $this->session->userdata('role'), $this->layout);
    }

    public function post_deal_index() {
        $investeeId = $this->session->userdata('user_id');

        $postDeal = $this->investee_model->get_post_deal($investeeId);
        if (empty($postDeal)) {
            redirect('investee/post_deal_info'); // if post deal is not present
        }

        $investee = $this->investee_model->fetch_investee($investeeId);
        $postDeal = $this->investee_model->get_post_deal($investeeId);
        $financialUpdates = $this->investee_model->get_post_deal_financial_updates($investeeId);
        $businessUpdates = $this->investee_model->get_post_deal_business_updates($investeeId);
        $pledges = $this->investee_model->get_pledges($investeeId);
        
        $viewData['pledges'] = $pledges;
        $viewData['financialUpdates'] = $financialUpdates;
        $viewData['businessUpdates'] = $businessUpdates;
        $viewData['post_deal'] = $postDeal;
        $viewData['investee'] = $investee;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('investee/post_deal_index', $bodyData, true);
        $this->load->view('layouts/' . $this->session->userdata('role'), $this->layout);
    }

    public function profile() {
        $userId = $this->session->userdata('user_id');
        $investee = $this->investee_model->fetch_investee($userId);
        $viewData['investee'] = $investee;
        $purposes = $this->user_model->get_investor_use_of_funds($userId);
        $viewData['purposes'] = $purposes;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('investee/profile', $bodyData, true);
        $this->load->view('layouts/investee', $this->layout);
    }

    public function getpledgedamount($investeeId){
        $userId = $this->session->userdata('user_id');

        if ($this->investee_model->is_pledged($userId, $investeeId)) {
            $pledged = $this->investee_model->get_pledged($userId, $investeeId);
            echo $pledged['investment'];           
        } else {
            echo 0;    
        }
    }

    public function pledge() {
        $userId = $this->session->userdata('user_id');
        $slugs = $this->user_model->find_user($userId);

        if ($this->input->post()) {
            $investeeId = $this->input->post('investee_id');
            $amount = str_replace(',', '', $this->input->post('amount'));

            if (!$amount){
                echo json_encode(array('result' => 'please enter the amount you pledge.'));
                return;
            }

            $investeeDetails = $this->user_model->find_investee($investeeId);

            if ($amount < $investeeDetails['commitment_per_investor']){
                echo json_encode(array('result' => 'mimimum amount that you have to pledge is ' . format_money($investeeDetails['commitment_per_investor']) . '.'));
                return;
            }

            if ($amount > $investeeDetails['investment_required']){
                echo json_encode(array('result' => 'Pledged amount cannot be more that investment required.'));
                return;
            }

            if (($investeeDetails['investment_required'] - $investeeDetails['fund_raise']) <= 0 ) {
                echo json_encode(array('result' => 'No pledge required. Fund raised exceeds investment required.'));
                return;
            }

            $percent = 0;
            $fund_raise = $investeeDetails['fund_raise'];
            $investment_required = $investeeDetails['investment_required'];

            if ($this->investee_model->is_pledged($userId, $investeeId)) {
                
                $oldPledged = $this->investee_model->get_pledged($userId, $investeeId);
                $oldPledgedAmount = $oldPledged['investment'];           
                 
                $investee = $this->user_model->find_user($investeeId);
                $this->investee_model->update_fund_raise($investeeId, -1 * $oldPledgedAmount);
                $this->investor_model->update_invested($userId, -1 * $oldPledgedAmount);

                $fund_raise = $fund_raise - $oldPledgedAmount;

                $this->investee_model->update_fund_raise($investeeId, $amount);
                $this->investor_model->update_invested($userId, $amount);
                $pledgeId = $this->investee_model->update_pledge($userId,$investeeId, $amount);

            } else {

                $investee = $this->user_model->find_user($investeeId);
                $insertData = array('investor_id' => $userId, 'investee_id' => $investeeId, 'investment' => $amount);
                $pledgeId = $this->investee_model->pledge($insertData);

                $this->investee_model->update_fund_raise($investeeId, $amount);
                $this->investor_model->update_invested($userId, $amount);
            }

            $display_text = $this->session->userdata('name') . ' has pledged ' . format_money($amount);
            $activity_text = $this->session->userdata('name') . ' has pledged ' . format_money($amount) . ' to <a href="' . base_url() . 'investee/view/' . $investee['id'] . '">' . $investee['company_name'] . '</a>';
            $link = 'investor/view/' . $slugs['slug_name'];
            $notification = array(
                'sender_id' => $userId,
                'receiver_id' => $investeeId,
                'display_text' => $display_text,
                'link' => $link,
                'date_created' => date('Y-m-d H:i:s')
            );
            $pledged = $this->session->userdata('pledged');
            $pledged[] = $investeeId;
            $this->session->set_userdata('pledged', $pledged);
            $this->user_model->add_notification($notification);

            $activity = array(
                'user_id' => $userId,
                'activity' => $activity_text,
                'date_created' => date('Y-m-d H:i:s')
            );

            $this->user_model->add_investor_activity($activity);

            $followingInvestors = $this->user_model->get_following_user_ids($investeeId);

            $display_text = $this->session->userdata('name') . ' has pledged ' . format_money($amount) . ' to ' . $investee['company_name'];
            foreach ($followingInvestors as $followingInvestor) {
                if ($userId != $followingInvestor) {
                    $notification = array(
                        'sender_id' => $userId,
                        'receiver_id' => $followingInvestor,
                        'display_text' => $display_text,
                        'link' => $link,
                        'date_created' => date('Y-m-d H:i:s')
                    );

                    $this->user_model->add_notification($notification);
                }
            }

            $display_text = $this->session->userdata('name') . ' has pledged ' . format_money($amount) . ' to ' . $investee['company_name'];
            $link = 'investor/view/' . $slugs['slug_name'];
            $notification = array(
                'sender_id' => $userId,
                'record_id' => $pledgeId,
                'display_text' => $display_text,
                'type' => 'pledge',
                'table_name' => 'pledge',
                'link' => $link,
                'date_created' => date('Y-m-d H:i:s')
            );

            $this->user_model->add_admin_notification($notification);

             if($investment_required){
                 $fund_raise = $fund_raise + $amount;
                 $percent = ($fund_raise/$investment_required)*100;
             }

            if($this->session->userdata('pledge_email') == 'on')
            {
               $this->send_pledged_email($investee['company_name'],$this->session->userdata('role'),$this->session->userdata('name'),$amount,'','admin');
               // $this->send_pledged_email($investee['company_name'],$this->session->userdata('role'),$this->session->userdata('name'),$amount,$this->session->userdata('email'),'investee');
            }
            echo json_encode(array('result' => 'success', 'raised' => $percent, 'investeeId' => $investeeId));            
        }
    }

    public function send_pledged_email($username,$role,$name,$amount,$user_email = NULL,$to)
    {

       if($to == 'admin') {
            $templatename = 'Investor_Pledge_Admin';
            $email_to   =   EQ_EMAIL;
            $full_name = 'EQ Team';
            $subject    = 'Investor Interest';
            $email_from = 'info@equitycrest.com';
            
            $vars=array(
                    array(
                        'name' => 'SUBJECT',
                        'content' => 'follow'
                    ),
                    array(
                        'name' => 'INVESTOR_TYPE',
                        'content' => $role
                    ),
                     array(
                        'name' => 'FIRST_NAME',
                        'content' => $full_name
                    ),
                      array(
                        'name' => 'INVESTOR_NAME',
                        'content' => $name
                    ),
                      array(
                        'name' => 'COMPANY_NAME',
                        'content' => $username    
                    ),

                       array(
                        'name' => 'PLEDGED_AMOUNT',
                        'content' => $amount
                    ),
                                     
            );
        }

        if($to == 'investee')
        {

            $templatename = 'Investor_Pledge_User';
            $email_to   =   $user_email;  
            $full_name = $this->session->userdata('name');
            $subject    = 'Investor Interest';
            $email_from = 'info@equitycrest.com';
            
            $vars=array(
                    array(
                        'name' => 'SUBJECT',
                        'content' => 'follow'
                    ),
                    array(
                        'name' => 'INVESTOR_TYPE',
                        'content' => $role
                    ),
                     array(
                        'name' => 'FIRST_NAME',
                        'content' => $full_name
                    ),
                      array(
                        'name' => 'INVESTOR_NAME',
                        'content' => $name
                    ),
                     
                       array(
                        'name' => 'PLEDGED_AMOUNT',
                        'content' => $amount
                    ),
                         array(
                        'name' => 'COMPANY_NAME',
                        'content' => $username    
                    ),
                                     
            );
        }
    
            $merge=array(
                    array(
                        'rcpt'=> $email_to,
                        'vars'=> $vars
                        )
            );
            
            $email = send_mandrill_mail($templatename,$email_to,$full_name,$merge,$email_from);
    }

    public function proposal_video() {
        $userId = $this->session->userdata('user_id');
        $investee = $this->investee_model->fetch_investee($userId);
        $viewData['investee'] = $investee;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('investee/proposal_video', $bodyData, true);
        $this->load->view('layouts/investee', $this->layout);
    }

    public function presentation() {
        $userId = $this->session->userdata('user_id');
        $investee = $this->investee_model->fetch_investee($userId);

        $this->user_model->update_notifications($userId, array('seen' => 1));
        $notifications = $this->user_model->get_notifications($userId, 4);
        $viewData['notifications'] = $notifications;

        $viewData['investee'] = $investee;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('investee/presentation', $bodyData, true);
        $this->load->view('layouts/investee', $this->layout);
    }

    public function meeting() {
        $userId = $this->session->userdata('user_id');

        $viewData = array();

        $this->user_model->update_notifications($userId, array('seen' => 1));
        $notifications = $this->user_model->get_notifications($userId, 4);
        $viewData['notifications'] = $notifications;

        $viewData['active'] = "2";
        $this->layout['banners'] = $this->banners_model->find_benners();
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('investee/meeting', $bodyData, true);
        $this->load->view('layouts/investee', $this->layout);
    }

    public function events() {
        $userId = $this->session->userdata('user_id');
        $events = $this->investee_model->get_confirmed_meetings($userId);
        $out = array();
        foreach ($events as $event) {
            $out[] = array(
                'id' => $event['id'],
                'title' => $event['title'],
                'url' => base_url() . 'investee/event_detail/' . $event['id'],
                'start' => strtotime($event['meeting_time']) . '000'
            );
        }

        echo json_encode(array('success' => 1, 'result' => $out));
        exit;
//        $this->load->view('investee/events');
    }

    public function event_detail($id) {
        $event = $this->investee_model->get_meeting_detail($id);

        echo $event['remark'];
    }

    public function offers() {
        $userId = $this->session->userdata('user_id');

        $viewData = array();
        $viewData['active'] = "3";
        $userId = $this->session->userdata('user_id');
        $offers = $this->investee_model->get_offers($userId);
        $viewData['offers'] = $offers;

        $this->user_model->update_notifications($userId, array('seen' => 1));
        $notifications = $this->user_model->get_notifications($userId, 4);
        $viewData['notifications'] = $notifications;

        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('investee/offers', $bodyData, true);
        $this->load->view('layouts/investee', $this->layout);
    }

    function post_offer() {
        //print_r($this->session->all_userdata());
        $userId = $this->session->userdata('user_id');
        $sector = $this->investee_model->get_company_sector($userId);
        $viewData = array();
        $viewData['sector'] = $sector;
//      print_r($viewData);
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('investee/post_offer', $bodyData, true);
        $this->load->view('layouts/investee', $this->layout);
    }

    function post_offer_process() {
        if ($this->input->post()) {
            //die(print_r($this->input->post()));
            $insertData = array(
                'company_id' => $this->input->post('company'),
                'sector' => $this->input->post('sector'),
                'shares' => $this->input->post('shares'),
                'holdings' => $this->input->post('holdings'),
                'amount' => $this->input->post('amt'),
                'total_amount' => $this->input->post('total_amt'),
                'date_created' => date('Y-m-d H:i:s')
            );
            $this->investee_model->add_offer($insertData);
            $this->session->set_flashdata('success-msg', 'Your offer has been successfully placed.');
        }
        redirect('investee/offers');
    }

    public function show_interest() {
        $userId = $this->session->userdata('user_id');
        if ($this->input->post()) {
            $investeeId = $this->input->post('investeeId');
            if ($this->investee_model->is_interested($userId, $investeeId)) {
                echo 'error';
            } else {
                $this->investee_model->show_interest($userId, $investeeId);
                $display_text = $this->session->userdata('name') . ' has shown interest in your proposals';
                $notification = array(
                    'sender_id' => $userId,
                    'receiver_id' => $investeeId,
                    'display_text' => $display_text,
                    'date_created' => date('Y-m-d H:i:s')
                );
                $this->user_model->add_notification($notification);
                echo 'success';
            }
        }
    }

    public function save_privacy() {
        $investeeId = $this->session->userdata('user_id');
        if ($this->input->post()) {
            $key = $this->input->post('optionKey');
            $value = $this->input->post('optionVal');

            $data = array($key => $value);

            $this->investee_model->update_privacy($investeeId, $data);
        }
    }

    public function add_privacy() {
        $this->investee_model->add_all_privacy();
    }

    public function submit_reply() {
        $investeeId = $this->session->userdata('user_id');
        $slugs = $this->user_model->find_user($investeeId);
        if ($this->input->post()) {
            $answer = $this->input->post('answer');
            $question_id = $this->input->post('question_id');

            $data = array('question_id' => $question_id,
                'answer' => $answer,
                'created_date' => date('Y-m-d H:i:s'));

            $this->investee_model->add_reply($data);
            $question = $this->investee_model->get_question($question_id);

            $display_text = $this->session->userdata('name') . ' answered the question ';
            $link = 'investee/view/' . $investeeId . '?tab=questions';
            $notification = array(
                'sender_id' => $investeeId,
                'receiver_id' => $question['investor_id'],
                'display_text' => $display_text,
                'link' => $link,
                'date_created' => date('Y-m-d H:i:s')
            );

            $this->user_model->add_notification($notification);
        }
    }

    public function pledged_investors() {
        $userId = $this->session->userdata('user_id');

        $investors = $this->investee_model->get_pledged_investors($userId, 9);
        $viewData['investors'] = $investors;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('investee/pledged_investors', $bodyData, true);
        $this->load->view('layouts/investee', $this->layout);
    }

    public function following_investors() {
        $userId = $this->session->userdata('user_id');
        $investors = $this->investee_model->get_following_investors($userId, 9);
        $viewData['investors'] = $investors;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('investee/following_investors', $bodyData, true);
        $this->load->view('layouts/investee', $this->layout);
    }

    public function investors_from_sectors() {
        $userId = $this->session->userdata('user_id');
        $investee = $this->investee_model->fetch_investee($userId);
        $investors = $this->user_model->get_investors_by_sector($investee['sector'], 9);
        $viewData['investors'] = $investors;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('investee/investors_from_sectors', $bodyData, true);
        $this->load->view('layouts/investee', $this->layout);
    }

    public function similar_companies() {
        $userId = $this->session->userdata('user_id');
        $investee = $this->investee_model->fetch_investee($userId);
        $investees = $this->investee_model->get_similar_companies($investee['sector_id'], $userId);
        $viewData['users'] = $investees;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('investee/similar_companies', $bodyData, true);
        $this->load->view('layouts/investee', $this->layout);
    }

    public function ask_advice() {
        $investeeId = $this->session->userdata('user_id');
        if ($this->input->post()) {
            $query = $this->input->post('query');

            if ($query == '') {
                echo 'No query specified.';
                return;
            }

            $data = array('user_id' => $investeeId,
                'query' => $query,
                'created_date' => date('Y-m-d H:i:s'));

            $rquestId = $this->investee_model->add_advice_request($data);

            $display_text = $this->session->userdata('company_name') . ' made request to give advice';
            $link = 'admin/advice_requests';
            $notification = array(
                'sender_id' => $investeeId,
                'record_id' => $rquestId,
                'display_text' => $display_text,
                'type' => 'advice',
                'table_name' => 'advice_requests',
                'link' => $link,
                'date_created' => date('Y-m-d H:i:s')
            );

            $this->user_model->add_admin_notification($notification);

            echo 'success';
        }
    }

    public function post_deal_info() {
        $userId = $this->session->userdata('user_id');
        $postDeal = $this->investee_model->get_post_deal($userId);
        if ($postDeal) {
            $financialUpdates = $this->investee_model->get_post_deal_financial_updates($userId);
            $businessUpdates = $this->investee_model->get_post_deal_business_updates($userId);

            $viewData['financialUpdates'] = $financialUpdates;
            $viewData['businessUpdates'] = $businessUpdates;
            $viewData['post_deal'] = $postDeal;
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('investee/edit_post_deal_info', $bodyData, true);
            $this->load->view('layouts/investee', $this->layout);
        } else {
            $bodyData = array();
            $this->layout['content'] = $this->load->view('investee/post_deal_info', $bodyData, true);
            $this->load->view('layouts/investee', $this->layout);
        }
    }

    public function post_deal_info_process() {
        $investeeId = $this->session->userdata('user_id');

        $path = '';
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'pdf|doc|docx|jpg|png|gif|ppt|pptx|xsl|xslx';
        $config['max_size'] = '2048';

        $this->load->library('upload', $config);

        $insertData = array(
            'investee_id' => $investeeId,
            'video_link' => $this->input->post('proposal_video'),
            'presentation' => $this->input->post('presentation'),
            'media' => $this->input->post('media'),
            'awards' => $this->input->post('awards'),
            'testimonials' => $this->input->post('testimonials'),
            'created_date' => date('Y-m-d H:i:s')
        );

        $this->investee_model->add_post_deal($insertData);

        if ($this->input->post('financial_file_name_1')) {
            $financial_update_count = $this->input->post('financial_update_count');

            for ($i = 1; $i <= $financial_update_count; $i++) {

                $financialUpdateData = array(
                    'investee_id' => $investeeId,
                    'file_name' => $this->input->post('financial_file_name_' . $i)
                );

                $financialUpdateId = $this->investee_model->add_post_deal_financial_updates($financialUpdateData);

                $path = "./uploads/post_deals/financial/" . $financialUpdateId;
                $config['upload_path'] = $path;
                $this->upload->initialize($config);

                if (!is_dir($path)) {
                    mkdir($path);
                }
                $file = 'file_financial_' . $i;

                if (!($_FILES[$file]['size'] == 0)) {
                    $financialFile = array();
                    if (!$this->upload->do_upload($file)) {
                        print_r($this->upload->display_errors());
                    } else {
                        $upload_data = $this->upload->data();
                        $financialFile['file'] = $upload_data['file_name'];
                    }

                    $this->investee_model->update_post_deal_financial_updates($financialUpdateId, $financialFile);
                }
            }
        }

        if ($this->input->post('business_file_name_1')) {
            $business_update_count = $this->input->post('business_update_count');

            for ($i = 1; $i <= $business_update_count; $i++) {

                $financialUpdateData = array(
                    'investee_id' => $investeeId,
                    'meeting_update' => $this->input->post('business_file_name_' . $i),
                    'message' => $this->input->post('msg_' . $i),
                );

                $businessUpdateId = $this->investee_model->add_post_deal_business_updates($financialUpdateData);

                $path = "./uploads/post_deals/business/" . $businessUpdateId;
                $config['upload_path'] = $path;
                $this->upload->initialize($config);

                if (!is_dir($path)) {
                    mkdir($path);
                }
                $file = 'file_business_' . $i;

                if (!($_FILES[$file]['size'] == 0)) {
                    $businessFile = array();
                    if (!$this->upload->do_upload($file)) {
                        print_r($this->upload->display_errors());
                    } else {
                        $upload_data = $this->upload->data();
                        $businessFile['meeting_update_file'] = $upload_data['file_name'];
                    }

                    $this->investee_model->update_post_deal_business_updates($businessUpdateId, $businessFile);
                }
            }
        }

        redirect('investee/post_deal_info');
    }

    public function edit_post_deal_info_process() {
        $investeeId = $this->session->userdata('user_id');

        $path = '';
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'pdf|doc|docx|jpg|png|gif|ppt|pptx|xsl|xslx';
        $config['max_size'] = '2048';

        $this->load->library('upload', $config);

        $data = array(
            'video_link' => $this->input->post('proposal_video'),
            'presentation' => $this->input->post('presentation'),
            'media' => $this->input->post('media'),
            'awards' => $this->input->post('awards'),
            'testimonials' => $this->input->post('testimonials')
        );
        $this->investee_model->update_post_deal($investeeId, $data);

        $financial_update_count = $this->input->post('financial_update_count');

        for ($i = 1; $i <= $financial_update_count; $i++) {
            $financialEditId = $this->input->post('financial_id_' . $i);
            if ($financialEditId) {
                $financialUpdateData = array(
                    'file_name' => $this->input->post('financial_file_name_' . $i)
                );

                $this->investee_model->update_post_deal_financial_updates($financialEditId, $financialUpdateData);

                $path = "./uploads/post_deals/financial/" . $financialEditId;
                $config['upload_path'] = $path;
                $this->upload->initialize($config);

                $file = 'file_financial_' . $i;
                if (isset($_FILES[$file])) {
                    if (!($_FILES[$file]['size'] == 0)) {
                        $financialFile = array();
                        if (!$this->upload->do_upload($file)) {
                            print_r($this->upload->display_errors());
                        } else {
                            $upload_data = $this->upload->data();
                            $financialFile['file'] = $upload_data['file_name'];
                        }

                        $this->investee_model->update_post_deal_financial_updates($financialEditId, $financialFile);
                    }
                }
            } else {
                $financialUpdateData = array(
                    'investee_id' => $investeeId,
                    'file_name' => $this->input->post('financial_file_name_' . $i)
                );

                $financialUpdateId = $this->investee_model->add_post_deal_financial_updates($financialUpdateData);

                $path = "./uploads/post_deals/financial/" . $financialUpdateId;
                $config['upload_path'] = $path;
                $this->upload->initialize($config);

                if (!is_dir($path)) {
                    mkdir($path);
                }
                $file = 'file_financial_' . $i;

                if (!($_FILES[$file]['size'] == 0)) {
                    $financialFile = array();
                    if (!$this->upload->do_upload($file)) {
                        print_r($this->upload->display_errors());
                    } else {
                        $upload_data = $this->upload->data();
                        $financialFile['file'] = $upload_data['file_name'];
                    }

                    $this->investee_model->update_post_deal_financial_updates($financialUpdateId, $financialFile);
                }
            }
        }




        $business_update_count = $this->input->post('business_update_count');

        for ($i = 1; $i <= $business_update_count; $i++) {

            $businessEditId = $this->input->post('business_id_' . $i);
            if ($businessEditId) {
                $businessUpdateData = array(
                    'meeting_update' => $this->input->post('business_file_name_' . $i),
                    'message' => $this->input->post('msg_' . $i),
                );

                $this->investee_model->update_post_deal_business_updates($businessEditId, $businessUpdateData);

                $path = "./uploads/post_deals/business/" . $businessEditId;
                $config['upload_path'] = $path;
                $this->upload->initialize($config);


                $file = 'file_business_' . $i;
                if (isset($_FILES[$file])) {
                    if (!($_FILES[$file]['size'] == 0)) {
                        $businessFile = array();
                        if (!$this->upload->do_upload($file)) {
                            print_r($this->upload->display_errors());
                        } else {
                            $upload_data = $this->upload->data();
                            $businessFile['meeting_update_file'] = $upload_data['file_name'];
                        }

                        $this->investee_model->update_post_deal_business_updates($businessEditId, $businessFile);
                    }
                }
            } else {
                $financialUpdateData = array(
                    'investee_id' => $investeeId,
                    'meeting_update' => $this->input->post('business_file_name_' . $i),
                    'message' => $this->input->post('msg_' . $i),
                );

                $businessUpdateId = $this->investee_model->add_post_deal_business_updates($financialUpdateData);

                $path = "./uploads/post_deals/business/" . $businessUpdateId;
                $config['upload_path'] = $path;
                $this->upload->initialize($config);

                if (!is_dir($path)) {
                    mkdir($path);
                }
                $file = 'file_business_' . $i;

                if (!($_FILES[$file]['size'] == 0)) {
                    $businessFile = array();
                    if (!$this->upload->do_upload($file)) {
                        print_r($this->upload->display_errors());
                    } else {
                        $upload_data = $this->upload->data();
                        $businessFile['meeting_update_file'] = $upload_data['file_name'];
                    }

                    $this->investee_model->update_post_deal_business_updates($businessUpdateId, $businessFile);
                }
            }
        }


        redirect('investee/post_deal_info');
    }

    public function notifications() {
        $userId = $this->session->userdata('user_id');

        $notifications = $this->user_model->get_all_notifications($userId);
        $viewData['notifications'] = $notifications;

        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('investee/notifications', $bodyData, true);
        $this->load->view('layouts/investee', $this->layout);
    }

    public function accept_access_request() {
        $investeeId = $this->session->userdata('user_id');
        $slugs = $this->user_model->find_user($investeeId);
        if ($this->input->post()) {
            $requestId = $this->input->post('recordId');
            $notificationId = $this->input->post('notificationId');
            $data = array('status' => 'granted');

            $this->investee_model->update_access_request($data, $requestId);

            $request = $this->user_model->get_access_request($requestId);

            $display_text = $this->session->userdata('company_name') . ' accepted request to access ' . $request['section_name'];
            $link = 'investee/view/' . $slugs['slug_name'];
            $notification = array(
                'sender_id' => $investeeId,
                'receiver_id' => $request['request_by'],
                'display_text' => $display_text,
                'link' => $link,
                'date_created' => date('Y-m-d H:i:s')
            );

            $this->user_model->add_notification($notification);

            $this->user_model->delete_notification($notificationId);

            echo 'success';
        }
    }

    public function reject_access_request() {
        $investeeId = $this->session->userdata('user_id');
        $slugs = $this->user_model->find_user($investeeId);
        if ($this->input->post()) {
            $requestId = $this->input->post('recordId');
            $notificationId = $this->input->post('notificationId');
            $data = array('status' => 'declined');

            $this->investee_model->update_access_request($data, $requestId);

            $request = $this->user_model->get_access_request($requestId);

            $display_text = $this->session->userdata('company_name') . ' declined request to access ' . $request['section_name'];
            $link = 'investee/view/' . $slugs['slug_name'];
            $notification = array(
                'sender_id' => $investeeId,
                'receiver_id' => $request['request_by'],
                'display_text' => $display_text,
                'link' => $link,
                'date_created' => date('Y-m-d H:i:s')
            );

            $this->user_model->add_notification($notification);

            $this->user_model->delete_notification($notificationId);

            echo 'success';
        }
    }

    public function accept_meeting_request() {
        $investeeId = $this->session->userdata('user_id');
          $slugs = $this->user_model->find_user($investeeId);
        if ($this->input->post()) {
            $requestId = $this->input->post('recordId');
            $notificationId = $this->input->post('notificationId');
            $data = array('status' => 'confirmed');

            $this->investee_model->update_meeting_request($data, $requestId);

            $request = $this->investee_model->get_meeting_detail($requestId);

            $display_text = $this->session->userdata('company_name') . ' accepted meeting request made by you on ' . date('M, d H:i A', strtotime($request['meeting_time']));
            $link = 'investee/view/' . $slugs['slug_name'];
            $notification = array(
                'sender_id' => $investeeId,
                'receiver_id' => $request['investor_id'],
                'display_text' => $display_text,
                'link' => $link,
                'date_created' => date('Y-m-d H:i:s')
            );

            $this->user_model->add_notification($notification);

            $investor = $this->user_model->find_user($request['investor_id']);

            $display_text = $this->session->userdata('company_name') . ' accepted meeting request made by ' . $investor['name'] . ' for ' . date('M, d H:i A', strtotime($request['meeting_time']));
            $link = 'admin/meetings';
            $notification = array(
                'sender_id' => $investeeId,
                'record_id' => $requestId,
                'display_text' => $display_text,
                'type' => 'meeting',
                'table_name' => 'meetings',
                'link' => $link,
                'date_created' => date('Y-m-d H:i:s')
            );

            $this->user_model->add_admin_notification($notification);

            $this->user_model->delete_notification($notificationId);

            echo 'success';
        }
    }

    public function reject_meeting_request() {
        $investeeId = $this->session->userdata('user_id');
        if ($this->input->post()) {
            $requestId = $this->input->post('meeting_id');
            $notificationId = $this->input->post('notification_id');
            $reject_remark = $this->input->post('reject_remark');
            
            if ($reject_remark == ''){
                echo 'Please specify the reason for rejection.';
                return;
            }

            $data = array('status' => 'decline', 'reject_remark' => $reject_remark);

            $this->investee_model->update_meeting_request($data, $requestId);

            $request = $this->investee_model->get_meeting_detail($requestId);

            $display_text = $this->session->userdata('company_name') . ' declined meeting request made by you on ' . date('M, d H:i A', strtotime($request['meeting_time']));
            $link = 'investor/notifications';
            $notification = array(
                'sender_id' => $investeeId,
                'receiver_id' => $request['investor_id'],
                'display_text' => $display_text,
                'link' => $link,
                'date_created' => date('Y-m-d H:i:s')
            );

            $this->user_model->add_notification($notification);

            $investor = $this->user_model->find_user($request['investor_id']);

            $display_text = $this->session->userdata('company_name') . ' rejected meeting request made by ' . $investor['name'] . ' for ' . date('M, d H:i A', strtotime($request['meeting_time']));
            $link = 'admin/meetings';
            $notification = array(
                'sender_id' => $investeeId,
                'record_id' => $requestId,
                'display_text' => $display_text,
                'type' => 'meeting',
                'table_name' => 'meetings',
                'link' => $link,
                'date_created' => date('Y-m-d H:i:s')
            );

            $this->user_model->add_admin_notification($notification);

            $this->user_model->delete_notification($notificationId);

            echo 'success';
        }
    }

     function view_profile($slug_name)
     {

        $where = "slug_name ='".$slug_name."'";
        $id = $this->investee_model->get_slug_values('user_master',$where,'id'); 
        redirect('user/investor_view/'.$id[0]['id']);
     }

     function download_file()
     {
        // print_r($this->input->get()); exit;
        $type           = strtoupper($this->input->get('type_doc'));
        $company_name   = $this->input->get('comp');
        $name           = $this->input->get('investee_name');
        $downloader_name= $this->session->userdata['name'];
        $templatename   = 'Download_Notification_Mail';
        // $email_to       = $this->input->get('email');
        $email_to       = EQ_DOWNLOAD_EMAIL;
        $full_name      = 'EQ Team';
        $subject        = $company_name.' - '.$type.' Downloaded';
        $email_from     = 'info@equitycrest.com';
        $email_bcc       = 'rohini.b@tailoredtech.in';

        $vars=array(
            array(
                'name' => 'SUBJECT',
                'content' => $subject
            ),
            array(
                'name' => 'FIRST_NAME',
                'content' => $name
            ),
              array(
                'name' => 'DOWNLOADER',
                'content' => $downloader_name
            ),
              array(
                'name' => 'DOWNLOAD_TYPE',
                'content' => $type    
            )                                     
        );
    
        $merge=array(
            array(
                'rcpt'=> $email_to,
                'vars'=> $vars
            )
        );

        $email = send_mandrill_mail($templatename,$email_to,$full_name,$merge,$email_from,'',$email_bcc);

        if($type == 'PDF')
        {
            // PDF DOWNLOAD
            $finalFilePath = urldecode($this->input->get('filepath')); 
            $finalFilePath = FCPATH.$finalFilePath;
            if (file_exists($finalFilePath)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename=' . basename($finalFilePath));
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($finalFilePath));
                readfile($finalFilePath);
                exit;
            }
        }
        else
        {
            echo "success";
        }
     }

	function download_link_mail()
     {
        $mail_to           = strtoupper($this->input->get('mail_to'));
        $username           = strtoupper($this->input->get('username'));
        $comp           = strtoupper($this->input->get('comp'));
        
        $message = "Hello ".$username.','."\n";
        $message .= $this->input->get('mail_message')."\n";
        $message .= "Regards,"."\n".$username;

        mail($mail_to, 'Youtube link', $message); 

        echo "success";
     }

}

/* End of file investee.php */
/* Location: ./application/controllers/investee.php */
