<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('investor_model');
        $this->load->model('investee_model');
        $this->load->helper('url');
        $this->load->helper('common');
        $this->load->helper('text');

        $this->load->model('banners_model');
        $this->layout['banners'] = $this->banners_model->find_benners();
        $this->layout['unseen_notifications'] = $this->user_model->get_unseen_notification_count($this->session->userdata('user_id'));

        $sectors = $this->user_model->sectors();
        $stages = $this->user_model->startup_stages();
        $this->layout['stages'] = $stages;
        $this->layout['sectors'] = $sectors;

        $this->load->library('pagination');
        $this->load->library('mailchimp_library');
    }

    public function index() {
        $this->load->view('welcome_message');
    }

    public function register_user() {
        $data['role'] = $this->input->get('role');
        $this->layout['content'] = $this->load->view('user/register_user', $data, true);
        $this->load->view('layouts/front', $this->layout);
    }

    public function register() {
        $userId = $this->session->userdata('user_id');
        if ($userId) {
            redirect($this->session->userdata('role') . '/index');
        }
        $this->load->model('banners_model');
        $this->layout['banners'] = $this->banners_model->find_benners();
        $viewData = array();
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('user/register', $bodyData, true);
        $this->load->view('layouts/front', $this->layout);
    }

    function Add_User_Registration_Notification($UserId,$UserName,$socialLogin = ""){

        if ($socialLogin != '') {
            $display_text = $UserName . ' joined EquityCrest via ' . $socialLogin;
        } else {
            $display_text = $UserName . ' joined EquityCrest';    
        }
        
        $link = 'admin/user/' . $UserId;
        $notification = array(
            'sender_id' => $UserId,
            'record_id' => $UserId,
            'display_text' => $display_text,
            'type' => 'new_user',
            'table_name' => 'user_master',
            'link' => $link,
            'date_created' => date('Y-m-d H:i:s')
        );

        $this->user_model->add_admin_notification($notification);
    }

    function Send_User_Welcome_Email($email,$name,$code,$role){
//                 $this->load->library('email');

//                 $this->email->set_mailtype("html");
//                 $this->email->from('info@equitycrest.com', 'Equitycrest');
//                 $this->email->to($email);

//                 $this->email->subject('Welcome to EquityCrest');

//                 $message = '<p class="MsoNormal"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#444444">Dear
// ' . $name . ',&nbsp;<br>
// <br>
// <b>Welcome to Equity Crest</b><br>
// <br>
// We are very happy to have you as part of our community. As this is the first
// time you\'ve signed into Equity Crest with this account, we would need to verify
// that this is really you.&nbsp;<br>
// <br>
// The email address that you gave us during registration is&nbsp;<a href="mailto:' . $email . '" target="_blank">' . $email . '</a>. Just
// click the link below to verify, sign in to Equity Crest and then follow the
// prompts given by the system. By verifying your email address below, you agree
// to abide by our&nbsp;</span><a href="' . base_url() . 'home/terms_of_use" target="_blank"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#0068cf">Terms
// of Use</span></a><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#444444">&nbsp;and&nbsp;</span><a href="' . base_url() . 'home/privacy_policy" target="_blank"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#0068cf">Privacy
// Policy</span></a><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#444444">.&nbsp;<br>
// <br>
// </span><a href="' . base_url() . 'user/activate/' . $code . '" target="_blank"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#0068cf">Verify Now</span></a><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#444444">&nbsp;</span><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"><br>
// <br>
// <span style="color:#444444">If the button above does not work, copy and paste
// the following link into your browser window:&nbsp;</span><span style="color:red"><a href="' . base_url() . 'user/activate/' . $code . '" target="_blank">' . base_url() . 'user/activate/' . $code . '</a><wbr>&nbsp;<br>
// </span><br>
// <span>Once you verify your email address you will get a call from our team to brief you about the Equity Crest and your account.</span><br>
// <span style="color:#444444">Thank you for signing up, we look forward to a long
// term partnership!</span></span><u></u><u></u></p>
// <p class="MsoNormal"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"><br>
// <span style="color:#444444">Happy to have you,&nbsp;</span><br>
// <span style="color:#444444">The Equity Crest Team&nbsp;</span></span><u></u><u></u></p>';
//                 $this->email->message($message);

//                 $this->email->send();






            $ACTIVATION_URL = base_url() . 'user/activate/' . $code. '/'.$role;
            $ACTIVATION_LINK = base_url() . 'user/activate/' . $code. '/'.$role;

            $templatename = 'Signup_Welcome_User';
          //$email_to =   'webdeveloper@tailoredtech.in';
            $email_to   =   $email;
            $full_name = $name;
            $subject    = 'Welcome to Equity Crest';
            $email_from = 'info@equitycrest.com';
            
            $vars=array(
                    array(
                        'name' => 'SUBJECT',
                        'content' => $subject
                    ),
                    array(
                        'name' => 'FIRST_NAME',
                        'content' => $name
                    ),
                     array(
                        'name' => 'USER_EMAIL',
                        'content' => $email
                    ),
                    array(
                        'name' => 'ACTIVATION_URL',
                        'content' => $ACTIVATION_URL
                    ), 
                     array(
                        'name' => 'ACTIVATION_LINK',
                        'content' => $ACTIVATION_LINK
                    )                    
            );
    
            $merge=array(
                    array(
                        'rcpt'=> $email_to,
                        'vars'=> $vars
                        )
            );
            
        $email = send_mandrill_mail($templatename,$email_to,$full_name,$merge,$email_from);  

    }


    function Send_EQ_REG_EMAIL($name,$role,$company_name,$cmp_email,$mob_num,$linkedin_url, $data=array(), $deck_link='')
    {

       //$email_to = 'info@dezigngenie.com'; //$email_address is the actual value;
            //$email_to =   'webdeveloper@tailoredtech.in';
            // $email_to   = 'rahul.singhal@equitycrest.com';
if(BYPASS_EQ_MAIL)
            {
                $email_to   = 'sagar.c@tailoredtech.in';
            }
            else
            $email_to   = 'rohit.krishna@equitycrest.com,amit.wadhwa@equitycrest.com';
            // $email_to   = 'rohini.b@tailoredtech.in';
            $cc = EQ_EMAIL;
            $full_name = 'EQ Team';
            $email_from = 'info@equitycrest.com';
           
           
           if($role == 'investor'){
            
            $templatename = 'Signup_ToTeam_Investor_Registration';


            $subject    = 'Investor Registeration';
            if(BYPASS_EQ_MAIL)
            {
               $subject    = 'Investor Registeration-(EQUITY MAIL)';
            }
            $vars=array(
                    array(
                        'name' => 'SUBJECT',
                        'content' => $subject
                    ),
                    array(
                        'name' => 'FIRST_NAME',
                        'content' => $full_name
                    ),
                     array(
                        'name' => 'NAME',
                        'content' => $name
                    ),
                     array(
                        'name' => 'USER_EMAIL',
                        'content' => $email_to
                    ),
                     array(
                        'name' => 'ROLE',
                        'content' => $role
                    ),
                     array(
                        'name' => 'COMPANYNAME',
                        'content' => $company_name
                    ),
                     array(
                        'name' => 'EMAIL',
                        'content' => $cmp_email
                    ),
                      array(
                        'name' => 'MOBNUM',
                        'content' => $mob_num
                    ),
                      array(
                        'name' => 'LINKEDINURL',
                        'content' => $linkedin_url
                    ),
                              
            );
        }

       else if($role == 'investee')
       {      

            $query = $this->db->get_where('user_master' , array('email' => $cmp_email));
            if($query->num_rows() == 1)
            {
                $row = $query->row_array();
                $entrepreneur = (int)$row['pop_up_registration'];
            }

            $templatename = 'Signup_ToTeam_Investor_Reg';

            if(BYPASS_EQ_MAIL)
            {
                $templatename = 'Signup_ToTeam_Investor_Reg_localhost';
            }

            $subject    = 'Investor Registeration';

            
            $stage_val = array('','Ideation','Proof of Concept','Beta Launched','Early Revenue','Steady Revenue');
            $vars=array(
                    array(
                        'name' => 'SUBJECT',
                        'content' => $subject
                    ),
                    array(
                        'name' => 'FIRST_NAME',
                        'content' => $full_name
                    ),
                    array(
                        'name' => 'USER_EMAIL',
                        'content' => $email_to
                    ),
                     array(
                        'name' => 'ROLE',
                        'content' => $role
                    ),
                    array(
                        'name' => 'FOUNDER',
                        'content' => $data['name']
                    ),
                    array(
                        'name' => 'MOBNUM',
                        'content' => $data['mobileno']
                    ),
                    array(
                        'name' => 'FOUNDER_EMAIL',
                        'content' => $data['email']
                    ),
                    array(
                        'name' => 'LINKEDIN_URL',
                        'content' => urlencode($data['linkedin_url'])
                    ),
                    array(
                        'name' => 'COMPANYNAME',
                        'content' => $data['company_name']
                    ),
                    array(
                        'name' => 'WEBSITE_LINK',
                        'content' => $data['company_url']
                    ),
                    array(
                        'name' => 'APP_LINK',
                        'content' => $data['app_link']
                    ),
                    array(
                        'name' => 'FOUNDING_YEAR',
                        'content' => $data['year']
                    ),
                    array(
                        'name' => 'PROBLEM_SOLVED',
                        'content' => $data['business_problem_being_solved']
                    ),
                    array(
                        'name' => 'PRODUCT_SERVICE',
                        'content' => $data['products_services_business']
                    ),
                    array(
                        'name' => 'UNIQUE_SELLING',
                        'content' => $data['how_different']
                    ),
                    array(
                        'name' => 'COMPETITION',
                        'content' => $data['competition']
                    ),
                    array(
                        'name' => 'TARGET_AUDIENCE',
                        'content' => $data['market_size_commentary']
                    ),
                    array(
                        'name' => 'STAGE',
                        'content' => $stage_val[$data['stage']]
                    ),
                    array(
                        'name' => 'INVESTMENT_REQUIRED',
                        'content' => $data['investment_required']
                    ),
                    array(
                        'name' => 'TRACTION',
                        'content' => $data['customer_traction']
                    ),
                    array(
                        'name' => 'HOW_DID_YOU_HEAR',
                        'content' => $data['hear_about_us']
                    ),
                    array(
                        'name' => 'HEARD_SOURCE',
                        'content' => $data['others']
                    ),
                    array(
                        'name' => 'FOUNDER_LOCATION',
                        'content' => $data['city']
                    ),
                    array(
                        'name' => 'DECK_LINK',
                        'content' => urlencode($deck_link)
                    ),
                    array(
                        'name' => 'ENTREPRENEUR',
                        'content' => isset($entrepreneur)?$entrepreneur:"",
                    ),
            );
       }

        else if($role == 'channel_partner')
       {
            $templatename = 'Signup_ToTeam_ChannelPartner';
            $subject    = 'Channel Partner Registeration';
            if(BYPASS_EQ_MAIL)
            {
               $subject    = 'Channel Partner Registeration-(EQUITY MAIL)';
            }
            $vars=array(
                    array(
                        'name' => 'SUBJECT',
                        'content' => $subject
                    ),
                    array(
                        'name' => 'FIRST_NAME',
                        'content' => $full_name
                    ),
                     array(
                        'name' => 'USER_EMAIL',
                        'content' => $email_to
                    ),
                     array(
                        'name' => 'ROLE',
                        'content' => $role
                    ),
                     array(
                        'name' => 'COMPANYNAME',
                        'content' => $company_name
                    ),
                     array(
                        'name' => 'EMAIL',
                        'content' => $cmp_email
                    ),
                      array(
                        'name' => 'MOBNUM',
                        'content' => $mob_num
                    ),              
            );
       }
    
    
            $merge=array(
                    array(
                        'rcpt'=> $email_to,
                        'vars'=> $vars
                        )
            );
            
            $email = send_mandrill_mail($templatename,$email_to,$full_name,$merge,$email_from,$cc);
    }

    public function register_process() {
    //die(print_r($this->input->post()));
        // print_r($_FILES['investor_deck']); exit; 
        $returnData = array();
        if ($this->input->post('name')) 
        {
            $code = rand_string(30);
            $role = $this->input->post('role');
            $email = $this->input->post('email');
            $name = $this->input->post('name');
			
			if ($role == '') 
            {
                /*$this->session->set_flashdata('error-msg', 'No role selected.');
                redirect('user/register');*/
                $returnData['success'] = FALSE;
                $returnData['error-msg']='No role selected.';
			}
            elseif ($this->user_model->find_by_email($email)) 
            {
               /* $this->session->set_flashdata('error-msg', 'This email Id ' . $email . ' is already registered.');
                redirect('user/register');*/
                $returnData['success'] = FALSE;
                $returnData['error-msg']='This email Id ' . $email . ' is already registered.';
            }
            else
            {
                $pass = md5(trim($this->input->post('user_password')));
                $insertData = array(
                    'name' => $name,
                    'mob_no' => $this->input->post('mobileno'),
                    'city' => $this->input->post('city') != '' ? $this->input->post('city') : '',
                    'email' => $email,
                    'password' => $pass,
                    'role' => $role,
                    'code' => $code,
                    'created_date' => date('Y-m-d H:i:s'),
                    'pop_up_registration' => (null != ($this->input->post('entrepreneur')))?$this->input->post('entrepreneur'):0,
                );
                if ($this->input->post('investor_type')) 
                {
                    $insertData['investor_type'] = $this->input->post('investor_type');
                }
                if ($this->input->post('linkedin_url') && $role != 'investee') {
                    $insertData['linkedin_url'] = $this->input->post('linkedin_url');
                }
                if ($this->input->post('company_name')) {
                    $insertData['company_name'] = $this->input->post('company_name');
                    $displayName = $this->input->post('company_name');
                    $slug_name = $this->check_duplicate(trim($this->input->post('company_name')));
                } 
                else
                {
                    $displayName = $name;
                    $slug_name = $this->check_duplicate(trim($name));
                }

                $insertData['slug_name'] = $slug_name;

                $userId = $this->user_model->add_user($insertData);
                $this->Add_User_Registration_Notification($userId,$displayName,"");

                $returnData['userId'] = $userId;
                if ($userId) 
                {
                    $returnData['success'] = TRUE;
                    if ($role == 'investee') {
                        $this->user_model->add_privacy($userId);
                    } 

                if($this->session->userdata('signup_email') == 'on'){
                       
                    $this->Send_User_Welcome_Email($email,$displayName,$code,$role);

                    $deck_link = '';
                    if(isset($_FILES['investor_deck']) && $_FILES['investor_deck']['name'] != '')
                    {
                        $deck_link = base_url().'/uploads/users/' . $userId . '/deck_files/'.$_FILES['investor_deck']['name'];
                    }

                   if($role != 'media' && $role != 'investee') 
                        $this->Send_EQ_REG_EMAIL($name,$role,$this->input->post('company_name'),$email,$this->input->post('mobileno'),$this->input->post('linkedin_url'));
                    if($role == 'investee')
                        $this->Send_EQ_REG_EMAIL($name,$role,$this->input->post('company_name'),$email,$this->input->post('mobileno'),$this->input->post('linkedin_url'), $this->input->post(), $deck_link);
                }
               
                if($role == 'investee')
                {
                    if($_FILES['investor_deck']['name'] != '')
                    {
                        $path = "./uploads/users/".$userId;
                        if (!is_dir($path)) {
                            mkdir($path);
			    mkdir($path.'/deck_files');
                        }
                        $config['upload_path'] = $path.'/deck_files';
                        $config['allowed_types'] = 'pdf|ppt|pptx';
                        $config['max_size'] = '1024000';

                        $this->load->library('upload', $config);
                        $file = 'investor_deck';
                        if (!($_FILES['investor_deck']['size'] == 0)) {
                            if (!$this->upload->do_upload($file)) {
                                // die(print_r($this->upload->display_errors()));
                            } else {
                                $upload_data = $this->upload->data();
                                $investee_data['investor_deck'] = $upload_data['file_name'];
                            }
                        }
                    }

                    $investee_data['user_id'] = $userId;
                    $investee_data['company_url'] = $this->input->post('company_url');
                    $investee_data['year'] = $this->input->post('year');
                    $investee_data['app_link'] = $this->input->post('app_link');
                    // $investee_data['location'] = $this->input->post('location');
                    $investee_data['business_problem_solved'] = $this->input->post('business_problem_being_solved');
                    $investee_data['products_services'] = $this->input->post('products_services_business');
                    $investee_data['how_different'] = $this->input->post('how_different');
                    $investee_data['competition'] = $this->input->post('competition');
                    $investee_data['market_size_commentary'] = $this->input->post('market_size_commentary');
                    $investee_data['stage'] = $this->input->post('stage');
                    $investee_data['investment_required'] = $this->input->post('investment_required');
                    $investee_data['customer_traction'] = $this->input->post('customer_traction');
                    // $investee_data['investor_deck'] = $this->input->post('');
                    $investee_data['how_you_heard_about_us'] = $this->input->post('hear_about_us');
                    if($this->input->post('hear_about_us') != '')
                        $investee_data['heard_source'] = $this->input->post('others');

                    $investee_id = $this->user_model->add_investee($investee_data);
                    // print_r($investee_data); exit;


                    $investee_team_data['investee_id'] = $investee_id;
                    $investee_team_data['name'] = $name;
                    $investee_team_data['linkedin'] = $this->input->post('linkedin_url');;
                    $team_id = $this->user_model->add_investee_team($investee_team_data);

                    if($investee_id != '')
                    {
                        if($this->input->post('team_name') != '')
                        {
                            $team_name = $this->input->post('team_name');
                            $team_linkedin_url = $this->input->post('team_linked_in');
                            foreach($team_name as $team)
                            {   
                                $i = 0;
                                $investee_team_data['investee_id'] = $investee_id;
                                $investee_team_data['name'] = $team;
                                $investee_team_data['linkedin'] = $team_linkedin_url[$i] != ''?$team_linkedin_url[$i]:'';

                                $team_id = $this->user_model->add_investee_team($investee_team_data);

                                $i++;
                            }
                        }
                    }
                }

                    switch ($role) 
                    {
                        case 'investor':
                            $role = "Investor";
                            break;
                        case 'investee':
                            $role = "Start-Up";
                           
                            if(null != $this->input->post('entrepreneur') && $this->input->post('entrepreneur') =='1' )
                            {
                                 $returnData['redirect']=site_url().'user/welcome_special_investee';
                                    $returnData['success-msg']= "";
                                     echo json_encode($returnData);
                                     exit();
                            }

                            break;
                        case 'channel_partner':
                            $role = "Channel partner";
                            break;
                        case 'media':
                            $role = "Media";
                            break;
                    }
                     $this->session->set_flashdata('success-msg', 'Thank you for registering on Equity Crest as an ' . $role . '. An activation e-mail has been sent to you on your e-mail ID ' . $email . '. Please do check your spam folder as well for the email. Request you to please activate your Equity Crest account by clicking on the link sent across.');
                     $returnData['redirect']=site_url().'user/login';
                     $returnData['success-msg']= 'Thank you for registering on Equity Crest as an ' . $role . '. An activation e-mail has been sent to you on your e-mail ID ' . $email . '. Please do check your spam folder as well for the email. Request you to please activate your Equity Crest account by clicking on the link sent across.'; 
              } 
                else
                {
                    $returnData['success'] = FALSE;
                }
            }
        } 
        else 
        {
            $returnData['success'] = FALSE;
        }

        echo json_encode($returnData);
    }


    function welcome_special_investee()
    {
         $this->layout['content'] = $this->load->view('user/welcome_special_investee');
        $this->load->view('layouts/front', $this->layout);
    }
    function check_duplicate($chck_name)
    {

        $name = str_replace(' ', '.', $chck_name); 
        $slug_names = $this->user_model->get_slug_names($name); 
      
        if(!empty($slug_names))
        {

           $sname = str_replace(' ', '_', $chck_name); 
        }
        else
        {
            $sname = $name;
        }

        return $sname; 
    }

    public function activate($code,$role=NULL) {
       
       $user = $this->user_model->find_by_code($code); 
        if ($user) {
            $data = array('active' => 1);
            $this->user_model->update_user($data, $user['id']);

            if($role == 'investee'){

            $templatename = 'User_Verify';
            $email_to   =   $user['email']; 
            $cc = EQ_EMAIL; 
            $email_from = 'info@equitycrest.com';
            
           $vars=array(
                    array(
                        'name' => 'FIRST_NAME',
                        'content' => $user['name']
                     )
                    );
    
           $merge=array(
                    array(
                        'rcpt'=> $email_to,
                        'vars'=> $vars
                        )
            ); 
            
             $email = send_mandrill_mail($templatename,$email_to,$user['name'],$merge,$email_from,$cc); 
            }
            $this->session->set_flashdata('success-msg', 'Your account has been verified.');
            redirect('user/login');
        } else {
            $this->session->set_flashdata('error-msg', 'Link is expired.');
            redirect('user/login');
        }
    }
    
    public function verify_again(){
            $id=  $this->input->post('id');        
            $user=$this->user_model->find_by_id($id);
            $email =$user['email'];
            $name=$user['name'];
            $code  =$user['code'];
            //email code
                $this->load->library('email');

                $this->email->set_mailtype("html");
                $this->email->from('info@equitycrest.com', 'Equitycrest');
                $this->email->to($email);
                
                $this->email->subject('Welcome to EquityCrest');
            
            //email message    
            $message = '<p class="MsoNormal"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#444444">Dear
' . $name . ',&nbsp;<br>
<br>
<b>Welcome to Equity Crest   from verify_again</b><br>
<br>
We are very happy to have you as part of our community. As this is the first
time you\'ve signed into Equity Crest with this account, we would need to verify
that this is really you.&nbsp;<br>
<br>
The email address that you gave us during registration is&nbsp;<a href="mailto:' . $email . '" target="_blank">' . $email . '</a>. Just
click the link below to verify, sign in to Equity Crest and then follow the
prompts given by the system. By verifying your email address below, you agree
to abide by our&nbsp;</span><a href="' . base_url() . 'home/terms_of_use" target="_blank"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#0068cf">Terms
of Use</span></a><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#444444">&nbsp;and&nbsp;</span><a href="' . base_url() . 'home/privacy_policy" target="_blank"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#0068cf">Privacy
Policy</span></a><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#444444">.&nbsp;<br>
<br>
</span><a href="' . base_url() . 'user/activate/' . $code . '" target="_blank"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#0068cf">Verify Now</span></a><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#444444">&nbsp;</span><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"><br>
<br>
<span style="color:#444444">If the button above does not work, copy and paste
the following link into your browser window:&nbsp;</span><span style="color:red"><a href="' . base_url() . 'user/activate/' . $code . '" target="_blank">' . base_url() . 'user/activate/' . $code . '</a><wbr>&nbsp;<br>
</span><br>
<span>Once you verify your email address you will get a call from our team to brief you about the Equity Crest and your account.</span><br>
<span style="color:#444444">Thank you for signing up, we look forward to a long
term partnership!</span></span><u></u><u></u></p>
<p class="MsoNormal"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"><br>
<span style="color:#444444">Happy to have you,&nbsp;</span><br>
<span style="color:#444444">The Equity Crest Team&nbsp;</span></span><u></u><u></u></p>';
                $this->email->message($message);

                $this->email->send();
    }

    public function login() {


        if($this->input->cookie('remember_me_token')){
            $cookie=$this->input->cookie('remember_me_token');
            //print_r($cookie);
            $user = $this->user_model->remember($cookie);
            
             if ($user) {

                // echo "hijhi"; die;
                 $this->session->set_userdata('user_id', $user['id']);
            $this->session->set_userdata('email', $user['email']);
            $this->session->set_userdata('name', $user['name']);
            $this->session->set_userdata('company_name', $user['company_name']);
            $this->session->set_userdata('image', $user['image']);
            $this->session->set_userdata('role', $user['role']);

            if ($user['role'] == 'investee') {
               // redirect('investee/index');
            } else if ($user['role'] == 'investor') {
                $investee_ids = $this->investor_model->get_pledged_investee_ids($user['id']);
                $this->session->set_userdata('pledged', $investee_ids);
                $investee_ids = $this->investor_model->get_followed_investee_ids($user['id']);
                $this->session->set_userdata('followed', $investee_ids);
               // redirect('investor/index');
            } else if ($user['role'] == 'channel_partner') {
               // redirect('channel_partner/index');
            }
                redirect('home/index');
             }
             else{
                 $this->session->set_flashdata('error-msg', ' Your session has expired. Please login again');
                    redirect('user/login');
             }
            
        }else{
        $this->load->model('banners_model');
        $this->layout['banners'] = $this->banners_model->find_benners();
        $viewData = array();
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('user/login', $bodyData, true);
        $this->load->view('layouts/front', $this->layout);
        }
    }

    public function login_process() {

        $email = $this->input->post('email');
        //$password = $this->input->post('password');
        $password = md5(trim($this->input->post('password')));

        $json=array();
        $remember = $this->input->post('remember');
        // die(print_r($this->input->post()));
        $user = $this->user_model->login($email, $password);

     


        if ($user) {
            if($user['active']==0)
            {
                $id=$user['id'];
                $json['error']['error-msg']='Please verify your account using verification mail sent earlier. <a id="verify" data-id="'.$id.'" href="javascript:void(0)">Send Again</a>';
                
            }
            elseif($user['role']=='investor' && $user['status']=='inactive')
            {
                $id=$user['id'];
                $json['error']['error-msg']='We thank you for joining Equity Crest. As Equity Crest is not a platform that is open to all, we request you to please bear with us for some time. Someone from our team will call you very shortly to activate your account';
            }
            
            elseif($user['role']=='media')
            {
                $id=$user['id'];
                $json['error']['error-msg']='Thank you for registering on Equity Crest. The media section is currently under construction. We will reach out to you shortly. For anything urgent please do reach out to: Preethi Chamikutty - +91 98190 74797 Or Amit Wadhwa - +91 98922 11438';
            }
            
            else
            {
                if ($remember) 
                {
                    $rememberHash = md5(trim($user['email'] + $user['name']));
                    //die($rememberHash);
                    $cookie = array(
                        'name' => 'remember_me_token',
                        'value' => $rememberHash,
                        'expire' => '1209600', // Two weeks
                       
                        'path' => '/'
                    );
                    $this->input->set_cookie($cookie);
                    $data=array('remember'=>$rememberHash);
                    $this->user_model->update_remember($user['id'],$data);
                }

                 

                $this->session->set_userdata('user_id', $user['id']);
                $this->session->set_userdata('email', $user['email']);
                $this->session->set_userdata('name', $user['name']);
                $this->session->set_userdata('company_name', $user['company_name']);
                $this->session->set_userdata('image', $user['image']);
                $this->session->set_userdata('role', $user['role']);
                $this->session->unset_userdata('sticky_pop_up');
                 $this->session->set_userdata('sticky_pop_up', '0');
                if ($user['role'] == 'investee') {
                  // redirect('investee/index');
                   $json['redirect'] =site_url().'investee/index';
                } else
                 {
                  if($this->session->userdata('redirect') == '')
                    {
                        
                    if ($user['role'] == 'investor') {
                    $investee_ids = $this->investor_model->get_pledged_investee_ids($user['id']);
                    $this->session->set_userdata('pledged', $investee_ids);
                    $investee_ids = $this->investor_model->get_followed_investee_ids($user['id']);
                    $this->session->set_userdata('followed', $investee_ids);
                   // redirect('investor/index');
                     $json['redirect'] =site_url().'investor/index';
                } else if ($user['role'] == 'channel_partner') {
                   // redirect('channel_partner/index');
                     $json['redirect'] =site_url().'channel_partner/index';
                } else if ($user['role'] == 'media') {
                 //  redirect('media/index');
                    $json['redirect'] =site_url().'media/index';
                }
             }
             else
             {
                 $json['redirect'] = site_url().$this->session->userdata('redirect');
               
             }
             }
            }
        } else {
            $json['error']['error-msg']='Please check your Email ID and Password';
        }
        echo json_encode($json);
    }

    public function inquiry() {
        if ($this->input->post()) {
            $email = $this->input->post('email');
            if ($email) {
                $code = rand_string(30);
                $this->load->library('email');

                $this->email->set_mailtype("html");
                $this->email->from($email);
                $this->email->to('admin@equitycrest.com', 'Equitycrest');

                $this->email->subject($this->input->post('subject') . ' - ' . $this->input->post('mobileno'));
                $message = $this->input->post('message');

                $this->email->message($message);

                $this->email->send();
                
				echo 'success';
            } else {
                echo 'Email could not be sent please try again later';
            }
        }
    }
	
    public function send_pass_reset_link() {

        if ($this->input->post()) {
            $email = $this->input->post('email');
            $user = $this->user_model->find_by_email($email);
            if ($user) {
                $code = rand_string(30);
                $this->load->library('email');

                $this->email->set_mailtype("html");
                $this->email->from('info@equitycerst.com', 'Equitycrest');//admin@equitycrest.com
                $this->email->to($email);
				$this->email->bcc('tech@codeginger.com');

                $this->email->subject('Equitycrest Reset Password');
                $message = 'Please click on below link to reset password.' . "\r\n" . '<br><br>
                            <a href="' . base_url() . 'user/reset_password/' . $code . '">Reset Password</a><br>';

                $this->email->message($message);

                $this->email->send();
                $this->user_model->update_user(array('reset_code' => $code), $user['id']);
                
				//echo $this->email->print_debugger();
				
                echo 'success';
            } else {
                echo 'Email could not be sent please try again later';
            }
        }
    }

    public function reset_password($code) {
        $user = $this->user_model->find_by_reset_code($code);
        if ($user) {
            $bodyData['email'] = $user['email'];
            $bodyData['id'] = $user['id'];
            $this->layout['content'] = $this->load->view('user/reset_password', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        } else {
            echo 'link is expired';
        }
    }

    public function reset_password_process() {
        if ($this->input->post()) {
            $id = $this->input->post('id');
            $pass = md5(trim($this->input->post('userpassword')));
            $data = array('password' => $pass, 'remember' => '');
            $this->user_model->update_user($data, $id);

            $this->session->set_flashdata('success-msg', 'Your password has been successfully changed.');
            redirect('user/login');
        }
    }

    public function fb_login() {
        parse_str($_SERVER['QUERY_STRING'], $_REQUEST);

        $this->load->library('Facebook', array("appId" => "693713417337707", "secret" => "24937704e490cea8d384a02fabee1945"));

        $this->user = $this->facebook->getUser();

        if ($this->user) {

            try {
                $userProfile = $this->facebook->api('/me');
                $localUser = $this->user_model->get_fbuser($userProfile['id']);

                if ($localUser) {
                    $this->session->set_userdata('user_id', $localUser['id']);
                    $this->session->set_userdata('facebook_id', $localUser['facebook_id']);
                    $this->session->set_userdata('google_picture', $localUser['google_picture']);
                    $this->session->set_userdata('linkedin_picture', $localUser['linkedin_picture']);
                    $this->session->set_userdata('image', $localUser['image']);
                    $this->session->set_userdata('email', $localUser['email']);
                    $this->session->set_userdata('company_name', $localUser['company_name']);
                    $this->session->set_userdata('email', $localUser['email']);
                    $this->session->set_userdata('role', $localUser['role']);

                    if ($this->session->userdata['role'] == 'investor') {
                        redirect('investor/index');
                    } elseif ($this->session->userdata['role'] == 'investee') {
                        redirect('investee/index');
                    }
                } else {

                    $userData = $this->user_model->find_by_email($userProfile['email']);

                    if ($userData) {

                        $this->user_model->update_facebookid($userData['id'], $userProfile['id']);
                        //linked in user auto-login

                        $localUser = $this->user_model->get_fbuser($userProfile['id']);

                        $this->session->set_userdata('user_id', $localUser['id']);
                        $this->session->set_userdata('facebook_id', $localUser['facebook_id']);
                        $this->session->set_userdata('google_picture', $localUser['google_picture']);
                        $this->session->set_userdata('linkedin_picture', $localUser['linkedin_picture']);
                        $this->session->set_userdata('image', $localUser['image']);
                        $this->session->set_userdata('email', $localUser['email']);
                        $this->session->set_userdata('company_name', $localUser['company_name']);
                        $this->session->set_userdata('email', $localUser['email']);
                        $this->session->set_userdata('role', $localUser['role']);

                        if ($this->session->userdata['role'] == 'investor') {
                            redirect('investor/index');
                        } elseif ($this->session->userdata['role'] == 'investee') {
                            redirect('investee/index');
                        }
                    } else {

                        $bodyData['facebook_id'] = $userProfile['id'];
                        $bodyData['fname'] = $userProfile['first_name'];
                        $bodyData['lname'] = $userProfile['last_name'];
                        $bodyData['email'] = $userProfile['email'];

                        //  die(print_r($profileData));
                        $this->layout['content'] = $this->load->view('user/facebook_register', $bodyData, true);
                        $this->load->view('layouts/front', $this->layout);
                    }
                }
            } catch (FacebookApiException $e) {

                print_r($e);
            }
        } else {

            $login = $this->facebook->getLoginUrl(array("scope" => "email"));
            redirect($login);
        }
    }

    function fb_process() {

        $facebookId = $this->input->post('facebook_id');
        $role = $this->input->post('role');
        $email = $this->input->post('email');
        $name = $this->input->post('name');
        $random = mt_rand();
        $pass = md5($random);
        $code = rand_string(30);

        if ($this->user_model->find_by_email($email)) {
            $this->session->set_flashdata('error-msg', 'This email Id ' . $email . ' is already registered.');
            redirect('user/linkedin_register');            
        }


        $insertData = array(
            'name' => $this->input->post('name'),
            'mob_no' => $this->input->post('mobileno'),
            'email' => $email,
            'role' => $role,
            'code' => $code,
            'password' => $pass,
            'facebook_id' => $facebookId
        );

        if ($this->input->post('company_name')) {
            $insertData['company_name'] = $this->input->post('company_name');
            $displayName = $this->input->post('company_name');
        } else {
            $displayName = $name;
        }

        $userId = $this->user_model->add_user($insertData);
        $userData = $this->user_model->get_fbuser($facebookId);

        $this->session->set_userdata('user_id', $userData['id']);
        $this->session->set_userdata('facebook_id', $userData['facebook_id']);
        $this->session->set_userdata('google_picture', $userData['google_picture']);
        $this->session->set_userdata('linkedin_picture', $userData['linkedin_picture']);
        $this->session->set_userdata('image', $userData['image']);
        $this->session->set_userdata('email', $userData['email']);
        $this->session->set_userdata('company_name', $userData['company_name']);
        $this->session->set_userdata('email', $userData['email']);
        $this->session->set_userdata('role', $userData['role']);

        if ($role == 'investee') {
            $this->user_model->add_privacy($userId);
        }        

        $this->Add_User_Registration_Notification($userId,$displayName,"Facebook");
        $this->Send_User_Welcome_Email($userData['email'],$displayName,$code);

        if ($this->session->userdata['role'] == 'investor') {
            redirect('investor/index');
        } elseif ($this->session->userdata['role'] == 'investee') {
            redirect('investee/index');
        }
    }

    function linkedin_login() {
        session_start();
        $this->load->library('linkedin');
        // OAuth 2 Control Flow
        if (isset($_GET['error'])) {
            // LinkedIn returned an error
            // load any error view here
            exit;
        } elseif (isset($_GET['code'])) {
            // User authorized your application
            if (isset($_SESSION['state'])) {
                if ($_SESSION['state'] == $_GET['state']) {
                    // Get token so you can make API calls
                    $this->linkedin->getAccessToken();
                } else {

                    // CSRF attack? Or did you mix up your states?
                    exit;
                }
            }
        } else {
            if ((empty($_SESSION['expires_at'])) || (time() > $_SESSION['expires_at'])) {
                // Token has expired, clear the state
                $_SESSION = array();
            }
            if (empty($_SESSION['access_token'])) {
                // Start authorization process
                $this->linkedin->getAuthorizationCode();
            }
        }
        // define the array of profile fields
        $profile_fileds = array(
            'id',
            'firstName',
            'maiden-name',
            'lastName',
            'picture-url',
            'email-address',
            'location:(country:(code))',
            'industry',
            'summary',
            'specialties',
            'interests',
            'public-profile-url',
            'last-modified-timestamp',
            'num-recommenders',
            'date-of-birth',
            'picture-urls::(original)'
        );
        $profileData = $this->linkedin->fetch('GET', '/v1/people/~:(' . implode(',', $profile_fileds) . ')');

//        http://api.linkedin.com/v1/people/~/picture-urls::(original)

        if ($profileData) {
            $profileData = get_object_vars($profileData);

            $localUser = $this->user_model->get_linkedinuser($profileData['id']);

            if ($localUser) {
                $this->session->set_userdata('user_id', $localUser['id']);
                $this->session->set_userdata('facebook_id', $localUser['facebook_id']);
                $this->session->set_userdata('google_picture', $localUser['google_picture']);
                $this->session->set_userdata('linkedin_picture', $localUser['linkedin_picture']);
                $this->session->set_userdata('image', $localUser['image']);
                $this->session->set_userdata('email', $localUser['email']);
                $this->session->set_userdata('company_name', $localUser['company_name']);
                $this->session->set_userdata('email', $localUser['email']);
                $this->session->set_userdata('role', $localUser['role']);

                if ($this->session->userdata['role'] == 'investor') {
                    redirect('investor/index');
                } elseif ($this->session->userdata['role'] == 'investee') {
                    redirect('investee/index');
                }
            } else {

                $userData = $this->user_model->find_by_email($profileData['emailAddress']);

                if ($userData) {

                    $this->user_model->update_linkedinid($userData['id'], $profileData['id'], $profileData['pictureUrl']);
                    //linked in user auto-login

                    $localUser = $this->user_model->get_linkedinuser($profileData['id']);

                    $this->session->set_userdata('user_id', $localUser['id']);
                    $this->session->set_userdata('facebook_id', $localUser['facebook_id']);
                    $this->session->set_userdata('google_picture', $localUser['google_picture']);
                    $this->session->set_userdata('linkedin_picture', $localUser['linkedin_picture']);
                    $this->session->set_userdata('image', $localUser['image']);
                    $this->session->set_userdata('email', $localUser['email']);
                    $this->session->set_userdata('company_name', $localUser['company_name']);
                    $this->session->set_userdata('email', $localUser['email']);
                    $this->session->set_userdata('role', $localUser['role']);

                    if ($this->session->userdata['role'] == 'investor') {
                        redirect('investor/index');
                    } elseif ($this->session->userdata['role'] == 'investee') {
                        redirect('investee/index');
                    }
                } else {

                    $bodyData['linkedin_id'] = $profileData['id'];
                    $bodyData['fname'] = $profileData['firstName'];
                    $bodyData['lname'] = $profileData['lastName'];
                    $bodyData['email'] = $profileData['emailAddress'];
                    $pictures = get_object_vars($profileData['pictureUrls']);
                    $bodyData['linkedin_picture'] = $pictures['values'][0];

                    //  die(print_r($profileData));
                    $this->layout['content'] = $this->load->view('user/linkedin_register', $bodyData, true);
                    $this->load->view('layouts/front', $this->layout);
                }
            }
        } else {
            // linked return an empty array of profile data
        }
    }


    function linkedin_process() {

        $linkedinId = $this->input->post('linkedin_id');
        $role = $this->input->post('role');
        $email = $this->input->post('email');
        $name = $this->input->post('name');
        $linkedin_picture = $this->input->post('linkedin_picture');
        $random = mt_rand();
        $pass = md5($random);
        $code = rand_string(30);

        if ($this->user_model->find_by_email($email)) {
            $this->session->set_flashdata('error-msg', 'This email Id ' . $email . ' is already registered.');
            redirect('user/linkedin_register');            
        }

        
        $insertData = array(
            'name' => $this->input->post('name'),
            'mob_no' => $this->input->post('mobileno'),
            'email' => $email,
            'role' => $role,
            'password' => $pass,
            'code' => $code,
            'linkedin_id' => $linkedinId,
            'linkedin_picture' => $linkedin_picture
        );

        if ($this->input->post('company_name')) {
            $insertData['company_name'] = $this->input->post('company_name');
            $displayName = $this->input->post('company_name');
        } else {
            $displayName = $name;
        }

        $userId = $this->user_model->add_user($insertData);
        $userData = $this->user_model->get_linkedinuser($linkedinId);

        $this->session->set_userdata('user_id', $userData['id']);
        $this->session->set_userdata('facebook_id', $userData['facebook_id']);
        $this->session->set_userdata('google_picture', $userData['google_picture']);
        $this->session->set_userdata('linkedin_picture', $userData['linkedin_picture']);
        $this->session->set_userdata('image', $userData['image']);
        $this->session->set_userdata('email', $userData['email']);
        $this->session->set_userdata('company_name', $userData['company_name']);
        $this->session->set_userdata('email', $userData['email']);
        $this->session->set_userdata('role', $userData['role']);

        if ($role == 'investee') {
            $this->user_model->add_privacy($userId);
        }        

        $this->Add_User_Registration_Notification($userId,$displayName,"LinkedIn");
        $this->Send_User_Welcome_Email($userData['email'],$displayName,$code);

        if ($this->session->userdata['role'] == 'investor') {
            redirect('investor/index');
        } elseif ($this->session->userdata['role'] == 'investee') {
            redirect('investee/index');
        }
    }

    public function googleplus_login() {
        session_start();
        $this->load->library('googleplus');
        if (isset($_GET['code'])) {
            $this->googleplus->client->authenticate();
            $_SESSION['token'] = $this->googleplus->client->getAccessToken();
            $redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
            header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
        }
        if (isset($_SESSION['token'])) {
            $this->googleplus->client->setAccessToken($_SESSION['token']);
        }
        if ($this->googleplus->client->getAccessToken()) {
            $userProfile = $this->googleplus->people->get('me');
            $_SESSION['token'] = $this->googleplus->client->getAccessToken();

            $localUser = $this->user_model->get_googleuser($userProfile['id']);

            if ($localUser) {
                $this->session->set_userdata('user_id', $localUser['id']);
                $this->session->set_userdata('facebook_id', $localUser['facebook_id']);
                $this->session->set_userdata('google_picture', $localUser['google_picture']);
                $this->session->set_userdata('linkedin_picture', $localUser['linkedin_picture']);
                $this->session->set_userdata('image', $localUser['image']);
                $this->session->set_userdata('email', $localUser['email']);
                $this->session->set_userdata('company_name', $localUser['company_name']);
                $this->session->set_userdata('email', $localUser['email']);
                $this->session->set_userdata('role', $localUser['role']);

                if ($this->session->userdata['role'] == 'investor') {
                    redirect('investor/index');
                } elseif ($this->session->userdata['role'] == 'investee') {
                    redirect('investee/index');
                }
            } else {

                $userData = $this->user_model->find_by_email($userProfile['emails'][0]['value']);

                if ($userData) {

                    $this->user_model->update_googleid($userData['id'], $userProfile['id'], $userProfile['image']['url']);
                    //linked in user auto-login

                    $localUser = $this->user_model->get_googleuser($userProfile['id']);

                    $this->session->set_userdata('user_id', $localUser['id']);
                    $this->session->set_userdata('facebook_id', $localUser['facebook_id']);
                    $this->session->set_userdata('google_picture', $localUser['google_picture']);
                    $this->session->set_userdata('linkedin_picture', $localUser['linkedin_picture']);
                    $this->session->set_userdata('image', $localUser['image']);
                    $this->session->set_userdata('email', $localUser['email']);
                    $this->session->set_userdata('company_name', $localUser['company_name']);
                    $this->session->set_userdata('email', $localUser['email']);
                    $this->session->set_userdata('role', $localUser['role']);

                    if ($this->session->userdata['role'] == 'investor') {
                        redirect('investor/index');
                    } elseif ($this->session->userdata['role'] == 'investee') {
                        redirect('investee/index');
                    }
                } else {

                    $bodyData['google_id'] = $userProfile['id'];
                    $bodyData['google_picture'] = $userProfile['image']['url'];
                    $bodyData['fname'] = $userProfile['name']['givenName'];
                    $bodyData['lname'] = $userProfile['name']['familyName'];
                    $bodyData['email'] = $userProfile['emails'][0]['value'];

                    //  die(print_r($profileData));
                    $this->layout['content'] = $this->load->view('user/google_register', $bodyData, true);
                    $this->load->view('layouts/front', $this->layout);
                }
            }
        } else {
            $authUrl = $this->googleplus->client->createAuthUrl();
            header('Location: ' . $authUrl);
        }
    }

    function googleplus_process() {

        $googleId = $this->input->post('google_id');
        $role = $this->input->post('role');
        $email = $this->input->post('email');
        $name = $this->input->post('name');
        $google_picture = $this->input->post('google_picture');
        $random = mt_rand();
        $pass = md5($random);
        $code = rand_string(30);
        

        if ($this->user_model->find_by_email($email)) {
            $this->session->set_flashdata('error-msg', 'This email Id ' . $email . ' is already registered.');
            redirect('user/googleplus_register');            
        }


        $insertData = array(
            'name' => $this->input->post('name'),
            'mob_no' => $this->input->post('mobileno'),
            'email' => $email,
            'role' => $role,
            'code' => $code,
            'password' => $pass,
            'google_id' => $googleId,
            'google_picture' => $google_picture
        );

        if ($this->input->post('company_name')) {
            $insertData['company_name'] = $this->input->post('company_name');
            $displayName = $this->input->post('company_name');
        } else {
            $displayName = $name;
        }

        $userId = $this->user_model->add_user($insertData);
        $userData = $this->user_model->get_googleuser($googleId);

        $this->session->set_userdata('user_id', $userData['id']);
        $this->session->set_userdata('facebook_id', $userData['facebook_id']);
        $this->session->set_userdata('google_picture', $userData['google_picture']);
        $this->session->set_userdata('linkedin_picture', $userData['linkedin_picture']);
        $this->session->set_userdata('image', $userData['image']);
        $this->session->set_userdata('email', $userData['email']);
        $this->session->set_userdata('company_name', $userData['company_name']);
        $this->session->set_userdata('email', $userData['email']);
        $this->session->set_userdata('role', $userData['role']);

        if ($role == 'investee') {
            $this->user_model->add_privacy($userId);
        }        

        $this->Add_User_Registration_Notification($userId,$displayName,"GooglePlus");
        $this->Send_User_Welcome_Email($userData['email'],$displayName,$code);

        if ($this->session->userdata['role'] == 'investor') {
            redirect('investor/index');
        } elseif ($this->session->userdata['role'] == 'investee') {
            redirect('investee/index');
        }
    }

    public function logout() {
        //$userId = $this->session->set_userdata('user_id');
       
         $this->session->unset_userdata('user_id');
                $this->session->unset_userdata('email');
                $this->session->unset_userdata('name');
                $this->session->unset_userdata('company_name');
                $this->session->unset_userdata('image');
                $this->session->unset_userdata('role');
                 $this->session->unset_userdata('pledged');
                  $this->session->unset_userdata('followed');
         $this->session->set_userdata('home_page_visit_count' , 2);
        /* SAVE RESTORE URL*/
        //$this->session->set_userdata('last-role-id', $userId);
        //$this->session->set_userdata('signin-url', $_SERVER["HTTP_REFERER"]);

        $this->load->helper('cookie');
        delete_cookie('remember_me_token');

        redirect(base_url());
    }

    public function investor_info($activePanel = null,$investor_info_id = NULL) {

        $viewData['activePanel'] = '';
        if ($activePanel) {
            $viewData['activePanel'] = $activePanel;
        }

        if ($this->session->userdata('user_id') == '') {
            redirect('user/login');
       }
        
        if($investor_info_id == NULL){
        if($this->input->post('id')!=null)
        {
             $user_id = $this->input->post('id');
            $this->session->set_userdata('update_investor_id',$user_id);
        }
        elseif($this->session->userdata('update_investor_id')!=null)
        {
            $user_id = $this->session->userdata('update_investor_id');
        }
        else
        {
            $user_id = $this->session->userdata('user_id');
        }
      }
      else
      {
         $user_id = $investor_info_id;
      }
       
       
        $countries = $this->user_model->countries();
        $sectors = $this->user_model->sectors();

       // $sectors = $this->user_model->user_sectors();
        $viewData['sectors'] = $sectors;
        $viewData['countries'] = $countries;
        $viewData['user_id'] = $user_id; 

        $this->user_model->update_notifications($user_id, array('seen' => 1));
        $notifications = $this->user_model->get_notifications($this->session->userdata('user_id'), 4);
        $viewData['notifications'] = $notifications;

        $portfolios = array();
        $viewData['states']=array();
        $user = $this->investor_model->fetch_investor($user_id);
       
        // print_r($user); exit;
        $viewData['user'] = $user;
        $states = $this->user_model->states($user['country']);
           
        $portfolios = $this->investor_model->get_portfolios($user_id);
        $viewData['portfolios'] = $portfolios;
        $viewData['states'] = $states;
        if($this->session->userdata('role')=='admin')
        {
            $investors = $this->user_model->get_investors_dropdown();
            $viewData['investors_list'] = $investors;
        }
        $bodyData = $viewData;
   
        if($investor_info_id == NULL){
        $this->layout['content'] = $this->load->view('user/edit_investor_info', $bodyData, true);
       } else
       {
          $this->layout['content'] = $this->load->view('user/view_investor_info', $bodyData, true);
       }
        $this->load->view('layouts/investor', $this->layout);
      
    }



      function investor_view($investor_id = null)
        {
            if(!$investor_id)
            {
                if($this->session->userdata('update_investor_id'))
                {
                 $investor_id = $this->session->userdata('update_investor_id');
              }
              else
              {

                  $investor_id = $this->session->userdata('user_id'); 
              
              }
            }
      
           $this->investor_info(null,$investor_id);
        }

    public function update_investors()
      {
        if ($this->session->userdata('user_id') == '') {
            redirect('user/login');
        }
        
        $user_id = $this->session->userdata('user_id');
        
        $investors = $this->user_model->get_investors_dropdown();
        $viewData['investors_list'] = $investors;

        /*$this->user_model->update_notifications($user_id, array('seen' => 1));*/
        $notifications = $this->user_model->get_notifications($user_id, 4);
        $viewData['notifications'] = $notifications;

        $portfolios = array();
        $viewData['states']=array();
        $user = $this->user_model->find_user($user_id);
        $viewData['user'] = $user;
        $states = $this->user_model->states($user['country']);
           
        $portfolios = $this->investor_model->get_portfolios($user_id);
        $viewData['portfolios'] = $portfolios;
        $viewData['states'] = $states;

        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('user/admin_investor_info', $bodyData, true); 
        $this->load->view('layouts/investor', $this->layout);
    }

    public function channel_partner_info() {
        $user_id = $this->session->userdata('user_id');
        $countries = $this->user_model->countries();
        $sectors = $this->user_model->sectors();
        $viewData['sectors'] = $sectors;
        $viewData['countries'] = $countries;
        $viewData['user_id'] = $user_id;

        $this->user_model->update_notifications($user_id, array('seen' => 1));
        $notifications = $this->user_model->get_notifications($user_id, 4);
        $viewData['notifications'] = $notifications;

        $user = $this->user_model->find_channel_partner($user_id);
        if ($user) {

            $viewData['user'] = $user;
            $states = $this->user_model->states($user['country']);
            $portfolios = array();
            $portfolios = $this->investor_model->get_portfolios($user_id);


            $viewData['user'] = $user;
            $viewData['portfolios'] = $portfolios;
            $viewData['states'] = $states;


            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('user/edit_channel_partner_info', $bodyData, true);
            $this->load->view('layouts/channel_partner', $this->layout);
        } else {
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('user/channel_partner_info', $bodyData, true);
            $this->load->view('layouts/channel_partner', $this->layout);
        }
    }

    public function channel_partner_info_process() {

   
        $id = $this->input->post('user_id');
        $path = "./uploads/users/" . $id;

        if (!is_dir($path)) {
            mkdir($path);
        }

        $sector_expertise = '';
        $post = $this->input->post('sector_expertise');
        if (!empty($post)) {
            $sector_expertise = implode(",", $this->input->post('sector_expertise'));
        }



        $insertData = array(
            'user_id' => $id,
            'experience' => $this->input->post('experience'),
            'role' => $this->input->post('role'),
            'area_of_expertise' => $this->input->post('expertise'),
            'sector_expertise' => $sector_expertise,
        );

        $this->user_model->add_channel_partner($insertData);

        $company_name = '';
        if ($this->input->post('company_name')) {
            $company_name = $this->input->post('company_name');
        }
        $data = array(
            'company_name' => $company_name,
            'country' => $this->input->post('country'),
            'state' => $this->input->post('state'),
            'city' => $this->input->post('city'),
            'fb_url' => $this->input->post('fb_url'),
            'linkedin_url' => $this->input->post('linkedin_url'),
            'twitter_handle' => $this->input->post('twitter_handle')
        );

        $config['upload_path'] = $path;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '1024000';
        $config['max_width'] = '1024000';
        $config['max_height'] = '1024000';

        $this->load->library('upload', $config);

        $file = "image";
        if (!$this->upload->do_upload($file)) {
            //die(print_r($this->upload->display_errors()));
        } else {
            $upload_data = $this->upload->data();
            $data['image'] = $upload_data['file_name'];
        }

        $this->user_model->update_user($data, $id);

        redirect('user/channel_partner_info');
    }

    public function edit_channel_partner_info_process() {

         $id = $this->input->post('user_id');

        $sector_expertise = '';
        $post = $this->input->post('sector_expertise');
        if (!empty($post)) {
            $sector_expertise = implode(",", $this->input->post('sector_expertise'));
        }

        $updateInvestor = array(
            'experience' => $this->input->post('experience'),
            'role' => $this->input->post('role'),
            'area_of_expertise' => $this->input->post('expertise'),
            'sector_expertise' => $sector_expertise
        );

        $this->user_model->update_channel_partner($updateInvestor, $id);

        $company_name = '';
        if ($this->input->post('company_name')) {
            $company_name = $this->input->post('company_name');
        }
        $data = array(

            'name' => $this->input->post('name'),
            'company_name' => $company_name,
            'country' => $this->input->post('country'),
            'state' => $this->input->post('state'),
            'city' => $this->input->post('city'),
            'fb_url' => $this->input->post('fb_url'),
            'linkedin_url' => $this->input->post('linkedin_url'),
            'twitter_handle' => $this->input->post('twitter_handle'),
        );

        $this->user_model->update_user($data, $id);

        $this->session->set_flashdata('success','Profile Updated Successfully');
        redirect('user/channel_partner_info');
    }

    public function investor_info_process() {
        $activePanel = '';
        if ($this->input->post('personal_info_submit')) {
            $activePanel = 'investment';
        }

        if ($this->input->post('investment_submit')) {
            $activePanel = 'startup-portfolio';
        }

        if ($this->input->post('startup_portfolio_submit')) {
            $activePanel = 'interest';
        }

        if ($this->input->post('interest_submit')) {
            $activePanel = 'redirect';//personal-info
        }
		
        $id = $this->input->post('user_id');
        $path = "./uploads/users/" . $id;

        if (!is_dir($path)) {
            mkdir($path);
        }

        $sector_expertise = '';
        $post = $this->input->post('sector_expertise');
        if (!empty($post)) {
            $sector_expertise = implode(",", $this->input->post('sector_expertise'));
        }

        $insertData = array(
            'user_id' => $id,
            'type' => $this->input->post('type'),
            'investor_name' => $this->input->post('name'),
            'experience' => $this->input->post('experience'),
            'role' => $this->input->post('role'),
            'mentoring_sectors' => $this->input->post('mentoring_sectors'),
            'expertise' => $this->input->post('expertise'),
            'duration' => $this->input->post('duration'),
            'sector_expertise' => $sector_expertise,
            'key_points' => $this->input->post('key_points'),
            'created_date' => date('Y-m-d H:i:s')
        );

        $this->user_model->add_investor($insertData);

        $company_name = '';
        if ($this->input->post('company_name')) {
            $company_name = $this->input->post('company_name');
        }
        $data = array(
            'company_name' => $company_name,
            'name' => $this->input->post('name'),
            'country' => $this->input->post('country'),
            'state' => $this->input->post('state'),
            'city' => $this->input->post('city'),
            'fb_url' => $this->input->post('fb_url'),
            'linkedin_url' => $this->input->post('linkedin_url'),
            'twitter_handle' => $this->input->post('twitter_handle')
        );

        $config['upload_path'] = $path;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '1024000';
        $config['max_width'] = '1024000';
        $config['max_height'] = '1024000';

        $this->load->library('upload', $config);

        $file = "image";
		$errImage = '';
        if (!($_FILES[$file]['size'] == 0)) {
            if (!$this->upload->do_upload($file)) {
				$errImage = $this->upload->display_errors();
			//die(print_r($this->upload->display_errors()));
            } else {
                $upload_data = $this->upload->data();
                $data['image'] = $upload_data['file_name'];
            }
        }

		if ($errImage == '') { 
			$this->user_model->update_user($data, $id);
		}
		
        if ($this->input->post('portfolio_name_1')) {
            $portfolio_count = $this->input->post('portfolio_count');

            for ($i = 1; $i <= $portfolio_count; $i++) {

                $portfolioData = array(
                    'user_id' => $id,
                    'name' => $this->input->post('portfolio_name_' . $i),
                    'url' => $this->input->post('portfolio_url_' . $i),
                    'location' => $this->input->post('portfolio_location_' . $i),
                    'sector_id' => $this->input->post('portfolio_sector_' . $i),
                    'role' => $this->input->post('portfolio_role_' . $i),
                    'remark' => $this->input->post('portfolio_remarks_' . $i)
                );

                $PortfolioId = $this->user_model->add_portfolio($portfolioData);

                $path = "./uploads/portfolios/" . $PortfolioId;
                $config['upload_path'] = $path;
                $this->upload->initialize($config);

                if (!is_dir($path)) {
                    mkdir($path);
                }
                $file = 'portfolio_image_' . $i;

                if (!($_FILES[$file]['size'] == 0)) {
                    $portfolioImgData = array();
                    $err = '';
					if (!$this->upload->do_upload($file)) {
                        $err = $this->upload->display_errors();
						//print_r($this->upload->display_errors());
                    } else {
                        $upload_data = $this->upload->data();
                        $portfolioImgData['image'] = $upload_data['file_name'];
                    }

                    if ($err = ''){
						$this->user_model->update_portfolio($portfolioImgData, $PortfolioId);
					}
                }
            }
        }

		if ($activePanel == 'redirect'){
            redirect('investor/profile');
        } else {
            redirect('user/investor_info/' . $activePanel);
        }
    }

    public function edit_investor_info_process() {
    
        $activePanel = '';
        if ($this->input->post('personal_info_submit')) {
            $activePanel = 'investment';
        }

        if ($this->input->post('investment_submit')) {
            $activePanel = 'startup-portfolio';
        }

        if ($this->input->post('startup_portfolio_submit')) {
            $activePanel = 'interest';
        }

        if ($this->input->post('interest_submit')) {
            $activePanel = 'redirect';//personal-info
        }

         // if($this->session->userdata('role')=='admin')
         // {
     
           
         // }

        $id = $this->input->post('user_id');
        if($id!=$this->session->userdata('user_id'))
        {
             $this->session->userdata('update_investor_id',$id);
              $updatePref = array(
                'cheque_size' => $this->input->post('cheque_size'),
                'preference_type' => $this->input->post('preference_type'),
                'got_to_know' => $this->input->post('got_to_know'),
                'others_value' => $this->input->post('others-value')
                );

            $this->user_model->update_user($updatePref,$id);

        }
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '204800';
        $config['max_width'] = '1024';
        $config['max_height'] = '768';

        $this->load->library('upload', $config);

        $sector_expertise = '';
        $post = $this->input->post('sector_expertise');
        if (!empty($post)) {
            $sector_expertise = implode(",", $this->input->post('sector_expertise'));
        }



        $updateInvestor = array(
            'type' => $this->input->post('type'),
            'investor_name' => $this->input->post('name'),
            'experience' => $this->input->post('experience'),
            'role' => $this->input->post('role'),
            'mentoring_sectors' => $this->input->post('mentoring_sectors'),
            'expertise' => $this->input->post('expertise'),
            'duration' => $this->input->post('duration'),
            'sector_expertise' => $sector_expertise,
            'key_points' => $this->input->post('key_points'),
        );

        $user = $this->user_model->find_investor($id);
        if ($user) {
            $this->user_model->update_investor($updateInvestor, $id);
        } else {
            $updateInvestor['user_id'] = $this->input->post('user_id');
            $path = "./uploads/users/" . $id;

            if (!is_dir($path)) {
                mkdir($path);
            }

            $this->user_model->add_investor($updateInvestor);
        } 
            
       // echo $this->db->last_query(); die();
        $company_name = '';
        if ($this->input->post('company_name')) {
            $company_name = $this->input->post('company_name');
        }
        $data = array('company_name' => $company_name,
            'investor_type' => $this->input->post('type'),
            'name' => $this->input->post('name'),
            'mob_no' => $this->input->post('mobileno'),
            'country' => $this->input->post('country'),
            'state' => $this->input->post('state'),
            'city' => $this->input->post('city'),
            'fb_url' => $this->input->post('fb_url'),
            'linkedin_url' => $this->input->post('linkedin_url'),
            'twitter_handle' => $this->input->post('twitter_handle'),
		);
	$pass='';
	if($this->input->post('user_password')){
		$pass=$this->input->post('user_password');
        }
	if(trim($pass)!=''){
          $data['password']=md5($pass);
        }

        $this->user_model->update_user($data, $id);
     //   $this->user_model->update_user($data, $id);
        if($id==$this->session->userdata('user_id'))
        {
            $this->session->set_userdata('name', $this->input->post('name'));
        }

        $portfolio_id = $this->input->post('startup_portfolio');

       // $port = $this->user_model->get_portfolio($id);
      //  print_r($port['name']);
        if ($this->input->post('portfolio_name_1')) {
            $portfolio_count = $this->input->post('portfolio_count');
      
            for ($i = 1; $i <= $portfolio_count; $i++) {

                $PortfolioEditId = $this->input->post('id_' . $i);
             $Action = $this->input->post('action_' . $i);
                    if ($Action == 'edit' ) {
                       $portfolioData = array(
                            'name' => $this->input->post('portfolio_name_' . $i),
                            'url' => $this->input->post('portfolio_url_' . $i),
                            'location' => $this->input->post('portfolio_location_' . $i),
                            'sector_id' => $this->input->post('portfolio_sector_' . $i),
                            'role' => $this->input->post('portfolio_role_' . $i),
                            'remark' => $this->input->post('portfolio_remarks_' . $i)
                        );

                        $this->user_model->update_portfolio($portfolioData, $PortfolioEditId);
                    } else if ($this->input->post('portfolio_name_' . $i) != ''){
                        $portfolioData = array(
                            'user_id' => $id,
                            'name' => $this->input->post('portfolio_name_' . $i),
                            'url' => $this->input->post('portfolio_url_' . $i),
                            'location' => $this->input->post('portfolio_location_' . $i),
                            'sector_id' => $this->input->post('portfolio_sector_' . $i),
                            'role' => $this->input->post('portfolio_role_' . $i),
                            'remark' => $this->input->post('portfolio_remarks_' . $i)
                        );

                        $PortfolioId = $this->user_model->add_portfolio($portfolioData);

                        $path = "./uploads/portfolios/" . $PortfolioId;
                        $config['upload_path'] = $path;
                        $this->upload->initialize($config);

                        if (!is_dir($path)) {
                            mkdir($path);
                        }

                        $err = "";
                        $file = 'portfolio_image_' . $i;
                        $portfolioImgData = array();
                        if (!$this->upload->do_upload($file)) {
                            $err = $this->upload->display_errors();
                            //print_r($this->upload->display_errors());
                        } else {
                            $upload_data = $this->upload->data();
                            $portfolioImgData['image'] = $upload_data['file_name'];
                        }

                        if ($err == ""){
                            $this->user_model->update_portfolio($portfolioImgData, $PortfolioId);
                        }
                    }
            }
        }

        if ($activePanel == 'redirect'){
            redirect('user/investor_view');
        } else {
            redirect('user/investor_info/' . $activePanel);
        }
        
    }

    public function investee_info($activePanel = null) {

        $viewData['activePanel'] = '';
        if ($activePanel) {
            $viewData['activePanel'] = $activePanel;
        }
        if ($this->session->userdata('user_id') == '') {
            redirect('user/login');
        }

        $this->load->model('banners_model');
        $this->layout['banners'] = $this->banners_model->find_benners();
          
        $user_id = $this->session->userdata('user_id');
           
        $countries = $this->user_model->countries();
        $sectors = $this->user_model->sectors();
        $stages = $this->user_model->startup_stages();
        $types = $this->user_model->business_types();
        $viewData['countries'] = $countries;
        $viewData['sectors'] = $sectors;
        $viewData['stages'] = $stages;
        $viewData['types'] = $types;
        $viewData['user_id'] = $user_id;

        $this->user_model->update_notifications($user_id, array('seen' => 1));
        $notifications = $this->user_model->get_notifications($user_id, 4);
        $viewData['notifications'] = $notifications;

        $user = $this->user_model->find_investee($user_id,'Left');

        $viewData['user'] = $user;
        $states = $this->user_model->states($user['country']);
        $viewData['states'] = $states;
        $purposes = $this->user_model->get_investor_use_of_funds($user_id);
        $viewData['purposes'] = $purposes;
        $privacy = $this->investee_model->get_privacy($user_id);
        $viewData['privacy'] = $privacy;

        $investee_id = $user['id'];
        $viewData['investee_id'] = $investee_id;
        $viewData['investee_data'] = $this->user_model->get_investee_team($investee_id);
        // $viewData['investee_achievements'] = $this->user_model->get_investee_achievements($user_id);
        // $viewData['investee_financial_info'] = $this->user_model->get_investee_financial_info($user_id);
        $bodyData = $viewData;
        // $this->layout['content'] = $this->load->view('user/edit_investee_info_17Dec2015', $bodyData, true);
        $this->layout['content'] = $this->load->view('user/edit_investee_info', $bodyData, true);
        $this->load->view('layouts/investee', $this->layout);
    }

    public function investee_info_process() {

        $activePanel = '';
        if ($this->input->post('company_submit')) {
            $activePanel = 'team';
        }

        if ($this->input->post('team_submit')) {
            $activePanel = 'business';
        }

        if ($this->input->post('business_submit')) {
            $activePanel = 'revenue';
        }

        if ($this->input->post('revenue_submit')) {
            $activePanel = 'market';
        }

        if ($this->input->post('market_submit')) {
            $activePanel = 'raise';
        }

        if ($this->input->post('raise_submit')) {
            $activePanel = 'financial';
        }

        if ($this->input->post('financial_submit')) {
            $activePanel = 'achievements';
        }

        if ($this->input->post('achievements_submit')) {
            $activePanel = 'updates';
        }

        if ($this->input->post('updates_submit')) {
            $activePanel = 'redirect';
        }
		
        $id = $this->input->post('user_id');
        $path = "./uploads/users/" . $id;

        if (!is_dir($path)) {
            mkdir($path);
        }


        $insertData = array(
            'user_id' => $id,
            'company_url' => $this->input->post('company_url'),
            'product_url' => $this->input->post('product_url'),
            'year' => $this->input->post('year'),
            'stage' => $this->input->post('stage'),
            'sector' => $this->input->post('sector'),
            'business_type' => $this->input->post('business_type'),
            'funding_history' => $this->input->post('funding_history'),
            'investment_required' => str_replace(',', '', $this->input->post('investment_required')),
            'commitment_per_investor' => str_replace(',', '', $this->input->post('commitment_per_investor')),
            'products_services' => $this->input->post('products_services'),
            'team_summary' => $this->input->post('team_summary'),
            'customer_traction' => $this->input->post('customer_traction'),
            'how_different' => $this->input->post('how_different'),
            'how_we_make_money' => $this->input->post('how_we_make_money'),
            'addressable_market' => str_replace(',', '', $this->input->post('addressable_market')),
            'fixed_opex' => str_replace(',', '', $this->input->post('fixed_opex')),
            'cash_burn' => str_replace(',', '', $this->input->post('cash_burn')),
            'debt' => str_replace(',', '', $this->input->post('debt')),
            'monthly_revenue' => str_replace(',', '', $this->input->post('monthly_revenue')),
            'competition' => $this->input->post('competition'),
            'equity_offered' => $this->input->post('equity_offered'),
            'validity_period' => $this->input->post('validity_period'),
            'created_date' => date('Y-m-d H:i:s')
        );

        $this->user_model->add_investee($insertData);

        if ($this->input->post('purpose')) {
            $purposes = $this->input->post('purpose');
            $purpose_amounts = $this->input->post('purpose_amount');
            $purpose = array();
            foreach ($purposes as $key => $prps) {
                $purpose[$key]['user_id'] = $id;
                $purpose[$key]['purpose'] = $prps;
                $purpose[$key]['amount'] = str_replace(',', '', $purpose_amounts[$key]);
            }

            $this->user_model->delete_investor_use_of_funds($id);
            $this->user_model->add_investor_use_of_funds($purpose);
        }

        $data = array(
            'country' => $this->input->post('country'),
            'state' => $this->input->post('state'),
            'city' => $this->input->post('city'),
            'fb_url' => $this->input->post('fb_url'),
            'linkedin_url' => $this->input->post('linkedin_url'),
            'twitter_handle' => $this->input->post('twitter_handle'),
            'company_name' => $this->input->post('company_name'),
            'name' => $this->input->post('name'),
            'active' => 1
        );

        $config['upload_path'] = $path;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '1024000';
        $config['max_width'] = '1024000';
        $config['max_height'] = '1024000';

        $this->load->library('upload', $config);

        $file = "image";
		$err = '';
        if (!$this->upload->do_upload($file)) {
            $err = $this->upload->display_errors();
			//die(print_r($this->upload->display_errors()));
        } else {
            $upload_data = $this->upload->data();
            $data['image'] = $upload_data['file_name'];
            $this->session->set_userdata('image', $upload_data['file_name']);
        }
		
		if ($err == '') {
			$this->user_model->update_user($data, $id);
		}
		
		if ($activePanel == 'redirect') {
			redirect('/investee/index');
		} else {
			redirect('user/investee_info/' . $activePanel);
		}		
    }

     public function edit_investee_info_process123() {
 
         $activePanel = '';
         if ($this->input->post('company_submit')) {
             $activePanel = 'team';
         }
 
         if ($this->input->post('team_submit')) {
             $activePanel = 'business';
         }
 
         if ($this->input->post('business_submit')) {
             $activePanel = 'revenue';
         }
 
         if ($this->input->post('revenue_submit')) {
             $activePanel = 'market';
         }
 
         if ($this->input->post('market_submit')) {
             $activePanel = 'raise';
         }
 
         if ($this->input->post('raise_submit')) {
             $activePanel = 'financial';
         }
 
         if ($this->input->post('financial_submit')) {
             $activePanel = 'redirect';
         }
 
     
         $id = $this->input->post('user_id'); 
         $investment_required = str_replace(',', '', $this->input->post('investment_required'));
 
        $updateInvestee = array(
             'company_url' => $this->input->post('company_url'),
             'product_url' => $this->input->post('product_url'),
             'year' => $this->input->post('year'),
             'stage' => $this->input->post('stage'),
             'sector' => $this->input->post('sector'),
             'business_type' => $this->input->post('business_type'),
             'funding_history' => $this->input->post('funding_history'),
             'investment_required' => $investment_required,
             'commitment_per_investor' => str_replace(',', '', $this->input->post('commitment_per_investor')),
             'products_services' => $this->input->post('products_services'),
             'team_summary' => $this->input->post('team_summary'),
             'customer_traction' => $this->input->post('customer_traction'),
             'how_different' => $this->input->post('how_different'),
             'how_we_make_money' => $this->input->post('how_we_make_money'),
             'addressable_market' => str_replace(',', '', $this->input->post('addressable_market')),
             'fixed_opex' => str_replace(',', '', $this->input->post('fixed_opex')),
             'cash_burn' => str_replace(',', '', $this->input->post('cash_burn')),
             'debt' => str_replace(',', '', $this->input->post('debt')),
             'monthly_revenue' => str_replace(',', '', $this->input->post('monthly_revenue')),
             'competition' => $this->input->post('competition'),
             'equity_offered' => $this->input->post('equity_offered'),
             'validity_period' => $this->input->post('validity_period'),
         ); 
 
         $user = $this->user_model->find_investee($id);
         if ($user) {
             $this->user_model->update_investee($updateInvestee, $id);
         } else {
             $updateInvestee['user_id'] = $this->input->post('user_id');
             $path = "./uploads/users/" . $id;
 
             if (!is_dir($path)) {
                 mkdir($path);
             }
             $this->user_model->add_investee($updateInvestee);
         } 
 
         if ($this->input->post('purpose')) {
            
           $purposes = $this->input->post('purpose');
            $purpose_amounts = $this->input->post('purpose_amount');
             $purpose = array();
             foreach ($purposes as $key => $prps) {
                 $purpose[$key]['user_id'] = $id;
                 $purpose[$key]['purpose'] = $prps;
                 $purpose[$key]['amount'] = str_replace(',', '', $purpose_amounts[$key]);
             }
        
             $this->user_model->delete_investor_use_of_funds($id);
             $this->user_model->add_investor_use_of_funds($purpose);
         }
        
 
         $data = array(
             'country' => $this->input->post('country'),
             'state' => $this->input->post('state'),
             'city' => $this->input->post('city'),
             'fb_url' => $this->input->post('fb_url'),
             'linkedin_url' => $this->input->post('linkedin_url'),
             'twitter_handle' => $this->input->post('twitter_handle'),
             'company_name' => $this->input->post('company_name'),
             'name' => $this->input->post('name'),
         );
         $this->user_model->update_user($data, $id);
         
         $config['upload_path'] = $path;
         $config['allowed_types'] = 'gif|jpg|png|jpeg';
         $config['max_size'] = '1024000';
         $config['max_width'] = '1024000';
         $config['max_height'] = '1024000';
 
         $this->load->library('upload', $config);
 
         $file = "image";
         $err = '';
         if (!$this->upload->do_upload($file)) {
             $err = $this->upload->display_errors();
             //die(print_r($this->upload->display_errors()));
         } else {
             $upload_data = $this->upload->data();
             $data['image'] = $upload_data['file_name'];
             $this->session->set_userdata('image', $upload_data['file_name']);
         }
         
         if ($err == '') {
             $this->user_model->update_user($data, $id);
         }
 
 
         $this->session->set_userdata('name', $this->input->post('name'));
 
         $followingInvestors = $this->user_model->get_following_user_ids($id);
         $slugs = $this->user_model->find_user($id);
 
         $display_text = $this->session->userdata('name') . ' has updated his profile';
         $link = 'investee/view/' . $slugs['slug_name'];
         foreach ($followingInvestors as $followingInvestor) {
             if ($id != $followingInvestor) {
                 $notification = array(
                     'sender_id' => $id,
                     'receiver_id' => $followingInvestor,
                     'display_text' => $display_text,
                     'link' => $link,
                     'date_created' => date('Y-m-d H:i:s')
                 );
 
                 $this->user_model->add_notification($notification);
             }
         }
 
       if($activePanel!=''){
 
        if ($activePanel == 'redirect') {
              
            redirect('/investee/index');
        } else {
           
            redirect('user/investee_info/' . $activePanel);
        }
     }
         else
         {
             redirect('user/edit_file_upload');
         }
 
 
     }

public function edit_investee_info_process() {
    $activePanel = '';
    if ($this->input->post('company_submit')) {
        $activePanel = 'team';

        $id = $this->input->post('user_id'); 
        $investment_required = str_replace(',', '', $this->input->post('investment_required'));

        $updateInvestee = array(
            'company_url' => $this->input->post('company_url'),
            'product_url' => $this->input->post('product_url'),
            'year' => $this->input->post('year'),
            'stage' => $this->input->post('stage'),
            'sector' => implode(',',$this->input->post('sector')),
            'commitment_per_investor' => str_replace(',', '', $this->input->post('commitment_per_investor')),
            'addressable_market' => str_replace(',', '', $this->input->post('addressable_market')),
            'validity_period' => $this->input->post('validity_period'),
        ); 

        //print_r($updateInvestee); exit;

        $user = $this->user_model->find_investee($id);
        if ($user) {
            $this->user_model->update_investee($updateInvestee, $id);
        } else {
            $updateInvestee['user_id'] = $this->input->post('user_id');
            $path = "./uploads/users/" . $id;

            if (!is_dir($path)) {
                mkdir($path);
            }
            $this->user_model->add_investee($updateInvestee);
        }        

        $data = array(
            'country' => $this->input->post('country'),
            'state' => $this->input->post('state'),
            'city' => $this->input->post('city'),
            'fb_url' => $this->input->post('fb_url'),
            'linkedin_url' => $this->input->post('linkedin_url'),
            'twitter_handle' => $this->input->post('twitter_handle'),
            'company_name' => $this->input->post('company_name')
        );
        $this->user_model->update_user($data, $id);
        
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '1024000';
        $config['max_width'] = '1024000';
        $config['max_height'] = '1024000';

        $this->load->library('upload', $config);

        $file = "image";
        $err = '';
        if (!$this->upload->do_upload($file)) {
            $err = $this->upload->display_errors();
            //die(print_r($this->upload->display_errors()));
        } else {
            $upload_data = $this->upload->data();
            $data['image'] = $upload_data['file_name'];
            $this->session->set_userdata('image', $upload_data['file_name']);
        }
        
        if ($err == '') {
            $this->user_model->update_user($data, $id);
        }

        // $this->session->set_userdata('name', $this->input->post('name'));

        $followingInvestors = $this->user_model->get_following_user_ids($id);
        $slugs = $this->user_model->find_user($id);

        $display_text = $this->session->userdata('name') . ' has updated his profile';
        $link = 'investee/view/' . $slugs['slug_name'];
        foreach ($followingInvestors as $followingInvestor) {
            if ($id != $followingInvestor) {
                $notification = array(
                    'sender_id' => $id,
                    'receiver_id' => $followingInvestor,
                    'display_text' => $display_text,
                    'link' => $link,
                    'date_created' => date('Y-m-d H:i:s')
                );

                $this->user_model->add_notification($notification);
            }
        }            
    }

    if ($this->input->post('team_submit')) {
        $activePanel = 'business';

        $team_info = $this->input->post();
        $no_of_team_mem = count($team_info['name']);
        // $this->user_model->delete_investee_team($team_info['user_id']);

        $to_be_deleted = array_diff(explode(',',$team_info['investee_exist_ids']), $team_info['id']);  
        
        foreach($to_be_deleted as $key => $value)
        {
            $this->user_model->delete_investee_team($value);
        }

        for($i=0; $i< $no_of_team_mem; $i++) {
            $team_data['investee_id'] = $team_info['user_id'];
            $team_data['name'] = $team_info['name'][$i];
            $team_data['designation'] = $team_info['designation'][$i];
            $team_data['experience'] = $team_info['experience'][$i];
            $team_data['linkedin'] = $team_info['linkedin'][$i];
            $team_data['twitter'] = $team_info['twitter'][$i];
            if($team_info['name'][$i] != '')
            {
                if(isset($team_info['id'][$i]))
                {    
                   $this->user_model->update_investee_team($team_data, $team_info['id'][$i]);
                }
                else
                {
                    $this->user_model->add_investee_team($team_data);
                }
            }
        }
    }

    if ($this->input->post('business_submit')) {
        $activePanel = 'revenue';

        $id = $this->input->post('investee_id'); 
        $updateInvestee = array(
            'products_services' => $this->input->post('products_services_business'),
            'business_problem_solved' => $this->input->post('business_problem_solved'),
            'how_different' => $this->input->post('how_different'),
        ); 

        $this->user_model->update_investee($updateInvestee, $id);
    }

    if ($this->input->post('revenue_submit')) {
        $activePanel = 'market';
        // if($this->input->post('how_we_make_money') != '' || $this->input->post('customer_traction') != '')
        // {
            $id = $this->input->post('investee_id'); 
            $updateInvestee = array(
                'how_we_make_money' => $this->input->post('how_we_make_money'),
                'customer_traction' => $this->input->post('customer_traction'),
            ); 

            $this->user_model->update_investee($updateInvestee,$id);
        // }
    }

    if ($this->input->post('market_submit')) {
        $activePanel = 'raise';
        if($this->input->post('competition') != '' || $this->input->post('market_size_commentary ')!= '' || $this->input->post('addressable_market') != '')
        {
            $id = $this->input->post('investee_id'); 
            $updateInvestee = array(
                'competition' => $this->input->post('competition'),
                'addressable_market' => $this->input->post('addressable_market'),
                'market_size_commentary' => $this->input->post('market_size_commentary'),
            ); 

            $this->user_model->update_investee($updateInvestee,$id);
        }
    }

    if ($this->input->post('raise_submit')) {
        $activePanel = 'financial';

        $id = $this->input->post('user_id'); 
        $investee_id = $this->input->post('investee_id'); 

        $investment_required = str_replace(',', '', $this->input->post('investment_required'));
        $commitment_per_investor = str_replace(',', '', $this->input->post('commitment_per_investor'));
        $updateInvestee = array(
            'funding_history' => $this->input->post('funding_history'),
            'investment_required' => $investment_required,
            'commitment_per_investor' => $commitment_per_investor,
            'equity_offered' => $this->input->post('equity_offered'),
        ); 

        $this->user_model->update_investee($updateInvestee,$investee_id);

        if ($this->input->post('purpose')) {
           
            $purposes = $this->input->post('purpose');
            $purpose_amounts = $this->input->post('purpose_amount');
            $purpose = array();
            foreach ($purposes as $key => $prps) {
                if($prps != '')
                {
                    $purpose[$key]['user_id'] = $investee_id;
                    $purpose[$key]['purpose'] = $prps;
                    $purpose[$key]['amount'] = str_replace(',', '', $purpose_amounts[$key]);
                }
            }
            
            $this->user_model->delete_investor_use_of_funds($investee_id);
            if(!empty($purpose))
            {
                $this->user_model->add_investor_use_of_funds($purpose);
            }
        }
    }

    if ($this->input->post('financial_submit')) {
        $activePanel = 'product_info';

        $id = $this->input->post('investee_id'); 

        $updateInvestee = array(
            'fixed_opex' => str_replace(',', '', $this->input->post('fixed_opex')),
            'cash_burn' => str_replace(',', '', $this->input->post('cash_burn')),
            'debt' => str_replace(',', '', $this->input->post('debt')),
            'monthly_revenue' => str_replace(',', '', $this->input->post('monthly_revenue'))
        ); 

        $this->user_model->update_investee($updateInvestee,$id);
    }
    if ($this->input->post('product_info_submit')) {
        $activePanel = 'financial_info';

        $id = $this->input->post('investee_id'); 

        $updateInvestee = array(
            'presentation' => str_replace(',', '', $this->input->post('presentation')),
            'video_link' => str_replace(',', '', $this->input->post('video_link'))
        ); 

        $this->user_model->update_investee($updateInvestee,$id);
    }
    if ($this->input->post('financial_info_submit')) {
        $activePanel = 'achievements';

        $id = $this->input->post('investee_id'); 
        $path = "./uploads/users/" . $id . "/files";

        if (!is_dir($path)) {
            mkdir($path);
        }

        $config['upload_path'] = $path;
        $config['allowed_types'] = 'pdf|doc|docx|jpg|png|gif|ppt|pptx|xls|xlsx|jpeg';
        $config['max_size'] = '1024000';

        $this->load->library('upload', $config);

        if($_FILES['financial_forecast']['name'] != '')
        {            
            $file = "financial_forecast";
            if (!$this->upload->do_upload($file)) {
                // die(print_r($this->upload->display_errors()));
            } else {
                $upload_data = $this->upload->data();
                $data['financial_forecast'] = $upload_data['file_name'];
            }
        }

        if($_FILES['financial_statement']['name'] != '')
        {
            $file = "financial_statement";
            if (!$this->upload->do_upload($file)) {
                // die(print_r($this->upload->display_errors()));
            } else {
                $upload_data = $this->upload->data();
                $data['financial_statement'] = $upload_data['file_name'];
            }
        }

        if($_FILES['financial_statement']['name'] != '')
        {
            $file = "other";
            if (!$this->upload->do_upload($file)) {
                // die(print_r($this->upload->display_errors()));
            } else {
                $upload_data = $this->upload->data();
                $data['other'] = $upload_data['file_name'];
            }
        }

        $this->user_model->update_investee($data, $id);
    }
    if ($this->input->post('achievements_submit')) {
        $activePanel = 'updates';

        // $id = $this->input->post('achievement_id'); 

        $id = $this->input->post('investee_id');

        $updateAchievements = array(
            'media' => $this->input->post('media'),
            'awards' => $this->input->post('awards'),
            'testimonials' => $this->input->post('testimonials')
        ); 

        // print_r($updateAchievements); exit;

        $this->user_model->update_investee($updateAchievements,$id);
    }

    if ($this->input->post('updates_submit')) {
        $activePanel = 'redirect';

        $id = $this->input->post('investee_id'); 
        $updateInvestee = array(
            'updates' => $this->input->post('updates')
        ); 

        $this->user_model->update_investee($updateInvestee, $id);
    }

    // echo $this->uri->segment(4); exit;

    if($this->uri->segment(3) == 'save')
    {
        $activePanel = 'redirect';
    }
    else if($this->uri->segment(3) != '')
    {
        $activePanel = $this->uri->segment(3);
    }

    if($activePanel!=''){

        if ($activePanel == 'redirect') {
             
            redirect('/investee/index');
        } 
        else if($activePanel == 'initial')
        {
            redirect('user/investee_info');
        }
        else {
          
            redirect('user/investee_info/' . $activePanel);
        }
    }
    else
    {
        redirect('user/edit_file_upload');
    }
}

    public function states() {
        $countryId = $this->input->post('countryId');
        $states = $this->user_model->states($countryId);
        $viewData['states'] = $states;
        $this->load->view('user/states', $viewData);
    }

    public function follow() {
        $userId = $this->session->userdata('user_id');
        if ($this->input->post()) {
            $followId = $this->input->post('followId');
            $slugs = $this->user_model->find_user($followId);
            if ($this->user_model->is_followed($userId, $followId)) {
                echo 'error';
            } else {
    
                $this->user_model->follow($userId, $followId);
                $display_text = $this->session->userdata('name') . ' started following you';
                $link = $this->session->userdata('role') . '/view/' . $slugs['slug_name'];
                $notification = array(
                    'sender_id' => $userId,
                    'receiver_id' => $followId,
                    'display_text' => $display_text,
                    'link' => $link,
                    'date_created' => date('Y-m-d H:i:s')
                );
                $followed = $this->session->userdata('followed');
                $followed[] = $followId;
                $this->session->set_userdata('followed', $followed);
                $this->user_model->add_notification($notification);

                if ($this->session->userdata('role') == 'investor') {
                  $user = $this->user_model->find_user($followId);
                    $user_name = '';
                    if ($user['company_name']) {
                        $user_name = $user['company_name'];
                    } else {
                        $user_name = $user['name'];
                    }
                    $activity_text = $this->session->userdata('name') . ' started following <a href="' . base_url() . $user['role'] . '/view/' . $user['id'] . '">' . $user_name . '</a>';
                    $activity = array(
                        'user_id' => $userId,
                        'activity' => $activity_text,
                        'date_created' => date('Y-m-d H:i:s')
                    );

                    $this->user_model->add_investor_activity($activity);

                }

                if($this->session->userdata('pledge_email') == 'on') {

                $this->send_follow_email($user_name,$this->session->userdata('role'),$this->session->userdata('name'),'','admin');
                $this->send_follow_email($user_name,$this->session->userdata('role'),$this->session->userdata('name'),$user['email'],'investee');
            }
            
                echo 'success';
            }
        }
    }

    public function send_follow_email($username,$role,$name,$user_email = NULL,$to)
    {
         
         if($to == 'admin') {
            $templatename = 'Investor_Interest_Admin';
            $email_to   =   EQ_EMAIL;
            $full_name = 'EQ Team';
            $subject    = 'Investor Interest';
            $email_from = 'info@equitycrest.com';
            
            $vars=array(
                    array(
                        'name' => 'SUBJECT',
                        'content' => 'follow'
                    ),
                    array(
                        'name' => 'INVESTOR_TYPE',
                        'content' => $role
                    ),
                     array(
                        'name' => 'FIRST_NAME',
                        'content' => $full_name
                    ),
                      array(
                        'name' => 'INVESTOR_NAME',
                        'content' => $name
                    ),
                      array(
                        'name' => 'COMPANY_NAME',
                        'content' => $username    
                    ),

                       array(
                        'name' => 'PLEDGED_AMOUNT',
                        'content' => '0'
                    ),
                                     
            );
        }

         if($to == 'investee')
        {

            $templatename = 'Investor_Follow_user';
            $email_to   =   $user_email; 
            $full_name = $this->session->userdata('name');
            $subject    = 'Investor Interest';
            $email_from = 'info@equitycrest.com';
            
            $vars=array(
                    array(
                        'name' => 'SUBJECT',
                        'content' => 'follow'
                    ),
                    array(
                        'name' => 'INVESTOR_TYPE',
                        'content' => $role
                    ),
                     array(
                        'name' => 'FIRST_NAME',
                        'content' => $full_name
                    ),
                      array(
                        'name' => 'INVESTOR_NAME',
                        'content' => $name
                    ),
                     
                       array(
                        'name' => 'PLEDGED_AMOUNT',
                        'content' => '0'
                    ),
                       array(
                        'name' => 'COMPANY_NAME',
                        'content' => $username    
                    ),

                                     
            );
        }
    
           $merge=array(
                    array(
                        'rcpt'=> $email_to,
                        'vars'=> $vars
                        )
            ); 
            
            $email = send_mandrill_mail($templatename,$email_to,$full_name,$merge,$email_from);
    }

    public function unfollow() {
        $userId = $this->session->userdata('user_id');
        if ($this->input->post()) {
            $followId = $this->input->post('followId');

            $this->user_model->unfollow($userId, $followId);

            $followed = $this->session->userdata('followed');
            $key = array_search($followId, $followed);
            unset($followed[$key]);
            $this->session->set_userdata('followed', $followed);

            echo 'success';
        }
    }

    public function mail() {
        $this->load->library('email');

        $this->email->set_mailtype("html");
        $this->email->from('abhijit@pyxis.co.in', 'Abhijit Nair');
        $this->email->to('amit@pyxis.co.in');

        $this->email->subject('Email Test');
        $message = 'Please click on below link to activate account.' . "\r\n" . '<br><br>
                     <a href="' . base_url() . 'user/investor_info/45435345">Activation Link</a><br>';
        $this->email->message($message);

        $this->email->send();

        echo $this->email->print_debugger();
    }

    public function success() {
        $bodyData = array();
        $this->layout['content'] = $this->load->view('user/success', $bodyData, true);
        $this->load->view('layouts/front', $this->layout);
    }

    public function file_upload() {
        $userId = $this->session->userdata('user_id');
        $privacy = $this->investee_model->get_privacy($userId);
        $viewData['privacy'] = $privacy;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('user/file_upload', $bodyData, true);
        $this->load->view('layouts/investee', $this->layout);
    }

    public function file_upload_process() {
        $userId = $this->session->userdata('user_id');

        $path = "./uploads/users/" . $userId . "/files";

        if (!is_dir($path)) {
            mkdir($path);
        }

        $data = array('video_link' => $this->input->post('proposal_video'));
        $data['presentation'] = $this->input->post('company_presentation');
        $data['awards'] = $this->input->post('awards');
        $data['testimonials'] = $this->input->post('testimonials');
        $data['media'] = $this->input->post('media');

        $config['upload_path'] = $path;
        $config['allowed_types'] = 'pdf|doc|docx|jpg|png|gif|ppt|pptx|xsl|xslx|jpeg';
        $config['max_size'] = '1024000';

        $this->load->library('upload', $config);

//        $file = "company_presentation";
//        if(!$this->upload->do_upload($file)){
////            die(print_r($this->upload->display_errors()));
//        }else{
//            $upload_data = $this->upload->data();
//            $data['presentation'] = $upload_data['file_name'];
//        }

        $file = "financial_forecast";
        if (!$this->upload->do_upload($file)) {
//            die(print_r($this->upload->display_errors()));
        } else {
            $upload_data = $this->upload->data();
            $data['financial_forecast'] = $upload_data['file_name'];
        }

        $file = "financial_statement";
        if (!$this->upload->do_upload($file)) {
//            die(print_r($this->upload->display_errors()));
        } else {
            $upload_data = $this->upload->data();
            $data['financial_statement'] = $upload_data['file_name'];
        }

        $file = "other";
        if (!$this->upload->do_upload($file)) {
//            die(print_r($this->upload->display_errors()));
        } else {
            $upload_data = $this->upload->data();
            $data['other'] = $upload_data['file_name'];
        }

//        $file = "media";
//        if(!$this->upload->do_upload($file)){
////            die(print_r($this->upload->display_errors()));
//        }else{
//            $upload_data = $this->upload->data();
//            $data['media'] = $upload_data['file_name'];
//        }
//        $file = "awards";
//        if(!$this->upload->do_upload($file)){
////            die(print_r($this->upload->display_errors()));
//        }else{
//            $upload_data = $this->upload->data();
//            $data['awards'] = $upload_data['file_name'];
//        }
//         $file = "testimonials";
//        if(!$this->upload->do_upload($file)){
////            die(print_r($this->upload->display_errors()));
//        }else{
//            $upload_data = $this->upload->data();
//            $data['testimonials'] = $upload_data['file_name'];
//        }
//        $file = "annual_report";
//        if(!$this->upload->do_upload($file)){
////            die(print_r($this->upload->display_errors()));
//        }else{
//            $upload_data = $this->upload->data();
//            $data['annual_report'] = $upload_data['file_name'];
//        }
//        
//        
//        
//        $file = "riskfactors";
//        if(!$this->upload->do_upload($file)){
////            die(print_r($this->upload->display_errors()));
//        }else{
//            $upload_data = $this->upload->data();
//            $data['riskfactors'] = $upload_data['file_name'];
//        }
//        $file = "fund_sources";
//        if(!$this->upload->do_upload($file)){
////            die(print_r($this->upload->display_errors()));
//        }else{
//            $upload_data = $this->upload->data();
//            $data['fund_sources'] = $upload_data['file_name'];
//        }
//        
//        $file = "pl_report";
//        if(!$this->upload->do_upload($file)){
////            die(print_r($this->upload->display_errors()));
//        }else{
//            $upload_data = $this->upload->data();
//            $data['pl_report'] = $upload_data['file_name'];
//        }
//        
//        $file = "bankers";
//        if(!$this->upload->do_upload($file)){
////            die(print_r($this->upload->display_errors()));
//        }else{
//            $upload_data = $this->upload->data();
//            $data['bankers'] = $upload_data['file_name'];
//        }
//        
//        $file = "lawyers";
//        if(!$this->upload->do_upload($file)){
////            die(print_r($this->upload->display_errors()));
//        }else{
//            $upload_data = $this->upload->data();
//            $data['lawyers'] = $upload_data['file_name'];
//        }
//        
//        $file = "auditors";
//        if(!$this->upload->do_upload($file)){
////            die(print_r($this->upload->display_errors()));
//        }else{
//            $upload_data = $this->upload->data();
//            $data['auditors'] = $upload_data['file_name'];
//        }
//        
//        $file = "share_valuation";
//        if(!$this->upload->do_upload($file)){
////            die(print_r($this->upload->display_errors()));
//        }else{
//            $upload_data = $this->upload->data();
//            $data['share_valuation'] = $upload_data['file_name'];
//        }



        $this->user_model->update_investee($data, $userId);

        redirect('investee/index');
    }

    public function edit_file_upload() {

       
        $userId = $this->session->userdata('user_id');
        $user = $this->investee_model->fetch_investee($userId);
        $privacy = $this->investee_model->get_privacy($userId);
        $viewData['privacy'] = $privacy;
        $viewData['user'] = $user;

        $this->user_model->update_notifications($userId, array('seen' => 1));
        $notifications = $this->user_model->get_notifications($userId, 4);
        $viewData['notifications'] = $notifications;
        
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('user/edit_file_upload', $bodyData, true);
        $this->load->view('layouts/investee', $this->layout);
    }

    public function edit_file_upload_process() {

        $userId = $this->session->userdata('user_id');
         $slugs = $this->user_model->find_user($userId);
        $path = "./uploads/users/" . $userId . "/files";

        if (!is_dir($path)) {
            mkdir($path);
        }

        $data = array('video_link' => $this->input->post('proposal_video'));
        $data['presentation'] = $this->input->post('company_presentation');
        $data['awards'] = $this->input->post('awards');
        $data['testimonials'] = $this->input->post('testimonials');
        $data['media'] = $this->input->post('media');

        $config['upload_path'] = $path;
        $config['allowed_types'] = 'pdf|doc|docx|jpg|png|gif|ppt|pptx|xls|xlsx|jpeg';
        $config['max_size'] = '1024000';

        $this->load->library('upload', $config);



        $file = "financial_forecast";
        if (!$this->upload->do_upload($file)) {
//            die(print_r($this->upload->display_errors()));
        } else {
            $upload_data = $this->upload->data();
            $data['financial_forecast'] = $upload_data['file_name'];

            $followingInvestors = $this->user_model->get_following_user_ids($userId);

            $display_text = $this->session->userdata('name') . ' has updated Financial Forecast';
            $link = 'investee/view/' . $slugs['slug_name'];
            foreach ($followingInvestors as $followingInvestor) {
                if ($userId != $followingInvestor) {
                    $notification = array(
                        'sender_id' => $userId,
                        'receiver_id' => $followingInvestor,
                        'display_text' => $display_text,
                        'link' => $link,
                        'date_created' => date('Y-m-d H:i:s')
                    );

                    $this->user_model->add_notification($notification);
                }
            }
        }

        $file = "financial_statement";
        if (!$this->upload->do_upload($file)) {
//            die(print_r($this->upload->display_errors()));
        } else {
            $upload_data = $this->upload->data();
            $data['financial_statement'] = $upload_data['file_name'];

            $followingInvestors = $this->user_model->get_following_user_ids($userId);

            $display_text = $this->session->userdata('name') . ' has updated Financial Statements';
            $link = 'investee/view/' . $slugs['slug_name'];
            foreach ($followingInvestors as $followingInvestor) {
                if ($userId != $followingInvestor) {
                    $notification = array(
                        'sender_id' => $userId,
                        'receiver_id' => $followingInvestor,
                        'display_text' => $display_text,
                        'link' => $link,
                        'date_created' => date('Y-m-d H:i:s')
                    );

                    $this->user_model->add_notification($notification);
                }
            }
        }

        $file = "other";
        if (!$this->upload->do_upload($file)) {
//            die(print_r($this->upload->display_errors()));
        } else {
            $upload_data = $this->upload->data();
            $data['other'] = $upload_data['file_name'];
        }

//        $file = "media";
//        if(!$this->upload->do_upload($file)){
////            die(print_r($this->upload->display_errors()));
//        }else{
//            $upload_data = $this->upload->data();
//            $data['media'] = $upload_data['file_name'];
//        }
//        $file = "awards";
//        if(!$this->upload->do_upload($file)){
////            die(print_r($this->upload->display_errors()));
//        }else{
//            $upload_data = $this->upload->data();
//            $data['awards'] = $upload_data['file_name'];
//        }
//        
//         $file = "testimonials";
//        if(!$this->upload->do_upload($file)){
////            die(print_r($this->upload->display_errors()));
//        }else{
//            $upload_data = $this->upload->data();
//            $data['testimonials'] = $upload_data['file_name'];
//        }

        $this->user_model->update_investee($data, $userId);

        redirect('investee/index');
    }

    public function md5_string() {
        $password = md5(trim($srting));
        echo $password;
    }

    function ajax_image_upload($fieldname = 'image') {
        
        if($this->input->post('id')!=null)
        {
             $id = $this->input->post('id');
        }
        else
        {
            $id = $this->session->userdata('user_id');
        }
        $path = "./uploads/users/" . $id;

        if (!is_dir($path)) {
            mkdir($path);
        }

        // print_r($_FILES); exit;

        $config['upload_path'] = $path;
        if($fieldname == 'image' || $fieldname == 'banner_image')
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
        else
            $config['allowed_types'] = 'pdf|doc|docx|jpg|png|gif|ppt|pptx|xls|xlsx|jpeg';
        $config['max_size'] = '1024000';
        $config['max_width'] = '1024000';
        $config['max_height'] = '1024000';

        $this->load->library('upload', $config);

        $return_data = array();
     
        if (!$this->upload->do_upload()) {
          $return_data['error'] = $this->upload->display_errors('','');
        } else {
            $return_data['error'] = '';
            $upload_data = $this->upload->data();
            $data[$fieldname] = $upload_data['file_name'];
            if($fieldname == 'image')
                $this->user_model->update_user($data, $id);
            else
                $this->user_model->update_investee($data, $id);
            if($this->session->userdata('user_id')==$id)
            {
                $this->session->set_userdata($fieldname,$upload_data['file_name']);
            }
            $return_data['role']= $this->session->userdata('role');
            $img_url = 'uploads/users/' . $id . '/' . $upload_data['file_name'];
            $return_data['img'] = $img_url;

        }

        echo json_encode($return_data);
    }

    public function portfolio() {
        $investees = $this->user_model->get_investees(9);
        $viewData['users'] = $investees;

        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('user/portfolio', $bodyData, true);
        $this->load->view('layouts/front', $this->layout);
    }

    function account_setting() {
        $this->load->model('banners_model');
        $this->layout['banners'] = $this->banners_model->find_benners();
        $viewData = array();
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('user/account_setting', $bodyData, true);
        $this->load->view('layouts/' . $this->session->userdata('role'), $this->layout);
    }

    function account_setting_process() {
        if ($this->input->post()) {
            $id = $this->session->userdata('user_id');
            $pass = md5(trim($this->input->post('password')));
            $data = array('password' => $pass);
            $this->user_model->update_user($data, $id);

            $this->session->set_flashdata('success-msg', 'Your password has been successfully changed.');
            redirect('user/account_setting');
        }
    }

    function get_notifications() {
        $userId = $this->session->userdata('user_id');

        $this->user_model->update_notifications($userId, array('seen' => 1));
        $notifications = $this->user_model->get_notifications($userId, 4);
        if (empty($notifications)) {
            echo '';
        } else {
            $viewData['notifications'] = $notifications;
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('user/notifications', $bodyData);
        }
    }

    function access_request() {
        if ($this->input->post()) {
            $userId = $this->session->userdata('user_id');
            $sectionId = $this->input->post('sectionId');
            $section = $this->input->post('section');
            $investeeId = $this->input->post('investeeId');
            if ($userId != $investeeId) {
                $already_requested = $this->user_model->is_already_access_requested($userId, $investeeId, $sectionId);
                
                if ($already_requested) {
                    $access_granted = $this->user_model->is_access_granted($userId, $investeeId, $sectionId);
                } 

                    
                if (!$already_requested) {
                    $data = array('request_to' => $investeeId,
                        'request_by' => $userId,
                        'section_id' => $sectionId,
                        'created_date' => date('Y-m-d H:i:s')
                    );
                    $rquestId = $this->user_model->add_access_request($data);

                    $display_text = $this->session->userdata('name') . ' made request to access ' . $section;
                    $link = 'investee/notifications';
                    $notification = array(
                        'sender_id' => $userId,
                        'receiver_id' => $investeeId,
                        'record_id' => $rquestId,
                        'display_text' => $display_text,
                        'type' => 'access',
                        'link' => $link,
                        'date_created' => date('Y-m-d H:i:s')
                    );

                    $this->user_model->add_notification($notification);
                    echo 'success';
                } else if ($access_granted == 'declined') {
                    $rquestId = $this->user_model->get_access_request_id($userId, $investeeId, $sectionId);

                    $display_text = $this->session->userdata('name') . ' again made a request to access ' . $section;
                    $link = 'investee/notifications';
                    $notification = array(
                        'sender_id' => $userId,
                        'receiver_id' => $investeeId,
                        'record_id' => $rquestId,
                        'display_text' => $display_text,
                        'type' => 'access',
                        'link' => $link,
                        'date_created' => date('Y-m-d H:i:s')
                    );

                    $this->user_model->add_notification($notification);
                    echo 'success';
                } else {
                    echo 'error';
                }
            } else {
                echo 'error';
            }
        }
    }

    function download_request() {
        if ($this->input->post()) {
            $userId = $this->session->userdata('user_id');
            $sectionId = $this->input->post('sectionId');
            $section = $this->input->post('section');
            $investeeId = $this->input->post('investeeId');
            if ($userId != $investeeId) {
                $already_requested = $this->user_model->is_already_access_requested($userId, $investeeId, $sectionId);

                if ($already_requested) {
                    $access_granted = $this->user_model->is_access_granted($userId, $investeeId, $sectionId);
                } 

                if (!$already_requested) {
                    $data = array('request_to' => $investeeId,
                        'request_by' => $userId,
                        'section_id' => $sectionId,
                        'created_date' => date('Y-m-d H:i:s')
                    );
                    $rquestId = $this->user_model->add_access_request($data);

                    $display_text = $this->session->userdata('name') . 'made request to access ' . $section . ' file.';
                    $link = 'investee/notifications';
                    $notification = array(
                        'sender_id' => $userId,
                        'receiver_id' => $investeeId,
                        'record_id' => $rquestId,
                        'display_text' => $display_text,
                        'type' => 'access',
                        'link' => $link,
                        'date_created' => date('Y-m-d H:i:s')
                    );

                    $this->user_model->add_notification($notification);
                    echo 'success';
                } else if ($access_granted == 'declined') {
                    $rquestId = $this->user_model->get_access_request_id($userId, $investeeId, $sectionId);

                    $display_text = $this->session->userdata('name') . ' again made a request to access ' . $section . ' file.';
                    $link = 'investee/notifications';
                    $notification = array(
                        'sender_id' => $userId,
                        'receiver_id' => $investeeId,
                        'record_id' => $rquestId,
                        'display_text' => $display_text,
                        'type' => 'access',
                        'link' => $link,
                        'date_created' => date('Y-m-d H:i:s')
                    );

                    $this->user_model->add_notification($notification);
                    echo 'success';
                } else {
                    echo 'error';
                }
            } else {
                echo 'error';
            }
        }
    }

    function subscribe_process() {

        $this->load->library('mailchimp_library');
      
       
        if ($this->input->post()) {

            $email = $this->input->post('email');
            $data = array('email' => $email,
                'created_date' => date('Y-m-d H:i:s'));
            $subscriberId = $this->user_model->add_subscriber($data);

            $display_text = $email . ' subscribed Equity Crest newsletter\'s';
            $link = 'admin/subscribed_users';
            $notification = array(
                'record_id' => $subscriberId,
                'display_text' => $display_text,
                'type' => 'subscribe',
                'table_name' => 'subscribe',
                'link' => $link,
                'date_created' => date('Y-m-d H:i:s')
            );

            $subs_adminId = $this->user_model->add_admin_notification($notification);

       //     echo 'success'; 
           
           // $api_key = "bd65b0d64b2552d2cbf0f4a850a69392-us11";
          //  $list_id = "bf8aa4aba1";



            $result = $this->mailchimp_library->call('lists/subscribe', array(
                'id'                => 'bf8aa4aba1',
                'email'             => array('email'=>$email),
              //  'merge_vars'        =>array('FNAME'=>'G','LNAME'=>'P'),
                'double_optin'      => false,
                'update_existing'   => true,
                'replace_interests' => false,
                'send_welcome'      => false,
            ));

          
 
           if (!empty($result['leid']) ) {
                   echo "success";
                }
                else
                {
                    echo "failed";
                }                
          
            
            // $mailchimp = new Mailchimp('bd65b0d64b2552d2cbf0f4a850a69392-us11');
            // $Mailchimp_Lists = new Mailchimp_Lists($mailchimp);
            // $subscriber = $Mailchimp_Lists->subscribe( $list_id, array( 'email' => $email ) );
            //  if ( ! empty( $subscriber['leid'] ) ) {
            //     echo "success";
            // }
            //  else
            //  {
            //      echo "fail";
            //  }

           
                 
        }
    }

     function view_profile($slug_name)
     {
          $id = $this->uri->segment(3, 0); 

          if(is_numeric($id))
          {
             $slugs = $this->user_model->find_user($id);
             header('Location: ' . base_url()."user/view_profile/".$slugs['slug_name']);
             $this->investor_view($id);  
          }
          else
          {
            $where = "slug_name ='".$slug_name."'"; 
            $id = $this->investee_model->get_slug_values('user_master',$where,'id'); 
            $this->investor_view($id[0]['id']);
           }
     }

     function view_profile_investee($slug_name) 
      {
    $id = $this->uri->segment(3, 0); 
          if(is_numeric($id))
          {
             $slugs = $this->user_model->find_user($id);
           //  header('Location: ' . base_url()."user/view_profile_investee/".$slugs['slug_name']);
             redirect('investee/investee_profile/'.$slugs['slug_name']);
            //  $this->investee_info('',$id);  
          }
          else
          {
                // $where = "slug_name ='".$slug_name."'";
                // $id = $this->investee_model->get_slug_values('user_master',$where,'id'); 
                // $this->investee_info('',$id[0]['id']);
            redirect('investee/investee_profile/'.$slug_name);
       }
     }
}

/* End of file welcome.php */
/* Location: ./application/controllers/user.php */
