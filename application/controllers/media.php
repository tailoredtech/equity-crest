<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Media extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('investee_model');
        $this->load->model('user_model');
        $this->load->model('banners_model');
        $this->load->helper('common_helper');
        $this->load->helper('text_helper');

		$this->layout['panels'] = $this->platform();
		
        $this->load->model('banners_model');
        $this->layout['banners']=  $this->banners_model->find_benners();
        $this->layout['unseen_notifications'] = $this->user_model->get_unseen_notification_count($this->session->userdata('user_id'));
    }
    
        function platform(){
            $this->load->model('investee_model');
            $data = $this->investee_model->get_stats('sector');
            $data2 = $this->investee_model->get_stats('location');
            $colorArray = array('#9de219','#033939','#004d38','#006634','#068c35','#90cc38','#3f51b5','#03a9f4','#ff9800','#f9ce1d','#4caf50');
            $i = 0;
            foreach($data as $result){
                
                $color = $colorArray[$i]; //'#'.substr(md5(rand()), 0, 6);
                if(!in_array($color, $colorArray)){
                    $colorArray[] = $color;
                }
                ++$i;

                $result['color'] = $color;
                $result['highlight'] = $color;
                 $result['actual'] = $result['value'];
                $result['value'] = $result['value']*10;
                $jsonData[] = $result;
                $finalData['sector'] = $jsonData;
            }

            $colorArray = array('#3f51b5','#03a9f4','#ff9800','#f9ce1d','#4caf50','#9de219','#033939','#004d38','#006634','#068c35','#90cc38');
            $i = 0;
            foreach($data2 as $result){
                

                $color = $colorArray[$i];  //$color ='#'.substr(md5(rand()), 0, 6);
                if(!in_array($color, $colorArray)){
                    $colorArray[] = $color;
                }
                ++$i;

                $result['color'] = $color;
                $result['highlight'] = $color;
                $result['actual'] = $result['value'];
                $result['value'] = $result['value']*10;
                $jsonData2[] = $result;
                $finalData['location'] = $jsonData2;
            }
            return $finalData;
        }
	
    public function index(){
        $userId = $this->session->userdata('user_id');
        $user = $this->user_model->find_user($userId);	

        $viewData['user'] = $user;
        $bodyData = $viewData; 
		
        $this->layout['content'] = $this->load->view('media/thanks', $bodyData, true);
        $this->load->view('layouts/media', $this->layout);
    }
}

/* End of file investee.php */
/* Location: ./application/controllers/investee.php */