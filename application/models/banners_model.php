<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banners_model extends CI_Model {
    public function __construct()
    {
	parent::__construct();
    }
    function find_benners($banner_flag = '1', $page_title = 'home'){
        $this->db->select('*');
        $this->db->where('page', $page_title);
        $this->db->where('flag', $banner_flag);
        $query=$this->db->get('page_banners');
        // echo $this->db->last_query(); exit;
        $banners=array();
        foreach ($query->result() as $row)
        {
           array_push($banners,$row);
        }
        return $banners;
    }
    
    function get_banners(){
        
        $query = $this->db->get('page_banners');
        $banners = $query->result_array();
        return $banners;
    }
    
    function get_banner($id){
        $query = $this->db->get_where('page_banners',array('id'=>$id));
        $banner = $query->row_array();
        return $banner;
    }
    
    function update_banner($banner){
        $this->db->where('id', $banner['id']);
        $this->db->update('page_banners', $banner); 
        // echo $this->db->last_query(); exit;
        return TRUE;
    }
}
?>