<?php

class user_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function add_user($insertdata) {
        $this->db->insert('user_master', $insertdata);
        return $this->db->insert_id();
    }

    function update_user($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('user_master',$data);

       // echo $this->db->last_query(); die;
    }

    function update_investee($data, $investee_id='', $id='') {
        if(!empty($data)){
            if($id=='')
                $this->db->where('user_id', $investee_id);
            else
                $this->db->where('id', $id);
            $this->db->update('investee', $data);
        }
    }

    function update_investor($data, $id) {
        $this->db->where('user_id', $id);
        $this->db->update('investor', $data);
    }

    function update_channel_partner($data, $id) {
        $this->db->where('user_id', $id);
        $this->db->update('channel_partner', $data);
    }

    function countries() {
        $countries = $this->db->get('country')->result_array();
        return $countries;
    }

    function states($countryId) {
        $states = $this->db->get_where('states', array('country_id' => $countryId))->result_array();
        return $states;
    }

    function sectors() {
        $this->db->select('*');
         $this->db->from('sectors');
        $this->db->order_by('name','ASC');
      $sectors = $this->db->get();
     return  $sectors ->result_array();
        //return $sectors;
    }

    

    function active_sectors() {
        $this->db->select('sectors.*');
        $this->db->distinct('investee.sector');
        $this->db->from('sectors');
        $this->db->join('investee', 'sectors.id = investee.sector');
        $sectors = $this->db->get()->result_array();
        return $sectors;
    }

    function startup_stages() {
        $startup = $this->db->get('startup_stage')->result_array();
        return $startup;
    }

    function get_locations() {
        $locations = $this->db->get('locations')->result_array();
        return $locations;
    }

    function business_types() {
        $startup = $this->db->get('business_types')->result_array();
        return $startup;
    }

    function find_user($userID) {
        $user = $this->db->get_where('user_master', array('id' => $userID))->row_array();
        return $user;
    }

    function get_fbuser($fbId) {
        $this->db->select('*');
        $this->db->from('user_master');
        $this->db->where('facebook_id', $fbId);
        $userdata = $this->db->get()->row_array();
        return $userdata;
    }

    function get_googleuser($google_id) {
        $this->db->select('*');
        $this->db->from('user_master');
        $this->db->where('google_id', $google_id);
        $userdata = $this->db->get()->row_array();
        return $userdata;
    }

    function get_linkedinuser($linkedin_id) {
        $this->db->select('*');
        $this->db->from('user_master');
        $this->db->where('linkedin_id', $linkedin_id);
        $userdata = $this->db->get()->row_array();
        return $userdata;
    }

    function update_facebookid($userId, $facebookId) {
        $data = array('facebook_id' => $facebookId);
        $this->db->where('id', $userId);
        $this->db->update('user_master', $data);
    }

    function update_linkedinid($userId, $linkedId, $linked_picture) {
        $data = array('linkedin_id' => $linkedId, 'linkedin_picture' => $linked_picture);
        $this->db->where('id', $userId);
        $this->db->update('user_master', $data);
    }

    function update_googleid($userId, $linkedId, $google_picture) {
        $data = array('google_id' => $linkedId, 'google_picture' => $google_picture);
        $this->db->where('id', $userId);
        $this->db->update('user_master', $data);
    }

    function find_by_code($code) {
        $user = $this->db->get_where('user_master', array('code' => $code))->row_array();
        return $user;
    }

    function find_by_reset_code($code) {
        $user = $this->db->get_where('user_master', array('reset_code' => $code))->row_array();
        return $user;
    }

    function find_by_email($email) {
        $user = $this->db->get_where('user_master', array('email' => $email))->row_array();
        return $user;
    }

    function find_by_id($id) {
        $user = $this->db->get_where('user_master', array('id' => $id))->row_array();
        return $user;
    }

    function login($email, $password) {
        $user = $this->db->get_where('user_master', array('email' => $email, 'password' => $password))->row_array();
        return $user;
    }

    function add_investor($insertdata) {
        $this->db->insert('investor', $insertdata);
        return $this->db->insert_id();
    }

    function add_channel_partner($insertdata) {
        $this->db->insert('channel_partner', $insertdata);
        return $this->db->insert_id();
    }

    function find_investor($userId,$Join = '') {
        $this->db->select('user_master.*,investor.*');
        $this->db->from('user_master');
        $this->db->join('investor', 'user_master.id = investor.user_id',$Join);
        $this->db->where('user_master.id', $userId);
        $this->db->where('user_master.role', 'investor');
        $investor = $this->db->get()->row_array();
        return $investor;
    }

    function find_channel_partner($userId) {
        $this->db->select('user_master.*,channel_partner.*');
        $this->db->from('user_master');
        $this->db->join('channel_partner', 'user_master.id = channel_partner.user_id');
        $this->db->where('user_master.id', $userId);
        $channel_partner = $this->db->get()->row_array();
        return $channel_partner;
    }

    function get_investors($limit) {
        $this->db->select('SQL_CALC_FOUND_ROWS null as rows,user_master.*,investor.*', false);
        $this->db->from('user_master');
        $this->db->join('investor', 'user_master.id = investor.user_id');
        $this->db->where_in('user_master.status', array('active','semi_active'));
        $this->db->order_by("user_master.name", "asc");
        //$this->db->limit($limit);

        $investors = $this->db->get()->result_array();
        return $investors;
    }

    function get_all_investors() {
        $this->db->select('SQL_CALC_FOUND_ROWS null as rows,user_master.*,investor.user_id,investor.id as investor_id ,investor.sector_expertise, investor.mentoring_sectors,investor.expertise, investor.role as designation', false);
        $this->db->from('user_master');
        $this->db->join('investor', 'user_master.id = investor.user_id', 'LEFT');
        $this->db->where('user_master.role', 'investor');
        $this->db->where_in('user_master.status', array('active'));
        $this->db->order_by("user_master.name", "asc");

        $investors1 = $this->db->get()->result_array();
  /*

		//echo $this->db->last_query();
        $this->db->select('user_master.*');
        $this->db->from('user_master');
        $this->db->where('user_master.role', 'investor');
        $this->db->where_in('user_master.status', array('active','semi_active'));
        $this->db->order_by("user_master.name", "asc");
        $investors2 = $this->db->get()->result_array();
*/
        //$investors = array_merge($investors1, $investors2);
        $investors = $investors1;

        function cmp($a, $b) {
            return strcmp($a["name"], $b["name"]);
        }
		
		usort($investors, "cmp");

        
        //print_r($investors);
        $outputArray = array(); // The results will be loaded into this array.
        $keysArray = array(); // The list of keys will be added here.
        foreach ($investors as $innerArray) { // Iterate through your array.
            if (!in_array($innerArray['id'], $keysArray)) { // Check to see if this is a key that's already been used before.
                $keysArray[] = $innerArray['id']; // If the key hasn't been used before, add it into the list of keys.
                $outputArray[] = $innerArray; // Add the inner array into the output.
            }
        }
//        echo "<pre>";
//        print_r($outputArray);
//        echo "</pre>";
        return $outputArray;
    }

    function get_investors_by_sector($sector_name, $limit) {
        $this->db->select('SQL_CALC_FOUND_ROWS null as rows,user_master.*,investor.*', false);
        $this->db->from('user_master');
        $this->db->join('investor', 'user_master.id = investor.user_id');
        $this->db->like('investor.sector_expertise', $sector_name);
        //$this->db->order_by("user_master.id", "RANDOM");
        //$this->db->where_in('user_master.status', array('active','semi_active'));
        $this->db->limit($limit);
        $investors = $this->db->get()->result_array();
        return $investors;
    }

      public function get_investors_dropdown()
    {

       $this->db->select('user_master.*'); 
        $this->db->from('user_master');
        $this->db->join('investor', 'user_master.id = investor.user_id', 'LEFT');
        $this->db->where('user_master.role', 'investor');
        $this->db->order_by("user_master.name", "asc");
       $all_investors = $this->db->get()->result_array();
    
        return $all_investors;
    }
    function get_investors_by_sector_count($sector_name) {
        $this->db->from('user_master');
        $this->db->join('investor', 'user_master.id = investor.user_id');
        //$this->db->where_in('user_master.status', array('active','semi_active'));
        $this->db->like('investor.sector_expertise', $sector_name);
        $investors = $this->db->count_all_results();
        return $investors;
    }

    function find_investee($userId,$Join = '') {
        //used for edit profile to get  sector_id,stage_id.
        // $this->db->select('user_master.*,investee.*,sectors.name as sector_name,startup_stage.name as stage_name,business_types.name as business_name');
        $this->db->select('user_master.*,investee.*,startup_stage.name as stage_name');
        $this->db->from('user_master');
        $this->db->join('investee', 'user_master.id = investee.user_id', $Join);
        // $this->db->join('sectors', 'investee.sector = sectors.id', $Join);
        $this->db->join('startup_stage', 'investee.stage = startup_stage.id', $Join);
        // $this->db->join('business_types', 'investee.business_type = business_types.id', $Join);
        $this->db->where('user_master.id', $userId);
        $this->db->where('user_master.role', 'investee');
        $investee = $this->db->get()->row_array();
        // echo $this->db->last_query(); exit;
        return $investee;
    }

    function get_investees($limit) {
        $this->db->select('SQL_CALC_FOUND_ROWS null as rows,user_master.*,investee.*,sectors.name as sector_name,startup_stage.name as stage_name,business_types.name as business_name', false);
        $this->db->from('user_master');
        $this->db->join('investee', 'user_master.id = investee.user_id');
        $this->db->join('sectors', 'investee.sector = sectors.id');
        $this->db->join('startup_stage', 'investee.stage = startup_stage.id');
        $this->db->join('business_types', 'investee.business_type = business_types.id');
//        $ids = array(53,80,70,90,91,94,95);
//        $this->db->or_where_in('user_master.id', $ids);
        $this->db->where_in('user_master.status', array('active','semi_active'));
        $this->db->order_by("user_master.id", "RANDOM");
        $this->db->limit($limit);

        $investees = $this->db->get()->result_array();
        return $investees;
    }

   

    function get_all_investees() {
        $this->db->select('SQL_CALC_FOUND_ROWS null as rows,user_master.*,investee.*,sectors.name as sector_name,startup_stage.name as stage_name,business_types.name as business_name', false);
        $this->db->from('user_master');
        $this->db->join('investee', 'user_master.id = investee.user_id');
        $this->db->join('sectors', 'investee.sector = sectors.id','LEFT');
        $this->db->join('startup_stage', 'investee.stage = startup_stage.id');
        $this->db->join('business_types', 'investee.business_type = business_types.id');
        $this->db->where_in('user_master.status', array('active','semi_active', 'funded'));
        //$this->db->order_by("user_master.id", "RANDOM");

        $investees = $this->db->get()->result_array();

       // echo $this->db->last_query(); die;
        return $investees;
    }
    
    function get_funded_investees() {
        $this->db->select('SQL_CALC_FOUND_ROWS null as rows,user_master.*,investee.*,sectors.name as sector_name,startup_stage.name as stage_name,business_types.name as business_name', false);
        $this->db->from('user_master');
        $this->db->join('investee', 'user_master.id = investee.user_id');
        $this->db->join('sectors', 'investee.sector = sectors.id');
        $this->db->join('startup_stage', 'investee.stage = startup_stage.id');
        $this->db->join('business_types', 'investee.business_type = business_types.id');
        $this->db->where_in('user_master.status', array('funded'));
        //$this->db->order_by("user_master.id", "RANDOM");

        $investees = $this->db->get()->result_array();
        return $investees;
    }

    function get_raised_investees() {
        $this->db->select('SQL_CALC_FOUND_ROWS null as rows,user_master.*,investee.*,sectors.name as sector_name,startup_stage.name as stage_name,business_types.name as business_name', false);
        $this->db->from('user_master');
        $this->db->join('investee', 'user_master.id = investee.user_id');
        $this->db->join('sectors', 'investee.sector = sectors.id');
        $this->db->join('startup_stage', 'investee.stage = startup_stage.id');
        $this->db->join('business_types', 'investee.business_type = business_types.id');
        $this->db->where_in('user_master.status', array('active','semi_active'));
        //$this->db->order_by("user_master.id", "RANDOM");

        $investees = $this->db->get()->result_array();
        return $investees;
    }

    function get_featured_investees() {
        $this->db->select('SQL_CALC_FOUND_ROWS null as rows,user_master.*,investee.*,sectors.name as sector_name,startup_stage.name as stage_name,business_types.name as business_name', false);
        $this->db->from('featured_investees');
        $this->db->join('user_master', 'featured_investees.user_id = user_master.id ');
        $this->db->join('investee', 'user_master.id = investee.user_id');
        $this->db->join('sectors', 'investee.sector = sectors.id','LEFT');
        $this->db->join('startup_stage', 'investee.stage = startup_stage.id');
        $this->db->join('business_types', 'investee.business_type = business_types.id');
        $this->db->where_in('user_master.status', array('active','semi_active'));
        $this->db->order_by("featured_investees.sort", "ASC"); 

        $investees = $this->db->get()->result_array();
        return $investees;
    }

    function get_investees_by_sector($sector_id) {
        $this->db->select('user_master.*,investee.*,sectors.name as sector_name,startup_stage.name as stage_name,business_types.name as business_name');
        $this->db->from('user_master');
        $this->db->join('investee', 'user_master.id = investee.user_id');
        $this->db->join('sectors', 'investee.sector = sectors.id');
        $this->db->join('startup_stage', 'investee.stage = startup_stage.id');
        $this->db->join('business_types', 'investee.business_type = business_types.id');
        $this->db->where('investee.sector', $sector_id);
        $this->db->where_in('user_master.status', array('active','semi_active'));
        //$this->db->order_by("user_master.id", "RANDOM");
        //$this->db->limit(9);

        $investees = $this->db->get()->result_array();
        return $investees;
    }

    function get_investees_by_sector_count($sector_id) {
        $this->db->from('user_master');
        $this->db->join('investee', 'user_master.id = investee.user_id');
        $this->db->where('investee.sector', $sector_id);
        $this->db->where_in('user_master.status', array('active','semi_active'));
        $investees = $this->db->count_all_results();
        return $investees;
    }

    function get_investees_by_stage($stage_id) {
        $this->db->select('user_master.*,investee.*,sectors.name as sector_name,startup_stage.name as stage_name,business_types.name as business_name');
        $this->db->from('user_master');
        $this->db->join('investee', 'user_master.id = investee.user_id');
        $this->db->join('sectors', 'investee.sector = sectors.id');
        $this->db->join('startup_stage', 'investee.stage = startup_stage.id');
        $this->db->join('business_types', 'investee.business_type = business_types.id');
        $this->db->where_in('user_master.status', array('active','semi_active'));
        $this->db->where('investee.stage', $stage_id);
        //$this->db->order_by("user_master.id", "RANDOM");
        //$this->db->limit(9);

        $investees = $this->db->get()->result_array();
        return $investees;
    }

    function get_investees_by_location($city) {
        $this->db->select('user_master.*,investee.*,sectors.name as sector_name,startup_stage.name as stage_name,business_types.name as business_name');
        $this->db->from('user_master');
        $this->db->join('investee', 'user_master.id = investee.user_id');
        $this->db->join('sectors', 'investee.sector = sectors.id');
        $this->db->join('startup_stage', 'investee.stage = startup_stage.id');
        $this->db->join('business_types', 'investee.business_type = business_types.id');
        $this->db->where_in('user_master.status', array('active','semi_active'));
        $this->db->where('user_master.city', $city);
        //$this->db->order_by("user_master.id", "RANDOM");
        //$this->db->limit(9);

        $investees = $this->db->get()->result_array();
        return $investees;
    }

    public function get_investees_by_investement_sizes($size) {
        $this->db->select('user_master.*,investee.*,sectors.name as sector_name,startup_stage.name as stage_name,business_types.name as business_name');
        $this->db->from('user_master');
        $this->db->join('investee', 'user_master.id = investee.user_id');
        $this->db->join('sectors', 'investee.sector = sectors.id');
        $this->db->join('startup_stage', 'investee.stage = startup_stage.id');
        $this->db->join('business_types', 'investee.business_type = business_types.id');
        if ($size == 5) {
            $this->db->where('investment_required >=', 50000000);
        } elseif ($size == 4) {
            $this->db->where('investment_required >=', 10000000);
            $this->db->where('investment_required <=', 50000000);
        } elseif ($size == 3) {
            $this->db->where('investment_required >=', 5000000);
            $this->db->where('investment_required <=', 10000000);
        } elseif ($size == 2) {
            $this->db->where('investment_required >=', 1000000);
            $this->db->where('investment_required <=', 5000000);
        } elseif ($size == 1) {
            $this->db->where('investment_required <=', 1000000);
        }

        $this->db->where('user_master.status', 'active');
        $this->db->or_where('user_master.status', 'semi_active');
        //$this->db->order_by("user_master.id", "RANDOM");
        //$this->db->limit(9);

        $investees = $this->db->get()->result_array();
        return $investees;
    }

    function get_investees_by_status($status) {
        $this->db->select('user_master.*,investee.*,sectors.name as sector_name,startup_stage.name as stage_name,business_types.name as business_name');
        $this->db->from('user_master');
        $this->db->join('investee', 'user_master.id = investee.user_id');
        $this->db->join('sectors', 'investee.sector = sectors.id');
        $this->db->join('startup_stage', 'investee.stage = startup_stage.id');
        $this->db->join('business_types', 'investee.business_type = business_types.id');
        $this->db->where_in('user_master.status', array('active','semi_active'));
        if ($status == "top") {
            $this->db->order_by("investee.fund_raise", "DESC");
        } else {
            $this->db->order_by("user_master.id", "DESC");
        }
        $this->db->limit(9);

        $investees = $this->db->get()->result_array();
        return $investees;
    }
    function get_count()
    {
        $query = $this->db->query('SELECT FOUND_ROWS() AS `Count`');
        return $query->row()->Count;
    }
    public function search_investees($options, $limit) {
        $this->db->select('SQL_CALC_FOUND_ROWS null as rows,user_master.*,investee.*,sectors.name as sector_name,startup_stage.name as stage_name,business_types.name as business_name',false);
        $this->db->from('user_master');
        $this->db->join('investee', 'user_master.id = investee.user_id');
        $this->db->join('sectors', 'investee.sector = sectors.id');
        $this->db->join('startup_stage', 'investee.stage = startup_stage.id');
        $this->db->join('business_types', 'investee.business_type = business_types.id');
        $this->db->where_in('user_master.status', array('active','semi_active'));

        if ($options['name']) {
            $this->db->like('user_master.company_name', $options['name']);
        }

        if ($options['stage'] && !(in_array('All', $options['stage'])) ) {
            $this->db->where_in('investee.stage', $options['stage']);
        }

        if ($options['sector'] && !(in_array('All', $options['sector']))) {
            $this->db->where_in('investee.sector', $options['sector']);
        }

        if ($options['size'] && !(in_array('All', $options['size']))) {
            $size = $options['size'];
            if (in_array(5, $size)) {
                $size_whr[]='investment_required >= 50000000';
               } 
            if (in_array(4, $size)) {
                $size_whr[]='(investment_required >= 10000000 AND investment_required <= 50000000)';
            }
            if (in_array(3, $size)) {
               $size_whr[]='(investment_required >= 5000000 AND investment_required <= 10000000)';
            } 
            if (in_array(2, $size)) {
                $size_whr[]='(investment_required >= 1000000 AND investment_required <= 5000000)';
            } 
            if (in_array(1, $size)) {
                $size_whr[]='investment_required <= 1000000';
            }
            $size_whr_or='('.implode(' OR ', $size_whr).')';
           $this->db->where($size_whr_or);  
        }

        if ($options['location'] && !(in_array('All', $options['location'])) ) {

            if (in_array('Bengaluru', $options['location'])) 
			{
                array_push($options['location'], 'Bengaluru');
			} 
				$this->db->where_in('user_master.city', $options['location']);

        }


        //$this->db->order_by("user_master.id", "RANDOM");
        $this->db->limit($limit);

        $investees = $this->db->get()->result_array();
        return $investees;
    }

    function add_investor_team($data) {
        $this->db->insert_batch('investor_team', $data);
    }

    function get_investor_team($userId) {
        $team = $this->db->get_where('investor_team', array('user_id' => $userId))->result_array();
        return $team;
    }

    function delete_investor_team($userId) {
        $this->db->where('user_id', $userId);
        $this->db->delete('investor_team');
    }

    function add_investee($insertdata) {
        $this->db->insert('investee', $insertdata);
        return $this->db->insert_id();
    }

    function add_investee_team($insertdata) {
        $this->db->insert('investee_team_summary', $insertdata);
        return $this->db->insert_id();
    }

    function get_investee_team($investee_id)
    {
        $this->db->from('investee_team_summary');
        $this->db->where('investee_id', $investee_id);
        $result = $this->db->get()->result_array();

        // print_r($this->db->last_query()); exit;

        return $result;
    }

    function get_investee_achievements($investee_id)
    {
        $this->db->from('investee_achievements');
        $this->db->where('investee_id', $investee_id);
        $result = $this->db->get()->row_array();

        // print_r($result); exit;

        return $result;
    }

    function is_followed($userId, $followId) {
        $this->db->from('follow');
        $this->db->where('user_id', $userId);
        $this->db->where('follow_id', $followId);
        return $this->db->count_all_results();
    }

    function follow($userId, $followId) {
        $insertData = array('user_id' => $userId, 'follow_id' => $followId);
        $this->db->insert('follow', $insertData);
    }

    function unfollow($userId, $followId) {
        $data = array('user_id' => $userId, 'follow_id' => $followId);
        $this->db->where($data);
        $this->db->delete('follow');
    }

    public function get_following_user_ids($followId) {
        $this->db->select('user_id');
        $this->db->from('follow');
        $this->db->where('follow_id', $followId);
        $investees = $this->db->get()->result_array();
        $user_ids = array();
        foreach ($investees as $investee) {
            $user_ids[] = $investee['user_id'];
        }
        return $user_ids;
    }

    function add_investor_use_of_funds($data) {

        $this->db->insert_batch('use_of_funds', $data);
        return $this->db->insert_id();
    }

    function get_investor_use_of_funds($userId) {
        $team = $this->db->get_where('use_of_funds', array('user_id' => $userId))->result_array();
        return $team;
    }

    function delete_investor_use_of_funds($userId) {
        $this->db->where('user_id', $userId);
        $this->db->delete('use_of_funds');
    }

    function delete_investee_team($id) {
        $this->db->where('id', $id);
        $this->db->delete('investee_team_summary');
    }

    function add_portfolio($insertdata) {
        $this->db->insert('investor_portfolios', $insertdata);
        return $this->db->insert_id();
    }

    function update_portfolio($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('investor_portfolios', $data);
    }

    function update_investee_team($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('investee_team_summary', $data);
    }

    function update_achievements($data, $id = '') {
        if($id != '')
        {
            $this->db->where('id', $id);
            $this->db->update('investee_achievements', $data);
        }
        else
        {
            $this->db->insert('investee_achievements', $data);
            return $this->db->insert_id();
        }
    }

    function delete_portfolio($id) {
        $this->db->where('id', $id);
        $this->db->delete('investor_portfolios');
    }


    function get_portfolio($user_id) {
        $this->db->select('*');
        $this->db->where('user_id', $user_id);
        $res = $this->db->get('investor_portfolios');
        return $res->result_array();
    }

    function add_notification($insertdata) {
        $this->db->insert('notification', $insertdata);
        return $this->db->insert_id();
    }

    function delete_notification($id) {
        $this->db->where('id', $id);
        $this->db->delete('notification');
    }

    function add_admin_notification($insertdata) {
        $this->db->insert('admin_notifications', $insertdata);
        return $this->db->insert_id();
    }

    function get_notifications($userId, $limit) {
        $this->db->select('user_master.image,user_master.status,notification.*');
        $this->db->from('notification');
        $this->db->join('user_master', 'notification.sender_id = user_master.id');
        $this->db->where('notification.receiver_id', $userId);
        $this->db->order_by("notification.id", "DESC");
        $this->db->limit($limit);
        $notifications = $this->db->get()->result_array();
        return $notifications;
    }

    function get_all_notifications($userId) {
        $this->db->select('user_master.image,user_master.status,notification.*');
        $this->db->from('notification');
        $this->db->join('user_master', 'notification.sender_id = user_master.id');
        $this->db->where('notification.receiver_id', $userId);
        $this->db->order_by("notification.id", "DESC");
        $notifications = $this->db->get()->result_array();
        return $notifications;
    }

    function get_unseen_notification_count($userId) {
        $this->db->from('notification');
        $this->db->where('notification.receiver_id', $userId);
        $this->db->where('notification.seen', 0);
        $count = $this->db->count_all_results();
        return $count;
    }

    function update_notifications($userId, $data) {
        $this->db->where('receiver_id', $userId);
        $this->db->update('notification', $data);
    }

    function add_investor_activity($insertdata) {
        $this->db->insert('investor_activities', $insertdata);
        return $this->db->insert_id();
    }

    function get_investor_activities($userId, $limit) {
        $this->db->select('investor_activities.*');
        $this->db->from('investor_activities');
        $this->db->where('investor_activities.user_id', $userId);
        $this->db->order_by("investor_activities.id", "DESC");
        $this->db->limit($limit);
        $activities = $this->db->get()->result_array();
        return $activities;
    }

    function add_privacy($investeeId) {
        $this->db->insert('privacy_settings', array('investee_id' => $investeeId));
    }

    function add_refer($insertdata) {
        $this->db->insert('refer', $insertdata);
        return $this->db->insert_id();
    }

    function add_subscriber($data) {
        $this->db->insert('subscribe', $data);
        return $this->db->insert_id();
    }

    function get_bids_offer() {
        $this->db->select('bids_offer.*,sectors.name as sector_name,user_master.company_name as company_name');
        $this->db->from('bids_offer', 'sectors', 'investee');
        $this->db->join('sectors', 'bids_offer.sector = sectors.id');
        $this->db->join('user_master', 'bids_offer.company_id = user_master.id');
        $bids = $this->db->get()->result_array();
        return $bids;
    }

    function add_access_request($data) {
        $this->db->insert('access_requests', $data);
        return $this->db->insert_id();
    }

    function get_access_request($id) {
        $this->db->select('access_requests.*,investee_sections.*');
        $this->db->from('access_requests');
        $this->db->join('investee_sections', 'access_requests.section_id = investee_sections.id');
        $this->db->where('access_requests.id', $id);
        $request = $this->db->get()->row_array();
        return $request;
    }

    function is_access_granted($userId, $investeeId, $sectionId) {
        $this->db->select('access_requests.status');
        $this->db->from('access_requests');
        $this->db->where('section_id', $sectionId);
        $this->db->where('request_by', $userId);
        $this->db->where('request_to', $investeeId);
        $request = $this->db->get()->row();
        return $request->status;
    }    

    function get_access_request_id($userId, $investeeId, $sectionId) {
        $this->db->select('access_requests.id');
        $this->db->from('access_requests');
        $this->db->where('request_to', $investeeId);
        $this->db->where('request_by', $userId);
        $this->db->where('section_id', $sectionId);
        $request = $this->db->get()->row();
        return $request->id;
    }

    function is_already_access_requested($userId, $investeeId, $sectionId) {
        $this->db->from('access_requests');
        $this->db->where('request_to', $investeeId);
        $this->db->where('request_by', $userId);
        $this->db->where('section_id', $sectionId);
        return $this->db->count_all_results();
    }

    function update_remember($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('user_master', $data);
    }

    function remember($rememberHash) {
        $user = $this->db->get_where('user_master', array('remember' => $rememberHash))->row_array();
        return $user;
    }

    function get_email_settings()
    {
      $this->db->select('*');
      $this->db->from('email_settings');
      $settings = $this->db->get()->row();

      return $settings;

    }

    function get_slug_names($slug_name)
    {
        $this->db->select('slug_name');
        $this->db->from('user_master');
        $this->db->where('slug_name',$slug_name);
        $result = $this->db->get()->row();
        return $result;
    }

}

?>