<?php
class admin_model extends CI_Model {
    public function __construct()
    {
	parent::__construct();
    }
    
    function login($email,$password){
        $user = $this->db->get_where('user_master', array('email' => $email,'password'=>$password))->row_array();
        return $user;
    }
    
    function add_user($insertdata){
        $this->db->insert('user_master', $insertdata);
        return $this->db->insert_id();
    }
    
    function update_user($data,$id){
        $this->db->where('id', $id);
        $this->db->update('user_master', $data);
    }

    function update_email_settings($table,$data)
    {
         $res = $this->db->update($table, $data);
         return $res; 
    }
    
    function update_users($data,$ids){
        $this->db->where_in('id', $ids);
        $this->db->update('user_master', $data);
    }
    
    function countries(){
        $countries = $this->db->get('country')->result_array();
        return $countries;
    }
    
    function states($countryId){
        $states = $this->db->get_where('states', array('country_id' => $countryId))->result_array();
        return $states;
    }

    function get_investors(){
        $investors = $this->db->get_where('user_master', array('role' => 'investor'))->result_array();
        return $investors;
    }

    function download($userType){
        $this->db->select('name, email, role, status, created_date, activated_on');
        $this->db->where(array('role' => $userType));
        $this->db->order_by('created_date', 'DESC');
        $investors = $this->db->get('user_master')->result_array();
        return $investors;
    }
    
    function get_active_investors(){
        $investors = $this->db->order_by("id","desc")->get_where('user_master', array('role' => 'investor','status'=>'active'))->result_array();
        return $investors;
    }
    
    function get_inactive_investors(){
        $investors = $this->db->order_by("id","desc")->get_where('user_master', array('role' => 'investor','status'=>'inactive'))->result_array();
        return $investors;
    }
    
    function fetch_investor($id){
        $this->db->select('user_master.*,investor.*');
        $this->db->from('user_master');
        $this->db->join('investor', 'user_master.id = investor.user_id');
        $this->db->where('user_master.id', $id);
        $investor = $this->db->get()->row_array();
        return $investor;
    }
    
    function get_active_partners(){
        $partners = $this->db->order_by("id","desc")->get_where('user_master', array('role' => 'channel_partner','status'=>'active'))->result_array();
        return $partners;
    }
    
    function get_inactive_partners(){
        $partners = $this->db->order_by("id","desc")->get_where('user_master', array('role' => 'channel_partner','status'=>'inactive'))->result_array();
        return $partners;
    }
    
    function fetch_partner($id){
        $this->db->select('user_master.*,channel_partner.*');
        $this->db->from('user_master');
        $this->db->join('channel_partner', 'user_master.id = channel_partner.user_id');
        $this->db->where('user_master.id', $id);
        $partner = $this->db->get()->row_array();
        return $partner;
    }
    
    function get_users($options){
        $this->db->select('user_master.*');
        $this->db->from('user_master');
        
        if($options['name']){
            $this->db->like('user_master.name', $options['name']); 
        }
        
        if($options['company_name']){
            $this->db->like('user_master.company_name', $options['company_name']); 
        }
        
        if($options['role']){
            $this->db->where('user_master.role', $options['role']); 
        }
        
        if($options['status']){
            $this->db->where('user_master.status', $options['status']); 
        }
        
        $this->db->order_by("user_master.id", "DESC");
        $users = $this->db->get()->result_array();
        return $users;
    }
    
    function get_active_users(){
        $users = $this->db->get_where('user_master', array('status'=>'active'))->result_array();
        return $users;
    }
    
    function get_inactive_users(){
        $users = $this->db->get_where('user_master', array('status'=>'inactive'))->result_array();
        return $users;
    }
    
    function get_investees(){
        $investees = $this->db->get_where('user_master', array('role' => 'investee'))->result_array();
        return $investees;
    }
    
    function get_semiactive_investees(){
        $investees = $this->db->order_by("id","desc")->get_where('user_master', array('role' => 'investee','status'=>'semi_active'))->result_array();
        return $investees;        
    }
    function get_funded_investees(){
        $investees = $this->db->order_by("id","desc")->get_where('user_master', array('role' => 'investee','status'=>'funded'))->result_array();
        return $investees;        
    }
    function get_active_investees(){
        $investees = $this->db->order_by("id","desc")->get_where('user_master', array('role' => 'investee','status'=>'active'))->result_array();
        return $investees;
    }
    
    function get_inactive_investees(){
        $investees = $this->db->order_by("id","desc")->get_where('user_master', array('role' => 'investee','status'=>'inactive'))->result_array();
        return $investees;
    }
    
    function get_featured_investees(){
        $this->db->select('user_master.*,featured_investees.sort');
        $this->db->from('featured_investees');
        $this->db->join('user_master', 'featured_investees.user_id = user_master.id ');
        $this->db->order_by("featured_investees.sort", "ASC");
        $investees = $this->db->get()->result_array();
        return $investees;
    }
    
    function fetch_investee($id){
        $this->db->select('user_master.*,investee.*');
        $this->db->from('user_master');
        $this->db->join('investee', 'user_master.id = investee.user_id');
        $this->db->where('user_master.id', $id);
        $this->db->order_by('investee.id', 'DESC');
	$investee = $this->db->get()->row_array();
        return $investee;
    }
    
    function sectors(){
        $sectors = $this->db->get('sectors')->result_array();
        return $sectors;
    }
    
    function startup_stages(){
        $startup = $this->db->get('startup_stage')->result_array();
        return $startup;
    }
    
    function business_types(){
        $startup = $this->db->get('business_types')->result_array();
        return $startup;
    }
    
    function find_by_code($code){
        $user = $this->db->get_where('user_master', array('code' => $code))->row_array();
        return $user;
    }
    
    function add_investor($insertdata){
        $this->db->insert('investor', $insertdata);
        return $this->db->insert_id();
    }
    
    function add_investor_team($data){
        $this->db->insert_batch('investor_team',$data);
    }
    
    function add_investee($insertdata){
        $this->db->insert('investee', $insertdata);
        return $this->db->insert_id();
    }
    
    function get_referred_companies(){
        $this->db->select('user_master.name as referred_by,refer.*');
        $this->db->from('refer');
        $this->db->join('user_master', 'refer.user_id = user_master.id');
        $company = $this->db->get()->result_array();
        return $company;
    }
    
    function get_advice_requests(){
        $this->db->select('user_master.name as asked_by,advice_requests.*');
        $this->db->from('advice_requests');
        $this->db->join('user_master', 'advice_requests.user_id = user_master.id');
        $company = $this->db->get()->result_array();
        return $company;
    }
    
    function get_users_count(){
        $this->db->from('user_master');
        $count = $this->db->count_all_results();
        return $count;
    }
    
    function get_investees_count(){
        $this->db->from('user_master');
        $this->db->where('role', 'investee');
        $count = $this->db->count_all_results();
        return $count;
    }
    
    function get_investors_count(){
        $this->db->from('user_master');
        $this->db->where('role', 'investor');
        $count = $this->db->count_all_results();
        return $count;
    }
    
    function get_channel_partners_count(){
        $this->db->from('user_master');
        $this->db->where('role', 'channel_partner');
        $count = $this->db->count_all_results();
        return $count;
    }
    
	function isFeatured($UserID) {
        $this->db->from('featured_investees');
        $this->db->where('user_id', $UserID);
        $count = $this->db->count_all_results();
        return $count;	
	}
	
    function delete_featured($ids){
        $this->db->where_in('user_id', $ids);
        $this->db->delete('featured_investees'); 
    }
    
    function add_featured($data){
        $this->db->insert_batch('featured_investees', $data);  
    }
    
    function get_subscribed_users(){
        $subscribes = $this->db->get('subscribe')->result_array();
        return $subscribes;
    }
    
    function get_all_notifications(){
        $this->db->from('admin_notifications');
        $this->db->order_by("admin_notifications.id", "DESC");
        $notifications = $this->db->get()->result_array();
        
        return $notifications;
    }
    
    function get_meetings(){
        $this->db->select('meetings.*,investor.name as investor_name,investee.name as investee_name');
       $this->db->from('meetings');
       $this->db->join('user_master as investor', 'meetings.investor_id = investor.id');
       $this->db->join('user_master as investee', 'meetings.investee_id = investee.id');
       $this->db->order_by("meetings.id", "DESC");
       $meetings = $this->db->get()->result_array();
        
       return $meetings; 
    }
    
    function get_pledges($investeeId){
       $this->db->select('pledge.*,user_master.id as investor_id,user_master.name as investor_name');
       $this->db->from('pledge');
       $this->db->join('user_master', 'user_master.id=pledge.investor_id');
       $this->db->where('pledge.investee_id', $investeeId);
       //$this->db->order_by("pledge.id", "DESC");
       $pledges = $this->db->get()->result_array();
       return $pledges;
    }
    
    function add_approval($id,$data){                           //pledge approval from admin
        $this->db->where('id', $id);
        $this->db->update('pledge', $data);
    }
    
    function get_current_fundraise($investeeId){
       $this->db->select('investee.fund_raise');
       $this->db->from('investee');
       $this->db->where('investee.user_id', $investeeId);       
       $funds = $this->db->get()->result_array();
       return $funds;
    }
    
    function update_fundraise($investeeId,$data){
        $this->db->where('user_id',$investeeId);
        $this->db->update('investee', $data);
    }
    
    function get_qna($investeeId){                                  //questins & answers for investee
       $this->db->select('questions.*,answers.*,user_master.id as investor_id,user_master.name as investor_name');
       $this->db->from('questions');
       $this->db->join('answers', 'questions.id=answers.question_id');
       $this->db->join('user_master', 'questions.investor_id=user_master.id');
       $this->db->where('questions.investee_id', $investeeId);
       $this->db->order_by("questions.id", "DESC");
       $qna = $this->db->get()->result_array();
       return $qna;
    }
    
    function verify_user($id,$data){                           //user verify from admin
        $this->db->where('id', $id);
        $this->db->update('user_master', $data);
        //echo $str = $this->db->last_query(); 
    }
    
    function change_priority($id,$data){                           //change deal priority
        $this->db->where('user_id', $id);
        $this->db->update('featured_investees', $data);
        //echo $str = $this->db->last_query(); 
    }
}
?>
