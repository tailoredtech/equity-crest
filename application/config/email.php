<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['useragent'] = 'CodeIgniter';
$config['mailpath'] = '/usr/sbin/sendmail';	// Sendmail path
$config['protocol'] = 'Sendmail';	// mail/sendmail/smtp
$config['smtp_host'] = 'smtp.gmail.com';		// SMTP Server.  Example: mail.earthlink.net
$config['smtp_user'] = 'info@equitycrest.com';		// SMTP Username
$config['smtp_pass'] = 'beegram123456';		// SMTP Password
$config['smtp_port'] = 465;		//'587' SMTP Port	
$config['smtp_timeout'] = 15;		// SMTP Timeout in seconds
$config['smtp_crypto'] 	= 'ssl';		// SMTP Encryption. Can be null, tls or ssl.
$config['wordwrap']  = TRUE;		// TRUE/FALSE  Turns word-wrap on/off
$config['wrapchars'] = 76;		// Number of characters to wrap at.
$config['mailtype'] = 'html';	// text/html  Defines email formatting
$config['charset'] 	= 'utf-8';	// Default char set: iso-8859-1 or us-ascii
$config['multipart'] = 'mixed';	// "mixed" (in the body) or "related" (separate)
$config['starttls'] 	= FALSE;
$config['newline'] 	= '\r\n';