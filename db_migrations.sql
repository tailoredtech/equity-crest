ALTER TABLE `page_banners` ADD `background_image` MEDIUMTEXT NULL AFTER `background_color`, ADD `banner_link` VARCHAR(255) NULL AFTER `background_image`;

ALTER TABLE `user_master` ADD `investor_type` VARCHAR(50) NULL AFTER `name`;

ALTER TABLE `investee` ADD `banner_image` MEDIUMTEXT NULL AFTER `business_type`, ADD `snapshot` MEDIUMTEXT NULL AFTER `banner_image`;

ALTER TABLE `investor` CHANGE `type` `type` ENUM('individual','angel','Angel', 'Family Office','Venture Fund','Angel Fund','Accelarator') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

ALTER TABLE `user_master` ADD `cheque_size` VARCHAR(100) NOT NULL AFTER `role`;

ALTER TABLE `user_master` ADD `preference_type` ENUM('Lead','No Lead') NOT NULL AFTER `cheque_size`;

ALTER TABLE `user_master` ADD `got_to_know` VARCHAR(100) NOT NULL AFTER `preference_type`;

ALTER TABLE `user_master` ADD `others_value` VARCHAR(100) NOT NULL AFTER `got_to_know`;

ALTER TABLE `page_banners` ADD `banner_text_font` VARCHAR(100) NOT NULL ;


CREATE TABLE email_settings (
id INT(6)  AUTO_INCREMENT PRIMARY KEY,
signup_email VARCHAR(50) ,
pledge_email VARCHAR(50),
activate_email VARCHAR(50),
);

INSERT INTO `equity_crest_dev`.`email_settings` (`id`, `signup_email`, `pledge_email`, `activate_email`) VALUES (NULL, 'on', 'on', 'on');

UPDATE `user_master` SET `slug_name` = replace(`company_name`,' ','.') where `role` != 'investor' ;

UPDATE `user_master` SET `slug_name` = replace(`name`,' ','.') where `role` = 'investor'; 




ALTER TABLE `user_master` 
ADD COLUMN `from_entrepreneur_pop` TINYINT(3) NULL DEFAULT 0 AFTER `activated_on`;


ALTER TABLE `user_master` 
CHANGE COLUMN `from_entrepreneur_pop` `from_entrepreneur_popup` TINYINT(3) NULL DEFAULT '0' ;


ALTER TABLE `user_master` 
CHANGE COLUMN `from_entrepreneur_popup` `entrepreneur` TINYINT(3) NULL DEFAULT '0' ;


ALTER TABLE `user_master` 
CHANGE COLUMN `entrepreneur` `pop_up_registration` TINYINT(3) NULL DEFAULT '0' ;
	

	