<?php

class investor_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function fetch_investor($userID) {
        $this->db->select('user_master.*,investor.*');
        $this->db->from('user_master');
        $this->db->join('investor', 'user_master.id = investor.user_id');
//        $this->db->join('sectors', 'investee.sector = sectors.id');
//        $this->db->join('startup_stage', 'investee.stage = startup_stage.id');
//        $this->db->join('business_types', 'investee.business_type = business_types.id');
        $this->db->where('user_master.id', $userID);
        $investor = $this->db->get()->row_array();
        return $investor;
    }

    public function add_preferred_sectors($data) {
        $this->db->insert_batch('preferred_sectors', $data);
    }

    public function get_preferred_sectors($userId) {
        $sectors = $this->db->get_where('preferred_sectors', array('user_id' => $userId))->result_array();
        return $sectors;
    }

    public function delete_preferred_sectors($userId) {
        $this->db->where('user_id', $userId);
        $this->db->delete('preferred_sectors');
    }

    public function add_preferred_investment_size($data) {
        $this->db->insert_batch('preferred_investment_size', $data);
    }

    public function get_preferred_investment_size($userId) {
        $sizes = $this->db->get_where('preferred_investment_size', array('user_id' => $userId))->result_array();
        return $sizes;
    }

    public function delete_preferred_investment_size($userId) {
        $this->db->where('user_id', $userId);
        $this->db->delete('preferred_investment_size');
    }

    public function add_preferred_locations($data) {
        $this->db->insert_batch('preferred_locations', $data);
    }

    public function get_preferred_locations($userId) {
        $countries = $this->db->get_where('preferred_locations', array('user_id' => $userId))->result_array();
        return $countries;
    }

    public function delete_preferred_locations($userId) {
        $this->db->where('user_id', $userId);
        $this->db->delete('preferred_locations');
    }

    public function add_preferred_investment_stage($data) {
        $this->db->insert_batch('preferred_investment_stage', $data);
    }

    public function get_preferred_investment_stage($userId) {
        $stages = $this->db->get_where('preferred_investment_stage', array('user_id' => $userId))->result_array();
        return $stages;
    }

    public function delete_preferred_investment_stage($userId) {
        $this->db->where('user_id', $userId);
        $this->db->delete('preferred_investment_stage');
    }

    public function add_other_preferences($data) {
        $this->db->insert_batch('preferred_others', $data);
    }

    public function get_other_preferences($userId) {
        $others = $this->db->get_where('preferred_others', array('user_id' => $userId))->result_array();
        return $others;
    }

    public function delete_other_preferences($userId) {
        $this->db->where('user_id', $userId);
        $this->db->delete('preferred_others');
    }

    public function get_portfolios($userId) {
        $this->db->select('investor_portfolios.*,sectors.name as sector');
        $this->db->from('investor_portfolios');
        $this->db->join('sectors', 'investor_portfolios.sector_id = sectors.id');
        $this->db->where('investor_portfolios.user_id', $userId);

        $portfolios = $this->db->get()->result_array();
        return $portfolios;
    }

    public function has_pledged($userId) {
        $this->db->from('pledge');
        $this->db->where('investor_id', $userId);
        return $this->db->count_all_results();
    }

    public function has_interest($userId) {
        $this->db->from('interested');
        $this->db->where('investor_id', $userId);
        return $this->db->count_all_results();
    }

    function update_invested($id, $raise) {
        $this->db->set('invested', 'invested + ' . $raise, FALSE);
        $this->db->where('user_id', $id);
        $this->db->update('investor');
    }

    public function has_preferred_sectors($userId) {
        $this->db->from('preferred_sectors');
        $this->db->where('user_id', $userId);
        return $this->db->count_all_results();
    }

    public function get_pledged_investee_ids($userId) {
        $this->db->select('investee_id');
        $this->db->from('pledge');
        $this->db->where('investor_id', $userId);
        $investees = $this->db->get()->result_array();
        $investee_ids = array();
        foreach ($investees as $investee) {
            $investee_ids[] = $investee['investee_id'];
        }
        return $investee_ids;
    }

    public function get_pledged_investees($userId, $limit) {
        $this->db->select('user_master.*,investee.*,sectors.name as sector_name,startup_stage.name as stage_name,business_types.name as business_name');
        $this->db->from('pledge');
        $this->db->join('user_master', 'user_master.id = pledge.investee_id');
        $this->db->join('investee', 'user_master.id = investee.user_id');
        $this->db->join('sectors', 'investee.sector = sectors.id');
        $this->db->join('startup_stage', 'investee.stage = startup_stage.id');
        $this->db->join('business_types', 'investee.business_type = business_types.id');
        $this->db->where('pledge.investor_id', $userId);
        $this->db->limit($limit);

        $investees = $this->db->get()->result_array();
        return $investees;
    }

    public function get_followed_investee_ids($userId) {
        $this->db->select('follow_id');
        $this->db->from('follow');
        $this->db->where('user_id', $userId);
        $investees = $this->db->get()->result_array();
        $investee_ids = array();
        foreach ($investees as $investee) {
            $investee_ids[] = $investee['follow_id'];
        }
        return $investee_ids;
    }

    public function get_followed_investees($userId, $limit) {
        $this->db->select('user_master.*,investee.*,sectors.name as sector_name,startup_stage.name as stage_name,business_types.name as business_name');
        $this->db->from('follow');
        $this->db->join('user_master', 'user_master.id = follow.follow_id');
        $this->db->join('investee', 'user_master.id = investee.user_id');
        $this->db->join('sectors', 'investee.sector = sectors.id');
        $this->db->join('startup_stage', 'investee.stage = startup_stage.id');
        $this->db->join('business_types', 'investee.business_type = business_types.id');
        $this->db->where('follow.user_id', $userId);
        $this->db->limit($limit);

        $investees = $this->db->get()->result_array();
        return $investees;
    }

    public function add_question($data) {
        $this->db->insert('questions', $data);
    }

    public function get_preferred_investees($preferred_options, $limit) {
        $this->db->select('user_master.*,investee.*,sectors.name as sector_name,startup_stage.name as stage_name');
        $this->db->from('user_master');
        $this->db->join('investee', 'user_master.id = investee.user_id');
        $this->db->join('sectors', 'investee.sector = sectors.id');
        $this->db->join('startup_stage', 'investee.stage = startup_stage.id');
        $this->db->where('user_master.status', 'active');
        if (isset($preferred_options['sectors'])) {
            $this->db->where_in('investee.sector', $preferred_options['sectors']);
        }
        if (isset($preferred_options['stages'])) {
            $this->db->where_in('investee.stage', $preferred_options['stages']);
        }
        if (isset($preferred_options['locations'])) {
            $locations = $preferred_options['locations'];
            if (in_array(6, $locations)) {
                $this->db->like('user_master.city', '');
            }
            if (in_array(5, $locations)) {
                $this->db->like('user_master.city', 'Hyderabad');
            }
            if (in_array(4, $locations)) {
                $this->db->like('user_master.city', 'Bengaluru');
            }
            if (in_array(3, $locations)) {
                $this->db->like('user_master.city', 'Chennai');
            }
            if (in_array(2, $locations)) {
                $this->db->like('user_master.city', 'Delhi');
            }
            if (in_array(1, $locations)) {
                $this->db->like('user_master.city', 'Mumbai');
            }
        }
        if (isset($preferred_options['sizes'])) {
            $sizes = $preferred_options['sizes'];
            if (!in_array(6, $sizes)) {

                if (in_array(5, $sizes)) {
                    $this->db->where('investment_required >=', 50000000);
                } elseif (in_array(4, $sizes)) {
                    $this->db->where('investment_required >=', 10000000);
                    $this->db->where('investment_required <=', 50000000);
                } elseif (in_array(3, $sizes)) {
                    $this->db->where('investment_required >=', 5000000);
                    $this->db->where('investment_required <=', 10000000);
                } elseif (in_array(2, $sizes)) {
                    $this->db->where('investment_required >=', 1000000);
                    $this->db->where('investment_required <=', 5000000);
                } elseif (in_array(1, $sizes)) {
                    $this->db->where('investment_required <=', 1000000);
                }
            }
        }

        $this->db->limit($limit);
        $investees = $this->db->get()->result_array();
        //echo $this->db->last_query();
        return $investees;
    }

    public function add_meeting($data) {
        $this->db->insert('meetings', $data);
        return $this->db->insert_id();
    }

    function get_all_offers() {
        $this->db->select('offers.*,sectors.name as sector_name,user_master.company_name as company_name');
        $this->db->from('offers', 'sectors', 'investee');
        $this->db->join('sectors', 'offers.sector = sectors.id');
        $this->db->join('user_master', 'offers.company_id = user_master.id');
        
        $offers = $this->db->get()->result_array();
        return $offers;
    }

    function get_offers($id) {
        $this->db->select('offers.*,sectors.name as sector_name,user_master.company_name as company_name');
        $this->db->from('offers', 'sectors', 'investee');
        $this->db->join('sectors', 'offers.sector = sectors.id');
        $this->db->join('user_master', 'offers.investor_id = user_master.id');
        $this->db->where('user_master.id', $id);
        $offers = $this->db->get()->result_array();
        return $offers;
    }

    function get_all_companies() {
        $this->db->select('user_master.id as id,user_master.company_name as company_name,sectors.id as sector_id,sectors.name as sector_name');
        $this->db->from('user_master');
        $this->db->join('investee', 'user_master.id = investee.user_id');
        $this->db->join('sectors', 'investee.sector = sectors.id');

        $this->db->where('user_master.status', 'active');
        $this->db->order_by("user_master.company_name", "asc");

        $investees = $this->db->get()->result_array();
        return $investees;
    }

    function add_offer($insertdata) {
        $this->db->insert('offers', $insertdata);
        return $this->db->insert_id();
    }
    
    public function add_bid($data) {
        $this->db->insert('bids', $data);
        return $this->db->insert_id();
    }
    
        function get_all_bids() {
        $this->db->select('bids.*,sectors.name as sector_name,user_master.company_name as company_name');
        $this->db->from('bids', 'sectors', 'investee');
        $this->db->join('sectors', 'bids.sector = sectors.id');
        $this->db->join('user_master', 'bids.company_id = user_master.id');
        
        $bids = $this->db->get()->result_array();
        return $bids;
    }

    function get_bids($id) {
        $this->db->select('bids.*,sectors.name as sector_name,user_master.company_name as company_name');
        $this->db->from('bids', 'sectors', 'investee');
        $this->db->join('sectors', 'bids.sector = sectors.id');
        $this->db->join('user_master', 'bids.investor_id = user_master.id');
        $this->db->where('user_master.id', $id);
        $bids = $this->db->get()->result_array();
        return $bids;
    }

    public function add_interested($data) {
        $this->db->insert('interested', $data);
        return $this->db->insert_id();
    }

}

?>