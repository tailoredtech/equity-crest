<?php

class investee_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function get_investors($limit) {
        $this->db->select('user_master.*,investor.*');
        $this->db->from('user_master');
        $this->db->join('investor', 'user_master.id = investor.user_id');
        $this->db->where('user_master.role', 'investor');
        $this->db->order_by("user_master.id", "RANDOM");
        $this->db->limit($limit);
        $investors = $this->db->get()->result_array();
        return $investors;
    }

    function get_pledged_investors($userID, $limit) {
        $this->db->select('user_master.*,investor.*');
        $this->db->from('pledge');
        $this->db->join('user_master', 'pledge.investor_id = user_master.id ');
        $this->db->join('investor', 'user_master.id = investor.user_id');
        $this->db->where('pledge.investee_id', $userID);
        $this->db->where('user_master.status', 'active');
        $this->db->order_by("user_master.id", "RANDOM");
        $this->db->limit($limit);
        $investors = $this->db->get()->result_array();
        return $investors;
    }

    function get_pledged_investors_count($userID) {
        $this->db->from('pledge');
        $this->db->where('pledge.investee_id', $userID);
        $investors = $this->db->count_all_results();
        return $investors;
    }

    function get_following_investors($userID, $limit) {
        $this->db->select('user_master.*,investor.*');
        $this->db->from('follow');
        $this->db->join('user_master', 'follow.user_id = user_master.id ');
        $this->db->join('investor', 'user_master.id = investor.user_id');
        $this->db->where('follow.follow_id', $userID);
        $this->db->order_by("user_master.id", "RANDOM");
        $this->db->limit($limit);
        $investors = $this->db->get()->result_array();
        return $investors;
    }

    function get_following_investors_count($userID) {
        $this->db->from('follow');
        $this->db->where('follow.follow_id', $userID);
        $investors = $this->db->count_all_results();
        return $investors;
    }

    function fetch_investee($userID) {
        $this->db->select('user_master.*,investee.*,sectors.name as sector,sectors.id as sector_id,startup_stage.name as stage,business_types.name as business');
        $this->db->from('user_master');
        $this->db->join('investee', 'user_master.id = investee.user_id');
        $this->db->join('sectors', 'investee.sector = sectors.id');
        $this->db->join('startup_stage', 'investee.stage = startup_stage.id');
        $this->db->join('business_types', 'investee.business_type = business_types.id');
        $this->db->where('user_master.id', $userID);
        $investee = $this->db->get()->row_array();
        return $investee;
    }

    function get_similar_companies($sector_id, $userId) {
        $this->db->select('user_master.*,investee.*,sectors.name as sector_name,startup_stage.name as stage_name,business_types.name as business_name');
        $this->db->from('user_master');
        $this->db->join('investee', 'user_master.id = investee.user_id');
        $this->db->join('sectors', 'investee.sector = sectors.id');
        $this->db->join('startup_stage', 'investee.stage = startup_stage.id');
        $this->db->join('business_types', 'investee.business_type = business_types.id');
        $this->db->where('investee.sector', $sector_id);
        $this->db->where('user_master.id !=', $userId);
        $this->db->where('user_master.status', 'active');
        $this->db->order_by("user_master.id", "RANDOM");
        $this->db->limit(9);

        $investees = $this->db->get()->result_array();
        return $investees;
    }

    function get_similar_companies_count($sector_id, $userId) {
        $this->db->from('user_master');
        $this->db->join('investee', 'user_master.id = investee.user_id');
        $this->db->where('investee.sector', $sector_id);
        $this->db->where('user_master.id !=', $userId);
        $this->db->where('user_master.status', 'active');
        $investees = $this->db->count_all_results();
        return $investees;
    }

    function is_pledged($investorId, $investeeId) {
        $this->db->from('pledge');
        $this->db->where('investor_id', $investorId);
        $this->db->where('investee_id', $investeeId);
        return $this->db->count_all_results();
    }

    function pledge($insertData) {
        $this->db->insert('pledge', $insertData);
        return $this->db->insert_id();
    }

    function update_fund_raise($id, $raise) {
        $this->db->set('fund_raise', 'fund_raise + ' . $raise, FALSE);
        $this->db->where('user_id', $id);
        $this->db->update('investee');
    }

    function is_interested($investorId, $investeeId) {
        $this->db->from('interested');
        $this->db->where('investor_id', $investorId);
        $this->db->where('investee_id', $investeeId);
        return $this->db->count_all_results();
    }

    function show_interest($investorId, $investeeId) {
        $insertData = array('investor_id' => $investorId, 'investee_id' => $investeeId);
        $this->db->insert('interested', $insertData);
    }

    function get_privacy($investeeId) {
        $this->db->select('*');
        $this->db->from('privacy_settings');
        $this->db->where('investee_id', $investeeId);
        $privacy = $this->db->get()->row_array();
        return $privacy;
    }

    function add_all_privacy() {
        $this->db->select('id');
        $this->db->from('user_master');
        $this->db->where('role', 'investee');
        $investees = $this->db->get()->result_array();

        foreach ($investees as $investee) {
            $this->db->insert('privacy_settings', array('investee_id' => $investee['id']));
        }
    }

    function update_privacy($investeeId, $data) {
        $this->db->where('investee_id', $investeeId);
        $this->db->update('privacy_settings', $data);
    }

    function get_questions($investeeId) {
        $this->db->select('questions.*,questions.created_date as qn_created_date,answers.answer,answers.created_date as an_created_date,user_master.name');
        $this->db->from('questions');
        $this->db->join('user_master', 'questions.investor_id = user_master.id');
        $this->db->join('answers', 'questions.id = answers.question_id', 'left');
        $this->db->where('investee_id', $investeeId);
        $this->db->order_by("questions.id", "DESC");
        $questions = $this->db->get()->result_array();
        return $questions;
    }

    function get_question($id) {
        $question = $this->db->get_where('questions', array('id' => $id))->row_array();
        return $question;
    }

    function add_reply($data) {
        $this->db->insert('answers', $data);
    }

    function add_advice_request($data) {
        $this->db->insert('advice_requests', $data);
        return $this->db->insert_id();
    }

    function get_confirmed_meetings($userId) {
        $this->db->select('meetings.*');
        $this->db->from('meetings');
        $this->db->where('investee_id', $userId);
        $this->db->where('status', 'confirmed');
        $events = $this->db->get()->result_array();
        return $events;
    }

    function get_meeting_detail($id) {
        $this->db->select('meetings.*');
        $this->db->from('meetings');
        $this->db->where('id', $id);
        $event = $this->db->get()->row_array();
        return $event;
    }

    function update_meeting_request($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('meetings', $data);
    }

    function get_post_deal($investeeId) {
        $this->db->select('post_deals.*');
        $this->db->from('post_deals');
        $this->db->where('investee_id', $investeeId);
        $postDeal = $this->db->get()->row_array();
        return $postDeal;
    }

    function add_post_deal($data) {
        $this->db->insert('post_deals', $data);
    }

    function update_post_deal($investeeId, $data) {
        $this->db->where('investee_id', $investeeId);
        $this->db->update('post_deals', $data);
    }

    function add_post_deal_financial_updates($data) {
        $this->db->insert('post_deal_financial_updates', $data);
        return $this->db->insert_id();
    }

    function update_post_deal_financial_updates($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('post_deal_financial_updates', $data);
    }

    function get_post_deal_financial_updates($investeeId) {
        $this->db->select('post_deal_financial_updates.*');
        $this->db->from('post_deal_financial_updates');
        $this->db->where('investee_id', $investeeId);
        $financialUpdates = $this->db->get()->result_array();
        return $financialUpdates;
    }

    function add_post_deal_business_updates($data) {
        $this->db->insert('post_deal_business_updates', $data);
        return $this->db->insert_id();
    }

    function update_post_deal_business_updates($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('post_deal_business_updates', $data);
    }

    function get_post_deal_business_updates($investeeId) {
        $this->db->select('post_deal_business_updates.*');
        $this->db->from('post_deal_business_updates');
        $this->db->where('investee_id', $investeeId);
        $businessUpdates = $this->db->get()->result_array();
        return $businessUpdates;
    }

    function is_access_granted($sectionId, $investeeId, $userId) {
        $this->db->from('access_requests');
        $this->db->where('section_id', $sectionId);
        $this->db->where('request_by', $userId);
        $this->db->where('request_to', $investeeId);
        $this->db->where('status', 'granted');
        $granted = $this->db->count_all_results();
        return $granted;
    }

    function update_access_request($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('access_requests', $data);
    }

    function get_offers($id) {
        $this->db->select('offers.*,sectors.name as sector_name,user_master.company_name as company_name');
        $this->db->from('offers', 'sectors', 'investee');
        $this->db->join('sectors', 'offers.sector = sectors.id');
        $this->db->join('user_master', 'offers.company_id = user_master.id');
        $this->db->where('user_master.id', $id);
        $offers = $this->db->get()->result_array();
        return $offers;
    }

    function get_company_sector($userId) {
        $this->db->select('sectors.id,sectors.name');
        $this->db->from('investee', 'sectors');
        $this->db->join('sectors', 'sectors.id = investee.sector');
        $this->db->where('investee.user_id', $userId);
        $sector = $this->db->get()->row_array();
        return $sector;
    }

    function add_offer($insertdata) {
        $this->db->insert('offers', $insertdata);
        return $this->db->insert_id();
    }
    
    function get_stats($type){
        if($type == 'sector'){
            $this->db->select('COUNT(investee.id) as value,sectors.name as label');
            $this->db->from('investee');
            $this->db->join('sectors', 'sectors.id=investee.sector');
            $this->db->join('user_master','user_master.id=investee.user_id');
            $this->db->where('user_master.status','active');
            $this->db->group_by("investee.sector");
            $results = $this->db->get()->result_array();
        }elseif($type='location'){
            $this->db->select('COUNT(investee.id) as value,user_master.city as label');
            $this->db->from('investee');
            $this->db->join('user_master','user_master.id=investee.user_id');
            $this->db->where('user_master.status','active');
            $this->db->group_by("user_master.city");
            $results = $this->db->get()->result_array();
        }
        
        return $results;
    }
     
        function get_pledges($investeeId){
       $this->db->select('pledge.*,user_master.id as investor_id,user_master.name as investor_name');
       $this->db->from('pledge');
       $this->db->join('user_master', 'user_master.id=pledge.investor_id');
       $this->db->where('pledge.investee_id', $investeeId);
       //$this->db->order_by("pledge.id", "DESC");
       $pledges = $this->db->get()->result_array();
       
       return $pledges;
    }
}
?>