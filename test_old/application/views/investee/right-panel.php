<div class="col-sm-3">
    <div class="content-infographic">
        <div class="item" style="display: none">
            <span class="market-icon"></span>
            <div> Stage</div>
            <div class="lato-bold"><?php echo $investee['stage']; ?></div>
        </div>
        <div class="item">
            <span class="value-icon"></span>
            <div> Founding Year</div>
            <div class="lato-bold"><?php echo $investee['year']; ?></div>
        </div>
        <div class="item">
            <span class="competition-icon"></span>
            <div> Sector</div>
            <div class="lato-bold"><?php echo $investee['sector']; ?></div>
        </div>
        <div class="item">
            <span class="cost-icon"></span>
            <div> Business Type</div>
            <div class="lato-bold"><?php echo $investee['business']; ?></div>
        </div>
        <div class="item">
            <span class="fund-icon"></span>
            <div> Company URL</div>
            <div class="lato-bold"><?php echo anchor(prep_url($investee['company_url']),$investee['company_url'],'target="_blank"'); ?></div>
        </div>
        <div class="item">
            <span class="product-icon"></span>
            <div> Product URL</div>
            <div class="lato-bold"><?php echo anchor(prep_url($investee['product_url']),$investee['product_url'],'target="_blank"'); ?></div>
        </div>
    </div>
</div>
