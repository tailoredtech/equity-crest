<div class="main">
    <div class="container wrap">
        <div class="row">
            <h4 class="form-header  col-sm-8 "><?php echo $investee['company_name'] ?></h4>
            <div class="btn-group profile-btn-group col-sm-3">
               <a href="<?php echo base_url(); ?>investee/view/<?php echo $investee['user_id'];  ?>" class="btn btn-default">Pre Deal</a>
               <a href="<?php echo base_url(); ?>investee/post_deal_view/<?php echo $investee['user_id'];  ?>" class="btn btn-default active">Post Deal</a>
           </div>
        </div>
    <div class="content-box row">
            <div class="profile-basic col-sm-12">
                <div class="row">
                    <div class="company-logo">
                        <div class="company-image company-border">
                            <img src="<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id'] ?>/<?php echo $investee['image']; ?>" width="77" height="77" />
                            
                        </div>
                    </div>
                    <div class="company-title">
                        <div class="company-name">
                            <?php echo $investee['company_name']; ?>
                        </div>
                        <div class="company-location">
                            <?php echo $investee['city']; ?>
                        </div>
                        <div class="company-rating">
                            <?php echo $investee['sector']; ?>
                        </div>
                    </div>
                    <div class="company-investment">
                        <div class="company-border"> Fund Raise</div>
                        <div class="lato-bold company-border"><img style="height: 11px;margin-top: -2px;" src='<?php echo base_url(); ?>assets/images/rupee.png' /> <?php  echo format_money($investee['investment_required']); ?></div>
                    </div>
                    <div class="company-offered">
                        <div class="company-border"> Stage</div>
                        <div class="lato-bold company-border"><?php echo $investee['stage']; ?></div>
                    </div>
                    <div class="company-validity">
                        <div class="company-border"> Validity Period</div>
                        <div class="lato-bold company-border"><?php echo $investee['validity_period']; ?></div>
                    </div>
                    <div class="company-commit">
                        <div> Minimum Investment</div>
                        <div class="lato-bold"><img style="height: 11px;margin-top: -2px;" src='<?php echo base_url(); ?>assets/images/rupee.png' /> <?php echo format_money($investee['commitment_per_investor']); ?></div>
                    </div>
                </div>
            </div>


            <div class="profile-tabs col-sm-12">
                <div class="row">
                    <div class="col-sm-11 col-sm-offset-1">
                    <ul class="nav tablist" role="tablist">
                        <li>
                            <div class="tab-item tab-active" style="border-left: 1px solid #d1d0d0;">
                                <a href="#financial" role="tab" data-toggle="tab">Financial Updates</a>
                            </div>
                        </li>
                        <li>
                            <div class="tab-item">
                                <a href="#business" role="=tab" data-toggle="tab">Business Updates</a>
                            </div>
                        </li>
                        <li>
                            <div class="tab-item">
                                <a href="#achievement" role="=tab" data-toggle="tab">Achievements</a>
                            </div>
                        </li>
                        
                    </ul>
                        </div>
                </div>
            </div>

            <div class="tab-content">
                <div class="profile-content col-sm-12 tab-pane active" id="financial">
                    <div class="row">
                        <!-- LEFT COLUMN -->
                        <?php $this->load->view('investee/post-deal-view-left',$investee);?>
                        <!-- LEFT COLUMN END -->
                        <div class="col-sm-6">
                            <?php foreach($financialUpdates as $financial){ ?>
                            <div class="content-text">
                                <h5 class="lato-bold"><?php echo $financial['file_name']; ?></h5>
                                <p>
                                    File : <?php echo $financial['file']; ?>
                                </p>
                            </div>
                            <?php } ?>
                        </div>
                        <!-- RIGHT COLUMN -->
                        <?php $this->load->view('investee/right-panel',$investee);?>
                        <!-- RIGHT COLUMN END -->

                    </div>
                </div>
                
                <div class="profile-content col-sm-12 tab-pane" id="business">
                    <div class="row">
                        <!-- LEFT COLUMN -->
                        <?php $this->load->view('investee/post-deal-view-left',$investee);?>
                        <!-- LEFT COLUMN END -->
                        <div class="col-sm-6">
                            <?php foreach($businessUpdates as $business){ ?>
                            <div class="content-text">
                                <h5 class="lato-bold"><?php echo $business['meeting_update']  ?></h5>
                                <p>
                                    File : <?php echo $business['meeting_update_file']; ?>
                                </p>
                                <p>
                                    Message to Investor : <?php echo $business['message']; ?>
                                </p>
                            </div>
                            <?php } ?>
                        </div>
                        <!-- RIGHT COLUMN -->
                        <?php $this->load->view('investee/right-panel',$investee);?>
                        <!-- RIGHT COLUMN END -->

                    </div>
                </div>

                <!-- achievement tab -->
                
                <div class="profile-content col-sm-12 tab-pane" id="achievement">
                    <div class="row">
                        <!-- LEFT COLUMN -->
                        <?php $this->load->view('investee/post-deal-view-left',$investee);?>
                        <!-- LEFT COLUMN END -->
                        <div class="col-sm-6">
                            <div class="content-text">
                                <h5 class="lato-bold">Media</h5>
                                <p>
                                    <?php if($post_deal['media']){ 
                                        echo $post_deal['media']; ?>
                                    <?php }else{ ?>
                                        Not Available
                                    <?php } ?>
                                </p>

                            </div>
                            <div class="content-text">
                                <h5 class="lato-bold">Awards & Recognition</h5>
                                <p>
                                    <?php if($post_deal['awards']){ 
                                        echo $post_deal['awards']; ?>
                                    <?php }else{ ?>
                                        Not Available
                                    <?php } ?>
                                </p>

                            </div>

                            <div class="content-text">
                                <h5 class="lato-bold">Testimonials</h5>
                                <p>
                                    <?php if($post_deal['testimonials']){ 
                                        echo $post_deal['testimonials']; ?>
                                    <?php }else{ ?>
                                        Not Available
                                    <?php } ?>
                                </p>

                            </div>
                        </div>
                        <!-- RIGHT COLUMN -->
                        <?php $this->load->view('investee/right-panel',$investee);?>
                        <!-- RIGHT COLUMN END -->
                    </div>
                </div>
                <!-- end achievement tab -->
                
                <div class="profile-content col-sm-12 tab-pane" id="presentation">
                    <div class="row">
                        <!-- LEFT COLUMN -->
                        <?php $this->load->view('investee/post-deal-view-left',$investee);?>
                        <!-- LEFT COLUMN END -->
                        <div class="col-sm-8">
                            <div class="content-text presentation-area">
                                
                                <p>
                                    <iframe src="" width="550" height="350" frameborder="0" marginwidth="undefined" marginheight="undefined" scrolling="no" allowfullscreen> </iframe> 
                                </p>
                                <div id="slideshare-iframe" style="display: none;">
                                    <?php echo $post_deal['presentation']; ?>
                            </div>
                        </div>
                        </div>
                        <!-- RIGHT COLUMN -->
                        <?php //$this->load->view('investee/right-panel',$investee);?>
                        <!-- RIGHT COLUMN END -->
                    </div>
                </div>
                
                <div class="profile-content col-sm-12 tab-pane" id="video">
                    <div class="row">
                        <!-- LEFT COLUMN -->
                        <?php $this->load->view('investee/post-deal-view-left',$investee);?>
                        <!-- LEFT COLUMN END -->
                        <div class="col-sm-6">
                            <div class="content-text">
                                
                                <?php $video_key = end(explode("=", $post_deal['video_link'])); ?>
                                 <iframe style="margin: 11px 0px;" width="550" height="350" src="//youtube.com/embed/<?php echo $video_key;  ?>" frameborder="0" allowfullscreen></iframe>

                            </div>
                        </div>
                        <!-- RIGHT COLUMN -->
                        <?php //$this->load->view('investee/right-panel',$investee);?>
                        <!-- RIGHT COLUMN END -->
                    </div>
                </div>
            </div>                <!-- end of tabpanes -->


        </div>

    

</div>





</div>
<!-- POPUP CONTAINER -->
        <div style="display: none;">
            <div class="pop-content" id="ask-question">
                <h3 class="form-header">Ask Question</h3>

                <div class="askexpert">
                    <form id="ask-qn-form" method="POST">
                        <textarea name="question" placeholder="your query" cols="69" rows="10"></textarea>
                        <input type="hidden" name="investee_id" value="<?php echo $investee['user_id']; ?>" />
                        <div class="buttons">
                            <div class="btn btn-gray box-close">Cancel</div>
                            <input type="submit" value="Send" class="btn btn-green" />
                        </div>
                    </form>
                </div>

            </div>
            
            <div id="new-qn-txt">
              <div class="qn-ans">  
                <div class="qa-text qn">
                    <span class="q pull-left">Q.</span>
                    <p class="pull-right qun"></p>
                    <p class="pull-right small-txt">Added by <span><?php echo $this->session->userdata('name'); ?></span>, 26 Jan 2014, 12:00PM</p>
                </div>
              </div>    
            </div>
            
            <div class="pop-content" id="pledge-pop">
                <h3 class="form-header">Pledge</h3>
                <div class="askexpert">
                    <form id="form-pledge" method="post">
                        <div class="form-group">
                            <label for="founder-name" class="col-sm-2 form-label black lato-bold ">Amount</label>
                            <div class="col-sm-10">
                                <input type="text" name="amount" value="" class="form-control form-input"  placeholder="Ex. 1,00,000">
                                <input type="hidden" name="investee_id" value="" class="invste_id" />
                            </div>
                        </div>

                        <div class="buttons">
                            <input type="submit" value="Send" class="btn btn-green" />
                        </div>
                    </form>
                </div>

            </div>
            
            <div class="pop-content" id="schedule-meeting-pop">
                <h3 class="form-header">Schedule Meeting</h3>
                <div class="askexpert">
                    <form id="schedule-meeting-form" method="post">
                        <div class="form-group">
                            <label for="founder-name" class="col-sm-2 form-label black lato-bold ">Date</label>
                            <div class="col-sm-10">
                                <input type="text" name="meeting_time" id="meeting-time" value="" class="form-control form-input"  placeholder="">
                                <input type="hidden" name="investee_id" value="<?php echo $investee['user_id']; ?>"  />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="founder-name" class="col-sm-2 form-label black lato-bold ">Title</label>
                            <div class="col-sm-10">
                                <input type="text" name="title"  value="" class="form-control form-input"  placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="founder-name" class="col-sm-2 form-label black lato-bold ">Remark</label>
                            <div class="col-sm-10">
                                <textarea name="remark"  class="form-control form-input form-area"  placeholder=""></textarea>
                                
                            </div>
                        </div>
                        <div class="buttons">
                            <input type="submit" value="Send" class="btn btn-green" />
                        </div>
                    </form>
                </div>

            </div>
            
        </div>
<script>
    $(document).ready(function(){
      
        var embed_id = $('.presentation-area #slideshare-iframe iframe').attr('src');
        $('.presentation-area p iframe').attr('src',embed_id);
        
        var investeeId = '<?php echo $investee['user_id']; ?>';
        $('a[href="#presentation"]').on('click', function () {
            $('a[href="#presentation"] .sidelink').css('background','#a7e41e');
            $('a[href="#video"] .sidelink').css('background','#878686');
            
        });
        
        $('a[href="#video"]').on('click', function () {
            $('a[href="#video"] .sidelink').css('background','#a7e41e');
            $('a[href="#presentation"] .sidelink').css('background','#878686');
            
        });
       
        $('.nav a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $('a[href="#video"] .sidelink').css('background','#878686');
            $('a[href="#presentation"] .sidelink').css('background','#878686');
        });
     
        $(".tab-item").click(function() {
            $(".profile-tabs .tab-active").removeClass("tab-active");
            $(this).addClass("tab-active");
            $('a .sidelink').css('background', '#878686');
        });
     
    });
</script>