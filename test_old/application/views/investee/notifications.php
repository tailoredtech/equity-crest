<div class="main">
    <div class="container wrap">
        <div class="row">
            <h4 class="form-header  col-sm-12 ">Notifications</h4>

        </div>
        <div class="content-box row">
            <ul class="list-group">
                <?php foreach ($notifications as $notification) { ?>
                <li class="list-group-item">
                    <div class="notification">
                        <span class="notification-thumb img-thumbnail" style="background-image: url('<?php echo base_url(); ?>uploads/users/<?php echo $notification['sender_id'] ?>/<?php echo $notification['image']; ?>');  background-size: 100%; "></span> 
                        <span class="notification-text"><?php echo $notification['display_text']; ?></span>
                        <span class="notification-time"><?php echo date('M, d H:i A', strtotime($notification['date_created'])); ?></span>
                        <?php if($notification['type'] == 'access' || $notification['type'] == 'meeting'){ ?>
                        
                        <div class="notification-action-btns">
                            <div class="small-msg" style="display:none;"></div>
                            <?php if($notification['type'] == 'access'){ ?>
                                <span class="btn btn-primary access-accept" data-notification-id="<?php echo $notification['id']; ?>" data-record-id="<?php echo $notification['record_id']; ?>">Accept</span>
                                <span class="btn btn-default access-reject" data-notification-id="<?php echo $notification['id']; ?>" data-record-id="<?php echo $notification['record_id']; ?>">Reject</span>
                            <?php } ?>
                            
                            <?php if($notification['type'] == 'meeting'){ ?>
                               
                               <span class="btn btn-primary meeting-accept" data-notification-id="<?php echo $notification['id']; ?>" data-record-id="<?php echo $notification['record_id']; ?>">Accept</span>
                               <span class="btn btn-default meeting-reject" data-notification-id="<?php echo $notification['id']; ?>" data-record-id="<?php echo $notification['record_id']; ?>">Reject</span>
                               <a class="view-msg-link" data-record-id="<?php echo $notification['record_id']; ?>" href="javascript:void(0);">View Message</a>
                            <?php } ?>
                            
                        </div>
                        <?php } ?>
                    </div>
                </li>
              <?php } ?>
            </ul>
            
        </div>

    </div>
</div>
<div style="display: none;">
    <div class="pop-content" id="reject-remark">
        <h3 class="form-header">Reject Remark</h3>
        <div class="askexpert">
            <form id="reject-remark-form">
                <textarea name="reject_remark" placeholder="Reject Remark" cols="54" rows="8"></textarea>
                <input type="hidden" name="meeting_id" class="meeting-id" value="" />
                <input type="hidden" name="notification_id" class="notification-id" value="" />
                <div class="buttons">
                    <input class="btn btn-green" type="submit" value="send" />
                </div>
            </form>
        </div>

    </div>
</div>
<script>
    $(document).ready(function(){
        $('.access-accept').on('click',function(){
            var record_id = $(this).attr('data-record-id');
            var notification_id = $(this).attr('data-notification-id');
            var notification = $(this).parents('.list-group-item');
            $.ajax({
                url: "<?php echo base_url(); ?>investee/accept_access_request",
                type: 'POST',
                data : {recordId : record_id,notificationId : notification_id}
            })
            .done(function( data ) {
                 if( data == 'success'){
                    notification.remove();
                }else{
                    
                }
            });
        });
        
        $('.meeting-accept').on('click',function(){
            var record_id = $(this).attr('data-record-id');
            var notification_id = $(this).attr('data-notification-id');
            var notification = $(this).parents('.list-group-item');
            $.ajax({
                url: "<?php echo base_url(); ?>investee/accept_meeting_request",
                type: 'POST',
                data : {recordId : record_id,notificationId : notification_id}
            })
            .done(function( data ) {
                 if( data == 'success'){
                    notification.remove();
                }else{
                    
                }
            });
        });
        
        $('.access-reject').on('click',function(){
            var record_id = $(this).attr('data-record-id');
            var notification_id = $(this).attr('data-notification-id');
            var notification = $(this).parents('.list-group-item');
            $.ajax({
                url: "<?php echo base_url(); ?>investee/reject_access_request",
                type: 'POST',
                data : {recordId : record_id,notificationId : notification_id}
            })
            .done(function( data ) {
                 if( data == 'success'){
                    notification.remove();
                }else{
                    
                }
            });
        });
        
        $('.meeting-reject').on('click',function(){
            var record_id = $(this).attr('data-record-id');
            var notification_id = $(this).attr('data-notification-id');
            $('#reject-remark-form .meeting-id').val(record_id);
            $('#reject-remark-form .notification-id').val(notification_id);
            $.colorbox({inline: true, href: '#reject-remark', innerWidth: '450px', innerHeight: '320px'});
        });
        
        $("#reject-remark-form").on('submit', (function(e) {
                    e.preventDefault();
                    $.ajax({
                        url: "<?php echo base_url(); ?>investee/reject_meeting_request",
                        type: 'POST',
                        data: new FormData(this),
                        contentType: false,
                        cache: false,
                        processData: false
                    }).done(function() {
                        $.colorbox.close();
                        location.reload();
                    });

                }));
        
//        $('.meeting-reject').on('click',function(){
//            var record_id = $(this).attr('data-record-id');
//            var notification_id = $(this).attr('data-notification-id');
//            var notification = $(this).parents('.list-group-item');
//            $.ajax({
//                url: "<?php echo base_url(); ?>investee/reject_meeting_request",
//                type: 'POST',
//                data : {recordId : record_id,notificationId : notification_id}
//            })
//            .done(function( data ) {
//                 if( data == 'success'){
//                    notification.remove();
//                }else{
//                    
//                }
//            });
//        });
        
        $('.view-msg-link').on('click',function(){
            var record_id = $(this).attr('data-record-id');
            var notification = $(this).parents('.list-group-item');
            $.ajax({
                url: "<?php echo base_url(); ?>investor/meeting_msg",
                type: 'POST',
                dataType: 'html',
                data : {recordId : record_id}
            })
            .done(function( data ) {
                 if( data != ' '){
                     notification.find('.small-msg').toggle();
                    notification.find('.small-msg').html(data);
                }
            });
        });
    });
</script>    