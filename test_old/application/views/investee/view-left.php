<div class="col-sm-3 profile-sidenav">
    <div class="row">
        <div class="col-sm-10 col-sm-offset-2">
            <a href="#presentation" role="tab" data-toggle="tab"><div class="sidelink">Company Presentation</div></a>
            <a href="#video" role="tab" data-toggle="tab"><div class="sidelink">Proposal Video</div></a>
            <a href="#questions" role="tab" data-toggle="tab"><div class="sidelink" <?php echo ($active_tab == 'questions') ? 'style="background: rgb(167, 228, 30);"' : ''; ?>>Q & A</div></a>
            <?php if ($this->session->userdata('role') == 'investor') { ?>
            <a href="javascript:void(0);"  id="schedule-meeting-btn" ><div class="sidelink">Schedule Meeting</div></a>
            <?php } ?>
        </div>

    </div>
    <div class="row">
        <div class="col-sm-10 col-sm-offset-2">
            <?php if ($this->session->userdata('role') == 'investor') { ?>
                <div class="row space-20">
                    <div class="col-sm-12">
                        <div class="row">
                            <?php
                            $followed = $this->session->userdata('followed');
                            if (is_array($followed)) {
                                if (in_array($investee['user_id'], $followed)) {
                                    ?>
                                    <div class="col-sm-4  unfollow action-btn">Unfollow</div>
                                <?php } else { ?>
                                    <div class="col-sm-4  follow action-btn">Follow</div>
                                <?php } ?>
                            <?php } else { ?>
                                <div class="col-sm-4  follow action-btn">Follow</div>
                             <?php } ?>
                            <div class="col-sm-4 action-btn">Ignore</div>
                            
                            <?php
                            $pledged = $this->session->userdata('pledged');
                            if (is_array($pledged)) {
                                if (in_array($investee['user_id'], $pledged)) {
                                    ?>
                                    <div class="col-sm-4  action-btn">Pledged</div>
                                <?php } else { ?>
                                    <div class="col-sm-4  pledge action-btn">Pledge</div>
                                <?php } ?>
                            <?php } else { ?>
                                <div class="col-sm-4  pledge action-btn">Pledge</div>
                             <?php } ?>
                        </div>
                    </div>
                </div>
<?php } ?>
        </div>
    </div>
    <div class="row space-20">
        <div class="col-sm-7 col-sm-offset-3 social-ico">
            <a href="<?php echo $investee['linkedin_url']; ?>" class="social-link" id="ln">Linked-in</a>
            <a href="<?php echo $investee['twitter_handle']; ?>" class="social-link" id="tw">Twitter</a>
            <a href="<?php echo $investee['fb_url']; ?>" class="social-link" id="fb">Facebook</a>
        </div> 
    </div>
</div>
