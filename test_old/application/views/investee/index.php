<div class="main">
    <div class="container wrap">
        <div class="row">
            <h4 class="form-header col-sm-8"><?php echo $investee['company_name'] ?><a href="<?php echo base_url(); ?>user/investee_info" class="btn"><span class="glyphicon glyphicon-edit"></span>Edit Profile</a></h4>
            <div class="btn-group profile-btn-group col-sm-3">
<!--               <a href="<?php echo base_url(); ?>investee/index" class="btn btn-default active">Pre Deal</a>
               <a href="<?php echo base_url(); ?>investee/post_deal_index" class="btn btn-default">Post Deal</a>-->
           </div>
        </div>

        <div class="row content-box">
            <div class="col-sm-12">
                <div class="row dashboard-links">
                    <a href="<?php echo base_url(); ?>investee/pledged_investors">
                        <div class="col-sm-6">
                            <span class="interest-icon"></span>
                            <div class="pull-left">Pledged Investors</div>
                            <div class="pull-right"><span class="lato-bold">(<?php echo $pledged_count; ?>)</span></div>
                        </div>
                    </a>
                    <!--                <div class="col-sm-4">
                                        <span class="view-icon"></span>
                                        <div class="pull-left">Investors Viewed Your Proposal</div>
                                        <div class="pull-right"><span class="lato-bold">(20)</span></div>
                                        </div>-->
                    <a href="<?php echo base_url(); ?>investee/following_investors">
                        <div class="col-sm-6">
                            <span class="track-icon"></span>
                            <div class="pull-left">Investors Following The Company</div>
                            <div class="pull-right"><span class="lato-bold">(<?php echo $following_investors_count; ?>)</span></div>
                        </div>
                </div>
                </a>
            </div>
            <div class="col-sm-12">
                <div class="row dashboard-links">
                    <a href="<?php echo base_url(); ?>investee/investors_from_sectors">
                        <div class="col-sm-6">
                            <span class="investor-icon"></span>
                            <div class="pull-left">Investors in your Sector</div>
                            <div class="pull-right"><span class="lato-bold">(<?php echo $investors_from_sector_count; ?>)</span></div>
                        </div>
                    </a>
                    <!--                <div class="col-sm-4">
                                        <span class="profile-icon"></span>
                                        <div class="pull-left">Investors Profile</div>
                                        <div class="pull-right"><span class="lato-bold">(20)</span></div>
                                    </div>-->
                    <a href="<?php echo base_url(); ?>investee/similar_companies">
                        <div class="col-sm-6">
                            <span class="similar-icon"></span>
                            <div class="pull-left">Similar Companies</div>
                            <div class="pull-right"><span class="lato-bold">(<?php echo $similar_companies_count; ?>)</span></div>
                        </div>
                    </a>
                </div>
            </div>
        </div>



        <div class="content-box row">
            <div class="profile-basic col-sm-12">
                <div class="row">
                    <div class="company-logo">
                        <div class="company-image company-border">
                            <img src="<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id'] ?>/<?php echo $investee['image']; ?>" width="77" height="77" />
                            <span class="change-img">Change</span>
                        </div>
                    </div>
                    <div class="company-title">
                        <div class="company-name">
                            <?php echo $investee['company_name']; ?>
                        </div>
                        <div class="company-location">
                            <?php echo $investee['city']; ?>
                        </div>
                        <div class="company-rating">
                            <?php echo $investee['sector']; ?>
                        </div>
                    </div>
                    <div class="company-investment">
                        <div class="company-border"> Fund Raise</div>
                        <div class="lato-bold company-border"><img style="height: 11px;margin-top: -2px;" src='<?php echo base_url(); ?>assets/images/rupee.png' /> <?php echo format_money($investee['investment_required']); ?></div>
                    </div>
                    <div class="company-offered">
                        <div class="company-border"> Stage</div>
                        <div class="lato-bold company-border"><?php echo $investee['stage']; ?></div>
                    </div>
                    <div class="company-validity">
                        <div class="company-border"> Validity Period</div>
                        <div class="lato-bold company-border"><?php echo $investee['validity_period']; ?></div>
                    </div>
                    <div class="company-commit">
                        <div> Minimum Investment</div>
                        <div class="lato-bold"><img style="height: 11px;margin-top: -2px;" src='<?php echo base_url(); ?>assets/images/rupee.png' /> <?php echo format_money($investee['commitment_per_investor']); ?></div>
                    </div>
                </div>
            </div>


            <div class="profile-tabs col-sm-12">
                <div class="row">
                    <div class="col-sm-11 col-sm-offset-1">
                        <ul class="nav tablist" role="tablist">
                            <li>
                                <div class="tab-item <?php echo ($active_tab == 'team') ? 'tab-active' : ''; ?>" style="border-left: 1px solid #d1d0d0;">
                                    <a href="#team" role="tab" data-toggle="tab">Team</a>
                                </div>
                            </li>
                            <li>
                                <div class="tab-item">
                                    <a href="#product" role="=tab" data-toggle="tab">Business</a>
                                </div>
                            </li>
                            <li>
                                <div class="tab-item">
                                    <a href="#customer" role="=tab" data-toggle="tab" >Revenue Model</a>
                                </div>
                            </li>
                            <li>
                                <div class="tab-item">
                                    <a href="#market" role="=tab" data-toggle="tab">Market</a>
                                </div>
                            </li>
                            <li>
                                <div class="tab-item" >
                                    <a href="#funding" role="=tab" data-toggle="tab">Raise</a>
                                </div>
                            </li>
                            <li>
                                <div class="tab-item">
                                    <a href="#finance" role="=tab" data-toggle="tab">Financials</a>
                                </div>
                            </li>
                            <li>
                                <div class="tab-item">
                                    <a href="#achievement" role="=tab" data-toggle="tab">Achievements</a>
                                </div>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>

            <div class="tab-content">
                <div class="profile-content col-sm-12 tab-pane <?php echo ($active_tab == 'team') ? 'active' : ''; ?>" id="team">
                    <div class="row">
                        <!-- LEFT COLUMN -->
                        <?php $this->load->view('investee/profile-left', $investee); ?>
                        <!-- LEFT COLUMN END -->
                        <div class="col-sm-6">

                            <div class="content-text">
                                <h5 class="lato-bold">Team</h5>
                                <p>
                                    <?php echo nl2br($investee['team_summary']); ?>
                                </p>

                            </div>


                        </div>
                        <!-- RIGHT COLUMN -->
                        <?php $this->load->view('investee/right-panel', $investee); ?>
                        <!-- RIGHT COLUMN END -->

                    </div>
                </div>


                <div class="profile-content col-sm-12 tab-pane" id="product">
                    <div class="row">
                        <!-- LEFT COLUMN -->
                        <?php $this->load->view('investee/profile-left', $investee); ?>
                        <!-- LEFT COLUMN END -->
                        <div class="col-sm-6">

                            <div class="content-text">
                                <h5 class="lato-bold">Product/Service Description</h5>
                                <p>
                                    <?php echo nl2br($investee['products_services']); ?>
                                </p>

                            </div>
                            <div class="content-text">
                                <h5 class="lato-bold">Unique Selling Proposition</h5>
                                <p>
                                    <?php echo nl2br($investee['how_different']); ?>
                                </p>
                            </div>

                        </div>
                        <!-- RIGHT COLUMN -->
                        <?php $this->load->view('investee/right-panel', $investee); ?>
                        <!-- RIGHT COLUMN END -->
                    </div>
                </div>


                <div class="profile-content col-sm-12 tab-pane" id="customer">
                    <div class="row">
                        <!-- LEFT COLUMN -->
                        <?php $this->load->view('investee/profile-left', $investee); ?>
                        <!-- LEFT COLUMN END -->
                        <div class="col-sm-6">
                            <div class="content-text">
                                <h5 class="lato-bold">Revenue Model</h5>
                                <p>
                                    <?php echo nl2br($investee['how_we_make_money']); ?>
                                </p>
                            </div>
                            <div class="content-text">
                                <h5 class="lato-bold">Revenue Traction</h5>
                                <p>
                                    <?php echo nl2br($investee['customer_traction']); ?>
                                </p>

                            </div>



                        </div>
                        <!-- RIGHT COLUMN -->
                        <?php $this->load->view('investee/right-panel', $investee); ?>
                        <!-- RIGHT COLUMN END -->

                    </div>
                </div>

                <div class="profile-content col-sm-12 tab-pane" id="market">
                    <div class="row">
                        <!-- LEFT COLUMN -->
                        <?php $this->load->view('investee/profile-left', $investee); ?>
                        <!-- LEFT COLUMN END -->
                        <div class="col-sm-6">

                            <div class="content-text">
                                <h5 class="lato-bold">Total Addressable Market</h5>
                                <p>
                                    <?php if($investee['addressable_market']){ ?>
                                       <img class="rupee-icon" src='<?php echo base_url(); ?>assets/images/rupee.png' /> <?php echo format_money($investee['addressable_market']); ?>
                                    <?php }else{ ?>
                                        Not Available
                                    <?php } ?>
                                </p>

                            </div>

                            <div class="content-text">
                                <h5 class="lato-bold">Competition</h5>
                                <p>
                                    <?php echo nl2br($investee['competition']); ?>
                                </p>

                            </div>

                        </div>
                        <!-- RIGHT COLUMN -->
                        <?php $this->load->view('investee/right-panel', $investee); ?>
                        <!-- RIGHT COLUMN END -->

                    </div>
                </div>

                <div class="profile-content col-sm-12 tab-pane" id="funding">
                    <div class="row">
                        <!-- LEFT COLUMN -->
                        <?php $this->load->view('investee/profile-left', $investee); ?>
                        <!-- LEFT COLUMN END -->
                        <div class="col-sm-6">
                            <div class="content-text">
                                <h5 class="lato-bold">Raise</h5>
                                <p>
                                     <?php if($investee['equity_offered']){ ?>
                                    Equity Offered : <?php echo $investee['equity_offered']; ?>%
                                    <?php }else{ ?>
                                    Equity Offered : Not Available
                                    <?php } ?>
                                </p>
                            </div>
                            <div class="content-text">
                                <h5 class="lato-bold">Valuation</h5>
                                <?php if($investee['equity_offered']){ 
                                       $post_money_val = '';
                                       $pre_money_val = '';
                                       $invst_required = $investee['investment_required'];
                                        $eqty_offered  = $investee['equity_offered']/100;
                                        $post_money_val = $invst_required/$eqty_offered;
                                        $pre_money_val = $post_money_val - $invst_required; 
                                 ?>
                                        <p>
                                            Pre Money Valuation : <?php echo format_money($pre_money_val); ?>
                                        </p>
                                        <p>
                                            Post Money Valution : <?php echo format_money($post_money_val) ?>
                                        </p>
                                <?php }else { ?>
                                       <p>
                                            Pre Money Valuation :  Not Available
                                        </p>
                                        <p>
                                            Post Money Valution :  Not Available
                                        </p>
                                <?php } ?>

                            </div>
                            <div class="content-text">
                                <h5 class="lato-bold">Use of Funds</h5>
                                <table class="table  investee-table">
                                    <tr>
                                        <th>Purpose</th>
                                        <th>Amount</th>
                                    </tr>
                                    <?php
                                    $total = 0;
                                    foreach ($purposes as $purpose) {
                                        if ($purpose['purpose'] != '') {
                                            ?>
                                            <tr>
                                                <td><?php echo $purpose['purpose']; ?></td>
                                                <td><img class="rupee-icon" src="<?php echo base_url(); ?>assets/images/rupee.png"><?php echo format_money($purpose['amount']); ?></td>
                                            </tr>
        <?php $total += $purpose['amount'];
    }
} ?>
                                    <tr>
                                        <td>Total</td>
                                        <td><img class="rupee-icon" src="<?php echo base_url(); ?>assets/images/rupee.png"><?php echo format_money($total); ?></td>
                                    </tr>
                                </table>

                            </div>
                            <div class="content-text">
                                <h5 class="lato-bold">Prior Fund raise details</h5>
                                <p>
                                    <?php if($investee['funding_history']){ 
                                        echo $investee['funding_history']; ?>
                                    <?php }else{ ?>
                                        Not Available
                                    <?php } ?>
                                </p>
                            </div>
                        </div>
                        <!-- RIGHT COLUMN -->
<?php $this->load->view('investee/right-panel', $investee); ?>
                        <!-- RIGHT COLUMN END -->

                    </div>
                </div>

                <div class="profile-content col-sm-12 tab-pane" id="finance">
                    <div class="row">
                        <!-- LEFT COLUMN -->
<?php $this->load->view('investee/profile-left', $investee); ?>
                        <!-- LEFT COLUMN END -->
                        <div class="col-sm-6">
                            <div class="content-text">
                                <h5 class="lato-bold">Current Monthly Financial Indicators</h5>
                                <table class="table investee-table">
                                    <tr>
                                        <td>Revenues</td>
                                        <td>
                                             <?php if($investee['monthly_revenue']){ ?>
                                               <?php if($investee['monthly_revenue']){?><img class="rupee-icon" src="<?php echo base_url(); ?>assets/images/rupee.png"><?php echo format_money($investee['monthly_revenue']); } ?>
                                            <?php }else{ ?>
                                                Not Available
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Fixed Cost (OPEX)</td>
                                        <td>
                                            <?php if($investee['fixed_opex']){ ?>
                                                <?php if($investee['fixed_opex']){ ?><img class="rupee-icon" src="<?php echo base_url(); ?>assets/images/rupee.png"><?php echo format_money($investee['fixed_opex']); } ?>
                                            <?php }else{ ?>
                                                Not Available
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Cash Burn</td>
                                        <td>
                                            <?php if($investee['cash_burn']){ ?>
                                                <?php if($investee['cash_burn']){ ?><img class="rupee-icon" src="<?php echo base_url(); ?>assets/images/rupee.png"><?php echo format_money($investee['cash_burn']); } ?>
                                            <?php }else{ ?>
                                                Not Available
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Debt</td>
                                        <td>
                                            <?php if($investee['debt']){ ?>
                                                <?php if($investee['debt']){ ?><img class="rupee-icon" src="<?php echo base_url(); ?>assets/images/rupee.png"><?php echo format_money($investee['debt']); } ?>
                                            <?php }else{ ?>
                                                Not Available
                                            <?php } ?>
                                        </td>
                                    </tr>
                                </table>

                            </div>
                            <div class="content-text">
                                <h5 class="lato-bold">Financial Forecast</h5>
                                <p>
                                    <?php if($investee['financial_forecast']){ ?>
                                        <a href='<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id']; ?>/files/<?php echo $investee['financial_forecast']; ?>' target="_blank"><?php echo $investee['financial_forecast']; ?></a>
                                    <?php }else{ ?>
                                        Not Available
                                    <?php } ?>
                                    
                                </p>

                            </div>

                            <div class="content-text">
                                <h5 class="lato-bold">Financial Statements</h5>
                                <p>
                                     <?php if($investee['financial_statement']){ ?>
                                        <a href='<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id']; ?>/files/<?php echo $investee['financial_statement']; ?>' target="_blank"><?php echo $investee['financial_statement']; ?></a>
                                    <?php }else{ ?>
                                        Not Available
                                    <?php } ?>
                                    
                                </p>

                            </div>

                            <div class="content-text">
                                <h5 class="lato-bold">Others (if any)</h5>
                                <p>
                                    <?php if($investee['other']){ ?>
                                        <a href='<?php echo base_url(); ?>uploads/users/<?php echo $investee['user_id']; ?>/files/<?php echo $investee['other']; ?>' target="_blank"><?php echo $investee['other']; ?></a>
                                    <?php }else{ ?>
                                        Not Available
                                    <?php } ?>
                                    
                                </p>

                            </div>

                        </div>
                        <!-- RIGHT COLUMN -->
<?php $this->load->view('investee/right-panel', $investee); ?>
                        <!-- RIGHT COLUMN END -->
                    </div>
                </div>

                <!-- achievement tab -->

                <div class="profile-content col-sm-12 tab-pane" id="achievement">
                    <div class="row">
                        <!-- LEFT COLUMN -->
<?php $this->load->view('investee/profile-left', $investee); ?>
                        <!-- LEFT COLUMN END -->
                        <div class="col-sm-6">

                            <div class="content-text">
                                <h5 class="lato-bold">Media</h5>
                                <p>
                                    <?php if($investee['media']){ 
                                        echo $investee['media']; ?>
                                    <?php }else{ ?>
                                        Not Available
                                    <?php } ?>
                                </p>

                            </div>
                            <div class="content-text">
                                <h5 class="lato-bold">Awards & Recognition</h5>
                                <p>
                                     <?php if($investee['awards']){ 
                                        echo $investee['awards']; ?>
                                    <?php }else{ ?>
                                        Not Available
                                    <?php } ?>
                                </p>

                            </div>

                            <div class="content-text">
                                <h5 class="lato-bold">Testimonials</h5>
                                <p>
                                    <?php if($investee['testimonials']){ 
                                        echo $investee['testimonials']; ?>
                                    <?php }else{ ?>
                                        Not Available
                                    <?php } ?>
                                </p>

                            </div>



                        </div>
                        <!-- RIGHT COLUMN -->
                        <?php $this->load->view('investee/right-panel', $investee); ?>
                        <!-- RIGHT COLUMN END -->
                    </div>
                </div>
                <!-- end achievement tab -->

                <div class="profile-content col-sm-12 tab-pane" id="presentation">
                    <div class="row">
                        <!-- LEFT COLUMN -->
                                    <?php $this->load->view('investee/profile-left', $investee); ?>
                        <!-- LEFT COLUMN END -->
                        <div class="col-sm-8">
                            <div class="content-text presentation-area">

                                <p>
                                    <iframe src="" width="550" height="350" frameborder="0" marginwidth="undefined" marginheight="undefined" scrolling="no" allowfullscreen> </iframe> 
                                </p>
                                <div id="slideshare-iframe" style="display: none;">
<?php echo $investee['presentation']; ?>
                                </div>
                            </div>
                        </div>
                        <!-- RIGHT COLUMN -->
                        <?php //$this->load->view('investee/right-panel',$investee);?>
                        <!-- RIGHT COLUMN END -->
                    </div>
                </div>

                <div class="profile-content col-sm-12 tab-pane" id="video">
                    <div class="row">
                        <!-- LEFT COLUMN -->
<?php $this->load->view('investee/profile-left', $investee); ?>
                        <!-- LEFT COLUMN END -->
                        <div class="col-sm-6">
                            <div class="content-text video-area">

<?php $video_key = end(explode("=", $investee['video_link'])); ?>
                                <iframe style="margin: 11px 0px;" width="550" height="350" src="//youtube.com/embed/<?php echo $video_key; ?>" frameborder="0" allowfullscreen></iframe>

                            </div>
                        </div>
                        <!-- RIGHT COLUMN -->
<?php //$this->load->view('investee/right-panel',$investee); ?>
                        <!-- RIGHT COLUMN END -->
                    </div>
                </div>
                <div  class="profile-content col-sm-12 tab-pane <?php echo ($active_tab == 'questions') ? 'active' : ''; ?>"  id="questions" >
                    <div class="row">
                        <!-- LEFT COLUMN -->
                                <?php $this->load->view('investee/profile-left', $investee); ?>
                        <!-- LEFT COLUMN END -->
                        <div class="col-sm-6">
                            <div class="content-text">
                                <div class="questions-head item-border">
                                    <h5 class="lato-bold pull-left">Questions</h5>

                                </div>
                                            <?php foreach ($questions as $question) { ?>
                                    <div class="qn-ans" id="<?php echo $question['id']; ?>">
                                        <div class="qa-text qn">
                                            <span class="q pull-left">Q.</span>
                                            <p class="pull-right"><?php echo $question['question']; ?></p>
                                            <p class="pull-right small-txt">Asked by <span><?php echo $question['name']; ?></span>, <?php echo date("j M Y, g:i A", strtotime($question['qn_created_date'])) ?> 

    <?php if ($question['answer'] == '') { ?>
                                                    <span class="reply-btn" style="float:right;" data-qn="<?php echo $question['id']; ?>">Reply</span>
                                        <?php } ?>
                                            </p>
                                        </div>
                                    <?php if ($question['answer']) { ?>
                                            <div class="qa-text ans">
                                                <span class="a pull-left">A.</span>
                                                <p class="pull-right"><?php echo $question['answer']; ?></p>
                                                <p class="pull-right small-txt">By <span><?php echo $investee['company_name']; ?></span>, <?php echo date("j M Y, g:i A", strtotime($question['an_created_date'])) ?></p>
                                            </div>
    <?php } ?>
                                    </div>
<?php } ?>

                            </div>
                        </div>
                        <!-- RIGHT COLUMN -->
<?php $this->load->view('investee/right-panel', $investee); ?>
                        <!-- RIGHT COLUMN END -->
                    </div>
                </div>

            </div>                <!-- end of tabpanes -->


        </div>



    </div>





</div> 
<div style="display: none">
    <div id="new-ans-txt">
        <div class="qa-text ans">
            <span class="a pull-left">A.</span>
            <p class="pull-right an"></p>
            <p class="pull-right small-txt">By <span><?php echo $investee['company_name'] ?></span>, 26 Jan 2014, 12:00PM</p>
        </div>
    </div>

    <div class="pop-content" id="reply-question">
        <h3 class="form-header">Reply</h3>

        <div class="askexpert">
            <form id="reply-qn-form" method="POST">
                <textarea name="answer" class="answer" placeholder="Your reply" cols="69" rows="10"></textarea>
                <input type="hidden" class="q-id" name="question_id" value="" />
                <div class="buttons">
                    <div class="btn btn-gray box-close">Cancel</div>
                    <input type="submit" value="Send" class="btn btn-green" />
                </div>
            </form>
        </div>

    </div>

    <div class="pop-content" id="ch-img">
        <h3 class="form-header">Upload Image</h3>

        <div class="askexpert">
            <form id="upload-image" action="<?php echo base_url(); ?>user/ajax_upload_image" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="col-sm-1 form-label">Image</label>
                    <div class="col-sm-11">
                        <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput">
                            <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                            <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="userfile"></span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>
                </div>
                <div class="buttons">
                    <input type="submit" class="btn btn-green" />
                </div>
            </form>
        </div>

    </div>
</div>
<script>
    $(document).ready(function() {

        var embed_id = $('.presentation-area #slideshare-iframe iframe').attr('src');
        $('.presentation-area p iframe').attr('src', embed_id);

        var investeeId = '<?php echo $investee['user_id']; ?>';
        $('a[href="#presentation"]').on('click', function() {
            $('a[href="#presentation"] .sidelink').css('background', '#a7e41e');
            $('a[href="#video"] .sidelink').css('background', '#878686');
            $('a[href="#questions"] .sidelink').css('background', '#878686');
        })

        $('a[href="#video"]').on('click', function() {
            $('a[href="#video"] .sidelink').css('background', '#a7e41e');
            $('a[href="#presentation"] .sidelink').css('background', '#878686');
            $('a[href="#questions"] .sidelink').css('background', '#878686');
        })

        $('a[href="#questions"]').on('click', function() {
            $('a[href="#questions"] .sidelink').css('background', '#a7e41e');
            $('a[href="#presentation"] .sidelink').css('background', '#878686');
            $('a[href="#video"] .sidelink').css('background', '#878686');
        })

        $('.nav a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
            $('a[href="#video"] .sidelink').css('background', '#878686');
            $('a[href="#presentation"] .sidelink').css('background', '#878686');
        })

        $('.reply-btn').on('click', function() {
            $('#reply-question .q-id').val($(this).attr('data-qn'));
            $.colorbox({inline: true, href: '#reply-question', innerWidth: '550px', innerHeight: '360px'});
        });

        $("#reply-qn-form").on('submit', (function(e) {
            e.preventDefault();
            var ans = $(this).find('.answer').val();
            var q_id = $(this).find('.q-id').val();
            var current = $('#' + q_id);

            $.ajax({
                url: "<?php echo base_url(); ?>investee/submit_reply",
                type: 'POST',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false
            })
                    .done(function() {
                $('#new-ans-txt .an').text(ans);
                current.append($('#new-ans-txt').html());
                $.colorbox.close();
            });

        }));

        $(".change-img").colorbox({inline: true, href: '#ch-img', innerWidth: '400px', innerHeight: '175px'});
        $("#upload-image").on('submit', (function(e) {
            e.preventDefault();
            $.ajax({
                url: "<?php echo base_url(); ?>user/ajax_image_upload",
                type: "POST",
                data: new FormData(this),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                success: function(data) {
                    $('.company-image img').attr('src', '<?php echo base_url(); ?>' + data.img);
                    $.colorbox.close()
                },
                error: function() {

                }
            });
        }));

        $(".tab-item").click(function() {
            $(".profile-tabs .tab-active").removeClass("tab-active");
            $(this).addClass("tab-active");
            $('a .sidelink').css('background', '#878686');
        });
    });

</script>
