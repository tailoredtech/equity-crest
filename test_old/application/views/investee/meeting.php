<div class="main">
    <div class="container">
        <div class="row">
            <h4 class="form-header  col-xs-12 ">Meeting Scheduler</h4>
        </div>
        <div class="content-box">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <div class="page-header">

                        <div class="pull-right form-inline">
                            <div class="btn-group">
                                <button class="btn btn-primary" data-calendar-nav="prev"><< Prev</button>
                                <button class="btn btn-default" data-calendar-nav="today">Today</button>
                                <button class="btn btn-primary" data-calendar-nav="next">Next >></button>
                            </div>
                            <div class="btn-group">
                                <button class="btn btn-warning" data-calendar-view="year">Year</button>
                                <button class="btn btn-warning active" data-calendar-view="month">Month</button>
                                <button class="btn btn-warning" data-calendar-view="week">Week</button>
                                <button class="btn btn-warning" data-calendar-view="day">Day</button>
                            </div>
                        </div>

                        <h3></h3>    
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div id="calendar"></div>
                        </div>
                        <!--        <div>
                                    <h4>Events</h4>
                                    <small>This list is populated with events dynamically</small>
                                    <ul id="eventlist" class="nav nav-list"></ul>
                                </div>-->
                    </div>

                    <div class="modal fade" id="events-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h3 class="modal-title">Meeting</h3>
                                </div>
                                <div class="modal-body">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div>
</div>
<?= js('underscore-min.js') ?>
<?= js('jstz.js') ?>
<?= js('calendar.js') ?>
<? //=js('app.js') ?>
<script type="text/javascript">
    var calendar = $("#calendar").calendar(
            {
                tmpl_path: "<?php echo base_url(); ?>assets/tmpls/",
                events_source: "<?php echo base_url(); ?>investee/events",
                tmpl_cache: false,
                day: '<?php echo date('Y-m-d') ?>',
                onAfterEventsLoad: function(events) {
                    if (!events) {
                        return;
                    }
                    var list = $('#eventlist');
                    list.html('');

                    $.each(events, function(key, val) {
                        $(document.createElement('li'))
                                .html('<a href="' + val.url + '">' + val.title + '</a>')
                                .appendTo(list);
                    });
                },
                onAfterViewLoad: function(view) {
                    $('.page-header h3').text(this.getTitle());
                    $('.btn-group button').removeClass('active');
                    $('button[data-calendar-view="' + view + '"]').addClass('active');
                },
                classes: {
                    months: {
                        general: 'label'
                    }
                }
            });
    $('.btn-group button[data-calendar-nav]').each(function() {
        var $this = $(this);
        $this.click(function() {
            calendar.navigate($this.data('calendar-nav'));
        });
    });

    $('.btn-group button[data-calendar-view]').each(function() {
        var $this = $(this);
        $this.click(function() {
            calendar.view($this.data('calendar-view'));
        });
    });

    $('#first_day').change(function() {
        var value = $(this).val();
        value = value.length ? parseInt(value) : null;
        calendar.setOptions({first_day: value});
        calendar.view();
    });

    $('#language').change(function() {
        calendar.setLanguage($(this).val());
        calendar.view();
    });


    calendar.setOptions({modal: "#events-modal"});



</script>