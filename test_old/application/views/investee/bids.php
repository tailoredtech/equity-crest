<div class="main">
    <div class="container wrap">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="form-header">Bids & Offers</h4>
            </div>
        </div>
    </div>
    <div class="container wrap">
        <div class=" row content-box">
            <div class="row">
                <div class="col-sm-12">
                    <a href="<?php echo base_url()."user/post_offer"?>" class="btn join-btn pull-right">Post Offer</a>
                </div>
            </div>    
            <div class="row">
            <div class="col-sm-12">    
                <div class="row terms-margin">
                    <table class="table table-striped table-bordered bids-font">
                        <tbody>
                        <tr>
                            <td>Sr. No</td>
                            <td>Date posted</td>
                            <td>Company Name</td>
                            <td>Sector</td>
                            <td>No of <br>shares</td>
                            <td>Holdings</td>
                            <td>Amt per <br>share</td>
                            <td>Total <br>Amount</td>
                            <td></td>
                        </tr>
                        </tbody>
                        <?php foreach ($bids as $bid) { ?>
                        <tr>
                            <td><?php echo $bid['id'] ?></td>
                            <td><?php echo date('M, d H:i A',strtotime($bid['date_created'])); ?></td>
                            <td><?php echo $bid['company_name'] ?></td>
                            <td><?php echo $bid['sector_name'] ?></td>
                            <td><?php echo $bid['shares'] ?></td>
                            <td><b><?php echo $bid['holdings'] ?></b>%</td>
                            <td><?php echo $bid['amount'] ?></td>
                            <td><b><?php echo $bid['total_amount'] ?></b></td>
                            <td><input type="submit" value="Interested" name="submit" class="join-btn col-sm-12 center lato-regular bids-interested"></td>
                         </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
                </div>
        </div>
    </div>
</div>

