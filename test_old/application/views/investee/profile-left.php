<div class="col-sm-3 profile-sidenav">
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <a href="#presentation" role="tab" data-toggle="tab"><div class="sidelink">Company Presentation</div></a>
            <a href="#video" role="tab" data-toggle="tab"><div class="sidelink">Proposal Video</div></a>
            <a href="#questions"  role="tab" data-toggle="tab"><div class="sidelink" <?php echo ($active_tab == 'questions') ? 'style="background: rgb(167, 228, 30);"' : ''; ?>>Q & A</div></a>
        </div>

    </div>
    <div class="row space-20">
        <div class="col-sm-7 col-sm-offset-3 social-ico">
                <a href="<?php echo $investee['linkedin_url']; ?>" class="social-link" id="ln">Linked-in</a>
                <a href="<?php echo $investee['twitter_handle']; ?>" class="social-link" id="tw">Twitter</a>
                <a href="<?php echo $investee['fb_url']; ?>" class="social-link" id="fb">Facebook</a>
        </div> 
    </div>
</div>
