<div class="main">
    <div class="container wrap">
        <div class="row">
            <div class="col-sm-7">
            <h4 class="form-header  ">Refer a Company</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="login-form">
                    <?php if($this->session->flashdata('success-msg')){ ?>
                    <div class="row failure-msg">
                        <div class="col-sm-10 col-md-offset-1">
                            <div class="alert alert-success" role="alert">
                                <?php echo $this->session->flashdata('success-msg'); ?>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if($this->session->flashdata('error-msg')){ ?>
                    <div class="row failure-msg">
                        <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-danger" role="alert">
                                <?php echo $this->session->flashdata('error-msg'); ?>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                  <form method="post" id="refer-form" action="<?php echo base_url(); ?>channel_partner/refer_process">
                    <div class=" col-sm-9 col-sm-offset-1 ">
                        <div class="row">
                            <div class="form-group">
                                <label for="sign-in-as" class="col-sm-3 form-label">Referral type</label>
                                <div class="col-sm-9">
                                    <div class="select-style">
                                        <select name="type" id="referral-type" class="form-control form-input">
                                            <option value="investor">Investor</option>
                                            <option value="investee">Startup</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div id="name-fields">
                            <div class="row">
                                <div class="form-group">
                                    <label  class="col-sm-3 form-label">Full Name *</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="name" class="form-control form-input"  placeholder="" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="form-group">
                                    <label for="company-name" class="col-sm-3 form-label">Company Name *</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="company_name" class="form-control form-input" id="company-name" placeholder="" required>
                                    </div>
                                </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label for="email" class="col-sm-3 form-label  ">Email ID *</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-input" id="email" name="email" placeholder="">
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 form-label">Mobile No *</label>
                                <div class="col-sm-9">
                                    <input type="text" name="mobileno" class="form-control form-input" id="mobile" placeholder="" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-11 col-sm-offset-2">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="agree"> I give my consent to Equity Crest directly contacting my referred Investor / Company
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="join-now col-sm-5 pull-left">
                                <input type="submit" value="Submit" name="submit" class="join-btn col-sm-12 pull-right lato-regular" style="margin-right: 0px">
                            </div>
                        </div>
                        
                        

                    </div>
                  </form>
                </div>
            </div>
        </div>
    </div>  

</div>
<!-- POPUP CONTAINER -->
<div style="display: none;">
    <div id="investor-name-fields">
        <div class="row">
            <div class="form-group">
                <label  class="col-sm-3 form-label">Full Name *</label>
                <div class="col-sm-9">
                    <input type="text" name="name" class="form-control form-input"  placeholder="" required>
                </div>
            </div>
        </div>
    </div>
    <div id="investee-name-fields">
        <div class="row">
            <div class="form-group">
                <label  class="col-sm-3 form-label">Founder Name *</label>
                <div class="col-sm-9">
                    <input type="text" name="name" class="form-control form-input"  placeholder="" required>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- FOOTER CONTAINER -->
<script type="text/javascript">
        $(document).ready(function(){
            $('#refer-form').validate({
                rules: {  
                            email: { required : true,
                                    email: true
                                },
                             agree:'required'
                        }
            });
            
            $('#refer-form select').on('change',function(){
                var referral = $(this).val();
                
                if(referral == 'investor'){
                     $('#name-fields').html($('#investor-name-fields').html());
                }else{
                     $('#name-fields').html($('#investee-name-fields').html());
                }
            });
        });
    </script>


        