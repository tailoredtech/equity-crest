<html>
    <!DOCTYPE html>
    <!-- PAGE HEAD -->
    <?php $this->load->view('front-end/head'); ?>
    <!-- PAGE HEAD END -->
    <body>
        <!-- HEADER CONTAINER ENDS-->
        <?php $this->load->view('front-end/header'); ?>
        <!-- HEADER CONTAINER ENDS-->

        <!-- PAGE BANNER -->
        <?php $this->load->view('front-end/top-bar-slider', $banners); ?>
        <!-- PAGE BANNER END -->


        <div class="main-page">
            <?= $content ?>
            <?php $this->load->view('front-end/panels'); ?>
        </div>  





        <!-- FOOTER CONTAINER -->
        <?php $this->load->view('front-end/footer'); ?>
        <!-- FOOTER CONTAINER ENDS -->
        <div class="modal fade" id="loginModal">
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Equity Crest</h4>
                </div>
                <div class="modal-body">
                    <p>Please sign-in or Join to view more details&hellip;</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <a href="<?php echo base_url(); ?>user/login" class="btn btn-primary">Sign in</a>
                    <a href="<?php echo base_url(); ?>user/register" class="btn btn-primary">Join</a>
                </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <script>
            $('document').ready(function() {
                if ($('#top-bar-slider').length) {
//                    bx_mode= $("#top-bar-slider").attr("data-slide-mode")||"fade";
//                    bx_controls= $("#top-bar-slider").attr("data-slide-controls")||false;
//                    bx_auto= $("#top-bar-slider").attr("data-slide-auto")||true;
//                    bx_pause= $("#top-bar-slider").attr("data-slide-pause")||5500;
//                    bx_pager= $("#top-bar-slider").attr("data-slide-pager")||false;
//                    bx_max= $("#top-bar-slider").attr("data-slide-max")||1;
//                    bx_min= $("#top-bar-slider").attr("data-slide-min")||1;
//                    bx_interval= $("#top-bar-slider").attr("data-slide-interval")||7000;
                    slider = $('#top-bar-slider').slick({
                        dots: true,
                        autoplay: true,
                        autoplaySpeed: 5000,
                        arrows: false
                    });
                }
                if ($('.deals-slider').length) {
//                    bx_mode= $(".deals-slider").attr("data-slide-mode")||"fade";
//                    bx_controls= $(".deals-slider").attr("data-slide-controls")||false;
//                    bx_auto= $(".deals-slider").attr("data-slide-auto")||true;
//                    bx_pause= $(".deals-slider").attr("data-slide-pause")||5500;
//                    bx_pager= $(".deals-slider").attr("data-slide-pager")||false;
//                    bx_max= $(".deals-slider").attr("data-slide-max")||1;
//                    bx_min= $(".deals-slider").attr("data-slide-min")||1;
//                    bx_interval= $(".deals-slider").attr("data-slide-interval")||7000;
                    slider = $('.deals-slider').slick({
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        dots: false,
                        autoplay: false,
                        autoplaySpeed: 5000,
                        arrows: true,
                        nextArrow: '<button type="button" class="slick-next"></button>',
                        responsive: [
                            {
                                breakpoint: 1170,
                                settings: {
                                    slidesToShow: 3,
                                    slidesToScroll: 3,
                                    infinite: true,
                                    dots: true
                                }
                            },
                            {
                                breakpoint: 1024,
                                settings: {
                                    slidesToShow: 3,
                                    slidesToScroll: 2
                                }
                            },
                            {
                                breakpoint: 768,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1
                                }
                            }
                        ]
                    });
                }
            });
        </script>
    </body>
</html>