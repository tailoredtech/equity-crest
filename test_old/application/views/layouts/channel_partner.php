<html>
    <head>
        <meta charset=utf-8 />
        <title>Equity Crest Partners</title>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <?= js('bootstrap.min.js') ?>
        <?= js('jasny-bootstrap.min.js') ?>
        <?= js('jquery.validate.min.js') ?>
        <?= js('slick.min.js') ?>
        <?= css('bootstrap.min.css') ?>
        <?= css('jasny-bootstrap.css') ?>
        <?= js('jquery.bxslider.min.js') ?>
        <?= js('jquery.colorbox.js') ?>
        <?= js('jHtmlArea-0.8.min.js') ?>
        <?= css('style.css') ?>
        <?= css('jquery.bxslider.css') ?>
        <?= css('jquery.slick.css') ?>
        <?= css('calendar.css') ?>
        <?= css('colorbox.css') ?>
        <?= css('jHtmlArea.css') ?>
        <link href="<?php echo base_url(); ?>assets/js/minimal/minimal.css" rel="stylesheet">

        

        <!--[if IE]>
                <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

    </head>

    <body>
        <!-- HEADER CONTAINER STARTS-->
        <?php $this->load->view('front-end/header'); ?>
        <!-- HEADER CONTAINER ENDS-->
        <?php $this->load->view('front-end/top-bar-slider', $banners); ?>        
        <div class="container-fluid investee-nav">
            <div class="container wrap">
                <div class="row ">
                    <?php if(!isset($active)) $active=0; ?>
                    <div  class="col-sm-2 investee-nav-link <?php if($active == 1){echo "link-active";}?>"><a href="<?php echo base_url(); ?>user/channel_partner_info">Profile </a></div>
                    <div  class="col-sm-3 investee-nav-link <?php if($active == 2){echo "link-active";}?>"><a href="<?php echo base_url(); ?>channel_partner/refer">Refer a Partner</a></div>
                    <div  class="col-sm-3 investee-nav-link <?php if($active == 3){echo "link-active";}?>"><a href="<?php echo base_url(); ?>home/portfolio_all">View Proposals</a></div>
                    
                    <div  class="col-sm-2 investee-nav-link <?php if($active == 4){echo "link-active";}?>"><a href="<?php echo base_url(); ?>home/investor_all">View Investors</a></div>
                    <div  class="col-sm-2 investee-nav-link"><div class="notification-box"><span class="notification-icon"></span><span class="notification-num" style="display:none"></span></div></div>
                </div>
            </div>
        </div>
        <?= $content; ?>
        <!-- POPUP CONTAINER -->
        <div style="display: none;">
            <div class="pop-content" id="advice">
                <h3 class="form-header">Ask for Expert Advice</h3>

                <div class="sendto">  To: Equity Crest</div>
                <div class="askexpert">
                    <form>
                        <textarea placeholder="your query" cols="54" rows="8"></textarea>
                        <div class="buttons">
                           
                            <button class="btn btn-green">Send</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
        <!-- FOOTER CONTAINER -->
        <?php $this->load->view('front-end/footer'); ?>
        <!-- FOOTER CONTAINER ENDS -->
        <script>
            $(document).ready(function() {
                $('.dropdown-toggle').dropdown();
                slider = $('#top-bar-slider').slick({
                    dots: true,
                    autoplay: true,
                    autoplaySpeed: 5000,
                    arrows: false
                });

            });
        </script>

    </body>
</html>