<html>
    <head>
        <meta charset=utf-8 />
        <title>EquityCrest Investee</title>

        <?= css('bootstrap.min.css') ?>
        <?= css('jasny-bootstrap.css') ?>
        <?= css('style.css') ?>
        <?= css('jquery.slick.css') ?>
        <?= css('calendar.css') ?>
        <?= css('colorbox.css') ?>
        <link href="<?php echo base_url(); ?>assets/js/minimal/minimal.css" rel="stylesheet">

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <?= js('bootstrap.min.js') ?>
        <?= js('jasny-bootstrap.min.js') ?>
        <?= js('slick.min.js') ?>
        <?= js('jquery.colorbox.js') ?>
        <?= js('jquery.validate.min.js') ?>
        <!--[if IE]>
                <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    </head>
    
    <body>
        <!-- HEADER CONTAINER ENDS-->
            <?php $this->load->view("front-end/header"); ?>
        <!-- HEADER CONTAINER ENDS-->
        
        <!-- PAGE BANNER -->
        <?php $this->load->view('front-end/top-bar-slider',$banners);?>
        <!-- PAGE BANNER END -->
        

        <div class="main">
            <?= $content ?>
        </div>  
                
            
        
        
        
        <!-- FOOTER CONTAINER -->
               <?php $this->load->view("front-end/footer"); ?>
        <!-- FOOTER CONTAINER ENDS -->
        <script>
            $('document').ready(function(){                        
                slider=$('#top-bar-slider').slick({
                        dots: true,
                        autoplay: true,
                        autoplaySpeed:5000,
                        arrows:false
                    });
            });
        </script>
        
    </body>
</html>