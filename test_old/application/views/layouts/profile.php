<html>
    <head>
        <meta charset=utf-8 />
        <title></title>
        
<!--        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>-->
        <?=js('jquery-1.11.0.js')?>
        <?=js('bootstrap.min.js')?>
        <?=js('jasny-bootstrap.min.js')?>
        <?=js('jquery.validate.min.js')?>
        <?=css('bootstrap.min.css')?>
        <?=css('jasny-bootstrap.css')?>
        <?=css('style.css')?>
        <link href="<?php echo base_url(); ?>assets/js/minimal/minimal.css" rel="stylesheet">
        
        <!--[if IE]>
                <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        
    </head>
    
    <body>
        <!-- HEADER CONTAINER ENDS-->
        <div class="container">
            <div class="row">
                <header class="col-lg-12 col-md-12 col-sm-12 col-xs-12 header">
                    <div class="logo col-lg-2 col-md-2 col-sm-2 col-xs-2">
                        <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/images/logo.png" class="header-logo"></a>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 pull-right">
                        <div class="top-links pull-right">
                            <?php if($this->session->userdata('user_id') != ''){ ?>
                              <div class="dropdown">
                                    <a id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="/page.html">
                                        <?php echo $this->session->userdata('name'); ?> <span class="caret"></span>
                                    </a>


                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                        <?php if($this->session->userdata('role') == 'investor'){ ?>
                                          <li role="presentation"> <a href="<?php echo base_url(); ?>user/investor_info">Update Profile</a></li>
                                          <li><a href="<?php echo base_url(); ?>investor/profile">View Profile</a></li>
                                          <li><a href="<?php echo base_url(); ?>investor/index">Dashboard</a></li>
                                        <?php }else{ ?>
                                          <li> <a href="<?php echo base_url(); ?>user/investee_info">Upload Business Proposal</a></li>
                                          <li><a href="<?php echo base_url(); ?>investee/profile">View Profile</a></li>
                                          <li><a href="<?php echo base_url(); ?>investee/index">Dashboard</a></li>
                                        <?php } ?>
                                        
                                        <li><a href="#">Account Settings</a></li>
                                        <li><a href="<?php echo base_url(); ?>user/logout">Sign Out</a></li>
                                    </ul>
                                </div>
                            <?php }else{ ?>
                              <a href="#">FAQs</a>
                            <?php } ?>
                        </div>
                        <div class="top-nav col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <nav>
                                <ul class="pull-right">
                                    <li class="pull-left">
                                        <a href="#" class=" text-center">
                                            <img src="<?php echo base_url(); ?>assets/images/home.png">
                                        </a>
                                    </li>

                                    <li class="pull-left">
                                        <a href="#" class=" text-center">
                                            INVEST
                                        </a>
                                    </li>
                                    <li class="pull-left">
                                        <a href="#" class=" text-center">
                                            RAISE
                                        </a>
                                    </li>
                                    <li class="pull-right" >
                                        <a href="#" class=" text-center">
                                            PORTFOLIO
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </header>
            </div>
        </div>
        <!-- HEADER CONTAINER ENDS-->
        
        <div class="container-fluid setup-steps ">
            <div class="row">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 step-1 text-center step">
                                Create Account<div class="step-complete"></div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 step-3 text-center step step-active">
                                Update Profile
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 step-4 text-center step">
                                View Profile
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

        <div class="main">
            <?= $content ?>
        </div>  
                
            
        
        
        
        <!-- FOOTER CONTAINER -->
                <div class="container-fluid footer-links">
            <div class="row">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <ul>
                                <li class="pull-left" >
                                    <a href="#" class="">Investment Opportunies</a>
                                </li>
                                <li class="pull-left" >
                                    <a href="#" class="">Raising Finance</a>
                                </li>
                                <li class="pull-left" >
                                    <a href="#" class="">Join Now</a>
                                </li>
                                <li class="pull-left" >
                                    <a href="#" class="">Why Us?</a>
                                </li>
                                <li class="pull-left" >
                                    <a href="<?php echo base_url(); ?>home/about_us" class="">About Us</a>
                                </li>
                                <li class="pull-left" >
                                    <a href="<?php echo base_url(); ?>home/news_top_stories" class="">News</a>
                                </li>
                                <li class="pull-left" >
                                    <a href="<?php echo base_url(); ?>home/events" class="">Events</a>
                                </li>
                                <li class="pull-left" >
                                    <a href="<?php echo base_url(); ?>home/contact_us" class="">Contact Us</a>
                                </li>
                                <li class="pull-left" >
                                    <a href="<?php echo base_url(); ?>home/faqs" class="">FAQ</a>
                                </li>
                            </ul>
                            <ul class="socia-links pull-right">
                                <li class="pull-left" >
                                    <a href="#"><img src="<?php echo base_url(); ?>assets/images/socia.png"></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        
        <div class="container-fluid footer-bottom">
            <div class="row">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <ul>
                                <li class="pull-left" >
                                    &copy; 2014 Equity Crest. All Rights Reserved.
                                </li>
                                <li class="pull-left" >
                                    <a href="#" class="">Privacy Policy</a>
                                </li>
                                <li class="pull-left" >
                                    <a href="#" class="">Terms of Use</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        <!-- FOOTER CONTAINER ENDS -->
        <script>
          $(document).ready(function(){
              $('.dropdown-toggle').dropdown()
          }); 
       </script>
        
    </body>
</html>