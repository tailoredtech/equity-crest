<html>
    <head>
        <meta charset=utf-8 />
        <title></title>
        
         <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <?= js('bootstrap.min.js') ?>
        <?= js('jasny-bootstrap.min.js') ?>
        <?= js('jquery.validate.min.js') ?>
        <?= css('bootstrap.min.css') ?>
        <?= css('jasny-bootstrap.css') ?>
        <?= css('style.css') ?>
        <link href="<?php echo base_url(); ?>assets/js/minimal/minimal.css" rel="stylesheet">
        <!--[if IE]>
                <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    </head>
    
    <body>
        <!-- HEADER CONTAINER ENDS-->

        <?php $this->load->view("front-end/header"); ?>
        <!-- HEADER CONTAINER ENDS-->
        
        
        <div class="main-page">
            <?= $content ?>
        </div>  
                
            
        
        
        
        <!-- FOOTER CONTAINER -->
                
            <?php $this->load->view("front-end/footer"); ?>
        <!-- FOOTER CONTAINER ENDS -->
        
        <script>
            $(document).ready(function() {
                $('.dropdown-toggle').dropdown();
            });
        </script>
    </body>
</html>