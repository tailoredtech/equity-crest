<html>
    <head>
        <meta charset=utf-8 />
        <title>Startups | Crowd-funding | Equity Cres</title>
<meta name="description=" content="If you have a promising business, you don’t need to worry about funding. Equity Crest has onboard both national and international investors who are looking for promising ventures to invest in. 
Equity Crest will make the connection and help you fulfill your funding needs.">
        <?= css('bootstrap.min.css') ?>
        <?= css('jasny-bootstrap.css') ?>
        <?= css('style.css') ?>
        <?= css('jquery.slick.css') ?>
        <?= css('calendar.css') ?>
        <?= css('colorbox.css') ?>
        <?= css('jHtmlArea.css') ?>
        <link href="<?php echo base_url(); ?>assets/js/minimal/minimal.css" rel="stylesheet">

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <?= js('bootstrap.min.js') ?>
        <?= js('jasny-bootstrap.min.js') ?>
        <?= js('slick.min.js') ?>
        <?= js('jquery.colorbox.js') ?>
        <?= js('jquery.validate.min.js') ?>
        <?= js('jHtmlArea-0.8.min.js') ?>
        <!--[if IE]>
                <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    </head>

    <body>
        <!-- HEADER CONTAINER-->
        <?php $this->load->view("front-end/header"); ?>
        <!-- HEADER CONTAINER ENDS-->

        <?php $this->load->view('front-end/top-bar-slider', $banners); ?>        

        <div class="container-fluid investee-nav">
            <div class="container wrap">
                <div class="row ">
                    <?php if (!isset($active)) $active = 0; ?>
                    <div  class="col-sm-2 investee-nav-link <?php
                    if ($active == 1) {
                        echo "link-active";
                    }
                    ?>"><a href="<?php echo base_url(); ?>investee/index">Profile </a></div>
                    <div  class="col-sm-3 investee-nav-link"><a href="javascript::void(0)" id="expert">Ask for Expert Advice</a></div>
                    <div  class="col-sm-3 investee-nav-link <?php
                    if ($active == 2) {
                        echo "link-active";
                    }
                    ?>"><a href="<?php echo base_url(); ?>investee/meeting"> Meeting Scheduler</a></div>

                    <div  class="col-sm-2 investee-nav-link <?php
                    if ($active == 3) {
                        echo "link-active";
                    }
                    ?>"><a href="<?php echo base_url(); ?>investee/offers"> Bids & Offers</a></div>
                    <div  class="col-sm-2 investee-nav-link">
                        <div class="notification-box dropdown">
                            <a  href="javascript:void(0);" class="notification-btn"><span class="notification-icon"></span><span class="notification-num" style="<?php echo ($unseen_notifications) ? 'visibility: visible;' : 'visibility: hidden;' ?>"><?php echo $unseen_notifications; ?></span></a>
                            <ul class="dropdown-menu notifications">
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?= $content; ?>
        <?php if ($this->uri->segment(2) != 'investee_info' && $this->uri->segment(2) != 'file_upload' && $this->uri->segment(2) != 'edit_file_upload' && $this->uri->segment(2) != 'portfolio_all') { ?>
            <div class="main">
                <?php $this->load->view('front-end/panels'); ?>
            </div>
        <?php } ?>
        <!-- POPUP CONTAINER -->
        <div style="display: none;">
            <div class="pop-content" id="advice">
                <h3 class="form-header">Ask for Expert Advice</h3>

                <div class="sendto">  To: Equity Crest</div>
                <div class="askexpert">
                    <form id="expert-advice-form">
                        <textarea name="query" placeholder="your query" cols="54" rows="8"></textarea>
                        <div class="buttons">
                            <input class="btn btn-green" type="submit" value="send" />
                        </div>
                    </form>
                </div>

            </div>
        </div>
        <!-- FOOTER CONTAINER -->
        <?php $this->load->view('front-end/footer'); ?>
        <!-- FOOTER CONTAINER ENDS -->
        <script>
            $(document).ready(function() {
                $('.dropdown-toggle').dropdown();

                slider = $('#top-bar-slider').slick({
                    dots: true,
                    autoplay: true,
                    autoplaySpeed: 5000,
                    arrows: false
                });

                $("#expert").colorbox({inline: true, href: '#advice', innerWidth: '450px', innerHeight: '335px'});

                $('.box-close').on('click', function() {
                    $.colorbox.close()
                });

                $("#expert-advice-form").on('submit', (function(e) {
                    e.preventDefault();
                    $.ajax({
                        url: "<?php echo base_url(); ?>investee/ask_advice",
                        type: 'POST',
                        data: new FormData(this),
                        contentType: false,
                        cache: false,
                        processData: false
                    }).done(function() {
                        $.colorbox.close();
                    });

                }));

                
                $('.notification-btn').on('click',function(){
                      $('.notification-num').css('visibility', 'hidden'); 
                      $('.notification-num').html('0');
                     $.ajax({
                        url: "<?php echo base_url(); ?>user/get_notifications"
                    }).done(function(html) {
                          if (html.trim() == "") {
                              data = "<li class='zero-notification'>no notifications yet</li>";
                                $('.notifications').css('display', 'block');
                                $('.notifications').html(data);
                            } else {
                                $('.notifications').css('display', 'block');
                                $('.notifications').html(html);
                            }                    
                    });  
 
                });
                $(document).click(function() {
        if ($('.notifications').is(':visible')) {
            $('.notifications').slideToggle('slow');
        }
    });
            });
        </script>

    </body>
</html>