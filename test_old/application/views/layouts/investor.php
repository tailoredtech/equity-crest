<html>
    <head>
        <meta charset=utf-8 />
        <title>Angel/VC Investors, Investment Opportunity, Accelerators, Incubators | Equity Crest</title>
        <meta name="description=" content="Investors get the opportunity to invest in high quality, curated deals, where they can be absolutely sure of their investments. The Equity Crest platform is synonymous with credibility, accountability, transparency, quality and top class expertise.
Equity Crest will keep you abreast with how things are moving within the startup, through regular updates and reports.">
        <?= css('bootstrap.min.css') ?>
        <?= css('jasny-bootstrap.css') ?>
        <?= css('style.css') ?>
        <?= css('jquery.slick.css') ?>
        <?= css('calendar.css') ?>
        <?= css('colorbox.css') ?>
        <?= css('jHtmlArea.css') ?>
        <link href="<?php echo base_url(); ?>assets/js/minimal/minimal.css" rel="stylesheet">

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <?= js('bootstrap.min.js') ?>
        <?= js('jasny-bootstrap.min.js') ?>
        <?= js('slick.min.js') ?>
        <?= js('jquery.colorbox.js') ?>
        <?= js('jquery.validate.min.js') ?>
        <?= js('jHtmlArea-0.8.min.js') ?>
        <!--[if IE]>
                <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

    </head>

    <body>
        <!-- HEADER CONTAINER -->
        <?php $this->load->view("front-end/header"); ?>
        <!-- HEADER CONTAINER ENDS-->
        <?php $this->load->view('front-end/top-bar-slider', $banners); ?>
        <div class="container-fluid investor-nav">
            <div class="row">
                <div class="container wrap">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="investor-navbar">
                                <?php if (!isset($active)) $active = 0; ?>
                                <li <?php
                                if ($active == 1) {
                                    echo "class='link-active'";
                                }
                                ?>><a href="<?php echo base_url(); ?>investor/my_investment" >My Investments</a></li>
                                <li <?php
                                if ($active == 2) {
                                    echo "class='link-active'";
                                }
                                ?>><a href="<?php echo base_url(); ?>investor/tracked" id="tracker" >My Tracker </a></li>
                                <li <?php
                                if ($active == 3) {
                                    echo "class='link-active'";
                                }
                                ?>><a href="<?php echo base_url(); ?>investor/preferred" id="pref" >Preferences</a></li>
                                <li <?php
                                if ($active == 4) {
                                    echo "class='link-active'";
                                }
                                ?>><a href="<?php echo base_url(); ?>investor/recommended">Recommended</a></li>
                                <li <?php
                                if ($active == 5) {
                                    echo "class='link-active'";
                                }
                                ?>><a href="<?php echo base_url(); ?>investor/profile">Profile</a></li>
                                <li <?php
                                if ($active == 6) {
                                    echo "class='link-active'";
                                }
                                ?>><a href="<?php echo base_url(); ?>investor/offers" >Offers</a></li>
                                <li <?php
                                if ($active == 7) {
                                    echo "class='link-active'";
                                }
                                ?>><a href="javascript:void(0);" ><span class="search-icon"></span></a></li>
                                <li>
                                    <div class="notification-box dropdown">
                                        <a  href="javascript:void(0);" class="notification-btn"><span class="notification-icon"></span><span class="notification-num" style="<?php echo ($unseen_notifications) ? 'visibility: visible;' : 'visibility: hidden;' ?>"><?php echo $unseen_notifications; ?></span></a>
                                        <ul class="dropdown-menu notifications">
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="container-fluid">
                    <div class="row search-divider"></div>
                </div>
                <div class="container wrap">
                    <div class="row search-bar" style="display: none">
                        <div class="col-sm-12">
                            <?php $this->load->view("front-end/investor_searchbar"); ?>

                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="main">
            <?= $content; ?>
            <?php if ($this->uri->segment(2) != 'investor_info') { ?>

                <?php $this->load->view('front-end/panels'); ?>

            <?php } ?>
        </div>
        <!-- POPUP CONTAINER -->
        <div style="display: none;">
            <div class="pop-content" id="pledge-pop">
                <h3 class="form-header">Pledge</h3>
                <div class="askexpert">
                    <form id="pledge-form" method="post">
                        <div class="form-group">
                            <label for="founder-name" class="col-sm-2 form-label black lato-bold ">Amount</label>
                            <div class="col-sm-10">
                                <input type="text" name="amount" value="" class="form-control form-input"  placeholder="Ex. 1,00,000">
                                <input type="hidden" name="investee_id" value="" class="invste_id" />
                            </div>
                        </div>

                        <div class="buttons">
                            <input type="submit" value="Send" class="btn btn-green" />
                        </div>
                    </form>
                </div>

            </div>
        </div>
        <!-- FOOTER CONTAINER -->
        <?php $this->load->view('front-end/footer'); ?>
        <!-- FOOTER CONTAINER ENDS -->
        <script>
            $(document).ready(function() {
                $('.dropdown-toggle').dropdown();

                //                if ($('.slider-markup').length) {
                //
                //                    $('.slider-markup').each(function() {
                //                        bx_mode = $(this).attr("data-slide-mode") || "fade";
                //                        bx_controls = $(this).attr("data-slide-controls") || true;
                //                        bx_auto = $(this).attr("data-slide-auto") || true;
                //                        bx_pause = $(this).attr("data-slide-pause") || 5500;
                //                        bx_pager = $(this).attr("data-slide-pager") || true;
                //                        bx_interval = $(this).attr("data-slide-interval") || 7000;
                ////                        console.log(mode);
                //                        slider = $(this).bxSlider({
                //                            mode: bx_mode,
                //                            controls: bx_controls,
                //                            auto: bx_auto,
                //                            pause: bx_pause,
                //                            pager: bx_pager,
                //                            interval: bx_interval
                //                        });
                //                    });
                //                }

                slider = $('#top-bar-slider').slick({
                    dots: true,
                    autoplay: true,
                    autoplaySpeed: 5000,
                    arrows: false
                });

                $('.tracker-panel').slideUp();
                $('#pref').click(function() {
                    $('.pref-panel').slideToggle();
                });

                $('.glyphicon-remove').click(function() {
                    $('.pref-panel').slideUp();
                });
                $('#tracker').click(function() {

                    $('.tracker-panel').slideToggle();
                });

                $('.deal-footer-links').on('click', '.follow', function() {
                    var invste_id = $(this).attr('data-user-id');
                    var this_var = $(this);
                    $.ajax({
                        url: "<?php echo base_url(); ?>user/follow",
                        type: 'POST',
                        data: {followId: invste_id}
                    })
                            .done(function(data) {
                        if (data == 'success') {
                            this_var.text('Following');
                            this_var.removeClass('follow')
                        } else {
                            console.log('error');
                        }
                    });
                });

                $('.deal-footer-links').on('click', '.unfollow', function() {
                    var invste_id = $(this).attr('data-user-id');
                    var this_var = $(this);
                    $.ajax({
                        url: "<?php echo base_url(); ?>user/unfollow",
                        type: 'POST',
                        data: {followId: invste_id}
                    })
                            .done(function(data) {
                        if (data == 'success') {
                            this_var.text('Follow');
                            this_var.removeClass('unfollow')
                        } else {
                            console.log('error');
                        }
                    });
                });

                $('.deal-footer-links').on('click', '.pledge', function() {
                    var invste_id = $(this).attr('data-user-id');
                    $("#pledge-form .invste_id").val(invste_id);
                    $(this).addClass('pledging');
                    $.colorbox({inline: true, href: '#pledge-pop', innerWidth: '400px', innerHeight: '175px'});
                });

                $("#pledge-form").on('submit', (function(e) {
                    e.preventDefault();
                    $.ajax({
                        url: "<?php echo base_url(); ?>investee/pledge",
                        type: 'POST',
                        data: new FormData(this),
                        contentType: false,
                        cache: false,
                        processData: false
                    })
                            .done(function(data) {
                        if (data == 'success') {
                            $('.deal-footer-links .pledging').text('Pledged');
                            $('.deal-footer-links .pledging').removeClass('pledge pledging');
                        } else {
                            console.log('error');
                        }
                        $.colorbox.close();
                    });

                }));

                $(".search-icon").click(function() {
                    $(this).parents("li").addClass("link-active");
                    $(this).parents("li").siblings("li").removeClass("link-active");
                    $(".search-bar").toggle();
                });

                $('.notification-btn').on('click',function(){
                      $('.notification-num').css('visibility', 'hidden'); 
                      $('.notification-num').html('0');
                     $.ajax({
                        url: "<?php echo base_url(); ?>user/get_notifications"
                    }).done(function(html) {
                          if (html.trim() == "") {
                              data = "<li class='zero-notification'>no notifications yet</li>";
                                $('.notifications').css('display', 'block');
                                $('.notifications').html(data);
                            } else {
                                $('.notifications').css('display', 'block');
                                $('.notifications').html(html);
                            }                    
                    });  
 
                });
                
                $(document).click(function() {
        if ($('.notifications').is(':visible')) {
            $('.notifications').slideToggle('slow');
        }
    });

                if ($('.deals-slider').length) {
                    slider = $('.deals-slider').slick({
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        dots: false,
                        autoplay: false,
                        autoplaySpeed: 5000,
                        arrows: true,
                        nextArrow: '<button type="button" class="slick-next"></button>',
                        responsive: [
                            {
                                breakpoint: 1170,
                                settings: {
                                    slidesToShow: 3,
                                    slidesToScroll: 3,
                                    infinite: true,
                                    dots: true
                                }
                            },
                            {
                                breakpoint: 1024,
                                settings: {
                                    slidesToShow: 3,
                                    slidesToScroll: 2
                                }
                            },
                            {
                                breakpoint: 768,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1
                                }
                            }
                        ]
                    });
                }

            });
        </script>

    </body>
</html>