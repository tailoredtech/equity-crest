<div class="main">
    <div class="container form-contain">
        <div class="row">
            <h4 class="form-header col-lg-12 col-md-12 col-sm-12 col-xs-12 lato-regular">Account Setting</h4>
        </div>
    </div>
    <form id="account-setting-form" action="<?php echo base_url(); ?>user/account_setting_process"  method="post">
    <div class="container">
        <?php if($this->session->flashdata('success-msg')){ ?>
        <div class="row failure-msg">
            <div class="col-sm-10 col-md-offset-1">
                <div class="alert alert-success" role="alert">
                    <?php echo $this->session->flashdata('success-msg'); ?>
                </div>
            </div>
        </div>
        <?php } ?>
        <?php if($this->session->flashdata('error-msg')){ ?>
        <div class="row failure-msg">
            <div class="col-sm-9 col-md-offset-2">
                <div class="alert alert-danger" role="alert">
                    <?php echo $this->session->flashdata('error-msg'); ?>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="row content-box">
            <div class="container   col-md-7 col-sm-7 col-xs-8  col-md-offset-2 col-xs-offset-2 ">
                <div id="name-fields">
                    <div class="row">
                        <div class="form-group">
                            <label for="email_id" class="col-sm-3 form-label">Email ID</label>
                            <div class="col-sm-9">
                                <input type="text" name="name" class="form-control " id="name" value="<?php echo $this->session->userdata('email'); ?>" placeholder="" readonly>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label for="password" class="col-sm-3 form-label">Change Password</label>
                        <div class="col-sm-9">
                            <input type="password" name="password" class="form-control form-input" id="password" placeholder="">
                            
                            <div class="row pswd-description">Password should be of minimum 6 characters</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label for="change_password" class="col-sm-3 form-label">Confirm Password</label>
                        <div class="col-sm-9">
                            <input type="password" name="confirm_password" class="form-control form-input" id="confirm-password" placeholder="">
                        </div>
                    </div>
                </div>
<!--                <div class="row">
                    <div class="form-group">
                        <label for="contact_no" class="col-sm-3 form-label">Change Contact no</label>
                        <div class="col-sm-9">
                            <input type="text" name="mobileno" class="form-control form-input" id="mobile" placeholder="">
                        </div>
                    </div>
                </div>-->
                <div class="join-now col-sm-6">
                    <input type="submit" value="Submit" name="submit" class="join-btn col-sm-12 pull-right lato-regular">
                </div>
            </div>
        </div>
    </div>
    </form>
</div>
<script type="text/javascript">
        $('document').ready(function(){
                        $('#account-setting-form').validate({
                rules: {
                            password: { required : true,
                                    minlength:6
                            },
                            confirm_password:{ required : true,
                                               minlength:6,
                                               equalTo: "#password"
                            },
                        },
		messages: {
                		password: {
					required: "Please provide a password",
					minlength: "Your password must be at least 6 characters long"
				},
				confirm_password: {
					required: "Please provide a password",
					minlength: "Your password must be at least 6 characters long",
					equalTo: "Please enter the same password as above"
				},
			},
                
            });

        });
</script>