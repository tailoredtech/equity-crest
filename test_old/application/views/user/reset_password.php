<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
            <h4 class="form-header  ">Reset Password</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="login-form">
                  <form method="post" id="reset-pass-form" action="<?php echo base_url(); ?>user/reset_password_process">
                    <div class=" col-sm-8 col-sm-offset-1 ">
                        <div class="row">
                            <div class="form-group">
                                <label for="password" class="col-sm-4 form-label">Password</label>
                                <div class="col-sm-8">
                                    <input type="password" class="form-control form-input" id="password" name="password" placeholder="">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="form-group">
                                <label for="password" class="col-sm-4 form-label">Confirm Password</label>
                                <div class="col-sm-8">
                                    <input type="password" class="form-control form-input" id="confirm-password" name="confirm_password" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="join-now col-sm-3 col-sm-offset-3">
                            <input type="hidden" name="email" value="<?php echo $email; ?>" />
                            <input type="hidden" name="id" value="<?php echo $id; ?>" />
                            <input type="submit" value="Reset" name="submit" class="join-btn col-sm-12 pull-right lato-regular">
                        </div>
                    </div>
                  </form>
                </div>
            </div>
        </div>
    </div>  

</div>
<script type="text/javascript">
        $(document).ready(function(){
            $('#reset-pass-form').validate({
                rules: {  
                            email: { required : true,
                                    email: true
                                },
                            password: { required : true,
                                    minlength:6
                            },
                            confirm_password:{ required : true,
                                               minlength:6,
                                               equalTo: "#password"
                            }
                        }
            });
          
          
            
        });
    </script>


        