<?= css('jquery.tagit.css') ?>
<?= css('tagit.ui-zendesk.css') ?>

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
<?= js('tag-it.js') ?>
<div class="main">
    <div class="container wrap">
        <div class="row">
            <h4 class="form-header col-lg-12 col-md-12 col-sm-12 col-xs-12 ">Profile Update</h4>
        </div>

        <form id="investor-info-form" action="<?php echo base_url(); ?>user/edit_investor_info_process" enctype="multipart/form-data" method="post" >
            <div class="container investor-register-form">
                <div class="row">
                    <div class="col-md-12 section-head">
                        <span class="glyphicon glyphicon-chevron-down dropicon"></span><span class="lato-bold">Personal Information</span>
                    </div>
                </div>
                <div class="container personal-info  col-sm-7 col-sm-offset-2">
                    <div class="row">
                        <div class="form-group">
                        <label for="sign-in-as" class="form-label  col-sm-4">Type of Investor</label>
                        <div class="col-sm-8 select-wrap">
                            <div class="select-style">
                                <select name="type" id="type" class="form-control form-input">
                                    <option value="individual" <?php echo ($user['type'] == 'individual') ? 'selected' : ''; ?>>Individual</option>
                                    <option value="angel" <?php echo ($user['type'] == 'angel') ? 'selected' : ''; ?>>Angel</option>
                                </select>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="name" class="col-sm-4 form-label">Name</label>
                            <div class="col-sm-8">
                                <input type="text" name="name" class="form-control form-input" id="name" value="<?php echo $user['name']; ?>" placeholder="">
                            </div>
                        </div>
                    </div>
                    <?php if($user['type'] == 'angel'){ ?>
                    <div class="row company-name-input">
                         <div class="form-group">
                            <label for="company-name" class="col-sm-4 form-label">Company Name</label>
                            <div class="col-sm-8">
                                <input type="text" name="company_name" class="form-control form-input" id="company-name" value="<?php echo $user['company_name']; ?>" placeholder="">
                            </div>
                        </div>
                    </div>
                    <?php }else{ ?>
                    <div class="row company-name-input" style="display: none;">
                         
                    </div>
                    <?php } ?>
                    <div class="row">
                        <div class="form-group">
                            <label for="experience" class="col-sm-4 form-label">Experience Summary</label>
                            <div class="col-sm-8">
                                <textarea class="form-control form-input form-area" name="experience"  id="experience" placeholder=""><?php echo $user['experience']; ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="role" class="col-sm-4 form-label">Role</label>
                            <div class="col-sm-8">
                                <input type="text" name="role" value="<?php echo $user['role']; ?>" class="form-control form-input" id="role" placeholder="Ex. CEO,Founder">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="fb-url" class="col-sm-4 form-label">Facebook</label>
                            <div class="col-sm-8">
                                <input type="text" name="fb_url" value="<?php echo $user['fb_url']; ?>" class="form-control form-input" id="fb-url" placeholder="Facebook URL">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="linkedin-url" class="col-sm-4 form-label">Linkedin</label>
                            <div class="col-sm-8">
                                <input type="text" name="linkedin_url" value="<?php echo $user['linkedin_url']; ?>" class="form-control form-input" id="linkedin-url" placeholder="Linkedin URL">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="twitter-handle" class="col-sm-4 form-label">Twitter</label>
                            <div class="col-sm-8">
                                <input type="text" name="twitter_handle" value="<?php echo $user['twitter_handle']; ?>" class="form-control form-input" id="twitter-handle" placeholder="Twitter handle">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="country" class="col-sm-4 form-label">Country</label>
                            <div class="col-sm-8 select-wrap">
                                <div class="select-style">
                                    <select class="form-control form-input country" name="country"  placeholder="">
                                        <option value="">Select Country</option>
                                        <?php foreach ($countries as $country) { ?>
                                            <option value="<?php echo $country['id']; ?>" <?php echo ($country['id'] == $user['country'] ) ? 'selected' : '' ?> ><?php echo $country['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="state" class="col-sm-4 form-label">State</label>
                            <div class="col-sm-8 select-wrap">
                                <div class="select-style">
                                    <select class="form-control form-input state" name="state"  placeholder="">
                                        <?php foreach ($states as $state) { ?>
                                            <option value="<?php echo $state['id']; ?>" <?php echo ($state['id'] == $user['state'] ) ? 'selected' : '' ?>><?php echo $state['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="city" class="col-sm-4 form-label">City</label>
                            <div class="col-sm-8">
                                <input type="text" name="city" value="<?php echo $user['city']; ?>" class="form-control form-input" id="city" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <input type="submit" name="submit"  class="save-btn pull-right" value="Save" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12"><hr></div>
                </div>
                <div class="row">
                    <div class="col-md-12 section-head">
                        <span class="glyphicon glyphicon-chevron-down dropicon"></span><span class="lato-bold">Investment Philosophy</span>
                    </div>                    
                </div>

                <div class="container investment wrap" style="display: none;">
                    <div class="row">
                        <div class="col-sm-7 col-sm-offset-2">
                            <div class="row">
                                <div class="form-group">
                                    <label for="team-summary" class="col-sm-4 form-label">Sector Expertise</label>
                                    <div class="col-sm-8">
                                        <div class="select-style">
                                            <select class="form-control form-input" name="sector_expertise[]"  placeholder="" multiple>
                                                <option value="">Select Multiple Sectors</option>
                                                <?php
                                                $sector_autocomplete_string = '';
                                                $sector_expertise = explode(",", $user['sector_expertise']);
                                                foreach ($sectors as $sector) {
                                                    $sector_autocomplete_string .= '"' . $sector['name'] . '",';
                                                    ?>
                                                    <option value="<?php echo $sector['name']; ?>" <?php echo (in_array($sector['name'], $sector_expertise)) ? 'selected' : ''; ?> ><?php echo $sector['name']; ?></option>
                                                <?php
                                                }
                                                $sector_autocomplete_string = trim($sector_autocomplete_string, ",");
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group">
                                    <label for="key-points" class="col-sm-4 form-label">Key Points in Company I look</label>

                                    <div class="col-sm-8 key-points-list">
                                        <input type="text" class="form-control form-input form-area" id="key-points" name="key_points" value="<?php echo $user['key_points'] ?>" placeholder="Ex. Team, Market, Traction, Production, Business Model, Others " />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="display: none;">
                    <div class="col-sm-6">
                        <input type="submit" name="submit"  class="save-btn pull-right" value="Save" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12"><hr></div>
                </div>
                <div class="row">
                    <div class="col-md-12 section-head companies">
                        <span class="glyphicon glyphicon-chevron-down dropicon"></span><span class="lato-bold">Portfolio of Early Stage Companies</span>
                    </div>
                </div>
                <div class="container startup-portfolio  col-sm-7 col-sm-offset-2" style="display: none;">
                    <ul class="list-group space-20">
                        <?php $portfolio_count = count($portfolios); foreach ($portfolios as $portfolio) { ?>
                        <li class="list-group-item">
                            <a href="javascript:void(0);" class="company" id="company-<?php echo $portfolio['id'];  ?>"><?php echo $portfolio['name'];  ?></a>
                            <a href="javascript:void(0);" class="remove-company" data-company-id="<?php echo $portfolio['id'];  ?>">
                                <span class="glyphicon glyphicon-remove pull-right"></span>
                            </a>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="container startup-portfolio  col-sm-7 col-sm-offset-2" >
                    <div id="add-portfolios">
                        <div class="portfolios-list">
                            <?php  if($portfolio_count) { ?>
                          <?php $count=1; foreach ($portfolios as $portfolio) { ?>
                            <div class="portfolio-box" id="portfolio-<?php echo $portfolio['id'];  ?>" style="display: none;">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-sm-4 form-label">Name of Company</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="portfolio_name_<?php echo $count; ?>" class="form-control form-input" value="<?php echo $portfolio['name'] ?>" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-sm-4 form-label">Website URL</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="portfolio_url_<?php echo $count; ?>" value="<?php echo $portfolio['url'] ?>" class="form-control form-input" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-sm-4 form-label">Location</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="portfolio_location_<?php echo $count; ?>" value="<?php echo $portfolio['location'] ?>" class="form-control form-input" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label  class="col-sm-4 form-label">Sector</label>
                                        <div class="col-sm-8">
                                            <div class="select-style">
                                                <select class="form-control form-input" name="portfolio_sector_<?php echo $count; ?>"  placeholder="">
                                                    <option value="">Sector</option>
                                                    <?php foreach ($sectors as $sector) { ?>
                                                        <option value="<?php echo $sector['id']; ?>" <?php echo ($sector['id'] == $portfolio['sector_id']) ? 'selected' : ''; ?> ><?php echo $sector['name']; ?></option>
                                                   <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="role" class="col-sm-4 form-label">Role</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="portfolio_role_<?php echo $count; ?>" value="<?php echo $portfolio['role'] ?>" class="form-control form-input" placeholder="">
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label  class="col-sm-4 form-label">Your Logo</label>
                                        <div class="col-sm-3 portfolio-image">
                                           <img src="<?php echo base_url() ?>uploads/portfolios/<?php echo $portfolio['id']; ?>/<?php echo $portfolio['image'] ?>" width="100" height="100">
                                           <span class="change-img">Change</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label  class="col-sm-4 form-label">Remarks</label>
                                        <div class="col-sm-8">
                                            <textarea class="form-control form-input form-area"  name="portfolio_remarks_<?php echo $count; ?>" placeholder=""><?php echo $portfolio['remark'] ?></textarea>
                                            <input type="hidden" name="id_<?php echo $count; ?>" value="<?php echo $portfolio['id'] ?>" />
                                        </div>
                                    </div>
                                </div>
                            </div>   
                          <?php $count++;} ?>
                            <?php }else { ?>
                            <div class="new-portfolio-box" style="display:none;">
                               <div class="row">
                                    <div class="form-group">
                                        <label class="col-sm-4 form-label">Name of Company</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="portfolio_name_1" class="form-control form-input" value="" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-sm-4 form-label">Website URL</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="portfolio_url_1" class="form-control form-input" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-sm-4 form-label">Location</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="portfolio_location_1" class="form-control form-input" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label  class="col-sm-4 form-label">Sector</label>
                                        <div class="col-sm-8">
                                            <div class="select-style">
                                                <select class="form-control form-input" name="portfolio_sector_1"  placeholder="">
                                                    <option value="">Sector</option>
                                                    <?php foreach ($sectors as $sector) { ?>
                                                        <option value="<?php echo $sector['id']; ?>" ><?php echo $sector['name']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="role" class="col-sm-4 form-label">Role</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="portfolio_role_1" class="form-control form-input" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label  class="col-sm-4 form-label">Your Logo</label>
                                        <div class="col-sm-8">
                                            <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput">
                                                <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                                                <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file"  name="portfolio_image_1"></span>
                                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label  class="col-sm-4 form-label">Remarks</label>
                                        <div class="col-sm-8">
                                            <textarea class="form-control form-input form-area"  name="portfolio_remarks_1" placeholder=""></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="more-portfolio-btn" style="display: none;">Add More</div>
                </div>
                <div class="row portfolio-save" style="display: none;">
                    <div class="col-sm-6">
                        <input type="submit" name="submit"  class="save-btn pull-right" value="Save" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12"><hr></div>
                </div>
                <div class="row">
                    <div class="col-md-12 section-head">
                        <span class="glyphicon glyphicon-chevron-down dropicon"></span><span class="lato-bold">Interest in Mentoring</span>
                    </div>
                </div>
                <div class="container interest col-sm-7 col-sm-offset-2" style="display: none;">

                    <div class="row">
                        <div class="form-group">
                            <label for="mentoring-sectors" class="col-sm-4 form-label">Sector</label>
                            <div class="col-sm-8">
    <!--                            <textarea class="form-control form-input form-area" id="mentoring-sectors" name="mentoring_sectors" placeholder=""></textarea>-->
                                <input type="text" class="form-control form-input form-area"  id="mentoring-sectors" name="mentoring_sectors" value="<?php echo $user['mentoring_sectors'] ?>" placeholder=""   />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="expertise" class="col-sm-4 form-label">Area of Expertise</label>
                            <div class="col-sm-8">
    <!--                            <textarea class="form-control form-input form-area" id="expertise" name="expertise" placeholder=""></textarea>-->
                                <input type="text" class="form-control form-input form-area" id="expertise" name="expertise" value="<?php echo $user['expertise'] ?>" placeholder="" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="duration" class="col-sm-4 form-label">Duration</label>
                            <div class="col-sm-8">
                                <input type="text" name="duration" class="form-control form-input" id="duration" value="<?php echo $user['duration'] ?>" placeholder="">
                                <span class="col-sm-2 input-placeholder   pull-right">hrs/week</span>
                            </div>
                        </div>
                    </div>

                </div> 
                <div class="row" style="display: none;">
                    <div class="col-sm-6">
                        <input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
                        <input type="hidden" id="portfolio_count" name="portfolio_count" value="<?php echo ($portfolio_count) ? $portfolio_count: 1;?>" />
                        <input type="submit" class="save-btn  pull-right" name="submit" value="Save" />
                    </div>
                </div>
            </div>      
        </form> 
    </div>
</div>    
<div style="display: none;">
    <div id="portfolio-fields">
         <div class="new-portfolio-box">
            <div class="row">
                <div class="form-group">
                    <label class="col-sm-4 form-label">Name of Company</label>
                    <div class="col-sm-8">
                        <input type="text" name="portfolio_name" class="form-control form-input" value="" placeholder="" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label class="col-sm-4 form-label">Website URL</label>
                    <div class="col-sm-8">
                        <input type="text" name="portfolio_url" class="form-control form-input" placeholder="" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label class="col-sm-4 form-label">Location</label>
                    <div class="col-sm-8">
                        <input type="text" name="portfolio_location" class="form-control form-input" placeholder="" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label  class="col-sm-4 form-label">Sector</label>
                    <div class="col-sm-8">
                        <div class="select-style">
                            <select class="form-control form-input" name="portfolio_sector"  placeholder="" required>
                                <option value="">Sector</option>
                                <?php foreach ($sectors as $sector) { ?>
                                    <option value="<?php echo $sector['id']; ?>" ><?php echo $sector['name']; ?></option>
                              <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label for="role" class="col-sm-4 form-label">Role</label>
                    <div class="col-sm-8">
                        <input type="text" name="portfolio_role" class="form-control form-input" placeholder="" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label  class="col-sm-4 form-label">Your Logo</label>
                    <div class="col-sm-8">
                        <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput">
                            <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                            <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file"  name="portfolio_image" required></span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label  class="col-sm-4 form-label">Remarks</label>
                    <div class="col-sm-8">
                        <textarea class="form-control form-input form-area"  name="portfolio_remarks" placeholder=""></textarea>
                        <input type="hidden" name="id" value="0" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <div class="col-sm-5 col-sm-offset-4">
                        <div class="btn btn-gray cancle-portfolio-btn">Cancle</div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
    
    <div id="company-name-field">
        <div class="form-group">
            <label for="company-name" class="col-sm-4 form-label">Company Name</label>
            <div class="col-sm-8">
                <input type="text" name="company_name" class="form-control form-input" id="company-name" value="" placeholder="">
            </div>
        </div>
    </div>
    
    <div class="pop-content" id="ch-img">
        <h3 class="form-header">Upload Image</h3>
        <div class="askexpert">
            <form id="upload-image" action="<?php echo base_url(); ?>investor/ajax_portfolio_image_upload" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="col-sm-1 form-label">Image</label>
                    <div class="col-sm-11">
                        <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput">
                            <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                            <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="userfile"></span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>
                </div>
                <div class="buttons">
                    <input type="hidden" class="portfolio-id" name="portfolio_id" value="" />
                    <input type="submit" class="btn btn-green" />
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $('document').ready(function(){
        var portfolio_count = '<?php echo ($portfolio_count) ? $portfolio_count : 1; ?>';
        $('#investor-info-form').on('change', '.country', function(){
            $.ajax({
                url: "<?php echo base_url(); ?>user/states",
                type: 'POST',
                data : {countryId : $(this).val()}
            })
            .done(function( data ) {
                $('#investor-info-form .state').html(data);
            });
        });
        
        $('#investor-info-form textarea').htmlarea({
            toolbar: [
                        ["bold", "italic", "underline"],
                        ["p"],
                        ["link", "unlink"],
                        ["orderedList","unorderedList"],
                        ["indent","outdent"],
                        ["justifyleft", "justifycenter", "justifyright"] 
                    ]
        });
        
        $('#investor-info-form').on('change', '#type', function(){
            var type = $(this).val();
            if(type == 'angel'){
                $('.company-name-input').html($('#company-name-field').html());
                $('.company-name-input').show();
            }else{
                $('.company-name-input').html('');
                $('.company-name-input').hide();
            }
        });
      
        $('#investor-info-form').validate({
            rules: {
                name: { required : true,
                    minlength:4
                },
                experience: 'required',    
                website:'required',
                country:'required',
                state:'required',
                city:'required',
                year:'required',
                company_name:'required',
                about_company:'required',
                'optional_name[]':'required'
            } ,
            messages: {
                name: {
                    required: "Please provide name",
                    minlength: "Name must be at least 4 characters long"
                },
                experience: {
                    required: "Please enter your experience summary"
                },
                website: {
                    required: "Please provide your website"
                },
                country: 'Please select country',
                state: 'Please select state',
                city: 'Please enter city',
                year:'Please enter year of establishment',
                company_name: "Please enter name of the compnay",
                about_company: "Please enter about your company"
            },
            errorPlacement: function (error, element) {
                if($(element).parent().hasClass("select-style")){
                    $(element).parents('.select-wrap').append(error);
                }else if($(element).attr("type") == 'file'){
                    $(element).parents('.select-wrap').append(error);
                }else{
                    error.insertAfter(element); 
                } 
            }
        });
        
        $('.more-portfolio-btn').on('click',function(){
            ++portfolio_count;
            console.log(portfolio_count);
            $('#portfolio_count').val(portfolio_count);
            $('#portfolio-fields').find('input[name*="portfolio_name"]').attr('name','portfolio_name_'+portfolio_count);
            $('#portfolio-fields').find('input[name*="portfolio_url"]').attr('name','portfolio_url_'+portfolio_count);
            $('#portfolio-fields').find('input[name*="portfolio_location"]').attr('name','portfolio_location_'+portfolio_count);
            $('#portfolio-fields').find('select[name*="portfolio_sector"]').attr('name','portfolio_sector_'+portfolio_count);
            $('#portfolio-fields').find('input[name*="portfolio_role"]').attr('name','portfolio_role_'+portfolio_count);
            $('#portfolio-fields').find('input[name*="portfolio_image"]').attr('name','portfolio_image_'+portfolio_count);
            $('#portfolio-fields').find('textarea[name*="portfolio_remarks"]').attr('name','portfolio_remarks_'+portfolio_count);
            $('#portfolio-fields').find('input[name*="id"]').attr('name','id_'+portfolio_count);
            $('#add-portfolios .portfolios-list').append($('#portfolio-fields').html());
        });
        
        $('#add-portfolios').on('click','.change-img',function(){
            var portfolio_id = $(this).parents('.portfolio-box').attr('id');
            portfolio_id = portfolio_id.slice(-1);
            $('#ch-img .portfolio-id').val(portfolio_id);
            $.colorbox({inline: true, href: '#ch-img', innerWidth: '400px', innerHeight: '175px'});
        });
         
       $("#upload-image").on('submit',(function(e){
            e.preventDefault();
            $.ajax({
                    url: "<?php echo base_url(); ?>investor/ajax_portfolio_image_upload",
                    type: "POST",
                    data:  new FormData(this),
                    dataType :'json',
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(data){
                           $('.portfolio-box#portfolio-'+data.id).find('img').attr('src','<?php echo base_url(); ?>'+data.img);
                           $.colorbox.close()
                    },
                    error: function(){

                    } 
            });         
        }));
        
        
        $('#key-points').tagit({
            availableTags: ['Team','Market','Traction','Production','Business Model','Others'],
            minLength: 2
        });
        
        $('#mentoring-sectors').tagit({
            availableTags: [<?php echo $sector_autocomplete_string; ?>],
            minLength: 2
        });
        $('#expertise').tagit({
            allowSpaces : true
        });
        
        $('.section-head').on('click',function(){
            $(this).parents('.row').next('.container').toggle();
            $(this).parents('.row').next('.container').next('.row').toggle();
            
            if($(this).hasClass('companies')){
                $('.more-portfolio-btn').toggle();
                $('.portfolio-save').toggle();
                if ( $('#add-portfolios .new-portfolio-box' ).length ) {
 
                    $('#add-portfolios .new-portfolio-box').toggle();

                }
            }
        });
        
        $('.company').on('click',function(){
            var company_id = $(this).attr('id');
            company_id = company_id.split('-');
            company_id = company_id[1];
            $('.portfolio-box').hide();
            $('#portfolio-'+company_id).show();
        });
        
        $('.remove-company').on('click',function(){
            var company_id = $(this).attr('data-company-id');
            var current_company = $(this).parents('.list-group-item');
            $.ajax({
                    url: "<?php echo base_url(); ?>investor/delete_portfolio",
                    type: "POST",
                    data:  {company_id:company_id},
                    dataType :'html',
                    success: function(data){
                       current_company.remove();
                    },
                    error: function(){

                    } 
            });
        });
        
        $('#add-portfolios').on('click','.cancle-portfolio-btn',function(){
              $(this).parents('.new-portfolio-box').remove();
              portfolio_count--;
              $('#portfolio_count').val(portfolio_count);
        });
        
    });
</script>
