<?php parse_str($_SERVER['QUERY_STRING'],$_GET);
    $user = '';
    if(isset($_GET['user']) && $_GET['user'] != ''){
       $user = $_GET['user'];
    }
?>    
<div class="container register-container form-contain wrap">
            <div class="row">
                    <h4 class="form-header col-lg-12 col-md-12 col-sm-12 col-xs-12 lato-regular">Join Now</h4>
            </div>
        
            <div class="container investor-register-form">
                <div class="row success-msg" style="display: none;">
                    <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-success" role="alert">
                                Thank you for registering on Equity Crest as an <span class="u"></span>. An activation e-mail has been sent to you on your e-mail ID <span class="e"></span>. Request you to please activate your Equity Crest account by clicking on the link sent across.
                            </div>
                    </div>
                    
                </div>
                <?php if($this->session->flashdata('error-msg')){ ?>
                    <div class="row failure-msg">
                        <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-danger" role="alert">
                                <?php echo $this->session->flashdata('error-msg'); ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <form method="post" id="register-form" action="<?php echo base_url(); ?>user/register_process">
                    <div class="container   col-md-7 col-sm-7 col-xs-8  col-md-offset-2 col-xs-offset-2 ">
                            
                            <div class="row">
                                <div class="lato-regular" style="margin-bottom: 10px;">
                                            <label for="sign-in-as" class="form-label col-lg-3 col-md-3 col-sm-3 col-xs-3 black lato-bold ">Sign In as</label>
                                                <div class="col-sm-9">
                                                    
                                                    <label class="radio-inline ">
                                                        <input type="radio" name="role"  value="investor"> Investor
                                                    </label>
                                                    <label class="radio-inline ">
                                                            <input type="radio" name="role"  value="investee"> Start-Up
                                                    </label>
                                                    <label class="radio-inline ">
                                                            <input type="radio" name="role"  value="channel_partner"> Partner
                                                    </label>
                                                    <label class="radio-inline ">
                                                            <input type="radio" name="role"  value="media"> Media
                                                    </label>
                                                </div>
                                    </div>

                            </div>
                           <div id="name-fields">
                               <div class="row">
                                        <div class="form-group">
                                            <label for="name" class="col-sm-3 form-label">Full Name *</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="name" class="form-control " id="name" placeholder="" required>
                                            </div>
                                        </div>
                                </div>
                           </div>
                            
                            <div class="row">
                                    <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-3 form-label">Mobile No *</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="mobileno" class="form-control form-input" id="mobile" placeholder="">
                                                </div>
                                                </div>
                            </div>
                            <div class="row">
                                    <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-3 form-label">Email Id *</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="email" class="form-control form-input" id="email" placeholder="">
                                                </div>
                                                </div>
                            </div>
                            <div class="row">
                                    <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-3 form-label">Password *</label>
                                                <div class="col-sm-9">
                                                    <input type="password" name="password" class="form-control form-input" id="password" placeholder="">
                                                </div>
                                                </div>
                            </div>
                            <div class="row">
                                    <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-3 form-label">Confirm Password *</label>
                                                <div class="col-sm-9">
                                                    <input type="password" name="confirm_password" class="form-control form-input" id="confirm-password" placeholder="">
                                                </div>
                                                </div>
                            </div>
                            <div class="row">
                                    <div class="col-sm-6 col-sm-offset-3">
                                            <input type="checkbox" name="agree" class="pull-left icheckbox privacy-check"> 
                                            <p class="pull-left form-agree col-sm-11 lato-regular">
                                                    I agree to EC
                                                    <a href="<?php echo base_url(); ?>home/terms_of_use" class="form-link">
                                                            Terms of Use
                                                    </a>,
                                                    <a href="<?php echo base_url(); ?>home/privacy_policy" class="form-link">
                                                            Privacy policy
                                                    </a>&
                                                    <a href="<?php echo base_url(); ?>home/nda" class="form-link">
                                                            NDA
                                                    </a>
                                                    
                                            </p>
                                    </div>
                                    <div class="join-now col-sm-3 pull-right">
                                        <input type="submit" value="Join Now" name="submit" class="join-btn col-sm-12 pull-right lato-regular" style="margin-right: 0px">
                                    </div>
                            </div>


                            <div class="row social-or"></div>
                            <div class="row social-sign-up">
                                    <div class="col-sm-8 col-sm-offset-2">
                                            <p class="col-sm-5 pull-left social-head">Quick Sign in using</p>
                                            <div class="col-sm-7 pull-left social-ico">
                                                    <a href="<?php echo base_url(); ?>user/linkedin_login" class="social-reg" id="linkedin">Linked-in</a>
                                                    <a href="<?php echo base_url(); ?>user/googleplus_login" class="social-reg" id="google">Google</a>
                                                    <a href="<?php echo base_url(); ?>user/fb_login" class="social-reg" id="facebook">Facebook</a>
                                            </div>
                                    </div>
                            </div>
                    </div>
                    </form>
            </div>
    </div>
   <div style="display: none;">
       <div id="investor-name-fields">
           <div class="row">
                <div class="form-group">
                    <label for="name" class="col-sm-3 form-label">Full Name *</label>
                    <div class="col-sm-9">
                        <input type="text" name="name" class="form-control form-input" id="name" placeholder="" required>
                    </div>
                </div>
            </div>
       </div>
       <div id="investee-name-fields">
           <div class="row">
                <div class="form-group">
                    <label for="company-name" class="col-sm-3 form-label">Company Name *</label>
                    <div class="col-sm-9">
                        <input type="text" name="company_name" class="form-control form-input" id="company-name" placeholder="" required>
                    </div>
                </div>
            </div>
           <div class="row">
                <div class="form-group">
                    <label for="name" class="col-sm-3 form-label">Founder Name *</label>
                    <div class="col-sm-9">
                        <input type="text" name="name" class="form-control form-input" id="name" placeholder="" required>
                    </div>
                </div>
            </div>
       </div>
       <div id="channel-name-fields">
           <div class="row">
                <div class="form-group">
                    <label for="company-name" class="col-sm-3 form-label">Company Name *</label>
                    <div class="col-sm-9">
                        <input type="text" name="company_name" class="form-control form-input" id="company-name" placeholder="" required>
                    </div>
                </div>
            </div>
           <div class="row">
                <div class="form-group">
                    <label for="name" class="col-sm-3 form-label">Representative Name *</label>
                    <div class="col-sm-9">
                        <input type="text" name="name" class="form-control form-input" id="name" placeholder="" required>
                    </div>
                </div>
            </div>
       </div>
       <div id="media-name-fields">
           <div class="row">
                <div class="form-group">
                    <label for="company-name" class="col-sm-3 form-label">Company Name *</label>
                    <div class="col-sm-9">
                        <input type="text" name="company_name" class="form-control form-input" id="company-name" placeholder="" required>
                    </div>
                </div>
            </div>
           <div class="row">
                <div class="form-group">
                    <label for="name" class="col-sm-3 form-label">Representative Name *</label>
                    <div class="col-sm-9">
                        <input type="text" name="name" class="form-control form-input" id="name" placeholder="" required>
                    </div>
                </div>
            </div>
       </div>
   </div>	
        
    <?=js('icheck.min.js')?>

    <script type="text/javascript">
        $(document).ready(function(){
//            $('.radio-inline input,.icheckbox').iCheck({
//                checkboxClass: 'icheckbox_minimal',
//                radioClass: 'iradio_minimal' // optional
//            });

          <?php if($user){
              if($user == 'investor'){ ?>
                  $('#name-fields').html($('#investor-name-fields').html());
                  $('input[value="investor"]').attr('checked','checked');
          <?php    }     
               if($user == 'investee'){ ?>
                  $('#name-fields').html($('#investee-name-fields').html());
                  $('input[value="investee"]').attr('checked','checked');
          <?php }  
          
                if($user == 'channel_partner'){ ?>
                    $('#name-fields').html($('#channel-name-fields').html());
                    $('input[value="channel_partner"]').attr('checked','checked');
           <?php }
           } ?>        

            $('.radio-inline  input[type="radio"]').click(function() {
//               var type = $(this).siblings('input[type="radio"]').val();
                 var type = $(this).val();
                switch (type) {
                                    case 'investor':
                                        $('#name-fields').html($('#investor-name-fields').html());
                                        break;
                                    case 'investee':
                                        $('#name-fields').html($('#investee-name-fields').html());
                                        break;
                                    case 'channel_partner':
                                        $('#name-fields').html($('#channel-name-fields').html());
                                        break;
                                    case 'media':
                                        $('#name-fields').html($('#media-name-fields').html());
                                        break;
                                }
            });

            $('#register-form').validate({
                rules: {
                            //role:'required',
                            name: { required : true,
                                    minlength:4
                                },
                            mobileno: { required : true,
                                        minlength:10
                                },    
                            email: { required : true,
                                    email: true
                                },
                            password: { required : true,
                                    minlength:6
                            },
                            confirm_password:{ required : true,
                                               minlength:6,
                                               equalTo: "#password"
                            },
                            agree:'required'
                        },
			messages: {
                                role:'Please select your role',
				name: {
					required: "Please provide your full name",
					minlength: "Your full name must be at least 4 characters long"
				},
                                mobileno: {
					required: "Please enter your mobile number",
                                        minlength: "Please enter atleast 10  digit mobile number"
				},
				password: {
					required: "Please provide a password",
					minlength: "Your password must be at least 6 characters long"
				},
				confirm_password: {
					required: "Please provide a password",
					minlength: "Your password must be at least 6 characters long",
					equalTo: "Please enter the same password as above"
				},
				email: "Please enter a valid email address",
				agree: "Please accept our policy"
			},
                        errorPlacement: function (error, element) {
                            if($(element).parent().hasClass("select-style")){
                                $(element).parents('.col-sm-8').append(error);
                            }else if($(element).attr("type") == 'file'){
                                $(element).parents('.col-sm-8').append(error);
                            }else{
                                error.insertAfter(element); 
                            } 
                        }
            });

            $('#register-form').submit(function(){
                    if($("#register-form").valid()==false){

                    }else{
                        return true;
//                        var email = $('#email').val();
////                        var type = $('input[name="role"]').val();
//                        var type = $('input:radio[name="role"]:checked').val();
//                        $.ajax({
//                        type:'post',
//                        dataType:'json',
//                        url:$(this).attr('action'),
//                        data: $(this).serializeArray(),
//                        success: function(data) {
//                                if(data.success == true){
//                                    
//                                    switch (type) {
//                                        case 'investor':
//                                            type = "Investor";
//                                            break;
//                                        case 'investee':
//                                            type = "Start-Up";
//                                            break;
//                                        case 'channel_partner':
//                                            type = "Channel partner";
//                                            break;
//                                        case 'media':
//                                            type = "Media";
//                                            break;
//                                    }
//                                    
//                                    $('.success-msg .u').html(type);
//                                    $('.success-msg .e').html(email);
//                                    $('.success-msg').show();
//                                }else{
//                                    $('.failure-msg').show();
//                                }
//                                
//                                $("#register-form").trigger('reset');
//                            },
//                            error: function(xhr, textStatus, thrownError) {
//                                console.log('error');
//                            }
//                        });

                    } 
                    return false;
            });
        });
    </script>
