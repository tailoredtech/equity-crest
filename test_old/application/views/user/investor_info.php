<?=css('jquery.tagit.css')?>
<?=css('tagit.ui-zendesk.css')?>

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
<?=js('tag-it.js')?>

<div class="main">
<div class="container wrap">
    <div class="row">
        <h4 class="form-header col-lg-12 col-md-12 col-sm-12 col-xs-12 ">Investor Profile Update</h4>
    </div>

    <form id="investor-info-form" action="<?php echo base_url(); ?>user/investor_info_process" enctype="multipart/form-data" method="post" >
        <div class="container investor-register-form">
            <div class="row">
                <div class="col-md-12 section-head">
                    <span class="glyphicon glyphicon-chevron-down dropicon"></span><span class="lato-bold ">Personal Information</span>
                </div>
            </div>
            <div class="container personal-info  col-sm-7 col-sm-offset-2">
                <div class="row">
                    <div class="form-group">
                        <label for="sign-in-as" class="col-sm-4 form-label">Type of Investor</label>
                        <div class="col-sm-8 select-wrap">
                            <div class="select-style">
                                <select name="type" id="type" class="form-control form-input">
                                    <option value="individual">Individual</option>
                                    <option value="angel">Angel</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="form-group">
                        <label for="name" class="col-sm-4 form-label">Name</label>
                        <div class="col-sm-8">
                            <input type="text" name="name" class="form-control form-input" id="name" value="<?php echo $this->session->userdata('name'); ?>" placeholder="">
                        </div>
                    </div>
                </div>
                <div class="row company-name-input" style="display: none;">
                    
                </div>
                <div class="row">
                    <div class="form-group">
                        <label for="experience" class="col-sm-4 form-label">Experience Summary</label>
                        <div class="col-sm-8">
                            <textarea class="form-control form-input form-area" name="experience" id="experience" placeholder=""></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label for="role" class="col-sm-4 form-label">Role</label>
                        <div class="col-sm-8">
                            <input type="text" name="role" class="form-control form-input" id="role" placeholder="Ex. CEO,Founder">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label for="fb-url" class="col-sm-4 form-label">Facebook</label>
                        <div class="col-sm-8">
                            <input type="text" name="fb_url" class="form-control form-input" id="fb-url" placeholder="Facebook URL">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label for="linkedin-url" class="col-sm-4 form-label">Linkedin</label>
                        <div class="col-sm-8">
                            <input type="text" name="linkedin_url" class="form-control form-input" id="linkedin-url" placeholder="Linkedin URL">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label for="twitter-handle" class="col-sm-4 form-label">Twitter</label>
                        <div class="col-sm-8">
                            <input type="text" name="twitter_handle" class="form-control form-input" id="twitter-handle" placeholder="Twitter handle">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label for="country" class="col-sm-4 form-label">Country</label>
                        <div class="col-sm-8 select-wrap">
                            <div class="select-style">
                                <select class="form-control form-input country" name="country"  placeholder="">
                                    <option value="">Select Country</option>
                                    <?php foreach ($countries as $country) { ?>
                                        <option value="<?php echo $country['id']; ?>" ><?php echo $country['name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label for="state" class="col-sm-4 form-label">State</label>
                        <div class="col-sm-8 select-wrap">
                            <div class="select-style">
                                <select class="form-control form-input state" name="state"  placeholder="">
                                    <option value="">Select State</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label for="city" class="col-sm-4 form-label">City</label>
                        <div class="col-sm-8">
                            <input type="text" name="city" class="form-control form-input" id="city" placeholder="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 form-label">Your Photo</label>
                        <div class="col-sm-8">
                            <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput">
                                <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                                <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" id="image" name="image"></span>
                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                    <div class="col-sm-6">
                        <input type="button" id="investment-next" class="save-btn pull-right" value="Save" />
                    </div>
            </div>
            <div class="row">
                <div class="col-sm-12"><hr></div>
            </div>
            <div class="row">
                <div class="col-md-12 section-head">
                    <span class="glyphicon glyphicon-chevron-down dropicon"></span><span class="lato-bold">Investment Philosophy</span>
                </div>
            </div>

            <div class="container investment" style="display: none;">
                <div class="row">
                    <div class="col-sm-7 col-sm-offset-2">
                        <div class="row">
                            <div class="form-group">
                                <label for="team-summary" class="col-sm-4 form-label">Sector Expertise</label>
                                <div class="col-sm-8">
                                    <div class="select-style">
                                        <select class="form-control form-input country" name="sector_expertise[]"  placeholder="" multiple>
                                            <option value="">Select Multiple Sectors</option>
                                            <?php 
                                            $sector_autocomplete_string = '';
                                            foreach ($sectors as $sector) {
                                                $sector_autocomplete_string .= '"'.$sector['name'].'",';
                                                ?>
                                                <option value="<?php echo $sector['name']; ?>" ><?php echo $sector['name']; ?></option>
                                            <?php }
                                             $sector_autocomplete_string = trim($sector_autocomplete_string, ",");
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label for="key-points" class="col-sm-4 form-label">Key Points in Company I look</label>

                                <div class="col-sm-8 key-points-list">
                                    <input type="text" class="form-control form-input form-area" id="key-points" name="key_points" value="" placeholder="Ex. Team, Market, Traction, Production, Business Model, Others " />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="display: none;">
                    <div class="col-sm-6">
                        <input type="button" id="portfolio-next" class="save-btn pull-right" value="Save" />
                    </div>
            </div>
            <div class="row">
                <div class="col-sm-12"><hr></div>
            </div>
            <div class="row">
                <div class="col-md-12 section-head">
                    <span class="glyphicon glyphicon-chevron-down dropicon"></span><span class="lato-bold ">Portfolio of Early Stage Companies</span>
                </div>
            </div>
            <div class="container startup-portfolio  col-sm-7 col-sm-offset-2" style="display: none;">
                <div id="add-portfolios">
                    <div class="portfolios-list">
                        <div class="portfolio-box">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-4 form-label">Name of Company</label>
                                <div class="col-sm-8">
                                    <input type="text" name="portfolio_name_1" class="form-control form-input" value="" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-4 form-label">Website URL</label>
                                <div class="col-sm-8">
                                    <input type="text" name="portfolio_url_1" class="form-control form-input" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-4 form-label">Location</label>
                                <div class="col-sm-8">
                                    <input type="text" name="portfolio_location_1" class="form-control form-input" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label  class="col-sm-4 form-label">Sector</label>
                                <div class="col-sm-8">
                                    <div class="select-style">
                                        <select class="form-control form-input country" name="portfolio_sector_1"  placeholder="">
                                            <option value="">Sector</option>
                                            <?php foreach ($sectors as $sector) { ?>
                                                <option value="<?php echo $sector['id']; ?>" ><?php echo $sector['name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label for="role" class="col-sm-4 form-label">Role</label>
                                <div class="col-sm-8">
                                    <input type="text" name="portfolio_role_1" class="form-control form-input" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label  class="col-sm-4 form-label">Your Logo</label>
                                <div class="col-sm-8">
                                    <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput">
                                        <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                                        <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file"  name="portfolio_image_1"></span>
                                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label  class="col-sm-4 form-label">Remarks</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control form-input form-area"  name="portfolio_remarks_1" placeholder=""></textarea>
                                </div>
                            </div>
                        </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <div class="btn btn-gray cancle-portfolio-btn pull-right">Cancle</div>
                                    </div>
                                </div>
                            </div>
                     </div>
                    </div>
                </div>
                <div class="more-portfolio-btn">Add More</div>&nbsp;&nbsp; Or &nbsp;&nbsp;
                  <div class="skip-portfolio-btn">Skip</div>
            </div>
            <div class="row" style="display: none;">
                    <div class="col-sm-6">
                        <input type="button" id="interest-next" class="save-btn pull-right" value="Save" />
                    </div>
            </div>
            <div class="row">
                <div class="col-sm-12"><hr></div>
            </div>
            <div class="row">
                <div class="col-md-12 section-head">
                    <span class="glyphicon glyphicon-chevron-down dropicon"></span><span class="lato-bold ">Interest in Mentoring</span>
                </div>
            </div>
            <div class="container interest col-sm-7 col-sm-offset-2" style="display: none;">

                <div class="row">
                    <div class="form-group">
                        <label for="mentoring-sectors" class="col-sm-4 form-label">Sector</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-input form-area"  id="mentoring-sectors" name="mentoring_sectors" placeholder=""   />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label for="expertise" class="col-sm-4 form-label">Area of Expertise</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-input form-area" id="expertise" name="expertise" placeholder="" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label for="duration" class="col-sm-4 form-label">Duration</label>
                        <div class="col-sm-8">
                            <input type="text" name="duration" class="form-control form-input" id="duration" value="" placeholder="">
                            <span class="col-sm-3 input-placeholder   pull-right">hours/week</span>
                        </div>
                    </div>
                </div>

            </div> 
            <div class="row" style="display: none;">
                <div class="col-sm-6">
                    <input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
                    <input type="hidden" id="portfolio_count" name="portfolio_count" value="1" />
                    <input type="submit" class="save-btn pull-right" name="submit" value="Save" />
                </div>
            </div>
        </div>      
    </form> 
</div>
</div>
<div style="display: none;">
    <div id="portfolio-fields">
        <div class="portfolio-box">
            <div class="row">
                <div class="form-group">
                    <label class="col-sm-4 form-label">Name of Company</label>
                    <div class="col-sm-8">
                        <input type="text" name="portfolio_name" class="form-control form-input" value="" placeholder="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label class="col-sm-4 form-label">Website URL</label>
                    <div class="col-sm-8">
                        <input type="text" name="portfolio_url" class="form-control form-input" placeholder="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label class="col-sm-4 form-label">Location</label>
                    <div class="col-sm-8">
                        <input type="text" name="portfolio_location" class="form-control form-input" placeholder="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label  class="col-sm-4 form-label">Sector</label>
                    <div class="col-sm-8">
                        <div class="select-style">
                            <select class="form-control form-input country" name="portfolio_sector"  placeholder="">
                                <option value="">Sector</option>
                                <?php foreach ($sectors as $sector) { ?>
                                    <option value="<?php echo $sector['id']; ?>" ><?php echo $sector['name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label for="role" class="col-sm-4 form-label">Role</label>
                    <div class="col-sm-8">
                        <input type="text" name="portfolio_role" class="form-control form-input" placeholder="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label  class="col-sm-4 form-label">Your Logo</label>
                    <div class="col-sm-8">
                        <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput">
                            <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                            <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file"  name="portfolio_image"></span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label  class="col-sm-4 form-label">Remarks</label>
                    <div class="col-sm-8">
                        <textarea class="form-control form-input form-area"  name="portfolio_remarks" placeholder=""></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <div class="col-sm-6">
                        <div class="btn btn-gray cancle-portfolio-btn pull-right">Cancle</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div id="company-name-field">
        <div class="form-group">
            <label for="company-name" class="col-sm-4 form-label">Company Name</label>
            <div class="col-sm-8">
                <input type="text" name="company_name" class="form-control form-input" id="company-name" value="" placeholder="">
            </div>
        </div>
    </div>
</div>
<script>
    $('document').ready(function(){
        var portfolio_count = 1;
        $('#investor-info-form').on('change', '.country', function(){
            $.ajax({
                url: "<?php echo base_url(); ?>user/states",
                type: 'POST',
                data : {countryId : $(this).val()}
            })
            .done(function( data ) {
                $('#investor-info-form .state').html(data);
            });
        });
        
       $('#investor-info-form textarea').htmlarea({
            toolbar: [
                        ["bold", "italic", "underline"],
                        ["p"],
                        ["link", "unlink"],
                        ["orderedList","unorderedList"],
                        ["indent","outdent"],
                        ["justifyleft", "justifycenter", "justifyright"] 
                    ]
        });
        
        $('#investor-info-form').on('change', '#type', function(){
            var type = $(this).val();
            if(type == 'angel'){
                $('.company-name-input').html($('#company-name-field').html());
                $('.company-name-input').show();
            }else{
                $('.company-name-input').html('');
                $('.company-name-input').hide();
            }
        });
      
        $('#investor-info-form').validate({
            rules: {
                name: { required : true,
                    minlength:4
                },
                experience: 'required',    
                website:'required',
                country:'required',
                state:'required',
                city:'required',
                year:'required',
                company_name:'required',
                about_company:'required',
                'optional_name[]':'required'
            } ,
            messages: {
                name: {
                    required: "Please provide name",
                    minlength: "Name must be at least 4 characters long"
                },
                experience: {
                    required: "Please enter your experience summary"
                },
                website: {
                    required: "Please provide your website"
                },
                country: 'Please select country',
                state: 'Please select state',
                city: 'Please enter city',
                year:'Please enter year of establishment',
                company_name: "Please enter name of the compnay",
                about_company: "Please enter about your company"
            },
            errorPlacement: function (error, element) {
                if($(element).parent().hasClass("select-style")){
                    $(element).parents('.select-wrap').append(error);
                }else if($(element).attr("type") == 'file'){
                    $(element).parents('.select-wrap').append(error);
                }else{
                    error.insertAfter(element); 
                } 
            }
        });
        
        $('.more-portfolio-btn').on('click',function(){
            ++portfolio_count
            $('#portfolio_count').val(portfolio_count);
           $('#portfolio-fields').find('input[name*="portfolio_name"]').attr('name','portfolio_name_'+portfolio_count);
            $('#portfolio-fields').find('input[name*="portfolio_url"]').attr('name','portfolio_url_'+portfolio_count);
            $('#portfolio-fields').find('input[name*="portfolio_location"]').attr('name','portfolio_location_'+portfolio_count);
            $('#portfolio-fields').find('select[name*="portfolio_sector"]').attr('name','portfolio_sector_'+portfolio_count);
            $('#portfolio-fields').find('input[name*="portfolio_role"]').attr('name','portfolio_role_'+portfolio_count);
            $('#portfolio-fields').find('input[name*="portfolio_image"]').attr('name','portfolio_image_'+portfolio_count);
            $('#portfolio-fields').find('textarea[name*="portfolio_remarks"]').attr('name','portfolio_remarks_'+portfolio_count);
            $('#add-portfolios .portfolios-list').append($('#portfolio-fields').html());
        });
        
        $('.skip-portfolio-btn').on('click',function(){
            $('.investor-register-form .container').hide();
            $('.investor-register-form .save-btn').parents('.row').hide();
            $('.container.interest').show();
            $('.container.interest').next('.row').show();
        });
        
        $('#key-points').tagit({
            availableTags: ['Team','Market','Traction','Production','Business Model','Others'],
            minLength: 2
        });
        
        $('#mentoring-sectors').tagit({
            availableTags: [<?php echo $sector_autocomplete_string; ?>],
            minLength: 2
        });
        $('#expertise').tagit({
            allowSpaces : true
        });
        
        $('#investment-next').on('click',function(){
            if($('#investor-info-form').valid()){
                $(this).hide();
                $('.investor-register-form .container').hide();
                $('.investor-register-form .save-btn').parents('.row').hide();
                $('.container.investment').show();
                $('.container.investment').next('.row').show();
            }
            
        });
        
        $('#portfolio-next').on('click',function(){
            $(this).hide();
            $('.investor-register-form .container').hide();
            $('.investor-register-form .save-btn').parents('.row').hide();
            $('.container.startup-portfolio').show();
            $('.container.startup-portfolio').next('.row').show();
            
        });
        
        $('#interest-next').on('click',function(){
            $(this).hide();
            $('.investor-register-form .container').hide();
            $('.investor-register-form .save-btn').parents('.row').hide();
            $('.container.interest').show();
            $('.container.interest').next('.row').show();
            
        });
        
        $('#add-portfolios').on('click','.cancle-portfolio-btn',function(){
              $(this).parents('.portfolio-box').remove();
              portfolio_count--;
              $('#portfolio_count').val(portfolio_count);
        });
        
         $('.section-head').on('click',function(){
            $(this).parents('.row').next('.container').toggle();
            $(this).parents('.row').next('.container').next('.row').toggle();
        });
    });
</script>