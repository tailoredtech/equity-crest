<div class="main">
    <div class="container wrap">
        <div class="row">
            <div class="col-sm-3">
            <h4 class="form-header  ">Sign In</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="login-form">
                    <?php if($this->session->flashdata('success-msg')){ ?>
                    <div class="row failure-msg">
                        <div class="col-sm-10 col-md-offset-1">
                            <div class="alert alert-success" role="alert">
                                <?php echo $this->session->flashdata('success-msg'); ?>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if($this->session->flashdata('error-msg')){ ?>
                    <div class="row failure-msg">
                        <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-danger" role="alert">
                                <?php echo $this->session->flashdata('error-msg'); ?>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                  <form method="post" id="login-form" action="<?php echo base_url(); ?>user/login_process">
                    <div class=" col-sm-8 col-sm-offset-1 ">
                        <div class="row">
                            <div class="form-group">
                                <label for="email" class="col-sm-4 form-label  ">Email ID</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control form-input" id="email" name="email" placeholder="">
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label for="password" class="col-sm-4 form-label">Password</label>
                                <div class="col-sm-8">
                                    <input type="password" class="form-control form-input" id="password" name="password" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-5 col-sm-offset-4">
                                <div class="checkbox" style="margin-top: 0px;margin-bottom: 20px">
                                    <label>
                                        <input type="checkbox" name="remember"> Keep me logged in
                                    </label>
                                </div>  
                            </div>
                            <div class="join-now col-sm-3 pull-left">
                                <input type="submit" value="Sign In" name="submit" class="join-btn col-sm-12 pull-right lato-regular" style="margin-right: 0px">
<!--                                <button class="join-btn col-sm-12 pull-right ">Sign In</button>-->
                            </div>
                        </div>
                        <div class="row space-10">
                            <div class="col-sm-4 col-sm-offset-4">
                                <a href="javascript:void(0);" class="frgt-pasw-link">Forgot Password?</a>
                            </div>
                            <div class="join-now col-sm-4 pull-left">
                                Not a Member? <a id="join" class="lato-bold" href="<?php echo base_url(); ?>user/register">Join Now</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-offset-2 social-or"></div>
                            <div class="row social-sign-up">
                                    <div class="col-sm-8 col-sm-offset-4">
                                            <p class=" pull-left social-head">Quick Sign in using</p>
                                            <div class=" pull-left social-ico">
                                                    <a href="<?php echo base_url(); ?>user/linkedin_login" class="social-reg" id="linkedin">Linked-in</a>
                                                    <a href="<?php echo base_url(); ?>user/googleplus_login" class="social-reg" id="google">Google</a>
                                                    <a href="<?php echo base_url(); ?>user/fb_login" class="social-reg" id="facebook">Facebook</a>
                                            </div>
                                    </div>
                            </div>
                        </div>
                        

                    </div>
                  </form>
                </div>
            </div>
        </div>
    </div>  

</div>
<!-- POPUP CONTAINER -->
<div style="display: none;">
    <div class="pop-content" id="forgotpsw-pop">
        <h3 class="form-header">Forgot Password</h3>
        <div class="askexpert">
            <form id="forgot-psw-form" method="post">
                <div class="form-group">
                    <label for="founder-name" class="col-sm-2 form-label black lato-bold ">Email</label>
                    <div class="col-sm-10">
                        <input type="text" name="email" value="" class="form-control form-input"  placeholder="Email">
                    </div>
                </div>

                <div class="buttons">
                    <input type="submit" value="Send" class="btn btn-green" />
                </div>
            </form>
        </div>

    </div>
    <div class="pop-content" id="verify-pop">
        <h3 class="form-header">Verification Mail</h3>
        <div class="askexpert">
        <h5>New verification mail has been sent. Please also check your spam folder for mail</h5>
        </div>
    </div>
</div>
<!-- FOOTER CONTAINER -->
<script type="text/javascript">
        $(document).ready(function(){
            $('#login-form').validate({
                rules: {
                              
                            email: { required : true,
                                    email: true
                                },
                            password: 'required'
                        }
            });
          
          $('.frgt-pasw-link').on('click',function(){
              $.colorbox({inline: true, href: '#forgotpsw-pop', innerWidth: '400px', innerHeight: '175px'});
          });
          
          $("#forgot-psw-form").on('submit', (function(e) {
                    e.preventDefault();
                    $.ajax({
                        url: "<?php echo base_url(); ?>user/send_pass_reset_link",
                        type: 'POST',
                        data: new FormData(this),
                        contentType: false,
                        cache: false,
                        processData: false
                    })
                    .done(function(data) {
                        if (data == 'success') {
                           
                        } else {
                            console.log('error');
                        }
                        $.colorbox.close();
                    });

                }));
                
                //$.colorbox({inline: true, href: '#verify-pop', innerWidth: '400px', innerHeight: '100px'});
            $('#verify').on('click',function(){
                var id = $(this).data("id");
                console.log(id);
                $.post("<?php echo base_url(); ?>user/verify_again",{id:id}).done(
                function(){
                $.colorbox({inline: true, href: '#verify-pop', innerWidth: '400px', innerHeight: '100px'});
                });
              
          });
        });
    </script>


        