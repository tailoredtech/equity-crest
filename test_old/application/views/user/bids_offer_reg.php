<div class="main">
    <div class="container form-contain">
        <div class="row">
            <h4 class="form-header col-sm-12 lato-regular">Offer Registration</h4>
        </div>
    </div>
    <form id="bids-offer-form" action="<?php echo base_url(); ?>user/bids_offer_reg_process" enctype="multipart/form-data" method="post" >
        <div class="container">
            <div class="row content-box">
                <div class="container">
                    <div clas="row">
                        <div class="col-sm-7 col-sm-offset-2">


                            <div class="name-fields">

                                <div class="row">
                                    <div class="form-group">
                                        <label for="name" class="col-sm-3 form-label">Company Name *</label>
                                        <div class="col-sm-9">
                                            <div class="select-style">
                                                <select class="form-control form-input" id="company" name="company" placeholder="Select Company">
                                                    <?php foreach ($companies as $company) { ?>
                                                        <option value='<?php echo $company['id']; ?>'><?php echo $company['company_name']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="sector" class="col-sm-3 form-label">Sector</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="sector" class="form-control " id="sector" placeholder="" required>
                                        </div>
                                    </div>
                                </div>               
                                <div class="row">
                                    <div class="form-group">
                                        <label for="shares" class="col-sm-3 form-label">No of shares</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="shares" class="form-control " id="shares" placeholder="" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="holdings" class="col-sm-3 form-label">% holdings</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="holdings" class="form-control " id="holdings" placeholder="" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="amt" class="col-sm-3 form-label">Amount / Share</label>
                                        <div class="col-sm-9">
                                            <span class="col-sm-1 rupee-placeholder-bids "> <img src='<?php echo base_url(); ?>assets/images/rupee.png'style="margin-top: 9px; margin-left: -2px;"></span>
                                            <input type="text" name="amt" class="form-control rupee-input" id="amt" placeholder="" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="total_amt" class="col-sm-3 form-label">Total Amount</label>
                                        <div class="col-sm-9">
                                            <span class="col-sm-1 rupee-placeholder-bids "> <img src='<?php echo base_url(); ?>assets/images/rupee.png' style="margin-top: 9px; margin-left: -2px;"></span>
                                            <input type="text" name="total_amt" class="form-control rupee-input" id="name" placeholder="" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="join-now col-sm-6">
                                <input type="submit" value="Submit" name="submit" class="join-btn col-sm-12 pull-right lato-regular">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<?php 
 $sectors=array();
foreach ($companies as $key=>$company):
    
    $sectors[$key]['id']=$company['id'];
    $sectors[$key]['sector']=$company['sector_name'];
endforeach;
 $sectorList=json_encode($sectors);
?>
<script>
    var sector = <?php echo $sectorList;?>;
    
    
</script>
