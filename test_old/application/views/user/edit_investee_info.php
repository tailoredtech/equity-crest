<?= js('jquery.datetimepicker.js') ?>
<?= css('jquery.datetimepicker.css') ?>
<div class="main">
    <div class="container wrap">
        <div class="row">
            <h4 class="form-header  col-xs-12 ">Company Information</h4>
        </div>
        <form id="investee-info-form" action="<?php echo base_url(); ?>user/edit_investee_info_process" enctype="multipart/form-data" method="post" >
            <div class="container investor-register-form">
                <div class="row">
                    <div class="col-md-10 section-head">
                        <span class="glyphicon glyphicon-chevron-down dropicon"></span>
                        <span class="lato-bold ">Company Info </span>
                    </div>
                     
                </div>
                <div class="container company-info  col-sm-7 col-sm-offset-2 " style="<?php echo ($activePanel == '') ? '' : 'display: none;' ?>">
                    <div class="row">
                        <div class="form-group">
                            <label for="founder-name" class="col-sm-4 form-label black lato-bold ">Founder Name</label>
                            <div class="col-sm-8">
                                <input type="text" name="name" value="<?php echo $user['name']; ?>" class="form-control form-input" id="founder-name" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="sign-in-as" class="col-sm-4 form-label black lato-bold ">Name of the Company</label>
                            <div class="col-sm-8">
                                <input type="text" name="company_name" class="form-control form-input" id="company_name" value="<?php echo $user['company_name']; ?>" placeholder="">
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="company_url" class="col-sm-4 form-label">Company URL</label>
                            <div class="col-sm-8">
                                <input type="text" name="company_url" value="<?php echo $user['company_url']; ?>" class="form-control form-input" id="company_url" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="product_url" class="col-sm-4 form-label">Product Website URL</label>
                            <div class="col-sm-8">
                                <input type="text" name="product_url" value="<?php echo $user['product_url']; ?>" class="form-control form-input" id="product_url" placeholder="www">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="country" class="col-sm-4 form-label">Country</label>
                            <div class="col-sm-8">
                                <div class="select-style">
                                    <select class="form-control form-input country" name="country"  placeholder="">
                                        <option value="">Select Country</option>
                                        <?php foreach ($countries as $country) { ?>
                                            <option value="<?php echo $country['id']; ?>" <?php echo ($country['id'] == $user['country'] ) ? 'selected' : '' ?>><?php echo $country['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="state" class="col-sm-4 form-label">State</label>
                            <div class="col-sm-8">
                                <div class="select-style">
                                    <select class="form-control form-input state" name="state"  placeholder="">
                                        <?php foreach ($states as $state) { ?>
                                            <option value="<?php echo $state['id']; ?>" <?php echo ($state['id'] == $user['state'] ) ? 'selected' : '' ?>><?php echo $state['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="city" class="col-sm-4 form-label">City</label>
                            <div class="col-sm-8">
                                <input type="text" name="city" value="<?php echo $user['city']; ?>" class="form-control form-input" id="city" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="year" class="col-sm-4 form-label">Founding Year</label>
                            <div class="col-sm-8">
                                <input type="text" name="year" value="<?php echo $user['year']; ?>" class="form-control form-input" id="year" placeholder="">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label for="stage" class="col-sm-4 form-label">Stage</label>
                            <div class="col-sm-8">
                                <div class="select-style">
                                    <select class="form-input" id="stage" name="stage" placeholder="Select Startup Stage">
                                        <?php foreach ($stages as $stage) { ?>
                                            <option value='<?php echo $stage['id']; ?>' <?php echo ($stage['id'] == $user['stage'] ) ? 'selected' : '' ?>><?php echo $stage['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="sector" class="col-sm-4 form-label">Sector</label>
                            <div class="col-sm-8">
                                <div class="select-style">
                                    <select class="form-control form-input" id="sector" name="sector" placeholder="Select Sector">
                                        <?php foreach ($sectors as $sector) { ?>
                                            <option value='<?php echo $sector['id']; ?>' <?php echo ($sector['id'] == $user['sector'] ) ? 'selected' : '' ?>><?php echo $sector['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="business_type" class="col-sm-4 form-label">Type of Business</label>
                            <div class="col-sm-8">
                                <div class="select-style">
                                    <select class="form-control form-input" id="business_type" name="business_type" placeholder="Select Business Type">
                                        <?php foreach ($types as $type) { ?>
                                            <option value='<?php echo $type['id']; ?>' <?php echo ($type['id'] == $user['business_type'] ) ? 'selected' : '' ?>><?php echo $type['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="fb-url" class="col-sm-4 form-label">Facebook</label>
                            <div class="col-sm-8">
                                <input type="text" name="fb_url" value="<?php echo $user['fb_url']; ?>" class="form-control form-input" id="fb-url" placeholder="Facebook URL">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="linkedin-url" class="col-sm-4 form-label">Linkedin</label>
                            <div class="col-sm-8">
                                <input type="text" name="linkedin_url" value="<?php echo $user['linkedin_url']; ?>" class="form-control form-input" id="linkedin-url" placeholder="Linkedin URL">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="twitter-handle" class="col-sm-4 form-label">Twitter</label>
                            <div class="col-sm-8">
                                <input type="text" name="twitter_handle" value="<?php echo $user['twitter_handle']; ?>" class="form-control form-input" id="twitter-handle" placeholder="Twitter handle">
                            </div>
                        </div>
                    </div>
                   
                    

                </div>
                <div class="row" style="<?php echo ($activePanel == '') ? '' : 'display: none;' ?>">
                    <div class="col-sm-6">
                        <input type="submit" name="company_submit"  class="join-btn pull-right" value="Save" />
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12"><hr></div>
                </div>
                <div class="row">
                    <div class="col-md-10 section-head">
                        <span class="glyphicon glyphicon-chevron-down dropicon dropicon"></span><span class="lato-bold">Team Summary</span>
                    </div>
                </div>
                
                <div class="container section wrap" style="<?php echo ($activePanel == 'team') ? '' : 'display: none;' ?>">
                    <div class="row">
                        <div class="col-sm-7 col-sm-offset-2">
                            <div class="row">
                                <div class="form-group">
                                    <label for="team-summary" class="col-sm-4 form-label">Team Summary</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control form-input form-area" cols="50" rows="15" id="team-summary" name="team_summary"  placeholder=""><?php echo $user['team_summary']; ?></textarea>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 pull-right">
                            <span class="glyphicon glyphicon-cog privacy-icon"></span>
                            <div class="privacy-options" style="display:none">
                                <div class="select-style-privacy">
                                <select name="team">
                                    <option value="" disabled selected>Visible to</option>
                                    <option value="0" <?php echo ($privacy['team'] == 0) ? 'selected' : ''; ?>>All</option>
                                    <option value="1" <?php echo ($privacy['team'] == 1) ? 'selected' : ''; ?>>All Investors & Partners</option>
                                    <option  value="2" <?php echo ($privacy['team'] == 2) ? 'selected' : ''; ?>>Pledged Investors</option>
                                    <option  value="3" <?php echo ($privacy['team'] == 3) ? 'selected' : ''; ?>>Followers</option>
                                    <option  value="4" <?php echo ($privacy['team'] == 4) ? 'selected' : ''; ?>>On Request</option>
                                </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="<?php echo ($activePanel == 'team') ? '' : 'display: none;' ?>">
                    <div class="col-sm-6">
                        <input type="submit" name="team_submit"  class="join-btn pull-right" value="Save" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12"><hr></div>
                </div>
                <div class="row">
                    <div class="col-md-10 section-head">
                        <span class="glyphicon glyphicon-chevron-down dropicon dropicon"></span><span class="lato-bold">Business</span>
                    </div>
                </div>
                <div class="container section wrap" style="<?php echo ($activePanel == 'business') ? '' : 'display: none;' ?>">
                    <div class="row">
                        <div class="col-sm-7 col-sm-offset-2">
                            <div class="row">
                                <div class="form-group">
                                    <label for="products-services" class="col-sm-4 form-label">Business Description</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control form-input form-area"   id="products-services" name="products_services"  placeholder=""><?php echo $user['products_services']; ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label for="how-different" class="col-sm-4 form-label">Unique Selling Proposition</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control form-input form-area"  id="how-different" name="how_different"  placeholder=""><?php echo $user['how_different']; ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 pull-right">
                            <span class="glyphicon glyphicon-cog privacy-icon"></span>
                            <div class="privacy-options" style="display: none;">
                                
                                <div class="select-style-privacy">
                                <select name="business">
                                    <option value="0" <?php echo ($privacy['business'] == 0) ? 'selected' : ''; ?>>All</option>
                                    <option value="1" <?php echo ($privacy['business'] == 1) ? 'selected' : ''; ?>>All Investors & Partners</option>
                                    <option  value="2" <?php echo ($privacy['business'] == 2) ? 'selected' : ''; ?>>Pledged Investors</option>
                                    <option  value="3" <?php echo ($privacy['business'] == 3) ? 'selected' : ''; ?>>Followers</option>
                                    <option  value="4" <?php echo ($privacy['business'] == 4) ? 'selected' : ''; ?>>On Request</option>
                                </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="<?php echo ($activePanel == 'business') ? '' : 'display: none;' ?>">
                    <div class="col-sm-6">
                        <input type="submit" name="business_submit"  class="join-btn pull-right" value="Save" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12"><hr></div>
                </div>
                <div class="row">
                    <div class="col-md-10 section-head">
                        <span class="glyphicon glyphicon-chevron-down dropicon dropicon"></span><span class="lato-bold">Revenue Model</span>
                    </div>
                </div>
                <div class="container section wrap" style="<?php echo ($activePanel == 'revenue') ? '' : 'display: none;' ?>">
                    <div class="row">
                        <div class="col-sm-7 col-sm-offset-2">
                            <div class="row">
                                <div class="form-group">
                                    <label for="how-we-make-money" class="col-sm-4 form-label">Revenue Model</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control form-input form-area" id="how-we-make-money" name="how_we_make_money"  placeholder=""><?php echo $user['how_we_make_money']; ?></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group">
                                    <label for="customer-traction" class="col-sm-4 form-label">Revenue Traction Summary</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control form-input form-area" id="customer-traction" name="customer_traction"  placeholder=""><?php echo $user['customer_traction']; ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 pull-right">
                            <span class="glyphicon glyphicon-cog privacy-icon"></span>
                            <div class="privacy-options" style="display: none;">
                                
                                <div class="select-style-privacy">
                                <select name="monetization">
                                    <option value="0" <?php echo ($privacy['monetization'] == 0) ? 'selected' : ''; ?>>All</option>
                                    <option value="1" <?php echo ($privacy['monetization'] == 1) ? 'selected' : ''; ?>>All Investors & Partners</option>
                                    <option  value="2" <?php echo ($privacy['monetization'] == 2) ? 'selected' : ''; ?>>Pledged Investors</option>
                                    <option  value="3" <?php echo ($privacy['monetization'] == 3) ? 'selected' : ''; ?>>Followers</option>
                                    <option  value="4" <?php echo ($privacy['monetization'] == 4) ? 'selected' : ''; ?>>On Request</option>
                                </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="<?php echo ($activePanel == 'revenue') ? '' : 'display: none;' ?>">
                    <div class="col-sm-6">
                        <input type="submit" name="revenue_submit"  class="join-btn pull-right" value="Save" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12"><hr></div>
                </div>
                <div class="row">
                    <div class="col-md-10 section-head">
                        <span class="glyphicon glyphicon-chevron-down dropicon dropicon"></span><span class="lato-bold">Market</span>
                    </div>
                </div>
                <div class="container section wrap" style="<?php echo ($activePanel == 'market') ? '' : 'display: none;' ?>">
                    <div class="row">
                        <div class="col-sm-7 col-sm-offset-2">
                            <div class="row">
                                <div class="form-group">
                                    <label for="addressable-market" class="col-sm-4 form-label">Total Addressable Market</label>
                                    <div class="col-sm-8">
                                        <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                        <input type="text" class="form-control form-input rupee-input" id="addressable-market" name="addressable_market" value="<?php echo rupeeFormat($user['addressable_market']); ?>" placeholder="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label for="competition" class="col-sm-4 form-label">Competition</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control form-input form-area" id="competition" name="competition"  placeholder=""><?php echo $user['competition']; ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 pull-right">
                            <span class="glyphicon glyphicon-cog privacy-icon"></span>
                            <div class="privacy-options" style="display: none;">
                                
                                <div class="select-style-privacy">
                                <select name="market">
                                    <option value="0" <?php echo ($privacy['market'] == 0) ? 'selected' : ''; ?>>All</option>
                                    <option value="1" <?php echo ($privacy['market'] == 1) ? 'selected' : ''; ?>>All Investors & Partners</option>
                                    <option  value="2" <?php echo ($privacy['market'] == 2) ? 'selected' : ''; ?>>Pledged Investors</option>
                                    <option  value="3" <?php echo ($privacy['market'] == 3) ? 'selected' : ''; ?>>Followers</option>
                                    <option  value="4" <?php echo ($privacy['market'] == 4) ? 'selected' : ''; ?>>On Request</option>
                                </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="<?php echo ($activePanel == 'market') ? '' : 'display: none;' ?>">
                    <div class="col-sm-6">
                        <input type="submit" name="market_submit"  class="join-btn pull-right" value="Save" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12"><hr></div>
                </div>
                <div class="row">
                    <div class="col-md-10 section-head">
                        <span class="glyphicon glyphicon-chevron-down dropicon dropicon"></span><span class="lato-bold">Raise</span>
                    </div>
                </div>
                <div class="container section wrap" style="<?php echo ($activePanel == 'raise') ? '' : 'display: none;' ?>">
                    <div class="row">
                        <div class="col-sm-7 col-sm-offset-2">

                            
                            
                            <div class="row">
                                <div class="form-group">
                                    <label for="investment-required" class="col-sm-4 form-label">Investment Required</label>
                                    <div class="col-sm-8">
                                        <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                        <input  class="form-control form-input rupee-input" id="investment-required" name="investment_required" value="<?php echo rupeeFormat($user['investment_required']); ?>" placeholder="Ex. 5,00,00,000">
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label for="commitment-per-investor" class="col-sm-4 form-label">Minimum Commitment Per User</label>
                                    <div class="col-sm-8">
                                        <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                        <input  class="form-control form-input rupee-input" id="commitment-per-investor" name="commitment_per_investor" value="<?php echo rupeeFormat($user['commitment_per_investor']); ?>" placeholder="Ex. 5,00,000">
                                        
                                    </div>
                                </div>
                            </div>  
                            <div class="row">
                                <div class="form-group">
                                    <label for="equity-offered" class="col-sm-4 form-label">Equity Offered</label>
                                    <div class="col-sm-3">

                                        <input  class="form-control form-input" id="equity-offered" name="equity_offered" value="<?php echo $user['equity_offered']; ?>" maxlength="2" placeholder="">
                                        <span class="col-sm-3 input-placeholder   pull-right">%</span>


                                    </div>

                                    <label for="validity-period" class="col-sm-2 form-label">Validity Period</label>
                                    <div class="col-sm-3">

                                        <input  class="form-control form-input" style="padding: 6px" id="validity-period" name="validity_period" value="<?php echo $user['validity_period']; ?>" placeholder="">
                                        <span class="col-sm-3 input-placeholder   pull-right calender"></span>


                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label for="funding_history" class="col-sm-4 form-label">Prior Fund Raise History</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control form-input form-area" id="funding_history" name="funding_history"  placeholder=""><?php echo $user['funding_history']; ?></textarea>
                                    </div>
                                </div>
                            </div>

                            <fieldset >
                                <legend >Use of Funds</legend>
                                <div id="fund-uses">
                                    <?php
                                    $count = count($purposes);
                                    $i = 0;
                                    foreach ($purposes as $purpose) {
                                        ?>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-sm-6">
                                                    <input type="text" name="purpose[]" value="<?php echo $purpose['purpose']; ?>" class="form-control form-input"  placeholder="Purpose">
                                                </div>
                                                <div class="col-sm-6">
                                                    <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                                    <input type="text" name="purpose_amount[]" value="<?php echo rupeeFormat($purpose['amount']); ?>"  class="form-control form-input rupee-input"  placeholder="Amount Ex. 1000000 ">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <?php $i++;
                                    } ?>
<?php $j = 5 - $i;
for ($i = 1; $i <= $j; $i++) {
    ?>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-sm-6">
                                                    <input type="text" name="purpose[]" class="form-control form-input"  placeholder="Purpose">
                                                </div>
                                                <div class="col-sm-6">
                                                    <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                                    <input type="text" name="purpose_amount[]" class="form-control form-input rupee-input"  placeholder="Amount Ex. 1000000 ">
                                                    
                                                </div>
                                            </div>
                                        </div>
<?php }
?>
                                </div>
                                <!--                        <div class="btn join-btn col-sm-12 pull-right add-purpose-btn">Add More</div>-->
                            </fieldset>
                        </div>
                        <div class="col-sm-3 pull-right">
                            <span class="glyphicon glyphicon-cog privacy-icon"></span>
                            <div class="privacy-options" style="display: none;">
                                
                                <div class="select-style-privacy">
                                <select name="raise">
                                    <option value="0" <?php echo ($privacy['raise'] == 0) ? 'selected' : ''; ?>>All</option>
                                    <option value="1" <?php echo ($privacy['raise'] == 1) ? 'selected' : ''; ?>>All Investors & Partners</option>
                                    <option  value="2" <?php echo ($privacy['raise'] == 2) ? 'selected' : ''; ?>>Pledged Investors</option>
                                    <option  value="3" <?php echo ($privacy['raise'] == 3) ? 'selected' : ''; ?>>Followers</option>
                                    <option  value="4" <?php echo ($privacy['raise'] == 4) ? 'selected' : ''; ?>>On Request</option>
                                </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="<?php echo ($activePanel == 'raise') ? '' : 'display: none;' ?>">
                    <div class="col-sm-6">
                        <input type="submit" name="raise_submit"  class="join-btn pull-right" value="Save" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12"><hr></div>
                </div>
                <div class="row">
                    <div class="col-md-10 section-head">
                        <span class="glyphicon glyphicon-chevron-down dropicon dropicon"></span><span class="lato-bold">Financials</span>
                    </div>
                </div>
                <div class="container section wrap" style="<?php echo ($activePanel == 'financial') ? '' : 'display: none;' ?>">
                    <div class="row">
                        <div class="col-sm-7 col-sm-offset-2">
                            <fieldset >
                                <legend >Monthly Financial Indicators</legend>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="monthly-revenue" class="col-sm-4 form-label">Revenues</label>
                                        <div class="col-sm-8">
                                            <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                            <input type="text" name="monthly_revenue" value="<?php echo rupeeFormat($user['monthly_revenue']); ?>"  class="form-control form-input rupee-input" id="monthly-revenue" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="fixed-opex" class="col-sm-4 form-label">Fixed Cost (OPEX)</label>
                                        <div class="col-sm-8">
                                            <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                            <input type="text" name="fixed_opex" value="<?php echo rupeeFormat($user['fixed_opex']); ?>"  class="form-control form-input rupee-input" id="fixed-opex" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="cash-burn" class="col-sm-4 form-label">Cash Burn</label>
                                        <div class="col-sm-8">
                                            <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                            <input type="text" name="cash_burn" value="<?php echo rupeeFormat($user['cash_burn']); ?>"  class="form-control form-input rupee-input" id="cash-burn" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="debt" class="col-sm-4 form-label">Debt</label>
                                        <div class="col-sm-8">
                                            <span class="col-sm-1 rupee-placeholder "> <img src='<?php echo base_url();?>assets/images/rupee.png'></span>
                                            <input type="text" name="debt" value="<?php echo rupeeFormat($user['debt']); ?>"  class="form-control form-input rupee-input" id="debt" placeholder="">
                                        </div>
                                    </div>
                                </div>

                            </fieldset>
                        </div>
                        <div class="col-sm-3 pull-right">
                            <span class="glyphicon glyphicon-cog privacy-icon"></span>
                            <div class="privacy-options" style="display: none;">
                                
                                <div class="select-style-privacy">
                                <select name="financial">
                                    <option value="0" <?php echo ($privacy['financial'] == 0) ? 'selected' : ''; ?>>All</option>
                                    <option value="1" <?php echo ($privacy['financial'] == 1) ? 'selected' : ''; ?>>All Investors & Partners</option>
                                    <option  value="2" <?php echo ($privacy['financial'] == 2) ? 'selected' : ''; ?>>Pledged Investors</option>
                                    <option  value="3" <?php echo ($privacy['financial'] == 3) ? 'selected' : ''; ?>>Followers</option>
                                    <option  value="4" <?php echo ($privacy['financial'] == 4) ? 'selected' : ''; ?>>On Request</option>
                                </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>   
                <div class="row" style="<?php echo ($activePanel == 'financial') ? '' : 'display: none;' ?>">
                    <div class="col-sm-6">
                        <input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
                        <input type="submit" class="join-btn pull-right" name="financial_submit" value="Save"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12"><hr></div>
                </div>
                <div class="row">
                    <div class="col-sm-offset-5 col-md-3">
                        <a href="<?php echo base_url(); ?>user/edit_file_upload" class="join-btn btn">Next</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>    
<div style="display: none;">
    <div id="fund-uses-fields">
        <div class="row">
            <div class="form-group">
                <div class="col-sm-6">
                    <input type="text" name="purpose[]" class="form-control form-input"  placeholder="Purpose">
                </div>
                <div class="col-sm-6">
                    <input type="text" name="purpose_amount[]" class="form-control form-input"  placeholder="Amount Ex. 1000000 ">
                    <span class="col-sm-2 input-placeholder   pull-right">(INR)</span>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#investee-info-form textarea').htmlarea({
            toolbar: [
                ["bold", "italic", "underline"],
                ["p"],
                ["link", "unlink"],
                ["orderedList", "unorderedList"],
                ["indent", "outdent"],
                ["justifyleft", "justifycenter", "justifyright"]
            ]
        });
                
        $('#validity-period').datetimepicker({
            timepicker:false,
            format:'d-m-Y'
        });

        $('#investee-info-form').on('change', '.country', function(){
            $.ajax({
                url: "<?php echo base_url(); ?>user/states",
                type: 'POST',
                data : {countryId : $(this).val()}
            })
            .done(function( data ) {
                $('#investee-info-form .state').html(data);
            });
        });
        
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");
        
        jQuery.validator.addMethod("rupeeonly", function(value, element) {
            return this.optional(element) || /^[-,0-9]+$/i.test(value);
        }, "Only digits and comma");
        
        $('#investee-info-form').validate({
            rules: {
                        
                company_name: { required : true,
                    minlength:4
                },
                company_url:'required',
                product_url:'required',
                year:'required',
                country:'required',
                state:'required',
                city:{ required : true,
                    lettersonly:true
                },
                image:'required',
                products_services:'required',
                investment_required: { required : true,
                                       rupeeonly: true
                },
                funding_history:'required',
                commitment_per_investor: { required : true,
                                           rupeeonly: true
                },
                valuation: { required : true,
                    number: true
                },
                equity_offered: { 
                    number: true
                },
                team_summary:'required',
                customer_traction:'required',
                how_different:'required',
                how_we_make_money:'required',
                addressable_market: {
                    rupeeonly: true
                },
                competition:'required',
                uses_of_funds:'required',
                costs_margins:'required'
            },
            messages: {
                city: {
                    required: "This field is required."
                }
            } ,
            errorPlacement: function (error, element) {
                if($(element).parent().hasClass("select-style")){
                    $(element).parents('.col-sm-8').append(error);
                }else if($(element).attr("type") == 'file'){
                    $(element).parents('.col-sm-8').append(error);
                }else{
                    error.insertAfter(element); 
                } 
            }
        });
        
        $('textarea').keydown(function(event){
            if (event.keyCode == 13) {
                event.preventDefault();
                this.value = this.value + "\n";
            }
        });
        
        $('.add-purpose-btn').on('click',function(){
            $('#fund-uses').append($('#fund-uses-fields').html());
        });
        
        $('.section-head').on('click',function(){
            $(this).parents('.row').next('.container').toggle();
            $(this).parents('.row').next('.container').next('.row').toggle();
        });
        
        $('.glyphicon-cog').on('click',function(){
            $(this).next('.privacy-options').toggle();
        });
        
        $('.privacy-options select').on('change',function(){
             $.ajax({
                url: "<?php echo base_url(); ?>investee/save_privacy",
                type: 'POST',
                data : {optionVal : $(this).val(),optionKey :$(this).attr('name') }
            })
            .done(function( data ) {
                
            });
        });
    });
</script>