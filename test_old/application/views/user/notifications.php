<?php foreach($notifications as $notification){ ?>
    <li>
        <a class="notify-url" href="<?php echo base_url().$notification['link']; ?>">
        <div class="notification">
            <span class="notification-thumb img-thumbnail" style="background-image: url('<?php echo base_url(); ?>uploads/users/<?php echo $notification['sender_id'] ?>/<?php echo $notification['image']; ?>');  background-size: 100%; "></span> 
            <span class="notification-text"><?php echo $notification['display_text']; ?></span>
            <span class="notification-time"><?php echo date('M, d H:i A',strtotime($notification['date_created'])); ?></span>
            <span class="border"></span>
        </div>
        </a>
    </li>
<?php } ?>
    <li>
        <a href="<?php echo base_url().$this->session->userdata('role')  ?>/notifications">View All</a>
    </li>
