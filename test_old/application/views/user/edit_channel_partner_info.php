<?=css('jquery.tagit.css')?>
<?=css('tagit.ui-zendesk.css')?>

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
<?=js('tag-it.js')?>

<div class="main">
<div class="container wrap">
    <div class="row">
        <h4 class="form-header col-lg-12 col-md-12 col-sm-12 col-xs-12 ">Channel Partner Profile Update</h4>
    </div>

    <form id="investor-info-form" action="<?php echo base_url(); ?>user/edit_channel_partner_info_process" enctype="multipart/form-data" method="post" >
        <div class="container investor-register-form">
            <div class="row">
                <div class="col-md-12 section-head">
                    <span class="glyphicon glyphicon-chevron-down dropicon"></span><span class="lato-bold ">Personal Information</span>
                </div>
            </div>
            <div class="container personal-info  col-sm-7 col-sm-offset-2">
                
                
                <div class="row">
                    <div class="form-group">
                        <label for="name" class="col-sm-3 form-label">Name</label>
                        <div class="col-sm-9">
                            <input type="text" name="name" class="form-control form-input" id="name" value="<?php echo $user['name']; ?>" placeholder="">
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="form-group">
                        <label for="company-name" class="col-sm-3 form-label">Company Name</label>
                        <div class="col-sm-9">
                            <input type="text" name="company_name" class="form-control form-input" id="company-name" value="<?php echo $user['company_name']; ?>" placeholder="">
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="form-group">
                        <label for="experience" class="col-sm-3 form-label">Experience Summary</label>
                        <div class="col-sm-9">
                            <textarea class="form-control form-input form-area" name="experience" id="experience" placeholder=""><?php echo $user['experience']; ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label for="role" class="col-sm-3 form-label">Role</label>
                        <div class="col-sm-9">
                            <input type="text" name="role" value="<?php echo $user['role']; ?>" class="form-control form-input" id="role" placeholder="Ex. CEO,Founder">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label for="fb-url" class="col-sm-3 form-label">Facebook</label>
                        <div class="col-sm-9">
                            <input type="text" name="fb_url" value="<?php echo $user['fb_url']; ?>" class="form-control form-input" id="fb-url" placeholder="Facebook URL">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label for="linkedin-url" class="col-sm-3 form-label">Linkedin</label>
                        <div class="col-sm-9">
                            <input type="text" name="linkedin_url" value="<?php echo $user['linkedin_url']; ?>" class="form-control form-input" id="linkedin-url" placeholder="Linkedin URL">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label for="twitter-handle" class="col-sm-3 form-label">Twitter</label>
                        <div class="col-sm-9">
                            <input type="text" name="twitter_handle" value="<?php echo $user['twitter_handle']; ?>" class="form-control form-input" id="twitter-handle" placeholder="Twitter handle">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label for="country" class="col-sm-3 form-label">Country</label>
                        <div class="col-sm-9">
                            <div class="select-style">
                                <select class="form-control form-input country" name="country"  placeholder="">
                                    <option value="">Select Country</option>
                                    <?php foreach ($countries as $country) { ?>
                                        <option value="<?php echo $country['id']; ?>" <?php echo ($country['id'] == $user['country'] ) ? 'selected' : '' ?> ><?php echo $country['name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label for="state" class="col-sm-3 form-label">State</label>
                        <div class="col-sm-9">
                            <div class="select-style">
                                <select class="form-control form-input state" name="state"  placeholder="">
                                     <?php foreach ($states as $state) { ?>
                                            <option value="<?php echo $state['id']; ?>" <?php echo ($state['id'] == $user['state'] ) ? 'selected' : '' ?>><?php echo $state['name']; ?></option>
                                      <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label for="city" class="col-sm-3 form-label">City</label>
                        <div class="col-sm-9">
                            <input type="text" name="city" value="<?php echo $user['city']; ?>" class="form-control form-input" id="city" placeholder="">
                        </div>
                    </div>
                </div>
         
                <div class="row">
                    <div class="form-group">
                        <label for="mentoring-sectors" class="col-sm-3 form-label">Sector of Expertise</label>
                        <div class="col-sm-9">
                            <div class="select-style">
                                <select class="form-control form-input country" name="sector_expertise[]"  placeholder="" multiple>
                                    <option value="">Select Multiple Sectors</option>
                                    <?php
                                    $sector_autocomplete_string = '';
                                    $sector_expertise = explode(",", $user['sector_expertise']);
                                    foreach ($sectors as $sector) {
                                        $sector_autocomplete_string .= '"' . $sector['name'] . '",';
                                        ?>
                                        <option value="<?php echo $sector['name']; ?>" <?php echo (in_array($sector['name'], $sector_expertise)) ? 'selected' : ''; ?> ><?php echo $sector['name']; ?></option>
                                    <?php
                                    }
                                    $sector_autocomplete_string = trim($sector_autocomplete_string, ",");
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label for="expertise" class="col-sm-3 form-label">Area of Expertise</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control form-input form-area" id="expertise" name="expertise" value="<?php echo $user['area_of_expertise'] ?>" placeholder="" />
                        </div>
                    </div>
                </div>

             
            <div class="row">
                <div class="col-sm-6">
                    <input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
                    <input type="submit" class="join-btn  pull-right" name="submit" value="Save" />
                </div>
            </div>
          </div>
        </div>      
    </form> 
</div>
</div>

<script>
    $('document').ready(function(){
        
        $('#investor-info-form').on('change', '.country', function(){
            $.ajax({
                url: "<?php echo base_url(); ?>user/states",
                type: 'POST',
                data : {countryId : $(this).val()}
            })
            .done(function( data ) {
                $('#investor-info-form .state').html(data);
            });
        });
      
        $('#expertise').tagit({
            allowSpaces : true
        });
        
        
        
        
    });
</script>