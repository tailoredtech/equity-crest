<?php 
if(isset($panels)){
    $sector = json_encode($panels['sector']);
    $location = json_encode($panels['location']);
}
?>
<div class="container wrap">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 panel-box">
            <div class="panel-group " id="panels-container">
                <div class="panel panel-default">
                    <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="#collapseOne" data-parent="#panels-container" data-toggle="collapse" class="center-block text-center collapsed">
                                    <span class="glyphicon glyphicon-chevron-down dropicon"></span><span class="collapse-title">Platform Statistics</span>
                                </a>
                            </h4>
                        </div>
                    <div id="collapseOne" class="panel-collapse collapse">
                        <div class="panel-body">
                            <!--<img src='<?php echo base_url(); ?>assets/images/stats.jpg' />-->
                            <?php if(isset($panels)){ ?>
                            <div class="platform-stat-wrap">
                                <canvas id="myChart" width="250" height="250"></canvas>
                                <div id="platform-stat-1" class="platform-legend">
                                    
                                </div>
                                <div class="platform-stat-title">
                                    <h4>Sector-wise Distribution</h4>
                                </div>
                            </div>
                            <div class="platform-stat-divide"></div>
                            <div class="platform-stat-wrap">
                                <canvas id="myChart2" width="250" height="250"></canvas>
                                <div id="platform-stat-2" class="platform-legend">
                                    
                                </div>
                                <div class="platform-stat-title">
                                    <h4>Location-wise Distribution</h4>
                                </div>
                            </div>
                            <?php }  else { ?>
                                 <h5 class="text-center">Coming Soon</h5>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    
                    <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="#collapseTwo" data-parent="#panels-container" data-toggle="collapse" class="center-block text-center collapsed">
                                    <span class="glyphicon glyphicon-chevron-down dropicon"></span><span class="collapse-title">Bids & Offers</span>
                                </a>
                            </h4>
                        </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                            <h5 class="text-center">Coming Soon</h5>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="#collapseThree" data-parent="#panels-container" data-toggle="collapse" class="center-block text-center collapsed">
                                    <span class="glyphicon glyphicon-chevron-down dropicon"></span><span class="collapse-title">Portfolio in Media</span> 
                                </a>
                            </h4>
                        </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                            <h5 class="text-center">Coming Soon</h5>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="#collapseFour" data-parent="#panels-container" data-toggle="collapse" class="center-block text-center collapsed">
                                    <span class="glyphicon glyphicon-chevron-down dropicon"></span><span class="collapse-title">EQ in Media</span> 
                                </a>
                            </h4>
                        </div>
                    <div id="collapseFour" class="panel-collapse collapse">
                        <div class="panel-body">
                            <h5 class="black lato-bold">Equity Crest featured in The Economic Times</h5>
                            <p>As we work hard on building the platform, we recently got a boost to our morale when The Economic Times covered Equity Crest in their article. <a href="http://epaperbeta.timesofindia.com/Article.aspx?eid=31815&articlexml=Deal-Discovery-Tools-Turn-Angels-Wands-14102014001083">Click Here</a> to read the article</p>
                            <img class="text-center" src="<?php echo base_url(); ?>assets/images/et.jpg">
                        </div>
                    </div>
                </div>
                
                <div class="panel panel-default">
                    <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="#collapseFive" data-parent="#panels-container" data-toggle="collapse" class="center-block text-center collapsed">
                                    <span class="glyphicon glyphicon-chevron-down dropicon"></span><span class="collapse-title">Customers Speak</span> 
                                </a>
                            </h4>
                        </div>
                    <div id="collapseFive" class="panel-collapse collapse">
                        <div class="panel-body">
                            <h5 class="text-center">Coming Soon</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(".panel-heading").click(function(){
        $(".panel-group .panel-active").removeClass("panel-active");
        $(this).addClass("panel-active");
    });
    <?php 
    if(isset($panels)){
    ?>
    $(document).ready(function(){
        
    var options={tooltipTemplate: "<%if (label){%><%=label%> <%}%>"};
    // Get context with jQuery - using jQuery's .get() method.
    var ctx = $("#myChart").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var myNewChart = new Chart(ctx);
    var data =  <?php echo $sector ?>;
    console.log(data);
    var myDoughnutChart = new Chart(ctx).Doughnut(data,options);
    for (var i = 0, len = data.length; i < len; i++) {
      var obj = data[i];
//      console.log(obj['value']);
      var co = obj['color'];
      var la = obj['label'];
      var li = "<li><span class='platform-color' style='background-color:"+co+"'></span>"+la+"</li>";
      $("#platform-stat-1").append(li);
    }
    
    // Get context with jQuery - using jQuery's .get() method.
    var ctx = $("#myChart2").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var myNewChart = new Chart(ctx);
    var data =  <?php echo $location ?>;
    console.log(data);
    var myDoughnutChart = new Chart(ctx).Pie(data,options);
    for (var i = 0, len = data.length; i < len; i++) {
      var obj2 = data[i];
//      console.log(obj['value']);
      var co = obj2['color'];
      var la = obj2['label'];
      var li = "<li><span class='platform-color' style='background-color:"+co+"'></span>"+la+"</li>";
      $("#platform-stat-2").append(li);
    }
    
    });
    <?php 
    }
    ?>
</script>    