<div id="offers-deals" class="container-fluid deals-block offers-block offers-block-1">
    <div class="row">
        <div class="container">
            <div class="row block-header">
                <div class="col-sm-3 block-title">
                    <h2 class="lato-bold">Recent</h2>
                </div>
                <div class="right-options col-lg-2 col-md-2 col-sm-2 col-xs-2 pull-right">
                    <div class="slider-marker pull-right">
                        <a class=" text-center" href="#">
                            <img src="<?php echo base_url(); ?>assets/images/dot.png">
                        </a>
                    </div>
                    <div class="extra-options pull-right">
                        <a class="extra-options-link text-center" href="#">=</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
            <?php $max = count($users); if($max){  ?>  
            <div class="slider-markup col-sm-12 deals-slider" data-slide-mode="slide" data-slide-max="3" data-slide-min="3" data-slide-pager="false">
                <?php
                $count = 0;
                    foreach ($users as $user) {
                        $data['user'] = $user;
                        $this->load->view('front-end/grid-single-item', $data);
                        $count++;
                    }
                 ?>
            </div>
            <?php }else{ ?>
                <div class="col-sm-12 success-msg" >
                    <div class="alert alert-success" role="alert">There is No deals available as per your search criteria</div>
                </div>
            <?php } ?>
        </div>
            </div>
    </div>
</div>