<div id="hot-deals" class="container-fluid deals-block ">
    <div class="row">
        <div class="container">
            <div class="row block-header">
                <div class="col-sm-2 block-title">
                    <h2>Opportunities</h2>
                </div>
                <div class="filter-options col-sm-2 col-sm-offset-3">
                    <div class="dropdown">
                        <a href="javascript:void(0);" class="refine-details dropdown-toggle" data-toggle="dropdown" >Refine Deals <span class="glyphicon glyphicon-chevron-down"></span></a>
                        <ul class="dropdown-menu refine" role="menu">
                            <li class="refine-filter">
                                <a href="javascript:void(0);">Status</a>
                                <ul class="dropdown-menu sub-menu" ">
                                    <li><a href="<?php echo base_url(); ?>home">All</a></li>
                                    <li><a href="<?php echo base_url(); ?>home?status=recent">Recent</a></li>
                                    <li><a href="<?php echo base_url(); ?>home?status=top">Top Funded</a></li>
                                </ul>
                            </li>
                            <li class="refine-filter">
                                <a href="javascript:void(0);">Sector</a>
                                <ul class="dropdown-menu sub-menu">
                                    <?php foreach ($sectors as $sector) { ?>
                                        <li><a href="<?php echo base_url() ?>home?sector=<?php echo $sector['id']; ?>"><?php echo $sector['name']; ?></a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <li class="refine-filter">
                                <a href="javascript:void(0);">Stage</a>
                                <ul class="dropdown-menu sub-menu">
                                    <?php foreach ($stages as $stage) { ?>
                                        <li><a href="<?php echo base_url() ?>home?stage=<?php echo $stage['id']; ?>"><?php echo $stage['name'] ?></a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <li class="refine-filter">
                                <a href="javascript:void(0);">Location</a>
                                <ul class="dropdown-menu sub-menu" >
                                    <?php foreach ($locations as $location) { ?>
                                        <li><a href="<?php echo base_url() ?>home?location=<?php echo $location['name']; ?>"><?php echo $location['name'] ?></a></li>
                                    <?php } ?>
                                </ul>   
                            </li>
                            <li class="refine-filter">
                                <a href="javascript:void(0);">Investment Size</a>
                                <ul class="dropdown-menu sub-menu" >
                                    <li><a href="<?php echo base_url() ?>home?size=1">Upto 10 Lakhs</a></li>
                                    <li><a href="<?php echo base_url() ?>home?size=2">10 Lakhs - 50 Lakhs</a></li>
                                    <li><a href="<?php echo base_url() ?>home?size=3">50 Lakhs - 1 Crore</a></li>
                                    <li><a href="<?php echo base_url() ?>home?size=4">1 Crore - 5 Crore</a></li>
                                    <li><a href="<?php echo base_url() ?>home?size=5">5 Crore & Above</a></li>
                                </ul>
                            </li>
                            
                        </ul>
                    </div>
                </div>

                <!--                <div class="right-options  col-sm-2  pull-right">
                                    <div class="slider-marker pull-right">
                                        <a class="text-center" href="#">
                                            <img src="<?php echo base_url(); ?>assets/images/dot.png">
                                        </a>
                                    </div>
                                    <div class="extra-options pull-right">
                                        <a class="extra-options-link text-center" href="#">=</a>
                                    </div>
                                </div>-->
            </div>
        </div>
        <div class="container">
            <div class="row">
                <?php $max = count($users);
                if ($max) { ?>  
                    <div class="slider-markup col-sm-12 deals-slider" data-slide-mode="slide" data-slide-max="3" data-slide-min="3" data-slide-pager="false">
                        <?php
                        $count = 0;
                        foreach ($users as $user) {
                            $data['user'] = $user;
                            $this->load->view('front-end/grid-single-item', $data);
                            $count++;
                        }
                        ?>
                    </div>
<?php } else { ?>
                    <div class="col-sm-12 space-20 success-msg" >
                        <div class="alert alert-success" role="alert">There are No Opportunities available as per your search criteria - <a href="<?php echo base_url(); ?>home/index">Please click here to go back</a></div>
                    </div>
<?php } ?>
            </div>
        </div>
    </div>
</div>