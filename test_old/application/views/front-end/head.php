<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Fund Startups | Angel/VC Investing | Equity Crest</title>
    <meta name="description=" content="Equity Crest is one-of-its-kind platform for startups & entrepreneurs to get funding for their ventures. Startups/Entrepreneurs can connect with investors from across the world through Equity Crest.
          We have built a world class ecosystem that promotes growth of new and young businesses, and creates long term value for all our stakeholders.">
          <?= css('bootstrap.min.css') ?>
          <?= css('style.css') ?>
          <?= css('jquery.slick.css') ?>
          <?= css('colorbox.css') ?>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <?= js('bootstrap.min.js') ?>
    <?= js('slick.min.js') ?>
    <?= js('jquery.colorbox.js') ?>
    <?= js('Chart.min.js') ?>
    <!--[if IE]>
            <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>