<div class="row">                        
    <div class="profile-box">
        <div class="user-info">
            <div class="col-sm-12">
                <div class="row">
                    <div style="display: table;margin: 0 auto;">
                        <div class="user-image">
                            <a href="javascript:void(0)">
                                <?php if (!empty($user['image'])) { ?>
                                    <img class="img-thumbnail" src="<?php echo base_url(); ?>uploads/users/<?php echo $user['id'] ?>/<?php echo $user['image']; ?>" >
                                <?php } else { ?>
                                    <img class="img-thumbnail" src="<?php echo base_url(); ?>uploads/investor.jpg" >
                                <?php } ?>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="user-name lato-bold text-center">
                            <?php if (isset($user['name'])) echo $user['name']; ?>
                        </div>
                        <div class="user-location text-center">
                            <?php if (isset($user['city'])) echo $user['city']; ?>
                        </div>
                        <div class="user-location text-center">
                            <?php if (isset($user['company_name'])) echo "founder @".$user['company_name']; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--
        <div class="user-bio">
            <?php if (isset($user['experience'])) echo character_limiter(strip_tags($user['experience']), 60); ?> 
        </div>

        <div class="user-investment">
            Invested Rs <?php if (isset($user['experience']))
                echo $user['invested'];
            else
                echo "0";
            ?>
        </div>-->

        <div class="user-interests">
            <div class="user-sectors">
                <span>Sectors :</span> <?php if (isset($user['mentoring_sectors'])) echo character_limiter($user['mentoring_sectors'],5); ?>
            </div>
            <div  class="user-areas">
                <span>Areas :</span> <?php if (isset($user['mentoring_sectors'])) echo character_limiter($user['expertise'],5); ?>
            </div>
        </div>

    </div>
</div>
