<div class="main">
    <div class="container form-contain wrap">
        <div class="row">
            <h4 class="form-header col-sm-12 lato-regular">News/Top Stories</h4>
        </div>

        <div class="container wrap">

            <div class="content-box row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-2">
                            <img  class="news-img-nav text-center" src="<?php echo base_url(); ?>assets/images/newsbox1.png">
                        </div>
                        <div class="col-sm-9 col-sm-offset-1">
                            <div class="row lato-bold news-headline">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
                            <div class="row lato-regular news-date">26 May 2014</div>
                            <div class="row lato-regular" style="padding-right:20px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras risus enim, vehicula vel tempus non, mollis non tellus. Etiam a arcu imperdiet, rhoncus sem et, adipiscing neque. Sed viverra risus ut leo aliquet, id porta lacus malesuada. Praesent a ipsum sed nibh sollicitudin semper non in elit. Sed id nibh eget magna porttitor rhoncus ac vitae dolor. Aliquam interdum sem nec massa lobortis, at luctus est accumsan. </div>

                        </div>

                    </div>
                    <div class="news-breaker"></div>
                    <div class="row">
                        <div class="col-sm-2">
                            <img  class="news-img-nav text-center" src="<?php echo base_url(); ?>assets/images/newsbox2.png">
                        </div>
                        <div class="col-sm-9 col-sm-offset-1">
                            <div class="row lato-bold news-headline">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
                            <div class="row lato-regular news-date">26 May 2014</div>
                            <div class="row lato-regular" style="padding-right:20px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras risus enim, vehicula vel tempus non, mollis non tellus. Etiam a arcu imperdiet, rhoncus sem et, adipiscing neque. Sed viverra risus ut leo aliquet, id porta lacus malesuada. Praesent a ipsum sed nibh sollicitudin semper non in elit. Sed id nibh eget magna porttitor rhoncus ac vitae dolor. Aliquam interdum sem nec massa lobortis, at luctus est accumsan. </div>

                        </div>

                    </div>
                    <div class="news-breaker"></div>
                    <div class="row">
                        <div class="col-sm-2">
                            <img  class="news-img-nav text-center" src="<?php echo base_url(); ?>assets/images/newsbox3.png">
                        </div>
                        <div class="col-sm-9 col-sm-offset-1">
                            <div class="row lato-bold news-headline">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
                            <div class="row lato-regular news-date">26 May 2014</div>
                            <div class="row lato-regular" style="padding-right:20px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras risus enim, vehicula vel tempus non, mollis non tellus. Etiam a arcu imperdiet, rhoncus sem et, adipiscing neque. Sed viverra risus ut leo aliquet, id porta lacus malesuada. Praesent a ipsum sed nibh sollicitudin semper non in elit. Sed id nibh eget magna porttitor rhoncus ac vitae dolor. Aliquam interdum sem nec massa lobortis, at luctus est accumsan. </div>

                        </div>

                    </div>
                    <div class="news-breaker"></div>
                    <div class="row">
                        <div class="col-sm-2">
                            <img  class="news-img-nav text-center" src="<?php echo base_url(); ?>assets/images/newsbox4.png">
                        </div>
                        <div class="col-sm-9 col-sm-offset-1">
                            <div class="row lato-bold news-headline">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
                            <div class="row lato-regular news-date">26 May 2014</div>
                            <div class="row lato-regular" style="padding-right:20px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras risus enim, vehicula vel tempus non, mollis non tellus. Etiam a arcu imperdiet, rhoncus sem et, adipiscing neque. Sed viverra risus ut leo aliquet, id porta lacus malesuada. Praesent a ipsum sed nibh sollicitudin semper non in elit. Sed id nibh eget magna porttitor rhoncus ac vitae dolor. Aliquam interdum sem nec massa lobortis, at luctus est accumsan. </div>

                        </div>

                    </div>
                    <div class="news-breaker"></div>
                    <div class="row">
                        <div class="col-sm-2">
                            <img  class="news-img-nav text-center" src="<?php echo base_url(); ?>assets/images/newsbox6.png">
                        </div>
                        <div class="col-sm-9 col-sm-offset-1">
                            <div class="row lato-bold news-headline">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
                            <div class="row lato-regular news-date">26 May 2014</div>
                            <div class="row lato-regular" style="padding-right:20px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras risus enim, vehicula vel tempus non, mollis non tellus. Etiam a arcu imperdiet, rhoncus sem et, adipiscing neque. Sed viverra risus ut leo aliquet, id porta lacus malesuada. Praesent a ipsum sed nibh sollicitudin semper non in elit. Sed id nibh eget magna porttitor rhoncus ac vitae dolor. Aliquam interdum sem nec massa lobortis, at luctus est accumsan. </div>

                        </div>

                    </div>
                </div>

            </div>
        </div>




    </div>
</div>