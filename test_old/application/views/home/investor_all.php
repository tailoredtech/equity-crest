<div class="main">
<div id="hot-deals" class="container-fluid deals-block ">
    <div class="row">
        <div class="container wrap">
            <div class="row block-header">
                <div class="col-sm-4 block-title">
                    <h2>Investors</h2>
                </div>
            </div>
        </div>
        <div class="container wrap">
            <div class="row">
                <?php $max = count($investors);
                if ($max) {
                    ?>  
                    <div class="slider-markup col-sm-12 ">
                        <div class="row">
                        <?php
                        $count = 0;
                        foreach ($investors as $user) { ?>
                            <div class="col-sm-3">
                            <?php $data['user'] = $user;
                            $this->load->view('front-end/investor_single_box', $data);
                            $count++; ?>
                                </div>
                       <?php }
                        ?>
                            </div>
                    </div>
<?php } else { ?>
                    <div class="col-sm-12 space-20 success-msg" >
                        <div class="alert alert-success" role="alert"> Investor verification and update in
    progress</div>
                    </div>
<?php } ?>
            </div>
        </div>
    </div>
</div>
</div>