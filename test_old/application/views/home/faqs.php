<div class="main">
    <div class="container form-contain">
        <div class="row">
            <h4 class="form-header col-lg-12 col-md-12 col-sm-12 col-xs-12 lato-regular">FAQs</h4>
        </div>
    </div>


        <div class="container">
            <div class="row content-box">
                <div class="col-md-12">    
                    <div id="investee-faq" class="row">
                        <h3 class="faq-header"><div class="glyphicon glyphicon-plus"></div>  FAQs for Investees</h3>
                    </div>

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                            <div class="panel-group investee-faq-data" id="panels-container">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="faq-inactive lato-regular" data-toggle="collapse" data-parent="#panels-container" href="#collapseOne">
                                                <div class="glyphicon glyphicon-plus"></div>  <b>What is seed capital and why do startups need seed capital?</b>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            Seed Capital is the initial capital used to start a business. The amount of money is usually relatively small, because the business is still in the idea or conceptual stage. Startups are generally at a pre-revenue stage and need seed capital for research & development, to cover initial operating expenses until a product or service can start generating revenue, and to attract the attention of venture capitalists. 
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="faq-inactive lato-regular" data-toggle="collapse" data-parent="#panels-container" href="#collapseTwo">
                                                <div class="glyphicon glyphicon-plus"></div>  <b>What kind of capital should a startup raise and where should it be raised from?</b>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>Mainly two types of capital are available for businesses at any stage: Debt and Equity.</p>
                                            <p>Debt capital, is the capital where the investor provides you capital at a certain interest rate for a fixed period of time, and generally asks you for collaterals to secure his investments. </p>
                                            <p>In comparison, while providing Equity, the investor takes a certain percentage of ownership in the company -- which means that if the business is successful both participants make profit (either through appreciation in share price or dividends, or both). And if the business fails, both of them loose their respective investments. </p>
                                            <p>Startups are unsure about the future and financial projections -- however smartly made -- are not certain. Startups face lot of risk and there are significant chances of failure, thus it is always good to share the risk at initial stage. In Equity capital, the promoter shares the risk with the investor, and it is in the investor’s interest to see that the firm grow -- thus he will also provide valuable mentorship to the startup. </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="faq-inactive lato-regular" data-toggle="collapse" data-parent="#panels-container" href="#collapseThree">
                                                <div class="glyphicon glyphicon-plus"></div>  <b>From whom should the startup raise capital?</b>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>There are many ways a startup can raise seed capital. The significant ones are:</p>
                                            <li>Through family and friends</li>
                                            <li>Through venture capital fund -- but not many VC funds provide seed capital</li>
                                            <li>Through angel investors, but to find an angel investor who is interested in a particular area of business is a tough game</li>
                                            <li>Through crowd funding</li>

                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="faq-inactive lato-regular" data-toggle="collapse" data-parent="#panels-container" href="#collapseFour">
                                                <div class="glyphicon glyphicon-plus"></div>  	<b>Does Equity Crest advise registered investors to invest in a startup?</b>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseFour" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            Equity Crest is a platform which provides a different channel to investors to invest in startups, where they get all the required information about the startup. Investors are free to interact with the startup, to understand their business idea and learn more about them. Equity Crest believes that having access to all the required information will help the investor make a sound investment decision. However, we do not provide our own judgement but we will make sure that startups listed with us are not onto something illegal, unethical or unviable.
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="faq-inactive lato-regular" data-toggle="collapse" data-parent="#panels-container" href="#collapseFive">
                                                <div class="glyphicon glyphicon-plus"></div>  	<b>What is Crowd-funding?</b>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseFive" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>Crowd-funding is solicitation of funds (in small amounts) from multiple investors through a web-based platform or social networking site for a specific project, business venture or social cause.</p>
                                            <p>Crowd-funding can be divided into four categories: donation crowd-funding, reward crowd-funding, peer-to-peer lending and equity crowd-funding.</p>
                                            <p>In equity based crowd-funding, in consideration of funds solicited from investors, equity shares of the company are issued. It refers to fund-raising by a business, particularly early-stage funding, through offer of equity interest in the business to these investors. Businesses seeking to raise capital through this mode typically advertise online through a crowd-funding platform, which serves as an intermediary between them and the investors.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="faq-inactive lato-regular" data-toggle="collapse" data-parent="#panels-container" href="#collapseSix">
                                                <div class="glyphicon glyphicon-plus"></div>  	<b>How is a crowd-funding platform more beneficial as compared to an offline mode of fund raise?</b>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseSix" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>Crowd-funding platform has a lot of benefits:</p>
                                            <li>It allows a startup to raise funds at a lower cost of capital without undergoing rigorous procedures</li>
                                            <li>Crowd-funding provides a fresh mode of financing for startups in the form of investor money. And by attracting investors, it provides them a new investment avenue for portfolio diversification </li>
                                            <li>Crowd-funding gives startups access to different investors having interests in different sector at the same time</li>
                                            <li>As many small investors come together to fund a project and diversify risk, a crowd-funding platform provides a better chance to the startup to get funded.</li>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="faq-inactive lato-regular" data-toggle="collapse" data-parent="#panels-container" href="#collapseSeven">
                                                <div class="glyphicon glyphicon-plus"></div>      <b>Is Equity Crest a crowd-funding platform?</b>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseSeven" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            Yes, Equity Crest is a crowd-funding platform. 
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="faq-inactive lato-regular" data-toggle="collapse" data-parent="#panels-container" href="#collapseEight">
                                                <div class="glyphicon glyphicon-plus"></div>  <b>How can Equity Crest help me raise the required capital?</b>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseEight" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>Equity Crest is a crowd-funding platform and investors from different fields, with different flair of investments are registered with us. The process for a startup to get funded through this platform is as follows:</p>

                                            <li>Startup gets registered with Equity Crest</li>
                                            <li>Startup has to upload its business plan, financials and other required documents</li>
                                            <li>A team from Equity Crest will screen the information, meet the startup to evaluate their proposal and do a back ground check. After due diligence if our team finds the proposal viable, it will be listed on Equity Crest.</li>
                                            <li>Once listed on Equity Crest, the proposal can be seen by all investors registered with us. If they like the proposal, they will get in touch with the startup through us to get more clarity on the project. </li>
                                            <li>The investor will then negotiate the terms and conditions of his investment, and finally fund them through Equity Crest. </li>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="faq-inactive lato-regular" data-toggle="collapse" data-parent="#panels-container" href="#collapseNine">
                                                <div class="glyphicon glyphicon-plus"></div>  <b>What kind of investors are registered with Equity Crest?</b>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseNine" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>We have a variety of investors registered with Equity Crest, such as:</p>
                                            <li>HNIs, who want to diversify their investment portfolio by investing in startups</li>
                                            <li>Angle investors, who have expertise of specific sectors and want to invest in quality startups in such sectors</li>
                                            <li>Retail investors </li>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="faq-inactive lato-regular" data-toggle="collapse" data-parent="#panels-container" href="#collapseTen">
                                                <div class="glyphicon glyphicon-plus"></div>  	<b>Is Equity Crest regulated by any Government body?</b>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTen" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            No. Currently, Equity Crest is not regulated by any Government body.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="investor-faq" class="row">
                        <h3 class="faq-header"><div class="glyphicon glyphicon-plus"></div>  FAQs  for Investors</h3>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                            <div class="panel-group investor-faq-data" id="panels-container" style="display: none">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="faq-inactive lato-regular" data-toggle="collapse" data-parent="#panels-container" href="#collapseEleven">
                                                <div class="glyphicon glyphicon-plus"></div>  <b>What is investing in startups about?</b>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseEleven" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            Investing in startups means to invest in business that are being built on new ideas, or those who are executing an existing idea in a different way. These business have tremendous potential to grow at high rate and can grow manifold over a short period of time. The investor invests his money in such business for a portion of equity. If the business succeeds, the investor's equity value could increase substantially. 
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="faq-inactive lato-regular" data-toggle="collapse" data-parent="#panels-container" href="#collapseTwelve">
                                                <div class="glyphicon glyphicon-plus"></div>  <b>Is investments in startups profitable?</b>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwelve" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>Startups are business who are executing new ideas, or executing already existing ideas in a different way. If their idea succeeds then the profits are significant, and the return on investment can be much higher than any other asset classes. Some prominent examples are Facebook, WhatsApp, and Yahoo! who began as startups, and today are multi-billion dollar companies. Examples od successful startups from India include companies like Flipkart, Redbus and Snapdeal.</p>
                                            <p>Most startups fail because of the lack of experience of promoters and the unpredictability of idea they are working on. But with the right mentorship and funding, the success rate of such startups can increase. The distribution of returns from startups is highly skewed, this means that while a majority of startups would fail, most profits come from a few big successes. </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="faq-inactive lato-regular" data-toggle="collapse" data-parent="#panels-container" href="#collapseThirteen">
                                                <div class="glyphicon glyphicon-plus"></div>  <b>What are other benefits of investing in startups?</b>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseThirteen" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>Apart from potential monetary profits if the startup succeeds, investors can enjoy a few additional benefits by investing in startups. </p>
                                            <p>The investor can be part of the next big invention or next big thing. Investing in a startup, allows the investor to participate in the growth of a company from the beginning. He can assist them with his valuable inputs at the early stage, which can be instrumental in their growth story. By investing in startups, the investor is participating in building the entrepreneurial culture in the society, and thereby helping build the nation.</p>

                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="faq-inactive lato-regular" data-toggle="collapse" data-parent="#panels-container" href="#collapseFourteen">
                                                <div class="glyphicon glyphicon-plus"></div>  	<b>Why do startups need investment at an early stage?</b>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseFourteen" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            Entrepreneurs have an idea and the zeal required to convert that idea into reality. However, to make the idea a reality and take it to masses they need financial assistance. Depending on the business, the startup needs money to lease office space, acquire inventory, build a website, keep it up and running, buy equipments, hire talented people, take care of day-to-day expenses and many such other things. 
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="faq-inactive lato-regular" data-toggle="collapse" data-parent="#panels-container" href="#collapseFifteen">
                                                <div class="glyphicon glyphicon-plus"></div>  	<b>How do I use Equity Crest to invest in startups?</b>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseFifteen" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            Equity Crest follows a rigorous system of evaluating businesses that eventually get listed on its platform. We have on our platform startups with good growth potential from different sectors. We provide our registered investors all the information and tools required to learn more about the startups, interact with them, and know what other investors think of them. Investors are give the tools and facilities to make a sound investment decision. Once they decide to invest, Equity Crest will take care of all the necessary formalities and ensure effective follow-ups subsequent to such investment.
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="faq-inactive lato-regular" data-toggle="collapse" data-parent="#panels-container" href="#collapseSixteen">
                                                <div class="glyphicon glyphicon-plus"></div>  	<b>Does Equity Crest review investee companies?</b>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseSixteen" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            Yes, Equity Crest reviews all the information that a startup provides. We go through all the financials, projections and other documents submitted by a startup and only list those startups who can provide us with reliable information. We make sure that the information provided by them is fair, complete and not misleading. 
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="faq-inactive lato-regular" data-toggle="collapse" data-parent="#panels-container" href="#collapseSeventeen">
                                                <div class="glyphicon glyphicon-plus"></div>      <b>Will Equity Crest advise me on which startups to invest in?</b>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseSeventeen" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            The Equity Crest platform provides a different channel to investors to invest in startups, where they can get all the required information to make their investment. We believe that having access to all this information will enable the investor to make a sound investment decision. However, we do not provide our own judgement. Nevertheless we ensure that the startups are not onto something which is illegal, unethical or unviable. 
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="faq-inactive lato-regular" data-toggle="collapse" data-parent="#panels-container" href="#collapseEighteen">
                                                <div class="glyphicon glyphicon-plus"></div>  <b>Is there a minimum or a maximum I can invest in a startup registered with Equity Crest?</b>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseEighteen" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>Under the current regulations governing a private placement, the value of such offer or invitation per person shall be with an investment size of not less than Rs 20,000 of face value of the securities. As per a recent Consultation Paper, floated by SEBI on crowd-funding in India the following limits were proposed.</p>

                                            <li>A Qualified Institutional Buyer(QIB) is required to purchase at least 5 times of the minimum offer value per person as specified in the aforementioned rule. <b>Collectively all the QIBs shall hold a minimum of 5% of the securities issued.</b> </li>
                                            <li>A Company is required to purchase at least 4 times of the minimum offer value per person as specified in the aforementioned rule.</li>

                                            <li>A HNI is required to purchase at least 3 times the minimum offer value per person.</li>
                                            <li>An ERI is required to purchase at least the minimum offer value per person. The maximum investment by an ERI in an issue shall not exceed Rs 60,000. The total of all investments in crowd-funding for an eligible retail investor in a year should not exceed 10% of its net worth.</li>
                                            <p>Given the risk associated with investment, Equity Crest suggests that the investor should have a balanced portfolio. </p>

                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="faq-inactive lato-regular" data-toggle="collapse" data-parent="#panels-container" href="#collapseNinteen">
                                                <div class="glyphicon glyphicon-plus"></div>  <b>Is there a minimum or a maximum amount that a startup can raise through Equity Crest?</b>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseNinteen" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            There is no minimum or maximum investment limot. However, our focus is primarily on seed and early-stage businesses, and so most campaigns on Equity Crest are raising amounts of money appropriate to business that are in the stage of development.
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="faq-inactive lato-regular" data-toggle="collapse" data-parent="#panels-container" href="#collapseTwenty">
                                                <div class="glyphicon glyphicon-plus"></div>  	<b>How much equity will I receive for my investment?</b>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwenty" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            Each startup decides how much money it wants to raise in exchange for what percentage of its equity. The final valuation numbers will depend on the negotiation between the investor and the startup.  Investor’s equity interest will be proportionate to the size of his investment in the startup. 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>
</div>
<script>
    $(document).ready(function() {
        var plus = $(".in").siblings(".panel-heading").find(".glyphicon");
//        function(){
//            plus.removeClass('glyphicon-plus');
//            //plus.addClass('glyphicon-minus');
//        }
        var flipinvestee = 0;
        $( "#investee-faq" ).click(function() {
            //console.log('hi');
            $( ".investee-faq-data" ).toggle( flipinvestee++ % 2 === 0 );
        });
        
        var flipinvestor = 0;
        $( "#investor-faq" ).click(function() {
            //console.log('hi');
            $( ".investor-faq-data" ).toggle( flipinvestor++ % 2 === 0 );
        });

    });</script>
