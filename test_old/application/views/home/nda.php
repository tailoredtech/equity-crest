<div class="main">
    <div class="container form-contain">
        <div class="row">
            <h4 class="form-header col-lg-12 col-md-12 col-sm-12 col-xs-12 lato-regular">Non Disclosure Agreement</h4>
        </div>
        <div class="container">
            <div class="row content-box">
                <div class="col-md-12">
                    <div class="row terms-margin">
                        <b class="center-block text-center">CONFIDENTIALITY TERMS</b><br>
                        
                        <p>EQ Advisors Private Limited (“Equity Crest”) proposes to provide access to the Investor to confidential information regarding various companies accessible on its website equitycrest.com, to evaluate such companies for the purpose of making investments and/or providing mentorship services. The Investor agrees to below mentioned terms of this agreement for using such confidential information. By mere use of this website, the Investor agrees to be subject to the applicable rules, guidelines, policies, terms, and conditions (as may be modified from time to time by Equity Crest) and the same shall be deemed to be incorporated into these terms and be considered as part and parcel of the same. Equity Crest reserves the right, at its sole discretion, to change, modify, add or remove portions of these terms, at any time without any prior written notice to the Investor. It is the Investor’s responsibility to review these terms periodically for updates / changes. The Investor’s continued use of the website following the posting of changes will mean that the Investor accepts and agrees to the revisions.</p>
                        
                        <br><b>TERMS</b><br>
                        <ul style="list-style: none">1. The Investor - agrees:
                            <li>    (a) to use the Confidential Information only for the Stated Purpose;</li>
                            <li>    (b) to hold the Confidential Information in confidence;</li>
                            <li>    (c) not to disclose, publish or communicate the Confidential Information to any third party; and</li>
                            <li>    (d) to abide by the other terms set out herein.</li>
					  <li>    (e) that he has no objection in being removed from the Investors list of Equity Crest without any prior notice or intimation to him by Equity Crest. </li>
				
                        </ul><br>
                        <ul style="list-style: none">2. The Investor may only disclose the Confidential Information:
                            <li>(a) to those of its employees, officers or agents; on the basis that such employees, officers and agents will keep the same confidential based on the terms stated herein;</li>
                            <li>(b) to professional advisers or consultants engaged to advise in connection with the Stated Purpose and who are under similar confidentiality obligations;</li>
                            <li>(c) as required by law or by any regulation or any order of court or regulatory authority or similar provision.</li>
                        </ul><br>
                        <ul style="list-style: none">The restrictions on use or disclosure of the Confidential Information will not apply to:</li>
                            <li>(a) any information which is generally available to the public (provided that this has not happened because of a breach of this Agreement, or any other duty of confidentiality between the parties);</li>
                            <li>(b) any information independently originated by the Investor or acquired by the Investor from a third party in circumstances in which the Investor is free to disclose it to others; or</li>
                            <li>(c) any information which is trivial or obvious.</li><br>
                        </ul><p>3. The Confidential Information shall remain the sole property of Equity Crest. Equity Crest may require, at any time, by written request, that the Investor return, destroy or delete (in such a manner that it cannot be recovered) all Confidential Information (including all copies) in its possession or control.</p>
                            <p>4. The Investor shall take appropriate security measures and keep the Confidential Information in such a way as to prevent its unauthorised disclosure.</p>
                            <p>5. All copyright and other intellectual property rights in and relating to the Confidential Information and belonging to Equity Crest will remain the property of Equity Crest .</p>
                            <p>6. The obligations in respect of Confidentiality mentioned herein shall survive even after the Investor ceases to be a registered Investor on this website.</p>
                            
                            <br><b>Definition</b><br>
                            <p>“Confidential Information” means all information, know-how, experience and materials, whether technical, commercial, financial or otherwise, relating to information available on the website equitycrest.com and/or its products, business or marketing activities, and any information which in the circumstances in which it is made available to the Investor, ought to be treated as confidential and whether in written, oral, machine readable or any other form and including any copies.</p>
                            
                            

                            
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>