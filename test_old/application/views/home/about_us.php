<div class="main">
    <div class="container form-contain wrap">
        <div class="row">
            <h4 class="form-header col-lg-12 col-md-12 col-sm-12 col-xs-12 lato-regular">About Us</h4>
        </div>
    </div>
    <div class="container wrap">
        <div class="row content-box">
            <div class="col-md-12">    
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                        <div class="panel-group investee-faq-data" id="panels-container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="faq-inactive lato-regular" data-toggle="collapse" data-parent="#panels-container" href="#collapseOne">
                                            <div class="glyphicon glyphicon-plus"></div>  <b>ABOUT EQUITY CREST</b>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <p>Equity Crest is the first club for entrepreneurship in India. We call it a club, because our objective is to make a success of everyone joining the club. We ‘Partner Businesses’ and build value for all our stakeholders --  be it Investee, Investors and Partners.</p>
                                        <p>1. Investee/ Company/ Businesses – can raise capital through Equity Crest</p>
                                        <p>2. Investors – Can invest in the companies/ businesses listed on Equity Crest, by partnering with us</p>
                                        <p>3. Partners – Partners of Equity Crest are Mentors, Advisors, anyone whom an Entrepreneur requires to make his business successful</p>
                                        <p>The entry into the club happens through the first deal you make through us, and this deal acts as your lifetime membership guarantee. Equity Crest will help you raise funds from seed stage, different Series round of funding, and also help you ultimately exit.</p>
                                        <p>The Equity Crest platform is synonymous with credibility, accountability, transparency, quality and top class expertise. We are a group of professionals who between us, have a great deal of experience in areas of business operations, fund raising, fund management, private equity, public markets, mergers and acquisitions.</p>
                                        <p>Some of the sectors covered by us are Media and Entertainment, FMCG, E-Commerce, Food & Agriculture, Impact investing and Financial Services to name a few. With our wide breadth of experience, we can help businesses at their early stages not just raise funds but also prepare themselves to become leading business ventures and generate value in the long term.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="faq-inactive lato-regular" data-toggle="collapse" data-parent="#panels-container" href="#collapseTwo">
                                            <div class="glyphicon glyphicon-plus"></div>  <b>VISION & PHILOSOPHY</b>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <P>We want to build a world class ecosystem that promotes growth of new and young businesses, so as to create long term value for all our stakeholders  -- entrepreneur, investors, partners and the Equity Crest team. </P>
                                        <p>We want to take entrepreneurship to masses in India - hence a collaborative approach is needed to ensure all stakeholders participate fully in this drive of entrepreneurship.</p>
                                        <p>We know credibility is critical for such a platform to succeed, hence protection of both investor, investee is of prime importance to us, and through our processes and techniques we will ensure it happens well.</p>
                                        <p>We only support strong companies, who have good scalable business models, run by committed entrepreneurs. A strong appraisal system, with strict guidelines, regular checks and balances, will ensure that everything we do is always in the open, and there is no ambiguity.</p>
                                        <p>Our advisory board comprises of people with domain expertise, thought leadership, and are among the strongest business professionals in their respective areas.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="faq-inactive lato-regular" data-toggle="collapse" data-parent="#panels-container" href="#collapseThree">
                                            <div class="glyphicon glyphicon-plus"></div>  <b>OBJECTIVE</b>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <li>To drive entrepreneurship and enable creation of a positive social and economic impact</li>
                                        <li>To create businesses who will be market leaders tomorrow, by identifying them today</li>
                                        <li>To create value for Investors by helping them make prudent investment decisions</li>
                                        <li>To explore new avenues that are opportunities for sound investment</li>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
