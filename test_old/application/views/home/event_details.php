<div class="main">
    <div class="container form-contain">
        <div class="row">
            <h4 class="form-header col-lg-12 col-md-12 col-sm-12 col-xs-12 lato-regular">Events Coming Up</h4>
        </div>
    </div>
    <div class="container">
        <div class="content-box">


            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10 events-bluebanner events-padding">
                    <div class="row text-center lato-regular events-headline">Startup Funding Made Easy</div>
                    <div class=" row text-center">
                        <div class="col-md-3"></div>
                        <div class="col-md-2">
                            <img src="<?php echo base_url(); ?>assets/images/funding_rising.png">
                            <div class="lato-regular events-name">Funding Rising</div>
                        </div>
                        <div class="col-md-2">

                            <img src="<?php echo base_url(); ?>assets/images/product_launch.png">
                            <div class="lato-regular events-name">Product Launch</div>
                        </div>
                        <div class="col-md-2">

                            <img src="<?php echo base_url(); ?>assets/images/market_insights.png">
                            <div class="lato-regular events-name">Market Insights</div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                </div>
                <div class="col-md-1"></div>

            </div>
            <div class="row eventdetails-margintop">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-10">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="row"><h3>Partners</h3></div>
                                    <div class="eventdetails-left-border">abcdef</div>
                                </div>
                                <div class="col-md-6"></div>
                                <div class="col-md-3"></div>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>