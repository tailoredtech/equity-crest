<div class="main">
<div id="hot-deals" class="container-fluid deals-block ">
    <div class="row">
        <div class="container wrap">
            <div class="row block-header">
                <div class="col-sm-4 block-title">
                    <h2>Portfolio</h2>
                </div>
            </div>
        </div>

        <div class="container wrap">
            <div class="row">
                <?php $max = count($users);
                if ($max) {
                    ?>  
                    <div class="slider-markup col-sm-12 ">
                        <div class="row">
                        <?php
                        $count = 0;
                        foreach ($users as $user) { ?>
                            <?php if(DayDifference(date('Y-m-d'), $user['validity_period']) > 0){ ?>
                            <div class="col-sm-4 portfolios-item">
                            <?php $data['user'] = $user;
                            $this->load->view('front-end/grid-single-item', $data);
                            $count++; ?>
                                </div>
                            <?php } ?>
                       <?php }
                        ?>
                            </div>
                    </div>
<?php } else { ?>
                    <div class="col-sm-12 space-20 success-msg" >
                        <div class="alert alert-success" role="alert">There are No Opportunities available as per your search criteria</div>
                    </div>
<?php } ?>
            </div>
        </div>
    </div>
</div>
</div>