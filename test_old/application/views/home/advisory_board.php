<div class="main">
    <div class="container form-contain wrap">
        <div class="row">
            <h4 class="form-header col-sm-12  lato-regular">Advisory Board</h4>
        </div>
    </div>
        <div class="container wrap">
            <div class="row content-box">
                <div class="col-sm-12">    
                    <div class="row terms-margin">
                        <p>The Equity Crest Advisory Board are a team of reputed, knowledgeable, movers & shakers from across industries. These individuals have excelled in their respected fields, and at present/in the past have been responsible for running multi-million dollar businesses.</p>
                        <p>Our Advisory Board will work with our investment team to evaluate businesses that we partner with. The Board is therefore responsible for vetting investments that we make through the platform.</p>
                        <p>Our Advisory Board will assist entrepreneurs and companies make their business investment-ready. When businesses approach us for investment, but don’t have a clear road-map for long term growth, or are not very differentiated in their offering, our Advisory Board can give them valuable inputs on how to build a sustainable and successful business. Our Advisory Board will work with the Equity Crest investment team, and guide companies to make their business investor friendly for short, as well as long term.</p>
                    </div>
                </div>
            </div>
        </div>
</div>
                    
