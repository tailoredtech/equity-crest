<div class="main">
     <div class="header-howitworks investee-nav">
         
         <div class="container wrap">
             <div class="row">
            <div class="col-xs-3">
                <div class="center-block text-center grey-font hiw-header">Are you >></div>
            </div>
             <div class="hiw-border"></div>
             <div class="col-xs-3">
                 <div class="center-block text-center grey-font hiw-header hiw-active-tab" id="hiw-investee"><a href="#">Raising Funds</a></div>
             </div>
             <div class="col-xs-3">
                 <div class="center-block text-center grey-font hiw-header" id="hiw-investor"><a href="#">Investor</a></div>
             </div>

<!--             <div class="col-md-3">
                 <div class="center-block text-center grey-font hiw-header"><a href="">Advisory Partner</a></div>
             </div>-->
             </div>
         </div>
     </div>
    <div class="container form-contain wrap">
        <div class="row">
            <h4 class="form-header col-sm-12 lato-regular">How it Works</h4>
        </div>
    </div>
    <div class="container investor-work wrap" style="display: none">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel-group " id="panels-container">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="#collapseOne" data-parent="#panels-container" data-toggle="collapse" class="text-center hiw-margin-40 collapsed">
                                    <div class="circle grey-font">1</div><span class="collapse-title"><b class="grey-font">Registration</b></span>
                                </a>
                            </h4>
                        </div>
<!--                        <div id="collapseOne" class="panel-collapse collapse">
                            <div class="panel-body">
                                <img src='<?php //echo base_url(); ?>assets/images/stats.jpg' />
                            </div>
                        </div>-->
                    </div>
                    <div class="panel panel-default">

                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="#" data-parent="#panels-container" data-toggle="collapse" class="text-center hiw-margin-40 collapsed">
                                    <div class="circle-black grey-font">2</div><span class="collapse-title"><b>Setup Your Profile</b></span>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <img src='<?php echo base_url(); ?>assets/images/how_it_works.jpg' />
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="#" data-parent="#panels-container" data-toggle="collapse" class="text-center hiw-margin-40 collapsed">
                                    <div class="circle grey-font">3</div><span class="collapse-title"><b class="grey-font">Find Deals</b></span>
                                </a>
                            </h4>
                            <div class="hiw-margin-70 grey-font">View deals (as per your preferences set)</div>
                        </div>
<!--                        <div id="collapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>-->
                    </div>
                    <div class="panel panel-default">
                       <div class="panel-heading">
                           <h4 class="panel-title">
                                <a href="#" data-parent="#panels-container" data-toggle="collapse" class="text-center hiw-margin-40 collapsed">
                                    <div class="circle grey-font center-block text-center">4</div><span class="collapse-title"><b class="grey-font">Understand Deals</b></span>
                                </a>
                            </h4>
                               <div class="hiw-margin-70 grey-font">Connect via Skype/Webinar/Physical Pay, Commitment Fee due, Digilence Procedure reports & Deal Terms to be finalised</div>
                        </div>
<!--                        <div id="collapseFour" class="panel-collapse collapse">
                            <div class="panel-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>-->
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="#" data-parent="#panels-container" data-toggle="collapse" class="text-center hiw-margin-40 collapsed">
                                    <div class="circle grey-font">5</div><span class="collapse-title"><b class="grey-font">Commit Deals</b></span>
                                </a>
                            </h4>
                            <div class="hiw-margin-70 grey-font">Shareholders agreements / Upload details about company meeting</div>
                        </div>
<!--                        <div id="collapseFive" class="panel-collapse collapse">
                            <div class="panel-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    
    <div class="container investee-work wrap">
        <div class="row">
            <div class="col-sm-12 ">
                <div class="panel-group " id="panels-container">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="#collapseOne" data-parent="#panels-container" data-toggle="collapse" class="text-center hiw-margin-40 collapsed">
                                    <div class="circle grey-font">1</div><span class="collapse-title"><b class="grey-font">Registration</b></span>
                                </a>
                            </h4>
                        </div>
<!--                        <div id="collapseOne" class="panel-collapse collapse">
                            <div class="panel-body">
                                <img src='<?php //echo base_url(); ?>assets/images/stats.jpg' />
                            </div>
                        </div>-->
                    </div>
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="#collapseTwo" data-parent="#panels-container" data-toggle="collapse" class="text-center hiw-margin-40 collapsed">
                                    <div class="circle grey-font">2</div><span class="collapse-title"><b class="grey-font">Find Deals</b></span>
                                </a>
                            </h4>
                            <div class="hiw-margin-70 grey-font">Once your proposal is uploaded, It will be analysed by Equity Crest. <br> Interaction with Equitycrest Team for changes, If required<br> Equity Crest Team to upload the proposal</div>
                        </div>
<!--                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>-->
                    </div>
                    
                    <div class="panel panel-default">

                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="#collapseThree" data-parent="#panels-container" data-toggle="collapse" class="text-center hiw-margin-40 collapsed">
                                    <div class="circle grey-font">3</div><span class="collapse-title grey-font" style="width: 200px;"><b>Track Investor Interest</b></span>
                                </a>
                            </h4>
                            <div class="hiw-margin-70 grey-font">Track Interest by investors, respond to Queries and Connect via Call, Webinar as required</div>
                        </div>
                        
                    </div>
                    
                    <div class="panel panel-default">
                       <div class="panel-heading">
                           <h4 class="panel-title">
                                <a href="#collapseThree" data-parent="#panels-container" data-toggle="collapse" class="text-center hiw-margin-40 collapsed">
                                    <div class="circle grey-font center-block text-center">4</div><span class="collapse-title"><b class="grey-font">Finalizing Terms</b></span>
                                </a>
                            </h4>
                               <div class="hiw-margin-70 grey-font">Due Digilence procedure reports & Deal Terms to be finalized</div>
                        </div>
<!--                        <div id="collapseFour" class="panel-collapse collapse">
                            <div class="panel-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>-->
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="#collapseThree" data-parent="#panels-container" data-toggle="collapse" class="text-center hiw-margin-40 collapsed">
                                    <div class="circle grey-font">5</div><span class="collapse-title"><b class="grey-font">Closure</b></span>
                                </a>
                            </h4>
                            <div class="hiw-margin-70 grey-font">Shareholders agreements / Upload details about company meeting</div>
                        </div>
<!--                        <div id="collapseFive" class="panel-collapse collapse">
                            <div class="panel-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>-->
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="#collapseThree" data-parent="#panels-container" data-toggle="collapse" class="text-center hiw-margin-40 collapsed">
                                    <div class="circle grey-font">6</div><span class="collapse-title"><b class="grey-font">Share Updates</b></span>
                                </a>
                            </h4>
                            <div class="hiw-margin-70 grey-font">Financial Updates, future fundraise, etc</div>
                        </div>
<!--                        <div id="collapseFive" class="panel-collapse collapse">
                            <div class="panel-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
  $("#hiw-investee").click(function(){
      $(".hiw-header").removeClass("hiw-active-tab");
    $(".investor-work").hide();
    $(".investee-work").show();
    $(this).addClass('hiw-active-tab');
  });
  $("#hiw-investor").click(function(){
      $(".hiw-header").removeClass("hiw-active-tab");
    $(".investee-work").hide();
    $(".investor-work").show();
    $(this).addClass('hiw-active-tab');
  });
});
</script>