<div class="main">
    <div class="container form-contain wrap">
        
        <div class="row">
            <h4 class="form-header col-sm-12 lato-regular" style="margin-left:33px ">Contact Us</h4>
        </div>
        <div class="container wrap">
            <div class="row content-box">
                <div class="col-sm-3">
                    
                        <b>Address</b><br>
                        505, ACME Plaza,<br>
                        Andheri Kurla Road, Chakala<br>
                        Andheri East <br>
                        Mumbai 400059, India <br><br>
                        <b>Contact Us: </b> +91 9930672680<br><br>
                        Email: info@equitycrest.com<br>
                    


                </div>
                <div class="col-sm-8">
                    <div>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3769.8736750499183!2d72.865545!3d19.113197000000003!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7c83a25825fd3%3A0x53a35e8fc56e469c!2sAcme+Plaza!5e0!3m2!1sen!2sin!4v1406706217444" width="581" height="340" frameborder="0" style="border:0"></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="container wrap">
            <div class="row content-box">
                <div class="col-sm-12">
                    <b>For Inquiry </b><br>
                    <div class="container col-sm-6 col-sm-offset-3 ">
                        <div class="row">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 form-label">Name</label>
                                <div class="col-sm-9">
                                    <input type="text" name="name" class="form-control form-input" id="name" placeholder="" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 form-label">Email</label>
                                <div class="col-sm-9">
                                    <input type="text" name="mobileno" class="form-control form-input" id="email" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 form-label">Subject</label>
                                <div class="col-sm-9">
                                    <input type="text" name="email" class="form-control form-input" id="subject" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 form-label">Message</label>
                                <div class="col-sm-9">
                                    <textarea id="message" class="form-control form-input" id="password" placeholder=""></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="center">
                                <div class="join-now col-sm-7" style="margin-left:90px;">
                                    <input type="submit" value="Submit" name="submit" class="join-btn col-sm-12 center lato-regular">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
