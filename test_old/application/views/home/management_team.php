<div class="main">
    <div class="container form-contain wrap">
        <div class="row">
            <h4 class="form-header col-sm-12 lato-regular">Management Team</h4>
        </div>
    </div>
    <div class="container wrap">
        <div class="row content-box">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-3">
                        <img class="img-thumbnail" src='<?php echo base_url(); ?>assets/images/AmitBanka.jpg' />
                    </div>
                    <div class="col-sm-9">
                        <div class="row team-member-name">Amit Banka<div class="team-member-share"><img src='<?php echo base_url(); ?>assets/images/share1.jpg' /></div></div>
                        <div class="row"><b>Founder</b></div>
                        <div class="row"></div>
                        <div class="row">
                            <p style="padding-right: 15px">Amit has rich experience in Fund Raising, Fund Management, Private Equity, Venture/ Early stage Investments, Impact Investments, Unlisted investments opportunities and Real Estate. He worked as Managing Director of Unilazer Ventures before starting-up Equity Crest. In his previous role as an angel investor, Amit has made investments in companies in Media & Entertainment, FMCG, E-Commerce, Agriculture etc. Prior to Unilazer, he has worked as an internal consultant on Mergers and Acquisitions with UTV for more than seven years. Amit has done his Masters in Finance from the Institute of Management Studies.
                            </p></div>
                        
                    </div>
                    
                </div>
                <hr>
                <div class="row">
                    <div class="col-sm-3">
                        <img class="img-thumbnail" src='<?php echo base_url(); ?>assets/images/AmitWadhwa.jpg' />
                    </div>
                    <div class="col-sm-9">
                        <div class="row team-member-name">Amit Wadhwa<div class="team-member-share"><img src='<?php echo base_url(); ?>assets/images/share1.jpg' /></div></div>
                        <div class="row"><b>Co-Founder</b></div>
                        <div class="row"></div>
                        <div class="row">
                            <p style="padding-right: 15px">
                                Amit has rich experience in Corporate Finance, Strategy, Investor Management, Fund Raising and M&A across broadcast television, motion pictures, internet and consumer products, among others. He has been on both sides of the table, and has a wealth of experience in raising funds for both established as well as new businesses. 
                        </p>
                        <p>Amit has also advised investors in buying and selling of businesses as well. His diverse experience includes working with top management, founder / promoters of leading family run organizations like Zee / Essel Group and Times Group; and professionally managed conglomerates like The Walt Disney Company. He has often driven ideation, implementation and decision making of operational and strategic initiatives including growth related initiatives (both organic and inorganic). Amit is also an expert in improving operational efficiencies and business & corporate strategy. Amit is a Master of Management Studies from Narsee Monjee Institute of Management Studies (NMIMS) in Mumbai. </p>
                        </div>
                    
                    </div>                 </div>                 <hr> <div
class="row">                     <div class="col-sm-3"> <img class="img-
thumbnail" src='<?php echo base_url(); ?>assets/images/rahulsinghal.jpg' />
</div> <div class="col-sm-9">                         <div class="row team-member-name">Rahul Singhal<div class="team-member-share"><img src='<?php echo
base_url(); ?>assets/images/share1.jpg' /></div></div> <div
class="row"><b>Senior Associate</b></div>                         <div
class="row"></div>                         <div class="row"> <p style
="padding-right: 15px">                                Rahul is a Investment Banking
professional with vast experience in debt syndication,private equity syndication, transaction advisory, IT and business development.
</p>                         <p>Rahul has excellent relationship network across corporate & banking circles and PE & VC funds.Rahul is
a Master of Management Studies from Indian Institute of Management, Ahmedabad (IIMA). </p> </div>
                    
                    </div>
                </div>                
                </div>
            </div>
        </div>
    </div>
</div>
