<div class="main">
    <div class="container form-contain wrap">
        <div class="row">
            <h4 class="form-header col-sm-12  lato-regular">Partners</h4>
        </div>
    </div>
        <div class="container wrap">
            <div class="row content-box">
                <div class="col-sm-12">    
                    <div class="row terms-margin">
                        <p>Equity Crest believes in a partnership approach. Partnering the entrepreneur to get funded is just one part of the process. The entrepreneur is often looking for all the help he can get in order for him to focus on the core of the business and run it efficiently.</p>
                        <p>Equity Crest will provide the entrepreneur with access to its network of specialists, experts and partners who can help ease the burden of many non-core, but essential tasks. Although these tasks are non-core, but they are essential for the smooth functioning of business, and we at Equity Crest can open doors to opportunities, markets and alliance partners to help perform these tasks.</p>
                        <p>For more information on posssible partnerships, the entrepreneur may please reach out to their respective account managers for assistance from our pool of partners.</p>
                        <p>To become an empanelled Partner with Equity Crest, please <u><a style="color: blue" href="mailto:info@equitycrest.com" target="_top">click here</a></u></p>
                        <!--<p>info@equitycrest.com</p>-->
                    </div>
                </div>
            </div>
        </div>
</div>
                    
