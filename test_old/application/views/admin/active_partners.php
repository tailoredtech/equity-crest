<div id="page-title">
    <h3>
        Active Partners
        <small>

        </small>
    </h3>
</div>
<div id="page-content">
    <form id="update-partners-form" action="<?php echo base_url(); ?>admin/update_partners" method="post">
        <div class="fg-toolbar ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix">
            <div id="example1_length" class="dataTables_length">
                <label>Status <select name="status" id="change-status">
                        <option value="" selected="selected">--Select to change --</option>
                        <option value="inactive">Inactive</option>
                    </select></label>
            </div>
        </div>
        <table class="table text-center">
            <thead>
                <tr>
                    <th></th>
                    <th class="text-left">Name</th>
                    <th class="text-center">Company Name</th>
                    <th class="text-center">Email</th>
                    <th class="text-center">Mobile</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($partners as $partner) { ?>
                    <tr>
                        <td><input type="checkbox" name="id[]" value="<?php echo $partner['id'] ?>" /></td>
                        <td class="font-bold text-left"><?php echo $partner['name'] ?></td>
                        <td><?php echo $partner['company_name'] ?></td>
                        <td><?php echo $partner['email'] ?></td>
                        <td><?php echo $partner['mob_no'] ?></td>
                        <td>
                            <a href="<?php echo  base_url()."admin/partner/".$partner['id'];?>" class="btn small bg-blue-alt tooltip-button" data-placement="top" title="View">
                                <i class="glyph-icon icon-flag"></i>
                            </a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </form>  
</div>
<script>
$('document').ready(function(){
    $('#change-status').on('change',function(){
       if($(this).val() != ''){
            $('#update-partners-form').submit();
        }
    });
});
</script>