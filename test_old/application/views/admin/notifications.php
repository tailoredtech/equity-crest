<div id="page-title">
    <h3>
        Notifications
        <small>

        </small>
    </h3>
</div>
<div id="page-content">
   <ul class="notifications-box">
       <?php foreach($notifications as $notification){ ?>
            <li>
                <a href="<?php echo base_url().$notification['link']; ?>"><span class="notification-text"><?php echo $notification['display_text']; ?></span></a>
                <div class="notification-time">
                    <?php echo date('M, d H:i A',strtotime($notification['date_created'])); ?>
                </div>
            </li>
       <?php } ?>
    </ul>    
</div>

