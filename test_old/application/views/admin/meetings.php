<div id="page-title">
    <h3>
        Meetings
        <small>
            
        </small>
    </h3>
</div>
<div id="page-content">
    <table class="table text-center">
        <thead>
            <tr>
                <th>Investor</th>
                <th class="text-center">Investee</th>
                <th class="text-center">Remark</th>
                <th class="text-center">Reject Remark</th>
                <th class="text-center">Time</th>
                <th class="text-center">Status</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach( $meetings as $meet){ ?>
            <tr>
                <td class="font-bold text-left"><?php echo $meet['investor_name'] ?></td>
                <td><?php echo $meet['investee_name'] ?></td>
                <td><?php echo $meet['remark'] ?></td>
                <td><?php echo $meet['reject_remark'] ?></td>
                <td><?php echo date('M, d H:i A', strtotime($meet['meeting_time'])); ?></td>
                <td><?php echo $meet['status']; ?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>