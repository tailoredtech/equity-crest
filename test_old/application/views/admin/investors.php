<div id="page-title">
    <h3>
        Form Fields
        <small>
            
        </small>
    </h3>
</div>
<div id="page-content">
    <table class="table text-center">
        <thead>
            <tr>
                <th>Name</th>
                <th class="text-center">Email</th>
                <th class="text-center">Mobile No</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach( $investors as $investor){ ?>
            <tr>
                <td class="font-bold text-left"><?php echo $investor['name'] ?></td>
                <td><?php echo $investor['email'] ?></td>
                <td><?php echo $investor['mob_no'] ?></td>
                <td>
                    <a href="<?php echo base_url(); ?>investor/view/<?php echo $investor['id'] ?>" class="btn small bg-blue-alt tooltip-button" data-placement="top" title="View">
                        <i class="glyph-icon icon-flag"></i>
                    </a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
