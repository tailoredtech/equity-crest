<div class="deal-single grid">
    <div class="deal-link">
        <?php if ($this->session->userdata('user_id')) { ?>
        <a href="<?php echo base_url(); ?>investee/view/<?php echo $user['user_id'] ?>"></a>
        <?php }else { ?>
        <a href="<?php echo base_url(); ?>user/login"></a>
        <?php } ?>
        <div class="title">
            <div class="deal-logo col-sm-5 text-center">
                    <div class="deal-image">
                        <img src="<?php echo base_url(); ?>uploads/users/<?php echo $user['user_id'] ?>/<?php echo $user['image'] ?>" >
                    </div>
            </div>
            <div class="deal-title-block col-sm-7">
                <?php if ($this->session->userdata('user_id')) { ?> 
                    <h2 class="deal-title lato-bold"><?php echo $user['company_name'] ?></h2>
                <?php } else { ?>
                    <h2 class="deal-title lato-bold"><?php echo $user['company_name'] ?></h2>
                <?php } ?>
                <div class="deal-location "><?php echo $user['city'] ?></div>
                <div class="deal-category "><?php echo $user['sector_name'] ?></div>
            </div>
        </div>
        <div class="deal-description">
            <p class="text-center center-block ">
                <?php echo character_limiter(strip_tags($user['products_services']), 60); ?> 
            </p>
        </div>
        <div class="deal-funds">
            <div class="fund-requested col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="funds-req pull-left">Fund Raise</div>
                <div class="funds-req-amount pull-right" style="width: 90px;">
                    <span style="float: right;"><?php echo format_money($user['investment_required']); ?></span>
                    <img style="height: 11px;margin-top: 4px;margin-right: 5px;float: right;" src='<?php echo base_url(); ?>assets/images/rupee.png' />
                </div>
            </div>
            <?php 
             $fund_raise = $user['fund_raise'];
             $investment_required = $user['investment_required'];
             if($investment_required){
                 $percent = ($fund_raise/$investment_required)*100;
             }else{
                 $percent = 0;
             }
             
            ?>
            <div class="funds-bar col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="progress">
                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $percent; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $percent,'%'; ?>">
                    </div>
                    <span class="sr-only">60% Complete</span>
                </div>
            </div>
            <div class="fund-stats col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="funds-raised col-lg-2 col-md-2 col-sm-2 col-xs-2">
                    <div class="text-center lato-bold"><?php echo floor($percent); ?>%</div>
                    <p class="text-center">Raised</p>
                </div>
                <div class="funds-raised middle col-lg-6 col-md-6 col-sm-6 col-xs-6  col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
                    <div class="text-center lato-bold">
                      <img style="height: 11px;display: inline" src='<?php echo base_url(); ?>assets/images/rupee.png' />
                      <span class="lato-bold"><?php echo format_money($user['commitment_per_investor']); ?></span> </div>
                    <p class="text-center">Min. Investment</p>
                </div>
                <div class="funds-raised col-lg-3 col-md-3 col-sm-3 col-xs-3">
                    <div class="text-center lato-bold"><?php echo DayDifference(date('Y-m-d'), $user['validity_period']); ?></div>
                    <p class="text-center">Days Left</p>
                </div>
            </div>
        </div>
    </div>
    <?php if($this->session->userdata('user_id')){ ?>
    <div class="deal-footer-links">
        <?php $followed = $this->session->userdata('followed'); 
            if(is_array($followed)){
         if(in_array($user['user_id'],$followed)){ ?>
               <div class="col-xs-4 text-center"><a href="javascript:void(0);" class="text-center unfollow" data-user-id="<?php echo $user['user_id']; ?>">Unfollow</a></div>
        <?php }else{ ?>
               <div class="col-xs-4 text-center"><a href="javascript:void(0);" class="text-center follow" data-user-id="<?php echo $user['user_id']; ?>">Follow</a></div>
        <?php }  }else{ ?>  
            <div class="col-xs-4 text-center"><a href="javascript:void(0);" class="text-center follow" data-user-id="<?php echo $user['user_id']; ?>">Follow</a></div>
            <?php } ?> 
            
        <div class="col-xs-4 text-center"><a href="javascript:void(0);" class="text-center ignore" data-user-id="<?php echo $user['user_id']; ?>">Ignore</a></div>
        
        <?php $pledged = $this->session->userdata('pledged'); 
         if(is_array($pledged)){
                
         if(in_array($user['user_id'],$pledged)){ ?>
            <div class="col-xs-4 text-center"><a href="javascript:void(0);" class="text-center" >Pledged</a></div>
        <?php }else{ ?>
            <div class="col-xs-4 text-center"><a href="javascript:void(0);" class="text-center pledge" data-user-id="<?php echo $user['user_id']; ?>">Pledge</a></div>
        <?php }  }else{ ?>
            <div class="col-xs-4 text-center"><a href="javascript:void(0);" class="text-center pledge" data-user-id="<?php echo $user['user_id']; ?>">Pledge</a></div>
        <?php } ?>
        
    </div>
    <?php }else{ ?>
    <div class="deal-footer-links">
        <div class="col-xs-4 text-center"><a href="<?php echo base_url(); ?>user/login" class="text-center">Follow</a></div>
        <div class="col-xs-4 text-center"><a href="<?php echo base_url(); ?>user/login" class="text-center">Ignore</a></div>
        <div class="col-xs-4 text-center"><a href="<?php echo base_url(); ?>user/login" class="text-center">Pledge</a></div>
    </div>
    <?php } ?>
</div>
