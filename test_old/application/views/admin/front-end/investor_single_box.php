<div class="row">                        
    <div class="profile-box">
        <div class="user-info row">
            <div class="col-sm-12">
                <div class="col-sm-5">
                    <div class="user-image">
                    <a href="<?php echo base_url(); ?>investor/view/<?php echo $user['user_id'] ?>">
                        <?php if($user['image']){ ?>
                        <img class="img-thumbnail" src="<?php echo base_url(); ?>uploads/users/<?php echo $user['user_id'] ?>/<?php echo $user['image']; ?>" >
                        <?php }else{ ?>
                        <img class="img-thumbnail" src="<?php echo base_url(); ?>uploads/investor.jpg" >
                        <?php } ?>
                    </a>
                </div>
                    </div>
                <div class="col-sm-7">
                    <div class="user-name lato-bold">
                        <?php echo $user['name']; ?>
                    </div>
                    <div class="user-location">
                        <?php echo $user['city']; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="user-bio">
            <?php echo character_limiter(strip_tags($user['experience']), 60); ?> 
        </div>

        <div class="user-investment">
            Invested Rs <?php echo $user['invested']; ?>
        </div>

        <div class="user-interests">
            <div class="user-sectors">
            <span><b>Sectors</b> :</span> <?php echo $user['mentoring_sectors']; ?>
            </div>
            <div  class="user-areas">
            <span><b>Areas</b> :</span> <?php echo $user['expertise']; ?>
            </div>
        </div>

    </div>
</div>
