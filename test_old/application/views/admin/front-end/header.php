<div class="container wrap">
    <div class="row">
        <header class="col-lg-12 col-md-12 col-sm-12 col-xs-12 header">
            <div class="logo col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <?php if($this->session->userdata('user_id')){ ?>
                <a href="<?php echo base_url(); ?><?php echo $this->session->userdata('role'); ?>/index"><img src="<?php echo base_url(); ?>assets/images/logo.png" class="header-logo"></a>
                <?php }else{ ?>
                <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/images/logo.png" class="header-logo"></a>
                <?php } ?>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 pull-right">
                <div class="top-links pull-right">
                    <?php if ($this->session->userdata('user_id') != '') { ?>
                        <a class="logout" href="<?php echo base_url(); ?>user/logout">Sign Out</a>
                        <div class="dropdown login-dropdown">
                            <a id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="/page.html">
                                <?php echo $this->session->userdata('name'); ?> <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                <li><a href="<?php echo base_url(); ?>user/account_setting">Account Settings</a></li>
                            </ul>
                        </div>
                    <?php } else { ?>
                        <a href="<?php echo base_url(); ?>user/login">SIGN IN</a>
                        <a href="<?php echo base_url(); ?>user/register">JOIN NOW</a>
                    <?php } ?>
                </div>
                <?php if(!$this->session->userdata('user_id')){ ?>
                <div class="top-nav  col-sm-12 ">
                    <nav>
                        <ul class="pull-right">
                            <li class="pull-left">
                                <a href="<?php echo base_url(); ?>user/register?user=investor" class=" text-center">
                                    INVESTOR
                                </a>
                            </li>
                            <li class="pull-left">
                                <a href="<?php echo base_url(); ?>user/register?user=investee" class=" text-center">
                                    PORTFOLIO
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <?php }else {?>
                <div class="top-nav  col-sm-12 ">
                    <nav>
                        <ul class="pull-right">
                            <li class="pull-left">
                                <a href="<?php echo base_url(); ?>home/investor_all" class=" text-center">
                                    INVESTOR
                                </a>
                            </li>
                            <li class="pull-left">
                                <a href="<?php echo base_url(); ?>home/portfolio_all" class=" text-center">
                                    PORTFOLIO
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <?php }?>
                
                
            </div>
        </header>
    </div>
</div>