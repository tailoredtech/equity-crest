<div class="container wrap">
    <form action="<?php echo base_url(); ?>investor/search" method="get">
        <div class="row">
            <div class="search-input">
                <input type="text" name="name" value="" class="form-control form-input"  placeholder="Name" style="margin-left: 20px;width: 160px;">
            </div>
            <div class="search-input">
                <div class="select-style">
                    <select class="form-control form-input" name="sector"  placeholder="">
                        <option value="">By Sector</option>
                        <?php foreach ($sectors as $sector) { ?>
                            <option value='<?php echo $sector['id']; ?>' <?php echo (isset($searchOptions['sector']) && $searchOptions['sector'] == $sector['id'] ) ? 'selected'  : ''; ?>><?php echo $sector['name']; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="search-input">
                <div class="select-style">
                    <select class="form-control form-input" name="stage"  placeholder="">
                        <option value="">By Stage</option>
                        <?php foreach ($stages as $stage) { ?>
                            <option value='<?php echo $stage['id']; ?>' <?php echo (isset($searchOptions['stage']) && $searchOptions['stage'] == $stage['id'] ) ? 'selected'  : ''; ?> ><?php echo $stage['name']; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>        
        <div class="search-input">
            <div class="select-style">
                <select class="form-control form-input" name="size"  placeholder="">
                    <option value="">By Investment Size</option>
                    <option value="1" <?php echo (isset($searchOptions['size']) && $searchOptions['size'] == 1 ) ? 'selected'  : ''; ?> >Upto 10 lakhs</option>
                    <option value="2" <?php echo (isset($searchOptions['size']) && $searchOptions['size'] == 2 ) ? 'selected'  : ''; ?> >10 lakhs - 50 lakhs</option>
                    <option value="3" <?php echo (isset($searchOptions['size']) && $searchOptions['size'] == 3 ) ? 'selected'  : ''; ?> >50 lakhs - 1 Crore</option>
                    <option value="4" <?php echo (isset($searchOptions['size']) && $searchOptions['size'] == 4 ) ? 'selected'  : ''; ?> >1 Crore - 5 Crore</option>
                    <option value="5" <?php echo (isset($searchOptions['size']) && $searchOptions['size'] == 5 ) ? 'selected'  : ''; ?> >5 Crore & Above</option>
                </select>
            </div>
        </div>
        <div class="search-input">
            <div class="select-style">
                <select class="form-control form-input" name="location"  placeholder="">
                    <option value="">By Location</option>
                    <option value="Mumbai" <?php echo (isset($searchOptions['location']) && $searchOptions['location'] == 'Mumbai' ) ? 'selected'  : ''; ?> >Mumbai</option>
                    <option value="Delhi" <?php echo (isset($searchOptions['location']) && $searchOptions['location'] == 'Delhi' ) ? 'selected'  : ''; ?> >Delhi</option>
                    <option value="Chennai" <?php echo (isset($searchOptions['location']) && $searchOptions['location'] == 'Chennai' ) ? 'selected'  : ''; ?> >Chennai</option>
                    <option value="Bengaluru" <?php echo (isset($searchOptions['location']) && $searchOptions['location'] == 'Bengaluru' ) ? 'selected'  : ''; ?> >Bengaluru</option>
                    <option value="Hyderabad" <?php echo (isset($searchOptions['location']) && $searchOptions['location'] == 'Hyderabad' ) ? 'selected'  : ''; ?> >Hyderabad</option>
                    <option value="6" <?php echo (isset($searchOptions['location']) && $searchOptions['location'] == 6 ) ? 'selected'  : ''; ?> >Others</option>
                </select>
            </div>
        </div>
            <div class="search-input">
                <input type="submit" class="btn search-btn" value="Search" />
            </div>
        </div>
    </form>
</div>
</div>
