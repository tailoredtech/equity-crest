<div class="container-fluid footer-links">
    <div class="row">
        <div class="container wrap">
            <div class="row">
                <div class="top-footer col-sm-12">
                    <div class="footer-nav-block">
                        <ul>
                            <li >
                                <a href="<?php echo base_url(); ?>home/about_us" class="">About Us</a>
                            </li>
                            <li >
                                <a href="<?php echo base_url(); ?>home/management_team" class="">Management Team</a>
                            </li>
                            <li >
                                <a href="<?php echo base_url(); ?>home/partners" class="">Partners</a>
                            </li>
                            <li >
                                <a href="<?php echo base_url(); ?>home/advisory_board" class="">Advisory Board</a>
                            </li>
                            <li >
                                <a href="<?php echo base_url(); ?>home/how_it_works" class="">How Equity Crest Works</a>
                            </li>
                            <li >
                                <a href="<?php echo base_url(); ?>home/faqs" class="">FAQ</a>
                            </li>
                        </ul>
                    </div>
                    <div class="footer-nav-block">
                        <ul>
                            <?php if(!$this->session->userdata('user_id')){ ?>
                                <li>
                                    <a href="<?php echo base_url(); ?>user/register?user=investor" class="">Investor</a>
                                </li>
                                <li >
                                    <a href="<?php echo base_url(); ?>user/register?user=investee" class="">Portfolio</a>
                                </li>
                                <li >
                                    <!--<a href="<?php echo base_url(); ?>user/register?user=channel_partner" class="">Become A Channel Partner</a>-->
                                </li>
                             <?php }else {?>
                                <li>
                                    <a href="<?php echo base_url(); ?>home/investor_all" class="">Investor</a>
                                </li>
                                <li >
                                    <a href="<?php echo base_url(); ?>user/home/portfolio_all" class="">Portfolio</a>
                                </li>
                                <li >
                                    <!--<a href="javascript:void(0);" class="">Become A Channel Partner</a>-->
                                </li>
                            <?php } ?>
                            
                            <li >
                                <!--<a href="<?php echo base_url(); ?>home/women_entrepreneurship" class="">Women Entrepreneurship</a>-->
                            </li>




                        </ul>
                    </div>
                    <div class="footer-nav-block">
                        <ul>

                            <li >
                                <a href="<?php echo base_url(); ?>home/news_top_stories" class="">Media/Press centre</a>
                            </li>
                            <li >
                                <a href="<?php echo base_url(); ?>home/events" class="">Events</a>
                            </li>
                            <li >
                                <a href="<?php echo base_url(); ?>home/blogs" class="">Blogs</a>
                            </li>
                            <li >
                                <a href="javascript:void(0)" id="subscribe">Subscribe</a>
                            </li>
                        </ul>
                    </div>

                    <div class="footer-nav-block">
                        <ul>
                            <li >
                                <a href="<?php echo base_url(); ?>home/contact_us" class="">Contact Us</a>
                            </li>
                            <li >
                                <a href="<?php echo base_url(); ?>home/careers" class="">Careers</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="container bottom-footer">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="socia-links">
                        <li class="pull-right" >
                            <span>Follow Us : </span>
                            <a target="_blank" href="#"><img src="<?php echo base_url(); ?>assets/images/socia1.png"></a>
                            <a target="_blank" href="https://www.linkedin.com/company/3807730?trk=vsrp_companies_res_name&trkInfo=VSRPsearchId%3A365969681411721454547%2CVSRPtargetId%3A3807730%2CVSRPcmpt%3Aprimary"><img src="<?php echo base_url(); ?>assets/images/socia2.png"></a>
                            <a target="_blank" href="https://www.facebook.com/EquityCrest"><img src="<?php echo base_url(); ?>assets/images/socia3.png"></a>
                            <a target="_blank" href="https://twitter.com/EquityCrest"><img src="<?php echo base_url(); ?>assets/images/socia4.png"></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid footer-bottom">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul>

                        <li class="pull-left" >
                            Copyrights of EQ Advisors Pvt. Ltd , 2014
                        </li>
                        <li class="pull-right" >
                            <a href="#" class="">Cookie</a>
                        </li>
                        <li class="pull-right" >
                            <a href="<?php echo base_url(); ?>home/privacy_policy" class="">Privacy</a>
                        </li>
                        <li class="pull-right" >
                            <a href="#" class="">Security</a>
                        </li>
                        <li class="pull-right" >
                            <a href="<?php echo base_url(); ?>home/terms_of_use" class="">Terms of Use</a>
                        </li>
                        <li class="pull-right" >
                            <span href="#" class="bold">Policies:</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
        <!-- POPUP CONTAINER -->
        <div style="display: none;">
            <div class="pop-content" id="subsciber-box">
                <h3 class="form-header">Subscribe to our newsletter</h3>

                <div class="sendto">  Enter your email address to receive all news from our website</div>
                <div class="askexpert">
                    <form id="subscriber-form">
                        <input type="text" name="email" class="form-control" style="width: 400px;" placeholder="your email address">
                        <div class="grey-font subscribe-line">Don't worry you'll not be spammed</div>
                        
                        <div class="center-block text-center">
                            <input class="btn btn-green" type="submit" value="Subscribe" />
                        </div>
                        
                    </form>
                </div>

            </div>
        </div>
        <script>
            $(document).ready(function() {
//                $("#subscribe").colorbox({inline: true, href: '#subsciber-box', innerWidth: '450px'});
//                
//                $("#subscriber-form").on('submit', (function(e) {
//                    e.preventDefault();
//                    $.ajax({
//                        url: "<?php echo base_url(); ?>user/subscribe_process",
//                        type: 'POST',
//                        data: new FormData(this),
//                        contentType: false,
//                        cache: false,
//                        processData: false
//                    }).done(function() {
//                        $.colorbox.close();
//                    });
//
//                }));
            });
         </script>