<div class="container wrap">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 panel-box">
            <div class="panel-group " id="panels-container">
                <div class="panel panel-default">
                    <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="#collapseOne" data-parent="#panels-container" data-toggle="collapse" class="center-block text-center collapsed">
                                    <span class="glyphicon glyphicon-chevron-down dropicon"></span><span class="collapse-title">Platform Statistics</span>
                                </a>
                            </h4>
                        </div>
                    <div id="collapseOne" class="panel-collapse collapse">
                        <div class="panel-body">
                            <!--<img src='<?php echo base_url(); ?>assets/images/stats.jpg' />-->
                            <h5 class="text-center">Coming Soon</h5>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    
                    <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="#collapseTwo" data-parent="#panels-container" data-toggle="collapse" class="center-block text-center collapsed">
                                    <span class="glyphicon glyphicon-chevron-down dropicon"></span><span class="collapse-title">Bids & Offers</span>
                                </a>
                            </h4>
                        </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                            <h5 class="text-center">Coming Soon</h5>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="#collapseThree" data-parent="#panels-container" data-toggle="collapse" class="center-block text-center collapsed">
                                    <span class="glyphicon glyphicon-chevron-down dropicon"></span><span class="collapse-title">Portfolio in Media</span> 
                                </a>
                            </h4>
                        </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                            <h5 class="text-center">Coming Soon</h5>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="#collapseFour" data-parent="#panels-container" data-toggle="collapse" class="center-block text-center collapsed">
                                    <span class="glyphicon glyphicon-chevron-down dropicon"></span><span class="collapse-title">EQ in Media</span> 
                                </a>
                            </h4>
                        </div>
                    <div id="collapseFour" class="panel-collapse collapse">
                        <div class="panel-body">
                            <h5 class="text-center">Coming Soon</h5>
                        </div>
                    </div>
                </div>
                
                <div class="panel panel-default">
                    <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="#collapseFive" data-parent="#panels-container" data-toggle="collapse" class="center-block text-center collapsed">
                                    <span class="glyphicon glyphicon-chevron-down dropicon"></span><span class="collapse-title">Customers Speak</span> 
                                </a>
                            </h4>
                        </div>
                    <div id="collapseFive" class="panel-collapse collapse">
                        <div class="panel-body">
                            <h5 class="text-center">Coming Soon</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(".panel-heading").click(function(){
        $(".panel-group .panel-active").removeClass("panel-active");
        $(this).addClass("panel-active");
    });
</script>    