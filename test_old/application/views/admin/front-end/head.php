<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>EquityCrest Home</title>
    
    <?=css('bootstrap.min.css')?>
    <?=css('style.css')?>
    <?=css('jquery.slick.css')?>
    <?= css('colorbox.css') ?>
    
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <?=js('bootstrap.min.js')?>
    <?= js('slick.min.js') ?>
    <?= js('jquery.colorbox.js') ?>
    <!--[if IE]>
            <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>