<div id="top-bar-slider" class="slider-markup banner" data-slide-mode="fade" data-slide-max="1" data-slide-min="1" data-slide-pager="true">
    <?php $count = 0; ?>
    <?php foreach ($banners as $banner): ?>
        <div class="item <?php if ($count == 0) {echo 'active';} ?>" data-active-bg="<?php echo $banner->background_color; ?>" style="background-color:<?php echo $banner->background_color; ?>">
        <h2 class="text-center titillium"><?php echo $banner->banner_title; ?></h2>
        <p class="text-center"><?php echo $banner->banner_text; ?></p>
        </div>
        <?php
        $count++;
    endforeach;
    ?>
</div>