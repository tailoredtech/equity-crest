<div id="page-title">
    <h3>
       Subscribed Users
        <small>

        </small>
    </h3>
</div>
<div id="page-content">
    <form id="update-investees-form" action="<?php echo base_url(); ?>admin/update_investees" method="post">
       
        <table class="table text-center">
            <thead>
                <tr>
                    <th class="text-center">Email</th>
                    
                </tr>
            </thead>
            <tbody>
                <?php foreach ($users as $user) { ?>
                    <tr>
                        <td><?php echo $user['email'] ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </form>  
</div>
