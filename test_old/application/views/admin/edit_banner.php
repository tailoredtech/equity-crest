<div id="page-title">
    <h3>
        Edit Banner
    </h3>
</div>
<div class="example-box">
    <div class="example-code clearfix">

        <form class="form-bordered" action="<?php echo base_url();?>admin/edit_banner" method="post"/>
        <input type="hidden" name="id" id="" value="<?php echo $banner['id']; ?>"/>
        <div class="form-row">
            <div class="form-label col-md-2">
                <label for="">
                    Banner Title:
                </label>
            </div>
            <div class="form-input col-md-10">
                <input type="text" name="banner_title" id="" value="<?php echo $banner['banner_title']; ?>"/>
            </div>
        </div>
        <div class="form-row">
            <div class="form-label col-md-2">
                <label for="">
                    Banner Text:
                </label>
            </div>
            <div class="form-input col-md-10">
                <input type="text" name="banner_text" id="" value="<?php echo $banner['banner_text']; ?>"/>
            </div>
        </div>
        <div class="form-row">
            <div class="form-label col-md-2">
                <label for="">
                    Banenr Color:
                </label>
            </div>
            <div class="form-input col-md-10">
                <input type="text" name="background_color" id="" value="<?php echo $banner['background_color']; ?>"/>
            </div>
        </div>
        <div class="form-input col-md-2 col-md-offset-2">
            <input class="btn primary-bg medium" type="submit" value="Save"/>
        </div>

        </form>

    </div>

</div>