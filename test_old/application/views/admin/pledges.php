<div id="page-title">
    <h3>
        Pledges
    </h3>
</div>
<pre><?php //print_r($pledges); ?></pre>
<div id="page-content">
    <table class="table  investee-table ">
                                    <tr>
                                        <th>Investor</th>
                                        <th>Amount</th>
                                        <th>Approval Status</th>
                                     
                                    </tr>
                                    <?php foreach ($pledges as $pledge):?>
                                    <tr>
                                        <td><?php echo $pledge['investor_name'] ?></td>
                                        <td><?php echo $pledge['investment'] ?></td>
                                        <td>
                                            <select id="approval">
                                                <option data-id="<?php echo $pledge['id'] ?>" data-investee-id="<?php echo $pledge['investee_id'] ?>" data-investment="<?php echo $pledge['investment'] ?>" value="1" <?php echo ($pledge['approval'] == 1) ? 'selected' : ''; ?>>yes</option>
                                                <option data-id="<?php echo $pledge['id'] ?>" data-investee-id="<?php echo $pledge['investee_id'] ?>" data-investment="<?php echo $pledge['investment'] ?>" value="0" <?php echo ($pledge['approval'] == 0) ? 'selected' : ''; ?>>no</option>
                                            </select>
                                            </td>
                                    </tr>
                                    <?php endforeach;?>
    </table>
</div>
<script>
    $(document).ready(function() {
       $("#approval").on("change", function() {
            var approval = $(this).val();
            var id = $(this).data("id");
            var investee_id = $(this).data("invetstee-id");
            var investment = $(this).data("investment");
            $.post("<?php echo base_url(); ?>admin/pledge_approval",{id:id,investee_id:investee_id,investment:investment,approval:approval});
        }); 
    });
</script>