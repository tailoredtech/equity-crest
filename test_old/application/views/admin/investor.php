<div id="page-title">
    <h3>
        Investor
        <small>
            
        </small>
    </h3>
</div>
<?php //print_r($investor); ?>
<div id="page-content">
    <div class="form-row">
        <div class="col-md-4">
            Name :
        </div>
        <div class="col-md-8">
            <?php if(isset($investor['name']))
            echo $investor['name']; ?>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-4">
            City :
        </div>
        <div class="col-md-8">
            <?php if(isset($investor['city']))
                echo $investor['city']; ?>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-4">
            Email :
        </div>
        <div class="col-md-8">
            <?php if(isset($investor['email']))
                echo $investor['email']; ?>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-4">
            Contact :
        </div>
        <div class="col-md-8">
            <?php if(isset($investor['mob_no']))
                echo $investor['mob_no']; ?>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-4">
            Type :
        </div>
        <div class="col-md-8">
            <?php if(isset($investor['type']))
                echo $investor['type']; ?>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-4">
            City :
        </div>
        <div class="col-md-8">
            <?php if(isset($investor['role']))
            echo $investor['role']; ?>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col-md-4">
            Experience :
        </div>
        <div class="col-md-8">
            <?php if(isset($investor['experience']))
                echo $investor['experience']; ?>
        </div>
    </div>
    
</div>    