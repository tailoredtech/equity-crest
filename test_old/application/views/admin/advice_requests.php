<div id="page-title">
    <h3>
        Advice Requests
        <small>
            
        </small>
    </h3>
</div>
<div id="page-content">
    <table class="table text-center">
        <thead>
            <tr>
                <th>Asked by</th>
                <th class="text-center">Query</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach( $requests as $request){ ?>
            <tr>
                <td class="font-bold text-left"><?php echo $request['asked_by'] ?></td>
                <td><?php echo $request['query'] ?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>