<div id="page-title">
    <h3>
        Inactive Investees
        <small>

        </small>
    </h3>
</div>
<div id="page-content">
    <form id="update-investees-form" action="<?php echo base_url(); ?>admin/update_investees" method="post">
        <div class="fg-toolbar ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix">
            <div id="example1_length" class="dataTables_length">
                <label>Status <select name="status" id="change-status">
                        <option value="" selected="selected">--Select to change --</option>
                        <option value="active">Active</option>
                    </select></label>
            </div>
        </div>
        <table class="table text-center">
            <thead>
                <tr>
                    <th></th>
                    <th class="text-left">Name</th>
                    <th class="text-center">Company Name</th>
                    <th class="text-center">Email</th>
                    <th class="text-center">Mobile</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($investees as $investee) { ?>
                    <tr>
                        <td><input type="checkbox" name="id[]" value="<?php echo $investee['id'] ?>" style="width: 25px"/></td>
                        <td class="font-bold text-left"><?php echo $investee['name'] ?></td>
                        <td><?php echo $investee['company_name'] ?></td>
                        <td><?php echo $investee['email'] ?></td>
                        <td><?php echo $investee['mob_no'] ?></td>
                        <td>
                            <a href="<?php echo  base_url()."admin/investee/".$investee['id'];?>" class="btn small bg-blue-alt tooltip-button" data-placement="top" title="View">
                                <i class="glyph-icon icon-flag"></i>
                            </a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </form>  
</div>
<script>
$('document').ready(function(){
    $('#change-status').on('change',function(){
       if($(this).val() != ''){
            $('#update-investees-form').submit();
        }
    });
});
</script>