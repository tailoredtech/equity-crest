<div class="main">

    <div class="container">                            
        <!-- start find deals -->
        <div id="offers-deals" class="container-fluid deals-block offers-block offers-block-1">
            <div class="row">
                <div class="container">
                    <div class="row block-header">
                        <div class="col-sm-4 block-title">
                            <h2 class="lato-bold">Deals as Prefrence</h2>
                        </div>
                        <div class="right-options col-lg-2 col-md-2 col-sm-2 col-xs-2 pull-right">
                            <div class="slider-marker pull-right">
                                <a class=" text-center" href="#">
                                    <img src="<?php echo base_url(); ?>assets/images/dot.png">
                                </a>
                            </div>
                            <div class="extra-options pull-right">
                                <a class="extra-options-link text-center" href="#">=</a>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="slider-markup col-sm-12 deals-slider" data-slide-mode="slide" data-slide-max="3" data-slide-min="3" data-slide-pager="false">
                            <?php
                                $max=count($users);
                                $count=0;
                                foreach($users as $user){
                                    $data['user']=$user;
                                    $this->load->view('front-end/grid-single-item',$data);
                                    $count++;
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end find deals -->

        <!-- start recommended deals -->
        <div id="offers-deals" class="container-fluid deals-block offers-block offers-block-1">
            <div class="row">
                <div class="container">
                    <div class="row block-header">
                        <div class="col-sm-4 block-title">
                            <h2 class="lato-bold">Deals as Prefrence</h2>
                        </div>
                        <div class="right-options col-lg-2 col-md-2 col-sm-2 col-xs-2 pull-right">
                            <div class="slider-marker pull-right">
                                <a class=" text-center" href="#">
                                    <img src="<?php echo base_url(); ?>assets/images/dot.png">
                                </a>
                            </div>
                            <div class="extra-options pull-right">
                                <a class="extra-options-link text-center" href="#">=</a>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="slider-markup col-sm-12 deals-slider" data-slide-mode="slide" data-slide-max="3" data-slide-min="3" data-slide-pager="false">
                            <?php
                                $max=count($users);
                                $count=0;
                                foreach($users as $user){
                                    $data['user']=$user;
                                    $this->load->view('front-end/grid-single-item',$data);
                                    $count++;
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end recommended deals -->
        
        
        <div class="row">
            <h4 class="form-header  col-xs-12 ">Statistics</h4>
        </div>

        <div class="row content-box" style="text-align: center">
            <img src="<?php echo base_url(); ?>assets/images/stats.jpg">
        </div>

        <!-- start news box-->
        <div class="row content-box news">

            <div class="col-sm-12">
                <div id="panels-container" class="panel-group ">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="#collapseOne" data-parent="#panels-container" data-toggle="collapse" class="center-block text-center collapsed">
                                    <span class="collapse-title">Sector Performance</span><span class="glyphicon glyphicon-chevron-down dropicon"></span>
                                </a>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse" id="collapseOne" style="height: 0px;">
                            <div class="panel-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="#collapseTwo" data-parent="#panels-container" data-toggle="collapse" class="center-block text-center collapsed">
                                    <span class="collapse-title">Performace Tracker</span><span class="glyphicon glyphicon-chevron-down dropicon"></span>
                                </a>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse" id="collapseTwo" style="height: 0px;">
                            <div class="panel-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="#collapseThree" data-parent="#panels-container" data-toggle="collapse" class="center-block text-center collapsed">
                                    <span class="collapse-title">Latest News</span><span class="glyphicon glyphicon-chevron-down dropicon"></span>
                                </a>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse" id="collapseThree" style="height: 0px;">
                            <div class="panel-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="#collapseThree" data-parent="#panels-container" data-toggle="collapse" class="center-block text-center collapsed">
                                     <span class="collapse-title">Whats Hot Now</span><span class="glyphicon glyphicon-chevron-down dropicon"></span>
                                </a>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse" id="collapseThree" style="height: 0px;">
                            <div class="panel-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end news box-->

    </div>
</div>
<script>
    $(document).ready(function(){
        
    });
</script>