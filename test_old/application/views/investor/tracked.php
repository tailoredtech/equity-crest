    <div class="container-fluid deals-block ">
        <div class="row">
            <div class="col-sm-12">
                <div class="container wrap">
                    <div class="row block-header">
                        <div class="col-sm-2 block-title">
                            <h2>Pledged</h2>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            
                <div class="container wrap">
                    <div class="row">
                        <?php $max = count($pledgedInvestees); if($max){  ?>
                        <div class="col-sm-12 slider-markup  deals-slider" data-slide-mode="slide" data-slide-max="3" data-slide-min="3" data-slide-pager="false">
                            <?php
                            $count = 0;
                            foreach ($pledgedInvestees as $user) {
                                $data['user'] = $user;
                                $this->load->view('front-end/grid-single-item', $data);
                                $count++;
                            }
                            ?>
                        </div>
                         <?php }else{ ?>
                            <div class="col-sm-12 space-20 success-msg" >
                                <div class="alert alert-success" role="alert">This section will display information on the opportunities that you have pledged money to.</div>
                            </div>
                        <?php } ?>
                    </div>
                    
                </div>
            
        </div>
    </div>
    
    <div class="container-fluid deals-block ">
        <div class="row">
            <div class="col-sm-12">
                <div class="container wrap">
                    <div class="row block-header">
                        <div class="col-sm-2 block-title">
                            <h2>Following</h2>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            
                <div class="container wrap">
                    <div class="row">
                        <?php $max = count($followedInvestees); if($max){  ?>
                        <div class="slider-markup col-sm-12 deals-slider" data-slide-mode="slide" data-slide-max="3" data-slide-min="3" data-slide-pager="false">
                            <?php
                            $max = count($followedInvestees);
                            $count = 0;
                            foreach ($followedInvestees as $user) {
                                $data['user'] = $user;
                                $this->load->view('front-end/grid-single-item', $data);
                                $count++;
                            }
                            ?>
                        </div>
                         <?php }else{ ?>
                            <div class="col-sm-12 space-20 success-msg" >
                                <div class="alert alert-success" role="alert">This section will display information on the opportunities that you are following.</div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            
        </div>
    </div>
