<div class="container wrap">
    <!-- start find deals -->


    <div class="row ">
        <div class="filter-options col-sm-2 col-sm-offset-5">
            <div class="dropdown preff-drop">
                <a href="javascript:void(0);" class="prefrence-details" data-toggle="dropdown">Preference<span class="glyphicon glyphicon-chevron-down dropicon"></span></a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="container wrap">
            <div class="pref-panel" <?php echo ($pref_set) ? 'style="display: none;"' : ''; ?> >
                <!--                        <div class="container">-->

                <form id="set-preferences-form" action="<?php echo base_url(); ?>investor/set_preferences" method="post">
                    <div class="row">
                        <!--                                    <div class="pull-right" style="position: absolute;right: 28px;top:10px;">
                                                                <span class="glyphicon glyphicon-remove"></span>
                                                            </div>-->
                        <h4 class="form-header  col-sm-12 ">Set your Preferences</h4>
                        <div class="content-box">

                            <div class="row">
                                <div class="col-sm-3 col-sm-offset-1">
                                    <h5 class="lato-bold">By Sector</h5>
                                </div>
                                <div class="col-sm-2"><h5 class="lato-bold">By Investment Size</h5></div>
                                <div class="col-sm-2"><h5 class="lato-bold">By Location</h5></div>
                                <div class="col-sm-2"><h5 class="lato-bold">By Investment Stage</h5></div>
                                <div class="col-sm-2"><h5 class="lato-bold">By Others</h5></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3 col-sm-offset-1" style=" height: 250px; overflow: auto; ">

                                    <?php foreach ($sectors as $sector) { ?>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="sector[]" value="<?php echo $sector['id']; ?>" <?php echo (in_array($sector['id'], $preferred_sectors)) ? 'checked' : ''; ?>>
                                                <?php echo $sector['name']; ?>
                                            </label>
                                        </div>
                                    <?php } ?>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="sector[]" value="0">
                                            All
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="size[]" value="1" <?php echo (in_array(1, $preferred_sizes)) ? 'checked' : ''; ?>>
                                            Upto 10 Lakhs
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="size[]" value="2" <?php echo (in_array(2, $preferred_sizes)) ? 'checked' : ''; ?>>
                                            10 Lakhs - 50 Lakhs
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="size[]" value="3" <?php echo (in_array(3, $preferred_sizes)) ? 'checked' : ''; ?>>
                                            50 Lakhs - 1 Crore
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="size[]" value="4" <?php echo (in_array(4, $preferred_sizes)) ? 'checked' : ''; ?>>
                                            1 Crore - 5 Crore
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="size[]" value="5" <?php echo (in_array(5, $preferred_sizes)) ? 'checked' : ''; ?>>
                                            5 Crore & Above
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="size[]" value="6" <?php echo (in_array(6, $preferred_sizes)) ? 'checked' : ''; ?>>
                                            All
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="location[]" value="1" <?php echo (in_array(1, $preferred_locations)) ? 'checked' : ''; ?>>
                                            Mumbai
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="location[]" value="2" <?php echo (in_array(2, $preferred_locations)) ? 'checked' : ''; ?>>
                                            Delhi
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="location[]" value="3" <?php echo (in_array(3, $preferred_locations)) ? 'checked' : ''; ?>>
                                            Chennai
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="location[]" value="4" <?php echo (in_array(4, $preferred_locations)) ? 'checked' : ''; ?>>
                                            Bengaluru
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="location[]" value="5" <?php echo (in_array(5, $preferred_locations)) ? 'checked' : ''; ?>>
                                            Hyderabad
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="location[]" value="6" <?php echo (in_array(6, $preferred_locations)) ? 'checked' : ''; ?>>
                                            Others
                                        </label>
                                    </div>

                                </div>
                                <div class="col-sm-2"><div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="stage[]" value="3" <?php echo (in_array(3, $preferred_stages)) ? 'checked' : ''; ?>>
                                            Beta Launched
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="stage[]" value="1" <?php echo (in_array(1, $preferred_stages)) ? 'checked' : ''; ?>>
                                            Ideation
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="stage[]" value="4" <?php echo (in_array(4, $preferred_stages)) ? 'checked' : ''; ?>>
                                            Early Revenue
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="stage[]" value="5" <?php echo (in_array(5, $preferred_stages)) ? 'checked' : ''; ?>>
                                            Steady Revenue
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="stage[]" value="2" <?php echo (in_array(2, $preferred_stages)) ? 'checked' : ''; ?>>
                                            Proof of Concept
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="stage[]" value="6" <?php echo (in_array(6, $preferred_stages)) ? 'checked' : ''; ?>>
                                            All
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="other[]" value="1" <?php echo (in_array(1, $preferred_others)) ? 'checked' : ''; ?>>
                                            Recently Added
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="other[]" value="2" <?php echo (in_array(2, $preferred_others)) ? 'checked' : ''; ?>>
                                            Top Funded
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="other[]" value="3" <?php echo (in_array(3, $preferred_others)) ? 'checked' : ''; ?>>
                                            Featured
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="other[]" value="4" <?php echo (in_array(4, $preferred_others)) ? 'checked' : ''; ?>>
                                            All
                                        </label>
                                    </div>
                                </div>
                                <input id="save-preff" class="join-btn" type="submit" value="Save"/>
                            </div>
                        </div>
                    </div>



                </form>
                <!--                        </div>-->
            </div>   
        </div>
    </div>
</div>
<!-- end find deals -->
<!-- start find deals -->
<div class="container-fluid deals-block ">
    <div class="row">

        <div class="container wrap">
            <div class="row block-header">
                <div class="col-sm-2 block-title">
                    <h2>Preferred</h2>
                </div>

            </div>
        </div>
    </div>

    <div class="row">

        <div class="container wrap">
            <div class="row">
                <?php $max = count($users);
                if ($max) {
                    ?>  
                    <div class="slider-markup col-sm-12 deals-slider" data-slide-mode="slide" data-slide-max="3" data-slide-min="3" data-slide-pager="false">
                        <?php
                        $count = 0;
                        foreach ($users as $user) {
                            $data['user'] = $user;
                            if ($user['status'] == 'active' && (DayDifference(date('Y-m-d'), $user['validity_period']) > 0)) {
                                $this->load->view('front-end/grid-single-item', $data);
                            }
                            $count++;
                        }
                        ?>
                    </div>
<?php } else { ?>
                    <div class="col-sm-12 space-20 success-msg" >
                        <div class="alert alert-success" role="alert">There are No Opportunities available as per your search criteria.</div>
                    </div>
<?php } ?>
            </div>
        </div>

    </div>
</div>
<!-- end find deals -->
<script>
    $(document).ready(function() {

        $('.prefrence-details').on('click', function(event) {
            event.stopPropagation();
            $('.pref-panel').slideToggle('slow');
        });

        $(".pref-panel").on("click", function(event) {
            event.stopPropagation();
        });
    });

    $(document).click(function() {
        if ($('.pref-panel').is(':visible')) {
            $('.pref-panel').slideToggle('slow');
        }
    });
</script>