    <div class="container-fluid deals-block ">
        <div class="row">
            <div class="col-sm-12">
                <div class="container">
                    <div class="row block-header">
                        <div class="col-sm-5 block-title">
                            <h2>My Investments</h2>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="container wrap">
                    <div class="row">
                        <div class="col-sm-12 space-20 success-msg" >
                            <div class="alert alert-success" role="alert">This section will display information on the investments that you make through Equity Crest</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script>
    $(document).ready(function(){
       if($('.deals-slider').length){
            slider=$('.deals-slider').slick({
                slidesToShow: 3,
                slidesToScroll: 3,
                dots: false,
                autoplay: false,
                autoplaySpeed:5000,
                arrows:true,
                nextArrow:'<button type="button" class="slick-next glyphicon glyphicon-chevron-right"><span class="glyphicon glyphicon-chevron-right"></span></button>'
            }); 
       }
    });
</script>