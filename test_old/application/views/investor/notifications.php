<div class="main">
    <div class="container wrap">
        <div class="row">
            <h4 class="form-header  col-sm-12 ">Notifications</h4>
        </div>
        <div class="content-box row">
            <ul class="list-group">
                <?php foreach ($notifications as $notification) { ?>
                <li class="list-group-item">
                    <div class="notification">
                        <span class="notification-thumb img-thumbnail" style="background-image: url('<?php echo base_url(); ?>uploads/users/<?php echo $notification['sender_id'] ?>/<?php echo $notification['image']; ?>');  background-size: 100%; "></span> 
                        <span class="notification-text"><?php echo $notification['display_text']; ?></span>
                        <span class="notification-time"><?php echo date('M, d H:i A', strtotime($notification['date_created'])); ?></span>
                        <?php if($notification['type'] == 'access' || $notification['type'] == 'meeting'){ ?>
                        
                        <div class="notification-action-btns">
                            <div class="small-msg" style="display:none;"></div>
                            <?php if($notification['type'] == 'meeting'){ ?>
                               <a class="view-msg-link" data-record-id="<?php echo $notification['record_id']; ?>" href="javascript:void(0);">View Message</a>
                            <?php } ?>
                            
                        </div>
                        <?php } ?>
                    </div>
                </li>
              <?php } ?>
            </ul>
            
        </div>

    </div>
</div>
<div style="display: none;">
    <div class="pop-content" id="reject-remark">
        <h3 class="form-header">Reject Remark</h3>
        <div class="askexpert">
            <form id="reject-remark-form">
                <textarea name="reject_remark" placeholder="Reject Remark" cols="54" rows="8"></textarea>
                <input type="hidden" name="meeting_id" class="meeting-id" value="" />
                <input type="hidden" name="notification_id" class="notification-id" value="" />
                <div class="buttons">
                    <input class="btn btn-green" type="submit" value="send" />
                </div>
            </form>
        </div>

    </div>
</div>
<script>
    $(document).ready(function(){
        $('.view-msg-link').on('click',function(){
            var record_id = $(this).attr('data-record-id');
            var notification = $(this).parents('.list-group-item');
            $.ajax({
                url: "<?php echo base_url(); ?>investor/reject_meeting_msg",
                type: 'POST',
                dataType: 'html',
                data : {recordId : record_id}
            })
            .done(function( data ) {
                 if( data != ' '){
                     notification.find('.small-msg').toggle();
                    notification.find('.small-msg').html(data);
                }
            });
        });
    });
</script>    