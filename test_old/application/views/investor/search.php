        <div class="container-fluid deals-block ">
        <div class="row">
            <div class="col-sm-12">
                <div class="container wrap">
                    <div class="row block-header">
                        <div class="col-sm-2 block-title">
                            <h2>Results</h2>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            
                <div class="container wrap">
                    <div class="row">
                       <?php $max = count($users);
                      if ($max) { ?>  
                            <div class="slider-markup col-sm-12 deals-slider" data-slide-mode="slide" data-slide-max="3" data-slide-min="3" data-slide-pager="false">
                                <?php
                                $count = 0;
                                foreach ($users as $user) {
                                    $data['user'] = $user;
                                    $this->load->view('front-end/grid-single-item', $data);
                                    $count++;
                                }
                                ?>
                            </div>
                        <?php } else { ?>
                            <div class="col-sm-12 space-20 success-msg" >
                                <div class="alert alert-success" role="alert">There are no opportunities available as per your search criteria</div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            
        </div>
    </div>   
<script>
    $(document).ready(function(){
       if($('.deals-slider').length){
            slider=$('.deals-slider').slick({
                slidesToShow: 3,
                slidesToScroll: 3,
                dots: false,
                autoplay: false,
                autoplaySpeed:5000,
                arrows:true,
                nextArrow:'<button type="button" class="slick-next glyphicon glyphicon-chevron-right"><span class="glyphicon glyphicon-chevron-right"></span></button>'
            }); 
       }
        $(".search-bar").toggle();
    });
</script>