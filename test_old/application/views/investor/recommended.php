    <div class="container-fluid deals-block ">
        <div class="row">
            <div class="col-sm-12">
                <div class="container wrap">
                    <div class="row block-header">
                        <div class="col-sm-2 block-title">
                            <h2>Recommended</h2>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            
                <div class="container wrap">
                    <div class="row">
                        <div class="col-sm-12 slider-markup  deals-slider" data-slide-mode="slide" data-slide-max="3" data-slide-min="3" data-slide-pager="false">
                            <?php
                            $max = count($users);
                            $count = 0;
                            foreach ($users as $user) {
                                $data['user'] = $user;
                                $this->load->view('front-end/grid-single-item', $data);
                                $count++;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            
        </div>
    </div>   
