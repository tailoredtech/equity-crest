<div class="main">
    
    <div class="container wrap">
        <div class="row">
            <h4 class="form-header  col-xs-12 ">Profile <a href="<?php echo base_url(); ?>user/investor_info" class="btn "><span class="glyphicon glyphicon-edit"></span>Edit Profile</a></h4>
            
        </div>
        <div class="row content-box">

            <div class="col-sm-12">
                <div class="investor-profile-box">
                    <div class="row user-info shadow-border">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="user-image text-center col-sm-2 ">
                                    <?php if($investor['image']){ ?>
                                    <img class="img-thumbnail" src="<?php echo base_url(); ?>uploads/users/<?php echo $investor['user_id'] ?>/<?php echo $investor['image']; ?>" width="77" height="77"/>
                                    <?php }else{ ?>
                                    <img class="img-thumbnail" src="<?php echo base_url(); ?>uploads/investor.jpg">
                                    <?php } ?>
                                    
                                    <span class="change-img">Change</span>
                                </div>
                                <div class="col-sm-8">
                                    <div class="user-name lato-bold">
                                        <?php echo $investor['name']; ?>
                                    </div>
                                    <div class="user-designation">
                                        <?php echo $investor['role']; ?>
                                    </div>
                                    <div class="user-location">
                                        <?php echo $investor['city']; ?>
                                    </div>
<!--                                    <div class="user-area">
                                        <span class="lato-bold">Areas I can hep </span>: Finance, Fund Rising, Strategic advice
                                    </div>-->
                                </div>
                                <div class="col-sm-2 social-ico space-20">
                                    <a target="_blank" href="<?php echo $investor['linkedin_url']; ?>" class="social-link" id="ln">Linked-in</a>
                                    <a target="_blank" href="<?php echo $investor['twitter_handle']; ?>" class="social-link" id="tw">Twitter</a>
                                    <a target="_blank" href="<?php echo $investor['fb_url']; ?>" class="social-link" id="fb">Facebook</a>
                                </div> 
                            </div>
                        </div>
                    </div>
                    <div class="profile-tabs row">
                        <div class="col-sm-12">
                            <div class="row">
                                <ul role="tablist" class="nav">
                                    <li>
                                        <div class=" col-sm-1">
                                            <a data-toggle="tab" role="tab" href="#overview">Overview</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="col-sm-1">
                                            <a data-toggle="tab" role="=tab" href="#portfolio">Portfolio</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="col-sm-1">
                                            <a data-toggle="tab" role="=tab" href="#activity">Activity</a>
                                        </div>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="tab-content row">
                        <!-- tab 1 -->
                        <div class="profile-content col-sm-12 tab-pane active" id="overview">
                            <div class="row">

                                <div class="col-sm-9">
                                    <h5 class="tab-header">Overview</h5>
                                    <?php if($investor['experience']){ ?>
                                    <div class="row">
                                        <div class="col-sm-12 ">
                                            <h6 class="tab-subheader lato-bold">Experience Summary</h6>
                                            <p class="dotted-border">
                                                <?php echo $investor['experience'];  ?>
                                            </p>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    
                                     <?php if($investor['key_points']){ ?>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h6 class="tab-subheader lato-bold">In startup I look for</h6>
                                            <div class="row">
                                                <?php $points = explode(",",$investor['key_points']);
                                                   foreach($points as $point){
                                                ?>
                                                <div class="col-sm-4 list-item">
                                                    <div class="item-border">
                                                        <span class="tick-icon"></span><?php echo $point; ?>
                                                    </div>
                                                </div>
                                                <?php } ?>

                                            </div>
                                            <p class="dotted-border"></p>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    
                                    <?php if($investor['sector_expertise']){ ?>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h6 class="tab-subheader lato-bold">Sector Expertise</h6>
                                            <div class="row">
                                                <?php $sec_expertise = explode(",",$investor['sector_expertise']);
                                                   foreach($sec_expertise as $expert){
                                                ?>
                                                <div class="col-sm-4 list-item">
                                                    <div class="item-border">
                                                        <span class="tick-icon"></span><?php echo $expert; ?>
                                                    </div>
                                                </div>
                                                <?php } ?>

                                            </div>
                                            <p class="dotted-border"></p>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    
                                    <?php if($investor['mentoring_sectors']){ ?>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h6 class="tab-subheader lato-bold">Interests in Mentoring Sectors</h6>
                                            <div class="row">
                                                <?php $men_sectors = explode(",",$investor['mentoring_sectors']);
                                                   foreach($men_sectors as $men_sector){
                                                ?>
                                                <div class="col-sm-4 list-item">
                                                    <div class="item-border">
                                                        <span class="tick-icon"></span><?php echo $men_sector; ?>
                                                    </div>
                                                </div>
                                                <?php } ?>

                                            </div>
                                            <p class="dotted-border"></p>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    
                                    <?php if($investor['expertise']){ ?>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h6 class="tab-subheader lato-bold">Area of Expertise</h6>
                                            <div class="row">
                                                <?php $expertise = explode(",",$investor['expertise']);
                                                   foreach($expertise as $expert){
                                                ?>
                                                <div class="col-sm-4 list-item">
                                                    <div class="item-border">
                                                        <span class="tick-icon"></span><?php echo $expert; ?>
                                                    </div>
                                                </div>
                                                <?php } ?>

                                            </div>
                                            <p class="dotted-border"></p>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    
                                    <?php if($investor['duration']){ ?>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h6 class="tab-subheader">Duration </h6>
                                            <p><?php echo $investor['duration']; ?> hours/week</p>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>

<!--                                <div class="col-sm-3">
                                    <div class="row">
                                        <h6 class="tab-header ">Followers (10)</h6>
                                        <div class="col-sm-11">
                                            <div class="row followers">
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>

                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                            </div>
                                            <a href="#" class="pull-right">See all <span class="glyphicon glyphicon-chevron-down"></span></a>
                                        </div>
                                    </div>

                                </div>-->
                            </div>
                        </div>
                        <!-- end tab 1 -->

                        <!-- tab 2 -->
                        <div class="profile-content col-sm-12 tab-pane" id="portfolio">


                            <div class="row">

                                <div class="col-sm-9">
                                    <h6 class="tab-header ">Portfolio</h6>
                                    <div class="row">
                                      <?php foreach($portfolios as $portfolio){ ?>
                                        <div class="col-sm-5 col-xs-10 small-box">
                                            <div class="">
                                                <div class="title">
                                                    <div class="deal-logo col-xs-5 text-center">
                                                        <img src="<?php echo base_url() ?>uploads/portfolios/<?php echo $portfolio['id']; ?>/<?php echo $portfolio['image'] ?>" width="100" height="100">
                                                    </div>    
                                                    <div class="deal-title-block col-xs-6">
                                                        <h4 class="deal-title lato-bold"><?php echo $portfolio['name']; ?></h4>
                                                        <div class="deal-location "><?php echo $portfolio['location']; ?></div>
                                                        <div class="deal-location "><?php echo $portfolio['url']; ?></div>
                                                        <div class="deal-category "><?php echo $portfolio['sector']; ?></div>
                                                    </div>
                                                </div>
                                                <div class="deal-description">
                                                    <p class="text-center center-block ">
                                                        Role : <?php echo $portfolio['role']; ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>  
                                        <?php } ?>
<!--                                        <div class="col-sm-5 col-xs-10 small-box">
                                            <div class="deal-single">
                                                <div class="title">
                                                    <div class="deal-logo col-xs-5 text-center">
                                                        <img src="http://localhost/equitycrest/assets/images/temp-logo-4.jpg">
                                                    </div>
                                                    <div class="deal-title-block col-xs-6">
                                                        <h2 class="deal-title lato-bold">Agile Communications</h2>
                                                        <div class="deal-location ">London East Face</div>
                                                        <div class="deal-category ">media Publishing</div>
                                                    </div>
                                                </div>
                                                <div class="deal-description">
                                                    <p class="text-center center-block ">
                                                        Role : Board of Director
                                                    </p>
                                                </div>
                                            </div>
                                        </div>-->
                                    </div>
                                </div>

<!--                                <div class="col-sm-3">

                                    <div class="row">
                                        <h6 class="tab-header ">Followers (10)</h6>
                                        <div class="col-sm-11">
                                            <div class="row followers">
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>

                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                            </div>
                                            <a href="#" class="pull-right">See all <span class="glyphicon glyphicon-chevron-down"></span></a>
                                        </div>
                                    </div>

                                </div>-->

                            </div>

                        </div>
                        <!-- end tab 2 -->

                        <!-- tab 3 -->
                        <div class="profile-content col-sm-12 tab-pane" id="activity">

                            <div class="row">

                                <div class="col-sm-9">
                                    <h6 class="tab-header ">Activity</h6>
                                    <div class="row">

                                        <div class="col-xs-12">
                                            <?php foreach($activities as $activity){ ?>
                                                <div class="activity-item">
                                                   <?php echo $activity['activity']; ?> - <span class="activity-time"><?php echo date('M, d H:i A',strtotime($activity['date_created'])); ?></span>
                                                </div>
                                            <?php } ?>
                                        </div>


                                    </div>
                                </div>

<!--                                <div class="col-sm-3">

                                    <div class="row">
                                        <h6 class="tab-header ">Followers (10)</h6>
                                        <div class="col-sm-11">
                                            <div class="row followers">
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>

                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                                <div class="col-xs-4"><img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/follower.png" ></div>
                                            </div>
                                            <a href="#" class="pull-right">See all <span class="glyphicon glyphicon-chevron-down"></span></a>
                                        </div>
                                    </div>

                                </div>-->

                            </div>

                        </div>
                        <!-- end tab 3 -->
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<div style="display: none">
    <div class="pop-content" id="ch-img">
        <h3 class="form-header">Upload Image</h3>
        <div class="askexpert">
            <form id="upload-image" action="<?php echo base_url(); ?>user/ajax_upload_image" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="col-sm-1 form-label">Image</label>
                    <div class="col-sm-11">
                        <div class="fileinput fileinput-new uploader input-group" data-provides="fileinput">
                            <div class="form-control uploader" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                            <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="userfile"></span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>
                </div>
                <div class="buttons">
                    <input type="submit" class="btn btn-green" />
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
      
        
    
        $(".change-img").colorbox({inline: true, href: '#ch-img', innerWidth: '400px', innerHeight: '175px'});
       $("#upload-image").on('submit',(function(e){
            e.preventDefault();
            $.ajax({
                    url: "<?php echo base_url(); ?>user/ajax_image_upload",
                    type: "POST",
                    data:  new FormData(this),
                    dataType :'json',
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(data){
                           $('.user-image img').attr('src','<?php echo base_url(); ?>'+data.img);
                           $.colorbox.close()
                    },
                    error: function(){

                    } 
            });         
        }));
   });
             
</script>