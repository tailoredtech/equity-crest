<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Channel_partner extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('investee_model');
        $this->load->model('user_model');
        $this->load->model('banners_model');
        $this->load->helper('common_helper');
        $this->load->helper('text_helper');
        
        $this->load->model('banners_model');
        $this->layout['banners']=  $this->banners_model->find_benners();
    }
    
    public function index(){
        $investees = $this->user_model->get_investees(3);
        
        $viewData['investees'] = $investees;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('channel_partner/index', $bodyData, true);
        $this->load->view('layouts/channel_partner', $this->layout);
    }
    
    public function refer(){
        $viewData['active']='2';
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('channel_partner/refer', $bodyData, true);
        $this->load->view('layouts/channel_partner', $this->layout);
    }
    
    public function refer_process(){
        $userId = $this->session->userdata('user_id');
        $insertData = array(
            'type' => $this->input->post('type'),
            'name' => $this->input->post('name'),
            'company_name' => $this->input->post('company_name'),
            'mob_no' => $this->input->post('mobileno'),
            'email' => $this->input->post('email'),
            'user_id' => $userId
        );
        
        $userId = $this->user_model->add_refer($insertData);
        
        $this->session->set_flashdata('success-msg', 'Successfully referred to the Equity Crest');
        redirect('channel_partner/refer');
    }
}

/* End of file investee.php */
/* Location: ./application/controllers/investee.php */