<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
    
        function __construct() {
            parent::__construct();
            $this->load->model('user_model');
            $this->load->helper('common_helper');
            $this->load->helper('text_helper');
            
            $this->load->model('banners_model');
            $this->layout['banners']=  $this->banners_model->find_benners();
            $this->layout['unseen_notifications']=  $this->user_model->get_unseen_notification_count($this->session->userdata('user_id'));
            $this->layout['panels'] = $this->platform();
            
            $sectors = $this->user_model->sectors();
            $stages = $this->user_model->startup_stages();
            $this->layout['stages'] = $stages;
            $this->layout['sectors']= $sectors;
        }

	public function index()
	{
            if( $this->session->userdata('session_id') ){
                if ($this->session->userdata('role') == 'investee') {
                redirect('investee/index');
            } else if ($this->session->userdata('role') == 'investor') {
                redirect('investor/index');
            } else if ($this->session->userdata('role') == 'channel_partner') {
                redirect('channel_partner/index');
            }
            }
            parse_str($_SERVER['QUERY_STRING'],$_GET);
            $this->load->model('banners_model');
            $this->layout['banners']=  $this->banners_model->find_benners();
            
            $sectors = $this->user_model->active_sectors();
            $stages = $this->user_model->startup_stages();
            $locations = $this->user_model->get_locations();
            if(isset($_GET['sector']) && $_GET['sector'] != ''){
                $sector_id = $_GET['sector'];
                $investees = $this->user_model->get_investees_by_sector($sector_id); 
            }elseif(isset($_GET['size']) && $_GET['size'] != ''){
                $size_id = $_GET['size'];
                $investees = $this->user_model->get_investees_by_investement_sizes($size_id); 
            }elseif(isset($_GET['stage']) && $_GET['stage'] != ''){
                $stage_id = $_GET['stage'];
                $investees = $this->user_model->get_investees_by_stage($stage_id); 
            }elseif(isset($_GET['location']) && $_GET['location'] != ''){
                $location = $_GET['location'];
                $investees = $this->user_model->get_investees_by_location($location); 
            }elseif(isset($_GET['status']) && $_GET['status'] != ''){
                $status = $_GET['status'];
                $investees = $this->user_model->get_investees_by_status($status); 
            }
            else{
               $investees = $this->user_model->get_featured_investees(); 
            }
            
            
            $viewData['users'] = $investees;
            $viewData['sectors'] = $sectors;
            $viewData['stages'] = $stages;
            $viewData['locations'] = $locations;
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('home/index', $bodyData, true);
            $this->load->view('layouts/home', $this->layout);
	}
        public function add_investee(){
            $data=array();
            $investees = $this->user_model->get_investees(9);
            foreach($investees as $investee){
                $data['user']=$investee;
                $this->load->view('front-end/grid-single-item', $data);
            }
        }
        public function file(){
            $response = array('response' => '');
            $this->load->view('upload_form', $response);
        }
        
        function do_upload()
	{
		$config['allowed_types'] = 'pdf|doc|jpg|png|gif';
		$config['max_size']	= '0';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';

		$this->load->library('upload', $config);
                $file1 = "file1";
                $file2 = "file2";
                $response = array();
                $config['upload_path'] = './uploads/1';
                $this->upload->initialize($config);
                if(!$this->upload->do_upload($file1)){
                    $response['response']['error'][] = $this->upload->display_errors();
                }else{
                    $data = $this->upload->data();
                    $response['response']['success'][] = $data['file_name'];
                }
                $config['upload_path'] = './uploads/2';
                $this->upload->initialize($config);
                if(!$this->upload->do_upload($file2)){
                    $response['response']['error'][] = $this->upload->display_errors();
                }else{
                    $data = $this->upload->data();
                    $response['response']['success'][] = $data['file_name'];
                }
                 $this->load->view('upload_form', $response);
//                $this->upload->do_upload($file2);
//		if ( ! $this->upload->do_upload())
//		{
//			$error = array('error' => $this->upload->display_errors());
//
//			$this->load->view('upload_form', $error);
//		}
//		else
//		{
//			$data = array('upload_data' => $this->upload->data());
//
//			$this->load->view('upload_success', $data);
//		}
	}

        function contact_us(){
            $viewData = array();
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('home/contact_us', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
            
        }
        function about_us(){
            $viewData = array();
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('home/about_us', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }
        function news_top_stories(){
            $viewData = array();
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('home/news_top_stories', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }
        function events(){
            $viewData = array();
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('home/events', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }
        function faqs(){
            $viewData = array();
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('home/faqs', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }
        function event_details(){
            $viewData = array();
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('home/event_details', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }
        function terms_of_use(){
            $viewData = array();
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('home/terms_of_use', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }
        function privacy_policy(){
            $viewData = array();
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('home/privacy_policy', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }
        function how_it_works(){
            $viewData = array();
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('home/how_it_works', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }
        function management_team(){
            $this->load->model('banners_model');
            $this->layout['banners']=  $this->banners_model->find_benners();
            $viewData = array();
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('home/management_team', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);        
        }
        function account_setting(){
            $this->load->model('banners_model');
            $this->layout['banners']=  $this->banners_model->find_benners();
            $viewData = array();
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('home/account_setting', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }
        function refer_company(){
            $this->load->model('banners_model');
            $this->layout['banners']=  $this->banners_model->find_benners();
            $viewData = array();
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('home/refer_company', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }
        function investor_all(){
            $investors = $this->user_model->get_all_investors();
            $viewData['investors'] = $investors;
//            echo "<pre>";            
//            print_r($investors);
//            echo "</pre>";
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('home/investor_all', $bodyData, true);
            $role = $this->session->userdata('role');
            
            if($role){
                $this->load->view('layouts/'.$role, $this->layout);
            }else{
                $this->load->view('layouts/front', $this->layout);
            }
        }
        function portfolio_all(){
            $investees = $this->user_model->get_all_investees(); 
            $viewData['users'] = $investees;
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('home/portfolio_all', $bodyData, true);
            
            $role = $this->session->userdata('role');
            
            if($role){
                $this->load->view('layouts/'.$role, $this->layout);
            }else{
                $this->load->view('layouts/front', $this->layout);
            }
            
            
        }
        function partners(){    
            $viewData = array();
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('home/partners', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }
        function advisory_board(){    
            $viewData = array();
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('home/advisory_board', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }
        function women_entrepreneurship(){    
            $viewData = array();
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('home/women_entrepreneurship', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }
        function community(){    
            $viewData = array();
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('home/community', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }
        function careers(){    
            $viewData = array();
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('home/careers', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }
        function nda(){
            $viewData = array();
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('home/nda', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }
        
        function blogs(){    
            $viewData = array();
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('home/blogs', $bodyData, true);
            
            $role = $this->session->userdata('role');
            if($role){
                $this->load->view('layouts/'.$role, $this->layout);
            }else{
                $this->load->view('layouts/front', $this->layout);
            }
        }
        
        function platform(){
            $this->load->model('investee_model');
            $data = $this->investee_model->get_stats('sector');
            $data2 = $this->investee_model->get_stats('location');
            $colorArray = array();
            foreach($data as $result){
                
                $color ='#'.substr(md5(rand()), 0, 6);
                if(!in_array($color, $colorArray)){
                    $colorArray[] = $color;
                }
                $result['color'] = $color;
                $result['highlight'] = $color;
                 $result['actual'] = $result['value'];
                $result['value'] = $result['value']*10;
                $jsonData[] = $result;
                $finalData['sector'] = $jsonData;
            }
            foreach($data2 as $result){
                
                $color ='#'.substr(md5(rand()), 0, 6);
                if(!in_array($color, $colorArray)){
                    $colorArray[] = $color;
                }
                $result['color'] = $color;
                $result['highlight'] = $color;
                $result['actual'] = $result['value'];
                $result['value'] = $result['value']*10;
                $jsonData2[] = $result;
                $finalData['location'] = $jsonData2;
            }
            return $finalData;
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */