<?php

class Admin extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin_model');
        $this->load->model('user_model');
        $this->load->model('banners_model');
        $this->load->helper('common_helper');

        if ($this->session->userdata('user_id') == '') {
            if ($this->uri->segment(2) != 'login' && $this->uri->segment(2) != 'login_process') {
                redirect('admin/login');
            }
        }
    }

    public function active_users() {
        parse_str($_SERVER['QUERY_STRING'],$_GET);
        
        $options['status'] = 'active';
        if(isset($_GET['name']) && $_GET['name'] != ''){
            $options['name'] = $_GET['name'];
        }else{
            $options['name'] = '';
        }
            
        if(isset($_GET['company_name']) && $_GET['company_name'] != ''){
           $options['company_name'] = $_GET['company_name'];    
        }else{
            $options['company_name'] = '';
        }

        if(isset($_GET['role']) && $_GET['role'] != ''){
            $options['role'] = $_GET['role'];  
        }else{
            $options['role'] = '';
        }
            
        $users = $this->admin_model->get_users($options);
        $viewData['filter'] = $options;
        $viewData['users'] = $users;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/active_users', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout); 
    }

    public function inactive_users() {
        parse_str($_SERVER['QUERY_STRING'],$_GET);
        
        $options['status'] = 'inactive';
        if(isset($_GET['name']) && $_GET['name'] != ''){
            $options['name'] = $_GET['name'];
        }else{
            $options['name'] = '';
        }
            
        if(isset($_GET['company_name']) && $_GET['company_name'] != ''){
           $options['company_name'] = $_GET['company_name'];    
        }else{
            $options['company_name'] = '';
        }

        if(isset($_GET['role']) && $_GET['role'] != ''){
            $options['role'] = $_GET['role'];  
        }else{
            $options['role'] = '';
        }
            
        $users = $this->admin_model->get_users($options);
        $viewData['filter'] = $options;
        $viewData['users'] = $users;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/inactive_users', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }

    public function index() {
        $users_count = $this->admin_model->get_users_count();
        $investors_count = $this->admin_model->get_investors_count();
        $investees_count = $this->admin_model->get_investees_count();
        $channel_partners_count = $this->admin_model->get_channel_partners_count();

        $viewData['users_count'] = $users_count;
        $viewData['investors_count'] = $investors_count;
        $viewData['investees_count'] = $investees_count;
        $viewData['channel_partners_count'] = $channel_partners_count;

        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/index', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }

    public function investors() {
        $viewData = array();
        $investors = $this->admin_model->get_investors();
        $viewData['investors'] = $investors;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/investors', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }

    public function investor($id) {
        $investor = $this->admin_model->fetch_investor($id);
        $viewData['investor'] = $investor;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/investor', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }

    public function investees() {

        $investees = $this->admin_model->get_investees();
        $viewData['investees'] = $investees;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/investees', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }

    public function active_investees() {
        $investees = $this->admin_model->get_active_investees();
        $viewData['investees'] = $investees;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/active_investees', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }

    public function inactive_investees() {
        $investees = $this->admin_model->get_inactive_investees();
        $viewData['investees'] = $investees;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/inactive_investees', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }

    public function active_investors() {
        $investors = $this->admin_model->get_active_investors();
        $viewData['investors'] = $investors;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/active_investors', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }

    public function inactive_investors() {
        $investors = $this->admin_model->get_inactive_investors();
        $viewData['investors'] = $investors;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/inactive_investors', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }
    
    public function active_partners() {
        $partners = $this->admin_model->get_active_partners();
        $viewData['partners'] = $partners;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/active_partners', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }

    public function inactive_partners() {
        $partners = $this->admin_model->get_inactive_partners();
        $viewData['partners'] = $partners;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/inactive_partners', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }
    
    public function partner($id) {
        $partner = $this->admin_model->fetch_partner($id);
        $viewData['partner'] = $partner;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/partner', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }

    public function featured_investees() {
        $investees = $this->admin_model->get_featured_investees();
        $viewData['investees'] = $investees;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/featured_investees', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }
    
    public function update_users() {
        if ($this->input->post()) {
            $ids = $this->input->post('id');
            $status = $this->input->post('status');
            
            $data = array('status' => $status);
            $this->admin_model->update_users($data, $ids);

            redirect('admin/active_users');
            
        }
    }

    public function update_investors() {
        if ($this->input->post()) {
            $ids = $this->input->post('id');
            $status = $this->input->post('status');
           
            $data = array('status' => $status);
            $this->admin_model->update_users($data, $ids);

            redirect('admin/active_investors');
            
        }
    }
    
        public function update_partners() {
        if ($this->input->post()) {
            $ids = $this->input->post('id');
            $status = $this->input->post('status');
           
            $data = array('status' => $status);
            $this->admin_model->update_users($data, $ids);

            redirect('admin/active_partners');
            
        }
    }

    public function update_investees() {
        if ($this->input->post()) {
            $ids = $this->input->post('id');
            $status = $this->input->post('status');
            if ($status == 'feature') {
                $featured = array();
                foreach ($ids as $id) {
                    $featured[] = array('user_id' => $id, 'sort' => 1);
                }
                $this->admin_model->add_featured($featured);
                redirect('admin/featured_investees');
            } else {
                $data = array('status' => $status);
                $this->admin_model->update_users($data, $ids);

                redirect('admin/active_investees');
            }
        }
    }

    public function remove_featured() {
        if ($this->input->post()) {

            $ids = $this->input->post('id');
            $this->admin_model->delete_featured($ids);

            redirect('admin/featured_investees');
        }
    }

    public function investee($id) {
        $investee = $this->admin_model->fetch_investee($id);
        $viewData['investee'] = $investee;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/investee', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }

    public function login() {
        $viewData = array();
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/login', $bodyData);
    }

    public function login_process() {
        $email = $this->input->post('email');
        $password = md5(trim($this->input->post('password')));
        $user = $this->admin_model->login($email, $password);
        if ($user) {
            $this->session->set_userdata('user_id', $user['id']);
            $this->session->set_userdata('email', $user['email']);
            $this->session->set_userdata('name', $user['name']);
            $this->session->set_userdata('role', $user['role']);

            if ($user['role'] == 'admin') {
                redirect('admin/index');
            } else {
                redirect('home/index');
            }
//            if(isset($this->session->userdata['last_url'])){
//                redirect($this->session->userdata['last_url']);
//            }else{
//                redirect('home/index'); 
//            }
        } else {
            $this->session->set_flashdata('error-msg', ' Please check your Email ID and Password.');
            redirect('admin/login');
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect(base_url() . 'admin/login');
    }

    public function referred() {
        $companies = $this->admin_model->get_referred_companies();
        $viewData['companies'] = $companies;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/referred', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }

    public function advice_requests() {
        $requests = $this->admin_model->get_advice_requests();
        $viewData['requests'] = $requests;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/advice_requests', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }

    public function subscribed_users() {
        $requests = $this->admin_model->get_subscribed_users();
        $viewData['users'] = $requests;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/subscribed_users', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }

    public function banners() {

        $banners = $this->banners_model->get_banners(); //print_r($banners);
        $viewData['banners'] = $banners;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/banners', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }

    public function edit_banner($id = null) {

        if ($this->input->post()) {
            $banner = $this->input->post(null, TRUE);
            $update = $this->banners_model->update_banner($banner);
            if ($update) {
                redirect('admin/banners');
            } else {
                echo "banner not saved";
            }
        } else {
            $banner = $this->banners_model->get_banner($id);
            $viewData['banner'] = $banner;
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('admin/edit_banner', $bodyData, true);
            $this->load->view('layouts/admin', $this->layout);
        }
    }
    
    public function notifications(){
        $notifications = $this->admin_model->get_all_notifications(); 
        $viewData['notifications'] = $notifications;
      
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/notifications', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }
    
    public function meetings(){
        $meetings = $this->admin_model->get_meetings(); 
        $viewData['meetings'] = $meetings;
      
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/meetings', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
        
    }
    
    public function pledges($investeeId){
        
        $pledges = $this->admin_model->get_pledges($investeeId);
        $viewData['pledges'] = $pledges;
      
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/pledges', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
        
        
    }

    public function pledge_approval(){
        $id=$this->input->post('id');
        $investeeId=$this->input->post('investee_id');
        $investment=$this->input->post('investment');
        $approval=$this->input->post('approval');
        $data= array('approval'=>$approval);
        $this->admin_model->add_approval($id,$data);
        
        $currentFundRaise=$this->admin_model->get_current_fundraise($investeeId);
        $newFundRaise=$currentFundRaise['fund_raise']+$investment;
        $data=array('fund_raise'=>$newFundRaise);
        $this->admin_model->update_fundraise($investeeId,$data);
        
    }
    
    public function qna($investeeId){
        $qna = $this->admin_model->get_qna($investeeId);
        $viewData['qna'] = $qna;
      
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('admin/qna', $bodyData, true);
        $this->load->view('layouts/admin', $this->layout);
    }
    
    public function verify_user(){
        
        $userId=$this->input->post('user_id');
        $verify=$this->input->post('verify');
        $data= array('active'=>$verify);
        $this->admin_model->verify_user($userId,$data);
    }
    
    public function delete_user($id){
        $this->db->where('id', $id);
        $this->db->delete('user_master');
        
        $this->db->where('user_id', $id);
        $this->db->delete('investee');
        
        $this->db->where('user_id', $id);
        $this->db->delete('investor');
        redirect('admin/inactive_users');
        
    }
    
    public function change_priority(){
        
        $userId=$this->input->post('user_id');
        $priority=$this->input->post('priority');
        $data= array('sort'=>$priority);
        $this->admin_model->change_priority($userId,$data);
    }
        
}

?>