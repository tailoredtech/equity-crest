<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('investor_model');
        $this->load->model('investee_model');
        $this->load->helper('common_helper');
        $this->load->helper('text_helper');
        
        $this->load->model('banners_model');
        $this->layout['banners']=  $this->banners_model->find_benners();
        $this->layout['unseen_notifications']=  $this->user_model->get_unseen_notification_count($this->session->userdata('user_id'));
        
        $sectors = $this->user_model->sectors();
        $stages = $this->user_model->startup_stages();
        $this->layout['stages'] = $stages;
        $this->layout['sectors']= $sectors;
    }

    public function index() {
        echo CI_VERSION;
        $this->load->view('welcome_message');
    }

    
    public function register(){
        $userId = $this->session->userdata('user_id');
        if($userId){
            redirect($this->session->userdata('role').'/index');
        }
        $this->load->model('banners_model');
        $this->layout['banners']=  $this->banners_model->find_benners();
        $viewData = array();
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('user/register', $bodyData, true);
        $this->load->view('layouts/front', $this->layout);
    }
    public function register_process(){
//        die(print_r($this->input->post()));
        $returnData = array();
        if ($this->input->post('name')) {
            $code = rand_string(30);
            $role = $this->input->post('role');
            $email = $this->input->post('email');
            $name = $this->input->post('name');
            if($this->user_model->find_by_email($email)){
                $this->session->set_flashdata('error-msg', 'This email Id '.$email.' is already taken.');
                redirect('user/register');
            }
            $pass = md5(trim($this->input->post('password')));
            $insertData = array(
                'name' => $name,
                'mob_no' => $this->input->post('mobileno'),
                'email' => $email,
                'password' => $pass,
                'role' => $role,
                'code' => $code,
                'created_date'=> date('Y-m-d H:i:s')
            );
            if($this->input->post('company_name')){
               $insertData['company_name'] = $this->input->post('company_name');
               $displayName = $this->input->post('company_name');
            }else{
               $displayName = $name;
            }
            $userId = $this->user_model->add_user($insertData);
            
            $display_text = $displayName . ' joined the Equity Crest';
            $link = 'admin/user/'.$userId;
            $notification = array(
                'sender_id' => $userId,
                'record_id' => $userId, 
                'display_text' => $display_text,
                'type' => 'new_user',
                'table_name' => 'user_master',
                'link'=> $link,
                'date_created' => date('Y-m-d H:i:s')
            );

            $this->user_model->add_admin_notification($notification);
            
            $returnData['userId']= $userId;
            if($userId){
                $returnData['success'] = TRUE;
                
                if($role == 'investee'){
                    $this->user_model->add_privacy($userId);
                }
                
                $this->load->library('email');
        
                $this->email->set_mailtype("html");
                $this->email->from('info@equitycrest.com', 'Equitycrest');
                $this->email->to($email); 

                $this->email->subject('Welcome');

//                $message ='<p class="MsoNormal"><span style="font-size:11.5pt;line-height:115%;color:rgb(68,68,68);background-image:initial;background-repeat:initial">Dear
//'.$this->input->post('name').',<span>&nbsp;</span></span><span style="font-size:11.5pt;line-height:115%;color:rgb(68,68,68)"><br>
//<br>
//<b><span style="background-image:initial;background-repeat:initial">Welcome to Equity Crest</span></b><br>
//<br>
//<span style="background-image:initial;background-repeat:initial">We are
//super excited that you are now part of our community. This is the first time
//you\'ve signed into EquityCrest with this account. Now all you need to do is
//verify that it belongs to you.<span>&nbsp;</span></span><br>
//<br>
//<span style="background-image:initial;background-repeat:initial">The
//email address that you gave us is <a href="mailto:'.$email.'" target="_blank">'.$email.'</a>. Just click the link
//below to verify, sign in to EquityCrest, then follow the prompts. By verifying
//your email address below, you agree to be bound by our<span>&nbsp;</span></span></span><a href="https://www.shoppertise.com/legal/terms-of-use" target="_blank"><span style="font-size:11.5pt;line-height:115%;color:rgb(0,104,207);background-image:initial;background-repeat:initial">Terms of
//Use</span></a><span><span style="font-size:11.5pt;line-height:115%;color:rgb(68,68,68);background-image:initial;background-repeat:initial">&nbsp;</span><span style="font-size:11.5pt;line-height:115%;color:rgb(68,68,68);background-image:initial;background-repeat:initial">and<span>&nbsp;</span></span></span><a href="https://www.shoppertise.com/legal/privacy-policy" target="_blank"><span style="font-size:11.5pt;line-height:115%;color:rgb(0,104,207);background-image:initial;background-repeat:initial">Privacy
//Policy</span></a><span style="font-size:11.5pt;line-height:115%;color:rgb(68,68,68);background-image:initial;background-repeat:initial">.<span>&nbsp;</span></span><span style="font-size:11.5pt;line-height:115%;color:rgb(68,68,68)"><br>
//
//<br>
//</span><a href="'.base_url() .'user/activate/'.$code.'" target="_blank"><span style="font-size:11.5pt;line-height:115%;color:rgb(0,104,207);background-image:initial;background-repeat:initial">Verify Now</span></a><span><span style="font-size:11.5pt;line-height:115%;color:rgb(68,68,68);background-image:initial;background-repeat:initial">&nbsp;</span></span><span style="font-size:11.5pt;line-height:115%"><br>
//
//<br>
//<span style="background-image:initial;background-repeat:initial"><font color="#444444">If the
//button above does not work, copy and paste the following link to your browser
//window: </font><font color="#ff0000"><a href="'.base_url() .'user/activate/'.$code.'" target="_blank">'.base_url() .'user/activate/'.$code.'</a><span>&nbsp;</span></font></span><font color="#ff0000"><br>
//</font>
//<br>
//<span style="color:rgb(68,68,68);background-image:initial;background-repeat:initial">Thank
//you for signing up, see you on EquityCrest soon!<span>&nbsp;</span></span><br>
//<br>
//<span style="color:rgb(68,68,68);background-image:initial;background-repeat:initial">Always
//at your service,<span>&nbsp;</span></span><br>
//<span style="color:rgb(68,68,68);background-image:initial;background-repeat:initial">The EquityCrest Team<span>&nbsp;</span></span></span></p>';
                
                
                
                $message = '<p class="MsoNormal"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#444444">Dear
'.$name.',&nbsp;<br>
<br>
<b>Welcome to Equity Crest</b><br>
<br>
We are very happy to have you as part of our community. As this is the first
time you\'ve signed into Equity Crest with this account, we would need to verify
that this is really you.&nbsp;<br>
<br>
The email address that you gave us during registration is&nbsp;<a href="mailto:'.$email.'" target="_blank">'.$email.'</a>. Just
click the link below to verify, sign in to Equity Crest and then follow the
prompts given by the system. By verifying your email address below, you agree
to abide by our&nbsp;</span><a href="'.base_url() .'home/terms_of_use" target="_blank"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#0068cf">Terms
of Use</span></a><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#444444">&nbsp;and&nbsp;</span><a href="'.base_url() .'home/privacy_policy" target="_blank"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#0068cf">Privacy
Policy</span></a><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#444444">.&nbsp;<br>
<br>
</span><a href="'.base_url() .'user/activate/'.$code.'" target="_blank"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#0068cf">Verify Now</span></a><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#444444">&nbsp;</span><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"><br>
<br>
<span style="color:#444444">If the button above does not work, copy and paste
the following link into your browser window:&nbsp;</span><span style="color:red"><a href="'.base_url() .'user/activate/'.$code.'" target="_blank">'.base_url() .'user/activate/'.$code.'</a><wbr>&nbsp;<br>
</span><br>
<span>Once you verify your email address you will get a call from our team to brief you about the Equity Crest and your account.</span><br>
<span style="color:#444444">Thank you for signing up, we look forward to a long
term partnership!</span></span><u></u><u></u></p>
<p class="MsoNormal"><span style="font-size:11.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"><br>
<span style="color:#444444">Happy to have you,&nbsp;</span><br>
<span style="color:#444444">The Equity Crest Team&nbsp;</span></span><u></u><u></u></p>';
                $this->email->message($message);	

                $this->email->send();
                
                switch ($role) {
                        case 'investor':
                            $role = "Investor";
                            break;
                        case 'investee':
                            $role = "Start-Up";
                            break;
                        case 'channel_partner':
                            $role = "Channel partner";
                            break;
                        case 'media':
                            $role = "Media";
                            break;
                    }
                $this->session->set_flashdata('success-msg', 'Thank you for registering on Equity Crest as an '.$role.'. An activation e-mail has been sent to you on your e-mail ID '.$email.'. Request you to please activate your Equity Crest account by clicking on the link sent across.');
                redirect('user/login');
        
            }else{
                $returnData['success'] = FALSE;
                
            }
        }else{
            $returnData['success'] = FALSE;
        }
        
        echo json_encode($returnData);
    
    }
    
    public function activate($code){
        $user = $this->user_model->find_by_code($code);
        if($user){
            $data = array('active' => 1);
            $this->user_model->update_user($data,$user['id']);
            
            $this->session->set_flashdata('success-msg', 'Your account has been activated.');
            redirect('user/login');
        }else{
            $this->session->set_flashdata('error-msg', 'Link is expired.');
            redirect('user/login');
        }
    }
    
    public function login(){
        $this->load->model('banners_model');
        $this->layout['banners']=  $this->banners_model->find_benners();
        $viewData = array();
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('user/login', $bodyData, true);
        $this->load->view('layouts/front', $this->layout);
    }
    
    public function login_process(){
        $email = $this->input->post('email');
//        $password = $this->input->post('password');
        $password = md5(trim($this->input->post('password')));
        $user = $this->user_model->login($email,$password);
        if($user){
            
            $this->session->set_userdata('user_id', $user['id']);
            $this->session->set_userdata('email', $user['email']);
            $this->session->set_userdata('name', $user['name']);
            $this->session->set_userdata('company_name', $user['company_name']);
            $this->session->set_userdata('image', $user['image']);
            $this->session->set_userdata('role', $user['role']);
            
            if($user['role'] == 'investee'){
                redirect('investee/index');
            }else if($user['role'] == 'investor'){
                $investee_ids = $this->investor_model->get_pledged_investee_ids($user['id']);
                $this->session->set_userdata('pledged', $investee_ids);
                $investee_ids = $this->investor_model->get_followed_investee_ids($user['id']);
                $this->session->set_userdata('followed', $investee_ids);
                redirect('investor/index');
            }else if($user['role'] == 'channel_partner'){
                redirect('channel_partner/index');
            }
//            if(isset($this->session->userdata['last_url'])){
//                redirect($this->session->userdata['last_url']);
//            }else{
//                redirect('home/index'); 
//            }
            
        }else{
            $this->session->set_flashdata('error-msg', ' Please check your Email ID and Password.');
            redirect('user/login');
        }
    }
    
    public function send_pass_reset_link(){
        if ($this->input->post()) {
            $email = $this->input->post('email');
            $user = $this->user_model->find_by_email($email);
            if ($user) {
                $code = rand_string(30);
                $this->load->library('email');
        
                $this->email->set_mailtype("html");
                $this->email->from('admin@equitycrest.com', 'Equitycrest');
                $this->email->to($email); 

                $this->email->subject('Reset Password');
                $message = 'Please click on below link to reset password.'."\r\n".'<br><br>
                            <a href="'.base_url() .'user/reset_password/'.$code.'">Reset Password</a><br>';
                
                $this->email->message($message);	

                $this->email->send();
                
                $this->user_model->update_user(array('reset_code'=>$code),$user['id']);
                echo 'success';
            } else {
               echo 'error';
            }
        }
    }
    
    public function reset_password($code){
        $user = $this->user_model->find_by_reset_code($code);
        if($user){
            $bodyData['email'] = $user['email'];
            $bodyData['id'] = $user['id'];
            $this->layout['content'] = $this->load->view('user/reset_password', $bodyData, true);
            $this->load->view('layouts/front', $this->layout);
        }else{
            echo 'link is expired';
        }
    }
    
    public function reset_password_process(){
         if ($this->input->post()) {
             $id = $this->input->post('id');
             $pass = md5(trim($this->input->post('password')));
             $data = array('password'=>$pass,'reset_code'=>'');
             $this->user_model->update_user($data,$id);
             
            $this->session->set_flashdata('success-msg', 'Your password has been successfully changed.');
            redirect('user/login');
         }
        
    }
    
    public function fb_login() {
        parse_str($_SERVER['QUERY_STRING'], $_REQUEST);

        $this->load->library('Facebook', array("appId" => "693713417337707", "secret" => "24937704e490cea8d384a02fabee1945"));

        $this->user = $this->facebook->getUser();

        if ($this->user) {

            try {
                $userProfile = $this->facebook->api('/me');
                $localUser = $this->user_model->get_fbuser($userProfile['id']);

                if ($localUser) {
                    $this->session->set_userdata('user_id', $localUser['id']);
                    $this->session->set_userdata('facebook_id', $localUser['facebook_id']);
                    $this->session->set_userdata('google_picture', $localUser['google_picture']);
                    $this->session->set_userdata('linkedin_picture', $localUser['linkedin_picture']);
                    $this->session->set_userdata('image', $localUser['image']);
                    $this->session->set_userdata('email', $localUser['email']);
                    $this->session->set_userdata('company_name', $localUser['company_name']);
                    $this->session->set_userdata('email', $localUser['email']);
                    $this->session->set_userdata('role', $localUser['role']);

                    if ($this->session->userdata['role'] == 'investor') {
                        redirect('investor/index'); 
                    }elseif ($this->session->userdata['role'] == 'investee') {
                        redirect('investee/index');
                    }
                } else {

                    $userData = $this->user_model->find_by_email($userProfile['email']);

                    if ($userData) {

                        $this->user_model->update_facebookid($userData['id'], $userProfile['id']);
                        //linked in user auto-login

                        $localUser = $this->user_model->get_fbuser($userProfile['id']);

                        $this->session->set_userdata('user_id', $localUser['id']);
                        $this->session->set_userdata('facebook_id', $localUser['facebook_id']);
                        $this->session->set_userdata('google_picture', $localUser['google_picture']);
                        $this->session->set_userdata('linkedin_picture', $localUser['linkedin_picture']);
                        $this->session->set_userdata('image', $localUser['image']);
                        $this->session->set_userdata('email', $localUser['email']);
                        $this->session->set_userdata('company_name', $localUser['company_name']);
                        $this->session->set_userdata('email', $localUser['email']);
                        $this->session->set_userdata('role', $localUser['role']);

                        if ($this->session->userdata['role'] == 'investor') {
                            redirect('investor/index'); 
                        }elseif ($this->session->userdata['role'] == 'investee') {
                            redirect('investee/index');
                        }
                    } else {
                      
                        $bodyData['facebook_id'] = $userProfile['id'];
                        $bodyData['fname'] = $userProfile['first_name'];
                        $bodyData['lname'] = $userProfile['last_name'];
                        $bodyData['email'] = $userProfile['email'];
                        
                    //  die(print_r($profileData));
                        $this->layout['content'] = $this->load->view('user/facebook_register', $bodyData, true);
                        $this->load->view('layouts/front', $this->layout);
                        
                    }
                }

            } catch (FacebookApiException $e) {

                print_r($e);
            }
        } else {

            $login = $this->facebook->getLoginUrl(array("scope" => "email"));
            redirect($login);
        }
    }
    
    function fb_process() {

        $facebookId = $this->input->post('facebook_id');
        $role = $this->input->post('role');
        $email = $this->input->post('email');
        $random = mt_rand();
        $pass = md5($random);
        $insertData = array(
                'name' => $this->input->post('name'),
                'mob_no' => $this->input->post('mobileno'),
                'email' => $email,
                'role' => $role,
                'password'=>$pass,
                'facebook_id'=>$facebookId
            );
            if($this->input->post('company_name')){
               $insertData['company_name'] = $this->input->post('company_name');
            }
        
        $userId = $this->user_model->add_user($insertData);    
        $userData = $this->user_model->get_fbuser($facebookId);
       

        $this->session->set_userdata('user_id', $userData['id']);
        $this->session->set_userdata('facebook_id', $userData['facebook_id']);
        $this->session->set_userdata('google_picture', $userData['google_picture']);
        $this->session->set_userdata('linkedin_picture', $userData['linkedin_picture']);
        $this->session->set_userdata('image', $userData['image']);
        $this->session->set_userdata('email', $userData['email']);
        $this->session->set_userdata('company_name', $userData['company_name']);
        $this->session->set_userdata('email', $userData['email']);
        $this->session->set_userdata('role', $userData['role']);

        if ($this->session->userdata['role'] == 'investor') {
            redirect('investor/index'); 
        }elseif ($this->session->userdata['role'] == 'investee') {
            redirect('investee/index');
        } 
    }
    
    function linkedin_login() {
        session_start();
        $this->load->library('linkedin');
        // OAuth 2 Control Flow
        if (isset($_GET['error'])) {
            // LinkedIn returned an error
            // load any error view here
            exit;
        } elseif (isset($_GET['code'])) {
            // User authorized your application
            if(isset($_SESSION['state'])){
                if ($_SESSION['state'] == $_GET['state']) {
                // Get token so you can make API calls
                $this->linkedin->getAccessToken();
                } else {

                    // CSRF attack? Or did you mix up your states?
                    exit;
                }
            }
            
        } else {
            if ((empty($_SESSION['expires_at'])) || (time() > $_SESSION['expires_at'])) {
                // Token has expired, clear the state
                $_SESSION = array();
            }
            if (empty($_SESSION['access_token'])) {
                // Start authorization process
                $this->linkedin->getAuthorizationCode();
            }
        }
        // define the array of profile fields
        $profile_fileds = array(
            'id',
            'firstName',
            'maiden-name',
            'lastName',
            'picture-url',
            'email-address',
            'location:(country:(code))',
            'industry',
            'summary',
            'specialties',
            'interests',
            'public-profile-url',
            'last-modified-timestamp',
            'num-recommenders',
            'date-of-birth',
            'picture-urls::(original)'
        );
        $profileData = $this->linkedin->fetch('GET', '/v1/people/~:(' . implode(',', $profile_fileds) . ')');
        
//        http://api.linkedin.com/v1/people/~/picture-urls::(original)

        if ($profileData) { 
          $profileData=  get_object_vars($profileData);
          
          $localUser = $this->user_model->get_linkedinuser($profileData['id']);

                if ($localUser) {
                    $this->session->set_userdata('user_id', $localUser['id']);
                    $this->session->set_userdata('facebook_id', $localUser['facebook_id']);
                    $this->session->set_userdata('google_picture', $localUser['google_picture']);
                    $this->session->set_userdata('linkedin_picture', $localUser['linkedin_picture']);
                    $this->session->set_userdata('image', $localUser['image']);
                    $this->session->set_userdata('email', $localUser['email']);
                    $this->session->set_userdata('company_name', $localUser['company_name']);
                    $this->session->set_userdata('email', $localUser['email']);
                    $this->session->set_userdata('role', $localUser['role']);

                    if ($this->session->userdata['role'] == 'investor') {
                        redirect('investor/index'); 
                    }elseif ($this->session->userdata['role'] == 'investee') {
                        redirect('investee/index');
                    }
                } else {

                    $userData = $this->user_model->find_by_email($profileData['emailAddress']);

                    if ($userData) {

                        $this->user_model->update_linkedinid($userData['id'], $profileData['id'],$profileData['pictureUrl']);
                        //linked in user auto-login

                        $localUser = $this->user_model->get_linkedinuser($profileData['id']);

                        $this->session->set_userdata('user_id', $localUser['id']);
                        $this->session->set_userdata('facebook_id', $localUser['facebook_id']);
                        $this->session->set_userdata('google_picture', $localUser['google_picture']);
                        $this->session->set_userdata('linkedin_picture', $localUser['linkedin_picture']);
                        $this->session->set_userdata('image', $localUser['image']);
                        $this->session->set_userdata('email', $localUser['email']);
                        $this->session->set_userdata('company_name', $localUser['company_name']);
                        $this->session->set_userdata('email', $localUser['email']);
                        $this->session->set_userdata('role', $localUser['role']);

                        if ($this->session->userdata['role'] == 'investor') {
                            redirect('investor/index');  
                        }elseif ($this->session->userdata['role'] == 'investee') {
                            redirect('investee/index');
                        }
                    } else {
                      
                        $bodyData['linkedin_id'] = $profileData['id'];
                        $bodyData['fname'] = $profileData['firstName'];
                        $bodyData['lname'] = $profileData['lastName'];
                        $bodyData['email'] = $profileData['emailAddress'];
                        $pictures = get_object_vars($profileData['pictureUrls']);
                        $bodyData['linkedin_picture'] = $pictures['values'][0];
                        
                    //  die(print_r($profileData));
                        $this->layout['content'] = $this->load->view('user/linkedin_register', $bodyData, true);
                        $this->load->view('layouts/front', $this->layout);
                        
                    }
                }

                        
        } else{
           // linked return an empty array of profile data
        }
    }
        
    function linkedin_process() {

        $linkedinId = $this->input->post('linkedin_id');
        $role = $this->input->post('role');
        $email = $this->input->post('email');
        $linkedin_picture =  $this->input->post('linkedin_picture');
        $random = mt_rand();
        $pass = md5($random);
        $insertData = array(
                'name' => $this->input->post('name'),
                'mob_no' => $this->input->post('mobileno'),
                'email' => $email,
                'role' => $role,
                'password'=>$pass,
                'linkedin_id'=>$linkedinId,
                'linkedin_picture' => $linkedin_picture
            );
            if($this->input->post('company_name')){
               $insertData['company_name'] = $this->input->post('company_name');
            }
        
        $userId = $this->user_model->add_user($insertData);    
        $userData = $this->user_model->get_linkedinuser($linkedinId);
       

        $this->session->set_userdata('user_id', $userData['id']);
        $this->session->set_userdata('facebook_id', $userData['facebook_id']);
        $this->session->set_userdata('google_picture', $userData['google_picture']);
        $this->session->set_userdata('linkedin_picture', $userData['linkedin_picture']);
        $this->session->set_userdata('image', $userData['image']);
        $this->session->set_userdata('email', $userData['email']);
        $this->session->set_userdata('company_name', $userData['company_name']);
        $this->session->set_userdata('email', $userData['email']);
        $this->session->set_userdata('role', $userData['role']);

        if ($this->session->userdata['role'] == 'investor') {
           redirect('investor/index'); 
        }elseif ($this->session->userdata['role'] == 'investee') {
            redirect('investee/index');
        } 
    }
    
    public function googleplus_login() {
        session_start();
        $this->load->library('googleplus');
        if (isset($_GET['code'])) {
            $this->googleplus->client->authenticate();
            $_SESSION['token'] = $this->googleplus->client->getAccessToken();
            $redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
            header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
        }
        if (isset($_SESSION['token'])) {
            $this->googleplus->client->setAccessToken($_SESSION['token']);
        }
        if ($this->googleplus->client->getAccessToken()) {
            $userProfile = $this->googleplus->people->get('me');
            $_SESSION['token'] = $this->googleplus->client->getAccessToken();

            $localUser = $this->user_model->get_googleuser($userProfile['id']);

                if ($localUser) {
                    $this->session->set_userdata('user_id', $localUser['id']);
                    $this->session->set_userdata('facebook_id', $localUser['facebook_id']);
                    $this->session->set_userdata('google_picture', $localUser['google_picture']);
                    $this->session->set_userdata('linkedin_picture', $localUser['linkedin_picture']);
                    $this->session->set_userdata('image', $localUser['image']);
                    $this->session->set_userdata('email', $localUser['email']);
                    $this->session->set_userdata('company_name', $localUser['company_name']);
                    $this->session->set_userdata('email', $localUser['email']);
                    $this->session->set_userdata('role', $localUser['role']);

                    if ($this->session->userdata['role'] == 'investor') {
                        redirect('investor/index'); 
                    }elseif ($this->session->userdata['role'] == 'investee') {
                        redirect('investee/index');
                    }
                } else {

                    $userData = $this->user_model->find_by_email($userProfile['emails'][0]['value']);

                    if ($userData) {

                        $this->user_model->update_googleid($userData['id'], $userProfile['id'],$userProfile['image']['url']);
                        //linked in user auto-login

                        $localUser = $this->user_model->get_googleuser($userProfile['id']);

                        $this->session->set_userdata('user_id', $localUser['id']);
                        $this->session->set_userdata('facebook_id', $localUser['facebook_id']);
                        $this->session->set_userdata('google_picture', $localUser['google_picture']);
                        $this->session->set_userdata('linkedin_picture', $localUser['linkedin_picture']);
                        $this->session->set_userdata('image', $localUser['image']);
                        $this->session->set_userdata('email', $localUser['email']);
                        $this->session->set_userdata('company_name', $localUser['company_name']);
                        $this->session->set_userdata('email', $localUser['email']);
                        $this->session->set_userdata('role', $localUser['role']);

                        if ($this->session->userdata['role'] == 'investor') {
                            redirect('investor/index');  
                        }elseif ($this->session->userdata['role'] == 'investee') {
                            redirect('investee/index');
                        }
                    } else {
                      
                        $bodyData['google_id'] = $userProfile['id'];
                        $bodyData['google_picture'] = $userProfile['image']['url'];
                        $bodyData['fname'] = $userProfile['name']['givenName'];
                        $bodyData['lname'] = $userProfile['name']['familyName'];
                        $bodyData['email'] = $userProfile['emails'][0]['value'];
                        
                    //  die(print_r($profileData));
                        $this->layout['content'] = $this->load->view('user/google_register', $bodyData, true);
                        $this->load->view('layouts/front', $this->layout);
                        
                    }
                }
            
        } else {
            $authUrl = $this->googleplus->client->createAuthUrl();
            header('Location: ' . $authUrl);
        }
    }
    
    function googleplus_process() {

        $googleId = $this->input->post('google_id');
        $role = $this->input->post('role');
        $email = $this->input->post('email');
        $google_picture =  $this->input->post('google_picture');
        $random = mt_rand();
        $pass = md5($random);
        $insertData = array(
                'name' => $this->input->post('name'),
                'mob_no' => $this->input->post('mobileno'),
                'email' => $email,
                'role' => $role,
                'password'=>$pass,
                'google_id'=>$googleId,
                'google_picture' => $google_picture
            );
            if($this->input->post('company_name')){
               $insertData['company_name'] = $this->input->post('company_name');
            }
        
        $userId = $this->user_model->add_user($insertData);    
        $userData = $this->user_model->get_googleuser($googleId);
       

        $this->session->set_userdata('user_id', $userData['id']);
        $this->session->set_userdata('facebook_id', $userData['facebook_id']);
        $this->session->set_userdata('google_picture', $userData['google_picture']);
        $this->session->set_userdata('linkedin_picture', $userData['linkedin_picture']);
        $this->session->set_userdata('image', $userData['image']);
        $this->session->set_userdata('email', $userData['email']);
        $this->session->set_userdata('company_name', $userData['company_name']);
        $this->session->set_userdata('email', $userData['email']);
        $this->session->set_userdata('role', $userData['role']);

        if ($this->session->userdata['role'] == 'investor') {
           redirect('investor/index'); 
        }elseif ($this->session->userdata['role'] == 'investee') {
            redirect('investee/index');
        } 
    }
    
    public function logout(){
        $this->session->sess_destroy();
        redirect(base_url() . 'user/login');
    }

    public function investor_info(){
        if($this->session->userdata('user_id') == ''){
            redirect('user/login');
        }
        $user_id = $this->session->userdata('user_id');
        $countries = $this->user_model->countries();
        $sectors = $this->user_model->sectors();
        $viewData['sectors'] = $sectors;
        $viewData['countries'] = $countries;
        $viewData['user_id'] = $user_id;
        
        $user = $this->user_model->find_investor($user_id);
        if($user){
            $viewData['user'] = $user;
            $states = $this->user_model->states($user['country']);
            $portfolios = array();
            $portfolios = $this->investor_model->get_portfolios($user_id);
            
           
            $viewData['user'] = $user;
            $viewData['portfolios'] = $portfolios;
            $viewData['states'] = $states;
            
            
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('user/edit_investor_info', $bodyData, true);
            $this->load->view('layouts/investor', $this->layout);
        }else{
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('user/investor_info', $bodyData, true);
            $this->load->view('layouts/investor', $this->layout);
        }
        
        
    }
    
    public function channel_partner_info(){
        $user_id = $this->session->userdata('user_id');
        $countries = $this->user_model->countries();
        $sectors = $this->user_model->sectors();
        $viewData['sectors'] = $sectors;
        $viewData['countries'] = $countries;
        $viewData['user_id'] = $user_id;
        
        $user = $this->user_model->find_channel_partner($user_id);
        if($user){
            $viewData['user'] = $user;
            $states = $this->user_model->states($user['country']);
            $portfolios = array();
            $portfolios = $this->investor_model->get_portfolios($user_id);
            
           
            $viewData['user'] = $user;
            $viewData['portfolios'] = $portfolios;
            $viewData['states'] = $states;
            
            
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('user/edit_channel_partner_info', $bodyData, true);
            $this->load->view('layouts/channel_partner', $this->layout);
        }else{
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('user/channel_partner_info', $bodyData, true);
            $this->load->view('layouts/channel_partner', $this->layout);
        }
    }
    
    public function channel_partner_info_process(){
        $id = $this->input->post('user_id');
        $path = "./uploads/users/" . $id;

        if (!is_dir($path)) {
            mkdir($path);
        }
        
        $sector_expertise = '';
        $post=$this->input->post('sector_expertise');
        if(!empty($post)){
            $sector_expertise = implode(",",$this->input->post('sector_expertise'));
        }
        
        
        
        $insertData = array(
            'user_id' => $id,
            'experience' => $this->input->post('experience'),
            'role' => $this->input->post('role'),
            'area_of_expertise'=>  $this->input->post('expertise'),
            'sector_expertise'=> $sector_expertise,
        );
        
        $this->user_model->add_channel_partner($insertData);
        
        $company_name='';
        if($this->input->post('company_name')){
          $company_name = $this->input->post('company_name'); 
        }
        $data = array(
            'company_name'=> $company_name,
            'country' => $this->input->post('country'),
            'state' => $this->input->post('state'),
            'city' => $this->input->post('city'),
            'fb_url'=> $this->input->post('fb_url'),
            'linkedin_url'=> $this->input->post('linkedin_url'),
            'twitter_handle'=> $this->input->post('twitter_handle')
        );
        
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '2048';
        $config['max_width'] = '1024';
        $config['max_height'] = '768';

        $this->load->library('upload', $config);
        
        $file = "image";
        if(!$this->upload->do_upload($file)){
            die(print_r($this->upload->display_errors()));
        }else{
            $upload_data = $this->upload->data();
            $data['image'] = $upload_data['file_name'];
        }
        
        $this->user_model->update_user($data,$id);
        
        redirect('user/channel_partner_info');
    }
    
    public function edit_channel_partner_info_process(){
        $id = $this->input->post('user_id');
        
        $sector_expertise = '';
        $post=$this->input->post('sector_expertise');
        if(!empty($post)){
            $sector_expertise = implode(",",$this->input->post('sector_expertise'));
        }
        
        
        
        $updateInvestor = array(
            'experience' => $this->input->post('experience'),
            'role' => $this->input->post('role'),
            'area_of_expertise'=>  $this->input->post('expertise'),
            'sector_expertise'=> $sector_expertise
        );
        
        $this->user_model->update_channel_partner($updateInvestor,$id);
      
         $company_name='';
        if($this->input->post('company_name')){
          $company_name = $this->input->post('company_name'); 
        }
        $data = array(
            'company_name'=> $company_name,
            'country' => $this->input->post('country'),
            'state' => $this->input->post('state'),
            'city' => $this->input->post('city'),
             'fb_url'=> $this->input->post('fb_url'),
            'linkedin_url'=> $this->input->post('linkedin_url'),
            'twitter_handle'=> $this->input->post('twitter_handle'),
        );
         
         $this->user_model->update_user($data,$id);
         
         redirect('user/channel_partner_info');
    }
    
    public function investor_info_process(){
        
        $id = $this->input->post('user_id');
        $path = "./uploads/users/" . $id;

        if (!is_dir($path)) {
            mkdir($path);
        }
        
        $sector_expertise = '';
        $post=$this->input->post('sector_expertise');
        if(!empty($post)){
            $sector_expertise = implode(",",$this->input->post('sector_expertise'));
        }
       
        $insertData = array(
            'user_id' => $id,
            'type' => $this->input->post('type'),
            'investor_name' => $this->input->post('name'),
            'experience' => $this->input->post('experience'),
            'role' => $this->input->post('role'),
            'mentoring_sectors'=>$this->input->post('mentoring_sectors'),
            'expertise'=>  $this->input->post('expertise'),
            'duration'=> $this->input->post('duration'),
            'sector_expertise'=> $sector_expertise,
            'key_points'=> $this->input->post('key_points'),
            'created_date'=> date('Y-m-d H:i:s')
        );
        
        $this->user_model->add_investor($insertData);
        
        $company_name='';
        if($this->input->post('company_name')){
          $company_name = $this->input->post('company_name'); 
        }
        $data = array(
            'company_name'=> $company_name,
            'name' => $this->input->post('name'),
            'country' => $this->input->post('country'),
            'state' => $this->input->post('state'),
            'city' => $this->input->post('city'),
            'fb_url'=> $this->input->post('fb_url'),
            'linkedin_url'=> $this->input->post('linkedin_url'),
            'twitter_handle'=> $this->input->post('twitter_handle')
        );
        
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '2048';
        $config['max_width'] = '1024';
        $config['max_height'] = '768';

        $this->load->library('upload', $config);
        
        $file = "image";
        if (!($_FILES[$file]['size'] == 0))
        {
          if(!$this->upload->do_upload($file)){
//                die(print_r($this->upload->display_errors()));
            }else{
                $upload_data = $this->upload->data();
                $data['image'] = $upload_data['file_name'];
            }
        }
        
        
        
        $this->user_model->update_user($data,$id);
        
        if($this->input->post('portfolio_name_1')){
            $portfolio_count = $this->input->post('portfolio_count');
            
            for($i=1; $i<=$portfolio_count;$i++){
            
                $portfolioData = array(
                    'user_id'=> $id,
                    'name' => $this->input->post('portfolio_name_'.$i),
                    'url' => $this->input->post('portfolio_url_'.$i),
                    'location' => $this->input->post('portfolio_location_'.$i),
                    'sector_id'=> $this->input->post('portfolio_sector_'.$i),
                    'role'=> $this->input->post('portfolio_role_'.$i),
                    'remark'=> $this->input->post('portfolio_remarks_'.$i)
                );

                $PortfolioId = $this->user_model->add_portfolio($portfolioData);

                $path = "./uploads/portfolios/" . $PortfolioId;
                $config['upload_path'] = $path;
                $this->upload->initialize($config);

                if (!is_dir($path)) {
                    mkdir($path);
                }
                $file = 'portfolio_image_'.$i;
                
              if (!($_FILES[$file]['size'] == 0))
                {
                    $portfolioImgData = array();
                    if(!$this->upload->do_upload($file)){
                        print_r($this->upload->display_errors());
                    }else{
                        $upload_data = $this->upload->data();
                        $portfolioImgData['image'] = $upload_data['file_name'];
                    }

                    $this->user_model->update_portfolio($portfolioImgData,$PortfolioId);
                }
                
            }
        }
        
        redirect('investor/profile');
    }
    
    public function edit_investor_info_process(){
        
        $id = $this->input->post('user_id');
        
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '2048';
        $config['max_width'] = '1024';
        $config['max_height'] = '768';

        $this->load->library('upload', $config);
        
        $sector_expertise = '';
        $post=$this->input->post('sector_expertise');
        if(!empty($post)){
            $sector_expertise = implode(",",$this->input->post('sector_expertise'));
        }
        
        
        
        $updateInvestor = array(
            'type' => $this->input->post('type'),
            'investor_name' => $this->input->post('name'),
            'experience' => $this->input->post('experience'),
            'role' => $this->input->post('role'),
            'mentoring_sectors'=>$this->input->post('mentoring_sectors'),
            'expertise'=>  $this->input->post('expertise'),
            'duration'=> $this->input->post('duration'),
            'sector_expertise'=> $sector_expertise,
            'key_points'=> $this->input->post('key_points'),
        );
        
        $this->user_model->update_investor($updateInvestor,$id);
      
         $company_name='';
        if($this->input->post('company_name')){
          $company_name = $this->input->post('company_name'); 
        }
        $data = array(
            'company_name'=> $company_name,
            'name' => $this->input->post('name'),
            'country' => $this->input->post('country'),
            'state' => $this->input->post('state'),
            'city' => $this->input->post('city'),
             'fb_url'=> $this->input->post('fb_url'),
            'linkedin_url'=> $this->input->post('linkedin_url'),
            'twitter_handle'=> $this->input->post('twitter_handle'),
        );
         
         $this->user_model->update_user($data,$id);
         $this->session->set_userdata('name', $this->input->post('name'));
         
         if($this->input->post('portfolio_name_1')){
            $portfolio_count = $this->input->post('portfolio_count');
            
            for($i=1; $i<=$portfolio_count;$i++){
                  $PortfolioEditId = $this->input->post('id_'.$i);
                  if($PortfolioEditId){
                      $portfolioData = array(
                            'name' => $this->input->post('portfolio_name_'.$i),
                            'url' => $this->input->post('portfolio_url_'.$i),
                            'location' => $this->input->post('portfolio_location_'.$i),
                            'sector_id'=> $this->input->post('portfolio_sector_'.$i),
                            'role'=> $this->input->post('portfolio_role_'.$i),
                            'remark'=> $this->input->post('portfolio_remarks_'.$i)
                        );
                      
                      $this->user_model->update_portfolio($portfolioData,$PortfolioEditId);
                  }else{
                      $portfolioData = array(
                            'user_id'=> $id,
                            'name' => $this->input->post('portfolio_name_'.$i),
                            'url' => $this->input->post('portfolio_url_'.$i),
                            'location' => $this->input->post('portfolio_location_'.$i),
                            'sector_id'=> $this->input->post('portfolio_sector_'.$i),
                            'role'=> $this->input->post('portfolio_role_'.$i),
                            'remark'=> $this->input->post('portfolio_remarks_'.$i)
                        );

                    $PortfolioId = $this->user_model->add_portfolio($portfolioData);

                    $path = "./uploads/portfolios/" . $PortfolioId;
                    $config['upload_path'] = $path;
                    $this->upload->initialize($config);

                    if (!is_dir($path)) {
                        mkdir($path);
                    }
                    
                    $file = 'portfolio_image_'.$i;
                    $portfolioImgData = array();
                    if(!$this->upload->do_upload($file)){
                        print_r($this->upload->display_errors());
                    }else{
                        $upload_data = $this->upload->data();
                        $portfolioImgData['image'] = $upload_data['file_name'];
                    }

                    $this->user_model->update_portfolio($portfolioImgData,$PortfolioId);

                  }
                
            }
        }
        
         redirect('investor/profile');
    }
    
    public function investee_info($activePanel=null){
        
        $viewData['activePanel'] = '';
        if($activePanel){
          $viewData['activePanel'] = $activePanel;
        }
            if($this->session->userdata('user_id') == ''){
                redirect('user/login');
            }
            
             $this->load->model('banners_model');
             $this->layout['banners']=  $this->banners_model->find_benners();
            
            $user_id = $this->session->userdata('user_id');
            $countries = $this->user_model->countries();
            $sectors = $this->user_model->sectors();
            $stages = $this->user_model->startup_stages();
            $types = $this->user_model->business_types();
            $viewData['countries'] = $countries;
            $viewData['sectors'] = $sectors;
            $viewData['stages'] = $stages;
            $viewData['types'] = $types;
            $viewData['user_id'] = $user_id;
            
            $user = $this->user_model->find_investee($user_id);
            if($user){
                $viewData['user'] = $user;
                $states = $this->user_model->states($user['country']);
                $viewData['states'] = $states;
                $purposes = $this->user_model->get_investor_use_of_funds($user_id);
                $viewData['purposes'] = $purposes;
                $privacy = $this->investee_model->get_privacy($user_id);
                $viewData['privacy'] = $privacy;
                $bodyData = $viewData;
                $this->layout['content'] = $this->load->view('user/edit_investee_info', $bodyData, true);
                $this->load->view('layouts/investee', $this->layout);
            }else{
                $user = $this->user_model->find_user($user_id);
                $viewData['user'] = $user;
                $bodyData = $viewData;
                $this->layout['content'] = $this->load->view('user/investee_info', $bodyData, true);
                $this->load->view('layouts/investee', $this->layout);
            }
       
        
    }
    
    public function investee_info_process(){
        $id = $this->input->post('user_id');
        $path = "./uploads/users/" . $id;

        if (!is_dir($path)) {
            mkdir($path);
        }
        
        $insertData = array(
            'user_id' => $id,
            'company_url' => $this->input->post('company_url'),
            'product_url' => $this->input->post('product_url'),
            'year' => $this->input->post('year'),
            'stage' => $this->input->post('stage'),
            'sector' => $this->input->post('sector'),
            'business_type' => $this->input->post('business_type'),
            'funding_history' => $this->input->post('funding_history'),
            'investment_required' => str_replace( ',', '', $this->input->post('investment_required') ),
            'commitment_per_investor' => str_replace( ',', '', $this->input->post('commitment_per_investor') ),
            'products_services' => $this->input->post('products_services'),
            'team_summary' => $this->input->post('team_summary'),
            'customer_traction' => $this->input->post('customer_traction'),
            'how_different' => $this->input->post('how_different'),
            'how_we_make_money' => $this->input->post('how_we_make_money'),
            'addressable_market' => str_replace( ',', '', $this->input->post('addressable_market') ),
            'fixed_opex' => str_replace( ',', '', $this->input->post('fixed_opex') ),
            'cash_burn' => str_replace( ',', '', $this->input->post('cash_burn') ),
            'debt' => str_replace( ',', '', $this->input->post('debt') ),
            'monthly_revenue' => str_replace( ',', '', $this->input->post('monthly_revenue') ),
            'competition' => $this->input->post('competition'),
            'equity_offered' => $this->input->post('equity_offered'),
            'validity_period' => $this->input->post('validity_period'),
            'created_date'=> date('Y-m-d H:i:s')
        );
        
        $this->user_model->add_investee($insertData);
        
        if($this->input->post('purpose')){
            $purposes = $this->input->post('purpose');
            $purpose_amounts = $this->input->post('purpose_amount');
            $purpose = array();
            foreach($purposes as $key => $prps){
                $purpose[$key]['user_id'] = $id;
                $purpose[$key]['purpose']= $prps;
                $purpose[$key]['amount']= str_replace( ',', '', $purpose_amounts[$key] );
            }
            
            $this->user_model->delete_investor_use_of_funds($id);
            $this->user_model->add_investor_use_of_funds($purpose);
        }
        
        $data = array(
            'country' => $this->input->post('country'),
            'state' => $this->input->post('state'),
            'city' => $this->input->post('city'),
            'fb_url'=> $this->input->post('fb_url'),
            'linkedin_url'=> $this->input->post('linkedin_url'),
            'twitter_handle'=> $this->input->post('twitter_handle'),
            'company_name' => $this->input->post('company_name'),
            'name' => $this->input->post('name'),
            'active' => 1
        );
        
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '2048';
        $config['max_width'] = '1024';
        $config['max_height'] = '768';

        $this->load->library('upload', $config);
        
        $file = "image";
        if(!$this->upload->do_upload($file)){
            die(print_r($this->upload->display_errors()));
        }else{
            $upload_data = $this->upload->data();
            $data['image'] = $upload_data['file_name'];
            $this->session->set_userdata('image', $upload_data['file_name']);
        }
        
        $this->user_model->update_user($data,$id);
        
        redirect('user/file_upload');
    }
    
    public function edit_investee_info_process(){
        $activePanel = '';
        if($this->input->post('company_submit')){
            $activePanel = 'team';
        }
        
        if($this->input->post('team_submit')){
             $activePanel = 'business'; 
        }
        
        if($this->input->post('business_submit')){
           $activePanel = 'revenue';
        }
        
        if($this->input->post('revenue_submit')){
            $activePanel = 'market';
        }
        
        if($this->input->post('market_submit')){
            $activePanel = 'raise';
        }
        
        if($this->input->post('raise_submit')){
            $activePanel = 'financial';
        }
        
        if($this->input->post('financial_submit')){
            $activePanel = 'financial';
        }
        
        
        $id = $this->input->post('user_id');
        $investment_required = str_replace( ',', '', $this->input->post('investment_required') );
          
        $updateInvestee = array(
            'company_url' => $this->input->post('company_url'),
            'product_url' => $this->input->post('product_url'),
            'year' => $this->input->post('year'),
            'stage' => $this->input->post('stage'),
            'sector' => $this->input->post('sector'),
            'business_type' => $this->input->post('business_type'),
            'funding_history' => $this->input->post('funding_history'),
            'investment_required' => $investment_required,
            'commitment_per_investor' => str_replace( ',', '', $this->input->post('commitment_per_investor') ),
            'products_services' => $this->input->post('products_services'),
            'team_summary' => $this->input->post('team_summary'),
            'customer_traction' => $this->input->post('customer_traction'),
            'how_different' => $this->input->post('how_different'),
            'how_we_make_money' => $this->input->post('how_we_make_money'),
            'addressable_market' => str_replace( ',', '', $this->input->post('addressable_market') ),
            
            'fixed_opex' => str_replace( ',', '', $this->input->post('fixed_opex') ),
            'cash_burn' => str_replace( ',', '', $this->input->post('cash_burn') ),
            'debt' => str_replace( ',', '', $this->input->post('debt') ),
            'monthly_revenue' => str_replace( ',', '', $this->input->post('monthly_revenue') ),
            
            'competition' => $this->input->post('competition'),
            'equity_offered' => $this->input->post('equity_offered'),
            'validity_period' => $this->input->post('validity_period'),
        );
        
        $this->user_model->update_investee($updateInvestee,$id);
        
        if($this->input->post('purpose')){
            $purposes = $this->input->post('purpose');
            $purpose_amounts = $this->input->post('purpose_amount');
            $purpose = array();
            foreach($purposes as $key => $prps){
                $purpose[$key]['user_id'] = $id;
                $purpose[$key]['purpose']= $prps;
                $purpose[$key]['amount']= str_replace( ',', '', $purpose_amounts[$key] );
            }
            
            $this->user_model->delete_investor_use_of_funds($id);
            $this->user_model->add_investor_use_of_funds($purpose);
        }
        
        
         $data = array(
            'country' => $this->input->post('country'),
            'state' => $this->input->post('state'),
            'city' => $this->input->post('city'),
            'fb_url'=> $this->input->post('fb_url'),
            'linkedin_url'=> $this->input->post('linkedin_url'),
            'twitter_handle'=> $this->input->post('twitter_handle'),
            'company_name' => $this->input->post('company_name'),
            'name' => $this->input->post('name'),
        );
         
         $this->user_model->update_user($data,$id);
         $this->session->set_userdata('name', $this->input->post('name'));
         
        $followingInvestors = $this->user_model->get_following_user_ids($id);
                
        $display_text = $this->session->userdata('name') . ' has updated his profile';
        $link = 'investee/view/'.$id;
        foreach($followingInvestors as $followingInvestor){
            if($id != $followingInvestor){
                $notification = array(
                    'sender_id' => $id,
                    'receiver_id' => $followingInvestor,
                    'display_text' => $display_text,
                    'link'=> $link,
                    'date_created' => date('Y-m-d H:i:s')
                );

                $this->user_model->add_notification($notification);
            }

        }
        
         redirect('user/investee_info/'.$activePanel);
    }
    
    public function states(){
       $countryId = $this->input->post('countryId');
       $states = $this->user_model->states($countryId);
       $viewData['states'] = $states;
      $this->load->view('user/states', $viewData);
    }
    
    public function follow(){
        $userId = $this->session->userdata('user_id');
         if ($this->input->post()) {
             $followId =  $this->input->post('followId');
             if($this->user_model->is_followed($userId,$followId)){
                 echo 'error';
             }else{
                 
                 $this->user_model->follow($userId,$followId);
                 $display_text = $this->session->userdata('name').' started following you';
                 $link =  $this->session->userdata('role').'/view/'.$userId;
                 $notification = array(
                     'sender_id'=> $userId,
                     'receiver_id'=> $followId,
                     'display_text'=> $display_text,
                     'link'=> $link,
                     'date_created'=> date('Y-m-d H:i:s')
                 );
                 $followed = $this->session->userdata('followed');
                 $followed[]= $followId;
                 $this->session->set_userdata('followed', $followed);
                 $this->user_model->add_notification($notification);
                 
                 if($this->session->userdata('role') == 'investor'){
                    $user = $this->user_model->find_user($followId);
                    $user_name = '';
                    if($user['company_name']){
                       $user_name = $user['company_name'];  
                    }else{
                       $user_name = $user['name']; 
                    }
                    $activity_text = $this->session->userdata('name') . ' started following <a href="'.base_url().$user['role'].'/view/'.$user['id'].'">'.$user_name.'</a>';
                    $activity = array(
                        'user_id' => $userId,
                        'activity' => $activity_text,
                        'date_created' => date('Y-m-d H:i:s')
                    );

                   $this->user_model->add_investor_activity($activity);
                 }
                 
                
                 echo 'success';
             }
         }
    }
    
    public function unfollow(){
        $userId = $this->session->userdata('user_id');
         if ($this->input->post()) {
             $followId =  $this->input->post('followId');
             
                 $this->user_model->unfollow($userId,$followId);
                 
                 $followed = $this->session->userdata('followed');
                 $key = array_search($followId, $followed);
                 unset($followed[$key]);
                 $this->session->set_userdata('followed', $followed);
                
                 echo 'success';
             
         }
    }
    
    public function mail(){
        $this->load->library('email');
        
        $this->email->set_mailtype("html");
        $this->email->from('abhijit@pyxis.co.in', 'Abhijit Nair');
        $this->email->to('amit@pyxis.co.in'); 

        $this->email->subject('Email Test');
        $message = 'Please click on below link to activate account.'."\r\n".'<br><br>
                     <a href="'.base_url() .'user/investor_info/45435345">Activation Link</a><br>';
        $this->email->message($message);	

        $this->email->send();

        echo $this->email->print_debugger();
    }
    
    public function success(){
        $bodyData = array();
        $this->layout['content'] = $this->load->view('user/success', $bodyData, true);
        $this->load->view('layouts/front', $this->layout);
    }
    
    public function file_upload(){
        $userId = $this->session->userdata('user_id');
        $privacy = $this->investee_model->get_privacy($userId);
        $viewData['privacy'] = $privacy;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('user/file_upload', $bodyData, true);
        $this->load->view('layouts/investee', $this->layout);
    }
    
    public function file_upload_process(){
        $userId = $this->session->userdata('user_id');
        
        $path = "./uploads/users/" . $userId."/files";

        if (!is_dir($path)) {
            mkdir($path);
        }
        
        $data = array('video_link'=>$this->input->post('proposal_video'));
        $data['presentation'] = $this->input->post('company_presentation');
        $data['awards'] = $this->input->post('awards');
        $data['testimonials'] = $this->input->post('testimonials');
        $data['media'] =$this->input->post('media');
        
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'pdf|doc|docx|jpg|png|gif|ppt|pptx|xsl|xslx';
        $config['max_size'] = '2048';
        
        $this->load->library('upload', $config);
        
//        $file = "company_presentation";
//        if(!$this->upload->do_upload($file)){
////            die(print_r($this->upload->display_errors()));
//        }else{
//            $upload_data = $this->upload->data();
//            $data['presentation'] = $upload_data['file_name'];
//        }
        
        $file = "financial_forecast";
        if(!$this->upload->do_upload($file)){
//            die(print_r($this->upload->display_errors()));
        }else{
            $upload_data = $this->upload->data();
            $data['financial_forecast'] = $upload_data['file_name'];
        }
        
        $file = "financial_statement";
        if(!$this->upload->do_upload($file)){
//            die(print_r($this->upload->display_errors()));
        }else{
            $upload_data = $this->upload->data();
            $data['financial_statement'] = $upload_data['file_name'];
        }
        
        $file = "other";
        if(!$this->upload->do_upload($file)){
//            die(print_r($this->upload->display_errors()));
        }else{
            $upload_data = $this->upload->data();
            $data['other'] = $upload_data['file_name'];
        }
      
//        $file = "media";
//        if(!$this->upload->do_upload($file)){
////            die(print_r($this->upload->display_errors()));
//        }else{
//            $upload_data = $this->upload->data();
//            $data['media'] = $upload_data['file_name'];
//        }
        
//        $file = "awards";
//        if(!$this->upload->do_upload($file)){
////            die(print_r($this->upload->display_errors()));
//        }else{
//            $upload_data = $this->upload->data();
//            $data['awards'] = $upload_data['file_name'];
//        }
        
//         $file = "testimonials";
//        if(!$this->upload->do_upload($file)){
////            die(print_r($this->upload->display_errors()));
//        }else{
//            $upload_data = $this->upload->data();
//            $data['testimonials'] = $upload_data['file_name'];
//        }
        
        
        
//        $file = "annual_report";
//        if(!$this->upload->do_upload($file)){
////            die(print_r($this->upload->display_errors()));
//        }else{
//            $upload_data = $this->upload->data();
//            $data['annual_report'] = $upload_data['file_name'];
//        }
//        
//        
//        
//        $file = "riskfactors";
//        if(!$this->upload->do_upload($file)){
////            die(print_r($this->upload->display_errors()));
//        }else{
//            $upload_data = $this->upload->data();
//            $data['riskfactors'] = $upload_data['file_name'];
//        }
        
//        $file = "fund_sources";
//        if(!$this->upload->do_upload($file)){
////            die(print_r($this->upload->display_errors()));
//        }else{
//            $upload_data = $this->upload->data();
//            $data['fund_sources'] = $upload_data['file_name'];
//        }
//        
//        $file = "pl_report";
//        if(!$this->upload->do_upload($file)){
////            die(print_r($this->upload->display_errors()));
//        }else{
//            $upload_data = $this->upload->data();
//            $data['pl_report'] = $upload_data['file_name'];
//        }
//        
//        $file = "bankers";
//        if(!$this->upload->do_upload($file)){
////            die(print_r($this->upload->display_errors()));
//        }else{
//            $upload_data = $this->upload->data();
//            $data['bankers'] = $upload_data['file_name'];
//        }
//        
//        $file = "lawyers";
//        if(!$this->upload->do_upload($file)){
////            die(print_r($this->upload->display_errors()));
//        }else{
//            $upload_data = $this->upload->data();
//            $data['lawyers'] = $upload_data['file_name'];
//        }
//        
//        $file = "auditors";
//        if(!$this->upload->do_upload($file)){
////            die(print_r($this->upload->display_errors()));
//        }else{
//            $upload_data = $this->upload->data();
//            $data['auditors'] = $upload_data['file_name'];
//        }
//        
//        $file = "share_valuation";
//        if(!$this->upload->do_upload($file)){
////            die(print_r($this->upload->display_errors()));
//        }else{
//            $upload_data = $this->upload->data();
//            $data['share_valuation'] = $upload_data['file_name'];
//        }
        
        
        
        $this->user_model->update_investee($data,$userId);
        
        redirect('investee/index');
    }
    
    public function edit_file_upload(){
        $userId = $this->session->userdata('user_id');
        $user = $this->user_model->find_investee($userId);
        $privacy = $this->investee_model->get_privacy($userId);
        $viewData['privacy'] = $privacy;
        $viewData['user'] = $user;
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('user/edit_file_upload', $bodyData, true);
        $this->load->view('layouts/investee', $this->layout);
    }
    
        public function edit_file_upload_process(){
        $userId = $this->session->userdata('user_id');
        
        $path = "./uploads/users/" . $userId."/files";

        if (!is_dir($path)) {
            mkdir($path);
        }
        
        $data = array('video_link'=>$this->input->post('proposal_video'));
        $data['presentation'] = $this->input->post('company_presentation');
        $data['awards'] = $this->input->post('awards');
        $data['testimonials'] = $this->input->post('testimonials');
        $data['media'] =$this->input->post('media');
        
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'pdf|doc|docx|jpg|png|gif|ppt|pptx|xsl|xslx';
        $config['max_size'] = '2048';
        
        $this->load->library('upload', $config);
        

        
        $file = "financial_forecast";
        if(!$this->upload->do_upload($file)){
//            die(print_r($this->upload->display_errors()));
        }else{
            $upload_data = $this->upload->data();
            $data['financial_forecast'] = $upload_data['file_name'];
            
            $followingInvestors = $this->user_model->get_following_user_ids($userId);
                
            $display_text = $this->session->userdata('name') . ' has updated Financial Forecast';
            $link = 'investee/view/'.$userId;
            foreach($followingInvestors as $followingInvestor){
                if($userId != $followingInvestor){
                    $notification = array(
                        'sender_id' => $userId,
                        'receiver_id' => $followingInvestor,
                        'display_text' => $display_text,
                        'link'=> $link,
                        'date_created' => date('Y-m-d H:i:s')
                    );

                    $this->user_model->add_notification($notification);
                }

            }
        }
        
        $file = "financial_statement";
        if(!$this->upload->do_upload($file)){
//            die(print_r($this->upload->display_errors()));
        }else{
            $upload_data = $this->upload->data();
            $data['financial_statement'] = $upload_data['file_name'];
            
            $followingInvestors = $this->user_model->get_following_user_ids($userId);
                
            $display_text = $this->session->userdata('name') . ' has updated Financial Statements';
            $link = 'investee/view/'.$userId;
            foreach($followingInvestors as $followingInvestor){
                if($userId != $followingInvestor){
                    $notification = array(
                        'sender_id' => $userId,
                        'receiver_id' => $followingInvestor,
                        'display_text' => $display_text,
                        'link'=> $link,
                        'date_created' => date('Y-m-d H:i:s')
                    );

                    $this->user_model->add_notification($notification);
                }

            }
        }
        
        $file = "other";
        if(!$this->upload->do_upload($file)){
//            die(print_r($this->upload->display_errors()));
        }else{
            $upload_data = $this->upload->data();
            $data['other'] = $upload_data['file_name'];
        }
      
//        $file = "media";
//        if(!$this->upload->do_upload($file)){
////            die(print_r($this->upload->display_errors()));
//        }else{
//            $upload_data = $this->upload->data();
//            $data['media'] = $upload_data['file_name'];
//        }
        
//        $file = "awards";
//        if(!$this->upload->do_upload($file)){
////            die(print_r($this->upload->display_errors()));
//        }else{
//            $upload_data = $this->upload->data();
//            $data['awards'] = $upload_data['file_name'];
//        }
//        
//         $file = "testimonials";
//        if(!$this->upload->do_upload($file)){
////            die(print_r($this->upload->display_errors()));
//        }else{
//            $upload_data = $this->upload->data();
//            $data['testimonials'] = $upload_data['file_name'];
//        }
        
        $this->user_model->update_investee($data,$userId);
        
        redirect('investee/index');
    }
    
    public function md5_string(){
        $password = md5(trim($srting));
        echo $password;
    }
    
    function ajax_image_upload(){
        
        $id = $this->session->userdata('user_id');
        $path = "./uploads/users/" . $id;

        if (!is_dir($path)) {
            mkdir($path);
        }

        $config['upload_path'] = $path;
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '2048';
        $config['max_width'] = '1024';
        $config['max_height'] = '768';

        $this->load->library('upload', $config);

        $return_data = array();
        if(!$this->upload->do_upload()){
            $return_data['error'] = $this->upload->display_errors();
        }else{
            $upload_data = $this->upload->data();
            $data['image'] = $upload_data['file_name'];
            $this->user_model->update_user($data,$id);
            $img_url  = 'uploads/users/'.$id.'/'.$upload_data['file_name'];
            $return_data['img'] = $img_url;
        }
        
        echo json_encode($return_data);
        
    }
     
    public function portfolio(){
       $investees = $this->user_model->get_investees(9); 
        $viewData['users'] = $investees;
            
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('user/portfolio', $bodyData, true);
        $this->load->view('layouts/front', $this->layout);
    }
    
    function account_setting(){
        $this->load->model('banners_model');
        $this->layout['banners']=  $this->banners_model->find_benners();
        $viewData = array();
        $bodyData = $viewData;
        $this->layout['content'] = $this->load->view('user/account_setting', $bodyData, true);
        $this->load->view('layouts/'.$this->session->userdata('role'), $this->layout);
    }
    
    function account_setting_process(){
         if ($this->input->post()) {
             $id = $this->session->userdata('user_id');
             $pass = md5(trim($this->input->post('password')));
             $data = array('password'=>$pass);
             $this->user_model->update_user($data,$id);
             
            $this->session->set_flashdata('success-msg', 'Your password has been successfully changed.');
            redirect('user/account_setting');
         }

    }
    
    function get_notifications(){
        $userId = $this->session->userdata('user_id');
        
        $this->user_model->update_notifications($userId,array('seen'=>1));
        $notifications = $this->user_model->get_notifications($userId,4);
        if(empty($notifications)){
            echo '';
        }else{
            $viewData['notifications'] = $notifications;    
            $bodyData = $viewData;
            $this->layout['content'] = $this->load->view('user/notifications', $bodyData);
        }
        
    }
    
    function access_request(){
        if ($this->input->post()) {
             $userId = $this->session->userdata('user_id');
             $sectionId = $this->input->post('sectionId');
             $section = $this->input->post('section');
             $investeeId = $this->input->post('investeeId');
             if($userId != $investeeId){
                 $already_requested = $this->user_model->is_already_access_requested($userId,$investeeId,$sectionId);
                    if(!$already_requested){
                        $data = array('request_to' => $investeeId,
                                'request_by'=> $userId,
                                'section_id' => $sectionId,
                                'created_date' => date('Y-m-d H:i:s')
                            );
                        $rquestId = $this->user_model->add_access_request($data);
                        
                        $display_text = $this->session->userdata('name') . ' made request to access '.$section;
                        $link = 'investee/notifications';
                        $notification = array(
                            'sender_id' => $userId,
                            'receiver_id' => $investeeId,
                            'record_id' => $rquestId, 
                            'display_text' => $display_text,
                            'type' => 'access',
                            'link'=> $link,
                            'date_created' => date('Y-m-d H:i:s')
                        );

                        $this->user_model->add_notification($notification);
                        echo 'success';
                    }else{
                        echo 'error';
                    }
             }else{
                echo 'error' ;
             }
         }
    }
    
    function download_request(){
        if ($this->input->post()) {
             $userId = $this->session->userdata('user_id');
             $sectionId = $this->input->post('sectionId');
             $section = $this->input->post('section');
             $investeeId = $this->input->post('investeeId');
             if($userId != $investeeId){
                 $already_requested = $this->user_model->is_already_access_requested($userId,$investeeId,$sectionId);
                    if(!$already_requested){
                        $data = array('request_to' => $investeeId,
                                'request_by'=> $userId,
                                'section_id' => $sectionId,
                                'created_date' => date('Y-m-d H:i:s')
                            );
                        $rquestId = $this->user_model->add_access_request($data);
                        
                        $display_text = $this->session->userdata('name') . 'made request to access '.$section.' file.';
                        $link = 'investor/view/'.$userId;
                        $notification = array(
                            'sender_id' => $userId,
                            'receiver_id' => $investeeId,
                            'record_id' => $rquestId, 
                            'display_text' => $display_text,
                            'type' => 'access',
                            'link'=> $link,
                            'date_created' => date('Y-m-d H:i:s')
                        );

                        $this->user_model->add_notification($notification);
                        echo 'success';
                    }else{
                        echo 'error';
                    }
             }else{
                echo 'error' ;
             }
         }
    }
               
    function subscribe_process(){
        if ($this->input->post()) {
            $email = $this->input->post('email');
            $data = array('email'=> $email,
                        'created_date'=> date('Y-m-d H:i:s'));
            $subscriberId = $this->user_model->add_subscriber($data);
            
            $display_text = $email . ' subscribed Equity Crest newsletter\'s';
            $link = 'admin/subscribed_users';
            $notification = array(
                'record_id' => $subscriberId, 
                'display_text' => $display_text,
                'type' => 'subscribe',
                'table_name' => 'subscribe',
                'link'=> $link,
                'date_created' => date('Y-m-d H:i:s')
            );

            $this->user_model->add_admin_notification($notification);
        }
    }
        

}

/* End of file welcome.php */
/* Location: ./application/controllers/user.php */