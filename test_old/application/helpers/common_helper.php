<?php

function rand_string($length) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $str = '';
    $size = strlen($chars);
    for ($i = 0; $i < $length; $i++) {
        $str .= $chars[rand(0, $size - 1)];
    }
    return $str;
}

function DayDifference($start, $end) {
    $start = strtotime($start);
    $end = strtotime($end);
    $diff = $end - $start;
    return round($diff / 86400);
}

function format_money($amount){
    if($amount >= 10000000){
       $crores = round(($amount/10000000),1);
         if($crores == 1){
           return  $crores.' Crore';
         }else{
            return  $crores.' Crores'; 
         }
         
    }else if($amount >= 100000){
        $lakhs = round(($amount/100000),1);
        
        if($lakhs == 1){
           return  $lakhs.' Lakh';
         }else{
            return  $lakhs.' Lakhs'; 
         }
    }
}

function rupeeFormat($str)
{   $insertstr=',';
    $len = strlen($str);
    if($len > 4){
        switch ($len) {
            case 10:
                $pos = array(3,5,7);
                break;
            case 9:
                $pos = array(2,4,6);
                break;
            case 8:
                $pos = array(1,3,5);
                break;
            case 7:
                $pos = array(2,4);
                break;
            case 6:
                $pos = array(1,3);
                break;
            case 5:
                $pos = array(2);
                break;
        }
        
        $offset=-1;
        foreach($pos as $p)
        {
            $offset++;
            $str = substr($str, 0, $p+$offset) . $insertstr . substr($str, $p+$offset);
            
        }
    }
  
    return $str;
}

?>
