<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter Linked API Class
 *
 *
 * @package         CodeIgniter
 * @subpackage      Libraries
 * @category        Libraries
 * @author          Muhamamd Hafeez
 */
class Google {

    function __construct(){
           $CI =& get_instance();
           $CI->load->helper('url');
    }

    function getAuthUrl(){
        require_once 'Google/Client.php';
        require_once 'Google/Service/Oauth2.php';

        /************************************************
        ATTENTION: Fill in these values! Make sure
        the redirect URI is to this page, e.g:
        http://localhost:8080/user-example.php
        ************************************************/
        $client_id = '599725337673-03ubr4gfrl2g6d7ujhjkmbuooni5mi04.apps.googleusercontent.com';
        $client_secret = '90y1Yj5fg5gCg_5uRgQEFrat';
        $redirect_uri = 'http://localhost/gplus/examples/user-example.php';

        /************************************************
        Make an API request on behalf of a user. In
        this case we need to have a valid OAuth 2.0
        token for the user, so we need to send them
        through a login flow. To do this we need some
        information from our API console project.
        ************************************************/
        $client = new Google_Client();
        $client->setClientId($client_id);
        $client->setClientSecret($client_secret);
        $client->setRedirectUri($redirect_uri);
        //$client->setScopes("https://www.googleapis.com/auth/plus");
        $client->setScopes('https://www.googleapis.com/auth/userinfo.profile');

        /************************************************
        When we create the service here, we pass the
        client to it. The client then queries the service
        for the required scopes, and uses that when
        generating the authentication URL later.
        ************************************************/
        $oauth2Service = new Google_Service_Oauth2($client);

        /************************************************
        If we're logging out we just need to clear our
        local access token in this case
        ************************************************/
        if (isset($_REQUEST['logout'])) {
        unset($_SESSION['access_token']);
        }

        /************************************************
        If we have a code back from the OAuth 2.0 flow,
        we need to exchange that with the authenticate()
        function. We store the resultant access token
        bundle in the session, and redirect to ourself.
        ************************************************/
        if (isset($_GET['code'])) {
        $client->authenticate($_GET['code']);
        $_SESSION['access_token'] = $client->getAccessToken();
        $redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
        header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
        }

        /************************************************
        If we have an access token, we can make
        requests, else we generate an authentication URL.
        ************************************************/
        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
        $client->setAccessToken($_SESSION['access_token']);
        } else {
        $authUrl = $client->createAuthUrl();
        }
    }

}
/* End of file Linked.php */
/* Location: ./application/libraries/google.php */