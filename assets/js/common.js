
// for modal

function show_modal(container_id)
  {
    $(".modal").modal("hide");
    $("#"+container_id).modal("show");
  }

/* center modal */

/*function centerModals(){
    $('.modal').each(function(i){
      var $clone = $(this).clone().css('display', 'block').appendTo('body');
      var top = Math.round(($clone.height() - $clone.find('.modal-content').height()) / 2);
      top = top > 0 ? top : 0;
      $clone.remove();
      $(this).find('.modal-content').css("margin-top", top);
    });
  }*/

  $(function() {

   function reposition() {
       var modal = $(this),
           dialog = modal.find('.modal-dialog');
       modal.css('display', 'block');
       
       // Dividing by two centers the modal exactly, but dividing by three 
       // or four works better for larger screens.
       dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
   }
   // Reposition when a modal is shown
   $('.modal').on('show.bs.modal', reposition);
   // Reposition when the window is resized
   $(window).on('resize', function() {
       $('.modal:visible').each(reposition);
   });
});


//custom check box
function customCheckbox(checkboxName){
        var checkBox = $('input[name="'+ checkboxName +'"]');
        $(checkBox).each(function(){
            $(this).wrap( "<span class='custom-checkbox'></span>" );
            if($(this).is(':checked')){
                $(this).parent().addClass("selected");
                $(this).closest( "label" ).toggleClass("selected");
            }
        });
        $(checkBox).click(function(){
            $(this).parent().toggleClass("selected");
            $(this).closest( "label" ).toggleClass("selected");
            
        });
    }


    // set div height dynamic to for layout

  function deal_logo_height()
  {
      $('.deal-single .deal-logo').css('height','0px');
      var deal_logo_width = $('.deal-single .deal-logo').width();
      $('.deal-single .deal-logo').css('height', (deal_logo_width*(3/5)));   
      $('.deal-single .deal-image').css('height', (deal_logo_width*(3/5)));

  }

  function deal_fund_height()
  {
      var deal_fund_heightdiv = $('.deal-single .deal-funds').height();    
      $('.deal-single .deal-footer-links').css('height', (deal_fund_heightdiv+(36)));

  }
  
  function owl_testimonial()
  {
      var owl_testimonialdiv = $('.owl_testimonial_slide').height(); 

      $('.owl_testimonial_slide .row .col-xs-12').css('height', (owl_testimonialdiv));

  }

  function profile_image_round()
  {
      var profile_image_round = $('.profile-image img').width(); 

      $('.profile-image img').css('height', (profile_image_round));      

  }

  function in_the_media_image()
  {
      $('.in-media-img img').css('height','0px');
      var in_the_media_image = $('.in-media-img img').width();
      $('.in-media-img img').css('height', (in_the_media_image*(3/5))); 
  }

      function home_chart_height()
      {
          var o = $(window).width();

          if (o > 768) {

                  var home_chart_height = $('#chart-container').height(); 

                  $('.custom-legend').css('height', (home_chart_height));         

          }

      }

// custom blue background to modal      

function haveBackdrop() {
    if ($('.modal-backdrop').length > 0) {
        $('.modal-backdrop').addClass('my-modal-backdrop');
        clearTimeout(mBackdrop);
        return true;
    }
    return false;
}

var mBackdrop;


//window load
$(window).load(function() {
  
	$(".accordion, .company-accordion, .accordion2").accordion({
	    active: false,
	    autoHeight: false,
	    navigation: true,
	    collapsible: true
	});
    
});

// document ready
$(document).ready(function() {
	$('.equity-lazy').lazyload();
		
      customCheckbox("sector[]");
      customCheckbox("stage[]");
      customCheckbox("size[]");
      customCheckbox("location[]");
      customCheckbox("remember");
      customCheckbox("agree");
     //centerModals();
      profile_image_round();
      deal_logo_height();
      deal_fund_height();
      home_chart_height();
      in_the_media_image();
      //owl_testimonial(); 
    $('.accordion').on('hidden.bs.collapse', function () {
    $(this).find('.accordion-heading').addClass('accordion-opened');
      // do something…
    })
  
      // window resize
      $(window).resize(function(){
	  	  deal_logo_height();
        home_chart_height();
        profile_image_round();
        in_the_media_image();       
        
        if(owlHome)
        {
	  		owlHome.reinit();
	  	}
	  	
	  	if(owlTestimonial)
	  	{
	  		owlTestimonial.reinit();
	  	}
	  	
      });  



// custom blue background to modal

$('#registerModal').on('show.bs.modal', function() {
    mBackdrop = setTimeout("haveBackdrop()", 100);   
});

$('#registerUserModal').on('show.bs.modal', function() {
    mBackdrop = setTimeout("haveBackdrop()", 100);   
});

$('#registerStartUpModal').on('show.bs.modal', function() {
    mBackdrop = setTimeout("haveBackdrop()", 100);   
});

//****Accordion ***//

$(window).bind('resize load', function() {
    if ($(this).width() < 767) {
        $('.accordion-body').removeClass('in');
        $('.accordion-toggle').addClass('collapsed'); 
    } else {
        $('.accordion-body').addClass('in');
        $('.accordion-toggle').removeClass('collapsed');
    }
});
//****End accordion ***//

/***Checkbox All function ***/

$('#selectAllSectors').click(function(event) {  //on click
    var clickedItem = this; 
    $('input:checkbox[name="sector[]"]').each(function() { //loop through each checkbox
    if (this == clickedItem) {// SKIP THIS ITEM 
    }
    else {
           if(clickedItem.checked!==this.checked)
           $(this).trigger('click');  //select all checkboxes with class "checkbox1"
          }
       });
   });

  $('#selectAllStage').click(function(event) {  //on click
    var clickedItem = this; 
    $('input:checkbox[name="stage[]"]').each(function() { //loop through each checkbox
    if (this == clickedItem) {// SKIP THIS ITEM 
    }
    else {
           if(clickedItem.checked!==this.checked)
           $(this).trigger('click');  //select all checkboxes with class "checkbox1"
          }
       });
   });

  $('#selectAllSize').click(function(event) {  //on click
    var clickedItem = this; 
    $('input:checkbox[name="size[]"]').each(function() { //loop through each checkbox
    if (this == clickedItem) {// SKIP THIS ITEM 
    }
    else {
           if(clickedItem.checked!==this.checked)
           $(this).trigger('click');  //select all checkboxes with class "checkbox1"
          }
       });
   });

  $('#selectAllLocation').click(function(event) {  //on click
    var clickedItem = this; 
    $('input:checkbox[name="location[]"]').each(function() { //loop through each checkbox
    if (this == clickedItem) {// SKIP THIS ITEM 
    }
    else {
           if(clickedItem.checked!==this.checked)
           $(this).trigger('click');  //select all checkboxes with class "checkbox1"
          }
       });
   });
    
//**End checkbox all ***/


//Reset Form
$("#filter-clear").click(function() {
    $('#search_frm').find('label').removeClass("selected");
    $('#search_frm').find('span').removeClass("selected");
    $(':input','#search_frm').not(':button, :submit, :reset, :hidden').removeAttr('checked').removeAttr('selected');
    $("#search_frm input[type=text], textarea").val("");
});


   // home slider 
  $("#owl_home_slider").owlCarousel({
	  pagination: true,
	  slideSpeed : 300,
	  paginationSpeed: 400,
	  singleItem:true,
      autoPlay:8000,
      lazyLoad : true 
  });
  
  var owlHome = $("#owl_home_slider").data('owlCarousel');

    // home slider 
  $("#testimonial_slider").owlCarousel({
    pagination: true,
    slideSpeed : 300,
    paginationSpeed: 400,
    singleItem:true,
      autoPlay:8000,
      lazyLoad : true
  });

  var owlTestimonial = $("#testimonial_slider").data('owlCarousel');

  //notification main menu code
  $("#notificationLinkres").click(function()
  {
    $("#notificationContainerres").fadeToggle(300);
    $("#notification_countres").fadeOut("slow");
    return false;
    
    });

  //Document Click
  $(document).click(function()
  {
    $("#notificationContainerres").hide();
  });
  
  //Popup Click
  $("#notificationContainerres").click(function()
  {
    return false
  });


 /* Login **********************************************************************/

 $('#login-form').validate({
  rules: {

    email: {
      required: true,
      email: true
    },
    password: {
      //required : true
    }
  },
  messages: {
    email: {
      required: "Please enter you email id.",
      email: "Please enter a valid email id."
    },
    password: {
      required: "Password cannot be blank."
    }
  }
});

$("#forgot-psw-form").on('submit', (function(e) {
  e.preventDefault();
  $.ajax({
    url: "<?php echo base_url(); ?>user/send_pass_reset_link",
    type: 'POST',
    data: new FormData(this),
    contentType: false,
    cache: false,
    processData: false
  })
    .done(function(data) {
    if (data == 'success') {
      // $.colorbox.close();
      $('#forgotPasswordModal').modal("hide");
    } else {
      $('#email_error').html(data);
    }
  });

}));

$('.login_option a').on('click', function() {

  $('#login-register-link').removeClass('open');

});

//$.colorbox({inline: true, href: '#verify-pop', innerWidth: '400px', innerHeight: '100px'});
$(document).delegate('#verify', 'click', function() {
//$('#verify').on('click', function() {
  var id = $(this).data("id");
  console.log(id);
  $.post("user/verify_again", {
    id: id
  }).done(function() {
   $('#login-register-link .failure-msg').remove();
  show_modal('verifyPopModal');
  }
  );

});  
 
}); // (document).ready(function() end