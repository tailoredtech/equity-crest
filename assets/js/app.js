
$(window).resize(function(){
  // Resize Colorbox when resizing window or changing mobile device orientation
  resizeColorBox();
  window.addEventListener("orientationchange", resizeColorBox, false);
});

var resizeTimer;
function resizeColorBox() {
  if (resizeTimer) {
    clearTimeout(resizeTimer);
  }
  resizeTimer = setTimeout(function() {
    if (jQuery('#cboxOverlay').is(':visible')) {
      jQuery.colorbox.reload();
    }
  }, 300);
}
